window.onload = () => {
    var btnStartRecording = document.querySelector('#btn-start-recording');
    var btnStopRecording = document.querySelector('#btn-stop-recording');

    var videoElement = document.querySelector('.vid-confirm');

    var progressBar = document.querySelector('#progress-bar');
    var percentage = document.querySelector('#percentage');

    var recorder;
// reusable helpers

    var fd_test;

    $('#btn-stop-recording').hide();

// this function submits recorded blob to nodejs server
    function postFiles() {
        var blob = recorder.getBlob();
        console.log(blob);
        // getting unique identifier for the file name
        var fileName = generateRandomString() + '.webm';
        console.log(fileName);
        var file = new File([blob], fileName, {
            type: 'video/webm'
        });
        videoElement.src = '';
        videoElement.poster = '/img/ajax-loader.gif';
        xhr('https://coinenter.com/verification/', file, function (responseText) {
            var fileURL = JSON.parse(responseText).fileURL;
            console.info('fileURL', fileURL);
            console.log('Response' + JSON.parse(responseText));
            videoElement.src = fileURL;
            videoElement.play();
            videoElement.muted = true;
            videoElement.controls = true;
            //document.querySelector('#footer-h2').innerHTML = '<a href="' + videoElement.src + '">' + videoElement.src + '</a>';
        });

        if (mediaStream) mediaStream.stop();
    }

// XHR2/FormData
    function xhr(url, data, callback) {
        console.log(data);
        var request = new XMLHttpRequest();
        request.onreadystatechange = function () {
            if (request.readyState == 4 && request.status == 200) {
                callback(request.responseText);
                window.location = '/buy-bitcoin/step/4';
            }
        };

        request.upload.onprogress = function (event) {
            progressBar.max = event.total;
            progressBar.value = event.loaded;
            progressBar.innerHTML = 'Upload Progress ' + Math.round(event.loaded / event.total * 100) + "%";
        };

        request.upload.onload = function () {
            percentage.style.display = 'none';
            progressBar.style.display = 'none';
        };
        request.open('POST', url);
        request.setRequestHeader("X-Requested-With", "XMLHttpRequest");
        var formData = new FormData();
        if (localStorage.request && localStorage.hash) {
            formData.append('request_id', localStorage.request);
            formData.append('confirm_code', localStorage.hash);
        } else {
            console.log('No request id & hash!');
        }
        formData.append('file_0', data);

        request.send(formData);
    }

// generating random string
    function generateRandomString() {
        if (window.crypto) {
            var a = window.crypto.getRandomValues(new Uint32Array(3)),
                token = '';
            for (var i = 0, l = a.length; i < l; i++) token += a[i].toString(36);
            return token;
        } else {
            return (Math.random() * new Date().getTime()).toString(36).replace(/\./g, '');
        }
    }

    var mediaStream = null;
// reusable getUserMedia
    function captureUserMedia(success_callback) {
        var session = {
            audio: true,
            video: true
        };

        navigator.getUserMedia(session, success_callback, function (error) {
            alert('Unable to capture your camera. Please try again.');
            console.error(error);
            $('#btn-stop-recording').hide();
            e.preventDefault();
            btnStartRecording.disabled = false;
            btnStopRecording.disabled = true;
            videoElement.classList.remove('vid-confirm-active');

        });
    }

// UI events handling
    btnStartRecording.onclick = function () {
        alert('true');
        $('#btn-stop-recording').show();
        btnStartRecording.disabled = true;
        videoElement.classList.add('vid-confirm-active');

        captureUserMedia(function (stream) {
            mediaStream = stream;

            videoElement.src = window.URL.createObjectURL(stream);
            videoElement.play();
            videoElement.muted = true;
            videoElement.controls = false;

            recorder = RecordRTC(stream, {
                type: 'video'
            });

            recorder.startRecording();

            // enable stop-recording button
            btnStopRecording.disabled = false;
        });
    };
    btnStopRecording.onclick = function (e) {
        $('#btn-stop-recording').hide();
        e.preventDefault();
        btnStartRecording.disabled = false;
        btnStopRecording.disabled = true;

        videoElement.classList.remove('vid-confirm-active');

        recorder.stopRecording(postFiles);
        console.log('sending files');
    };
    window.onbeforeunload = function () {
        startRecording.disabled = false;
    };
}
