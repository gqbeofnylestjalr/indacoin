/*const SimpleNodeLogger = require('simple-node-logger'),
opts = {
    logFilePath:'history.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
},
slog = SimpleNodeLogger.createSimpleLogger( opts );

var colors = require('colors');
var setCookie = require('set-cookie');*/
var colors = require('colors');
const FastBootAppServer = require('fastboot-app-server');

function log(message) {
    console.log(colors.green(message));
}

function log2(message) {
    console.log(colors.green(message));
}

let server = new FastBootAppServer({
    distPath: 'dist',
    gzip: true,
    port: 4200,
    beforeMiddleware(app) {
        app.use((request, response, next) => {
            try {
                const locales = ["ar", "de", "ar", "en", "es", "fr", "it", "pt", "ru", "tr"];
                const host = request.headers.host; // "indacoin.com"
                const url = request.url; // "/change"
                let country = request.headers['cf-ipcountry']; // RU
                
                // Отсеиваем запросы картинок для присовения cookie и редиректа, стилей и прочего по URL запроса
                if (url.slice(0, 6).toLowerCase() === '/ember' || url.slice(0, 7).toLowerCase() === '/assets') {
                    console.log('redirect');
                }
                //log('Request = ' + host + url);

                if ('/robots.txt' == request.url) {
                    if (host[host.length - 2] + host[host.length - 1] == 'io') {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        response.send("User-agent: *\nDisallow: /");
                        return next();
                    } else {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        response.send("User-agent: *\nCrawl-delay: 10");
                        return next();
                    }
                    log('Не должно выполниться');
                }

                let detectLang = false;

                // For SEO aliase indacoin.com/ = indacoin.com/en_GB
                if (url === '/') {
                    console.log('Find route: /');
                    detectLang = 'en_GB';
                }

                // Redirect from indacoin.com/en_GB/change/buy-bitcoin-with-cardusd to indacoin.com/change
                if (url.toLowerCase() === '/en_gb/change/buy-bitcoin-with-cardusd') {
                    let newUrl = 'https://' + host + "/change";
                    log('Redicrect from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from tr.indacoin.com/change to indacoin.com/tr_TR/change/buy-bitcoin-with-cardusd
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/change' || url === '/change/')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country + '/change/buy-bitcoin-with-cardusd';
                    //response.cookie('lang', host.substr(0, 2) + '_' + country);
                    console.log('Redicrect from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from ru.indacoin.com/ to indacoin.com/ru_RU
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/' || url === '')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country;
                    response.cookie('lang', host.substr(0, 2) + '_' + country);
                    log('Redicrect from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 31) https://tr.indacoin.com/verification -> https://indacoin.com/en_GB/help
                if (host.slice(0, 2) === 'tr' && url === '/verification') {
                    let newUrl = 'https://' + host.slice(3) + '/en_GB/help';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 32) https://ru.indacoin.com/news/verifikacija-pri-pervoj-oplate-bankovskoj-kartoj -> https://indacoin.com/ru_RU/news
                if (host.slice(0, 2) === 'ru' && url === '/news/verifikacija-pri-pervoj-oplate-bankovskoj-kartoj') {
                    let newUrl = 'https://' + host.slice(3) + '/ru_RU/news';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from ru.indacoin.com/en_GB/news to indacoin.com/en_GB/news
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com') {
                    let newUrl = '';
                    if (url.slice(0, 1) === '/' && url.slice(3, 4) === '_')
                        newUrl = 'https://' + host.slice(3) + '/' + host.slice(0, 2) + url.slice(3);
                    else
                        newUrl = 'https://' + host.slice(3) + url;
                    log('Redicrect from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Examples:
                // indacoin.com/change/tr -> indacoin.com/tr_GB/change/buy-bitcoin-with-cardusd -> indacoin.com/en_GB/change

                // indacoin.com/change/tr -> indacoin.com/tr_TR/change/buy-bitcoin-with-cardusd
                // indacoin.com/change/es -> indacoin.com/es_ES/change/buy-bitcoin-with-cardusd
                if (locales.indexOf(url.slice(8).toLowerCase()) !== -1 && url.slice(0, 7).toLowerCase() === '/change') {
                    let newUrl = 'https://' + host + '/' + url.slice(8).toLowerCase() + '_GB/change/buy-bitcoin-with-cardusd';
                    log('Redicrect from to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 1) indacoin.com/change/ethereum -> indacoin.com/en_GB/change/buy-Ethereum-with-cardusd
                if (url.toLowerCase() === '/change/ethereum') {
                    let newUrl = 'https://' + host + '/en_GB/change/buy-ethereum-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 2) indacoin.com/news -> indacoin.com/en_GB/news
                else if (url.toLowerCase() === '/news') {
                    let newUrl = 'https://' + host + '/en_GB/news';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 3) indacoin.com/api -> indacoin.com/en_GB/api
                else if (url.toLowerCase() === '/api') {
                    let newUrl = 'https://' + host + '/en_GB/api';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 4) indacoin.com/affiliate -> indacoin.com/en_GB/affiliate
                else if (url.toLowerCase() === '/affiliate') {
                    let newUrl = 'https://' + host + '/en_GB/affiliate';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 5) indacoin.com/help -> indacoin.com/en_GB/help
                else if (url.toLowerCase() === '/help') {
                    let newUrl = 'https://' + host + '/en_GB/help';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 6) indacoin.com/terms-of-use -> indacoin.com/en_GB/terms-of-use
                else if (url.toLowerCase() === '/terms-of-use') {
                    let newUrl = 'https://' + host + '/en_GB/terms-of-use';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 8) indacoin.com/login -> indacoin.com/en_GB/login
                else if (url.toLowerCase() === '/login') {
                    let newUrl = 'https://' + host + '/en_GB/login';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 9) indacoin.com/register -> indacoin.com/en_GB/register
                else if (url.toLowerCase() === '/register') {
                    let newUrl = 'https://' + host + '/en_GB/register';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 10) indacoin.com/charts/* -> indacoin.com/en_GB/bitcoin
                else if (url.slice(0, 7).toLowerCase() === '/charts') {
                    let newUrl = 'https://' + host + '/en_GB/bitcoin';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }


                ////////////////////////////////////
                //////////// New redirects /////////
                ////////////////////////////////////
                // 11) https://indacoin.com/change/qiwi_bitcoin -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/qiwi_bitcoin') {
                    let newUrl = 'https://' + host + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 12) https://indacoin.com/change/gb -> https://indacoin.com/
                else if (url.toLowerCase() === '/change/gb') {
                    let newUrl = 'https://' + host;
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 13) https://indacoin.com/change/cn -> https://indacoin.com/en_CN/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/cn') {
                    let newUrl = 'https://' + host + '/en_CN/change/buy-bitcoin-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 14) https://indacoin.com/change/sy -> https://indacoin.com/en_SY/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/sy') {
                    let newUrl = 'https://' + host + '/en_SY/change/buy-bitcoin-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 15) https://indacoin.com/change/nl -> https://indacoin.com/en_NL/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/nl') {
                    let newUrl = 'https://' + host + '/en_NL/change/buy-bitcoin-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 16) https://indacoin.com/change/id -> https://indacoin.com/en_ID/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/id') {
                    let newUrl = 'https://' + host + '/en_ID/change/buy-bitcoin-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 17) https://indacoin.com/change/ch -> https://indacoin.com/en_CH
                else if (url.toLowerCase() === '/change/ch') {
                    let newUrl = 'https://' + host + '/en_CH';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 18) https://indacoin.com/change/ca -> https://indacoin.com/en_CA
                else if (url.toLowerCase() === '/change/ca') {
                    let newUrl = 'https://' + host + '/en_CA';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 19) https://indacoin.com/change/il -> https://indacoin.com/en_IL
                else if (url.toLowerCase() === '/change/il') {
                    let newUrl = 'https://' + host + '/en_IL';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 20) https://indacoin.com/change/in -> https://indacoin.com/en_IN
                else if (url.toLowerCase() === '/change/in') {
                    let newUrl = 'https://' + host + '/en_IN';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 21) https://indacoin.com/change/hu -> https://indacoin.com/en_HU
                else if (url.toLowerCase() === '/change/hu') {
                    let newUrl = 'https://' + host + '/en_HU';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 22) https://indacoin.com/change/by -> https://indacoin.com/ru_BY
                else if (url.toLowerCase() === '/change/by') {
                    let newUrl = 'https://' + host + '/ru_BY';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 23) https://indacoin.com/change/us -> https://indacoin.com/en_US
                else if (url.toLowerCase() === '/change/us') {
                    let newUrl = 'https://' + host + '/en_US';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 24) https://indacoin.com/change/buy-xrp-with-usd -> https://indacoin.com/en_GB/change/buy-ripple-with-cardusd
                else if (url.toLowerCase() === '/change/buy-xrp-with-usd') {
                    let newUrl = 'https://' + host + '/en_GB/change/buy-ripple-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 25) https://indacoin.com/change/buy-ethereum-with-cardeur/US-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-ethereum-with-cardeur/us-en') {
                    let newUrl = 'https://' + host + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 26) https://indacoin.com/change/buy-bitcoin-with-cardeur/eg-ar -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/eg-ar') {
                    let newUrl = 'https://' + host + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 27) https://indacoin.com/change/buy-bitcoin-with-cardeur/ua-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/ua-en') {
                    let newUrl = 'https://' + host + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 28) https://indacoin.com/change/buy-bitcoin-with-cardeur/ua-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/ua-en') {
                    let newUrl = 'https://' + host + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 29) https://tr.indacoin.com/trade -> https://indacoin.com/change
                if (host.slice(0, 2) === 'tr' && url === '/trade') {
                    let newUrl = 'https://' + host.slice(3) + '/change';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 30) https://tr.indacoin.com/rates -> https://indacoin.com/en_GB/help
                if (host.slice(0, 2) === 'tr' && url === '/rates') {
                    let newUrl = 'https://' + host.slice(3) + '/en_GB/help';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // ========================================
                // ========================================
                // ========================================

                // 33) https://indacoin.com/change/sg -> https://indacoin.com/en_SG/change/buy-bitcoin-with-cardusd
                if (url.toLowerCase() === '/change/sg') {
                    let newUrl = 'https://' + host + '/en_SG/change/buy-bitcoin-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 2) Смотрим в cookie: 'lang: ru_RU'
                if (detectLang === false) {
                    let cookie = request.headers['cookie'];
                    if (cookie !== undefined) {
                        cookie.split(' ').forEach(function(item) {
                            if (item.split('=')[0] === 'lang') {
                                detectLang = item.split('=')[1].replace(';', '');
                                console.log('Find route from cookie: /' + detectLang);
                            }
                        });
                    }
                }

                if (detectLang === false) {
                    let region = 'GB';
                    let lang = 'en';
                    detectLang = lang + '_' + region;
                }

                var ip = request.headers['x-real-ip'];
                if (ip == undefined)
                    ip = request.headers['cf-connecting-ip'];
                response.cookie('lang', detectLang);
                if (url.substr(1, 5).toLowerCase() === "xx_xx") {
                    let newUrl = url.slice(0, 1) + detectLang + url.slice(6);
                    response.redirect(newUrl);
                }

                return next();
                return 0;
            } catch (e) {
                console.log('Ошибка: ' + e.message);
                return next();
            }

        });
    }
});
server.start();
log('Start server');
