'use strict';

const DISTRIBUTIVE = 'dist'; // last worked = dist2, 8, 3, 4, 6, 7, 9, 8
//NODE_ENV = 'production';
const FASTBOOT_REDIS_HOST = '127.0.0.1';
const FASTBOOT_REDIS_PORT = '6379';

const bodyParser = require('body-parser');
const parser = require('accept-language-parser');
const FastBootAppServer = require('fastboot-app-server');
//const RedisCache = require('fastboot-redis-cache');
const RSVP = require('rsvp');
const path = require('path');
const requestIp = require('request-ip');
var colors = require('colors');

var methodOverride = require('method-override');


//var RateLimit = require('express-rate-limit');

function log(message) {
    console.log(colors.green(message));
}

function errorLog(message) {
    console.log(colors.red(message));
}

const SimpleNodeLogger2 = require('simple-node-logger'),
opts2 = {
    logFilePath:'logs/history-errors.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
},
slogError = SimpleNodeLogger2.createSimpleLogger( opts2 );

const SimpleNodeLogger = require('simple-node-logger'),
opts = {
    logFilePath:'logs/history.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
},
slog = SimpleNodeLogger.createSimpleLogger( opts );


// let cache = new RedisCache({
//     port: FASTBOOT_REDIS_PORT,
//     expiration: 20, // 20 seconds
//     cacheKey(path, request) {
//         let cookie = request.headers['cookie'];
//         let detectLang = null;
//         if (cookie !== undefined) {
//             cookie.split(' ').forEach(function(item) {
//                 if (item.split('=')[0] === 'lang') {
//                     let lang = item.split('=')[1].replace(';', '');
//                     if (lang[2] === '_' && lang.length === 5) {
//                         detectLang = lang;
//                     }
//                 }
//             });
//         }
//         if (detectLang)
//             return `${path} ${detectLang}`;
//         else
//             return `${path}`;
//       }
// });

// var limiter = new RateLimit({
//     windowMs: 15*60*1000, // 15 minutes
//     max: 100, // limit each IP to 100 requests per windowMs
//     delayMs: 0 // disable delaying - full speed until the max limit is reached
//   });

// var limiter = new RateLimit({
//     windowMs: 5*60*1000, // 5 minutes
//     max: 1000,//1000, // limit each IP to 100 requests per windowMs
//     delayMs: 0, // disable delaying - full speed until the max limit is reached
//     onLimitReached: function(req, res, options) {
//         const ipAdress = requestIp.getClientIp(req);
//         console.log('=== onLimitReached === by IP = ' + ipAdress);
//     }
//   });

function logErrors(err, req, res, next) {
    errorLog('error hook from logErrors');
    console.log(err.stack);
    slogError.info('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
    next(err);
}

function clientErrorHandler(err, req, res, next) {
  errorLog('error hook from clientErrorHandler');
  errorLog('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
  console.log(err.stack);
  slogError.info('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
  if (req.xhr) {
    res.status(500).send({ error: 'Something failed!' });
  } else {
    next(err);
  }
}

function errorHandler(err, req, res, next) {
    errorLog('error hook from errorHandler');
    errorLog('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
    slogError.info('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
    if (res.headersSent) {
      return next(err);
    }
    res.status(500);
    res.render('error', { error: err });
  }

let server = new FastBootAppServer({
    distPath: DISTRIBUTIVE,
    gzip: true,
    port: 4200,
    //cache: cache,

    afterMiddleware(app) {
        app.use(bodyParser());
        app.use(methodOverride());
        //app.use(clientErrorHandler);
        app.use(errorHandler);
        //app.use(logErrors);
        // app.use((err, req, res, next) => {
        //     errorLog('error hook in afterMiddleware');
        //     console.log(err.stack);
        //     slogError.info('url = ' + req.url + ' | status = ' + res.statusCode + ' | error: ' + err.stack);
        //     next();
        // });

        app.use((request, response, next) => {
            log('hook in afterMiddleware');

            response.cookie('app1', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
            response.cookie('app2', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
            response.cookie('app3', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
            response.cookie('app4', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });

            next();
        });
    },

    beforeMiddleware(app) {
        //app.use(limiter);
        app.use((request, response, next) => {
            try {
                // errorLog(request.url + ' | response.headersSent = ' + response.headersSent);
                // if (response.headersSent) {
                //     return next(err);
                // }

                const url = request.url; // "/change"
                // Примеры URL, которые не нужно обрабатывать
                /*
                /assets/exclude/news-slider/prodat_crypto.png
                /ember/images/footer-segment/twitter.svg
                */

                // Отсеиваем запросы картинок для присовения cookie и редиректа, стилей и прочего по URL запроса
                if (url.slice(0, 6).toLowerCase() === '/ember' || url.slice(0, 7).toLowerCase() === '/assets') {
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    next();
                    return 0;
                }
                const locales = ["ar", "de", "ar", "en", "es", "fr", "it", "pt", "ru", "tr"];
                const host = request.headers.host; // "indacoin.com"
                const country = request.headers['cf-ipcountry']; // RU
                const userAgent = request.headers['user-agent']; // Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/71.0.3578.98 Safari/537.36

                // ******** IP logs ******
                //console.log(' ');
                //console.log(request.headers);
                //let ipAdress = request.headers['x-forwarded-for'];
                const ipAdress = requestIp.getClientIp(request);
                log('IP = ' + ipAdress + ' country from cludflare = ' + country + ' with status = ' + response.statusCode);

                //console.log(request.headers);
                // if (ipAdress == '41.136.44.131' || 
                //     ipAdress == '197.189.134.143' ||
                //     ipAdress == '101.227.190.229') {
                //     console.log('Block user with IP = ' + ipAdress);
                //     return 0;
                // }
                //console.log(' ');
                // ***********************

                response.cookie('test1', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
                response.cookie('test2', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
                response.cookie('test3', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
                response.cookie('test4', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });

                

                // Special mark for partners via key 'InstantCoin' in header 'User-Agent'. If true send cookie 'SpecialPartner'
                if (userAgent) {
                    if (userAgent.toLowerCase().indexOf('instantcoin') !== -1) {
                        response.cookie('SpecialPartner', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
                        log('set SpecialPartner = InstantCoin');
                    }
                }
                
                response.cookie('test5', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });
                
                //log('cookie:');
                //console.log(response.cookie);
                log('User-Agent:' + userAgent);
                //response.cookie('SpecialPartner', 'InstantCoin', { expires: new Date(Date.now() + 9000000) });

                if ('/90fxzttb9476buooa5lfd09s6ghejh.html' == request.url) {
                    response.type('text/plain');
                    response.send("90fxzttb9476buooa5lfd09s6ghejh");
                    return 0;
                }

                if ('/robots.txt' == request.url) {
                    if (host[host.length - 2] + host[host.length - 1] == 'io') {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        response.send("User-agent: *\nDisallow: /");
                        //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                        //next();
                        return 0;
                    } else {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                        response.send("User-agent: *\nhost: https://indacoin.com\nSitemap: https://indacoin.com/sitemap.xml\nCrawl-delay: 10\n# Directories\nDisallow: /api/\nDisallow: /notify/\n# Paths\nDisallow: /api/coinimage/\n# Files\n#quicktabs");
                        //next();
                        return 0;
                    }
                }

                let detectLang = false;

                // For SEO aliase indacoin.com/ = indacoin.com/en_GB
                // if (url === '/') {
                //     log('Find route: /');
                //     detectLang = 'en_GB';
                // }

                // 1. Redirect from indacoin.com/en_GB/change/buy-bitcoin-with-cardusd to indacoin.com/change
                // if (url.toLowerCase() === '/en_gb/change/buy-bitcoin-with-cardusd') {
                //     let newUrl = 'https://' + host + "/change";
                //     log('Redicrect (1) from ' + host + url + ' to ' + newUrl);
                //     response.redirect(newUrl);
                //     next();
                //     return 0;
                // }

                // 2. Redirect from tr.indacoin.com/change to indacoin.com/tr_TR/change/buy-bitcoin-with-cardusd
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/change' || url === '/change/')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country + '/change/buy-bitcoin-with-cardusd';
                    //response.cookie('lang', host.substr(0, 2) + '_' + country);
                    log('Redicrect (2) from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 3. Redirect from ru.indacoin.com/ to indacoin.com/ru_RU
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/' || url === '')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country;
                    response.cookie('lang', host.substr(0, 2) + '_' + country);
                    log('Redicrect (3) from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 4. https://tr.indacoin.com/verification -> https://indacoin.com/en_GB/help
                if (host.slice(0, 2) === 'tr' && url === '/verification') {
                    let newUrl = 'https://' + host.slice(3) + '/en_GB/help';
                    log('Redicrect (4) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 5. https://ru.indacoin.com/news/verifikacija-pri-pervoj-oplate-bankovskoj-kartoj -> https://indacoin.com/ru_RU/news
                if (host.slice(0, 2) === 'ru' && url === '/news/verifikacija-pri-pervoj-oplate-bankovskoj-kartoj') {
                    let newUrl = 'https://' + host.slice(3) + '/ru_RU/news';
                    log('Redicrect (5) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 6. Redirect from ru.indacoin.com/en_GB/news to indacoin.com/en_GB/news
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com') {
                    let newUrl = '';
                    if (url.slice(0, 1) === '/' && url.slice(3, 4) === '_')
                        newUrl = 'https://' + host.slice(3) + '/' + host.slice(0, 2) + url.slice(3);
                    else
                        newUrl = 'https://' + host.slice(3) + url;
                    log('Redicrect (6) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // Examples:
                // 7. indacoin.com/change/tr -> indacoin.com/tr_GB/change/buy-bitcoin-with-cardusd -> indacoin.com/en_GB/change              
                /*if (locales.indexOf(url.slice(8).toLowerCase()) !== -1 && url.slice(0, 7).toLowerCase() === '/change') {
                    let newUrl = 'https://' + host + '/' + url.slice(8).toLowerCase() + '_GB/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (7) from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }*/

                // 8. indacoin.com/change/ethereum -> indacoin.com/en_GB/change/buy-Ethereum-with-cardusd
                if (url.toLowerCase() === '/change/ethereum') {
                    let newUrl = 'https://' + host + '/en_GB/change/buy-ethereum-with-cardusd';
                    log('Redicrect (8) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 9. indacoin.com/news -> indacoin.com/en_GB/news
                else if (url.toLowerCase() === '/news') {
                    let newUrl = 'https://' + host + '/en_GB/news';
                    log('Redicrect (9) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 10. indacoin.com/api -> indacoin.com/en_GB/api
                // else if (url.toLowerCase() === '/api') {
                //     let newUrl = 'https://' + host + '/en_GB/api';
                //     log('Redicrect (10) from ' + host + url + ' to ' + newUrl);
                //     response.redirect(301, newUrl);
                //     return 0;
                // }

                // 10. indacoin.com/api -> indacoin.com/en_GB/api
                else if (url.toLowerCase() === '/api_doc') {
                    let newUrl = 'https://' + host + '/xx_XX/api';
                    log('Redicrect (10) from ' + host + url + ' to ' + newUrl);
                    response.redirect(301, newUrl);
                    return 0;
                }

                // 11. indacoin.com/affiliate -> indacoin.com/en_GB/affiliate
                else if (url.toLowerCase() === '/affiliate') {
                    let newUrl = 'https://' + host + '/en_GB/affiliate';
                    log('Redicrect (11) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 12. indacoin.com/help -> indacoin.com/en_GB/help
                else if (url.toLowerCase() === '/help') {
                    let newUrl = 'https://' + host + '/en_GB/help';
                    log('Redicrect (12) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 13. indacoin.com/terms-of-use -> indacoin.com/en_GB/terms-of-use
                else if (url.toLowerCase() === '/terms-of-use') {
                    let newUrl = 'https://' + host + '/en_GB/terms-of-use';
                    log('Redicrect (13) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 14-1. indacoin.com/login -> indacoin.com/en_GB/login
                else if (url.toLowerCase() === '/login') {
                    let newUrl = 'https://' + host + '/en_GB/login';
                    log('Redicrect (14-1) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 14-2. indacoin.com/login?redirect=Service.aspx?TransactOutExtConfirm=0DaDFcaa0572ee1Ed8Ec99DFbE8df1
                // -> indacoin.com/xx_XX/login?redirect=Service.aspx?TransactOutExtConfirm=0DaDFcaa0572ee1Ed8Ec99DFbE8df1

                // indacoin.com/login?redirect=Service.aspx?TransactOutExtConfirm=0DaDFcaa0572ee1Ed8Ec99DFbE8df1
                // https://indacoin.com/Service.aspx?TransactOutExtConfirm=0DaDFcaa0572ee1Ed8Ec99DFbE8df1
                if (url.slice(0, 51).toLowerCase() === '/login?redirect=service.aspx?transactoutextconfirm=') {
                    let newUrl = 'https://' + host + '/xx_XX' + url;
                    log('Redicrect (14-2) from ' + host + url + ' to ' + newUrl);
                    response.redirect(302, newUrl);
                    return 0;
                }

                // 15. indacoin.com/register -> indacoin.com/en_GB/register
                else if (url.toLowerCase() === '/register') {
                    let newUrl = 'https://' + host + '/en_GB/register';
                    log('Redicrect (15) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 16. indacoin.com/charts/* -> indacoin.com/en_GB/bitcoin
                else if (url.slice(0, 7).toLowerCase() === '/charts') {
                    let newUrl = 'https://' + host + '/en_GB/bitcoin';
                    log('Redicrect (16) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                ////////////////////////////////////
                //////////// New redirects /////////
                ////////////////////////////////////
                // 17. https://indacoin.com/change/qiwi_bitcoin -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/qiwi_bitcoin') {
                    let newUrl = 'https://' + host + '/change';
                    log('Redicrect (17) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 39. https://indacoin.com/change/ru -> https://indacoin.com/ru_RU/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/ru') {
                    let newUrl = 'https://' + host + '/ru_RU/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (39) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 40. https://indacoin.com/change/sg -> https://indacoin.com/en_SG/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/sg') {
                    let newUrl = 'https://' + host + '/en_SG/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (40) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 18. https://indacoin.com/change/gb -> https://indacoin.com/
                else if (url.toLowerCase() === '/change/gb') {
                    let newUrl = 'https://' + host;
                    log('Redicrect (18) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 19. https://indacoin.com/change/cn -> https://indacoin.com/en_CN/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/cn') {
                    let newUrl = 'https://' + host + '/en_CN/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (19) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 20. https://indacoin.com/change/sy -> https://indacoin.com/en_SY/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/sy') {
                    let newUrl = 'https://' + host + '/en_SY/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (20) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 21. https://indacoin.com/change/nl -> https://indacoin.com/en_NL/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/nl') {
                    let newUrl = 'https://' + host + '/en_NL/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (21) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 22. https://indacoin.com/change/id -> https://indacoin.com/en_ID/change/buy-bitcoin-with-cardusd
                else if (url.toLowerCase() === '/change/id') {
                    let newUrl = 'https://' + host + '/en_ID/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (22) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 23. https://indacoin.com/change/ch -> https://indacoin.com/en_CH
                else if (url.toLowerCase() === '/change/ch') {
                    let newUrl = 'https://' + host + '/en_CH';
                    log('Redicrect (23) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 24. https://indacoin.com/change/ca -> https://indacoin.com/en_CA
                else if (url.toLowerCase() === '/change/ca') {
                    let newUrl = 'https://' + host + '/en_CA';
                    log('Redicrect (24) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 25. https://indacoin.com/change/il -> https://indacoin.com/en_IL
                else if (url.toLowerCase() === '/change/il') {
                    let newUrl = 'https://' + host + '/en_IL';
                    log('Redicrect (25) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 26. https://indacoin.com/change/in -> https://indacoin.com/en_IN
                else if (url.toLowerCase() === '/change/in') {
                    let newUrl = 'https://' + host + '/en_IN';
                    log('Redicrect (26) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 27. https://indacoin.com/change/hu -> https://indacoin.com/en_HU
                else if (url.toLowerCase() === '/change/hu') {
                    let newUrl = 'https://' + host + '/en_HU';
                    log('Redicrect (27) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 28. https://indacoin.com/change/by -> https://indacoin.com/ru_BY
                else if (url.toLowerCase() === '/change/by') {
                    let newUrl = 'https://' + host + '/ru_BY';
                    log('Redicrect (28) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 29. https://indacoin.com/change/us -> https://indacoin.com/en_US
                else if (url.toLowerCase() === '/change/us') {
                    let newUrl = 'https://' + host + '/en_US';
                    log('Redicrect (29) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 30. https://indacoin.com/change/buy-xrp-with-usd -> https://indacoin.com/en_GB/change/buy-ripple-with-cardusd
                else if (url.toLowerCase() === '/change/buy-xrp-with-usd') {
                    let newUrl = 'https://' + host + '/en_GB/change/buy-ripple-with-cardusd';
                    log('Redicrect (30) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 31. https://indacoin.com/change/buy-ethereum-with-cardeur/US-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-ethereum-with-cardeur/us-en') {
                    let newUrl = 'https://' + host + '/change';
                    log('Redicrect (31) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 32. https://indacoin.com/change/buy-bitcoin-with-cardeur/eg-ar -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/eg-ar') {
                    let newUrl = 'https://' + host + '/change';
                    log('Redicrect (32) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 33. https://indacoin.com/change/buy-bitcoin-with-cardeur/ua-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/ua-en') {
                    let newUrl = 'https://' + host + '/change';
                    log('Redicrect (33) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 34. https://indacoin.com/change/buy-bitcoin-with-cardeur/ua-en -> https://indacoin.com/change
                else if (url.toLowerCase() === '/change/buy-bitcoin-with-cardeur/ua-en') {
                    let newUrl = 'https://' + host + '/change';
                    log('Redicrect (34) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 35. https://tr.indacoin.com/trade -> https://indacoin.com/change
                if (host.slice(0, 2) === 'tr' && url === '/trade') {
                    let newUrl = 'https://' + host.slice(3) + '/change';
                    log('Redicrect (35) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 36. https://tr.indacoin.com/rates -> https://indacoin.com/en_GB/help
                if (host.slice(0, 2) === 'tr' && url === '/rates') {
                    let newUrl = 'https://' + host.slice(3) + '/en_GB/help';
                    log('Redicrect (36) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // ========================================
                // ========================================
                // ========================================

                // 37. https://indacoin.com/change/sg -> https://indacoin.com/en_SG/change/buy-bitcoin-with-cardusd
                if (url.toLowerCase() === '/change/sg') {
                    let newUrl = 'https://' + host + '/en_SG/change/buy-bitcoin-with-cardusd';
                    log('Redicrect (37) from ' + host + url + ' to ' + newUrl);
                    //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                    response.redirect(301, newUrl);
                    //next();
                    return 0;
                }

                // 2) Смотрим в cookie: 'lang: ru_RU'
                var haveLangCookie = false;
                //if (detectLang === false) {
                    let cookie = request.headers['cookie'];
                    if (cookie !== undefined) {
                        cookie.split(' ').forEach(function(item) {
                            if (item.split('=')[0] === 'lang') {
                                let lang = item.split('=')[1].replace(';', '');
                                if (lang[2] === '_' && lang.length === 5) {
                                    detectLang = item.split('=')[1].replace(';', '');
                                    haveLangCookie = true;
                                    //console.log('Find route from cookie: /' + detectLang);
                                }
                                //else
                                    //console.log('Bad route from cookie: /' + detectLang);
                            }
                        });
                    }
                //}

                if (detectLang === false) {

                    const regions = [
                        "AD", "AE", "AF", "AG", "AG", "AI", "AL", "AM", "AN", "AO", "AQ", "AR", "AS", "AT", "AU", "AW", "AX", "AZ", "BA", "BB", "BD", "BE", "BF", "BG", "BH", "BI",
                        "BJ", "BM", "BN", "BO", "BR", "BS", "BT", "BV", "BW", "BY", "BZ", "CA", "CC", "CD", "CF", "CG", "CH", "CI", "CK", "CL", "CM", "CN", "CO", "CR", "CS", "CU",
                        "CV", "CX", "CY", "CZ", "DE", "DJ", "DK", "DM", "DO", "DZ", "EC", "EE", "EG", "EH", "ER", "ES", "ET", "FI", "FJ", "FK", "FM", "FO", "FR", "GA", "GB", "GD",
                        "GE", "GF", "GG", "GH", "GI", "GL", "GM", "GN", "GP", "GQ", "GR", "GS", "GT", "GU", "GW", "GY", "HK", "HM", "HN", "HR", "HT", "HU", "ID", "IE", "IL", "IM",
                        "IN", "IO", "IQ", "IS", "IT", "JE", "JM", "JO", "KE", "KG", "KH", "KI", "KM", "KN", "KR", "KW", "KY", "KZ", "LA", "LB", "LC", "LI", "LK",
                        "LR", "LS", "LT", "LU", "LV", "LY", "MA", "MC", "MD", "MG", "MH", "MK", "ML", "MM", "MN", "MO", "MP", "MQ", "MR", "MS", "MT", "MU", "MV", "MW", "MX", "MY",
                        "MZ", "NA", "NC", "NE", "NF", "NG", "NI", "NL", "NO", "NP", "NR", "NU", "NZ", "OM", "PA", "PE", "PF", "PG", "PH", "PK", "PL", "PM", "PN", "PR", "PS", "PT",
                        "PW", "PY", "QA", "RE", "RO", "RU", "RW", "SA", "SB", "SC", "SD", "SE", "SG", "SH", "SI", "SJ", "SK", "SL", "SM", "SN", "SO", "SR", "ST", "SV", "SY", "SZ",
                        "TC", "TD", "TF", "TG", "TH", "TJ", "TK", "TL", "TM", "TN", "TO", "TR", "TT", "TV", "TW", "TZ", "UA", "UG", "UM", "UY", "UZ", "VA", "VC", "VE", "VG",
                        "VI", "VN", "VU", "WF", "WS", "YE", "YT", "ZA", "ZM", "ZW" ];
                    let region = 'GB';
                    if (regions.indexOf(country) !== -1)
                        region = country;

                    
                    let lang = 'en';
                    const acceptLanguage = request.headers['accept-language']; // 'en-GB,en;q=0.8'
                    //console.log('DEBUG: acceptLanguage = ' + acceptLanguage);
                    if (acceptLanguage !== undefined) {
                        let language = parser.parse(acceptLanguage)[0].code;
                        //console.log('DEBUG: language = ' + language);
                        if (locales.indexOf(language) !== -1)
                            lang = language;
                    }

                    detectLang = lang + '_' + region;
                    //console.log('DEBUG: detectLang = ' + detectLang);
                }

                if (url.substr(3, 1) === "_" && (url.substr(1, 2).toLowerCase() == "xx" || url.substr(4, 2).toLowerCase() == "xx" )) {
                    let newUrl = url.slice(0, 1) + detectLang + url.slice(6);
                    log('Redicrect (38) from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    //next();
                    return 0;
                }

                if (!haveLangCookie)
                    response.cookie('lang', detectLang);

                let refferer = request.headers['referer']; //this.get('fastboot.request.headers.referer');
                if (refferer)
                    response.cookie('referrer2', refferer);

                // if (url.substr(1, 5).toLowerCase() === "xx_xx") {
                //     let newUrl = url.slice(0, 1) + detectLang + url.slice(6);
                //     log('Redicrect (38) from ' + host + url + ' to ' + newUrl);
                //     //response.append('order-waiting', 'k7dq');
                //     response.redirect(newUrl);
                //     next();
                //     return 0;
                // }
                //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                next();
                return 0;
            } catch (e) {
                log('Ошибка: ' + e.message);
                //slog.info('url = ' + url + ' | status = ' + response.statusCode);
                //slog.info('Ошибка: ' + e.message);
                next();
                return 0;
            }

        });

        // app.use((err, req, res, next) => {
        //     errorLog('error hook beforeMiddleware');
        //     console.log(err);

        //     slogError.info('url = ' + req.url + ' | status_beforeMiddleware = ' + res.statusCode);
        //     slogError.info(' ');
        //     slogError.info(err);
        //     slogError.info('========================================');

        //     next();
        // }),

        // app.use(errorHandler);
        // function errorHandler(err, req, res, next) {

        //     errorLog('error hook errorHandler');
        //     console.log(err);

        //     slogError.info('url = ' + req.url + ' | status_errorHandler = ' + res.statusCode);
        //     slogError.info(' ');
        //     slogError.info(err);
        //     slogError.info('========================================');

        //     if (res.headersSent) {
        //       return next(err);
        //     }
        //     next();
        //     return 0;
        //   }

    }
});
server.start();
log('Start server');
