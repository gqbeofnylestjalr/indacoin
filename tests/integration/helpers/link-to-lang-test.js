
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('link-to-lang', 'helper:link-to-lang', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{link-to-lang inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

