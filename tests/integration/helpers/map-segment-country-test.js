
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('map-segment-country', 'helper:map-segment-country', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{map-segment-country inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

