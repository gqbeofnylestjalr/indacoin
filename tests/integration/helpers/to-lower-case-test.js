
import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('to-lower-case', 'helper:to-lower-case', {
  integration: true
});

// Replace this with your real tests.
test('it renders', function(assert) {
  this.set('inputValue', '1234');

  this.render(hbs`{{to-lower-case inputValue}}`);

  assert.equal(this.$().text().trim(), '1234');
});

