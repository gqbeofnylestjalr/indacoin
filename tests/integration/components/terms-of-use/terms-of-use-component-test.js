import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('terms-of-use/terms-of-use-component', 'Integration | Component | terms of use/terms of use component', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{terms-of-use/terms-of-use-component}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#terms-of-use/terms-of-use-component}}
      template block text
    {{/terms-of-use/terms-of-use-component}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
