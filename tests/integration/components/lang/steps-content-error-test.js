import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('lang/steps-content-error', 'Integration | Component | lang/steps content error', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{lang/steps-content-error}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#lang/steps-content-error}}
      template block text
    {{/lang/steps-content-error}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
