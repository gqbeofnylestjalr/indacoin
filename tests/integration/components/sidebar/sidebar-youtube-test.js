import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('sidebar/sidebar-youtube', 'Integration | Component | sidebar/sidebar youtube', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{sidebar/sidebar-youtube}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#sidebar/sidebar-youtube}}
      template block text
    {{/sidebar/sidebar-youtube}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
