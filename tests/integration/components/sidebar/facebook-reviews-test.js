import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('sidebar/facebook-reviews', 'Integration | Component | sidebar/facebook reviews', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{sidebar/facebook-reviews}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#sidebar/facebook-reviews}}
      template block text
    {{/sidebar/facebook-reviews}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
