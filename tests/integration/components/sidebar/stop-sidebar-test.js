import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('sidebar/stop-sidebar', 'Integration | Component | sidebar/stop sidebar', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{sidebar/stop-sidebar}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#sidebar/stop-sidebar}}
      template block text
    {{/sidebar/stop-sidebar}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
