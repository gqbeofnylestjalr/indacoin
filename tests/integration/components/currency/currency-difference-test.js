import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('currency/currency-difference', 'Integration | Component | currency/currency difference', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{currency/currency-difference}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#currency/currency-difference}}
      template block text
    {{/currency/currency-difference}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
