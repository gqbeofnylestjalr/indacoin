import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('currency/ethereum/currency-presented', 'Integration | Component | currency/ethereum/currency presented', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{currency/ethereum/currency-presented}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#currency/ethereum/currency-presented}}
      template block text
    {{/currency/ethereum/currency-presented}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
