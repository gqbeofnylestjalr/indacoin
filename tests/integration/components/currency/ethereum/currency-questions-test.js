import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('currency/ethereum/currency-questions', 'Integration | Component | currency/ethereum/currency questions', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{currency/ethereum/currency-questions}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#currency/ethereum/currency-questions}}
      template block text
    {{/currency/ethereum/currency-questions}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
