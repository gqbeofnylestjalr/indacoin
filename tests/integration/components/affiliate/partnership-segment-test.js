import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('affiliate/partnership-segment', 'Integration | Component | affiliate/partnership segment', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{affiliate/partnership-segment}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#affiliate/partnership-segment}}
      template block text
    {{/affiliate/partnership-segment}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
