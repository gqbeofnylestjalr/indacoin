import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('affiliate/clients-segment', 'Integration | Component | affiliate/clients segment', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{affiliate/clients-segment}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#affiliate/clients-segment}}
      template block text
    {{/affiliate/clients-segment}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
