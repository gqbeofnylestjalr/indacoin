import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('affiliate/services-segment', 'Integration | Component | affiliate/services segment', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{affiliate/services-segment}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#affiliate/services-segment}}
      template block text
    {{/affiliate/services-segment}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
