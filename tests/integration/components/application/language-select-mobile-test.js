import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('application/language-select-mobile', 'Integration | Component | application/language select mobile', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{application/language-select-mobile}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#application/language-select-mobile}}
      template block text
    {{/application/language-select-mobile}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
