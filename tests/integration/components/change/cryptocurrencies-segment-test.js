import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('change/cryptocurrencies-segment', 'Integration | Component | change/cryptocurrencies segment', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{change/cryptocurrencies-segment}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#change/cryptocurrencies-segment}}
      template block text
    {{/change/cryptocurrencies-segment}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
