import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('change/steps-content-error', 'Integration | Component | change/steps content error', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{change/steps-content-error}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#change/steps-content-error}}
      template block text
    {{/change/steps-content-error}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
