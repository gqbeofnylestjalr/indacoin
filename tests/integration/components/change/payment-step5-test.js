import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('change/payment-step5', 'Integration | Component | change/payment step5', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{change/payment-step5}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#change/payment-step5}}
      template block text
    {{/change/payment-step5}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
