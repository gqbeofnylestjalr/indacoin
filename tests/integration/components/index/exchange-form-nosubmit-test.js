import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('index/exchange-form-nosubmit', 'Integration | Component | index/exchange form nosubmit', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{index/exchange-form-nosubmit}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#index/exchange-form-nosubmit}}
      template block text
    {{/index/exchange-form-nosubmit}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
