import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('index/currency-featured', 'Integration | Component | index/currency featured', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{index/currency-featured}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#index/currency-featured}}
      template block text
    {{/index/currency-featured}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
