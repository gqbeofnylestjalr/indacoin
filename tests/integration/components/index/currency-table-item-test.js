import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('index/currency-table-item', 'Integration | Component | index/currency table item', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{index/currency-table-item}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#index/currency-table-item}}
      template block text
    {{/index/currency-table-item}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
