import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('index/mobile-mockups', 'Integration | Component | index/mobile mockups', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{index/mobile-mockups}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#index/mobile-mockups}}
      template block text
    {{/index/mobile-mockups}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
