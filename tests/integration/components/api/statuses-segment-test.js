import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('api/statuses-segment', 'Integration | Component | api/statuses segment', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{api/statuses-segment}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#api/statuses-segment}}
      template block text
    {{/api/statuses-segment}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
