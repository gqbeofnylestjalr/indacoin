import { moduleForComponent, test } from 'ember-qunit';
import hbs from 'htmlbars-inline-precompile';

moduleForComponent('misc/remove-header', 'Integration | Component | misc/remove header', {
  integration: true
});

test('it renders', function(assert) {

  // Set any properties with this.set('myProperty', 'value');
  // Handle any actions with this.on('myAction', function(val) { ... });

  this.render(hbs`{{misc/remove-header}}`);

  assert.equal(this.$().text().trim(), '');

  // Template block usage:
  this.render(hbs`
    {{#misc/remove-header}}
      template block text
    {{/misc/remove-header}}
  `);

  assert.equal(this.$().text().trim(), 'template block text');
});
