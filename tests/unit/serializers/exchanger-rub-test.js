import { moduleForModel, test } from 'ember-qunit';

moduleForModel('exchanger-rub', 'Unit | Serializer | exchanger rub', {
  // Specify the other units that are required for this test.
  needs: ['serializer:exchanger-rub']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
