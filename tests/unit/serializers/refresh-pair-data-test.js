import { moduleForModel, test } from 'ember-qunit';

moduleForModel('refresh-pair-data', 'Unit | Serializer | refresh pair data', {
  // Specify the other units that are required for this test.
  needs: ['serializer:refresh-pair-data']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
