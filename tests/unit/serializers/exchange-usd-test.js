import { moduleForModel, test } from 'ember-qunit';

moduleForModel('exchange-usd', 'Unit | Serializer | exchange usd', {
  // Specify the other units that are required for this test.
  needs: ['serializer:exchange-usd']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
