import { moduleForModel, test } from 'ember-qunit';

moduleForModel('news2', 'Unit | Serializer | news2', {
  // Specify the other units that are required for this test.
  needs: ['serializer:news2']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
