import { moduleForModel, test } from 'ember-qunit';

moduleForModel('exchanger-eur', 'Unit | Serializer | exchanger eur', {
  // Specify the other units that are required for this test.
  needs: ['serializer:exchanger-eur']
});

// Replace this with your real tests.
test('it serializes records', function(assert) {
  let record = this.subject();

  let serializedRecord = record.serialize();

  assert.ok(serializedRecord);
});
