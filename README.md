# Indacoin

![IMAGE](./doc/promo.gif)

Indaexchange is written using Ember.js, Chart.js, Ember Fastboot, Semantic UI & Sass.

### Features

- Buying bitcoins and more than 100 cryptocurrencies with Visa & Mastercard
- Partner service integration
- Cryptocurrency market rankings, charts
- User authorization
- Localization, online support system

## Prerequisites

You will need the following things properly installed on your computer:

* [Git](https://git-scm.com/)
* [Node.js](https://nodejs.org/)
* [Ember CLI](https://ember-cli.com/)
* [PM2](https://pm2.keymetrics.io/)
* [Redis](https://redis.io/)
* [MongoDB](https://www.mongodb.com/)

## Installation

* `git clone <repository-url>`
* `cd indacoin/`
* `npm install`

## Running

### Development

* from the project folder `ember serve`
* visit your app at [http://localhost:4200](http://localhost:4200)

### Building

* from the project folder `npm run build`

### Production

Frontend server is serviced on Node.js / Express.js that is remotely located on the Debian. After building, we get the assembled project folder "dist/". For daemonize Node.js server we use PM2:

* from the project folder `pm2 start server.js -i max --name "indacoin"`
* stop/start/restart/delete/info `pm2 <stop/start/restart/delete/show> indacoin`

## Further Reading / Useful Links

* [Ember.js](http://emberjs.com/)
* [Semantic UI](https://semantic-ui.com/)
* [Chart.js](https://www.chartjs.org/)
* [Ember Fastboot](https://www.ember-fastboot.com/)
* [PM2](https://pm2.keymetrics.io/)
* [Ember Inspector for chrome](https://chrome.google.com/webstore/detail/ember-inspector/bmdblncegkenkacieihfhpjfppoconhi)