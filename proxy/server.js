'use strict';

const server = require('express')();
const proxy  = require('./lib/proxy');
const bodyParser = require('body-parser');

server.use(bodyParser.json());       // to support JSON-encoded bodies
server.use(bodyParser.urlencoded({     // to support URL-encoded bodies
  extended: true
}));

// travis proxy
/*server.use(proxy('api.travis-ci.org', {
  prefix  : '/travis',
  request : {
    headers         : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
      'Accept'     : 'application/vnd.travis-ci.2+json'
    }
  }
}));

// github proxy
server.use(proxy('api.github.com', {
  prefix  : '/github',
  request : {
    forceHttps : true,
    headers    : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
  }
}));

server.use(proxy('i.imgur.com', {
  prefix  : '/imgur',
  log  : true,
  request : {
    headers    : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
  }
}));

server.use(proxy('indacoin.com', { // прокси сюда
  prefix  : '/first', // по этому адресу
  log  : true,
  request : {
    headers    : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
  }
}));*/

/*server.use(proxy('localhost/api:4500', {
  prefix  : '/api',
  log  : true,
  request : {
    headers    : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
  }
}));*/

server.use(proxy('localhost:4500/api/mobgetcurrencies', { // прокси сюда
  prefix  : '/api3/mobgetcurrencies/', // по этому адресу
  log  : true,
  request : {
    headers    : {
      'User-Agent' : 'Mozilla/5.0 (X11; Linux i686 (x86_64)) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.71 Safari/537.36',
    }
  }
}));

// simple server health check
server.get('/status', function (req, res) {
  res.send('OK');
});

// return the app for all other routes
/*server.get('*', function (req, res) {
  res.set({ 'Content-Type': 'text/html; charset=utf-8' });
  res.sendFile(__dirname + '/app.html');
});*/

let port = 4201;
console.log('\nserver listening on port: ' + port + '\n');
server.listen(port);
