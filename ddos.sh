#!/bin/bash
for i in {1..500}
do
  echo "$(date) wait: $(curl -w '%{time_total} %{http_code}\n' -s -o /dev/null -H 'Host: indacoin.com' -H 'X-Forwarded-For: 1.2.3.$(($i%256))' -L indacoin.com)" >> log.txt & echo $i &
done

wait