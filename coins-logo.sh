#!/bin/bash
# Build with imagemagick
echo "Convert delete white background"
for file in `find /home/michael/Public/source/ -type f -name "*.png"`
do
	fileName=`basename $file .png`;
	convert source/${fileName}.png -transparent white white/${fileName}.png
done
echo "Blue fill images"
for file in `find /home/michael/Public/white/ -type f -name "*.png"`
do
	fileName=`basename $file .png`;
	convert white/${fileName}.png -fuzz 90% -fill '#64aecc' -opaque black blue/${fileName}.png
done
echo "Convert images size"
for file in `find /home/michael/Public/blue/ -type f -name "*.png"`
do
	fileName=`basename $file .png`;
	convert blue/${fileName}.png -resize 20x20 result/${fileName}.png;
done