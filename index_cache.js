/*const request = require("request");
const url =
  "https://maps.googleapis.com/maps/api/geocode/json?address=Florence";
request.get(url, (error, response, body) => {
  let json = JSON.parse(body);
  console.log(
    `City: ${json.results[0].formatted_address} -`,
    `Latitude: ${json.results[0].geometry.location.lat} -`,
    `Longitude: ${json.results[0].geometry.location.lng}`
  );
});*/

var fs = require('fs');



function getFile() {
    var contents = fs.readFileSync('DATA', 'utf8');
    console.log('contents');
    console.log(contents);
    return contents;
}


/*const SimpleNodeLogger = require('simple-node-logger'),
opts = {
    logFilePath:'history.log',
    timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
},
slog = SimpleNodeLogger.createSimpleLogger( opts );

var colors = require('colors');
var setCookie = require('set-cookie');*/
var colors = require('colors');
const FastBootAppServer = require('fastboot-app-server');

function log(message) {
    console.log(colors.green(message));
}

var me = this;
var test = "Test variable";
//const MY_GLOBAL = 'Stack_kek';
var MY_GLOBAL = 3;

var currency = function() {
    //return MY_GLOBAL++;
    return new Date();
}

/*class User {
    constructor(name) {
    this.name = name;
    }
    inc() {
        return this.name;
    }
}*/
    
//var user = new User(3);
//user.inc();

//var count = 0;
//var countServer = 0;

let server = new FastBootAppServer({
    distPath: 'dist',
    gzip: true,
    port: 4200,
    sandboxGlobals: {
        GLOBAL_VALUE: MY_GLOBAL,
        //currency: currency()
        currency: getFile()
    },
    beforeMiddleware(app) {
        app.use((request, response, next) => {
            try {
                const locales = ["ar", "de", "ar", "en", "es", "fr", "it", "pt", "ru", "tr"];
                const host = request.headers.host;
                const url = request.url; // indacoin.com
                let country = request.headers['cf-ipcountry'];

                if ('/robots.txt' == request.url) {
                    if (host[host.length - 2] + host[host.length - 1] == 'io') {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        response.send("User-agent: *\nDisallow: /");
                        return next();
                    } else {
                        log('url = ' + request.url);
                        response.type('text/plain');
                        response.send("User-agent: *\nCrawl-delay: 10");
                        return next();
                    }
                    log('Не должно выполниться');
                }

                let detectLang = false;

                // For SEO aliase indacoin.com/ = indacoin.com/en_GB
                if (url === '/') {
                    console.log('Find route: /');
                    detectLang = 'en_GB';
                }

                // Redirect from indacoin.com/en_GB/change/buy-bitcoin-with-cardusd to indacoin.com/change
                /*if (url.toLowerCase() === '/404') {
                    response.render('404', { url: url });
                    next();
                    return 0;
                    //return;
                    //next();
                    //return 0;
                }*/

                // Redirect from indacoin.com/en_GB/change/buy-bitcoin-with-cardusd to indacoin.com/change
                if (url.toLowerCase() === '/en_gb/change/buy-bitcoin-with-cardusd') {
                    let newUrl = 'https://' + host + "/change";
                    log('Redicrect from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from tr.indacoin.com/change to indacoin.com/tr_TR/change/buy-bitcoin-with-cardusd
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/change' || url === '/change/')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country + '/change/buy-bitcoin-with-cardusd';
                    response.cookie('lang', host.substr(0, 2) + '_' + country);
                    log('Redicrect from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from ru.indacoin.com/ to indacoin.com/ru_RU
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com' && (url === '/' || url === '')) {
                    let matchingCountry = { tr: 'TR', es: 'ES', ru: 'RU', ar: 'AE' };
                    let country = matchingCountry[host.substr(0, 2).toLowerCase()] || 'GB';
                    let newUrl = 'https://' + host.slice(3) + '/' + host.substr(0, 2) + '_' + country;
                    response.cookie('lang', host.substr(0, 2) + '_' + country);
                    log('Redicrect from ' + host + url + ' to ' + newUrl + ' with set cookie: ' + host.substr(0, 2) + '_' + country);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Redirect from ru.indacoin.com/en_GB/news to indacoin.com/en_GB/news
                // https://ru.indacoin.com/en_GB/affiliate -> https://ru.indacoin.com/ru_GB/affiliate
                if (locales.indexOf(host.substr(0, 2)) !== -1 && host.substr(2, 13) === '.indacoin.com') {
                    let newUrl = '';
                    if (url.slice(0, 1) === '/' && url.slice(3, 4) === '_')
                        newUrl = 'https://' + host.slice(3) + '/' + host.slice(0, 2) + url.slice(3);
                    else
                        newUrl = 'https://' + host.slice(3) + url;
                    log('Redicrect from ' + host + url + ' to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // Examples:
                // indacoin.com/change/tr -> indacoin.com/tr_GB/change/buy-bitcoin-with-cardusd -> indacoin.com/en_GB/change

                // indacoin.com/change/tr -> indacoin.com/tr_TR/change/buy-bitcoin-with-cardusd
                // indacoin.com/change/es -> indacoin.com/es_ES/change/buy-bitcoin-with-cardusd
                if (locales.indexOf(url.slice(8).toLowerCase()) !== -1 && url.slice(0, 7).toLowerCase() === '/change') {
                    let newUrl = 'https://' + host + '/' + url.slice(8).toLowerCase() + '_GB/change/buy-bitcoin-with-cardusd';
                    log('Redicrect from to ' + newUrl);
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 1) indacoin.com/change/ethereum -> indacoin.com/en_GB/change/buy-Ethereum-with-cardusd
                if (url.toLowerCase() === '/change/ethereum') {
                    let newUrl = 'https://indacoin.com/en_GB/change/buy-ethereum-with-cardusd';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 2) indacoin.com/news -> indacoin.com/en_GB/news
                else if (url.toLowerCase() === '/news') {
                    let newUrl = 'https://indacoin.com/en_GB/news';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 3) indacoin.com/api -> indacoin.com/en_GB/api
                else if (url.toLowerCase() === '/api') {
                    let newUrl = 'https://indacoin.com/en_GB/api';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 4) indacoin.com/affiliate -> indacoin.com/en_GB/affiliate
                else if (url.toLowerCase() === '/affiliate') {
                    let newUrl = 'https://indacoin.com/en_GB/affiliate';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 5) indacoin.com/help -> indacoin.com/en_GB/help
                else if (url.toLowerCase() === '/help') {
                    let newUrl = 'https://indacoin.com/en_GB/help';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 6) indacoin.com/terms-of-use -> indacoin.com/en_GB/terms-of-use
                else if (url.toLowerCase() === '/terms-of-use') {
                    let newUrl = 'https://indacoin.com/en_GB/terms-of-use';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 8) indacoin.com/login -> indacoin.com/en_GB/login
                else if (url.toLowerCase() === '/login') {
                    let newUrl = 'https://indacoin.com/en_GB/login';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 9) indacoin.com/register -> indacoin.com/en_GB/register
                else if (url.toLowerCase() === '/register') {
                    let newUrl = 'https://indacoin.com/en_GB/register';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }

                // 10) indacoin.com/charts/* -> indacoin.com/en_GB/bitcoin
                else if (url.slice(0, 7).toLowerCase() === '/charts') {
                    let newUrl = 'https://indacoin.com/en_GB/Bitcoin';
                    response.redirect(newUrl);
                    next();
                    return 0;
                }


                // TODO: Error: Can't set headers after they are sent
                // Отсеиваем запросы картинок для присовения cookie и редиректа, стилей и прочего по URL запроса
                //log('Request = ' + host + url);
                if (detectLang === false)
                    if (url !== '/')
                        if (url.substr(1, 5).toLowerCase() !== "xx_xx") {
                            next();
                            return 0;
                        }
                
                log('3');

                // 2) Смотрим в cookie: 'lang: ru_RU'
                if (detectLang === false) {
                    let cookie = request.headers['cookie'];
                    if (cookie !== undefined) {
                        cookie.split(' ').forEach(function(item) {
                            if (item.split('=')[0] === 'lang') {
                                detectLang = item.split('=')[1].replace(';', '');
                                console.log('Find route from cookie: /' + detectLang);
                            }
                        });
                    }
                }

                if (detectLang === false) {
                    let region = 'GB';
                    let lang = 'en';
                    detectLang = lang + '_' + region;
                }

                var ip = request.headers['x-real-ip'];
                if (ip == undefined)
                    ip = request.headers['cf-connecting-ip'];
                response.cookie('lang', detectLang);
                if (url.substr(1, 5).toLowerCase() === "xx_xx") {
                    let newUrl = url.slice(0, 1) + detectLang + url.slice(6);
                    response.redirect(newUrl);
                }

                // Caching request methods
                /*count++;
                console.log('Caching request: ' + count);
                response.setHeader("Count", count);*/

                return next();
                return 0;
            } catch (e) {
                console.log('Ошибка: ' + e.message);
                return next();
            }

        });
    }
});
server.start();

//console.log(server);

var stdin = process.openStdin();
stdin.addListener("data", function(data) {
    let input = data.toString().trim().toLowerCase();

    //console.log("you entered: [" + input + "]");

    if (input.slice(0, 1) === 's' && input.length > 2) {
        console.log('command:');
        console.log(server[input.split(' ')[1]]);
    } else {
        switch(input) {
            case 'info':
                console.log('info:');
                console.log(server);
                break
            case 'this':
                console.log('this:');
                console.log(this);
                break
            case 'this2':
                console.log('this2:');
                console.log(me);
                break
            case 'this3':
                console.log('this3:');
                for (key in me) {console.log(key + ' ' + me[key]);}
                break
            case 'this4':
                console.log('this4:');
                console.log(test);
                break
            case 'dist':
                console.log('dist:');
                console.log(server.distPath);
                break
            case 'c':
                console.log('count: ' + count);
                break
            case 'g':
                MY_GLOBAL++;
                console.log('MY_GLOBAL: ' + MY_GLOBAL);
                break
            default:
                console.log('Unknown comand: ' + input);
        }
    }

  });

log('Start server');
