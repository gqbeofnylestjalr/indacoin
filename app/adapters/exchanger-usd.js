import Ember from 'ember';
import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import CachedShoe   from 'ember-cached-shoe'

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  findAll(store, type, id) {

    /*let path = "";
    if (ENV.environment === 'development') {
      path = "https://indacoin.com/api/GetExchangerAmount/USD/100";
    } else if (ENV.environment === 'production') {
      path = "/api/GetExchangerAmount/USD/100";
    } else if (ENV.environment === 'debug') {
      path = "https://indacoin.com/api/GetExchangerAmount/USD/100";
    }*/

    const host = getHost();

    //let path = "https://indacoin.com/api2/GetExchangerAmount/USD/100";
    let path = host + "/api/GetExchangerAmount/USD/100";

    //console.log('ENV.environment = ' + ENV.environment);
    //console.log('path = ' + path);

    //return fetch("https://indacoin.com/api/GetExchangerAmount/USD/100").then((data) => {
      return fetch(path).then((data) => {
      return data.json();
    }).then((data) => {
      const i18n = this.get('i18n');
      const share = this.get('share');
      let region = share.getRegion(); // US
      if (region === null)
        region = "US";
      return {
        "data":
        [{
          "id": "1",
          "type": "exchanger-usd",
          "attributes": {
            "price": accounting.formatMoney(data * 0.3, "", 4, ".", ","),
            //"url": "https://indacoin.com/change/buy-bitcoin-with-cardusd/"+ i18n.locale +"?amount_pay=30",
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=30",
            "cur": "USD",
            "sum": "30,00"
          }
        },
        {
          "id": "2",
          "type": "exchanger-usd",
          "attributes": {
            "price": accounting.formatMoney(data, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=100",
            "cur": "USD",
            "sum": "100,00"
          },
        },
        {
          "id": "3",
          "type": "exchanger-usd",
          "attributes": {
            "price": accounting.formatMoney(data * 2.5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=250",
            "cur": "USD",
            "sum": "250,00"
          },
        },
        {
          "id": "4",
          "type": "exchanger-usd",
          "attributes": {
            "price": accounting.formatMoney(data * 5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=500",
            "cur": "USD",
            "sum": "500,00"
          }
        }]
      }
    });
  }
});
