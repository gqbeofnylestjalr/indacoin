import DS from 'ember-data';
import ENV from 'fastboot/config/environment';
import CachedShoe from 'ember-cached-shoe';


function getHost() {
    let host = '';

     // Execute on client
    if (typeof FastBoot === 'undefined') {
      host = 'https://indacoin.com';
    } else {

      if (ENV.environment === 'production') {
        host = 'http://localhost:4500';
      }
      
      if (ENV.environment === 'development') {
        host = 'https://indacoin.com';
      }
      
      if (ENV.environment === 'test') {
        host = 'https://indacoin.com';
      }

    }
    return host;
}

export default DS.JSONAPIAdapter.extend(CachedShoe, {
  /*shouldReloadAll: function(store, snapshot) {
      return true;
  },

  shouldReloadRecord: function(store, snapshot) {
      return true;
    },

  shouldReloadAll: function(store, snapshot) {
    return false;
  },

  shouldBackgroundReloadRecord: function(store, snapshot) {
    return false;
  },*/

  // Модель вообще не перезагружается при смене языка
  shouldBackgroundReloadAll: function(store, snapshot) {
    return false;
  },


  

//export default DS.JSONAPIAdapter.extend({

  // ()=> {
  //   let host = '';
  //   if (typeof FastBoot === 'undefined') {
  //     host = 'https://indacoin.com'
  //   } else {
  //     host = 'http://localhost:4500';
  //   }
  //   return host;
  // }
  host: getHost(),
  //host: 'https://indacoin.com',
  //host: 'http://localhost:4500',
  // host: function() {
  //   var res = "";
  //   if (ENV.environment === 'production') {
  //     res = 'https://indacoin.com';
  //   }

  //   if (ENV.environment === 'development') {
  //     res = 'http://localhost:4500';
  //   }

  //   if (ENV.environment === 'test') {
  //     res = 'https://indacoin.com';
  //   }
  //   console.log('Value adapter return host = ' + res);
  //   return res;
  // },

  //namespace: 'api2',
  //namespace: 'api',
  namespace: 'api/uni',

  pathForType(){
    //return 'mobgetcurrencies/';
    //return 'mobgetcurrenciesinfoi/1';
    return 'mobgetcurrenciesinfoi/1';
  }
});
