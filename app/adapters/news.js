import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import Ember from 'ember';
import CachedShoe from 'ember-cached-shoe';

function getHost() {
    if (typeof FastBoot === 'undefined')
      return 'https://indacoin.com';
    else
      return 'http://localhost:4500';
  }

export default DS.RESTAdapter.extend(CachedShoe, {

    /*shouldReloadAll: function(store, snapshot) {
        return true;
    },

    shouldReloadRecord: function(store, snapshot) {
        return true;
      },
    
      shouldReloadAll: function(store, snapshot) {
        return true;
      },
    
      shouldBackgroundReloadRecord: function(store, snapshot) {
        return false;
      },*/
    
      // Модель вообще не перезагружается при смене языка
      /*shouldBackgroundReloadAll: function(store, snapshot) {
        return false;
      },*/

  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),

  findAll(store, type, id) {

    // TODO: Костыль, пока нет новостей на других языках
    /*let lang = 'en';
    if (this.get('i18n.locale') === 'ru')
        lang = 'ru';*/

    let lang = this.get('i18n.locale');
    const host = getHost();

    //return fetch(`https://indacoin.com/api2/getarticles/${lang}`).then((data) => {
    return fetch(`${host}/api/getarticles/${lang}`).then((data) => {
      return data.json();
    }).then((data) => {
        const me = this;
        let payload = JSON.parse(data.Articles);
        var promises = payload.map(function(item){
            let url = item.url;
            //return fetch(`https://indacoin.com/api2/getarticle/${lang}/${url}`).then(function(response){
            return fetch(`${host}/api/getarticle/${lang}/${url}`).then(function(response){
                if (response.ok) {
                    return response.json();
                }
            }).then((data) => {
                item.fullText = JSON.parse(data.Article).FullText;
              });
        })

        return Promise.all(promises).then(function(results) {
            //return results;
            //return payload;
            //console.log('Debug: adapter news');
            //console.log(payload);
            return payload;
            //console.log('results');
            //console.log(results);
        })
        //console.log('adapter');
        //console.log(payload);
        //return payload;


          
          /*var p1 = new Promise((resolve, reject) => {
            return fetch(`https://indacoin.com/api/getarticle/${lang}/${url}`).then(function(response){
                reject(response);

            });
              
            });
            
            Promise.all([p1]).then(value => { 
              console.log(value);
            }, reason => {
              console.log(reason)
            });*/

        /*payload.forEach((item) => {
            let url = item.url;
            let description = "";
            $.ajax({
                url: `https://indacoin.com/api/getarticle/${lang}/${url}`,
                type: "get",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: false,
                success: function (result) {
                    description = JSON.parse(result.Article).FullText;
                }
              });

            item.id = i++;
            item.type = 'news';
            item.attributes = {
                createdAt: item.createdAt,
                title: item.title,
                header: item.title,
                description: description,
                shortText: item.shortText,
                url: url
            }
        });*/
        





        /*.then((data) => {
              return data;
            });*/




    });
  }
});
