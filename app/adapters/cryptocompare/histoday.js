import DS from 'ember-data';

export default DS.JSONAPIAdapter.extend({
  host: 'https://min-api.cryptocompare.com',
  namespace: 'data/histoday',

});
