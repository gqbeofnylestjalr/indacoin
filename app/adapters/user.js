import DS from 'ember-data';
import fetch from 'fetch';
import CachedShoe from 'ember-cached-shoe';

export default DS.RESTAdapter.extend(CachedShoe, {

  findAll(store, type, id) {
    const url = `https://indacoin.com/inner/RefreshPairData`
    return fetch(url,
        {
            method: 'POST',
            headers: {
                "content-type": "application/json",
                "accept": "*/*",
                "x-requested-with": "XMLHttpRequest"
            },
            body: "{'pair':'BTC_USD'}",
            credentials: "same-origin"
        }).then(function(response) {
            return response.json();
        })

  }

});