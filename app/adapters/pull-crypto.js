import DS from 'ember-data';
import fetch from 'fetch';
import CachedShoe from 'ember-cached-shoe';

export default DS.RESTAdapter.extend(CachedShoe, {
  query(store, type, query) {
    let currency = query.currency.replace(" ", "-");
    //console.log('query from adapter');
    //console.log(query);
    const url = "https://api.coinmarketcap.com/v1/ticker/" + currency + "/"; 
    return fetch(url).then(function(response) {
        return response.json();
      })
  }
});


/*return fetch('https://api.coinmarketcap.com/v1/ticker/' + fullName + '/').then((data) => {
    return data.json();
  }).then((data) => {

    console.log('pullCrypto:');
    console.log(data);
    data[0]['24h_volume_usd'] = parseFloat(data[0]['24h_volume_usd']).toLocaleString('en');
    data[0]['market_cap_usd'] = parseFloat(data[0]['market_cap_usd']).toLocaleString('en');
    data[0]['available_supply'] = parseFloat(data[0]['available_supply']).toLocaleString('en');
    return data;
  })*/