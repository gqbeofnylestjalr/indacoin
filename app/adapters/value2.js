import DS from 'ember-data';
import ENV from 'fastboot/config/environment';
//import CachedShoe from 'ember-cached-shoe';

export default DS.JSONAPIAdapter.extend({//CachedShoe, {
  host: 'http://localhost:4500',

  namespace: 'api',

  pathForType(){
    return 'mobgetcurrencies/';
  }
});