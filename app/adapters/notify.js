import DS from 'ember-data';
import CachedShoe from 'ember-cached-shoe';

export default DS.JSONAPIAdapter.extend(CachedShoe, {
  host: 'https://indacoin.com',

  pathForType(){
    return 'mobgetcurrencies/';
  }

});
