import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import CachedShoe from 'ember-cached-shoe';

export default DS.RESTAdapter.extend(CachedShoe, {
  i18n: Ember.inject.service(),
  query(store, type, query) {

      const i18n = this.get('i18n');
      let lang = query.lang;

      return {
        "data": [
          {
            "id": "19",
            "type": "facebook",
            "attributes": {
              "id": 19,
              "name": 'Radu Robotin',
              "date": '1 June 2018',
              'text': i18n.t('social-segment.facebook-review-19'),
              "link": 'https://www.facebook.com/radu.bogdan.robotin/posts/2005368926148115:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.2.720.720/p720x720/13658951_1248465525171796_2781755563519936591_n.jpg?_nc_cat=0&oh=1bdfb4f7f768ad55ff9508f8354f3f2f&oe=5BBA1701',
              "photo": '/ember/images/social-segment/fb-icons/radu-robotin.jpg',
              "stars": true
            }
          },
          {
            "id": "18",
            "type": "facebook",
            "attributes": {
              "id": 18,
              "name": 'Amarjargal Batmunkh',
              "date": '1 June 2018',
              'text': i18n.t('social-segment.facebook-review-18'),
              "link": 'https://www.facebook.com/amarjargal.batmunkh/posts/1896313717085326:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t31.0-1/c170.50.621.621/p720x720/55897_139703059413076_1317748_o.jpg?_nc_cat=0&oh=fa7ac9dbf8e721d966c4a6fdea393b9c&oe=5BB7F4F1',
              "photo": '/ember/images/social-segment/fb-icons/amarjargal-batmunkh.jpg',
              "stars": true
            }
          },
          {
            "id": "17",
            "type": "facebook",
            "attributes": {
              "id": 17,
              "name": 'Su Razz',
              "date": '6 April 2018',
              'text': i18n.t('social-segment.facebook-review-17'),
              "link": 'https://www.facebook.com/mrsuraaz/posts/10214186054423579:0',
              //"photo": 'https://scontent-frx5-1.xx.fbcdn.net/v/t1.0-1/c0.0.720.720/p720x720/23517670_10212999363917058_4989947067260278910_n.jpg?_nc_cat=0&oh=4063762cac39896ef7ef7f2d2cbe702a&oe=5B2B1198',
              "photo": '/ember/images/social-segment/fb-icons/su-razz.jpg',
              "stars": true
            }
          },
          {
            "id": "16",
            "type": "facebook",
            "attributes": {
              "id": 16,
              "name": 'Bruce Hosking',
              "date": '4 April 2018',
              'text': i18n.t('social-segment.facebook-review-16'),
              "link": 'https://www.facebook.com/bruce.hosking.16/posts/10155182898081577:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/28279834_10155092636981577_7186897493463334147_n.jpg?_nc_cat=0&oh=3087f85f7eec2bfa54ca6b57d5024bb6&oe=5B339682',
              "photo": '/ember/images/social-segment/fb-icons/bruce-hosking.jpg',
              "stars": true
            }
          },
          {
            "id": "15",
            "type": "facebook",
            "attributes": {
              "id": 15,
              "name": 'Mitchy Macsback',
              "date": '28 March 2018',
              'text': i18n.t('social-segment.facebook-review-15'),
              "link": 'https://www.facebook.com/mitchy.macsback.1/posts/188007221982379:0',
              //"photo": 'https://scontent.fiev2-1.fna.fbcdn.net/v/t31.0-1/c0.120.720.720/p720x720/23847257_108626473253788_7867917509437835874_o.jpg?_nc_cat=0&oh=f94f3e470ae22fc277b215df2e1ade72&oe=5B2EFE12',
              "photo": '/ember/images/social-segment/fb-icons/mitchy-macsback.jpg',
              "stars": true
            }
          },
          {
            "id": "14",
            "type": "facebook",
            "attributes": {
              "id": 14,
              "name": 'ZeXin Tan',
              "date": '15 March 2018',
              'text': i18n.t('social-segment.facebook-review-14'),
              "link": 'https://www.facebook.com/zexin.tan/posts/10155757918028813:0',
              //"photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/1662283_10152092800458813_1519926875_n.jpg?oh=f875479dc7af5129e6da2b9854756647&oe=5B3D246D',
              "photo": '/ember/images/social-segment/fb-icons/zexin-tan.jpg',
              "stars": false
            }
          },
          {
            "id": "13",
            "type": "facebook",
            "attributes": {
              "id": 13,
              "name": 'James D Ong',
              "date": '23 January 2018',
              'text': i18n.t('social-segment.facebook-review-13'),
              "link": 'https://www.facebook.com/jamesohc/posts/10156091823723988:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/13512_10153198221763988_4917367960444447177_n.jpg?oh=ae90665bd6b3390a297707436393987d&oe=5B1FAF83',
              "photo": '/ember/images/social-segment/fb-icons/james-d-ong.jpg',
              "stars": true
            }
          },
          {
            "id": "12",
            "type": "facebook",
            "attributes": {
              "id": 12,
              "name": 'Ravi Kumar',
              "date": '2 January 2018',
              'text': i18n.t('social-segment.facebook-review-12'),
              "link": 'https://www.facebook.com/avancharavi/posts/1545081162234848:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t31.0-1/c0.110.720.720/p720x720/19400587_1374518959291070_5612258024248812088_o.jpg?_nc_cat=0&oh=235aa5125ba009b36693bf5a36147806&oe=5BB835BD',
              "photo": '/ember/images/social-segment/fb-icons/ravi-kumar.jpg',
              "stars": true
            }
          },
          {
            "id": "11",
            "type": "facebook",
            "attributes": {
              "id": 11,
              "name": 'Edhi Sanggramawijaya',
              "date": '2 December 2017',
              'text': i18n.t('social-segment.facebook-review-11'),
              "link": 'https://www.facebook.com/edhifransiskus/posts/1948710585456232:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.104.272.272/28279776_1996216210705669_552727411512417370_n.jpg?_nc_cat=0&oh=03c9362c746c9ef68b7863d6b3d2136c&oe=5B615F20',
              "photo": '/ember/images/social-segment/fb-icons/edhi-sanggramawijaya.jpg',
              "stars": true
            }
          },
          {
            "id": "10",
            "type": "facebook",
            "attributes": {
              "id": 10,
              "name": 'David Joelson',
              "date": '2 December 2017',
              'text': i18n.t('social-segment.facebook-review-10'),
              "link": 'https://www.facebook.com/DavJoelson/posts/10155867919734344:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/31164199_10156246090889344_9193156626898943616_n.jpg?_nc_cat=0&oh=3b23235e6cec70aec89c9bd08cb5af5b&oe=5B7A33A1',
              "photo": '/ember/images/social-segment/fb-icons/david-joelson.jpg',
              "stars": true
            }
          },
          {
            "id": "9",
            "type": "facebook",
            "attributes": {
              "id": 9,
              "name": 'John Lufadeju',
              "date": '26 October 2017',
              'text': i18n.t('social-segment.facebook-review-9'),
              "link": 'https://www.facebook.com/johnluffa/posts/10155685247532295:0',
              //"photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/16711666_10154929416812295_3193779618206967220_n.jpg?oh=a3f3f62a5aadd18323d06ca768a56999&oe=5B410562',
              "photo": '/ember/images/social-segment/fb-icons/john-lufadeju.jpg',
              "stars": true
            }
          },
          {
            "id": "8",
            "type": "facebook",
            "attributes": {
              "id": 8,
              "name": 'Zubin Madon',
              "date": '2 October 2017',
              'text': i18n.t('social-segment.facebook-review-8'),
              "link": 'https://www.facebook.com/zmadon/posts/10159430187885597:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/10991096_10155248486680597_5444122527339881753_n.jpg?_nc_cat=0&oh=3b65caea8bf64c012b2e427d906c11ec&oe=5BBD0119',
              "photo": '/ember/images/social-segment/fb-icons/zubin-madon.jpg',
              "stars": false
            }
          },
          {
            "id": "7",
            "type": "facebook",
            "attributes": {
              "id": 7,
              "name": 'Saulo Oliveira',
              "date": '17 August 2017',
              'text': i18n.t('social-segment.facebook-review-7'),
              "link": 'https://www.facebook.com/saulooliver/posts/1407893592593315:0',
              //"photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/14522933_1105947412787936_77294846013930768_n.jpg?oh=ffd2bed89ee0c9f46434c5c7fbd8265c&oe=5B3E5554',
              "photo": '/ember/images/social-segment/fb-icons/saulo-oliveira.jpg',
              "stars": true
            }
          },
          {
            "id": "6",
            "type": "facebook",
            "attributes": {
              "id": 6,
              "name": 'Adarsh Chandra',
              "date": '5 July 2017',
              'text': i18n.t('social-segment.facebook-review-6'),
              "link": 'https://www.facebook.com/permalink.php?story_fbid=102688820378273&id=100019114657607&substory_index=0',
              //"photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=3a35b5f4b1b4b85c2f9100147ed9f4ae&oe=5B3BB62E',
              "photo": '/ember/images/social-segment/fb-icons/adarsh-chandra.jpg',
              "stars": true
            }
          },
          {
            "id": "5",
            "type": "facebook",
            "attributes": {
              "id": 5,
              "name": 'Stijn Van Bezouwen',
              "date": '28 June 2017',
              'text': i18n.t('social-segment.facebook-review-5'),
              "link": 'https://www.facebook.com/stijn.van/posts/1632543306787133:0',
              //"photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/24775042_1837660729608722_700394716654622961_n.jpg?oh=12ba23776d2067cd673cb145e49515de&oe=5B4C0339',
              "photo": '/ember/images/social-segment/fb-icons/stijn-van-bezouwen.jpg',
              "stars": true
            }
          },
          {
            "id": "4",
            "type": "facebook",
            "attributes": {
              "id": 4,
              "name": 'Welf von Hören',
              "date": '23 June 2017',
              'text': i18n.t('social-segment.facebook-review-4'),
              "link": 'https://www.facebook.com/Welfvh/posts/1423492301022788:0',
              //"photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.138.720.720/p720x720/29027384_1676689799036369_3645495673077366784_o.jpg?_nc_cat=0&oh=7fa04ff07f038360f44ecdaf9e50c893&oe=5B6ECD31',
              "photo": '/ember/images/social-segment/fb-icons/welf-von-horen.jpg',
              "stars": true
            }
          },
          {
            "id": "3",
            "type": "facebook",
            "attributes": {
              'id': 3,
              'name': 'Elior Moore',
              'date': '3 June 2017',
              'text': i18n.t('social-segment.facebook-review-3'),
              'link': 'https://www.facebook.com/MooreElior/posts/10211357511991285:0',
              //'photo': 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/p720x720/30730031_10214161437007658_746295780892909387_n.jpg?_nc_cat=0&oh=fa8f54e4366d5b8a1991575ae45e51c2&oe=5B8A3E18',
              "photo": '/ember/images/social-segment/fb-icons/elior-moore.jpg',
              'stars': true
            }
          },
          {
            "id": "2",
            "type": "facebook",
            "attributes": {
              'id': 2,
              'name': 'Lesley Brown',
              'date': '23 May 2017',
              'text': i18n.t('social-segment.facebook-review-2'),
              'link': 'https://www.facebook.com/LEISB/posts/1406771172699010:0',
              //'photo': 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.0.720.720/p720x720/18921788_1417554734953987_529796032077658846_n.jpg?oh=3612a2d9f49b9e99272a854a3c9c8070&oe=5B48F35F',
              "photo": '/ember/images/social-segment/fb-icons/lesley-brown.jpg',
              'stars': true
            }
          },
          {
            "id": "1",
            "type": "facebook",
            "attributes": {
              'id': 1,
              'name': 'John Yance',
              'date': '17 May 2017',
              'text': i18n.t('social-segment.facebook-review-1'),
              'link': 'https://www.facebook.com/john.yance.9/posts/1206759696120213:0',
              //'photo': 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/c0.1.720.720/p720x720/16195614_1098496233613227_1360363683957543436_n.jpg?oh=b26f1e56b3066fb5fa2a62905331fef2&oe=5B463C4F',
              "photo": '/ember/images/social-segment/fb-icons/john-yance.jpg',
              'stars': true
            }
          }
        ]
      }
  }
});




/*

import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';

export default DS.RESTAdapter.extend({
  i18n: Ember.inject.service(),
  query(store, type, query) {

      const i18n = this.get('i18n');
      let lang = query.lang;

      return {
        "data": [
          {
            "id": "1",
            "type": "facebook",
            "attributes": {
              "id": 1,
              "name": 'Mitchy Macsback',
              "date": '28 March 2018',
              'text': i18n.t('social-segment.facebook-review-16'),
              "link": 'https://www.facebook.com/mitchy.macsback.1/posts/188007221982379:0',
              "photo": 'https://scontent.fiev2-1.fna.fbcdn.net/v/t31.0-1/c0.120.720.720/p720x720/23847257_108626473253788_7867917509437835874_o.jpg?_nc_cat=0&oh=f94f3e470ae22fc277b215df2e1ade72&oe=5B2EFE12',
              "stars": true
            }
          },
          {
            "id": "2",
            "type": "facebook",
            "attributes": {
              "id": 2,
              "name": 'ZeXin Tan',
              "date": '15 March 2018',
              'text': i18n.t('social-segment.facebook-review-15'),
              "link": 'https://www.facebook.com/zexin.tan/posts/10155757918028813:0',
              "photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/1662283_10152092800458813_1519926875_n.jpg?oh=f875479dc7af5129e6da2b9854756647&oe=5B3D246D',
              "stars": false
            }
          },
          {
            "id": "3",
            "type": "facebook",
            "attributes": {
              "id": 3,
              "name": 'James D Ong',
              "date": '23 January 2018',
              'text': i18n.t('social-segment.facebook-review-14'),
              "link": 'https://www.facebook.com/jamesohc/posts/10156091823723988:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/13512_10153198221763988_4917367960444447177_n.jpg?oh=ae90665bd6b3390a297707436393987d&oe=5B1FAF83',
              "stars": true
            }
          },
          {
            "id": "4",
            "type": "facebook",
            "attributes": {
              "id": 4,
              "name": 'Ravi Kumar',
              "date": '2 January 2018',
              'text': i18n.t('social-segment.facebook-review-13'),
              "link": 'https://www.facebook.com/avancharavi/posts/1545081162234848:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t31.0-1/c0.110.720.720/p720x720/19400587_1374518959291070_5612258024248812088_o.jpg?oh=ece6bd72ae6e4c8c6109eb8159075f29&oe=5AF274BD',
              "stars": true
            }
          },
          {
            "id": "5",
            "type": "facebook",
            "attributes": {
              "id": 5,
              "name": 'Le Chivaliere Noir',
              "date": '9 December 2017',
              'text': i18n.t('social-segment.facebook-review-12'),
              "link": 'https://www.facebook.com/lechivaliere.noir/posts/765593383651676:0',
              "photo": 'https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/c22.22.272.272/954853_100700910140930_463254404_n.jpg?oh=a7cbe83d37e4beafa86a32a98f168339&oe=5AC6C524',
              "stars": true
            }
          },
          {
            "id": "6",
            "type": "facebook",
            "attributes": {
              "id": 6,
              "name": 'Edhi Sanggramawijaya',
              "date": '2 December 2017',
              'text': i18n.t('social-segment.facebook-review-11'),
              "link": 'https://www.facebook.com/edhifransiskus/posts/1948710585456232:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.104.272.272/28279776_1996216210705669_552727411512417370_n.jpg?_nc_cat=0&oh=03c9362c746c9ef68b7863d6b3d2136c&oe=5B615F20',
              "stars": true
            }
          },
          {
            "id": "7",
            "type": "facebook",
            "attributes": {
              "id": 7,
              "name": 'David Joelson',
              "date": '2 December 2017',
              'text': i18n.t('social-segment.facebook-review-10'),
              "link": 'https://www.facebook.com/DavJoelson/posts/10155867919734344:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.0.720.720/13886340_10154358510444344_5575470458989997254_n.jpg?oh=9060dbd9d8b35e038fe7b3af292b7e03&oe=5AF43959',
              "stars": true
            }
          },
          {
            "id": "8",
            "type": "facebook",
            "attributes": {
              "id": 8,
              "name": 'John Lufadeju',
              "date": '26 October 2017',
              'text': i18n.t('social-segment.facebook-review-9'),
              "link": 'https://www.facebook.com/johnluffa/posts/10155685247532295:0',
              "photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/16711666_10154929416812295_3193779618206967220_n.jpg?oh=a3f3f62a5aadd18323d06ca768a56999&oe=5B410562',
              "stars": true
            }
          },
          {
            "id": "9",
            "type": "facebook",
            "attributes": {
              "id": 9,
              "name": 'Zubin Madon',
              "date": '2 October 2017',
              'text': i18n.t('social-segment.facebook-review-8'),
              "link": 'https://www.facebook.com/zmadon/posts/10159430187885597:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/10991096_10155248486680597_5444122527339881753_n.jpg?oh=f652ea856dd57dd91e60674ff2eeff5f&oe=5AF74019',
              "stars": false
            }
          },
          {
            "id": "10",
            "type": "facebook",
            "attributes": {
              "id": 10,
              "name": 'Saulo Oliveira',
              "date": '17 August 2017',
              'text': i18n.t('social-segment.facebook-review-7'),
              "link": 'https://www.facebook.com/saulooliver/posts/1407893592593315:0',
              "photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/14522933_1105947412787936_77294846013930768_n.jpg?oh=ffd2bed89ee0c9f46434c5c7fbd8265c&oe=5B3E5554',
              "stars": true
            }
          },
          {
            "id": "11",
            "type": "facebook",
            "attributes": {
              "id": 11,
              "name": 'Adarsh Chandra',
              "date": '5 July 2017',
              'text': i18n.t('social-segment.facebook-review-6'),
              "link": 'https://www.facebook.com/permalink.php?story_fbid=102688820378273&id=100019114657607&substory_index=0',
              "photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=3a35b5f4b1b4b85c2f9100147ed9f4ae&oe=5B3BB62E',
              "stars": true
            }
          },
          {
            "id": "12",
            "type": "facebook",
            "attributes": {
              "id": 12,
              "name": 'Stijn Van Bezouwen',
              "date": '28 June 2017',
              'text': i18n.t('social-segment.facebook-review-5'),
              "link": 'https://www.facebook.com/stijn.van/posts/1632543306787133:0',
              "photo": 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/p720x720/24775042_1837660729608722_700394716654622961_n.jpg?oh=12ba23776d2067cd673cb145e49515de&oe=5B4C0339',
              "stars": true
            }
          },
          {
            "id": "13",
            "type": "facebook",
            "attributes": {
              "id": 13,
              "name": 'Welf von Hören',
              "date": '23 June 2017',
              'text': i18n.t('social-segment.facebook-review-4'),
              "link": 'https://www.facebook.com/Welfvh/posts/1423492301022788:0',
              "photo": 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.138.720.720/p720x720/29027384_1676689799036369_3645495673077366784_o.jpg?_nc_cat=0&oh=7fa04ff07f038360f44ecdaf9e50c893&oe=5B6ECD31',
              "stars": true
            }
          },
          {
            "id": "14",
            "type": "facebook",
            "attributes": {
              'id': 14,
              'name': 'Elior Moore',
              'date': '3 June 2017',
              'text': i18n.t('social-segment.facebook-review-3'),
              'link': 'https://www.facebook.com/MooreElior/posts/10211357511991285:0',
              'photo': 'https://scontent.fiev2-1.fna.fbcdn.net/v/t1.0-1/16425762_10210303663325727_2144166811873008511_n.jpg?oh=8ca5302672a4713225b8fe9cf5e25da8&oe=5ADB6E6C',
              'stars': true
            }
          },
          {
            "id": "15",
            "type": "facebook",
            "attributes": {
              'id': 15,
              'name': 'Lesley Brown',
              'date': '23 May 2017',
              'text': i18n.t('social-segment.facebook-review-2'),
              'link': 'https://www.facebook.com/LEISB/posts/1406771172699010:0',
              'photo': 'https://scontent.fhel3-1.fna.fbcdn.net/v/t1.0-1/c0.0.720.720/p720x720/18921788_1417554734953987_529796032077658846_n.jpg?oh=3612a2d9f49b9e99272a854a3c9c8070&oe=5B48F35F',
              'stars': true
            }
          },
          {
            "id": "16",
            "type": "facebook",
            "attributes": {
              'id': 16,
              'name': 'John Yance',
              'date': '17 May 2017',
              'text': i18n.t('social-segment.facebook-review-1'),
              'link': 'https://www.facebook.com/john.yance.9/posts/1206759696120213:0',
              'photo': 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/c0.1.720.720/p720x720/16195614_1098496233613227_1360363683957543436_n.jpg?oh=b26f1e56b3066fb5fa2a62905331fef2&oe=5B463C4F',
              'stars': true
            }
          }
        ]
      }         




  }
});

*/