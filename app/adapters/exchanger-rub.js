import Ember from 'ember';
import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import CachedShoe   from 'ember-cached-shoe'

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  findAll(store, type, id) {

    /*let path = "";
    if (ENV.environment === 'development') {
      path = "https://indacoin.com/api/GetExchangerAmount/USD/100";
    } else if (ENV.environment === 'production') {
      path = "/api/GetExchangerAmount/USD/100";
    } else if (ENV.environment === 'debug') {
      path = "https://indacoin.com/api/GetExchangerAmount/USD/100";
    }*/
    const host = getHost();
    //let path = host + "/api2/GetExchangerAmount/RUB/3000";
    let path = host + "/api/GetExchangerAmount/RUB/3000";

    //console.log('ENV.environment = ' + ENV.environment);
    //console.log('path = ' + path);

    //return fetch("https://indacoin.com/api/GetExchangerAmount/USD/100").then((data) => {
      return fetch(path).then((data) => {
      return data.json();
    }).then((data) => {
      const i18n = this.get('i18n');
      const share = this.get('share');
      let region = share.getRegion(); // US
      if (region === null)
        region = "US";
      return {
        "data":
        [{
          "id": "1",
          "type": "exchanger-rub",
          "attributes": {
            "price": accounting.formatMoney(data * 0.5, "", 4, ".", ","),
            //"url": "https://indacoin.com/change/buy-bitcoin-with-cardusd/"+ i18n.locale +"?amount_pay=30",
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardrub" + "?amount_pay=3000",
            "cur": "RUB",
            "sum": "3000,00"
          }
        },
        {
          "id": "2",
          "type": "exchanger-rub",
          "attributes": {
            "price": accounting.formatMoney(data, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardrub" + "?amount_pay=6000",
            "cur": "RUB",
            "sum": "6000,00"
          },
        },
        {
          "id": "3",
          "type": "exchanger-rub",
          "attributes": {
            "price": accounting.formatMoney(data * 2.5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardrub" + "?amount_pay=15000",
            "cur": "RUB",
            "sum": "14000,00"
          },
        },
        {
          "id": "4",
          "type": "exchanger-rub",
          "attributes": {
            "price": accounting.formatMoney(data * 5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardrub" + "?amount_pay=30000",
            "cur": "RUB",
            "sum": "30000,00"
          }
        }]
      }
    });
  }
});
