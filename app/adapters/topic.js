import DS from 'ember-data';
import fetch from 'fetch';
import CachedShoe from 'ember-cached-shoe';

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {
  query(store, type, query) {
    let lang = query.lang;
    let host = getHost();
    //return fetch(`https://indacoin.com/api2/getarticles/${lang}`).then((data) => {
    return fetch(`${host}/api/getarticles/${lang}`).then((data) => {
      return data.json();
    }).then((data) => {
        return JSON.parse(data.Articles);
    });
  }
});
