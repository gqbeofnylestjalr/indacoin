import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import Ember from 'ember';
import CachedShoe from 'ember-cached-shoe';

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {

  /*shouldReloadAll: function(store, snapshot) {
      return false;
  },

  shouldReloadRecord: function(store, snapshot) {
      return true;
    },

  shouldReloadAll: function(store, snapshot) {
    return true;
  },

  shouldBackgroundReloadRecord: function(store, snapshot) {
    return false;
  },

  // Модель вообще не перезагружается при смене языка
  shouldBackgroundReloadAll: function(store, snapshot) {
    return true;
  },*/

  query(store, type, query) {
    //console.log('Call query news2');

    //let lang = this.get('i18n.locale');
    let lang = query.lang;
    let host = getHost();

    //return fetch(`https://indacoin.com/api2/getarticles/${lang}`).then((data) => {
    return fetch(`${host}/api/getarticles/${lang}`).then((data) => {
      return data.json();
    }).then((data) => {
        const me = this;
        let payload = JSON.parse(data.Articles);
        var promises = payload.map(function(item){
            let url = item.url;
            return fetch(`https://indacoin.com/api2/getarticle/${lang}/${url}`).then(function(response){
            //return fetch(`https://indacoin.com/api/getarticle/${lang}/${url}`).then(function(response){
                if (response.ok) {
                    return response.json();
                }
            }).then((data) => {
                item.fullText = JSON.parse(data.Article).FullText;
              });
        })
        return Promise.all(promises).then(function(results) {
            return payload;
        })
    });
  }
});
