import DS from 'ember-data';
import CachedShoe from 'ember-cached-shoe';

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {
  //host: 'https://indacoin.com',
  host: getHost(),
  //namespace: 'api2',
  namespace: 'api',

  pathForType(){
    return 'GetExchangerAmount/USD/100';
  }
});