import DS from 'ember-data';
import fetch from 'fetch';
import accounting from "accounting"
import ENV from 'fastboot/config/environment';
import CachedShoe from 'ember-cached-shoe';

function getHost() {
  if (typeof FastBoot === 'undefined')
    return 'https://indacoin.com';
  else
    return 'http://localhost:4500';
}

export default DS.RESTAdapter.extend(CachedShoe, {
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  findAll(store, type, id) {
    /*let path = "";
    if (ENV.environment === 'development') {
      path = "https://indacoin.com/api/GetExchangerAmount/EUR/100";
    } else if (ENV.environment === 'production') {
      path = "/api/GetExchangerAmount/EUR/100";
    }*/
    const host = getHost();
    //return fetch("https://indacoin.com/api2/GetExchangerAmount/EUR/100").then((data) => {
    return fetch(host + "/api/GetExchangerAmount/EUR/100").then((data) => {
      return data.json();
    }).then((data) => {
      const i18n = this.get('i18n');
      const share = this.get('share');
      let region = share.getRegion(); // US
      if (region === null)
        region = "US";
      return {
        "data":
        [{
          "id": "1",
          "type": "exchanger-eur",
          "attributes": {
            "price": accounting.formatMoney(data * 0.3, "", 4, ".", ","),
            //"url": "https://indacoin.com/change/buy-bitcoin-with-cardeur/"+ i18n.locale +"?amount_pay=30",
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardeur" + "?amount_pay=30",
            "cur": "EUR",
            "sum": "30,00"
          }
        },
        {
          "id": "2",
          "type": "exchanger-eur",
          "attributes": {
            "price": accounting.formatMoney(data, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardeur" + "?amount_pay=100",
            "cur": "EUR",
            "sum": "100,00"
          },
        },
        {
          "id": "3",
          "type": "exchanger-eur",
          "attributes": {
            "price": accounting.formatMoney(data * 2.5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardeur" + "?amount_pay=250",
            "cur": "EUR",
            "sum": "250,00"
          },
        },
        {
          "id": "4",
          "type": "exchanger-eur",
          "attributes": {
            "price": accounting.formatMoney(data * 5, "", 4, ".", ","),
            "url": "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardeur" + "?amount_pay=500",
            "cur": "EUR",
            "sum": "500,00"
          }
        }]
      }
    });
  }
});
