import Ember from 'ember';

export function positiveNumber(params/*, hash*/) {

  if (Number(params[0]) <= 0) {
    return Ember.String.htmlSafe(`<img class="image-red-arrow" src="/ember/images/currency/red-arrow.svg" onselectstart="return false" onmousedown="return false">`);
  }
  else {
    return Ember.String.htmlSafe(`<img class="image-blue-arrow" src="/ember/images/currency/blue-arrow.svg" onselectstart="return false" onmousedown="return false">`);
  }
  
}

export default Ember.Helper.helper(positiveNumber);
