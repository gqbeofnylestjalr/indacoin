import Ember from 'ember';

export function newsText(params/*, hash*/) {
  let text = params[0];
  
  if (params[0] !== undefined) {
    let points = text.length < Number(params[1]) ? '' : '...';
    text = text.slice(0, Number(params[1])) + points;
  }
  return Ember.String.htmlSafe(`${text}`);
}

export default Ember.Helper.helper(newsText);