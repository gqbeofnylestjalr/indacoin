import Ember from 'ember';

export function mapSegmentCountry(params/*, hash*/) {
  //debugger;
  if (params[0].length === 2)
    return Ember.String.htmlSafe(`<span>${params[0][0]} <span class="blue">${params[0][1]}</span></span>`);
  else if (params[0].length === 1)
    return Ember.String.htmlSafe(`<span>${params[0][0]}</span>`);
  else return false;
}

export default Ember.Helper.helper(mapSegmentCountry);
