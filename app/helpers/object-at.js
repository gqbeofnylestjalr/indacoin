import Ember from 'ember';

export function objectAt([ array, index ]) {
  //debugger;
  if (array === undefined)
   return "";
  if (index === undefined)
    index = 0;
  //return array[index].photo;
  return array.objectAt(index).get('photo');
}

export default Ember.Helper.helper(objectAt);