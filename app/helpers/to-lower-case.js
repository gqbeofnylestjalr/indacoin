import Ember from 'ember';

export function toLowerCase(params/*, hash*/) {
  
  let text = ''; 
  if (params[0] !== undefined) {
    text = params[0].toLowerCase();
  }
  return text;
}

export default Ember.Helper.helper(toLowerCase);