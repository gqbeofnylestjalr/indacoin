import Ember from 'ember';
import { hrefTo } from 'ember-href-to/helpers/href-to';

export default Ember.Helper.extend({
  i18n: Ember.inject.service(),
  compute(params) {
    const i18n = this.get('i18n');
    let target = hrefTo(this, params[1]);
    let text = i18n.t(params[0]);
    return Ember.String.htmlSafe(`<a href="${target}">${text}</a>`);
  }
});