import DS from 'ember-data';

export default DS.Model.extend({
    usdAvailable: DS.attr('boolean'),
    eurAvailable: DS.attr('boolean'),
    rubAvailable: DS.attr('boolean'),

    btcAvailable: DS.attr('boolean'),
    audAvailable: DS.attr('boolean'),
    gbpAvailable: DS.attr('boolean'),

    usdmin: DS.attr('number'),
    usdmax: DS.attr('number'),
    eurmin: DS.attr('number'),
    eurmax: DS.attr('number'),
    rubmin: DS.attr('number'),
    rubmax: DS.attr('number'),

    btcmin: DS.attr('number'),
    btcmax: DS.attr('number'),
    audmin: DS.attr('number'),
    audmax: DS.attr('number'),
    gbpmin: DS.attr('number'),
    gbpmax: DS.attr('number'),
    test: DS.attr('string'),

    usdCommentPath: DS.attr('string'),
    eurCommentPath: DS.attr('string'),
    rubCommentPath: DS.attr('string'),
    btcCommentPath: DS.attr('string'),
    audCommentPath: DS.attr('string'),
    gbpCommentPath: DS.attr('string'),

    changeDirection: DS.attr('boolean')
});
