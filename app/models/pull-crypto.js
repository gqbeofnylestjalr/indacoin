import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    symbol: DS.attr('string'),
    rank: DS.attr('number'),
    price_usd: DS.attr('number'),
    price_btc: DS.attr('number'),
    volume_usd_24h: DS.attr('number'),
    market_cap_usd: DS.attr('number'),
    available_supply: DS.attr('number'),
    total_supply: DS.attr('number'),
    max_supply: DS.attr('number'),
    change1h: DS.attr('number'),
    change24h: DS.attr('number'),
    change7d: DS.attr('number')
});