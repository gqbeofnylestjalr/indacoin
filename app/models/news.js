import DS from 'ember-data';

export default DS.Model.extend({
    date: DS.attr('string'),
    title: DS.attr('string'),
    text: DS.attr('string'),
    shortText: DS.attr('string'),
    fullText: DS.attr('string'),
    url: DS.attr('string'),
    show: DS.attr('boolean')
});