import DS from 'ember-data';

export default DS.Model.extend({
  cur: DS.attr('string'),
  price: DS.attr('number'),
  sum: DS.attr('number'),
  url: DS.attr('string')
});
