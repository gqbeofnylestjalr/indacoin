import DS from 'ember-data';

export default DS.Model.extend({
    shortText: DS.attr('string'),
    url: DS.attr('string'),
    date: DS.attr('string')
});