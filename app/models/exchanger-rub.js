import DS from 'ember-data';

export default DS.Model.extend({
  cur: DS.attr('string'),
  price: DS.attr('string'),
  sum: DS.attr('string'),
  url: DS.attr('string')
});
