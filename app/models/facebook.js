import DS from 'ember-data';

export default DS.Model.extend({
    name: DS.attr('string'),
    date: DS.attr('string'),
    text: DS.attr('string'),
    link: DS.attr('string'),
    photo: DS.attr('string'),
    stars: DS.attr('boolean')
});
