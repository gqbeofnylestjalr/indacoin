import DS from 'ember-data';

export default DS.Model.extend({
  eternal: DS.attr('string'),
  name: DS.attr('string'),
  symbol: DS.attr('string'),
  price_usd: DS.attr('number'),
  price_btc: DS.attr('number'),
  market_cap_usd: DS.attr('number'),
  available_supply: DS.attr('number'),
  volume24h: DS.attr('number'),
  change1h: DS.attr('number'),
  change24h: DS.attr('number'),
  change7d: DS.attr('number'),
  color: DS.attr('string')
});
