export default {
  //'lang': 'ar',
  'language-select': {
    'language': {
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "recordVideo": {
    "send": "إرسال",
    "stop": "توقف",
    "record": "سجل"
  },
  "links": {
    "terms-of-use": {
      "text": "أوافق على {{{terms}}} وسياسة الخصوصية",
      "link": "تعليمات الاستخدام"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "في انتظار دفعتك",
      "status-2": "تبادل",
      "status-2-sub": "نحن نبحث عن أفضل سعر بالنسبة لك",
      "status-3": "إرسال إلى محفظتك",
      "transaction-completed": "تهانينا ، لقد اكتملت معاملتك!",
      "rate": "سعر الصرف 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "لقد أرسلت:",
      "toAddress": "إلى {{{cryptoAdress}}}",
      "receiveText": "ستستلم:"
    },
    "changeCrypto": {
      "send": "أرسل {{{amount}}}} BTC إلى العنوان أدناه",
      "confirmation": "بعد إرسال ترسين بيتكوين ≈ {{{amount}}} {{{shortName}}} إلى عنوانك {{{wallet}}}",
      "copy": "نسخ العنوان",
      "copied": "نسخ إلى الحافظة"
    },
    "order": "طلب",
    "status": {
      "title": "حالة",
      "completed": "تم عمل الطلب"
    },
    "payAmount": {
      "title": "تم الدفع"
    },
    "check": "التحقق من حالة العملية على blockexplorer.net",
    "resend-phone-code": "لم تستقبل مكالمة؟",
    "sms-code": "يرجى الرد على الهاتف وإدخال رمز مكون من 4 أرقام هنا",
    "registration": "اذهب إلى صفحة التسجيل",

    "internalAccount-1": "يرجى التحقق من محفظة Indacoin الخاصة بك من خلال",
    "internalAccount-link": "تسجيل الدخول",
    "internalAccount-2": "",
    "your-phone": "هاتفك {{{phone}}}",
    "many-requests": "آسف. إلى العديد من طلبات تغيير الهاتف"
  },
  "app":{
    "detect_lang":{
      "title":"إختيار اللغة الخاصة بك",
      "sub":"إنتظر، من فضلك..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) اشترِ وشارك {{{longName}}} على الفور و 100 عملة أخرى مشفرة بأفضل سعر في {{{country}}}",
    "title":"قم بشراء العملة التشفيرية عن طريق بطاقة الائتمان أو بطاقة الخصم - Indacoin",
    "description":"قم بشراء وتحويل أية من العملات التشفيرية على الفور مقابل يورو أو دولار أمريكي: Ripple، Litecoin، Ethereum، [cryptocurrency] و100 عملة تشفيرية أخرى.",
    "description-bitcoin":"قم بشراء وتبادل أي عملة مشفرة على الفور: بيتكوين ، إثيريوم ، ليتكوين ، Ripple و 100 عملة رقمية أخرى لليورو أو الدولار الأمريكي",
    "description-ethereum":"شراء وتبادل أي عملة مشفرة على الفور: Ethereum و Bitcoin و Litecoin و Ripple و 100 عملة رقمية أخرى لليورو أو الدولار الأمريكي",
    "description-litecoin":"شراء وتبادل أي عملة مشفرة على الفور: Litecoin و Bitcoin و Ethereum و Ripple و 100 عملة رقمية أخرى لليورو أو الدولار الأمريكي",
    "description-ripple":"شراء وتبادل أي عملة مشفرة على الفور: Ripple و Bitcoin و Ethereum و Litecoin و 100 عملة رقمية أخرى لليورو أو الدولار الأمريكي",
    
    "description2": "شراء وتبادل أي كورني التشفير على الفور: بيتكوين، إثريوم، ليتسوان، تموج، و 100 العملات الرقمية الأخرى لليورو أو الدولار",
    "description3":"شراء وتبادل أي عملة مشفرة على الفور: بيتكوين ، Ethereum ، Litecoin ، Ripple و 100 عملة رقمية أخرى مع الفيزا والماستركارد",
    "buy": "قم بشراء",
    "subTitle": "عن طريق بطاقة الائتمان أو بطاقة الخصم",
    "more": "في أكثر من 100 دولة، بما في ذلك [روسيا الإتحادية]",
    //"header": "تبادل بيتكوين إلى <span class='sub'>{{{name}}}</span>"
    "header": "تبادل <span class='sub'>BTC</span> على الفور إلى <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "قم بشراء العملة التشفيرية على الفور",
    "header-alt": "تبادل عملة التشفير على الفور",
    "sub": "يوفر Indacoin طريقة سهلة لشراء Bitcoins وأكثر من 100 عملة مشفرة باستخدام Visa & amp؛ بطاقة ماستر بطاقة ائتمان",
    "sub-alt": "Indacoin يوفر وسيلة سهلة لتبادل بيتكوين إلى أي من +100 العملات التشفير المختلفة",
    "observe": "تعرف على العملات التشفيرية المتاحة أدناه"
  },
  "navbar": {
    "main":"الصفحة الرئيسية",
    "btc": "شراء Bitcoin",
    "eth": "شراء كريبتوكيرنسي",
    "affiliate": "الشراكة",
    "news": "الأخبار",
    "faq": "الأسئلة الأكثر تكراراً",
    "login": "التسجيل",
    "registration": "تسجيل الدخول",
    "platform": "منتجات",

    "logout": "الخروج",
    "buy-instantly": "شراء التشفير على الفور",
    "trading-platform": "منصة التداول",
    "mobile-wallet": "المحفظة النقالة",
    "payment-processing": "واجهة برمجة تطبيقات معالجة الدفع"
  },
  "currency-table": {
    "rank": "المرتبة",
    "coin": "العملة",
    "market": "سعر السوق",
    "price": "السعر",
    "volume": "الحجم 24 ساعة",
    "change": "الزيادة 24 ساعة",
    "available-supply": "العرض المتاح",
    "search-cryptocurrency": "البحث عن التشفير التشفير",

    "change-1h": "1H يتغيرون",
    "change-24h": "1D يتغيرون",
    "change-7d": "1W يتغيرون"
  },
  "buy-blocks":{
    "give":"تعطي",
    "get":"تحصل"
  },
  "exchange-form": {
    "title": "شراء العملات التشفيرية على الفور",
    "submit": "صرف",
    "give": "تعطي",
    "get": "تحصل",
    "search": "بحث",
    "error-page": "نعتذر ، لكن الشراء غير متوفر حاليًا"
  },
  "pagination": {
    "page": "صفحة",
    "next": "إلى الأمام",
    "prev": "إلى الخلف",
    "first": "إلى البداية"
  },
  "sidebar":{
    "accept":{
      "title":"نحن نقبل بطاقات الائتمان",
      "button":"شراء العملة التشفيرية"
    },
    "youtube":{
      "title":"عنا",
      "play":"إطلاق"
    },
    "orders":{
      "title": "التحكم بالطلبات",
      "order": "الطلب #",
      "confirm": "كود التحقق",
      "check": "التحقق من الطلب",
      "new": "إنشاء طلب جديد",
      "or": "أو"
    },
    "social":{
      "title":"نحن في مواقع التواصل الإجتماعي"
    }
  },
  "wiki":{
    "etymology":"الاشتقاق"
  },
  "loading": {
    "title": "تحميل",
    "sub": "إنتظر، من فضلك...",
    "text": "تحميل"
  },
  "error": {
    "title": "خطأ",
    "sub": "حدث خطأ ما، لا تفزع",
    "text": "لقد واجهنا مشكلة غير معروفة ونقوم بحلها بالفعل. من فضلك، عد لاحقاً.",
    "home": "الرئيسية"
  },
  "features": {
    "title":"ما يمكنك فعله على <span class=\"blue\">Indacoin<span>",
    "column1": {
      "title": "شراء العملة التشفيرية",
      "text": "على Indacoin يمكنك شراء أكثر من 100 عملة تشفير مختلفة على الفور باستخدام بطاقة الائتمان أو الخصم"
    },
    "column2":{
      "title":"معرفة المزيد حول العملات التشفيرية",
      "text":"إرشاداتنا ونصائحنا البسيطة ستساعدك على المعرفة كيفية عمل العملة Altcoin"
    },
    "column3":{
      "title":"إتخاذ قرارات إستثمارية",
      "text":"أدوات التحليل القوية ستتيح لك من إظهار اللحظة الأفضل لتعبئة وسحب العملة"
    },
    "column4":{
      "title":"شراء بتكوين بدون تسجيل",
      "text":"بعد الدفع سيتم تحويل عملة بتكوين إلى العنوان المذكور"
    },
    "column5":{
      "title":"تعبير عن الرأي",
      "text":"بإمكانك مناقشة آخر تطورات في عالم العملات التشفيرية مع المستثمرين الرواد"
    },
    "column6":{
      "title":"احتفظ بجميع مواد العرض في مكان واحد",
      "text":"بإمكانك الإحتفاظ بأكثر من 100 عملة تشفيرية والتحكم بها في المحفظة Indacoin"
    }
  },
  "mockup":{
    "title":"محفظات للهواتف الجوال",
    "text":"تعمل محفظتنا الذكية Indacoin على نظام أندرويد وعلى أجهزة الآيفون وكذلك في أي متصفح أصبح إرسال عملة تشفيرية إلى جوال صديقك أمراً سهلاً كإرسال رسالة نصية"
  },
  "footer":{
    "column1":{
      "title":"شراء",
      "link1":"شراء Bitcoin",
      "link2":"شراء Ethereum",
      "link3":"شراء Ripple",
      "link4":"شراء Bitcoin Cash",
      "link5":"شراء Litecoin",
      "link6":"شراء Dash"
    },
    "column2":{
      "title":"المعلومات",
      "link1":"عنا",
      "link2":"الأسئلة",
      "link3":"قواعد الاستخدام",
      "link4":"الإرشادات"
    },
    "column3":{
      "title":"الأدوات",
      "link1":"API",
      "link2":"تغيير المنطقة",
      "link3":"تطبيق للهواتف المحمولة"
    },
    "column4":{
      "title":"جهات الاتصال"
    }
  },
  "country":{
    "AF":{
      "name":"أفغانستان"
    },
    "AX":{
      "name":"جزر آلاند"
    },
    "AL":{
      "name":"ألبانيا"
    },
    "DZ":{
      "name":"الجزائر"
    },
    "AS":{
      "name":"ساموا الأمريكية"
    },
    "AD":{
      "name":"أندورا"
    },
    "AO":{
      "name":"أنغولا"
    },
    "AI":{
      "name":"أنغيلا"
    },
    "AQ":{
      "name":"القارة القطبية الجنوبية"
    },
    "AG":{
      "name":"أنتيغوا وبربودا"
    },
    "AR":{
      "name":"الأرجنتين"
    },
    "AM":{
      "name":"أرمينيا"
    },
    "AW":{
      "name":"أروبا"
    },
    "AU":{
      "name":"أستراليا"
    },
    "AT":{
      "name":"النمسا"
    },
    "AZ":{
      "name":"أذربيجان"
    },
    "BS":{
      "name":"جزر الباهاما"
    },
    "BH":{
      "name":"البحرين"
    },
    "BD":{
      "name":"بنغلادش"
    },
    "BB":{
      "name":"بربادوس"
    },
    "BY":{
      "name":"روسيا البيضاء"
    },
    "BE":{
      "name":"بلجيكا"
    },
    "BZ":{
      "name":"بليز"
    },
    "BJ":{
      "name":"بنين"
    },
    "BM":{
      "name":"برمودا"
    },
    "BT":{
      "name":"بوتان"
    },
    "BO":{
      "name":"بوليفيا"
    },
    "BA":{
      "name":"البوسنة والهرسك"
    },
    "BW":{
      "name":"بوتسوانا"
    },
    "BV":{
      "name":"جزيرة بوفيت"
    },
    "BR":{
      "name":"البرازيل"
    },
    "IO":{
      "name":"إقليم المحيط البريطاني الهندي"
    },
    "BN":{
      "name":"بروناي دار السلام"
    },
    "BG":{
      "name":"بلغاريا"
    },
    "BF":{
      "name":"بوركينا فاسو"
    },
    "BI":{
      "name":"بوروندي"
    },
    "KH":{
      "name":"كمبوديا"
    },
    "CM":{
      "name":"كاميرون"
    },
    "CA":{
      "name":"كندا"
    },
    "CV":{
      "name":"الرأس الأخضر"
    },
    "KY":{
      "name":"جزر كايمان"
    },
    "CF":{
      "name":"جمهورية إفريقيا الوسطى"
    },
    "TD":{
      "name":"تشاد"
    },
    "CL":{
      "name":"تشيلي"
    },
    "CN":{
      "name":"الصين"
    },
    "CX":{
      "name":"جزيرة الكريسماس"
    },
    "CC":{
      "name":"جزر كوكوس (كيلينغ)"
    },
    "CO":{
      "name":"كولومبيا"
    },
    "KM":{
      "name":"جزر القمر"
    },
    "CG":{
      "name":"كونغو"
    },
    "CD":{
      "name":"جمهورية الكونغو الديمقراطية\n"
    },
    "CK":{
      "name":"جزر كوك"
    },
    "CR":{
      "name":"كوستا ريكا"
    },
    "CI":{
      "name":"ساحل العاج"
    },
    "HR":{
      "name":"كرواتيا"
    },
    "CU":{
      "name":"كوبا"
    },
    "CY":{
      "name":"قبرص"
    },
    "CZ":{
      "name":"جمهورية التشيك"
    },
    "DK":{
      "name":"دنمارك"
    },
    "DJ":{
      "name":"جيبوتي"
    },
    "DM":{
      "name":"دومينيكا"
    },
    "DO":{
      "name":"جمهورية الدومنيكان"
    },
    "EC":{
      "name":"الإكوادور"
    },
    "EG":{
      "name":"مصر"
    },
    "SV":{
      "name":"السلفادور"
    },
    "GQ":{
      "name":"غينيا الإستوائية"
    },
    "ER":{
      "name":"إرتيريا"
    },
    "EE":{
      "name":"إستونيا"
    },
    "ET":{
      "name":"أثيوبيا"
    },
    "FK":{
      "name":"جزر فوكلاند (مالفيناس)"
    },
    "FO":{
      "name":"جزر صناعية"
    },
    "FJ":{
      "name":"فيجي"
    },
    "FI":{
      "name":"فنلندا"
    },
    "FR":{
      "name":"فرنسا"
    },
    "GF":{
      "name":"غيانا الفرنسية"
    },
    "PF":{
      "name":"بولينيزيا الفرنسية"
    },
    "TF":{
      "name":"المناطق الجنوبية لفرنسا"
    },
    "GA":{
      "name":"الغابون"
    },
    "GM":{
      "name":"غامبيا"
    },
    "GE":{
      "name":"جورجيا"
    },
    "DE":{
      "name":"ألمانيا"
    },
    "GH":{
      "name":"غانا"
    },
    "GI":{
      "name":"جبل طارق"
    },
    "GR":{
      "name":"اليونان"
    },
    "GL":{
      "name":"الأرض الخضراء"
    },
    "GD":{
      "name":"غرينادا"
    },
    "GP":{
      "name":"جوادلوب"
    },
    "GU":{
      "name":"غوام"
    },
    "GT":{
      "name":"غواتيمالا"
    },
    "GG":{
      "name":"غيرنسي"
    },
    "GN":{
      "name":"غينيا"
    },
    "GW":{
      "name":"غينيا بيساو"
    },
    "GY":{
      "name":"غيانا"
    },
    "HT":{
      "name":"هايتي"
    },
    "HM":{
      "name":"جزيرة هيرد وجزر ماكدونالد"
    },
    "VA":{
      "name":"الكرسي الرسولي (دولة الفاتيكان)"
    },
    "HN":{
      "name":"هندوراس"
    },
    "HK":{
      "name":"هونغ كونغ"
    },
    "HU":{
      "name":"هنغاريا"
    },
    "IS":{
      "name":"أيسلندا"
    },
    "IN":{
      "name":"الهند"
    },
    "ID":{
      "name":"إندونيسيا"
    },
    "IR":{
      "name":"الجُمهوريَّة الإسلاميَّة الإيرانيَّة"
    },
    "IQ":{
      "name":"العراق"
    },
    "IE":{
      "name":"أيرلندا"
    },
    "IM":{
      "name":"جزيرة آيل أوف مان"
    },
    "IL":{
      "name":"إسرائيل"
    },
    "IT":{
      "name":"إيطاليا"
    },
    "JM":{
      "name":"جامايكا"
    },
    "JP":{
      "name":"اليابان"
    },
    "JE":{
      "name":"جيرسي"
    },
    "JO":{
      "name":"الأردن"
    },
    "KZ":{
      "name":"كازاخستان"
    },
    "KE":{
      "name":"كينيا"
    },
    "KI":{
      "name":"كيريباس"
    },
    "KP":{
      "name":"جمهورية كوريا الديمقراطية الشعبية"
    },
    "KR":{
      "name":"جمهورية كوريا"
    },
    "KW":{
      "name":"الكويت"
    },
    "KG":{
      "name":"قرغيزستان"
    },
    "LA":{
      "name":"جمهورية لاو الديمقراطية الشعبية"
    },
    "LV":{
      "name":"لاتفيا"
    },
    "LB":{
      "name":"لبنان"
    },
    "LS":{
      "name":"ليسوتو"
    },
    "LR":{
      "name":"ليبيريا"
    },
    "LY":{
      "name":"الجماهيرية العربية الليبية"
    },
    "LI":{
      "name":"ليختنشتاين"
    },
    "LT":{
      "name":"ليتوانيا"
    },
    "LU":{
      "name":"لوكسمبورغ"
    },
    "MO":{
      "name":"ماكاو"
    },
    "MK":{
      "name":"جمهورية مقدونيا اليوغوسلافية السابقة"
    },
    "MG":{
      "name":"مدغشقر"
    },
    "MW":{
      "name":"مالاوي"
    },
    "MY":{
      "name":"ماليزيا"
    },
    "MV":{
      "name":"جزر مالديف"
    },
    "ML":{
      "name":"مالي"
    },
    "MT":{
      "name":"مالطا"
    },
    "MH":{
      "name":"جزر مارشال"
    },
    "MQ":{
      "name":"مارتينيك"
    },
    "MR":{
      "name":"موريتانيا"
    },
    "MU":{
      "name":"موريشيوس"
    },
    "YT":{
      "name":"مايوت"
    },
    "MX":{
      "name":"المكسيك"
    },
    "FM":{
      "name":"ولايات ميكرونيسيا المتحدة"
    },
    "MD":{
      "name":"جمهورية مولدوفا"
    },
    "MC":{
      "name":"موناكو"
    },
    "MN":{
      "name":"منغوليا"
    },
    "MS":{
      "name":"مونتسيرات"
    },
    "MA":{
      "name":"المغرب"
    },
    "MZ":{
      "name":"موزمبيق"
    },
    "MM":{
      "name":"ميانمار"
    },
    "NA":{
      "name":"ناميبيا"
    },
    "NR":{
      "name":"ناورو"
    },
    "NP":{
      "name":"النيبال"
    },
    "NL":{
      "name":"هولندا"
    },
    "AN":{
      "name":"جزر الأنتيل الهولندية"
    },
    "NC":{
      "name":"كاليدونيا الجديدة"
    },
    "NZ":{
      "name":"نيوزيلاندا"
    },
    "NI":{
      "name":"نيكاراغوا"
    },
    "NE":{
      "name":"النيجر"
    },
    "NG":{
      "name":"نيجيريا"
    },
    "NU":{
      "name":"نيوي"
    },
    "NF":{
      "name":"جزيرة نورفولك"
    },
    "MP":{
      "name":"جزر مريانا الشمالية"
    },
    "NO":{
      "name":"النرويج"
    },
    "OM":{
      "name":"عمان"
    },
    "PK":{
      "name":"باكستان"
    },
    "PW":{
      "name":"بالاو"
    },
    "PS":{
      "name":"فلسطين"
    },
    "PA":{
      "name":"بنما"
    },
    "PG":{
      "name":"بابوا غينيا الجديدة"
    },
    "PY":{
      "name":"باراغواي"
    },
    "PE":{
      "name":"بيرو"
    },
    "PH":{
      "name":"الفلبين"
    },
    "PN":{
      "name":"بيتكيرن"
    },
    "PL":{
      "name":"بولندا"
    },
    "PT":{
      "name":"البرتغال"
    },
    "PR":{
      "name":"بورتوريكو"
    },
    "QA":{
      "name":"قطر"
    },
    "RE":{
      "name":"لا ريونيون"
    },
    "RO":{
      "name":"رومانيا"
    },
    "RU":{
      "name":"روسيا الإتحادية"
    },
    "RW":{
      "name":"رواندا"
    },
    "SH":{
      "name":"سانت هيلينا"
    },
    "KN":{
      "name":"سانت كيتس ونيفيس"
    },
    "LC":{
      "name":"القديسة لوسيا"
    },
    "PM":{
      "name":"سانت بيير وميكلون"
    },
    "VC":{
      "name":"سانت فنسنت وجزر غرينادين"
    },
    "WS":{
      "name":"ساموا"
    },
    "SM":{
      "name":"سان مارينو"
    },
    "ST":{
      "name":"ساو تومي وبرينسيبي"
    },
    "SA":{
      "name":"السعودية"
    },
    "SN":{
      "name":"السنغال"
    },
    "CS":{
      "name":"صربيا والجبل الأسود"
    },
    "SC":{
      "name":"سيشيل"
    },
    "SL":{
      "name":"سيرا ليون"
    },
    "SG":{
      "name":"سنغافورة"
    },
    "SK":{
      "name":"سلوفاكيا"
    },
    "SI":{
      "name":"سلوفينيا"
    },
    "SB":{
      "name":"جزر سلمون"
    },
    "SO":{
      "name":"صوماليا"
    },
    "ZA":{
      "name":"إفريقيا الشمالية"
    },
    "GS":{
      "name":"جورجيا الجنوبية وجزر ساندويتش الجنوبية"
    },
    "ES":{
      "name":"إسبانيا"
    },
    "LK":{
      "name":"سيريلانكا"
    },
    "SD":{
      "name":"السودان"
    },
    "SR":{
      "name":"سورينام"
    },
    "SJ":{
      "name":"سفالبارد وجان ماين"
    },
    "SZ":{
      "name":"سوازيلاند"
    },
    "SE":{
      "name":"السويد"
    },
    "CH":{
      "name":"سويسرا"
    },
    "SY":{
      "name":"الجمهورية العربية السورية"
    },
    "TW":{
      "name":"تايوان"
    },
    "TJ":{
      "name":"طاجيكستان"
    },
    "TZ":{
      "name":"جمهورية تنزانيا الاتحادية"
    },
    "TH":{
      "name":"تايلاند"
    },
    "TL":{
      "name":"تيمور الشرقية"
    },
    "TG":{
      "name":"توغو"
    },
    "TK":{
      "name":"توكيلاو"
    },
    "TO":{
      "name":"تونغا"
    },
    "TT":{
      "name":"ترينداد وتوباغو"
    },
    "TN":{
      "name":"تونس"
    },
    "TR":{
      "name":"تركيا"
    },
    "TM":{
      "name":"تركمنستان"
    },
    "TC":{
      "name":"جزر تركس وكايكوس"
    },
    "TV":{
      "name":"توفالو"
    },
    "UG":{
      "name":"أوغندا"
    },
    "UA":{
      "name":"أوكرانيا"
    },
    "AE":{
      "name":"الإمارات العربية المتحدة"
    },
    "GB":{
      "name":"المملكة المتحدة"
    },
    "US":{
      "name":"الولايات المتحدة"
    },
    "UM":{
      "name":"جزر الولايات المتحدة الصغيرة النائية"
    },
    "UY":{
      "name":"أوروغواي"
    },
    "UZ":{
      "name":"أوزبكستان"
    },
    "VU":{
      "name":"فانواتو"
    },
    "VE":{
      "name":"فنزويلا"
    },
    "VN":{
      "name":"فييتنام"
    },
    "VG":{
      "name":"الجزر العذراء البريطانية"
    },
    "VI":{
      "name":"الجزر العذراء الأمريكية"
    },
    "WF":{
      "name":"واليس وفوتونا"
    },
    "EH":{
      "name":"الصحراء الغربية"
    },
    "YE":{
      "name":"اليمن"
    },
    "ZM":{
      "name":"زامبيا"
    },
    "ZW":{
      "name":"زيمبابوي"
    }
  },
  "currency":{
    "RUB":"روبل روسي",
    "BTC":"بتكوين",
    "USD":"دولار أمريكي"
  },
  "buy":"شراء",
  "sell":"بيع",
  "weWillCallYou":"إذ كنت تستعمل هذه البطاقة للمة الأولى سيتوجب عليك كتابة رمزين سريين: من مستخرج مصرفي الخاص بك ومن اتصال المخبر الآلي على هاتفك.",
  "exchangerStatuses":{
        "TimeOut": "انتهت مدة الطلب",
        "Error": "لقد حدث خطأ، يرجى الكتابة إلى فريق الدعم support@indacoin.com",
        "WaitingForAccountCreation": "بانتظار فتح حسابكم (أرسلنا رسالة التسجيل)ا",
        "CashinWaiting": "تمت معالجة دفعتك",
        "BillWaiting": "في انتظار دفع الحساب من قبل الزبون",
        "Processing": "الطلب قيد المعالجة",
        "MoneySend": "تم إرسال المال",
        "Completed": "تم تنفيذ الطلب",
        "Verifying": "المصادقة",
        "Declined": "تم الرفض",
        "cardDeclinedNoFull3ds": "لقد تم رفض بطاقتك لأن المصرف الخاص بك لا يدعم الميزة 3ds على هذه البطاقة، إتصل بفريق الدعم في المصرف الخاص بك.",
        "cardDeclined": "تم رفض بطاقتك. إذ كان لديك أسئلة، يرجى الإتصال بفريق الدعم: support@indacoin.com",
        "Non3DS": "للأسف ، تم رفض دفعتك.تحقق في المصرف الخاص بك بأنك تستعمل بطاقة مع 3D-secure (المعترف برمز الحماية من قبل Visa أو MasterCard) مما يعني كتابة الرمز السري أثناء كل عملية الدفع، الذي عادة ما يتم إرساله إلى جوالك."
  },
  "exchanger_fields_ok":"انقر لشراء بتكوين",
  "exchanger_fields_error":"خطأ في الملء. تحقق من جميع الحقول",
  "bankRejected":"تم رفض المعاملة من قبل المصرف الخاص بك. يرجى الإتصال بالمصرف الخاص بك وإعادة الدفع من البطاقة",
  "wrongAuthCode":"لقد قمت بكتابة كود المصادقة خاطئ، يرجى الإتصال بفريق الدعم: support@indacoin.com",
  "wrongSMSAuthCode":"لقد قمت بكتابة كود خاطئ من الهاتف",
  "getCouponInfo":{
    "indo":"هذا الكوبون غير مقبول",
    "exetuted":"تم استعمال هذا الكوبون بالفعل",
    "freeRate":"تم تفعيل كوبون الخصم"
  },
  "ip":"تم حظر مؤقتاً عنوان بروتوكول الإنترنت الخاص بك",
  "priceChanged":"لقد تغير السعر. حسنا؟ً ستحصل ",
  "exchange_go":"– صرف",
  "exchange_buy_btc":"– شراء بتكوين",
  "month":"يناير فبراير مارس أبريل مايو يونيو يوليو أغسطس سبتمبر أكتوبر نوفمبر ديسمبر",
  "discount":"خصم",
  "cash":{
    "equivalent":" معدلاً ",
    "card3DS":"نقبل للدفع فقط بطاقات مع 3D-Secure (Verified by Visa or MasterCard SecureCode)",
    "cashIn":{
      "1": "بطاقات مصرفية(USD)",
      "2": "أجهزة الصراف الآلي والدفع",
      "3": "QIWI",
      "4": "المصرف باشكومسناب",
      "5": "أجهزة الدفع Novoplat",
      "6": "أجهزة الدفع Elexnet",
      "7": "بطاقات مصرفية(USD)",
      "8": "ألفا-كليك",
      "9": "تحويل دولي عن طريق مصرف",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Money",
      "21": "Elexnet",
      "22": "Kassira.net",
      "23": "Mobile Element",
      "24": "Svyaznoy",
      "25": "Evroset",
      "26": "PerfectMoney",
      "27": "Indacoin-داخلي",
      "28":	"CouponBonus",
      "29": "Yandex.Money",
      "30": "OKPay",
      "31": "برنامج الشراكة",
      "33": "Payza",
      "35": "كود BTC-E",
      "36": "بطاقات مصرفية(USD)",
      "37": "بطاقات مصرفية(USD)",
      "39": "تحويل دولي عن طريق مصرف",
      "40": "Yandex.Money",
      "42": "QIWI(يدوياً)",
      "43": "UnionPay",
      "44": "QIWI (تلقائي)",
      "45": "دفع من رصيد الهاتف",
      "49": "LibrexCoin",
      "50": "بطاقات مصرفية(EURO)",
      "51": "QIWI (تلقائي)",
      "52": "دفع من رصيد الهاتف",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "الرجاء إدخال عنوان التشفير الصحيح",
      "country": "بلد غير مقبول",
      "alfaclick": "تسجيل الدخول غير مقبول",
      "pan": "رقم البطاقة غير مقبول",
      "cardholdername": "بيانات غير مقبولة",
      "fio": "بيانات غير مقبولة",
      "bday": "تاريخ الميلاد غير صحيح",
      "city": "بيانات غير مقبولة",
      "address": "عنوان غير مقبول",
      "ypurseid": "رقم المحفظة غير مقبول",
      "wpurseid": "رقم المحفظة غير مقبول",
      "pmpurseid": "رقم الحساب غير مقبول",
      "payeer": "رقم الحساب غير مقبول",
      "okpayid": "رقم المحفظة غير مقبول",
      "purse": "رقم المحفظة غير مقبول",
      "phone": "هاتف غير مقبول",
      "btcaddress": "عنوان غير مقبول",
      "ltcaddress": "عنوان غير مقبول",
      "lxcaddress": "عنوان غير مقبول",
      "ethaddress": "عنوان غير مقبول",
      "properties": "تفاصيل الحساب غير مقبولة",
      "email": "بريد إلكتروني غير مقبول",
      "card_number": "لا يمكن دعم هذه البطاقة، يرجى التحقق من الرقم",
      "inc_card_number": "رقم البطاقة غير صحيح",
      "inn": "رقم غير صحيح",
      "poms": "رقم غير صحيح",
      "snils": "رقم غير صحيح",
      "cc_expr": "تاريخ غير صحيح",
      "cc_name": "إسم غير صحيح",
      "cc_cvv": "cvv غير صحيح"
    }
  },
  "change": {
    "wallet": "إنشاء محفظة جديدة / لدي بالفعل حساب Indacoin",
    //"accept": "أوافق على اتفاقية المستخدم وسياسة مكافحة غسل الأموال",
    "accept": "أوافق على شروط الاستخدام وسياسة الخصوصية",
    "coupone": "لدي كوبون",
    "watchVideo": "مشاهدة فيديو",
    "seeInstructions": "الدعم",
    "step1": {
      "title": "إنشاء طلب",
      "header": "قم بكتابة تفاصيل الدفع",
      "subheader": "دعنا نعلم ماذا تود أن تكتسب"
    },
    "step2": {
      "title": "معلومات الدفع",
      "header": "قم بكتابة تفاصيل بطاقة الائتمان الخاصة بك",
      "subheader": "نقبل فقط بطاقات Visa و Mastercard",
      "card": {
        "header": "أين يمكنني العثور على هذا الكود؟",
        "text": "بإمكانك العثور على الرقم CVV على الوجه الخلفي للبطاقة. يتألف هذا الكود من 3 أرقام"
      }
    },
    "step3": {
      "title": "معلومات الإتصال",
      "header": "قم بكتابة معلومات الإتصال الخاصة بك",
      "subheader": "إسمح لنا لنتعرف على زبائننا بشكل أفضل"
    },
    "step4": {
      "title": "المصادقة",
      "header": "قم بكتابة الكود الخاص بك من صفحة الدفع",
      "subheader": "معلومات الدفع الخاصة بك"
    },
    "step5": {
      "title": "معلومات حول الطلب"
    },
    "step6": {
      "title": "تسجيل فيديو",
      "header": "أرنا بطاقتك الشخصية ووجهك",
      "description": "أرنا أنك إنسان جدي",
      "startRecord": "أنقر على الزر بدء لبدء التسجيل",
      "stopRecord": "أنقر إيقاف لإيقاف التسجيل"
    },

    "nextButton": "التالى",
    "previousButton": "سابق",
    "passKYCVerification": "التحقق KYC",
    "soccer-legends": "أوافق على نقل بياناتي الشخصية ومعالجتها لشركة Soccer Legends Limited",
    "withdrawalAmount": "المبلغ المراد إضافته إلى الحساب"
  },
  "form": {
    "tag": "علامة {{{currency}}}",
    "loading": "جاري التحميل الرجاءالانتظار",
    "youGive": "أنت تعطي",
    "youTake": "خذ",
    "email": "البريد الإلكتروني",
    "password": "كلمه السر",
    //"cryptoAdress": "{{{name}}} محفظة عنوان",
    "cryptoAdress": "عنوان محفظة تشفير",
    "externalTransaction": "معرف المعاملة",
    "coupone": "لدي قسيمة خصم",
    "couponeCode": "رمز القسيمة",
    "cardNumber": "رقم البطاقة",
    "month": "MM",
    "year": "YY",
    "name": "اسم",
    "fullName": "الاسم الكامل",
    "mobilePhone": "تليفون محمول",
    "birth": "تاريخ ميلادك في شكل MM.DD.YYYY",
    "videoVerification": "التحقق من الفيديو",
    "verification": "التحقق",
    "status": "تحقق من حالة",
    "login": { // TODO: translate
      "captcha": "كلمة التحقق",
      "reset": "إعادة تعيين",
      "forgot": "هل نسيت كلمة المرور",
      "signIn": "تسجيل الدخول"
    },
    "registration": { // TODO: translate
        "create": "خلق"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "التحقق بسيط",
      "title-2": "أكثر من 100 عملة متاحة",
      "title-3": "أي رسوم خفية",
      "title-4": "رقم التسجيل"
    },
    "title": "نحن نعمل في أكثر من 100 دولة",
    "subtitle": "بما في ذلك {{{country}}}",
    "shortTitle": "نحن نعمل في أكثر من 100 دولة بما في ذلك روسيا",
    "text1": "التحقق البسيط",
    "popupText1": "المشترين لأول مرة لديهم فقط للتحقق من رقم الهاتف",
    "text2": "يتم دعم 100 ألتسوانز",
    "popupText2": "يمكنك شراء أكثر من 100 كريبتوكيرنسيز الواعدة مع بطاقة الائتمان / الخصم",
    "text3": "أي رسوم خفية",
    "popupText3": "سوف تحصل بالضبط نفس الكمية من ألتسوانز، التي تم عرضها قبل الصفقة",
    "text4": "رقم التسجيل",
    "popupText4": "لا تحتاج إلى الاشتراك، سيتم إرسال تشريبتوكيرنسي إلى العنوان الذي أشار"
  },
  "buying-segment": {
    "title": "أكثر من 500000 عميل",
    "sub-title": "تستخدم Indacoin منذ عام 2015"
  },
  "phone-segment": {
      "title": "تحميل تطبيقنا",
      "text1": "شراء وتخزين أكثر من 100 كريبتوكيرنسيز في محفظة واحدة",
      "text2": "تتبع التغييرات من محفظة الاستثمار الخاصة بك",
      "text3": "إرسال كريبتوكيرنسي إلى صديقك أصبح سهلا كما إرسال رسالة"
  },
  "social-segment": {
    "buttonText": "قراءة في الفيسبوك",
    "on": "على الفيس بوك",
    "facebook-review-1": "صفحة بيتكوين جيدة ، عملية سريعة وبسيطة ، أوصي بها",
    "facebook-review-2": "خدمة رائعة ومريحة للاستخدام ، والقوم هناك ودية ومفيدة للغاية. على الرغم من أنها عبارة عن صفقة رقمية ، إلا أن هناك شخصًا في الجانب الآخر منها (والذي يمكن أن يكون مساعدة هائلة) ويطمئن بالتأكيد ، خدمة 5 نجوم والتي سأستخدمها مرة أخرى",
    "facebook-review-3": "بما أنني بدأت في تذوق كل ما يحيط بالعالم المجهري ، فإن إجراء تبادل في موقعهم على الإنترنت أكثر من بسيط. ودعمهم في حالة تعلقك أو مجرد الحصول على المساعدة هو أكثر من مدهش.",
    "facebook-review-4": "دفع سريع جدا ، دعم كبير وأسعار صرف كبيرة.",
    "facebook-review-5": "قررت شراء 100 بت من البيتكوين باستخدام هذه الخدمة ، وكان من السهل متابعة الدفع. على الرغم من أني واجهت بعض المشاكل والأسئلة ، إلا أن فريق الدعم التابع له قد ساعدني على الفور. جعلني أشعر أكثر راحة شراء بيتكوين مع Indacoin. وأخطط لشراء المزيد في المستقبل.",
    "facebook-review-6": "أنا فقط استخدمت Indacoin لإجراء صفقة لشراء Bitcoin. ذهبت الصفقة على نحو سلس ، تليها مكالمة التحقق. من السهل جدا استخدام وبوابة صديقة للمستخدم لشراء العملات قليلا. أود أن أوصي هذا النظام الأساسي لشراء bitcoins!",
    "facebook-review-7": "من الصعب العثور على تسهيلات معاملة بطاقات ائتمانية سهلة لشراء عملة مشفرة ، وعاملتني شركة Indcoin بشكل جيد للغاية ، والصبر مع المستخدمين من البلدان الأخرى الذين لا يفهمون لغتهم ، ودردشة الاستعداد عبر الإنترنت. انكون شكرا لك!",
    "facebook-review-8": "سهل سريع. كان الدعم عبر الإنترنت سريعًا جدًا ومفيدًا. نصيحة للمحترفين: يؤدي استخدام التطبيق إلى تقليل الرسوم بنسبة 9٪.",
    "facebook-review-9": "هذه هي المرة الثانية التي استخدم فيها Indacoin - خدمة رائعة ، دعم سريع للمعاملات الرائعة للعملاء. سأستخدم بالتأكيد مرة أخرى.",
    "facebook-review-10": "خدمة واتصالات رائعة للغاية ، خطوة بخطوة تساعد في الدردشة الحية وموثوقة تماما ، حتى أنها اتصلت بي وطلبت وشرحت لي كيفية القيام بالتحقق من المعاملة ، وأسهل طريقة لشراء بيتكوين دون أي مشاكل على الإطلاق ، وشكرا لكم Indacoin سوف تفعل الكثير من المعاملات معك.",
    "facebook-review-11": "خدمة رائعة ، عملية سريعة. خدمة عملاء مفيدة. يستجيب بسرعة. طريقة بسيطة لشراء عملة التشفير. من بين خدمات أخرى ، بصراحة هذه هي الخدمة الوحيدة التي أستخدمها لشراء عملة التشفير. أنا أثق بهم لمعالجة عملية الشراء. أوصت لجميع الذين يتعاملون مع شراء عملة التشفير. أخيرًا وليس آخرًا ، كلمة واحدة لـ Indacoin: ممتاز!",
    //"facebook-review-12": "لدى IndaCoin خدمة فائقة سهلة وموثوقة وسريعة. كما أنهم كانوا سريعون للغاية في الإجابة على جميع أسئلتي ومساعدتي في عملية النقل. نوصي بشدة خدماتها للجميع! 10/10!",
    "facebook-review-12": "اشتريت bitcoins مقابل 200 يورو والموقع شرعي. على الرغم من أن الأمر استغرق وقتًا لإجراء المعاملة ، فإن خدمة العملاء ممتازة. لقد وصلت إلى دعم العملاء وقاموا بفرز مشكلتي على الفور. بالتأكيد أوصي بهذا الموقع للجميع. في بعض الأحيان بسبب ارتفاع عدد الزيارات ، يتراوح وقت المعاملة بين 8-10 ساعات. لا داعي للذعر إذا لم تستلم العملات المعدنية الخاصة بك على الفور في بعض الأحيان سوف يستغرق بعض الوقت ولكن بالتأكيد سوف تحصل على عملاتك المعدنية.",
    "facebook-review-13": "عمل رائع Indacoin. كان أفضل تجربة حتى الآن في شراء العملات المعدنية مع بطاقة الائتمان الخاصة بي مع Indacoin .. قام المستشار أندرو بعمل خدمة عملاء رائعة ، وقد أعجبت بكيفية نقل أقل من 3 دقائق ... سوف تستمر في استخدامها :)",
    "facebook-review-14": "Indacoin هي واجهة سهلة الاستخدام وسهلة الاستخدام يمكن للمشترين شراء Bitcoins أسهل في منصة هناك. من السهل أن تساعد من خلال الاستشاريين وكلاء خطوة بخطوة وهم على استعداد لمساعدتك على حل مشكلتك بشكل فوري. أنا ذاهب إلى إحالة الناس إلى Indacoin وهم باستمرار تحديث لي في مواقف بلدي وهم على استعداد للذهاب ميل إضافي لحل probems الخاص بك. شكرا لكم Indacoin!",
    "facebook-review-15": "الطريقة الأسهل والأسرع لشراء عملة التشفير التي وجدتها في الكثير من البحث. رفاق العمل العظيم ، شكرا!",
    "facebook-review-16": "طريقة سهلة حقا لشراء العملات المعدنية عبر الإنترنت. خدمة زبائن رائعة أيضا. انها رائعة لديهم عملات بديل لشراء المباشر كذلك. يأخذ التعقيدات للخروج من التبادلات. حقا أوصى هذا للجميع بعد تجربتي إيجابية الأولى.",
    "facebook-review-17": "خدمة رائعة. سهل الاستخدام وسهل لشراء مختلف العملات / الرموز.",
    "facebook-review-18": "هذا هو واحد من أفضل منصة لتبادل BTC. فعلت عدة تبادل وكان سريعا.",
    "facebook-review-19": "خدمة رائعة وسهلة الاستخدام ، وسهلة لشراء عملة التشفير المختلفة ورموز ERC20 مع بطاقة الائتمان الخاصة بك أو الخصم. سريع جدا وآمن أيضا! الخدمة والمنصة التي كنت أبحث عنها! نوصي تماما باستخدام Indacoin!"

  },
  "cryptocurrencies-segment": {
      "title": "أكثر من {{{count}}} عملة تشفير مختلفة متاحة للشراء على {{{name}}}"
  },


  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin في هاتفك",
    "text": "لدينا الذكية المحفظة إنداسوان متاح لمستخدمي الروبوت، دائرة الرقابة الداخلية وفي كل متصفح الإنترنت. إرسال كريبتوكيرنسي إلى صديقك أصبح سهلا كما إرسال رسالة"
  },

  "news-segment": {
    "no-news": "لا يوجد أخبار عن لغتك",
    "news-error": "نعتذر ، الأخبار غير متوفرة حاليًا",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },

  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "هذا التشفير العملة غير متوفرة حاليا ولكن سوف يعود قريبا",
    "unknown": "خطأ غير معروف في الخادم",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "تجاوز الحد",
    "phone": "Phone is not supported",
    "mainPageError1": "حاول زيارة الموقع لاحقا",
    "mainPageError2": "للأسف، لا تتوفر معلومات عن العملات المتاحة للتشفير",
    "page-404": "نعتذر ، لم يتم العثور على الصفحة",
    "page-error": "نعتذر ، حدث خطأ"
  },
  "tags": {
    "title-btc": "تبادل بيتكوين BTC ل {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "شراء [cryptocurrency] مع بطاقة الائتمان أو الخصم في [country] إنسانتلي - إنداسوان",
    "title2": "شراء بيتكوين مع بطاقة الائتمان أو الخصم في جميع أنحاء العالم - إنداكوان",
    "title3": "اشترِ {{{cryptocurrency}}} باستخدام بطاقة الائتمان أو الخصم في {{{country}}} - Indacoin",
    "title4": "اشترِ عملة مشفرة باستخدام بطاقة ائتمان أو بطاقة خصم في {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Indacoin التابعة لبرنامج",
      "news": "Indacoin آخر الأخبار",
      "help": "تبادل العملة المشفرة F.A.Q. - اندياكوين",
      "api": "واجهة برمجة التطبيقات لتبادل العملات - Indacoin",
      "terms-of-use": "شروط الاستخدام وسياسة مكافحة غسل الأموال - Indacoin",
      "login": "تسجيل الدخول - Indacoin",
      "register": "اشترك - Indacoin",
      "locale": "تغيير المنطقة - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) مخططات الأسعار وحجم التداول - Indacoin"
    },
    "descriptions": {
      "affiliate": "يتيح لك برنامج affaiate التابع لـ Indacoin كسب ما يصل إلى 3٪ من كل عملية شراء باستخدام بطاقة ائتمان / خصم. سيتم إرسال المكافأة إلى محفظة بيتكوين Indacoin الخاصة بك",
      "news": "تفضل بزيارة مدونتنا الإخبارية الرسمية للبقاء على اتصال مع آخر الأخبار حول صناعة العملات المشفرة! الكثير من المعلومات المفيدة والتعليقات الشاملة من خبراء Indacoin في مكان واحد!",
      "help": "العثور على إجابات لجميع أسئلتك حول كيفية شراء وبيع وتبادل العملات المشفرة على Indacoin.",
      "api": "يوفر API لدينا شراء وتبادل أي عملة مشفرة على الفور مع Visa & Mastercard. تحقق التكامل وأمثلة.",
      "terms-of-use": "معلومات عامة. تعليمات الاستخدام. تقديم خدمات. تحويل الأموال. نقل الحقوق. اللجان. سياسة مكافحة غسيل الأموال.",
      "login": "تسجيل الدخول أو إنشاء حساب جديد",
      "register": "انشاء حساب جديد",
      "locale": "اختر منطقتك ، وحدد هذا وسوف نتذكر منطقتك في المرة التالية التي تزور فيها Indacoin",
      "currency": "الأسعار الحالية لـ {{{fullCurrency}}} إلى USD مع حجم التداول ومعلومات عملة معماة التاريخية"
    }
  },
  "usAlert": "نحن آسفون، ولكن إنداسوان لا تعمل في الولايات المتحدة",
  "wrongPhoneCode": "لقد أدخلت رمز خطأ من هاتفك",
  "skip": "تخطى",
  "minSum": "الحد الأدنى لشراء",
  "currency": {
    "no-currency": "بالنسبة لمعيار عملة معطى ، لا يتوفر الرسم البياني",
    "presented": {
      "header": "تم تقديم البيتكوين للعالم في عام 2009 وفي عام 2017 ، تظل العملة الأساسية في العالم"
    },
    "difference": {
      "title": "كيف تختلف بيتكوين عن الأموال العادية؟",
      "header1": "انبعاثات محدودة",
      "text1": "العدد الإجمالي لل Bitcoins التي يمكن استخراجه هو 21M. أكثر من 16 مليون بيتكوين في التداول الآن. كلما حصلت على شعبية أكثر ، كانت أغلى.",
      "header2": "عملة عالمية",
      "text2": "لا توجد حكومة تسيطر على Bitcoin. يعتمد على الرياضيات فقط",
      "header3": "الغفلية",
      "text3": "يمكنك استخدام Bitcoins الخاص بك في أي وقت وفي أي مبلغ. هذه نتيجة حقيقية في مقابل تحويل الأموال من حسابك المصرفي ، عندما تحتاج إلى شرح وجهة الدفع وكذلك مصدر الأموال.",
      "header4": "سرعة",
      "text4": "سيستغرق المصرف وقتًا طويلاً لتحويل الأموال ، خاصةً عندما يتعلق الأمر بالقيم الكبيرة. مع Bitcoin ، سيستغرق روتين الإرسال والاستقبال ثواني.",
      "header5": "شفافية",
      "text5": "يمكن رؤية جميع معاملات Bitcoin في blockchain."
    },
    "motivation": {
      "title": "كونها عملة البكر من نوعها ، تمكنت من تغيير رؤية المال كما عرفناها. في الوقت الحالي ، تصل القيمة السوقية للبيتكوين إلى أكثر من 70 مليار دولار ، وهو ما يتجاوز عملاقين مثل Tesla و General Motors و FedEx."
    },
    "questions": {
      "header1": "ما هي العملة الرقمية والعملة المشفرة؟",
      "text1": "العملة المشفرة هي أحد الأصول الرقمية ، والتي تعمل كوسيط تبادل باستخدام التشفير لضمان المعاملات والتحكم في إنشاء وحدات إضافية للعملة. ليس له شكل مادي وهو لامركزي على عكس الأنظمة المصرفية المعتادة",
      "header2": "كيف تتم معاملة Bitcoin؟",
      "text2": "لكل معاملة 3 أبعاد: الإدخال - أصل البيتكوين المستخدم في المعاملة. يخبرنا عن المكان الذي حصل عليه المالك الحالي. المبلغ - عدد \"العملات\" المستخدمة. الإخراج - عنوان المتلقي من المال.",
      "header3": "من أين أتى بيتكوين؟",
      "text3": "على الرغم من أن العالم يعرف اسم مبتكر Bitcoin ، فإن \"Satoshi Nakamoto\" هو على الأرجح اسم مستعار لمبرمج أو حتى مجموعة من هذه. أساسا ، بيتكوين يأتي من 31000 سطر من التعليمات البرمجية التي تم إنشاؤها بواسطة \"ساتوشي\" ويتم التحكم فيها فقط من قبل البرامج.",
      "header4": "كيف تعمل بيتكوين؟",
      "text4": "تحدث المعاملات بين محافظ Bitcoin وهي شفافة. في blockchain ، يمكن لأي شخص التحقق من أصل ودورة الحياة لكل \"عملة معدنية\". ومع ذلك ، لا يوجد \"عملة معدنية\". ليس لدى البيتكوين شكلاً ماديًا وليس له شكل افتراضي ، سوى تاريخ المعاملات.",
      "header5": "ما هي محفظة بيتكوين؟",
      "text5": "محفظة Bitcoin هي عبارة عن برنامج يسمح لك بإدارة Bitcoin: استقبال وإرسالها ، والتحقق من الرصيد ومعدل Bitcoin. على الرغم من عدم وجود \"عملة معدنية\" في هذه المحافظ ، إلا أنها تحتوي على مفاتيح رقمية تستخدم للوصول إلى عناوين Bitcoin العامة وتوقيع المعاملات. محافظ شعبية:",
    },
    "history": {
      "title": "تاريخ موجز للبيتكوين",
      "text1": "وفقا للأسطورة ، هذا هو عندما يبدأ ساتوشي ناكاموتو عمله على Bitcoin",
      "text2": "أول معاملة تمت في بيتكوين. يتم تحديد قيمة البيتكوين ، بناءً على تكلفة الكهرباء المستخدمة لتوليد Bitcoin ، بسعر 1 دولار أمريكي لـ 1.306.03 BTC",
      "text3": "يتم بالفعل إنشاء 25 ٪ من جميع Bitcoins",
      "text4": "تبدأ Dell و Windows قبول Bitcoin",
      "text5": {
        "part1": "يرتفع سعر BTC من 1،402 دولارًا أمريكيًا إلى 19209 دولارًا أمريكيًا في غضون بضعة أشهر.",
        "part2": "يصل سعر البيتكوين إلى 20.000 دولار أمريكي لكل عملة معدنية في يوم 18 ديسمبر 2017"
      },
      "text6": "هو تسجيل Bitcoin.org",
      "text7": {
        "part1": "لحظة تاريخية حقا ، عندما يتم استخدام 10000 BTC لشراء 2 البيتزا بشكل غير مباشر. اليوم يمكنك شراء 2.500 بيتزا بيبروني بنفس الكمية.",
        "part2": "سعر البتكوين ينطلق من 0.008 دولار أمريكي إلى 0.08 دولار أمريكي ل 1 BTC في 5 أيام فقط.",
        "part3": "أول معاملة المحمول يحدث"
      },
      "text8": {
        "part1": "تصنف خزانة الولايات المتحدة BTC على أنها عملة افتراضية لامركزية قابلة للتحويل.",
        "part2": "أول صراف بيتكوين في سان دييجو ، كاليفورنيا.",
        "part3": "سعر بيتكوين يتضاعف ويصل إلى 503.10 دولار. ينمو إلى 1000 دولار في الشهر نفسه"
      },
      "text9": "استبعد القاضي الفيدرالي الأمريكي أن BTC هي \"أموال ضمن المعنى البسيط لهذا المصطلح\""
    },
    "graph": {
      "buy-button": "يشترى"
    },
    "ethereum": {
      "presented": {
        "header": "لدينا بالفعل Bitcoin ، فلماذا نحتاج Ethereum؟ دعونا نحفر في ذلك! Bitcoin يضمن تحويلات مالية عادلة وشفافة. يمكن أن تلعب Ether ، العملة الرقمية لشركة Ethereum ، نفس الدور. ومع ذلك ، هناك الكثير من الوظائف التي تقدمها Ethereum."
      },
      "questions": {
        "header1": "What is Ethereum?",
        "text1": "Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
        "header2": "Where did it come from?",
        "text2": "Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
        "header3": "What is ICO?",
        "text3": "Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
        "header4": "What is Ether?",
        "text4": "Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
        "header5": "What’s so special about “smart contracts” and what’s its difference from usual contracts?",
        "text5": "Each transaction has 3 dimensions: Input – the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount – the number of “coins” used. The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
        "header6": "How is Ethereum different from Bitcoin?",
        "text6": "Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens – Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
        "header7": "What are the most popular Ethereum wallets?",
        "text7": "An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets: "
      },
      "history": {
        "title": "The brief history of Ethereum",
        "text1": "Crowdsale is initiated to gather funds for creating Ethereum",
        "text2": "The Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history",
        "text3": "Ethereum gains 50% market share in initial coin offering",
        "text4": "Ethereum exchange rate reaches 400 USD (5,000% rise since January)",
        "text5": "The idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine",
        "text6": "30th of July platform goes online",
        "text7": "The DAO is hacked, Ethereum value drops from 21.5 USD to 8 USD. This leads to Ethereum being forked in two blockchains in 2017",
        "text8": "Enterprise Ethereum Alliance is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc",
        "text9": {
          "part1": "All cryptocurrencies drop after China bans ICO, but quickly recover afterward.",
          "part2": "Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "هل أنت على استعداد للسماح لعملائك بشراء تشفير باستخدام بطاقة الائتمان / الخصم؟ يرجى ترك البريد الإلكتروني وسوف نتصل في اسرع وقت ممكن",
        "input": "عنوان البريد الإلكتروني",
        "button": "البدء",
        "mail-success": "شكرا لكم. سنقوم بالرد عليك"
      },
      "api": {
        "title": "تكامل واجهة برمجة التطبيقات",
        "text": "باستخدام API الخاص بنا ، يمكنك السماح لعملائك بشراء العملات المشفرة على الفور من موقعك. كما يمكن استخدام منصة مكافحة الاحتيال الخاصة بنا والمدمجة على صفحة الويب الخاصة بك أو تطبيق الجوال كأداة تتيح لك مراقبة وتصفية المدفوعات التي يتم إجراؤها على منصتك."
      },
      "affiliate": {
        "title": "برنامج الانتساب",
        "text": "احصل على المكافأة لنصائح Indacoin للعملاء الآخرين. سوف تكسب ما يصل إلى 3 ٪ من مشتريات الإحالات",
        "block-1": {
          "title": "كيف يمكنني الانضمام إلى البرنامج التابع لشركة Indacoin؟",
          "text": "لا تتردد في تسجيل الدخول على موقعنا"
        },
        "block-2": {
          "title": "كيف سأتمكن من سحب الأموال التي تم جمعها في حسابي؟",
          "text": "يمكنك شراء BTC / ETH / العملات المعدنية البديلة الأخرى على Indacoin دون أي رسوم أو استلامها إلى بطاقة البنك الخاصة بك"
        },
        "block-3": {
          "title": "ما هي ميزة برنامج الانتساب الخاص بك؟",
          "text": "على منصة التداول لدينا ، لدينا العملاء الفرصة لشراء أكثر من 100 قطعة نقدية مختلفة باستخدام بطاقات الائتمان / الخصم دون تسجيل. وبالنظر إلى حقيقة أن عمليات الشراء تتم على الفور ، يمكن للشركاء الحصول على ربحهم على الفور"
        },
        "block-4": {
          "title": "هل يمكنني رؤية مثال رابط الإحالة؟",
          "text": "بالطبع ، هكذا يبدو الأمر: ({{{link}}})"
        }
      },
      "more": "اكتشف المزيد",
      "close": "قريب"
    },
    "achievements": {
      "partnership-title": "شراكة",
      "partnership-text": "هناك العديد من الطرق للتعاون مع Indacoin. برنامج الشراكة Indacoin هو للشركات ذات الصلة التشفير التي هي على استعداد لربط العالم من العملات crypto مع نظام المال التقليدي لزيادة المشاركة في صناعة التشفير. مع Indacoin ، ستتمكن من تزويد عملائك بتجربة مخصصة لتشفير التشفير ببطاقات الائتمان / الخصم",
      "block-1": {
        "header": "موثوق به",
        "text": "لقد عملنا في سوق التشفير منذ عام 2013 وشاركنا مع العديد من الشركات المرموقة التي تقود هذه الصناعة. استخدم أكثر من 500000 عميل من أكثر من 190 دولة خدماتنا لشراء العملات المشفرة"
      },
      "block-2": {
        "header": "آمنة",
        "text": "أنشأنا منصة مبتكرة وعالية الفعالية لمكافحة الاحتيال مع تكنولوجيا أساسية تم تطويرها خصيصًا لكشف ومنع الأنشطة الاحتيالية لمشاريع التشفير"
      },
      "block-3": {
        "header": "جاهز للاستخدام الحل",
        "text": "يتم استخدام منتجنا بالفعل من قبل ICO ، وتبادل العملات المعدلة ، ومنصات سلسلة الكتلة وأموال التشفير"
      },

      "header": "كسب المال معنا على اختراق",
      "subheader": "تقنيات بلوكشين",
      "text1": "{{{company}}} هو منصة عالمية، والتي تتيح للناس شراء الفور بيتكوين، إثريوم، تموج، موجات و 100 كريبتوكيرنسيز مختلفة أخرى مع بطاقة الائتمان أو الخصم",
      "text2": "وقد عملت هذه الخدمة منذ عام 2013 تقريبا في جميع أنحاء العالم بما في ذلك سبيل المثال لا الحصر:",
      "text3": "المملكة المتحدة وكندا وألمانيا وفرنسا وروسيا وأوكرانيا وإسبانيا وإيطاليا وبولندا وتركيا وأستراليا والفلبين وإندونيسيا والهند وبيلاروس والبرازيل ومصر والمملكة العربية السعودية والإمارات العربية المتحدة ونيجيريا وغيرها"
    },
    "clients": {
      "header": "أكثر من 500 000 العملاء",
      "subheader": "منذ 2013"
    },
    "benefits": {
      "header": "فوائدنا الرئيسية للشركات:",
      "text1": "خدمة فريدة من نوعها تتيح للعملاء شراء وإرسال بيتكوينس دون تسجيل. لذلك العملاء الذين يأتون إلى إنداسوان بعد وصلة الإحالة يجب أن مجرد إدخال تفاصيل البطاقة وعنوان المحفظة، حيث ينبغي إرسال التشفير. ولذلك العملاء المحتملين جعل عمليات الشراء على الفور وشركائنا الحصول على الربح.",
      "text2": "يتم الحصول على ما يصل إلى 3٪ من كل معاملة من قبل شركائنا. وعلاوة على ذلك، فإن مشرفي المواقع يحصلون على 3٪ من جميع المشتريات المستقبلية للإحالة.",
      "text3": "ويمكن استخدام بطاقات بيتكوين وبطاقات مصرفية لسحب الدخل."
    },
    "accounts": {
      "header": "الانضمام إلينا الآن",
      "emailPlaceholder": "أدخل عنوان بريدك الالكتروني",
      "captchaPlaceholder": "كلمة التحقق",
      "button": "البدء"
    }
  },
  "faq": {
    "title": "أسئلة وأجوبة",
    "header1": "اسئلة عامة",
    "header2": "عملة معماة",
    "header3": "التحقق",
    "header4": "اللجان",
    "header5": "شراء",
    "question1": {
      "title": "ما هو إنداكوان؟",
      "text": "نحن شركة تعمل في مجال كريبتوكيرنسي منذ عام 2013، ومقرها في لندن، المملكة المتحدة. يمكنك استخدام خدماتنا لشراء أكثر من 100 كريبتوكيرنسيز مختلفة من خلال الائتمان / بطاقة الخصم الدفع دون تسجيل."
    },
    "question2": {
      "title": "كيف يمكنني الاتصال بك؟",
      "text": "يمكنك الاتصال بفريق الدعم عن طريق البريد الإلكتروني (support@indacoin.com)، والهاتف (+ 44-207-048-2582) أو باستخدام خيار الدردشة الحية على موقعنا على شبكة الإنترنت."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "أي كريبتوكيرنسيز يمكنني شراء هنا؟",
      "text": "يمكنك شراء أكثر من 100 كريبتوكيرنسيز المختلفة التي يتم سرد كافة على موقعنا."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "ما هي البلدان التي يمكنها استخدام هذه الخدمة؟",
      "text": "ونحن نعمل مع جميع البلدان، مع استثناء فقط من الولايات المتحدة الأمريكية (في الوقت الراهن)"
    },
    "question7": {
      "title": "هل يمكنني بيع بيتكوين هنا؟",
      "text": "لا، يمكنك فقط شراء بيتسوانز هنا."
    },
    "question8": {
      "title": "ما هو كريبتوكيرنسي؟",
      "text": "العملة التشفيرية هي العملة الرقمية التي تستخدم التشفير للأمن، الميزة، التي تجعل من الصعب المزيفة. وهي لا تصدر عن أي سلطة مركزية، مما يجعلها محصنة نظريا من تدخل الحكومة أو التلاعب بها."
    },
    "question9": {
      "title": "أين يمكنني تخزين كريبتوكيرنسي؟",
      "text": "يمكنك إعداد محفظة منفصلة لكل كريبتوكيرنسي على جهاز الكمبيوتر الخاص بك أو يمكنك استخدام خدمة المحفظة المجانية وتخزين 100+ كريبتوكيرنسيز كل شيء في مكان واحد."
    },
    "question10": {
      "title": "ماذا تعني \"عنوان بيتكوين\"؟",
      "text": "هذا هو عنوان محفظة بيتكوين الخاص بك. يمكنك أيضا إدخال عنوان الشخص الذي تريد بيتسوانز ليتم إرسالها إلى أو إنشاء محفظة على إنداسوان"
    },
    "question11": {
      "title": "ما مدى سرعة معاملة كريبتوكيرنسي؟",
      "text": "أوقات المعاملات كريبتوكيرنسي تعتمد على كريبتوكيرنسي كنت ترسل، بيتكوين المعاملات يستغرق حوالي 15-20 دقيقة لإكمال ومتوسط السماح تصل إلى 1 ساعة لبقية القطع النقدية."
    },
    "question12": {
      "title": "ما الذي يجب علي فعله للتحقق من بطاقتي؟",
      //"text": "لإكمال عملية التحقق، يجب إدخال الرمز المكون من 4 أرقام الذي تتلقاه عبر مكالمة هاتفية. ولخطوتك الثانية، قد يطلب منك أيضا التحقق من الفيديو، حيث يمكنك تسجيل مقطع فيديو بعرض وجهك وتحميل فحص أو صورة من بطاقة الهوية / جواز السفر / رخصة القيادة. اعتمادا على التحقق، يمكن أن تكون هذه الخطوة إلزامية لأننا بحاجة إلى التأكد من أنك حامل البطاقة."
      "text": "لإكمال عملية التحقق ، ستحتاج إلى إدخال الرمز المكون من 4 أرقام الذي تتلقاه عبر مكالمة هاتفية. لخطوتك الثانية ، قد يُطلب منك أيضًا الخضوع للتحقق من الصورة ، والذي يتطلب منك تحميل صورة لجواز سفرك / بطاقة هويتك وصورة شخصية بهوية وقطعة من الورق تحتوي على النص المطلوب ، مما يؤكد عملية الشراء. قد نطلب بدلاً من ذلك التحقق من الفيديو ، حيث ستحتاج إلى تسجيل وجهك ، لنفترض: \"التحقق من Indacoin للتشفير\" إظهار بطاقة الهوية والبطاقة التي أجريت الدفع بواسطتها. عند عرض البطاقة ، اعرض آخر 4 أرقام فقط واسمها."
    },
    "question13": {
      "title": "ماذا لو لم أستطع تسجيل التحقق من الفيديو؟",
      "text": "يمكنك فتح نفس الرابط صفحة الطلب من الجهاز المحمول الخاص بك وتسجيلها من هناك. إذا كان لديك مشكلة، يمكنك أيضا إرسال الفيديو المطلوبة كمرفق عبر البريد الإلكتروني ل support@indacoin.com. تأكد من الإشارة إلى رقم طلب الصرف."
    },
    "question14": {
      "title": "لقد قدمت أمر ودخلت كل التفاصيل، لماذا لا يزال تجهيز؟",
      "text": "إذا كنت قد انتظرت أكثر من 2 دقيقة، ثم النظام لم تجعل من المعاملات. من المحتمل ألا تكون بطاقتك آمنة ثلاثية الأبعاد وقد تحتاج إلى إعادة المحاولة. تأكد من تأكيد عملية الشراء عبر رمز بين مع المصرف الذي تتعامل معه أو يمكنك محاولة الشراء بعملة مختلفة (ور / أوسد). خلاف ذلك، إذا كنت متأكدا من بطاقتك هي 3Ds وتغيير العملة لا يساعد ثم تحتاج إلى الاتصال بالمصرف الذي تتعامل معه، فإنها قد تمنع محاولات الشراء الخاصة بك."
    },
    "question15": {
      "title": "هل أحتاج إلى التحقق من كل مرة أجري فيها عملية شراء؟",
      "text": "تحتاج فقط للتحقق من بطاقتك مرة واحدة وجميع المعاملات التالية سوف تكون تلقائية. ومع ذلك، سنحتاج إلى التحقق إذا قررت استخدام بطاقة مصرفية أخرى."
    },
    "question16": {
      "title": "تم رفض طلبي من قبل البنك، فماذا أفعل؟",
      "text": "تحتاج إلى الاتصال بالمصرف الذي تتعامل معه واطلب سبب الرفض ، فربما يمكنه رفع التقييد بحيث يمكن إجراء عملية شراء."
    },
    "question17": {
      "title": "لماذا أحتاج إلى التحقق من بطاقتي المصرفية أو إرسال فيديو؟",
      "text": "تحميك عملية التحقق في حالة سرقة البطاقة أو القرصنة أو المحتال ؛ هذا يعمل بالطريقة المعاكسة في حالة محاولة شخص ما إجراء عملية شراء باستخدام البطاقة المصرفية لشخص آخر."
    },
    "question18": {
      "title": "كيف يمكنني الوثوق في أن بياناتي ومعلومات بطاقتي آمنة؟",
      "text": "نحن لا نشارك التفاصيل الخاصة بك مع أي طرف ثالث دون موافقتك. كما أننا لا نقبل سوى بطاقات 3D-سيكور، ولا نطلب أبدا أية بيانات حساسة مثل رمز كف أو رقم البطاقة الكاملة. لا تقدم سوى تفاصيل الدفع هذه من خلال محطات فيزا أو ماستركارد. نحن نجمع فقط المعلومات المطلوبة للتأكد من أنك مالك البطاقة."
    },
    "question19": {
      "title": "ما هي رسومك؟",
      "text": "تختلف الرسوم لدينا من الشراء إلى الشراء نظرًا لأنه يعتمد على العديد من العوامل ، ولهذا السبب أنشأنا آلة حاسبة تخبرك بالكمية الصحيحة من العملة التي ستحصل عليها ، وكل الرسوم متضمنة."
    },
    "question20": {
      "title": "كم سيتم فرض رسوم على المعاملة؟",
      "text": "سيتم محاسبتك فقط على المبلغ الذي تحدده أثناء طلبك في الحاسبة الخاصة بنا. يتم تضمين جميع الرسوم في هذا المبلغ."
    },
    "question21": {
      "title": "هل لديك أي خصومات؟",
      "text": "ونحن نفعل أحيانا الخصومات، يمكنك الحصول على مزيد من المعلومات من موظفينا الدعم."
    },
    "question22": { // TODO: link
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "ما مدى سرعة إرسال كريبتوكيرنسي؟",
      "text": "عادةً ما يستغرق الأمر 30 دقيقة تقريبًا بعد إجراء طلب أو التحقق الكامل (إذا كانت هذه هي المرة الأولى التي تستخدم فيها البطاقة على Indacoin)."
    },
    "question24": {
      "title": "ما هي الحد الأدنى والحد الأقصى للمشتريات؟",
      "text": "سيكون لديك حد أقصى قدره 200 دولار أمريكي للمعاملة الأولى ، 200 دولار إضافية للمعاملة الثانية المتوفرة بعد 4 أيام من الشراء الأولي ، 500 دولار بعد 7 أيام و 2000 دولار في 14 يومًا من أول عملية شراء. في غضون شهر من أول عملية شراء ، لا توجد حدود عند الدفع باستخدام بطاقتك. يرجى الأخذ في الاعتبار أن الحد الأدنى هو دائمًا 50 دولارًا."
    },
    "question25": {
      "title": "ما نوع البطاقات المقبولة؟",
      "text": "نحن نقبل فقط Visa و Mastercard مع 3D آمنة."
    },
    "question26": {
      "title": "ما هو 3D آمن؟",
      "text": "تتكون تقنية 3D Secure من البرامج التي تم التحقق منها بواسطة Visa و MasterCard SecureCode. بعد إدخال تفاصيل بطاقة الائتمان الخاصة بك في متجرنا عبر الإنترنت ، ستظهر نافذة جديدة تطلب رمز الأمان الشخصي الخاص بك. ستصادق مؤسستك المالية على المعاملة في غضون ثوانٍ ، فضلاً عن تأكيد أنك الشخص الذي يجري عملية الشراء."
    },
    "question27": {
      "title": "ما هي أنظمة الدفع الأخرى التي يمكنني استخدامها لشراء bitcoins؟",
      "text": "حاليا نحن نقبل فقط فيزا / ماستر دفعات بطاقة على منصة لدينا."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": { // TODO: translate
    "message": "يجب أن يكون المبلغ بين {{{limitsMin}}} {{{limitsCur}}} و {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "يجب أن يكون المبلغ من {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">يستخدم موقع الويب هذا ملفات تعريف الارتباط لضمان حصولك على أفضل تجربة على موقعنا.</span> {{{link}}}",
      "accept-button": "فهمتك",
      "link": "أعرف أكثر"
    },
    "success-video": {
      "text": "شكرا لكم! الآن بدأنا عملية التحقق ، والتي يمكن أن تستغرق بعض الوقت. عند اكتمال التحقق ، سيتم تسليم العملات التذكارية التشفير لك"
    },
    "accept-agreement": {
      //"text": "يرجى الإشارة إلى أنك قد قرأت ووافقت على اتفاقية المستخدم وسياسة مكافحة غسل الأموال"
      "text": "يرجى الإشارة إلى أنك قد قرأت ووافقت على شروط الاستخدام وسياسة الخصوصية"
    },
    "price-changed": {
      "header": "تم تغيير السعر",
      "agree": "ييوافق على",
      "cancel": "إلغاء"
    },
    "neoAttention": "بسبب ملامح عملة تشفير نيو، تتوفر الأعداد الصحيحة فقط للشراء",
    "login": {
      "floodDetect": "كشف الفيضان",
      "resetSuccess": "إعادة تعيين كلمة المرور النجاح. تفقد بريدك الالكتروني من فضلك",
      "wrongEmail": "لم يتم تسجيل هذا البريد الإلكتروني بعد",
      "unknownError": "خطأ في الخادم. يرجى إعادة المحاولة",
      "alreadyLoggedIn": "انت بالفعل داخل",
      "badLogin": "خطا في اسم الدخول او في كلمه المرور",
      "badCaptcha": "كلمة التحقق خاطئة",
      "googleAuthenticator": "الرجاء إدخال رمز Google Authenticator"
    },
    "registration": {
      "unknownError": "خطأ غير معروف",
      "floodDetect": "كشف الفيضان",
      "success": "سجل النجاح. تفقد بريدك الالكتروني من فضلك",
      "badLogin": "خطا في اسم الدخول او في كلمه المرور",
      "badCaptcha": "كلمة التحقق خاطئة",

      "empty": "ليست جميع الحقول مكتملة. يرجى ملء كل منهم",
      "repeatedRequest": "تم إرسال تأكيد التسجيل إلى عنوان بريدك الإلكتروني. يرجى التأكد من استلامها. فقط في حالة ، تحقق أيضًا من مجلد الرسائل غير المرغوب فيها",
      "badEmail": "لقد استخدمت عنوان البريد الإلكتروني غير الصحيح أو المؤقت",
      "blockedDomain": "يرجى تجربة عنوان بريد إلكتروني مختلف للتسجيل"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "لا توجد نتائج",
  "locale": {
    "title": "يرجى اختيار بلدك"
  }
};