export default {
  "lang": "en",
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "recordVideo": {
    "send": "Send",
    "stop": "Stop",
    "record": "Record",
    "button": "Record video"//,
    //"thank-message": "Thank you! Now we started the verification process, which can take some time. When verification is completed, the coins will be delivered to you"
  },
  "links": {
    "terms-of-use": {
      "text": "I agree to the {{{terms}}} and {{{policy}}}",
      "link-1": "Terms of Use",
      "link-2": "Privacy Policy"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "Waiting for your payment",
      "status-2": "Exchanging",
      "status-2-sub": "We are looking for the best rate for you",
      "status-3": "Sending to your wallet",
      "transaction-completed": "Congratulations, your transaction has been completed!",
      "rate": "Exchange rate 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "You have sent: ",
      "toAddress": "to {{{cryptoAdress}}}",
      "receiveText": "You will receive:"
    },
    "changeCrypto": {
      "send": "Send {{{amount}}} BTC to the address below",
      "confirmation": "After two bitcoin confirmations ≈ {{{amount}}} {{{shortName}}} will be sent to your address {{{wallet}}}...",
      "copy": "Copy address",
      "copied": "Copied to clipboard"
    },
    "order": "Order",
    "status": {
      "title": "Status",
      "completed": "The request has been processed"
    },
    "payAmount": {
      "title": "Paid"
    },
    "check": "Check the transaction status at blockexplorer.net",
    "resend-phone-code": "Haven't received a call?",
    "change-phone": "Change phone",
    "sms-code": "Please answer the phone and enter 4 digit code here",
    "registration": "Go to registration page",

    "internalAccount-1": "Please check your Indacoin wallet by",
    "internalAccount-link": "logging in",
    "internalAccount-2": "",
    "your-phone": "Your phone: {{{phone}}}",
    "many-requests": "Sorry. To many requests for change phone"
  },
  "app":{
    "detect_lang":{
      "title":"Detecting your language",
      "sub":"Please wait..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Buy and exchange {{{longName}}} instantly and 100 other cryptocurrencies at the best price in {{{country}}}",
    "title":"Indacoin: buy cryptocurrency with a credit or debit card",
    "description":"Buy and exchange any cryptocurreny instantly: [cryptocurrency], Bitcoin, Ethereum, Litecoin and 100 other digital currencies for EUR or USD",
    "description-bitcoin":"Buy and exchange any cryptocurrency instantly: Bitcoin, Ethereum, Litecoin, Ripple and 100 other digital currencies for EUR or USD",
    "description-ethereum":"Buy and exchange any cryptocurrency instantly: Ethereum, Bitcoin, Litecoin, Ripple and 100 other digital currencies for EUR or USD",
    "description-litecoin":"Buy and exchange any cryptocurrency instantly: Litecoin, Bitcoin, Ethereum, Ripple and 100 other digital currencies for EUR or USD",
    "description-ripple":"Buy and exchange any cryptocurrency instantly: Ripple, Bitcoin, Ethereum, Litecoin and 100 other digital currencies for EUR or USD",

    "description2":"Buy and exchange any cryptocurreny instantly: Bitcoin, Ethereum, Litecoin, Ripple, and 100 other digital currencies for EUR or USD",
    "description3":"Buy and exchange any cryptocurrency instantly: Bitcoin, Ethereum, Litecoin, Ripple and 100 other digital currencies with Visa & Mastercard",
    "buy": "Buy",
    "subTitle": "with a credit or debit card",
    "more": "In more than 100 countries including",
    //"header": "Exchange Bitcoin to {{{name}}}"
    //"header": "Exchange Bitcoin to <span class='sub'>{{{name}}}</span>"
    "header": "Exchange Instantly <span class='sub'>BTC</span> to <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "Buy Cryptocurrency Instantly",
    "header-alt": "Exchange Cryptocurrency Instantly",
    "sub": "Indacoin provides an easy way to buy bitcoins and more than 100 cryptocurrencies with Visa & Mastercard",
    "sub-alt": "Indacoin provides an easy way to exchange bitcoin to any of 100+ different cryptocurrencies",
    "observe": "Learn more about the cryptocurrencies available"
  },
  "navbar": {
    "main":"Home Page",
    "btc": "Buy Bitcoin",
    //"eth": "Buy Cryptocurrency",
    "eth": "Buy Altcoins",
    "affiliate": "Partnership",
    "news": "News",
    "faq": "FAQ",
    "login": "Sign in",
    "registration": "Sign up",
    "platform": "Products",

    "logout": "Logout",
    "buy-instantly": "Buy crypto instantly",
    "trading-platform": "Trading Platform",
    "mobile-wallet": "Mobile wallet",
    "payment-processing": "Payment processing API"
  },
  "currency-table": {
    "rank": "Rank",
    "coin": "Currency",
    "market": "Market cap",
    "price": "Price",
    "volume": "Volume, 24h",
    "change": "Change, 24h",
    "available-supply": "Available supply",
    "search-cryptocurrency": "Search for cryptocurrency",

    "change-1h": "1H Change",
    "change-24h": "1D Change",
    "change-7d": "1W Change"
  },
  "buy-blocks":{
    "give":"You pay",
    "get":"You receive"
  },
  "exchange-form": {
    "title": "Instantly purchase cryptocurrency",
    "submit": "Exchange",
    "give": "You pay",
    "get": "You receive",
    "search": "Search",
    "error-page": "We apologise, but the purchasing is currently unavailable"
  },
  "pagination": {
    "page": "Page",
    "next": "Forward",
    "prev": "Backward",
    "first": "Back to top"
  },
  "sidebar":{
    "accept":{
      "title":"We accept credit cards",
      "button":"Buy cryptocurrency"
    },
    "youtube":{
      "title":"About us",
      "play":"Start"
    },
    "orders":{
      "title": "Order management",
      "order": "Order #",
      "confirm": "Confirmation code",
      "check": "Verify order",
      "new": "Create new order",
      "or": "or"
    },
    "social":{
      "title":"Social"
    }
  },
  "wiki":{
    "etymology":"Etymology"
  },
  "loading": {
    "title": "Upload",
    "sub": "Please wait",
    "text": "Upload"
  },
  "error": {
    "title": "Error",
    "sub": "Oops! Something went wrong",
    "text": "An unknown error has occurred. We are already working on fixing it. Please come back later.",
    "home": "Home",
    "mainPageError1": "Unfortunately, information about available crypto-currencies is not available",
    "mainPageError2": "Try to visit the site a little later",
    "page-404": "We apologize, page not found",
    "page-error": "We apologize, error has occurred"
  },
  "features":{
    //"title":"What you can do on Indacoin",
    "title":"What you can<br><span class=\"offset\">do on <span class=\"blue\">Indacoin</span></span>",
    "column1":{
      "title":"Buy cryptocurrency",
      "text":"On Indacoin you can buy more than 100 different cryptocurrencies instantly with a credit or debit card"
    },
    "column2":{
      "title":"Learn about cryptocurrency",
      "text":"Our simple guides will help you better understand the fundamental principles of any altcoin"
    },
    "column3":{
      "title":"Make investment decisions",
      "text":"Powerful analytic tools let you capture the best momentum for entries and exits"
    },
    "column4":{
      "title":"Buy bitcoin without registering",
      "text":"Bitcoins will be sent to the address you indicated once the payment has been completed"
    },
    "column5":{
      "title":"Share your thoughts",
      "text":"Join the discussion about the latest crypto trends with other leading investors"
    },
    "column6":{
      "title":"Store all assets in one place",
      "text":"You can store and manage more than 100 digital currencies in one Indacoin Wallet"
    }
  },
  "mockup":{
    "title":"MOBILE WALLETS",
    "text":"Our smart and simple Indacoin wallet works on your Android or iPhone as well as your web browser. Now sending cryptocurrency to a friend’s phone is as easy as texting."
  },
  "footer":{
    "column1":{
      "title":"Buy",
      "link1":"Buy Bitcoin",
      "link2":"Buy Ethereum",
      "link3":"Buy Ripple",
      "link4":"Buy Bitcoin Cash",
      "link5":"Buy Litecoin",
      "link6":"Buy Dash"
    },
    "column2":{
      "title":"Information",
      "link1":"About us",
      "link2":"Questions",
      "link3":"Terms of use",
      "link4":"Instructions"
    },
    "column3":{
      "title":"Tools",
      "link1":"API",
      "link2":"Change region",
      "link3":"Mobile app"
    },
    "column4":{
      "title":"Contacts"
    }
  },
  "country":{
    "AF":{
      "name":"Afghanistan"
    },
    "AX":{
      "name":"Åland Islands"
    },
    "AL":{
      "name":"Albania"
    },
    "DZ":{
      "name":"Algeria"
    },
    "AS":{
      "name":"American Samoa"
    },
    "AD":{
      "name":"Andorra"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguilla"
    },
    "AQ":{
      "name":"Antarctica"
    },
    "AG":{
      "name":"Antigua and Barbuda"
    },
    "AR":{
      "name":"Argentina"
    },
    "AM":{
      "name":"Armenia"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Australia"
    },
    "AT":{
      "name":"Austria"
    },
    "AZ":{
      "name":"Azerbaijan"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Bahrain"
    },
    "BD":{
      "name":"Bangladesh"
    },
    "BB":{
      "name":"Barbados"
    },
    "BY":{
      "name":"Belarus"
    },
    "BE":{
      "name":"Belgium"
    },
    "BZ":{
      "name":"Belize"
    },
    "BJ":{
      "name":"Benin"
    },
    "BM":{
      "name":"Bermuda"
    },
    "BT":{
      "name":"Bhutan"
    },
    "BO":{
      "name":"Bolivia"
    },
    "BA":{
      "name":"Bosnia and Herzegovina"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Bouvet Island"
    },
    "BR":{
      "name":"Brazil"
    },
    "IO":{
      "name":"British Indian Ocean Territory"
    },
    "BN":{
      "name":"Brunei Darussalam"
    },
    "BG":{
      "name":"Bulgaria"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Cambodia"
    },
    "CM":{
      "name":"Cameroon"
    },
    "CA":{
      "name":"Canada"
    },
    "CV":{
      "name":"Cape Verde"
    },
    "KY":{
      "name":"Cayman Islands"
    },
    "CF":{
      "name":"Central African Republic"
    },
    "TD":{
      "name":"Chad"
    },
    "CL":{
      "name":"Chile"
    },
    "CN":{
      "name":"China"
    },
    "CX":{
      "name":"Christmas Island"
    },
    "CC":{
      "name":"Cocos (Keeling) Islands"
    },
    "CO":{
      "name":"Colombia"
    },
    "KM":{
      "name":"Comoros"
    },
    "CG":{
      "name":"Congo"
    },
    "CD":{
      "name":"Congo, The Democratic Republic of the"
    },
    "CK":{
      "name":"Cook Islands"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Cote D'Ivoire"
    },
    "HR":{
      "name":"Croatia"
    },
    "CU":{
      "name":"Cuba"
    },
    "CY":{
      "name":"Cyprus"
    },
    "CZ":{
      "name":"Czech Republic"
    },
    "DK":{
      "name":"Denmark"
    },
    "DJ":{
      "name":"Djibouti"
    },
    "DM":{
      "name":"Dominica"
    },
    "DO":{
      "name":"Dominican Republic"
    },
    "EC":{
      "name":"Ecuador"
    },
    "EG":{
      "name":"Egypt"
    },
    "SV":{
      "name":"El Salvador"
    },
    "GQ":{
      "name":"Equatorial Guinea"
    },
    "ER":{
      "name":"Eritrea"
    },
    "EE":{
      "name":"Estonia"
    },
    "ET":{
      "name":"Ethiopia"
    },
    "FK":{
      "name":"Falkland Islands (Malvinas)"
    },
    "FO":{
      "name":"Faroe Islands"
    },
    "FJ":{
      "name":"Fiji"
    },
    "FI":{
      "name":"Finland"
    },
    "FR":{
      "name":"France"
    },
    "GF":{
      "name":"French Guiana"
    },
    "PF":{
      "name":"French Polynesia"
    },
    "TF":{
      "name":"French Southern Territories"
    },
    "GA":{
      "name":"Gabon"
    },
    "GM":{
      "name":"Gambia"
    },
    "GE":{
      "name":"Georgia"
    },
    "DE":{
      "name":"Germany"
    },
    "GH":{
      "name":"Ghana"
    },
    "GI":{
      "name":"Gibraltar"
    },
    "GR":{
      "name":"Greece"
    },
    "GL":{
      "name":"Greenland"
    },
    "GD":{
      "name":"Grenada"
    },
    "GP":{
      "name":"Guadeloupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernsey"
    },
    "GN":{
      "name":"Guinea"
    },
    "GW":{
      "name":"Guinea-Bissau"
    },
    "GY":{
      "name":"Guyana"
    },
    "HT":{
      "name":"Haiti"
    },
    "HM":{
      "name":"Heard Island and McDonald Islands"
    },
    "VA":{
      "name":"Holy See (Vatican City State)"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hong Kong"
    },
    "HU":{
      "name":"Hungary"
    },
    "IS":{
      "name":"Iceland"
    },
    "IN":{
      "name":"India"
    },
    "ID":{
      "name":"Indonesia"
    },
    "IR":{
      "name":"Iran, Islamic Republic of"
    },
    "IQ":{
      "name":"Iraq"
    },
    "IE":{
      "name":"Ireland"
    },
    "IM":{
      "name":"Isle of Man"
    },
    "IL":{
      "name":"Israel"
    },
    "IT":{
      "name":"Italy"
    },
    "JM":{
      "name":"Jamaica"
    },
    "JP":{
      "name":"Japan"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Jordan"
    },
    "KZ":{
      "name":"Kazakhstan"
    },
    "KE":{
      "name":"Kenya"
    },
    "KI":{
      "name":"Kiribati"
    },
    "KP":{
      "name":"Korea, Democratic People's Republic of"
    },
    "KR":{
      "name":"Korea, Republic of"
    },
    "KW":{
      "name":"Kuwait"
    },
    "KG":{
      "name":"Kyrgyzstan"
    },
    "LA":{
      "name":"Lao People's Democratic Republic"
    },
    "LV":{
      "name":"Latvia"
    },
    "LB":{
      "name":"Lebanon"
    },
    "LS":{
      "name":"Lesotho"
    },
    "LR":{
      "name":"Liberia"
    },
    "LY":{
      "name":"Libyan Arab Jamahiriya"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Lithuania"
    },
    "LU":{
      "name":"Luxembourg"
    },
    "ME":{
      "name":"Montenegro"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Macedonia, Former Yugoslav Republic of"
    },
    "MG":{
      "name":"Madagascar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malaysia"
    },
    "MV":{
      "name":"Maldives"
    },
    "ML":{
      "name":"Mali"
    },
    "MT":{
      "name":"Malta"
    },
    "MH":{
      "name":"Marshall Islands"
    },
    "MQ":{
      "name":"Martinique"
    },
    "MR":{
      "name":"Mauritania"
    },
    "MU":{
      "name":"Mauritius"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"Mexico"
    },
    "FM":{
      "name":"Micronesia, Federated States of"
    },
    "MD":{
      "name":"Moldova, Republic of"
    },
    "MC":{
      "name":"Monaco"
    },
    "MN":{
      "name":"Mongolia"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Morocco"
    },
    "MZ":{
      "name":"Mozambique"
    },
    "MM":{
      "name":"Myanmar"
    },
    "NA":{
      "name":"Namibia"
    },
    "NR":{
      "name":"Nauru"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Netherlands"
    },
    "AN":{
      "name":"Netherlands Antilles"
    },
    "NC":{
      "name":"New Caledonia"
    },
    "NZ":{
      "name":"New Zealand"
    },
    "NI":{
      "name":"Nicaragua"
    },
    "NE":{
      "name":"Niger"
    },
    "NG":{
      "name":"Nigeria"
    },
    "NU":{
      "name":"Niue"
    },
    "NF":{
      "name":"Norfolk Island"
    },
    "MP":{
      "name":"Northern Mariana Islands"
    },
    "NO":{
      "name":"Norway"
    },
    "OM":{
      "name":"Oman"
    },
    "PK":{
      "name":"Pakistan"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Palestinian Territory, Occupied"
    },
    "PA":{
      "name":"Panama"
    },
    "PG":{
      "name":"Papua New Guinea"
    },
    "PY":{
      "name":"Paraguay"
    },
    "PE":{
      "name":"Peru"
    },
    "PH":{
      "name":"Philippines"
    },
    "PN":{
      "name":"Pitcairn"
    },
    "PL":{
      "name":"Poland"
    },
    "PT":{
      "name":"Portugal"
    },
    "PR":{
      "name":"Puerto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"Reunion"
    },
    "RO":{
      "name":"Romania"
    },
    "RU":{
      "name":"Russian Federation"
    },
    "RW":{
      "name":"Rwanda"
    },
    "SH":{
      "name":"Saint Helena"
    },
    "KN":{
      "name":"Saint Kitts and Nevis"
    },
    "LC":{
      "name":"Saint Lucia"
    },
    "PM":{
      "name":"Saint Pierre and Miquelon"
    },
    "VC":{
      "name":"Saint Vincent and the Grenadines"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"San Marino"
    },
    "ST":{
      "name":"Sao Tome and Principe"
    },
    "SA":{
      "name":"Saudi Arabia"
    },
    "SN":{
      "name":"Senegal"
    },
    "CS":{
      "name":"Serbia and Montenegro"
    },
    "SC":{
      "name":"Seychelles"
    },
    "SL":{
      "name":"Sierra Leone"
    },
    "SG":{
      "name":"Singapore"
    },
    "SK":{
      "name":"Slovakia"
    },
    "SI":{
      "name":"Slovenia"
    },
    "SB":{
      "name":"Solomon Islands"
    },
    "SO":{
      "name":"Somalia"
    },
    "ZA":{
      "name":"South Africa"
    },
    "GS":{
      "name":"South Georgia and the South Sandwich Islands"
    },
    "ES":{
      "name":"Spain"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Sudan"
    },
    "SR":{
      "name":"Suriname"
    },
    "SJ":{
      "name":"Svalbard and Jan Mayen"
    },
    "SZ":{
      "name":"Swaziland"
    },
    "SE":{
      "name":"Sweden"
    },
    "CH":{
      "name":"Switzerland"
    },
    "SY":{
      "name":"Syrian Arab Republic"
    },
    "TW":{
      "name":"Taiwan, Province of China"
    },
    "TJ":{
      "name":"Tajikistan"
    },
    "TZ":{
      "name":"Tanzania, United Republic of"
    },
    "TH":{
      "name":"Thailand"
    },
    "TL":{
      "name":"Timor-Leste"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad and Tobago"
    },
    "TN":{
      "name":"Tunisia"
    },
    "TR":{
      "name":"Turkey"
    },
    "TM":{
      "name":"Turkmenistan"
    },
    "TC":{
      "name":"Turks and Caicos Islands"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Uganda"
    },
    "UA":{
      "name":"Ukraine"
    },
    "AE":{
      "name":"United Arab Emirates"
    },
    "GB":{
      "name":"United Kingdom"
    },
    "US":{
      "name":"United States"
    },
    "UM":{
      "name":"United States Minor Outlying Islands"
    },
    "UY":{
      "name":"Uruguay"
    },
    "UZ":{
      "name":"Uzbekistan"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Viet Nam"
    },
    "VG":{
      "name":"Virgin Islands, British"
    },
    "VI":{
      "name":"Virgin Islands, U.S."
    },
    "WF":{
      "name":"Wallis and Futuna"
    },
    "EH":{
      "name":"Western Sahara"
    },
    "YE":{
      "name":"Yemen"
    },
    "ZM":{
      "name":"Zambia"
    },
    "ZW":{
      "name":"Zimbabwe"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Buy",
  "sell":"Sell",
  "weWillCallYou":"If it is your first time using this card, then you will have to enter two secret codes: one will be in your bank statement and the other will be given to you over the phone.",
  "exchangerStatuses":{
    "TimeOut": "The request has timed out",
    "Error": "An error has occurred. Please contact our support team at support@indacoin.com",
    "WaitingForAccountCreation": "Waiting for Indacoin account creation (we have sent to you a registration mail)",
    "CashinWaiting": "Your payment is being processed",
    "BillWaiting": "Waiting for the customer to pay the invoice",
    "Processing": "The request is being processed",
    "MoneySend": "The funds have been sent",
    "Completed": "The request has been processed",
    "Verifying": "Verification",
    "Declined": "Declined",
    "cardDeclinedNoFull3ds": "Your card has been declined because 3D-Secure is not enabled on your card. Please call your bank’s customer service department",
    "cardDeclined": "Your card has been declined. If you have any questions, please contact our support team at support@indacoin.com",
    //"Non3DS": "Your payment is being processed. This can take up to 3 minutes. If you have been waiting for more than 3 minutes and you still see this status, then unfortunately your payment has been declined. Check with your bank whether your card has 3D-Secure enabled (Verified by Visa or Mastercard SecureCode), which involves your entering of a secret code when paying online. The code is usually sent to your phone."
    "Non3DS": "Unfortunately your payment has been declined. Check with your bank whether your card has 3D-Secure enabled (Verified by Visa or Mastercard SecureCode), which involves your entering of a secret code when paying online. The code is usually sent to your phone"
  },
  "exchanger_fields_ok":"Click to buy bitcoins",
  "exchanger_fields_error":"An error occurred while filling out the fields. Please verify all fields",
  "bankRejected":"The transaction was declined by your bank. Please contact your bank and retry the card payment",
  "wrongAuthCode":"You have entered an incorrect authorization code. Please contact our support team at support@indacoin.com",
  "wrongSMSAuthCode":"The phone code you have entered is invalid",
  "getCouponInfo":{
    "indo":"This coupon is invalid",
    "exetuted":"This coupon has already been used",
    "freeRate":"The discount coupon has been activated"
  },
  "ip":"Your IP has been temporarily blocked",
  "priceChanged":"The price has changed. OK? You will receive ",
  "exchange_go":"– Exchange",
  "exchange_buy_btc":"– Buy bitcoins",
  "month":"Jan Feb Mar Apr May Jun Jul Aug Sep Oct Nov Dec",
  "discount":"Discount",
  "cash":{
    "shouldRedirect": "1You will be redirected to partner in {{{seconds}}} seconds",
    "equivalent":" equal to ",
    "card3DS":"We accept only cards with 3D-Secure enabled (Verified by Visa or MasterCard SecureCode)",
    "cashIn":{
      "1": "Bank cards (USD)",
      "2": "e-Payment",
      "3": "QIWI",
      "4": "Bashcomsnabbank",
      "5": "Novoplat terminals",
      "6": "Elecsnet terminals",
      "7": "Bank cards (USD)",
      "8": "Alpha Click",
      "9": "International wire transfer",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Money",
      "21": "Elecsnet",
      "22": "Kassira.net",
      "23": "Mobile Element",
      "24": "Svyaznoy",
      "25": "Euroset",
      "26": "PerfectMoney",
      "27": "Indacoin — internal",
      "28":	"CouponBonus",
      "29": "Yandex.Money",
      "30": "OKPay",
      "31": "Partner Program",
      "33": "Payza",
      "35": "BTC-E code",
      "36": "Bank cards (USD)",
      "37": "Bank cards (USD)",
      "39": "International wire transfer",
      "40": "Yandex.Money",
      "42": "QIWI(Manual)",
      "43": "UnionPay",
      "44": "QIWI(Automatic)",
      "45": "Pay by phone",
      "49": "LibrexCoin",
      "50": "Bank cards (EURO)",
      "51": "QIWI(Automatic)",
      "52": "Pay by phone",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Please enter correct crypto address",
      "country": "Invalid country",
      "alfaclick": "Invalid login",
      "pan": "Invalid card number",
      "cardholdername": "Invalid data",
      "fio": "Invalid data",
      "bday": "Invalid date of birth",
      "city": "Invalid data",
      "address": "Invalid address",
      "ypurseid": "Invalid wallet number",
      "wpurseid": "Invalid wallet number",
      "pmpurseid": "Invalid account number",
      "payeer": "Invalid account number",
      "okpayid": "Invalid wallet number",
      "purse": "Invalid wallet number",
      "phone": "Invalid phone number",
      "btcaddress": "Invalid address",
      "ltcaddress": "Invalid address",
      "lxcaddress": "Invalid address",
      "ethaddress": "Invalid address",
      "properties": "Invalid details",
      "email": "Invalid email address",
      "card_number": "This card is not supported. Please check the number",
      "inc_card_number": "Invalid card number",
      "inn": "Invalid number",
      "poms": "Invalid number",
      "snils": "Invalid number",
      "cc_expr": "Invalid date",
      "cc_name": "Invalid name",
      "cc_cvv": "Invalid CVV"
    }
  },
  "change": {
    //"wallet": "I want to create a crypto-wallet on Indacoin",
    "wallet": "Create new wallet / I already have Indacoin account",
    //"accept": "I agree to the User Agreement and AML Policy",
    "accept": "I agree to the Terms of Use and Privacy Policy",
    "coupone": "I have a discount coupon",
    "watchVideo": "Watch the video",
    "seeInstructions": "Help",
    "step1": {
      "title": "Create order",
      "header": "Enter your payment details",
      "subheader": "Please let us know what you wish to purchase"
    },
    "step2": {
      "title": "Payment details",
      "header": "Enter your credit card details",
      "subheader": "We accept only Visa and Mastercard",
      "card": {
        "header": "How can I find this code?",
        "text": "The CVV is located on the back of your card. This is a 3-digit code"
      }
    },
    "step3": {
      "title": "Contact details",
      "header": "Please enter your contact details",
      "subheader": "We want to know our customers better"
    },
    "step4": {
      "title": "Verification",
      "header": "Please enter your code from the payment page",
      "subheader": "Your payment details"
    },
    "step5": {
      "title": "Order information"
    },
    "step6": {
      "title": "Record video",
      "header": "Please show your passport and your face",
      "description": "Please show us that we can trust you",
      "startRecord": "Please click the Start button to begin recording",
      "stopRecord": "Please click the Stop button to stop recording"
    },
    
    "nextButton": "Next",
    "previousButton": "Previous",
    "error": "Server error connection. Please resend data",
    "passVideoVerification": "Pass verification by video",
    "passKYCVerification": "KYC verification",
    "soccer-legends": "I agree that my personal data be transferred and processed to Soccer Legends Limited company",
    "withdrawalAmount": "Amount to be credited to the account",
    "inc": "In More Than 100 Countries including "
  },
  "form": {
    "tag": "{{{currency}}} tag",
    "loading": "Loading...Please wait",
    "youGive": "You have",
    "youTake": "You get",
    "email": "Email",
    "password": "Password",
    //"cryptoAdress": "{{{name}}} Wallet Address",
    "cryptoAdress": "Crypto Wallet Address",
    "externalTransaction": "Transaction ID",
    "coupone": "I have a discount coupone",
    "couponeCode": "Coupone code",
    "cardNumber": "Card number",
    "month": "MM",
    "year": "YY",
    "name": "Name",
    "fullName": "Full name (Latin alphabet)",
    "mobilePhone": "Mobile phone",
    "birth": "Your date of birth in format MM.DD.YYYY",
    "videoVerification": "Video verification",
    "verification": "Verification",
    "status": "Check status",
    "login": {
      "captcha": "Captcha",
      "reset": "Reset",
      "forgot": "Forgot Password",
      "signIn": "Sign in"
    },
    "registration": {
        "create": "Create"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Simple verification",
      "title-2": "More than 100 altcoins available",
      "title-3": "No hidden charges",
      "title-4": "No registration"
    },
    "title": "We are working in more than 100 countries",
    "subtitle": "including {{{country}}}",
    "shortTitle": "We are working in more than 100 countries",
    "text1": "Simple verification",
    "popupText1": "First-time buyers just have to verify the phone number",
    "text2": "100 altcoins are supported",
    "popupText2": "You can purchase more than 100 most promising cryptocurrencies with a credit/debit card",
    "text3": "No hidden charges",
    "popupText3": "You will get exactly the same amount of altcoins, that was shown before the deal",
    "text4": "No registration",
    "popupText4": "You don’t need to sign up, сryptocurrency will be sent to the address you indicated"
  },
  "buying-segment": {
      "title": "More than 500 000 clients",
      "sub-title": "have been using Indacoin since 2015"
  },
  "phone-segment": {
      "title": "Download our application",
      "text1": "Purchase and store more than 100 cryptocurrencies in one wallet",
      "text2": "Track the changes of your investment portfolio",
      "text3": "Sending cryptocurrency to your friend became as easy as sending a message"
  },
  "social-segment": {
    "buttonText": "Read on Facebook",
    "on": "on Facebook",
    "facebook-review-1": "buena pagina de bitcoins, proceso rapido y sencillo, se los recomiendo",
    "facebook-review-2": "Brilliant service and convenient to use, the folk there are friendly and very helpful. Although it is a digital transaction, there is a person at the other side of it (which can be a MASSIVE help) and reassuring for sure, 5star service which I'll use again",
    "facebook-review-3": "As I'm only starting to taste everything that surrounds the cryptocurrency world, making an exchange at their website is more than simple. and their support in case ur getting stuck or just reaching out for help is far more than amazing.",
    "facebook-review-4": "Very quick payment, great support and great exchange rates.",
    "facebook-review-5": "I decided to buy 100 euros worth of bitcoins using this service, and it was easy to proceed with the payment. Although i had some troubles and questions, I was almost instantly helped by their support team. It made me feel a lot more comfortable buying Bitcoin with Indacoin. And am planning to buy more in the future.",
    "facebook-review-6": "I just used Indacoin to make a transaction for Bitcoin purchase. The transaction went smooth, followed by a verification call. Its very easy to use and a user friendly portal for buying bit coins. I would recommend this platform for purchasing bitcoins!!",
    "facebook-review-7": "It is difficult to find an easy credit card transaction facility to buy cryptographic currency and Indcoin treated me super well, patience with users from other countries who do not understand their language, online chat of readiness. Indcoin Thank you !!!",
    "facebook-review-8": "Fast & easy. Online support was very prompt and helpful. ProTip: Using the app reduces charges by 9%.",
    "facebook-review-9": "This is my second time using Indacoin - Great service, speedy transaction  brilliant customer support. I will definitely use again.",
    "facebook-review-10": "Extremely great service and communication , step by step help with the live chat and totally reliable, they even called me with out me asking and explained to me how to do verification for transaction , easiest way to buy Bitcoin with no problems at all , thank you Indacoin will be doing lots more transactions with you .",
    "facebook-review-11": "Great service, fast process. Helpful customer service. Fast responds. Simple way to buy crypto currency. Among others service, honestly this is the only service I use to buy crypto currency. I trust them to process my buying. Recommended to everybody who deal with buying crypto currency. Last but not least, only one word for Indacoin: Excellent!!!",
    //"facebook-review-12": "IndaCoin has Super easy, reliable and quick service. They were also extremely fast in answering all of my questions and helping me out with my transfer. Would highly recommend their services to everybody! 10/10!",
    "facebook-review-12": "I bought bitcoins for 200 Euros and the website is legit.Although it took sometime for the transaction to take place, the customer service is excellent. I've reached out to their customer support and they immediately sorted my issue. Definitely recommend this site to everyone. sometimes due to high traffic the transaction time ranges between 8-10 hrs. Don't panic if you don't receive your coins instantly sometimes it will take time but for sure you will get your coins.",
    "facebook-review-13": "Great job Indacoin. Had the best experience so far in purchase coins with my credit card with Indacoin.. The consultant Andrew did a great customer service, and i was impressed how the transfer too less than 3 mins ... Will continue using them :)",
    "facebook-review-14": "Indacoin is an easy and user friendly interface that buyers can easier buy bitcoins at there platform. It is easy to with step by step helps from the Consultant Agents and they are really willing to help you to solve your issue immedtely. I am defintely going to refer people to Indacoin and they constantly update me on my situations and they are willing to go extra mile to solve your probems. Thank You Indacoin!",
    "facebook-review-15": "The easiest and quickest way to purchase cryptocurrency that I've found in alot of searching. Great job guys, thanks!",
    "facebook-review-16": "Reallly easy way to buy coins online. Great customer service too. Its awesome they have alt coins to buy direct as well. Takes the complications out of the exchanges. Really recommend this to all after my first positive experience.",
    "facebook-review-17": "Great service. User Friendly and Easy to buy various coins/tokens.",
    "facebook-review-18": "This is one of the best platform to exchange BTC. I did several echange and was fast.",
    "facebook-review-19": "Great service, user-friendly, easy to buy various cryptocurrency and ERC20 tokens with your credit or debit card. Very fast and also secure! The service and platform I've been looking for! Totally recommend using Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "More than {{{count}}} different cryptocurrencies are available for purchase on {{{name}}}"
      //"title": "More than 100 different cryptocurrencies are available for purchase on Indacoin"
  },
  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",

  "mobileMockups": {
    "title": "Indacoin in your phone",
    "text": "Our smart-wallet Indacoin is available for users of Android, iOS and in every internet browser. Sending cryptocurrency to your friend became as easy as sending a message"
  },

  "news-segment": {
    "no-news": "There is no news for your language",
    "news-error": "We apologise, the news is currently unavailable",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },

  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "This cryptocurrency is currently not available but will be back soon",
    "unknown": "Unknown server error",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "Limit has exceeded",
    "phone": "Phone is not supported"
  },
  "tags": {
    "title-btc": "Exchange Bitcoin BTC to {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Buy [cryptocurrency] With a Credit Or Debit Card in [country] - Indacoin",
    "title2": "Buy Bitcoin With a Credit Or Debit Card Worldwide - Indacoin",
    "title3": "Buy {{{cryptocurrency}}} With a Credit Or Debit Card in {{{country}}} - Indacoin",
    "title4": "Buy Cryptocurrency with Credit or Debit card in {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Indacoin Affiliate Program",
      "news": "Indacoin Latest News",
      "help": "CryptoCurrency Exchange F.A.Q. - Indacoin",
      "api": "Cryptocurrency Exchange API - Indacoin",
      "terms-of-use": "Terms Of Use & AML Policy - Indacoin",
      "login": "Sign in - Indacoin",
      "register": "Sign up - Indacoin",
      "locale": "Change region - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) price charts & trade volume - Indacoin",
      "notFoundCurrency": "Price charts & trade volume - Indacoin"
    },
    "descriptions": {
      "affiliate": "Indacoin Affiliate Program allows you to earn up to 3% of each purchase with a credit/debit card. Remuneration will be sent to your Indacoin bitcoin wallet",
      "news": "Visit our official news blog to stay in touch with the latest news on cryptocurrency industry! A plenty of useful information and comprehensive comments from Indacoin experts in one place!",
      "help": "Find the answers to all your questions on how to buy, sell and exchange cryptocurrency on Indacoin.",
      "api": "Our API provides buying and exchanging any cryptocurrency instantly with Visa & Mastercard. Check Integration & Examples.",
      "terms-of-use": "General information. Terms of use. Services provided. Transfer of funds. Transfer of rights. Commissions. AML Policy.",
      "login": "Login or create new account",
      "register": "Create new account",
      "locale": "Choose your region, and select this and we will remember your region the next time you visit Indacoin",
      "currency": "Current prices for {{{fullCurrency}}} to USD with trading volume and historical cryptocurrency information",
      "notFoundCurrency": "Current prices with trading volume and historical cryptocurrency information"
    }
  },
  "usAlert": "We are sorry, but Indacoin doesn't operate in the US",
  "wrongPhoneCode": "You have entered a wrong code from your phone",
  "skip": "Skip",
  "minSum": "Minimum amount to buy",
  "currency": {
    "no-currency": "For a given cryptocurrency, the graph is not available",
    "presented": {
      "header": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world"
    },
    "difference": {
      "title": "How is Bitcoin different from regular money?",
      "header1": "Limited emission",
      "text1": "The total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive.",
      "header2": "Worldwide currency",
      "text2": "No government has control over Bitcoin. It’s based on mathematics only",
      "header3": "Anonymity",
      "text3": "You can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds.",
      "header4": "Speed",
      "text4": "Your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
      "header5": "Transparency",
      "text5": "All Bitcoin transactions can be seen at BlockChain."
    },
    "motivation": {
      "title": "Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx"
    },
    "questions": {
      "header1": "What is a digital currency or cryptocurrency?",
      "text1": "Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems",
      "header2": "How does Bitcoin transaction occur?",
      "text2": "Each transaction has 3 dimensions: Input – the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount – the number of “coins” used. Output – the address of the recipient of the money.",
      "header3": "Where did Bitcoin come from?",
      "text3": "Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
      "header4": "How does Bitcoin work?",
      "text4": "Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
      "header5": "What is a Bitcoin wallet?",
      "text5": "A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: ",
    },
    "history": {
      "title": "The brief history of Bitcoin",
      "text1": "According to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin",
      "text2": "First transaction made in Bitcoin. Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1 USD for 1.306.03 BTC",
      "text3": "25% of all Bitcoins are already generated",
      "text4": "Dell and Windows begin to accept Bitcoin",
      "text5": {
        "part1": "BTC price increases from 1,402 USD to 19,209 USD in a few months.",
        "part2": "Bitcoin rate reaches 20,000 USD per coin on the 18th of December 2017"
      },
      "text6": "Тhe registration of the bitcoin.org",
      "text7": {
        "part1": "A truly historical moment, when 10,000 BTC is used to indirectly buy 2 pizzas. Today you can buy 2,500 pepperoni pizzas for the same amount.",
        "part2": "Bitcoin price shoots up from 0.008 USD to 0.08 USD for 1 BTC in just 5 days.",
        "part3": "First mobile transaction occurs"
      },
      "text8": {
        "part1": "U.S. Treasury classifies BTC as a convertible decentralized virtual currency.",
        "part2": "The first Bitcoin ATM found in San Diego, California.",
        "part3": "Bitcoin price doubles and reaches 503.10 USD. It grows to 1,000 USD the same month"
      },
      "text9": "U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”"
    },
    "graph": {
      "buy-button": "Buy"
    },
    "ethereum": {
      "presented": {
        "header": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum."
      },
      "questions": {
        "header1": "What is Ethereum?",
        "text1": "Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
        "header2": "Where did it come from?",
        "text2": "Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
        "header3": "What is ICO?",
        "text3": "Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
        "header4": "What is Ether?",
        "text4": "Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
        "header5": "What’s so special about “smart contracts” and what’s its difference from usual contracts?",
        "text5": "Each transaction has 3 dimensions: Input – the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount – the number of “coins” used. The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
        "header6": "How is Ethereum different from Bitcoin?",
        "text6": "Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens – Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
        "header7": "What are the most popular Ethereum wallets?",
        "text7": "An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets: "
      },
      "history": {
        "title": "The brief history of Ethereum",
        "text1": "Crowdsale is initiated to gather funds for creating Ethereum",
        "text2": "The Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history",
        "text3": "Ethereum gains 50% market share in initial coin offering",
        "text4": "Ethereum exchange rate reaches 400 USD (5,000% rise since January)",
        "text5": "The idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine",
        "text6": "30th of July platform goes online",
        "text7": "The DAO is hacked, Ethereum value drops from 21.5 USD to 8 USD. This leads to Ethereum being forked in two blockchains in 2017",
        "text8": "Enterprise Ethereum Alliance is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc",
        "text9": {
          "part1": "All cryptocurrencies drop after China bans ICO, but quickly recover afterward.",
          "part2": "Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban"
        }
      }
    }
  },
  "affiliate": {
    "contacts": {
      "text": "If you are a journalist or a blogger who is willing to review Indacoin, please contact {{{email}}} to get the most recent information about our company and products."
    },
    "promo": {
      "offer-block": {
        "name": "Your name",
        "phone": "Your phone",
        "telegram": "Your Telegram",
        "email": "Your email address*",
        "website": "Website",
        "project-kind": "What kind of project do you have?",
        "message": "Please enter your message",
        "mail-message": "Field email* is required",
          "kinds": {
            "default": "Project kind",
            "ico": "ICO",
            "exchange": "Exchange",
            "wallet": "Wallet",
            "crypto-fund": "Crypto Fund",
            "mobile-app": "Mobile App",
            "other": "Other"
          },
        "open": "Fill out the form",
        "close": "Close form",
        "text": "Are you ready to let your clients buy crypto with a credit/debit card? Please fill out the form and we'll get in touch asap",
        "button": "Send",
        "mail-success": "Thank you. We will answer you"
      },
      "api": {
        "title": "API integration",
        "text": "Using our API, you can let your customers purchase cryptocurrencies instantly from your site. Our anti-fraud platform integrated on your web page or mobile App can also be used as a tool that will let you monitor and filter the payments made on your platform."
      },
      "affiliate": {
        "title": "Affiliate Programme",
        "text": "Get rewarded for advising Indacoin to other customers. You will be earning up to 3% from the purchases of the referrals.",
        "block-1": {
          "title": "How can I join the Affiliate Program of Indacoin?",
          "text": "Feel free to {{{link}}} on our site.",
          "link-text": "sign up"
        },
        "block-2": {
          "title": "How will I be able to withdraw the funds collected in my account?",
          "text": "You can buy BTC/ETH/other altcoins on Indacoin without any fee or receive them to your bank card."
        },
        "block-3": {
          "title": "What is the advantage of your Affiliate Program?",
          "text": "On our platform, the customers have the opportunity to buy more than 100 different coins using credit/debit cards without registration. Considering the fact that the purchases proceed almost immediately, the partners can get their profit instantly."
        },
        "block-4": {
          "title": "Can I see the example of the referral link?",
          "text": "Of course, this is how it looks like: ({{{link}}})"
        }
      },
      "more": "Find out more",
      "close": "Close"
    },
    "achievements": {
      "partnership-title": "Partnership",
      "partnership-text": "There are many ways of collaborating with Indacoin. The Indacoin Partnership Program is for crypto-related companies that are willing to connect the world of cryptocurrencies with traditional fiat money system to increase engagement in the crypto industry. With Indacoin, you will be able to provide your clients with the customized experience of the purchasing crypto with the credit/debit cards.",
      "block-1": {
        "header": "Trusted",
        "text": "We have been working in the crypto market since 2013 and have partnered with many respected companies that lead the industry. More than 500 000 clients from more than 190 countries have used our service to purchase cryptocurrencies."
      },
      "block-2": {
        "header": "Secure",
        "text": "We created an innovative and highly efficient anti-fraud platform with a core technology developed especially for the detection and prevention of fraudulent activities for crypto projects."
      },
      "block-3": {
        "header": "Ready&#8209;to&#8209;use solution",
        "text": "Our product is being used already by the ICOs, Cryptocurrency Exchanges, Blockchain Platforms and Crypto Funds."
      },

      "header": "Earn money with us on the breakthrough",
      "subheader": "Blockchain technologies",
      "text1": "{{{company}}} is a global platform, that lets people instantly purchase Bitcoin, Ethereum, Ripple, Waves and 100 other different cryptocurrencies with a credit or debit card",
      "text2": "The service has been working since 2013 almost all over the world including but not limited to the:",
      "text3": "UK, Canada, Germany, France, Russia, Ukraine, Spain, Italy, Poland, Turkey, Australia, Philippines, Indonesia, India, Belarus, Brazil, Egypt, Saudi Arabia, UAE, Nigeria and others"
    },
    "clients": {
      "header": "More than 500 000 clients",
      "subheader": "since 2013"
    },
    "benefits": {
      "header": "Our key benefits for webmasters:",
      "text1": "Unique service that lets clients buy and send bitcoins without registration. So customers that come to Indacoin following the referral link will have to just enter the card details and wallet address, where the crypto should be sent. Therefore potential customers will make the purchases immediately and our partners will get profit.",
      "text2": "Up to 3% of each transaction is earned by our partners. Furthermore, webmasters will get 3% of all future purchases of the referral.",
      "text3": "Bitcoin and banking cards could be used to withdraw the income."
    },
    "accounts": {
      "header": "Join us right now",
      "emailPlaceholder": "Enter your email address",
      "captchaPlaceholder": "Captcha",
      "button": "Get started"
    }
  },
  "faq": {
    "title": "Questions and answers",
    "header1": "General questions",
    "header2": "Cryptocurrency",
    "header3": "Verification",
    "header4": "Commissions",
    "header5": "Purchase",
    "question1": {
      "title": "What is Indacoin?",
      "text": "We are a company working in cryptocurrency field since 2013, based in London, the UK. You can use our service to buy over 100 different cryptocurrencies through credit/debit card payment without registration."
    },
    "question2": {
      "title": "How can I contact you?",
      "text": "You can contact our support team by e-mail (support@indacoin.com), phone (+44-207-048-2582) or by using a live chat option on our webpage."
    },
    "question3": {
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "Which cryptocurrencies can I buy here?",
      "text": "You can buy over 100 different cryptocurrencies that are all listed on our website."
    },
    "question5": {
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "Which countries can use this service?",
      "text": "We work with all countries,with the only exception of the USA(at the moment)"
    },
    "question7": {
      "title": "Can i sell bitcoin here?",
      "text": "No, you can only purchase bitcoins here."
    },
    "question8": {
      "title": "What is Cryptocurrency?",
      "text": "Сryptocurrency is a digital currency that uses cryptography for security, feature, that makes it difficult to counterfeit. It is not issued by any central authority, rendering it theoretically immune to government interference or manipulation."
    },
    "question9": {
      "title": "Where can I store cryptocurrency?",
      "text": "You can set up a separate wallet for each cryptocurrency on your PC or you can use our free wallet service and store 100+ cryptocurrencies all in one place."
    },
    "question10": {
      "title": "What do you mean by \"bitcoin address\"?",
      "text": "It is the address of your bitcoin wallet. You can also enter the address of a person you want bitcoins to be sent to or create a wallet on Indacoin"
    },
    "question11": {
      "title": "How fast is cryptocurrency transaction?",
      "text": "Cryptocurrency transaction times depend on which cryptocurrency you are sending, Bitcoin transactions take about 15-20 minutes to complete and on average allow up to 1 hour for the rest of the coins."
    },
    "question12": {
      "title": "What should I do to verify my card?",
      //"text": "To complete verification you’ll need to enter the 4 digit code you receive via phone call. For your second step you may also be asked to undergo video verification, where you record a video by showing your face and upload a scan or photo of your ID/Passport/Driver's license. Depending on your verification, this step can be mandatory since we need to be certain that you are the card holder."
      "text": "To complete verification you’ll need to enter the 4 digit code you receive via phone call. For your second step you may also be asked to undergo photo verification, which requires you to upload a photo of your passport/ID and a selfie with ID and a piece of paper with required text, confirming the purchase. We may alternatively ask for video verification, where you will need to record your face, say \"Indacoin verification for crypto\" show your ID and card you made the payment with. When showing the card, show only last 4 digits and the name."
    },
    "question13": {
      "title": "What if I can't record a video verification?",
      "text": "You can open the same order page link from your mobile device and record it from there. If having trouble, you can also send the required video as attachment via e-mail to support@indacoin.com. Make sure to Indicate your exchange order number."
    },
    "question14": {
      "title": "I have made an order and entered all the details, why is it still processing?",
      "text": "If you have waited more than 2 minutes, then the order didn't make it to transaction. It's possible that your card isn't 3D secure and you may need to try again. Make sure you confirm your purchase via PIN code with your bank or you can try buying with a different currency (EUR/USD). Otherwise, if you are certain your card is 3Ds and changing currency doesn't help then you need to contact your bank, they may be blocking your purchase attempts."
    },
    "question15": {
      "title": "Do I need to go through verification every time I make a purchase?",
      "text": "You only need to verify your card once and all following transactions will be automatic. However we will require verification if you decide to use another bank card."
    },
    "question16": {
      "title": "My order was declined by the bank, what should I do?",
      "text": "You need to contact your bank and ask for the reason of the decline, perhaps they can lift the restriction so a purchase can be made."
    },
    "question17": {
      "title": "Why do I need to verify my banking card or send a video?",
      "text": "The verification process protects you in case of card theft, hacking or fraudsters ; this works the opposite way in case someone is trying to place a purchase using someone else's banking card."
    },
    "question18": {
      "title": "How can I trust that my details and card's info are secure?",
      "text": "We never share your details with any Third Parties without your consent. Also, we only accept 3D-Secure cards ,and we never request any sensitive data such as the CCV code or the full card's number. You only provide such payment details through Visa or Mastercard gateway terminals. We only collect the information required to make sure that you're the card's owner."
    },
    "question19": {
      "title": "What is your fee?",
      "text": "Our fee varies from purchase to purchase as it is based on numerous factors, that's why we have created a calculator that will tell you the exact amount of cryptocurrency you will receive, all fees included."
    },
    "question20": {
      "title": "How much will I be charged for the transaction?",
      "text": "You will be charged only the amount you specify during your order in our calculator. All fees are included in that amount."
    },
    "question21": {
      "title": "Do you have any discounts?",
      "text": "We do occasionally have discounts, you can get more information from our support staff."
    },
    "question22": {
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "How fast will my cryptocurrency be sent??",
      "text": "Usually it takes about 30 minutes after you make an order or complete verification (if it’s the first time you use the card on Indacoin)."
    },
    "question24": {
      "title": "What are the minimum and maximum purchases?",
      "text": "You will have the limit of maximum $200 for the first transaction, additional $200 for the second transaction available after 4 days of the initial purchase, $500 after 7 days and $2000 in 14 days of the first buy. In a month from your first purchase there are no limits when paying with your card. Please keep in mind that the minimum limit is always $50."
    },
    "question25": {
      "title": "What type of cards are accepted?",
      "text": "We only accept Visa and Mastercard with 3D secure."
    },
    "question26": {
      "title": "What is 3D secure?",
      "text": "3-D Secure technology consists of the programs Verified by Visa and MasterCard SecureCode.After you enter your credit card details in our online store, a new window will appear, requesting your personal security code. Your financial institution will authenticate the transaction within seconds, as well as confirm that you are the individual making the purchase."
    },
    "question27": {
      "title": "What other payment systems can I use to buy bitcoins?",
      "text": "Currently we only accept Visa/Master card payments on our platform."
    }
  },
  "api": {
    "introduction": {
      "header-1": "api",
      "header-2": "Types of integration",
      "text-1": "On this page, you can find methods that are used to enable your service with the cryptocurrency exchange features of Indacoin. For example, if you want to provide your customers with the option to pay via credit/debit cards, with our API you will be able to do that.",
      "text-2": "For more information on the possible use of our API please email us to <span class=\"blue\">pr@indacoin.com</span>",
      "title-1": "How to become a partner with Indacoin:",
      "title-2": "Light integration",
      "title-3": "Standard integration",
      "title-4": "Possible statuses of transaction:",
      "title-5": "User limit",
      "title-6": "Create transaction",
      "title-7": "Get price",
      "title-8": "Transaction history",
      "title-9": "Transaction info",
      "title-10": "Callback section",
      "title-11": "Methods:",

      "text-3": "Select the type of the integration you are interested in.",
      "text-4": "Contact us to verify your company’s profile.",
      "text-5": "Receive the partnername and the secret for the integration.",
      "text-6": "Start API implementation.",
      "text-7": "In both types of the integration the customer goes to our platform to make the payment, to get verified and to receive the confirmation of the purchase.",
      "text-8": "After the client makes the payment, you get a callback about every change in the status of the transaction.",
      //"text-9": "We recommend making a request <code>exgw_getUserlimits</code> to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform). (It is not mandatory)",
      "text-9": "The minimum transaction limit is 30 USD / EUR, and the maximum limit is 6000 USD / EUR",
      "text-10": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request <code>exgw_createTransaction</code>, where you choose amounts, currencies and wallet address.",
      "text-11": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL.",
      "text-12": "You can forward users to URL without the creation of transaction by API, <span class=\"blue\">cur_from</span> should be used from the list (USD, EURO, RUB) <span class=\"blue\">cur_to</span> should be <span class=\"blue\">short_name</span> of the coin.",
      "text-13": "Full list: <span class=\"blue\">https://indacoin.com/api/mobgetcurrenciesinfoi</span>",
      "text-14": "General view of the query: <span class=\"blue\">https://indacoin.com/gw/payment_form?partner={YourPartnerName}&cur_from={CurrencyFrom}&cur_to={CurrencyTo}&amount={Amount}&address={WalletAddress}&user_id={UserID}</span>",
      "text-15": "The purchase is considered to be successful when the status Finished is reached.",
      "text-16": "create new transaction",
      "text-17": "Parameters in json:",
      "text-18": "the method is valid only for crypto processing",
      "text-19": "receive amount user will get",
      "text-20": "Parameters in GET:",
      "text-21": "receive info about last transactions",
      "text-22": "Parameters in json:",
      "text-23": "all parameters are optional",
      "text-24": "receive transaction's info",
      "text-25": "Parameters in json:",
      "text-26": "Example of how to create the request on PHP and Node.js",
      "text-27": "Example of how to create the request for URL",
      "text-28": "To create the request you need to get transaction ID from the previous request.",
      "text-29": "receive user's limit (and create if non-existent)",
      "text-30": "Parameters in json:",
      "text-31": "We send callbacks in json POST format. To verify callback we will send you headers <code>gw-sign</code>, <code>gw-nonce</code> where nonce will be random number and sign will be",
      "text-32": "Example:",

      "NotFound": "the transaction hasn't been found",
      "Chargeback": "chargeback has been filed",
      "Declined": "we declined the payment, very likely because the card wasn't 3D secure",
      "Cancelled": "the payment wasn’t made during some hours after the creation of the order",
      "Failed": "general case when payment failed due to various reasons",
      "Draft": "not paid yet or there were no tries to pay",
      "Paid": "money has been withdrawn from the user's bank account",
      "Verification": "the user started the verification",
      "FundsSent": "the coins have been sent to the user's wallet but have not been received",
      "Finished": "coins have been received successfully"
    },

    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": {
    "message": "Amount must be between {{{limitsMin}}} {{{limitsCur}}} and {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "Amount must be from {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">This website uses cookies to ensure you get the best experience on our website.</span> {{{link}}}",
      "accept-button": "Got it",
      "link": "Learn More"
    },
    "success-video": {
      "text": "Thank you! Now we started the verification process, which can take some time. When verification is completed, the cryptocoins will be delivered to you"
    },
    "accept-agreement": {
      //"text": "Please indicate that you have read and agree to the User Agreement and AML Policy"
      "text": "Please indicate that you have read and agree to the Terms of Use and Privacy Policy"
    },
    "price-changed": {
      "header": "Price changed",
      "agree": "Agree",
      "cancel": "Cancel"
    },
    "neoAttention": "Because of the features of the NEO crypto currency, only integers are available for purchase",
    "login": {
      "floodDetect": "Flood detect",
      "resetSuccess": "Reset password success. Please check your email",
      "wrongEmail": "This e-mail is not registered yet",
      "unknownError": "Server error. Please retry the request",
      "alreadyLoggedIn": "You are already logged in",
      "badLogin": "Wrong login or password",
      "badCaptcha": "Wrong captcha",
      "googleAuthenticator": "Please enter google authenticator code"
    },
    "registration": {
      "unknownError": "Unknown error",
      "floodDetect": "Flood detect",
      "success": "Register success. Please check your email",
      "badLogin": "Wrong login or password",
      "badCaptcha": "Wrong captcha",

      "empty": "Not all fields are completed. Please fill them all",
      "repeatedRequest": "The Confirmation of Registration was sent to your email address. Please make sure that you have received it. Just in case, check also a spam folder",
      "badEmail": "You used the incorrect or temporary email address",
      "blockedDomain": "Please try a different email address to register"
    }
  },
  "stop-sidebar": {
    "title": "Dear Customers!",
    "text": "We have to notify you that due to the recent increase in our volume we will take a pause in selling cryptocurrencies on our site and mobile apps. We apologize for the inconvenience! After the tech restoration, the purchase will be available again on",
    "time": "Wednesday after 16 pm (UTC+0)"
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "No results found",
  "locale": {
    "title": "Please choose your country"
  }
}