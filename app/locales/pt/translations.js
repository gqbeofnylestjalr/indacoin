export default {
  'lang': 'pt',
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    } 
  },
  "recordVideo": {
    "send": "Enviar",
    "stop": "Pare",
    "record": "Registro"
  },
  "links": {
    "terms-of-use": {
      "text": "Eu concordo com os {{{terms}}} e {{{policy}}}",
      "link-1": "Termos de uso",
      "link-2": "Política de Privacidade"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "Esperando pelo seu pagamento",
      "status-2": "Trocar",
      "status-2-sub": "Estamos procurando a melhor taxa para você",
      "status-3": "Enviando para sua carteira",
      "transaction-completed": "Parabéns, sua transação foi concluída!",
      "rate": "Taxa de câmbio 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Você enviou:",
      "toAddress": "para {{{cryptoAdress}}}",
      "receiveText": "Você receberá:"
    },
    "changeCrypto": {
      "send": "Envie {{{amount}}} BTC para o endereço abaixo",
      "confirmation": "Após duas confirmações de bitcoin, {{{amount}}} {{{shortName}}} será enviado para o seu endereço {{{wallet}}} ...",
      "copy": "Copiar endereço",
      "copied": "Copiado para a área de transferência"
    },
    "order": "Encomenda",
    "status": {
      "title": "Estado",
      "completed": "Pedido executado"
    },
    "payAmount": {
      "title": "Pago"
    },
    "check": "Verificar o estado da operação no blockexplorer.net",
    "resend-phone-code": "Não recebeu uma ligação?",
    "sms-code": "Por favor, atenda o telefone e digite o código de 4 dígitos aqui",
    "registration": "Ir para a página de registro",

    "internalAccount-1": "Por favor, verifique sua carteira Indacoin",
    "internalAccount-link": "logging",
    "internalAccount-2": "",
    "your-phone": "seu telefone {{{phone}}}",
    "many-requests": "Desculpa. Para muitos pedidos de mudança de telefone"
  },
  "app":{
    "detect_lang":{
      "title":"Configuração do seu idioma",
      "sub":"Por favor aguarde..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Compre e troque {{{longName}}} instantaneamente e 100 outras moedas criptografadas ao melhor preço em {{{country}}}",
    "title":"Compre criptomoeda com cartão de crédito ou de débito - Indacoin",
    "description":"Compre e faça câmbio de criptomoedas instantaneamente, entre [cryptocurrency], Ethereum, Litecoin, Ripple e mais 100 moedas digitais, para EUR ou USD.",
    "description-bitcoin":"Compre e troque qualquer criptomoeda instantaneamente: Ripple, Bitcoin, Ethereum, Litecoin e outras 100 moedas digitais por EUR ou USD",
    "description-ethereum":"Compre e troque qualquer criptomoeda instantaneamente: Ethereum, Bitcoin, Litecoin, Ripple e outras 100 moedas digitais por EUR ou USD",
    "description-litecoin":"Compre e troque qualquer criptomoeda instantaneamente: Litecoin, Bitcoin, Ethereum, Ripple e outras 100 moedas digitais por EUR ou USD",
    "description-ripple":"Compre e troque qualquer criptomoeda instantaneamente: Ripple, Bitcoin, Ethereum, Litecoin e outras 100 moedas digitais por EUR ou USD",

    "description2": "Compre e troque instantaneamente qualquer crypto curreny: Bitcoin, Ethereum, Litecoin, Ripple e 100 outras moedas digitais para EUR ou USD",
    "description3": "Compre e troque qualquer criptomoeda instantaneamente: Bitcoin, Ethereum, Litecoin, Ripple e outras 100 moedas digitais com Visa e Mastercard",
    "buy": "Compre",
    "subTitle": "com cartão de débito ou de crédito",
    "more": "Em mais de 100 países incluindo",
    //"header": "Troque Bitcoin para <span class='sub'>{{{name}}}</span>",
    "header": "Trocar instantaneamente <span class='sub'>BTC</span> para <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "COMPRE CRIPTOMOEDA INSTANTANEAMENTE",
    "header-alt": "Troca de Criptomoeda Instantânea",
    "sub": "Indacoin fornece uma maneira fácil de comprar Bitcoins e mais de 100 criptocorrências com Visa e Mastercard",
    "sub-alt": "A Indacoin fornece uma maneira fácil de trocar bitcoins por qualquer uma das 100+ criptomoedas diferentes",
    "observe": "Consulte criptomoedas disponíveis em baixo"
  },
  "navbar": {
    "main":"Página principal",
    "btc": "Comprar Bitcoin",
    "eth": "Comprar Cryptocurrency",
    "affiliate": "Parceria",
    "news": "Notícias",
    "faq": "FAQ",
    "login": "Entrar",
    "registration": "Registrar-se",
    "platform": "Produtos",

    "logout": "Sair",
    "buy-instantly": "Compre o crypto instantaneamente",
    "trading-platform": "Plataforma de negociação",
    "mobile-wallet": "Carteira móvel",
    "payment-processing": "API de processamento de pagamento"
  },
  "currency-table": {
    "rank": "Ranking",
    "coin": "Moeda",
    "market": "Valor de mercado",
    "price": "Preço",
    "volume": "Volume 24h",
    "change": "Crescimento 24h",
    "available-supply": "Fornecimento disponível",
    "search-cryptocurrency": "Pesquisa por criptomoeda",

    "change-1h": "1H mudança",
    "change-24h": "1D mudança",
    "change-7d": "1W mudança"
  },
  "buy-blocks":{
    "give":"Você entrega",
    "get":"Você recebe"
  },
  "exchange-form": {
    "title": "Compra instantânea de criptomoeda",
    "submit": "Cambiar",
    "give": "Você entrega",
    "get": "Você recebe",
    "search": "Pesquisa",
    "error-page": "Pedimos desculpas, mas a compra está indisponível no momento"
  },
  "pagination": {
    "page": "Página",
    "next": "Próxima",
    "prev": "Voltar",
    "first": "Início"
  },
  "sidebar":{
    "accept":{
      "title":"Aceitamos cartões de crédito",
      "button":"Comprar criptomoeda"
    },
    "youtube":{
      "title":"Sobre nós",
      "play":"Lançar"
    },
    "orders":{
      "title": "Gestão de encomendas",
      "order": "Encomenda #",
      "confirm": "Código de confirmação",
      "check": "Verificar encomenda",
      "new": "Criar nova encomenda",
      "or": "Ou"
    },
    "social":{
      "title":"Nós nas redes sociais"
    }
  },
  "wiki":{
    "etymology":"Etimologia"
  },
  "loading": {
    "title": "Carregando",
    "sub": "Por favor aguarde",
    "text": "Carregando"
  },
  "error": {
    "title": "Erro",
    "sub": "Algo correu mal, sem pânico",
    "text": "Enfrentamos um erro inesperado e já estamos trabalhando para corrigi-lo. Por favor, volte mais tarde.",
    "home": "Principal",
    "mainPageError1": "Infelizmente, informações sobre crypto-moedas disponíveis não estão disponíveis",
    "mainPageError2": "Tente visitar o site um pouco mais tarde",
    "page-404": "Pedimos desculpas, página não encontrada",
    "page-error": "Pedimos desculpas, ocorreu um erro"
  },
  "features":{
    "title":"O que pode<br><span class=\"offset\">fazer no <span class=\"blue\">Indacoin</span></span>",
    "column1":{
      "title":"Comprar criptomoeda",
      "text":"Na Indacoin você pode comprar mais de 100 moedas criptográficas diferentes instantaneamente com um cartão de crédito ou débito"
    },
    "column2":{
      "title":"Saiba mais sobre criptomoedas",
      "text":"Nossas guias compreensíveis irão ajudar perceber melhor os princípios fundamentais de qualquer moeda alternativa"
    },
    "column3":{
      "title":"Tome decisões de investimento",
      "text":"Ferramentas analíticas poderosas permitir-lhe-ão apanhar o melhor momento para entrar e sair"
    },
    "column4":{
      "title":"Compre bitcoin sem registro",
      "text":"Bitcoins serão enviados para o endereço indicado por você depois de realização do pagamento"
    },
    "column5":{
      "title":"Partilhe suas ideias",
      "text":"Se junte à discussão das últimas tendências de criptomoeda com outros investidores de topo"
    },
    "column6":{
      "title":"Guarde todas as moedas no mesmo sítio",
      "text":"Pode guardar e gerir mais de 100 moedas digitais na mesma Carteira Indacoin"
    }
  },
  "mockup":{
    "title":"CARTEIRAS NO CELULAR",
    "text":"Nossa carteira Indacoin simples e inteligente funciona no seu celular Android ou iPhone, além do seu browser. Agora mandar criptomoeda a amigo é tão fácil como enviar mensagem."
  },
  "footer":{
    "column1":{
      "title":"Comprar",
      "link1":"Comprar Bitcoin",
      "link2":"Comprar Ethereum",
      "link3":"Comprar Ripple",
      "link4":"Comprar Bitcoin Cash",
      "link5":"Comprar Litecoin",
      "link6":"Comprar Dash"
    },
    "column2":{
      "title":"Informações",
      "link1":"Sobre nós",
      "link2":"Perguntas",
      "link3":"Regras de utilização",
      "link4":"Manuais"
    },
    "column3":{
      "title":"Ferramentas",
      "link1":"API",
      "link2":"Mudar de região",
      "link3":"Aplicativo celular"
    },
    "column4":{
      "title":"Contatos"
    }
  },
  "country":{
    "AF":{
      "name":"Afeganistão"
    },
    "AX":{
      "name":"Ilhas de Aland"
    },
    "AL":{
      "name":"Albânia"
    },
    "DZ":{
      "name":"Argélia"
    },
    "AS":{
      "name":"Samoa Americana"
    },
    "AD":{
      "name":"Andorra"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguilla"
    },
    "AQ":{
      "name":"Antártica"
    },
    "AG":{
      "name":"Antígua e Barbuda"
    },
    "AR":{
      "name":"Argentina"
    },
    "AM":{
      "name":"Arménia"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Austrália"
    },
    "AT":{
      "name":"Áustria"
    },
    "AZ":{
      "name":"Azerbaijão"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Bahrain"
    },
    "BD":{
      "name":"Bangladesh"
    },
    "BB":{
      "name":"Barbados"
    },
    "BY":{
      "name":"Bielorrússia"
    },
    "BE":{
      "name":"Bélgica"
    },
    "BZ":{
      "name":"Belize"
    },
    "BJ":{
      "name":"Benim"
    },
    "BM":{
      "name":"Bermuda"
    },
    "BT":{
      "name":"Butão"
    },
    "BO":{
      "name":"Bolívia"
    },
    "BA":{
      "name":"Bósnia e Herzegovina"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Ilha de Bouvet"
    },
    "BR":{
      "name":"Brasil"
    },
    "IO":{
      "name":"Território Britânico do Oceano Índico"
    },
    "BN":{
      "name":"Brunei Darussalam"
    },
    "BG":{
      "name":"Bulgária"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Camboja"
    },
    "CM":{
      "name":"Camarões"
    },
    "CA":{
      "name":"Canadá"
    },
    "CV":{
      "name":"Cabo Verde"
    },
    "KY":{
      "name":"Ilhas Caimão"
    },
    "CF":{
      "name":"República Centro-Africana"
    },
    "TD":{
      "name":"Chade"
    },
    "CL":{
      "name":"Chile"
    },
    "CN":{
      "name":"China"
    },
    "CX":{
      "name":"Ilha Christmas"
    },
    "CC":{
      "name":"Ilhas Cocos (Keeling)"
    },
    "CO":{
      "name":"Colômbia"
    },
    "KM":{
      "name":"Comores"
    },
    "CG":{
      "name":"Congo"
    },
    "CD":{
      "name":"Congo, República Democrática"
    },
    "CK":{
      "name":"Ilhas Cook"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Costa de Marfim"
    },
    "HR":{
      "name":"Croácia"
    },
    "CU":{
      "name":"Cuba"
    },
    "CY":{
      "name":"Chipre"
    },
    "CZ":{
      "name":"República Checa"
    },
    "DK":{
      "name":"Dinamarca"
    },
    "DJ":{
      "name":"Djibouti"
    },
    "DM":{
      "name":"Domínica"
    },
    "DO":{
      "name":"República Dominicana"
    },
    "EC":{
      "name":"Equador"
    },
    "EG":{
      "name":"Egito"
    },
    "SV":{
      "name":"El Salvador"
    },
    "GQ":{
      "name":"Guiné Equatorial"
    },
    "ER":{
      "name":"Eritreia"
    },
    "EE":{
      "name":"Estónia"
    },
    "ET":{
      "name":"Etiópia"
    },
    "FK":{
      "name":"Ilhas Falkland (Malvinas)"
    },
    "FO":{
      "name":"Ilhas Faroé"
    },
    "FJ":{
      "name":"Fiji"
    },
    "FI":{
      "name":"Finlândia"
    },
    "FR":{
      "name":"França"
    },
    "GF":{
      "name":"Guiana Francesa"
    },
    "PF":{
      "name":"Polinésia Francesa"
    },
    "TF":{
      "name":"Territórios Franceses do Sul"
    },
    "GA":{
      "name":"Gabão"
    },
    "GM":{
      "name":"Gâmbia"
    },
    "GE":{
      "name":"Geórgia"
    },
    "DE":{
      "name":"Alemanha"
    },
    "GH":{
      "name":"Gana"
    },
    "GI":{
      "name":"Gibraltar"
    },
    "GR":{
      "name":"Grécia"
    },
    "GL":{
      "name":"Gronelândia"
    },
    "GD":{
      "name":"Grenada"
    },
    "GP":{
      "name":"Guadalupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernsey"
    },
    "GN":{
      "name":"Guiné"
    },
    "GW":{
      "name":"Guiné Bissau"
    },
    "GY":{
      "name":"Guiana"
    },
    "HT":{
      "name":"Haiti"
    },
    "HM":{
      "name":"Ilhas Heard e Ilhas Mcdonald"
    },
    "VA":{
      "name":"Santa Sé (Estado da Cidade de Vaticano)"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hong Kong"
    },
    "HU":{
      "name":"Hungria"
    },
    "IS":{
      "name":"Icelândia"
    },
    "IN":{
      "name":"Índia"
    },
    "ID":{
      "name":"Indonésia"
    },
    "IR":{
      "name":"Irã, República Islâmica"
    },
    "IQ":{
      "name":"Iraque"
    },
    "IE":{
      "name":"Irlanda"
    },
    "IM":{
      "name":"Ilha de Man"
    },
    "IL":{
      "name":"Israel"
    },
    "IT":{
      "name":"Itália"
    },
    "JM":{
      "name":"Jamaica"
    },
    "JP":{
      "name":"Japão"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Jordão"
    },
    "KZ":{
      "name":"Cazaquistão"
    },
    "KE":{
      "name":"Quénia"
    },
    "KI":{
      "name":"Quiribati"
    },
    "KP":{
      "name":"Coreia, República Democrática Popular"
    },
    "KR":{
      "name":"Coreia, República"
    },
    "KW":{
      "name":"Kuwait"
    },
    "KG":{
      "name":"Quirguistão"
    },
    "LA":{
      "name":"Lau, República Democrática Popular"
    },
    "LV":{
      "name":"Letónia"
    },
    "LB":{
      "name":"Líbano"
    },
    "LS":{
      "name":"Lesoto"
    },
    "LR":{
      "name":"Libéria"
    },
    "LY":{
      "name":"Líbia, Jamahiria Árabe"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Lituânia"
    },
    "LU":{
      "name":"Luxemburgo"
    },
    "MO":{
      "name":"Macau"
    },
    "MK":{
      "name":"Macedónia, Antiga República Jugoslava"
    },
    "MG":{
      "name":"Madagáscar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malásia"
    },
    "MV":{
      "name":"Maldivas"
    },
    "ML":{
      "name":"Mali"
    },
    "MT":{
      "name":"Malta"
    },
    "MH":{
      "name":"Ilhas Marshall"
    },
    "MQ":{
      "name":"Martinica"
    },
    "MR":{
      "name":"Mauritânia"
    },
    "MU":{
      "name":"Maurício"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"México"
    },
    "FM":{
      "name":"Micronésia, Estados Federados"
    },
    "MD":{
      "name":"Moldávia, República"
    },
    "MC":{
      "name":"Mónaco"
    },
    "MN":{
      "name":"Mongólia"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Marrocos"
    },
    "MZ":{
      "name":"Moçambique"
    },
    "MM":{
      "name":"Myanmar"
    },
    "NA":{
      "name":"Namíbia"
    },
    "NR":{
      "name":"Naurú"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Países Baixos"
    },
    "AN":{
      "name":"Antilhas Holandesas"
    },
    "NC":{
      "name":"Nova Caledónia"
    },
    "NZ":{
      "name":"Nova Zelândia"
    },
    "NI":{
      "name":"Nicarágua"
    },
    "NE":{
      "name":"Níger"
    },
    "NG":{
      "name":"Nigéria"
    },
    "NU":{
      "name":"Niué"
    },
    "NF":{
      "name":"Ilha Norfolk"
    },
    "MP":{
      "name":"Ilhas Marianas do Norte"
    },
    "NO":{
      "name":"Noruega"
    },
    "OM":{
      "name":"Omã"
    },
    "PK":{
      "name":"Paquistão"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Territórios Palestinianos, Ocupados"
    },
    "PA":{
      "name":"Panamá"
    },
    "PG":{
      "name":"Papua Nova Guiné"
    },
    "PY":{
      "name":"Paraguai"
    },
    "PE":{
      "name":"Peru"
    },
    "PH":{
      "name":"Filipinas"
    },
    "PN":{
      "name":"Pitcairn"
    },
    "PL":{
      "name":"Polónia"
    },
    "PT":{
      "name":"Portugal"
    },
    "PR":{
      "name":"Puerto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"Reunion"
    },
    "RO":{
      "name":"Roménia"
    },
    "RU":{
      "name":"Federação Russa"
    },
    "RW":{
      "name":"Rwanda"
    },
    "SH":{
      "name":"Santa Helena"
    },
    "KN":{
      "name":"Saint Kitts e Nevis"
    },
    "LC":{
      "name":"Santa Lucia"
    },
    "PM":{
      "name":"Saint Pierre e Miquelon"
    },
    "VC":{
      "name":"Saint Vincent e Grenadinas"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"San Marino"
    },
    "ST":{
      "name":"São Tomé e Príncipe"
    },
    "SA":{
      "name":"Arábia Saudita"
    },
    "SN":{
      "name":"Senegal"
    },
    "CS":{
      "name":"Sérvia e Montenegro"
    },
    "SC":{
      "name":"Seychelles"
    },
    "SL":{
      "name":"Serra Leoa"
    },
    "SG":{
      "name":"Singapura"
    },
    "SK":{
      "name":"Eslováquia"
    },
    "SI":{
      "name":"Eslovénia"
    },
    "SB":{
      "name":"Ilhas Solomon"
    },
    "SO":{
      "name":"Somália"
    },
    "ZA":{
      "name":"África do Sul"
    },
    "GS":{
      "name":"Ilhas Geórgia do Sul e Sandwich do Sul"
    },
    "ES":{
      "name":"Espanha"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Sudão"
    },
    "SR":{
      "name":"Suriname"
    },
    "SJ":{
      "name":"Svalbard e Jan Mayen"
    },
    "SZ":{
      "name":"Suazilândia"
    },
    "SE":{
      "name":"Suécia"
    },
    "CH":{
      "name":"Suíça"
    },
    "SY":{
      "name":"República Árabe de Síria"
    },
    "TW":{
      "name":"Taiwan, Província Chinesa"
    },
    "TJ":{
      "name":"Tajiquistão"
    },
    "TZ":{
      "name":"Tanzânia, República Unida"
    },
    "TH":{
      "name":"Tailândia"
    },
    "TL":{
      "name":"Timor Leste"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad e Tobago"
    },
    "TN":{
      "name":"Tunísia"
    },
    "TR":{
      "name":"Turquia"
    },
    "TM":{
      "name":"Turcomenistão"
    },
    "TC":{
      "name":"Ilhas Turcas e Caicos"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Uganda"
    },
    "UA":{
      "name":"Ucrânia"
    },
    "AE":{
      "name":"Emirados Árabes Unidos"
    },
    "GB":{
      "name":"Reino Unido"
    },
    "US":{
      "name":"Estados Unidos da América"
    },
    "UM":{
      "name":"Ilhas Menores Distantes dos Estados Unidos"
    },
    "UY":{
      "name":"Uruguai"
    },
    "UZ":{
      "name":"Usbequistão"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Vietname"
    },
    "VG":{
      "name":"Ilhas Virgens Britânicas"
    },
    "VI":{
      "name":"Ilhas Virgens EUA"
    },
    "WF":{
      "name":"Wallis e Futuna"
    },
    "EH":{
      "name":"Saara Ocidental"
    },
    "YE":{
      "name":"Iémene"
    },
    "ZM":{
      "name":"Zâmbia"
    },
    "ZW":{
      "name":"Zimbabwe"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Comprar",
  "sell":"Vender",
  "weWillCallYou":"Caso esteja usando este cartão pela primeira vez, deverá introduzir 2 códigos secretos, do seu extrato bancário e da chamada automática para o seu celular.",
  "exchangerStatuses":{
        "TimeOut": "Seu pedido expirou",
        "Error": "Ocorreu um erro, por favor, contate o serviço de apoio support@indacoin.com",
        "WaitingForAccountCreation": "Esperando pela criação da conta Indacoin (enviamos a você um e-mail de registro)",
        "CashinWaiting": "Seu pagamento está sendo processado",
        "BillWaiting": "Aguardando pelo pagamento da fatura pelo cliente",
        "Processing": "Pedido em processamento",
        "MoneySend": "Fundos enviados",
        "Completed": "Pedido executado",
        "Verifying": "Verificação",
        "Declined": "Rejeitado",
        "cardDeclinedNoFull3ds": "Seu cartão foi rejeitado porque o seu banco não suporta a opção de 3ds completo para este cartão, por favor contate o serviço de apoio do seu banco",
        "cardDeclined": "Seu cartão foi rejeitado, contate o serviço de apoio support@indacoin.com em caso de dúvidas",
        "Non3DS": "Infelizmente seu pagamento foi recusado. Confirme junto ao seu banco que está usando um cartão com função 3D-Secure (Verified by Visa ou MasterCard SecureCode) que prevê a introdução de um código secreto que é normalmente recebido no celular para cada pagamento na internet"
  },
  "exchanger_fields_ok":"Carregue para comprar bitcoins",
  "exchanger_fields_error":"Erro de preenchimento. Verifique todos os campos",
  "bankRejected":"A transação foi rejeitada pelo seu banco. Por favor, contate o banco e repita o pagamento com cartão",
  "wrongAuthCode":"Introduziu um código de autorização errado, por favor, contate o serviço de apoiosupport@indacoin.com",
  "wrongSMSAuthCode":"Introduziu um código errado do celular",
  "getCouponInfo":{
    "indo":"Este cupão não é permitido",
    "exetuted":"Este cupão já foi usado",
    "freeRate":"O cupão de desconto foi ativado"
  },
  "ip":"Seu IP foi temporariamente bloqueado",
  "priceChanged":"Preço foi alterado. Ok? Vai receber ",
  "exchange_go":"– Cambiar",
  "exchange_buy_btc":"– Comprar bitcoins",
  "month":"Jan Feb Mar Abr Mai Jun Jul Aug Set Out Nov Dez",
  "discount":"Desconto",
  "cash":{
    "equivalent":" equivalente ",
    "card3DS":"Estamos aceitando apenas cartões 3D-Secure (Verified by Visa ou MasterCard SecureCode)",
    "cashIn":{
      "1": "Cartões bancários (USD)",
      "2": "Terminais de pagamento",
      "3": "QIWI",
      "4": "Bashcomsnabbank",
      "5": "Terminais Novoplat",
      "6": "Terminais Elexnet",
      "7": "Cartões bancários (USD)",
      "8": "Alfa-click",
      "9": "Transferência bancária internacional",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Money",
      "21": "Elexnet",
      "22": "Cassira.net",
      "23": "Mobil element",
      "24": "Svyaznoy",
      "25": "Euroset",
      "26": "PerfectMoney",
      "27": "Indacoin-interno",
      "28":	"CouponBonus",
      "29": "Yandex.Money",
      "30": "OKPay",
      "31": "Programa de parceria",
      "33": "Payza",
      "35": "Código BTC-E",
      "36": "Cartões bancários (USD)",
      "37": "Cartões bancários (USD)",
      "39": "Transferência bancária internacional",
      "40": "Yandex.Money",
      "42": "QIWI (Manual)",
      "43": "UnionPay",
      "44": "QIWI(Automático)",
      "45": "Pagamento da conta do celular",
      "49": "LibrexCoin",
      "50": "Cartões bancários (EURO)",
      "51": "QIWI(Automático)",
      "52": "Pagamento da conta do celular",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Digite o endereço de criptografia correto",
      "country": "País não permitido",
      "alfaclick": "Login não permitido",
      "pan": "Número de cartão não permitido",
      "cardholdername": "Dados não permitidos",
      "fio": "Dados não permitidos",
      "bday": "Data de nascimento incorreta",
      "city": "Dados não permitidos",
      "address": "Endereço não permitido",
      "ypurseid": "Número de carteira não permitido",
      "wpurseid": "Número de carteira não permitido",
      "pmpurseid": "Número de conta não permitido",
      "payeer": "Número de conta não permitido",
      "okpayid": "Número de carteira não permitido",
      "purse": "Número de carteira não permitido",
      "phone": "Celular não permitido",
      "btcaddress": "Endereço não permitido",
      "ltcaddress": "Endereço não permitido",
      "lxcaddress": "Endereço não permitido",
      "ethaddress": "Endereço não permitido",
      "properties": "Dados não permitidos",
      "email": "Email não permitido",
      "card_number": "Este cartão não é suportado, verifique o número",
      "inc_card_number": "Número de cartão errado",
      "inn": "Número incorreto",
      "poms": "Número incorreto",
      "snils": "Número incorreto",
      "cc_expr": "Data errada",
      "cc_name": "Nome errado",
      "cc_cvv": "CVV errado"
    }
  },
  "change": {
    "wallet": "Criar nova carteira / Eu já tenho conta Indacoin",
    //"accept": "Eu concordo com o Acordo do Usuário e com a Política de AML",
    "accept": "Eu concordo com os Termos de Uso e Política de Privacidade",
    "coupone": "Tenho um cupão",
    "watchVideo": "Assistir ao vídeo",
    "seeInstructions": "Ajuda",
    "step1": {
      "title": "Criação da encomenda",
      "header": "Introduza dados de pagamento",
      "subheader": "Queira informar o que deseja comprar"
    },
    "step2": {
      "title": "Dados de pagamento",
      "header": "Introduza dados do seu cartão bancário",
      "subheader": "Aceitamos apenas Visa e Mastercard",
      "card": {
        "header": "Onde encontro este código?",
        "text": "O código CVV se encontra no verso do cartão. É composto por 3 dígitos"
      }
    },
    "step3": {
      "title": "Dados de contato",
      "header": "Introduza seus dados de contato",
      "subheader": "Gostaríamos de conhecer melhor os nossos clientes"
    },
    "step4": {
      "title": "Verificação",
      "header": "Introduza o seu código da página de pagamento",
      "subheader": "Seus dados de pagamento"
    },
    "step5": {
      "title": "Dados da encomenda"
    },
    "step6": {
      "title": "Gravação de vídeo",
      "header": "Nos mostre a sua cara e o seu documento de identidade",
      "description": "Mostre que se pode colaborar consigo",
      "startRecord": "Carregue no botão Iniciar para iniciar a gravação",
      "stopRecord": "Carregue no Parar para concluir a gravação"
    },
    
    "nextButton": "Próximo",
    "previousButton": "Anterior",
    "passKYCVerification": "Verificação KYC",
    "soccer-legends": "Concordo que meus dados pessoais sejam transferidos e processados para a empresa Soccer Legends Limited",
    "withdrawalAmount": "Valor a ser creditado na conta"
  },
  "form": {
    "tag": "Tag {{{currency}}}",
    "loading": "Número do cartão",
    "youGive": "Você dá",
    "youTake": "Você toma",
    "email": "O email",
    "password": "Senha",
    //"cryptoAdress": "Endereço da carteira {{{name}}}",
    "cryptoAdress": "Endereço da carteira de criptografia",
    "externalTransaction": "ID de transação",
    "coupone": "Eu tenho um cupom de desconto",
    "couponeCode": "Código do cupom",
    "cardNumber": "Número do cartão",
    "month": "MM",
    "year": "YY",
    "name": "Nome",
    "fullName": "Nome completo",
    "mobilePhone": "Celular",
    "birth": "Sua data de nascimento no formato MM.DD.YYYY",
    "videoVerification": "Verificação de vídeo",
    "verification": "Verificação",
    "status": "Verificar o status",
    "login": { // TODO: translate
      "captcha": "CAPTCHA",
      "reset": "Restabelecer",
      "forgot": "Esqueceu a senha",
      "signIn": "assinar em"
    },
    "registration": { // TODO: translate
        "create": "Crio"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Verificação simples",
      "title-2": "Mais de 100 altcoins disponíveis",
      "title-3": "Sem taxas ocultas",
      "title-4": "Sem registro"
    },
    "title": "Estamos trabalhando em mais de 100 países",
    "subtitle": "incluindo {{{country}}}",
    "shortTitle": "Estamos trabalhando em mais de 100 países",
    "text1": "Verificação simples",
    "popupText1": "Os compradores pela primeira vez só precisam verificar o número de telefone",
    "text2": "100 altcoins são suportados",
    "popupText2": "Você pode comprar mais de 100 criptografia mais promissoras com um cartão de crédito / débito",
    "text3": "Sem taxas ocultas",
    "popupText3": "Você receberá exatamente a mesma quantidade de altcoins, que foi mostrado antes do acordo",
    "text4": "Nenhum registro",
    "popupText4": "Você não precisa se inscrever, сryptocurrency será enviado para o endereço que você indicou"
  },
  "buying-segment": {
    "title": "Mais de 500.000 clientes",
    "sub-title": "tem vindo a utilizar Indacoin desde 2015"
  },
  "phone-segment": {
      "title": "Baixe nosso aplicativo",
      "text1": "Compre e armazene mais de 100 criptografia em uma carteira",
      "text2": "Acompanhe as mudanças de sua carteira de investimentos",
      "text3": "Enviar criptografia para seu amigo tornou-se tão fácil como enviar uma mensagem"
  },
  "social-segment": {
    "buttonText": "Leia no Facebook",
    "on": "no Facebook",
    "facebook-review-1": "Boa página bitcoins, processo rápido e simples, eu os recomendo",
    "facebook-review-2": "Brilhante serviço e conveniente de usar, o pessoal é amigável e muito útil. Embora seja uma transação digital, há uma pessoa do outro lado (o que pode ser uma ajuda MASSIVE) e assegurando com certeza o serviço de 5star que usarei de novo",
    "facebook-review-3": "Como eu só estou começando a provar tudo o que envolve o mundo da criptografia, fazer uma troca em seu site é mais do que simples. e seu apoio no caso de você ficar preso ou simplesmente procurar ajuda é muito mais do que surpreendente.",
    "facebook-review-4": "Pagamento muito rápido, excelente suporte e excelentes taxas de câmbio.",
    "facebook-review-5": "Eu decidi comprar 100 euros de bitcoins usando este serviço, e foi fácil proceder com o pagamento. Embora eu tivesse alguns problemas e perguntas, quase sempre fui ajudado por sua equipe de suporte. Isso me fez sentir muito mais confortável comprar Bitcoin com Indacoin. E estou planejando comprar mais no futuro.",
    "facebook-review-6": "Acabei de usar a Indacoin para fazer uma transação para a compra da Bitcoin. A transação foi suave, seguida de uma chamada de verificação. É muito fácil de usar e um portal fácil de usar para comprar moedas de bits. Eu recomendaria esta plataforma para comprar bitcoins !!",
    "facebook-review-7": "É difícil encontrar uma facilidade fácil de transação de cartão de crédito para comprar moeda criptográfica e a Indcoin me tratou muito bem, a paciência com usuários de outros países que não entendem seu idioma, chat online de prontidão. Indcoin Obrigado !!!",
    "facebook-review-8": "Rápido e fácil. O suporte on-line foi muito rápido e útil. Dica Pro: usar o aplicativo reduz as cobranças em 9%.",
    "facebook-review-9": "Esta é a segunda vez que uso Indacoin - Ótimo serviço, atendimento ao cliente brilhante de transações rápidas. Definitivamente vou usar novamente.",
    "facebook-review-10": "Extremamente excelente serviço e comunicação, passo a passo, ajuda com o bate-papo ao vivo e totalmente confiável, eles até me ligaram sem perguntar e me explicaram como fazer a verificação para a transação, a maneira mais fácil de comprar o Bitcoin sem problemas, obrigado Indacoin estará fazendo muitas mais transações com você.",
    "facebook-review-11": "Ótimo serviço, processo rápido. Serviço ao cliente útil. Rápido responde. Uma forma simples de comprar moeda cripto. Entre outros serviços, honestamente, esse é o único serviço que uso para comprar moeda cripto. Confio neles para processar minhas compras. Recomendado para todos que lidam com a compra de moeda criptográfica. Por último, mas não menos importante, apenas uma palavra para Indacoin: Excelente !!!",
    //"facebook-review-12": "O IndaCoin possui um serviço Super fácil, confiável e rápido. Eles também foram extremamente rápidos em responder todas as minhas perguntas e me ajudar com a minha transferência. Recomendaria altamente seus serviços a todos! 10/10!",
    "facebook-review-12": "Comprei bitcoins por 200 euros e o site é legítimo. Embora tenha demorado algum tempo para que a transação ocorra, o atendimento ao cliente é excelente. Eu alcancei seu suporte ao cliente e eles imediatamente resolveram meu problema. Definitivamente recomendo este site para todos. às vezes devido ao alto tráfego, o tempo de transação varia entre 8 a 10 horas. Não entre em pânico se você não receber suas moedas instantaneamente às vezes isso levará tempo, mas com certeza você receberá suas moedas.",
    "facebook-review-13": "Ótimo trabalho Indacoin. Tinha a melhor experiência até agora na compra de moedas com meu cartão de crédito com a Indacoin .. O consultor Andrew fez um ótimo serviço ao cliente, e fiquei impressionado com a transferência de menos de 3 minutos ... Continuará usando-os :)",
    "facebook-review-14": "Indacoin é uma interface fácil e amigável que os compradores podem comprar mais facilmente bitcoins na plataforma. É fácil com as ajudas passo a passo dos Agentes Consultores e eles estão realmente dispostos a ajudá-lo a resolver seu problema imediatamente. Eu definitivamente vou encaminhar as pessoas para Indacoin e eles constantemente me atualizam em minhas situações e eles estão dispostos a dar uma força extra para resolver seus problemas. Obrigado Indacoin!",
    "facebook-review-15": "A maneira mais fácil e rápida de comprar criptomoedas que encontrei em muitas pesquisas. Bom trabalho pessoal, obrigado!",
    "facebook-review-16": "Really maneira fácil de comprar moedas online. Ótimo serviço ao cliente também. É incrível que eles têm moedas alt para comprar direto também. Leva as complicações das trocas. Realmente recomendo isso para todos depois da minha primeira experiência positiva.",
    "facebook-review-17": "Ótimo serviço. User Friendly e fácil de comprar várias moedas / fichas.",
    "facebook-review-18": "Esta é uma das melhores plataformas para trocar BTC. Eu fiz várias trocas e fui rápido.",
    "facebook-review-19": "Ótimo serviço, fácil de usar, fácil de comprar vários tokens cryptocurrency e ERC20 com seu cartão de crédito ou débito. Muito rápido e também seguro! O serviço e plataforma que eu tenho procurado! Recomendo totalmente o uso de Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "Mais de {{{count}}} criptografia diferentes estão disponíveis para compra na {{{name}}}"
  },

  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin no seu telefone",
    "text": "Nosso Indacoin de carteira inteligente está disponível para usuários de Android, iOS e em todos os navegadores da Internet. Enviar moeda de criptografia para o seu amigo tornou-se tão fácil como enviar uma mensagem"
  },

  "news-segment": {
    "no-news": "Não há notícias para o seu idioma",
    "news-error": "Pedimos desculpas, a notícia está indisponível no momento",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },
  
  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "Este cryptocurrency não está disponível no momento, mas estará de volta em breve",
    "unknown": "Erro de servidor desconhecido",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "O limite excedeu",
    "phone": "Phone is not supported"
  },
  "tags": {
    "title-btc": "Troque o Bitcoin BTC para {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Compre [cryptocurrency] com um cartão de crédito ou débito em [country] de forma insidiosa - Indacoin",
    "title2": "Compre Bitcoin com cartão de crédito ou débito em todo o mundo - Indacoin",
    "title3": "Compre {{{cryptocurrency}}} com um cartão de crédito ou débito em {{{country}}} de forma insidiosa - Indacoin",
    "title4": "Compre criptomoeda com cartão de crédito ou débito em {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Programa afiliado Indacoin",
      "news": "Últimas notícias Indacoin",
      "help": "Troca de criptomoeda F.A.Q. - Indacoin",
      "api": "API de troca de criptomoeda - Indacoin",
      "terms-of-use": "Termos de uso e política AML - Indacoin",
      "login": "Iniciar sessão - Indacoin",
      "register": "Registre-se - Indacoin",
      "locale": "Mudar de região - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) Gráficos de preços e volume de negócios - Indacoin"
    },
    "descriptions": {
      "affiliate": "O Programa de Afiliados da Indacoin permite que você ganhe até 3% de cada compra com um cartão de crédito / débito. Remuneração será enviada para sua carteira de bitcoin Indacoin",
      "news": "Visite nosso blog oficial de notícias para ficar em contato com as últimas notícias sobre a indústria de criptomoedas! Uma abundância de informações úteis e comentários abrangentes de especialistas da Indacoin em um só lugar!",
      "help": "Encontre as respostas para todas as suas perguntas sobre como comprar, vender e trocar criptomoedas na Indacoin.",
      "api": "Nossa API fornece compra e troca de criptomoedas instantaneamente com Visa e Mastercard. Verifique Integração e Exemplos.",
      "terms-of-use": "Informação geral. Termos de uso. Serviços prestados. Transferência de dinheiro. Transferência de direitos. Comissões Política AML.",
      "login": "Faça o login ou crie uma nova conta",
      "register": "Criar nova conta",
      "locale": "Escolha a sua região, selecione esta opção e nós nos lembraremos da sua região na próxima vez que visitar a Indacoin",
      "currency": "Preços atuais para {{{fullCurrency}}} para USD com volume de negociação e informações históricas sobre criptomoeda"
    }
  },
  "usAlert": "Desculpe, mas Indacoin não opera nos Estados Unidos da América",
  "wrongPhoneCode": "Você digitou um código errado do seu telefone",
  "skip": "Pular",
  "minSum": "Valor mínimo para comprar",
  "currency": {
    "no-currency": "Para uma determinada criptomoeda, o gráfico não está disponível",
    "presented": {
      "header": "Bitcoin foi apresentado ao mundo em 2009 e em 2017 continua a ser a principal criptomoeda do mundo"
    },
    "difference": {
      "title": "Como o Bitcoin é diferente do dinheiro comum?",
      "header1": "Emissão limitada",
      "text1": "O número total de Bitcoins que podem ser extraídos é de 21M. Mais de 16 milhões de Bitcoins estão em circulação agora. Quanto mais popular, mais caro.",
      "header2": "Moeda Mundial",
      "text2": "Nenhum governo tem controle sobre o Bitcoin. É baseado apenas em matemática",
      "header3": "Anonimato",
      "text3": "Você pode usar seus Bitcoins a qualquer momento e em qualquer quantidade. Esta é uma constatação verdadeira, em oposição à transferência de dinheiro da sua conta bancária, quando você precisa explicar o destino do pagamento, bem como a origem dos fundos.",
      "header4": "Rapidez",
      "text4": "Seu banco levará tempo para transferir dinheiro, especialmente quando se trata de grandes valores. Com o Bitcoin, a rotina de envio e recebimento levará segundos.",
      "header5": "Transparência",
      "text5": "Todas as transações de Bitcoin podem ser vistas no BlockChain."
    },
    "motivation": {
      "title": "Sendo a moeda primitiva de seu tipo, ela conseguiu mudar a visão do dinheiro como nós conhecíamos. Até agora, o valor de mercado da Bitcoin chega a mais de US $ 70 bilhões, superando gigantes como Tesla, General Motors e FedEx."
    },
    "questions": {
      "header1": "O que é uma moeda digital e criptomoeda?",
      "text1": "Criptomoeda é um ativo digital, que funciona como um meio de troca usando criptografia para proteger as transações e controlar a criação de unidades adicionais da moeda. Não tem forma física e é descentralizada em oposição aos sistemas bancários usuais",
      "header2": "Como ocorre a transação Bitcoin?",
      "text2": "Cada transação tem 3 dimensões: Entrada - a origem do Bitcoin usado na transação. Diz-nos onde o dono atual conseguiu. Quantidade - o número de “moedas” usadas. Saída - o endereço do destinatário do dinheiro.",
      "header3": "De onde é que o Bitcoin veio?",
      "text3": "Embora o mundo esteja familiarizado com o nome do criador do Bitcoin, “Satoshi Nakamoto” é provavelmente um apelido para um programador ou mesmo um grupo de tal. Basicamente, o Bitcoin vem de 31.000 linhas de código criadas por “Satoshi” e é controlado apenas por software.",
      "header4": "Como o Bitcoin funciona?",
      "text4": "As transações ocorrem entre as carteiras Bitcoin e são transparentes. No blockchain, qualquer um pode verificar a origem e o ciclo de vida de cada “moeda”. No entanto, não há \"moeda\". Bitcoin não tem forma física e não tem forma virtual, apenas histórico de transações.",
      "header5": "O que é uma carteira Bitcoin?",
      "text5": "Uma carteira Bitcoin é um programa que permite gerenciar o Bitcoin: recebê-los e enviá-los, verificar o saldo e a taxa de Bitcoins. Embora não exista “moeda” em tais carteiras, ele contém chaves digitais, usadas para acessar endereços públicos de Bitcoin e transações de “assinatura”. Carteiras populares:",
    },
    "history": {
      "title": "A breve história do Bitcoin",
      "text1": "Segundo a lenda, é quando Satoshi Nakamoto inicia seu trabalho no Bitcoin",
      "text2": "Primeira transação feita no Bitcoin. O valor do bitcoin, baseado no custo da eletricidade usada para gerar Bitcoin, é fixado em 1 USD para 1.306.03 BTC",
      "text3": "25% de todos os Bitcoins já foram gerados",
      "text4": "Dell e Windows começam a aceitar Bitcoin",
      "text5": {
        "part1": "O preço do BTC aumenta de 1,402 USD para 19,209 USD em poucos meses.",
        "part2": "Taxa de Bitcoin atinge 20.000 USD por moeda no dia 18 de dezembro de 2017"
      },
      "text6": "O registro do bitcoin.org",
      "text7": {
        "part1": "Um momento verdadeiramente histórico, quando 10.000 BTC é usado para comprar indiretamente 2 pizzas. Hoje você pode comprar 2.500 pizzas de pepperoni pelo mesmo valor.",
        "part2": "O preço do Bitcoin sobe de 0,008 USD para 0,08 USD por 1 BTC em apenas 5 dias.",
        "part3": "Primeira transação móvel ocorre"
      },
      "text8": {
        "part1": "O Tesouro dos EUA classifica o BTC como uma moeda virtual descentralizada conversível.",
        "part2": "O primeiro Bitcoin ATM encontrado em San Diego, Califórnia.",
        "part3": "O preço do Bitcoin dobra e atinge 503.10 USD. Ela cresce para 1.000 USD no mesmo mês"
      },
      "text9": "O juiz federal dos EUA exclui que os BTC sejam “fundos dentro do significado simples desse termo”"
    },
    "graph": {
      "buy-button": "Comprar"
    },
    "ethereum": {
      "presented": {
        "header": "Nós já temos o Bitcoin, então por que precisamos do Ethereum? Vamos cavar isso! O Bitcoin garante transferências de dinheiro justas e transparentes. Ether, a moeda digital da Ethereum, pode desempenhar o mesmo papel. No entanto, há muito mais para a funcionalidade, oferecida pela Ethereum."
      },
      "questions": {
        "header1": "O que é o Ethereum?",
        "text1": "Ethereum é uma plataforma de código aberto, que usa tecnologia blockchain para permitir computação distribuída. Ele é garantido por contratos inteligentes e apresenta sua própria criptomoeda, chamada \"ether\", como meio de pagamento.",
        "header2": "De onde veio?",
        "text2": "Ethereum foi oferecido pela primeira vez como uma idéia no final de 2013 pelo programador canadense, Vitalik Buterin. Financiado por um crowdsale que aconteceu durante o verão de 2014, a plataforma foi finalmente apresentada ao mundo em julho de 2015.",
        "header3": "O que é o ICO?",
        "text3": "Muitas aplicações são construídas na Ethereum e também podem levantar fundos com o Ether em um processo que é chamado de oferta inicial de moeda (ICO). O modo como funciona é bem parecido com o que o Kickstarter faz. Os programadores oferecem diferentes tokens de valor em troca de fundos (geralmente Ether) para cobrir as despesas de desenvolvimento do aplicativo. Quando eles atingem um determinado valor predeterminado ou uma determinada data ocorre, os fundos são liberados para programadores. Se a meta não for atingida, os fundos retornarão aos contribuintes iniciais.",
        "header4": "O que é o éter?",
        "text4": "Ether (ETH) é uma moeda digital e um token de valor usado no Ethereum. Além de ser o meio de pagamento para diferentes serviços na plataforma Ethereum, ele também é apresentado nas trocas de criptomoedas e pode ser trocado por dinheiro real.",
        "header5": "O que há de tão especial em \"contratos inteligentes\" e qual é a diferença entre os contratos habituais?",
        "text5": "Cada transação tem 3 dimensões: Entrada - a origem do Bitcoin usado na transação. Diz-nos onde o dono atual conseguiu. Quantidade - o número de “moedas” usadas. O principal objetivo do contrato inteligente é garantir que todos os termos de um contrato sejam executados corretamente. Sua funcionalidade descentralizada fornece mais segurança para os contribuidores da Ethereum, usando aplicativos imparciais, armazenados na blockchain, para verificar e impor seus termos. Essa tecnologia quase não deixa espaço para fraudes. E enquanto para o contrato usual você geralmente precisa de um terceiro (advogado) para lidar com quaisquer questões, como violação, contratos inteligentes são imparciais e usar a matemática para fornecer segurança.",
        "header6": "Como o Ethereum é diferente do Bitcoin?",
        "text6": "Inspirada pelo Bitcoin, a Ethereum também usa a tecnologia Blockchain, sendo hospedada pelos computadores pessoais de programadores voluntários de todo o mundo. Para este trabalho, eles recebem tokens de valor - Ether. O preço do Ethereum é estabelecido por trocas entre pessoas, assim como é com o Bitcoin. A diferença é enorme embora. Ethereum é, antes de tudo, uma plataforma para computação distribuída. Ele fornece uma oportunidade para os programadores criarem vários aplicativos descentralizados (Dapps) em sua base, obter fundos para despesas operacionais. Desta forma, não é necessária uma gestão intermédia e, por isso, não há despesas adicionais. A plataforma Ethereum também utiliza contratos inteligentes como formas de facilitar as relações entre as partes e sua própria criptomoeda, “éter”, como meio de pagamento. É justo dizer que, embora tanto o Bitcoin quanto o Ethereum sejam grandes e tenham algumas semelhanças, seus propósitos são inicialmente diferentes. O primeiro é apenas uma moeda, enquanto o segundo usa apenas Éter principalmente para seus propósitos internos.",
        "header7": "Quais são as carteiras mais populares da Ethereum?",
        "text7": "Uma carteira Ethereum é um programa que permite gerenciar sua criptomoeda: recebê-las e enviá-las, verificar o saldo e a taxa de câmbio do Ethereum. Carteiras populares:"
      },
      "history": {
        "title": "A breve história da Ethereum",
        "text1": "Crowdsale é iniciado para reunir fundos para criar Ethereum",
        "text2": "A organização autônoma descentralizada (DAO) é criada na plataforma Ethereum. Financiado por um crowdsale simbólico, é no momento a maior campanha de crowdfunding da história",
        "text3": "Ethereum ganha 50% de participação de mercado na oferta inicial de moedas",
        "text4": "Taxa de câmbio Ethereum atinge 400 USD (aumento de 5.000% desde janeiro)",
        "text5": "A ideia da Ethereum é oferecida por Vitalik Buterin, um programador canadense e fundador da Bitcoin Magazine",
        "text6": "30 de julho plataforma fica online",
        "text7": "",
        "text8": "Enterprise Ethereum Alliance is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc",
        "text9": {
          "part1": "All cryptocurrencies drop after China bans ICO, but quickly recover afterward.",
          "part2": "Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "Você está pronto para permitir que seus clientes comprem criptografia com um cartão de crédito / débito? Por favor, deixe seu e-mail e entraremos em contato o mais cedo possível",
        "input": "Endereço de e-mail",
        "button": "iniciar",
        "mail-success": "Obrigado. Nós responderemos a você"
      },
      "api": {
        "title": "Integração API",
        "text": "Usando nossa API, você pode permitir que seus clientes comprem criptomoedas instantaneamente do seu site. Nossa plataforma antifraude, integrada em sua página da Web ou no aplicativo móvel, também pode ser usada como uma ferramenta que permite monitorar e filtrar os pagamentos feitos em sua plataforma."
      },
      "affiliate": {
        "title": "Programa de Afiliados",
        "text": "Seja recompensado por aconselhar a Indacoin a outros clientes. Você estará ganhando até 3% das compras das referências.",
        "block-1": {
          "title": "Como posso participar do Programa de Afiliados da Indacoin?",
          "text": "Sinta-se à vontade para se cadastrar em nosso site."
        },
        "block-2": {
          "title": "Como poderei retirar os fundos coletados na minha conta?",
          "text": "Você pode comprar BTC / ETH / outras altcoins na Indacoin sem qualquer taxa ou recebê-las no seu cartão bancário."
        },
        "block-3": {
          "title": "Qual é a vantagem do seu Programa de Afiliados?",
          "text": "Em nossa plataforma, os clientes têm a oportunidade de comprar mais de 100 moedas diferentes usando cartões de crédito / débito sem registro. Considerando o fato de que as compras ocorrem quase que imediatamente, os parceiros podem obter seu lucro instantaneamente."
        },
        "block-4": {
          "title": "Posso ver o exemplo do link de referência?",
          "text": "Claro, é assim que parece: ({{{link}}})"
        }
      },
      "more": "Descubra mais",
      "close": "Fechar"
    },
    "achievements": {
      "partnership-title": "Parceria",
      "partnership-text": "Existem muitas maneiras de colaborar com a Indacoin. O Programa de Parcerias da Indacoin é para empresas relacionadas à criptografia que estão dispostas a conectar o mundo das criptomoedas com o sistema tradicional de moeda fiduciária para aumentar o envolvimento na indústria de criptografia. Com a Indacoin, você será capaz de fornecer aos seus clientes a experiência personalizada da criptografia de compra com os cartões de crédito / débito.",
      "block-1": {
        "header": "Confiável",
        "text": "Trabalhamos no mercado de criptografia desde 2013 e nos associamos a muitas empresas respeitadas que lideram o setor. Mais de 500.000 clientes de mais de 190 países usaram nosso serviço para comprar moedas criptografadas."
      },
      "block-2": {
        "header": "Seguro",
        "text": "Criamos uma plataforma anti-fraude inovadora e altamente eficiente com uma tecnologia central desenvolvida especialmente para a detecção e prevenção de atividades fraudulentas para projetos de criptografia."
      },
      "block-3": {
        "header": "Solução pronta para uso",
        "text": "Nosso produto já está sendo usado pelas OICs, Câmbios de Criptomoeda, Plataformas Blockchain e Fundos Crypto."
      },

      "header": "Ganhe dinheiro conosco no avanço",
      "subheader": "Tecnologias Blockchain",
      "text1": "A {{{company}}} é uma plataforma global que permite que as pessoas compram Bitcoin, Ethereum, Ripple, Waves e outras criptografia diferentes com cartão de crédito ou débito.",
      "text2": "O serviço vem funcionando desde 2013 em todo o mundo, incluindo, entre outros:",
      "text3": "Reino Unido, Canadá, Alemanha, França, Rússia, Ucrânia, Espanha, Itália, Polônia, Turquia, Austrália, Filipinas, Indonésia, Índia, Bielorrússia, Brasil, Egito, Arábia Saudita, Emirados Árabes Unidos, Nigéria e outros"
    },
    "clients": {
      "header": "Mais de 500 000 clientes",
      "subheader": "desde 2013"
    },
    "benefits": {
      "header": "Nossos principais benefícios para as empresas:",
      "text1": "Serviço exclusivo que permite aos clientes comprar e enviar bitcoins sem registro. Portanto, os clientes que vierem à Indacoin após o link de referência terão que apenas inserir os detalhes do cartão e o endereço da carteira, onde o criptograma deve ser enviado. Portanto, clientes potenciais farão as compras imediatamente e nossos parceiros obterão lucros.",
      "text2": "Até 3% de cada transação são ganhos por nossos parceiros. Além disso, os webmasters receberão 3% de todas as futuras compras da remessa.",
      "text3": "Bitcoin e cartões bancários poderiam ser usados para retirar a renda."
    },
    "accounts": {
      "header": "Junte-se a nós agora",
      "emailPlaceholder": "Insira o seu endereço de email",
      "captchaPlaceholder": "CAPTCHA",
      "button": "iniciar"
    }
  },
  "faq": {
    "title": "Perguntas e respostas",
    "header1": "Questões gerais",
    "header2": "Cryptocurrency",
    "header3": "Verificação",
    "header4": "Comissões",
    "header5": "Compra",
    "question1": {
      "title": "O que é Indacoin?",
      "text": "Somos uma empresa que trabalha em campo de criptografia desde 2013, com sede em Londres, no Reino Unido. Você pode usar nosso serviço para comprar mais de 100 criptografia diferentes através do pagamento por cartão de crédito / débito sem registro."
    },
    "question2": {
      "title": "Como posso entrar em contato com você?",
      "text": "Você pode entrar em contato com nossa equipe de suporte por e-mail (support@indacoin.com), telefone (+ 44-207-048-2582) ou usando uma opção de bate-papo ao vivo em nossa página."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "Quais criptografia posso comprar aqui?",
      "text": "Você pode comprar mais de 100 criptografia diferentes que estão todas listadas no nosso site."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "Quais países podem usar esse serviço?",
      "text": "Trabalhamos com todos os países, com a única exceção dos EUA (no momento)"
    },
    "question7": {
      "title": "Posso vender bitcoin aqui?",
      "text": "Não, você só pode comprar bitcoins aqui."
    },
    "question8": {
      "title": "O que é Cryptocurrency?",
      "text": "Сryptocurrency é uma moeda digital que usa criptografia para segurança, recurso, o que dificulta a falsificação. Não é emitida por nenhuma autoridade central, tornando-a teoricamente imune à interferência ou manipulação do governo."
    },
    "question9": {
      "title": "Onde posso armazenar cryptocurrency?",
      "text": "Você pode configurar uma carteira separada para cada cryptocurrency em seu PC ou você pode usar o nosso serviço de carteira grátis e armazenar mais 100 criptografia em um só lugar."
    },
    "question10": {
      "title": "O que você quer dizer com \"endereço bitcoin\"?",
      "text": "É o endereço da sua carteira bitcoin. Você também pode inserir o endereço de uma pessoa para a qual você deseja enviar bitcoins ou criar uma carteira na Indacoin"
    },
    "question11": {
      "title": "Quão rápida é a transação cryptocurrency?",
      "text": "Os tempos de transação Cryptocurrency dependem de quais cryptocurrency você está enviando, as transações Bitcoin levam cerca de 15 a 20 minutos para completar e, em média, permitem até 1 hora para o resto das moedas."
    },
    "question12": {
      "title": "O que devo fazer para verificar meu cartão?",
      //"text": "Para completar a verificação, você precisará inserir o código de 4 dígitos que você recebe por meio de chamada telefônica. Para o seu segundo passo, você também pode solicitar uma verificação de vídeo, onde você grava um vídeo, mostrando seu rosto e enviando uma varredura ou foto da sua licença de identificação / passaporte / motorista. Dependendo da sua verificação, esta etapa pode ser obrigatória, pois precisamos ter certeza de que você é o titular do cartão."
      "text": "Para concluir a verificação, você precisará inserir o código de 4 dígitos recebido por telefone. Para sua segunda etapa, você também pode ser solicitado a passar por uma verificação de foto, que exige que você faça o upload de uma foto do seu passaporte / ID e uma selfie com ID e um pedaço de papel com o texto necessário, confirmando a compra. Como alternativa, podemos solicitar a verificação por vídeo, em que você precisará registrar seu rosto e dizer: \"Indacoin verification for crypto\" mostre sua identidade e cartão com os quais você efetuou o pagamento. Ao mostrar o cartão, mostre apenas os últimos 4 dígitos e o nome."
    },
    "question13": {
      "title": "E se eu não conseguir gravar uma verificação de vídeo?",
      "text": "Você pode abrir o mesmo link da página de pedidos do seu dispositivo móvel e gravá-lo a partir daí. Se tiver problemas, você também pode enviar o vídeo solicitado como anexo por e-mail para support@indacoin.com. Certifique-se de Indicar seu número de ordem de troca."
    },
    "question14": {
      "title": "Eu fiz um pedido e entrei todos os detalhes, por que ainda está sendo processado?",
      "text": "Se você esperou mais de 2 minutos, a ordem não chegou a transação. É possível que seu cartão não seja 3D seguro e talvez seja necessário tentar novamente. Certifique-se de confirmar sua compra via código PIN com o seu banco ou pode tentar comprar com uma moeda diferente (EUR / USD). Caso contrário, se você tiver certeza de que seu cartão é 3D e a mudança de moeda não ajuda, então você precisa entrar em contato com seu banco, eles podem estar bloqueando suas tentativas de compra."
    },
    "question15": {
      "title": "Preciso passar pela verificação sempre que faço uma compra?",
      "text": "Você só precisa verificar seu cartão uma vez e todas as transações seguintes serão automáticas. No entanto, exigiremos a verificação se você decidir usar outro cartão bancário."
    },
    "question16": {
      "title": "Meu pedido foi recusado pelo banco, o que devo fazer?",
      "text": "Você precisa entrar em contato com seu banco e pedir o motivo do declínio, talvez eles possam levantar a restrição para que uma compra possa ser feita."
    },
    "question17": {
      "title": "Por que preciso verificar meu cartão bancário ou enviar um vídeo?",
      "text": "O processo de verificação protege você em caso de roubo de cartões, hackers ou fraudadores; Isso funciona da maneira oposta no caso de alguém tentar fazer uma compra usando o cartão bancário de outra pessoa."
    },
    "question18": {
      "title": "Como posso confiar em que meus detalhes e informações do cartão são seguras?",
      "text": "Nunca compartilhamos seus detalhes com terceiros sem o seu consentimento. Além disso, nós só aceitamos cartões 3D-Secure, e nunca solicitamos nenhum dado sensível, como o código CCV ou o número completo do cartão. Você fornece apenas esses detalhes de pagamento através de terminais de gateway Visa ou Mastercard. Nós apenas coletamos as informações necessárias para garantir que você seja o proprietário do cartão."
    },
    "question19": {
      "title": "Qual é a sua taxa?",
      "text": "Nossa taxa varia de compra para compra, pois é baseada em vários fatores, é por isso que criamos uma calculadora que indicará o valor exato da cryptocurrency que você receberá, todas as taxas incluídas."
    },
    "question20": {
      "title": "Quanto me cobrarão pela transação?",
      "text": "Você será cobrado apenas o valor que você especificou durante o seu pedido em nossa calculadora. Todas as taxas estão incluídas nesse montante."
    },
    "question21": {
      "title": "Você tem algum desconto?",
      "text": "Nós, ocasionalmente, temos descontos, você pode obter mais informações de nossa equipe de suporte."
    },
    "question22": { // TODO: link
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "Quão rápida será minha cryptocurrency ser enviada?",
      "text": "Geralmente, demora cerca de 30 minutos depois de fazer um pedido ou completar a verificação (se é a primeira vez que usa o cartão no Indacoin)."
    },
    "question24": {
      "title": "Quais são as compras mínimas e máximas?",
      "text": "Você terá o limite máximo de US $ 200 para a primeira transação, US $ 200 adicionais para a segunda transação disponível após 4 dias da compra inicial, US $ 500 após 7 dias e US $ 2000 em 14 dias da primeira compra. Em um mês a partir da sua primeira compra, não há limites ao pagar com o seu cartão. Tenha em mente que o limite mínimo é sempre de $ 50."
    },
    "question25": {
      "title": "Que tipo de cartões são aceitos?",
      "text": "Aceitamos Visa e Mastercard com segurança 3D."
    },
    "question26": {
      "title": "O que é 3D seguro?",
      "text": "A tecnologia 3D Secure consiste nos programas verificados pela Visa e MasterCard SecureCode. Depois de inserir os detalhes do seu cartão de crédito em nossa loja online, aparecerá uma nova janela, solicitando seu código de segurança pessoal. Sua instituição financeira autenticará a transação em segundos, bem como confirmará que você é o indivíduo fazendo a compra."
    },
    "question27": {
      "title": "Que outros sistemas de pagamento posso usar para comprar bitcoins?",
      "text": "Atualmente, aceitamos apenas pagamentos com cartão Visa / Master na nossa plataforma."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": { // TODO: translate
    "message": "O valor deve estar entre {{{limitsMin}}} {{{limitsCur}}} e {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "O valor deve ser de {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">Este site usa cookies para garantir que você obtenha a melhor experiência em nosso site.</span> {{{link}}}",
      "accept-button": "Consegui",
      "link": "Saber mais"
    },
    "success-video": {
      "text": "Obrigado! Agora começamos o processo de verificação, o que pode levar algum tempo. Quando a verificação for concluída, as moedas criptográficas serão entregues a você"
    },
    "accept-agreement": {
      //"text": "Por favor, indique que leu e concorda com o Acordo do Usuário e a Política de AML"
      "text": "Por favor, indique que leu e concorda com os Termos de Uso e Política de Privacidade"
    },
    "price-changed": {
      "header": "Preço alterado",
      "agree": "Aceita",
      "cancel": "Cancelar"
    },
    "neoAttention": "Devido aos recursos da moeda de criptografia NEO, apenas números inteiros estão disponíveis para compra",
    "login": {
      "floodDetect": "Detecção de inundação",
      "resetSuccess": "Redefinir o sucesso da senha. Por favor verifique seu email",
      "wrongEmail": "Este e-mail ainda não está registrado",
      "unknownError": "Erro de servidor. Por favor, tente novamente o pedido",
      "alreadyLoggedIn": "Você já está logado",
      "badLogin": "login ou senha errada",
      "badCaptcha": "CAPTCHA errado",
      "googleAuthenticator": "Insira o código do google authenticator"
    },
    "registration": {
      "unknownError": "Erro desconhecido",
      "floodDetect": "Detecção de inundação",
      "success": "Registre o sucesso. Por favor verifique seu email",
      "badLogin": "login ou senha errada",
      "badCaptcha": "CAPTCHA errado",

      "empty": "Nem todos os campos estão completos. Por favor, preencha todos",
      "repeatedRequest": "A Confirmação do Registro foi enviada para o seu endereço de e-mail. Certifique-se de que o recebeu. Por favor, verifique também uma pasta de spam",
      "badEmail": "Você usou o endereço de e-mail incorreto ou temporário",
      "blockedDomain": "Por favor, tente um endereço de e-mail diferente para se registrar"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "Nenhum resultado encontrado",
  "locale": {
    "title": "Por favor escolha o seu país"
  }
}