export default {
  'lang': 'es',
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    } 
  },
  "recordVideo": {
    "send": "Enviar",
    "stop": "Detener",
    "record": "Grabar"
  },
  "links": {
    "terms-of-use": {
      "text": "Acepto los {{{terms}}} y la {{{policy}}}",
      "link-1": "Términos de Uso",
      "link-2": "Política de privacidad"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "Esperando su pago",
      "status-2": "Intercambio",
      "status-2-sub": "Estamos buscando la mejor tarifa para usted",
      "status-3": "Enviando a su billetera",
      "transaction-completed": "Felicidades, su transacción se ha completado!",
      "rate": "Tasa de cambio 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Has enviado:",
      "toAddress": "al {{{cryptoAdress}}}",
      "receiveText": "Usted recibirá:"
    },
    "changeCrypto": {
      "send": "Envíe {{{amount}}} BTC a la dirección siguiente",
      "confirmation": "Después de dos confirmaciones de bitcoin, ≈ {{{amount}}} {{{shortName}}} se enviarán a su dirección {{{wallet}}} ...",
      "copy": "Copiar dirección",
      "copied": "Copiado al portapapeles"
    },
    "order": "Pedido",
    "status": {
      "title": "Estado",
      "completed": "La solicitud se ha procesado"
    },
    "payAmount": {
      "title": "Pagado"
    },
    "check": "Compruebe el estado de la transacción en blockexplorer.net",
    "resend-phone-code": "¿No has recibido una llamada?",
    "sms-code": "Responda el teléfono e ingrese el código de 4 dígitos aquí",
    "registration": "Ir a la página de registro",

    "internalAccount-1": "Verifique su billetera Indacoin al",
    "internalAccount-link": "iniciar sesión",
    "internalAccount-2": "",
    "your-phone": "tu teléfono {{{phone}}}",
    "many-requests": "Lo siento. A muchas solicitudes de cambio de teléfono."
  },
  "app":{
    "detect_lang":{
      "title":"Detectando su idioma",
      "sub":"Espere..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Compre e intercambie {{{longName}}} instantáneamente y 100 otras criptomonedas al mejor precio en {{{country}}}",
    "title":"Indacoin: compre criptomoneda con una tarjeta de crédito o débito",
    "description":"Compre y cambie cualquier criptomoneda al instante: [cryptocurrency], Ethereum, Litecoin, Ripple y 100 monedas digitales más para EUR o USD.",
    "description-bitcoin":"Compre e intercambie cualquier criptomoneda instantáneamente: Bitcoin, Ethereum, Litecoin, Ripple y otras 100 monedas digitales por EUR o USD",
    "description-ethereum":"Compre e intercambie cualquier criptomoneda al instante: Ethereum, Bitcoin, Litecoin, Ripple y otras 100 monedas digitales por EUR o USD",
    "description-litecoin":"Compre e intercambie cualquier criptomoneda al instante: Litecoin, Bitcoin, Ethereum, Ripple y otras 100 monedas digitales por EUR o USD",
    "description-ripple":"Compre e intercambie cualquier criptomoneda instantáneamente: ondulación, Bitcoin, Ethereum, Litecoin y otras 100 monedas digitales por EUR o USD",
    
    "description2": "Compre e intercambie cualquier currículum criptográfico instantáneamente: Bitcoin, Ethereum, Litecoin, Ripple y otras 100 monedas digitales por EUR o USD",
    "description3":"Compre e intercambie cualquier criptomoneda instantáneamente: Bitcoin, Ethereum, Litecoin, Ripple y otras 100 monedas digitales con Visa y Mastercard",
    "buy": "Compre",
    "subTitle": "con una tarjeta de crédito o débito",
    "more": "En más de 100 países, incluida",
    //"header": "Cambiar Bitcoin a <span class='sub'>{{{name}}}</span>",
    "header": "Intercambia instantáneamente <span class='sub'>BTC</span> a <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "COMPRE CRIPTOMONEDA AL INSTANTE",
    "header-alt": "Criptomoneda de Exchange al instante",
    "sub": "Indacoin proporciona una forma fácil de comprar Bitcoins y más de 100 criptomonedas con Visa y Mastercard",
    "sub-alt": "Indacoin proporciona una manera fácil de intercambiar bitcoin a cualquiera de las 100+ criptomonedas diferentes",
    "observe": "Más información sobre las criptomonedas disponibles a continuación"
  },
  "navbar": {
    "main":"Página de inicio",
    "btc": "Comprar Bitcoin",
    "eth": "Comprar Criptomoneda",
    "affiliate": "Asociación",
    "news": "Noticias",
    "faq": "Preguntas frecuentes",
    "login": "Iniciar sesión",
    "registration": "Registrarse",
    "platform": "Productos",

    "logout": "Cerrar sesión",
    "buy-instantly": "Comprar crypto al instante",
    "trading-platform": "Plataforma de negocios",
    "mobile-wallet": "Billetera móvil",
    "payment-processing": "API de procesamiento de pagos"
  },
  "currency-table": {
    "rank": "Rango",
    "coin": "Moneda",
    "market": "Capitalización bursátil",
    "price": "Coste",
    "volume": "Volumen, 24 h",
    "change": "Cambio, 24 h",
    "available-supply": "Suministro disponible",
    "search-cryptocurrency": "Buscar criptomonedas",

    "change-1h": "1H Cambio",
    "change-24h": "1D Cambio",
    "change-7d": "1W Cambio"
  },
  "buy-blocks":{
    "give":"Usted paga",
    "get":"Usted recibe"
  },
  "exchange-form": {
    "title": "Compre criptomoneda al instante",
    "submit": "Cambio",
    "give": "Usted paga",
    "get": "Usted recibe",
    "search": "Buscar",
    "error-page": "Nos disculpamos, pero la compra no está disponible en este momento"
  },
  "pagination": {
    "page": "Página",
    "next": "Siguiente",
    "prev": "Atrás",
    "first": "Volver arriba"
  },
  "sidebar":{
    "accept":{
      "title":"Aceptamos tarjetas de crédito",
      "button":"Comprar criptomoneda"
    },
    "youtube":{
      "title":"Nosotros",
      "play":"Inicio"
    },
    "orders":{
      "title": "Gestión del pedido",
      "order": "Pedido nº",
      "confirm": "Código de confirmación",
      "check": "Verificar pedido",
      "new": "Crear nuevo pedido",
      "or": "o"
    },
    "social":{
      "title":"Social"
    }
  },
  "wiki":{
    "etymology":"Etimología"
  },
  "loading": {
    "title": "Cargar",
    "sub": "Espere",
    "text": "Cargar"
  },
  "error": {
    "title": "Error",
    "sub": "¡Vaya! Ha ocurrido algo",
    "text": "Se ha producido un error desconocido. Ya lo estamos arreglando. Vuelva más tarde.",
    "home": "Inicio",
    "mainPageError1": "Desafortunadamente, la información sobre criptomonedas disponibles no está disponible",
    "mainPageError2": "Intenta visitar el sitio un poco más tarde",
    "page-404": "Nos disculpamos, página no encontrada",
    "page-error": "Pedimos disculpas, ha ocurrido un error."
  },
  "features":{
    "title":"¿Qué se puede<br><span class=\"offset\">hacer en <span class=\"blue\">Indacoin</span>?</span>",
    "column1":{
      "title":"Comprar criptomoneda",
      "text":"En Indacoin puede comprar más de 100 monedas criptográficas diferentes al instante con una tarjeta de crédito o débito"
    },
    "column2":{
      "title":"Más información sobre criptomonedas",
      "text":"Con nuestras sencillas guías podrá entender mejor los principios fundamentales de cualquier moneda alternativa"
    },
    "column3":{
      "title":"Tome decisiones de inversión",
      "text":"Con las potentes herramientas analíticas podrá aprovechar el impulso de las entradas y salidas"
    },
    "column4":{
      "title":"Compre bitcoin sin registrarse",
      "text":"Los bitcoins se enviarán a la dirección que haya indicado cuando se haya completado el pago"
    },
    "column5":{
      "title":"Diga lo que piensa",
      "text":"Únase a nuestro debate sobre las últimas criptotendencias con otros inversores líderes"
    },
    "column6":{
      "title":"Almacene todas los activos en el mismo lugar",
      "text":"Puede almacenar y gestionar más de 100 divisas digitales en un monedero Indacoin"
    }
  },
  "mockup":{
    "title":"MONEDEROS MÓVILES",
    "text":"Nuestro monedero Indacoin sencillo e inteligente funciona con Android o iPhone, así como con su navegador. Ahora enviar criptomoneda al teléfono de un amigo es tan fácil como enviar un mensaje de texto."
  },
  "footer":{
    "column1":{
      "title":"Comprar",
      "link1":"Comprar Bitcoin",
      "link2":"Comprar Ethereum",
      "link3":"Comprar Ripple",
      "link4":"Comprar Bitcoin Cash",
      "link5":"Comprar Litecoin",
      "link6":"Comprar Dash"
    },
    "column2":{
      "title":"Información",
      "link1":"Nosotros",
      "link2":"Preguntas",
      "link3":"Condiciones de uso",
      "link4":"Instrucciones"
    },
    "column3":{
      "title":"Herramientas",
      "link1":"API",
      "link2":"Cambia región",
      "link3":"App móvil"
    },
    "column4":{
      "title":"Contactos"
    }
  },
  "country":{
    "AF":{
      "name":"Afganistán"
    },
    "AX":{
      "name":"Islas Aland"
    },
    "AL":{
      "name":"Albania"
    },
    "DZ":{
      "name":"Argelia"
    },
    "AS":{
      "name":"Samoa Americana"
    },
    "AD":{
      "name":"Andorra"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguila"
    },
    "AQ":{
      "name":"Antártida"
    },
    "AG":{
      "name":"Antigua y Barbuda"
    },
    "AR":{
      "name":"Argentina"
    },
    "AM":{
      "name":"Armenia"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Australia"
    },
    "AT":{
      "name":"Austria"
    },
    "AZ":{
      "name":"Azerbaiyán"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Baréin"
    },
    "BD":{
      "name":"Bangladesh"
    },
    "BB":{
      "name":"Barbados"
    },
    "BY":{
      "name":"Bielorrusia"
    },
    "BE":{
      "name":"Bélgica"
    },
    "BZ":{
      "name":"Belice"
    },
    "BJ":{
      "name":"Benin"
    },
    "BM":{
      "name":"Bermudas"
    },
    "BT":{
      "name":"Bután"
    },
    "BO":{
      "name":"Bolivia"
    },
    "BA":{
      "name":"Bosnia-Herzegovina"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Isla Bouvet"
    },
    "BR":{
      "name":"Brasil"
    },
    "IO":{
      "name":"Territorio Británico del Océano Índico"
    },
    "BN":{
      "name":"Brunéi Darussalam"
    },
    "BG":{
      "name":"Bulgaria"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Camboya"
    },
    "CM":{
      "name":"Camerún"
    },
    "CA":{
      "name":"Canadá"
    },
    "CV":{
      "name":"Cabo Verde"
    },
    "KY":{
      "name":"Islas Caimán"
    },
    "CF":{
      "name":"República Centroafricana"
    },
    "TD":{
      "name":"Chad"
    },
    "CL":{
      "name":"Chile"
    },
    "CN":{
      "name":"China"
    },
    "CX":{
      "name":"Isla de Navidad"
    },
    "CC":{
      "name":"Islas Cocos (Keeling)"
    },
    "CO":{
      "name":"Colombia"
    },
    "KM":{
      "name":"Comoras"
    },
    "CG":{
      "name":"Congo"
    },
    "CD":{
      "name":"Congo, República Democrática del"
    },
    "CK":{
      "name":"Islas Cook"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Costa de Marfil"
    },
    "HR":{
      "name":"Croacia"
    },
    "CU":{
      "name":"Cuba"
    },
    "CY":{
      "name":"Chipre"
    },
    "CZ":{
      "name":"República Checa"
    },
    "DK":{
      "name":"Dinamarca"
    },
    "DJ":{
      "name":"Yibuti"
    },
    "DM":{
      "name":"Dominica"
    },
    "DO":{
      "name":"República Dominicana"
    },
    "EC":{
      "name":"Ecuador"
    },
    "EG":{
      "name":"Egipto"
    },
    "SV":{
      "name":"El Salvador"
    },
    "GQ":{
      "name":"Guinea Ecuatorial"
    },
    "ER":{
      "name":"Eritrea"
    },
    "EE":{
      "name":"Estonia"
    },
    "ET":{
      "name":"Etiopía"
    },
    "FK":{
      "name":"Islas Malvinas"
    },
    "FO":{
      "name":"Islas Faroe"
    },
    "FJ":{
      "name":"Fiyi"
    },
    "FI":{
      "name":"Finlandia"
    },
    "FR":{
      "name":"Francia"
    },
    "GF":{
      "name":"Guayana Francesa"
    },
    "PF":{
      "name":"Polinesia Francesa"
    },
    "TF":{
      "name":"Tierras Australes y Antárticas Francesas"
    },
    "GA":{
      "name":"Gabón"
    },
    "GM":{
      "name":"Gambia"
    },
    "GE":{
      "name":"Georgia"
    },
    "DE":{
      "name":"Alemania"
    },
    "GH":{
      "name":"Ghana"
    },
    "GI":{
      "name":"Gibraltar"
    },
    "GR":{
      "name":"Grecia"
    },
    "GL":{
      "name":"Groenlandia"
    },
    "GD":{
      "name":"Granada"
    },
    "GP":{
      "name":"Guadalupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernsey"
    },
    "GN":{
      "name":"Guinea"
    },
    "GW":{
      "name":"Guinea-Bissau"
    },
    "GY":{
      "name":"Guyana"
    },
    "HT":{
      "name":"Haití"
    },
    "HM":{
      "name":"Islas Heard y McDonald"
    },
    "VA":{
      "name":"Ciudad Estado del Vaticano"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hong Kong"
    },
    "HU":{
      "name":"Hungría"
    },
    "IS":{
      "name":"Islandia"
    },
    "IN":{
      "name":"India"
    },
    "ID":{
      "name":"Indonesia"
    },
    "IR":{
      "name":"Irán, República Islámica de"
    },
    "IQ":{
      "name":"Irak"
    },
    "IE":{
      "name":"Irlanda"
    },
    "IM":{
      "name":"Isla de Man"
    },
    "IL":{
      "name":"Israel"
    },
    "IT":{
      "name":"Italia"
    },
    "JM":{
      "name":"Jamaica"
    },
    "JP":{
      "name":"Japón"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Jordania"
    },
    "KZ":{
      "name":"Kazajistán"
    },
    "KE":{
      "name":"Kenia"
    },
    "KI":{
      "name":"Kiribati"
    },
    "KP":{
      "name":"Corea, República Popular Democrática de"
    },
    "KR":{
      "name":"Corea, República de"
    },
    "KW":{
      "name":"Kuwait"
    },
    "KG":{
      "name":"Kirguistán"
    },
    "LA":{
      "name":"República Democrática Popular Lao"
    },
    "LV":{
      "name":"Letonia"
    },
    "LB":{
      "name":"Líbano"
    },
    "LS":{
      "name":"Lesotho"
    },
    "LR":{
      "name":"Liberia"
    },
    "LY":{
      "name":"Gran Yamahiriya Árabe Libia Popular Socialista"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Lituania"
    },
    "LU":{
      "name":"Luxemburgo"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Macedonia, Antigua República Yugoslava de"
    },
    "MG":{
      "name":"Madagascar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malasia"
    },
    "MV":{
      "name":"Maldivas"
    },
    "ML":{
      "name":"Malí"
    },
    "MT":{
      "name":"Malta"
    },
    "MH":{
      "name":"Islas Marshall"
    },
    "MQ":{
      "name":"Martinica"
    },
    "MR":{
      "name":"Mauritania"
    },
    "MU":{
      "name":"Mauricio"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"México"
    },
    "FM":{
      "name":"Micronesia, Estados Federados de"
    },
    "MD":{
      "name":"Moldavia, República de"
    },
    "MC":{
      "name":"Mónaco"
    },
    "MN":{
      "name":"Mongolia"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Marruecos"
    },
    "MZ":{
      "name":"Mozambique"
    },
    "MM":{
      "name":"Birmania"
    },
    "NA":{
      "name":"Namibia"
    },
    "NR":{
      "name":"Nauru"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Países Bajos"
    },
    "AN":{
      "name":"Antillas Neerlandesas"
    },
    "NC":{
      "name":"Nueva Caledonia"
    },
    "NZ":{
      "name":"Nueva Zelanda"
    },
    "NI":{
      "name":"Nicaragua"
    },
    "NE":{
      "name":"Níger"
    },
    "NG":{
      "name":"Nigeria"
    },
    "NU":{
      "name":"Niue"
    },
    "NF":{
      "name":"Isla Norfolk"
    },
    "MP":{
      "name":"Islas Marianas del Norte"
    },
    "NO":{
      "name":"Noruega"
    },
    "OM":{
      "name":"Omán"
    },
    "PK":{
      "name":"Pakistán"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Territorios Palestinos, Ocupados"
    },
    "PA":{
      "name":"Panamá"
    },
    "PG":{
      "name":"Papúa Nueva Guinea"
    },
    "PY":{
      "name":"Paraguay"
    },
    "PE":{
      "name":"Perú"
    },
    "PH":{
      "name":"Filipinas"
    },
    "PN":{
      "name":"Pitcairn"
    },
    "PL":{
      "name":"Polonia"
    },
    "PT":{
      "name":"Portugal"
    },
    "PR":{
      "name":"Puerto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"Reunión"
    },
    "RO":{
      "name":"Rumanía"
    },
    "RU":{
      "name":"Federación Rusa"
    },
    "RW":{
      "name":"Ruanda"
    },
    "SH":{
      "name":"Santa Elena"
    },
    "KN":{
      "name":"San Cristóbal y Nieves"
    },
    "LC":{
      "name":"Santa Lucía"
    },
    "PM":{
      "name":"San Pedro y Miguelón"
    },
    "VC":{
      "name":"San Vincente y las Granadinas"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"San Marino"
    },
    "ST":{
      "name":"Sao Tome y Príncipe"
    },
    "SA":{
      "name":"Arabia Saudí"
    },
    "SN":{
      "name":"Senegal"
    },
    "CS":{
      "name":"Serbia y Montenegro"
    },
    "SC":{
      "name":"Seychelles"
    },
    "SL":{
      "name":"Sierra Leona"
    },
    "SG":{
      "name":"Singapur"
    },
    "SK":{
      "name":"Eslovaquia"
    },
    "SI":{
      "name":"Eslovenia"
    },
    "SB":{
      "name":"Islas Salomón"
    },
    "SO":{
      "name":"Somalia"
    },
    "ZA":{
      "name":"Sudáfrica"
    },
    "GS":{
      "name":"Islas Georgias del Sur y Sandwich del Sur"
    },
    "ES":{
      "name":"España"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Sudán"
    },
    "SR":{
      "name":"Surinam"
    },
    "SJ":{
      "name":"Svalbard y Jan Mayen"
    },
    "SZ":{
      "name":"Suazilandia"
    },
    "SE":{
      "name":"Suecia"
    },
    "CH":{
      "name":"Suiza"
    },
    "SY":{
      "name":"República Árabe Siria"
    },
    "TW":{
      "name":"Taiwán, Provincia de China"
    },
    "TJ":{
      "name":"Tayikistán"
    },
    "TZ":{
      "name":"Tanzania, República Unida de"
    },
    "TH":{
      "name":"Tailandia"
    },
    "TL":{
      "name":"Timor Oriental"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad y Tobago"
    },
    "TN":{
      "name":"Túnez"
    },
    "TR":{
      "name":"Turquía"
    },
    "TM":{
      "name":"Turkmenistán"
    },
    "TC":{
      "name":"Islas Turcas y Caicos"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Uganda"
    },
    "UA":{
      "name":"Ucrania"
    },
    "AE":{
      "name":"Emiratos Árabes Unidos"
    },
    "GB":{
      "name":"Reino Unido"
    },
    "US":{
      "name":"Estados Unidos"
    },
    "UM":{
      "name":"Islas Ultramarinas Menores de Estados Unidos"
    },
    "UY":{
      "name":"Uruguay"
    },
    "UZ":{
      "name":"Uzbekistán"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Vietnam"
    },
    "VG":{
      "name":"Islas Vírgenes, Británicas"
    },
    "VI":{
      "name":"Islas Vírgenes, EE. UU."
    },
    "WF":{
      "name":"Wallis y Futuna"
    },
    "EH":{
      "name":"Sáhara Occidental"
    },
    "YE":{
      "name":"Yemen"
    },
    "ZM":{
      "name":"Zambia"
    },
    "ZW":{
      "name":"Zimbabue"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Comprar",
  "sell":"Vender",
  "weWillCallYou":"Si es la primera vez que usa esta tarjeta, deberá introducir dos códigos secretos: uno estará en su extracto bancario y el otro se le dará por teléfono.",
  "exchangerStatuses":{
        "TimeOut": "La solicitud ha caducado",
        "Error": "Se ha producido un error. Póngase en contacto con nuestro equipo de soporte en support@indacoin.com",
        "WaitingForAccountCreation": "Esperando la creación de la cuenta de Indacoin (le hemos enviado un correo de registro)",
        "CashinWaiting": "Su pago se está procesando",
        "BillWaiting": "Esperando a que el cliente pague la factura",
        "Processing": "Su solicitud se está procesando",
        "MoneySend": "Los fondos se han enviado",
        "Completed": "La solicitud se ha procesado",
        "Verifying": "Verificación",
        "Declined": "Rechazado",
        "cardDeclinedNoFull3ds": "Se ha rechazado su tarjeta porque 3D-Secure no está habilitado en su tarjeta. Llame al departamento de atención al cliente de su banco",
        "cardDeclined": "Su tarjeta se ha rechazado. Si tiene alguna pregunta, póngase en contacto con nuestro equipo de soporte en support@indacoin.com",
        "Non3DS": "Desafortunadamente su pago ha sido rechazado. Póngase en contacto con su banco para saber si su tarjeta tiene habilitado 3D-Secure (Verified by Visa o Mastercard SecureCode). Si es así, tendrá que introducir un código secreto al realizar pagos en línea. El código se suele enviar a su teléfono."
  },
  "exchanger_fields_ok":"Haga clic para comprar bitcoins",
  "exchanger_fields_error":"Se ha producido un error al rellenar los campos. Verifique todos los campos",
  "bankRejected":"Su banco ha rechazado la transacción. Póngase en contacto con su banco e intente realizar el pago con tarjeta de nuevo",
  "wrongAuthCode":"Ha introducido un código de autorización incorrecto. Póngase en contacto con nuestro equipo de soporte en support@indacoin.com",
  "wrongSMSAuthCode":"El código de teléfono que ha introducido no es válido",
  "getCouponInfo":{
    "indo":"Este cupón no es válido",
    "exetuted":"Este cupón ya se ha usado",
    "freeRate":"El cupón de descuento se ha activado"
  },
  "ip":"Su IP se ha bloqueado temporalmente",
  "priceChanged":"El precio ha cambiado. ¿ACEPTAR? Recibirá ",
  "exchange_go":"– Cambio",
  "exchange_buy_btc":"– Comprar bitcoins",
  "month":"Ene Feb Mar Abr May Jun Jul Ago Sep Oct Nov Dic",
  "discount":"Descuento",
  "cash":{
    "equivalent":" igual que ",
    "card3DS":"Solo aceptamos tarjetas con 3D-Secure habilitado (Verified by Visa o MasterCard SecureCode)",
    "cashIn":{
      "1": "Tarjetas bancarias (USD)",
      "2": "Pago electrónico",
      "3": "QIWI",
      "4": "Bashcomsnabbank",
      "5": "Terminales Novoplat",
      "6": "Terminales Elecsnet",
      "7": "Tarjetas bancarias (USD)",
      "8": "Alpha Click",
      "9": "Transferencia bancaria internacional",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Money",
      "21": "Elecsnet",
      "22": "Kassira.net",
      "23": "Mobile Element",
      "24": "Svyaznoy",
      "25": "Euroset",
      "26": "PerfectMoney",
      "27": "Indacoin, interno",
      "28":	"CouponBonus",
      "29": "Yandex.Money",
      "30": "OKPay",
      "31": "Programa de socios",
      "33": "Payza",
      "35": "Código BTC-E",
      "36": "Tarjetas bancarias (USD)",
      "37": "Tarjetas bancarias (USD)",
      "39": "Transferencia bancaria internacional",
      "40": "Yandex.Money",
      "42": "QIWI (Manual)",
      "43": "UnionPay",
      "44": "QIWI (Automático)",
      "45": "Pago por teléfono",
      "49": "LibrexCoin",
      "50": "Tarjetas bancarias (EUR)",
      "51": "QIWI (Automático)",
      "52": "Pago por teléfono",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Por favor ingrese la dirección criptográfica correcta",
      "country": "País no válido",
      "alfaclick": "Inicio de sesión no válido",
      "pan": "Número de tarjeta no válido",
      "cardholdername": "Datos no válidos",
      "fio": "Datos no válidos",
      "bday": "Fecha de nacimiento no válida",
      "city": "Datos no válidos",
      "address": "Dirección no válida",
      "ypurseid": "Número de monedero",
      "wpurseid": "Número de monedero no válido",
      "pmpurseid": "Número de cuenta no válido",
      "payeer": "Número de cuenta no válido",
      "okpayid": "Número de monedero no válido",
      "purse": "Número de monedero no válido",
      "phone": "Número de teléfono no válido",
      "btcaddress": "Dirección no válida",
      "ltcaddress": "Dirección no válida",
      "lxcaddress": "Dirección no válida",
      "ethaddress": "Dirección no válida",
      "properties": "Datos no válidos",
      "email": "Correo electrónico no válido",
      "card_number": "Esta tarjeta no es compatible. Compruebe el número",
      "inc_card_number": "Número de tarjeta no válido",
      "inn": "Número no válido",
      "poms": "Número no válido",
      "snils": "Número no válido",
      "cc_expr": "Fecha no válida",
      "cc_name": "Nombre no válido",
      "cc_cvv": "CVV no válido"
    }
  },
  "change": {
    "wallet": "Crear nueva billetera / ya tengo la cuenta de Indacoin",
    //"accept": "Acepto el Acuerdo del usuario y la Política AML",
    "accept": "Estoy de acuerdo con los Términos de uso y la Política de privacidad",
    "coupone": "Tengo un cupón de descuento",
    "watchVideo": "Ver el vídeo",
    "seeInstructions": "Ayuda",
    "step1": {
      "title": "Crear pedido",
      "header": "Introduzca sus datos de pago",
      "subheader": "Díganos lo que desea comprar"
    },
    "step2": {
      "title": "Datos del pago",
      "header": "Introduzca los datos de su tarjeta de crédito",
      "subheader": "Solo aceptamos Visa y MasterCard",
      "card": {
        "header": "¿Dónde está ese código?",
        "text": "El CVV se encuentra por detrás de su tarjeta. Es un código de 3 dígitos"
      }
    },
    "step3": {
      "title": "Datos de contacto",
      "header": "Introduzca sus datos de contacto",
      "subheader": "Queremos conocer mejor a nuestros clientes"
    },
    "step4": {
      "title": "Verificación",
      "header": "Introduzca su código en la página de pagos",
      "subheader": "Sus datos de pago"
    },
    "step5": {
      "title": "Información del pedido"
    },
    "step6": {
      "title": "Grabar vídeo",
      "header": "Muestre su pasaporte y su cara",
      "description": "Demuéstrenos que es de fiar",
      "startRecord": "Haga clic en el botón Inicio para empezar a grabar",
      "stopRecord": "Haga clic en el botón Stop para detener la grabación"
    },
    "nextButton": "Siguiente",
    "previousButton": "Anterior",
    "error": "Conexión de error del servidor Por favor, vuelva a enviar los datos",
    "passVideoVerification": "Pase la verificación por video",
    "passKYCVerification": "Verificación KYC",
    "soccer-legends": "Acepto que mis datos personales sean transferidos y procesados a la compañía Soccer Legends Limited",
    "withdrawalAmount": "Monto a acreditar a la cuenta",
    "inc": "In More Than 100 Countries including "
  },
  "form": {
    "tag": "etiqueta de {{{currency}}}",
    "loading": "Cargando por favor espere...",
    "youGive": "Das",
    "youTake": "Usted toma",
    "email": "Email",
    "password": "Contraseña",
    //"cryptoAdress": "Dirección de {{{name}}} Wallet",
    "cryptoAdress": "Dirección de la cartera criptográfica",
    "externalTransaction": "ID de transacción",
    "coupone": "Tengo un cupón de descuento",
    "couponeCode": "Código promocional",
    "cardNumber": "Número de tarjeta",
    "month": "MM",
    "year": "YY",
    "name": "Nombre",
    "fullName": "Nombre completo",
    "mobilePhone": "Teléfono móvil",
    "birth": "Su fecha de nacimiento en formato MM.DD.YYYY",
    "videoVerification": "Verificación de video",
    "verification": "Verificación",
    "status": "Comprobar estado",
    "login": { // TODO: translate
      "captcha": "Captcha",
      "reset": "Reiniciar",
      "forgot": "Se te olvidó tu contraseña",
      "signIn": "Registrarse"
    },
    "registration": { // TODO: translate
        "create": "Crear"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Verificación simple",
      "title-2": "Más de 100 altcoins disponibles",
      "title-3": "Sin cargos ocultos",
      "title-4": "No hay registro"
    },
    "title": "Estamos trabajando en más de 100 países",
    "subtitle": "incluido {{{country}}}",
    "shortTitle": "Estamos trabajando en más de 100 países",
    "text1": "Verificación simple",
    "popupText1": "Los compradores por primera vez solo tienen que verificar el número de teléfono",
    "text2": "100 altcoins son compatibles",
    "popupText2": "Puede comprar más de 100 criptomonedas más prometedoras con una tarjeta de crédito / débito",
    "text3": "Sin cargos ocultos",
    "popupText3": "Obtendrá exactamente la misma cantidad de altcoins, que se mostró antes del acuerdo",
    "text4": "No hay registro",
    "popupText4": "No es necesario que se registre, la criptomoneda se enviará a la dirección que indicó"
  },
  "buying-segment": {
    "title": "Más de 500 000 clientes",
    "sub-title": "han estado usando Indacoin desde 2015"
  },
  "phone-segment": {
      "title": "Descargue nuestra aplicación",
      "text1": "Compra y almacena más de 100 criptomonedas en una sola billetera",
      "text2": "Haga un seguimiento de los cambios de su cartera de inversiones",
      "text3": "Enviar criptomonedas a tu amigo se volvió tan fácil como enviar un mensaje"
  },
  "social-segment": {
    "buttonText": "Leer en Facebook",
    "on": "en Facebook",
    "facebook-review-1": "buena pagina de bitcoins, proceso rapido y sencillo, se los recomiendo",
    "facebook-review-2": "Brillante servicio y cómodo de usar, la gente es amable y muy servicial. Aunque es una transacción digital, hay una persona al otro lado (que puede ser una ayuda MÁSIVA) y tranquilizadora, servicio de 5 estrellas que volveré a usar",
    "facebook-review-3": "Como solo estoy comenzando a probar todo lo que rodea al mundo de las criptomonedas, hacer un intercambio en su sitio web es más que simple. y su apoyo en caso de que se atasque o simplemente busque ayuda es mucho más que sorprendente.",
    "facebook-review-4": "Pago muy rápido, excelente soporte y excelentes tasas de cambio.",
    "facebook-review-5": "Decidí comprar 100 bit de bitcoins usando este servicio, y fue fácil proceder con el pago. Aunque tuve algunos problemas y preguntas, casi instantáneamente me ayudó su equipo de soporte. Me hizo sentir mucho más cómodo comprando Bitcoin con Indacoin. Y planeo comprar más en el futuro.",
    "facebook-review-6": "Acabo de utilizar Indacoin para hacer una transacción para la compra de Bitcoin. La transacción fue fluida, seguida de una llamada de verificación. Es muy fácil de usar y un portal fácil de usar para comprar monedas de poco. ¡Recomendaría esta plataforma para comprar bitcoins!",
    "facebook-review-7": "Es difícil encontrar una facilidad de transacción fácil con tarjeta de crédito para comprar moneda criptográfica e Indcoin me trató muy bien, paciencia con usuarios de otros países que no entienden su idioma, chat en línea de preparación. Indcoin Gracias !!!",
    "facebook-review-8": "Rápido y fácil. El soporte en línea fue muy rápido y útil. ProTip: el uso de la aplicación reduce los cargos en un 9%.",
    "facebook-review-9": "Esta es la segunda vez que uso Indacoin: excelente servicio, excelente atención al cliente y rápida transacción. Definitivamente voy a usar de nuevo.",
    "facebook-review-10": "Excelente servicio y comunicación, ayuda paso a paso con el chat en vivo y totalmente confiable, incluso me llamaron sin preguntarme y me explicaron cómo hacer la verificación de la transacción, la manera más fácil de comprar Bitcoin sin ningún problema, gracias Indacoin hará muchas más transacciones con usted.",
    "facebook-review-11": "Gran servicio, proceso rápido. Servicio al cliente útil. Rápido responde. Manera simple de comprar moneda crypto. Entre otros servicios, sinceramente este es el único servicio que uso para comprar moneda criptográfica. Confío en que procesen mi compra. Recomendado para todos los que se ocupan de comprar moneda criptográfica. Por último, pero no menos importante, solo una palabra para Indacoin: ¡¡¡Excelente !!!",
    //"facebook-review-12": "IndaCoin tiene un servicio súper fácil, confiable y rápido. También fueron extremadamente rápidos al responder todas mis preguntas y ayudarme con mi transferencia. ¡Recomiendo encarecidamente sus servicios a todos! 10/10!",
    "facebook-review-12": "Compré bitcoins por 200 euros y el sitio web es legítimo. A pesar de que tardó un tiempo en realizarse la transacción, el servicio al cliente es excelente. Me comuniqué con su servicio de atención al cliente e inmediatamente resolvieron mi problema. Definitivamente recomiendo este sitio a todos. a veces debido al alto tráfico, el tiempo de transacción oscila entre 8-10 horas. No entre en pánico si no recibe sus monedas al instante, a veces llevará tiempo, pero seguramente obtendrá sus monedas.",
    "facebook-review-13": "Gran trabajo Indacoin. Tuve la mejor experiencia hasta ahora en la compra de monedas con mi tarjeta de crédito con Indacoin. El consultor Andrew hizo un gran servicio al cliente, y me impresionó cómo la transferencia también en menos de 3 minutos ... Continuaré usándolos :)",
    "facebook-review-14": "Indacoin es una interfaz fácil de usar y fácil de usar para que los compradores puedan comprar bitcoins más fácilmente en su plataforma. Es fácil con ayuda paso a paso de los Agentes Consultores y están realmente dispuestos a ayudarlo a resolver su problema de forma inmediata. Definitivamente voy a referir a las personas a Indacoin y constantemente me actualizan sobre mi situación y están dispuestos a hacer un esfuerzo adicional para resolver sus indagaciones. Gracias Indacoin!",
    "facebook-review-15": "La forma más fácil y rápida de comprar moneda cifrada que he encontrado en muchas búsquedas. Buen trabajo chicos, gracias!",
    "facebook-review-16": "Una forma realmente fácil de comprar monedas en línea. Excelente servicio al cliente también. Es increíble que tengan monedas alt para comprar directamente también. Toma las complicaciones de los intercambios. Realmente recomiendo esto a todos después de mi primera experiencia positiva.",
    "facebook-review-17": "Gran servicio. Fácil de usar y fácil de comprar varias monedas / fichas.",
    "facebook-review-18": "Esta es una de las mejores plataformas para intercambiar BTC. Hice varios cambios y fue rápido.",
    "facebook-review-19": "Gran servicio, fácil de usar, fácil de comprar varias criptomonedas y tokens ERC20 con su tarjeta de crédito o débito. ¡Muy rápido y también seguro! ¡El servicio y la plataforma que he estado buscando! ¡Recomiendo totalmente usar Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "Más de {{{count}}} diferentes criptomonedas están disponibles para su compra en {{{name}}}"
  },

  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin en su teléfono",
    "text": "Smart-veskið Indacoin okkar er fáanlegt fyrir notendur Android, IOS og í öllum netvafrum. Sending gjaldeyris til vinar þinnar varð svo auðvelt að senda skilaboð"
  },

  "news-segment": {
    "no-news": "No hay noticias para tu idioma",
    "news-error": "Nos disculpamos, la noticia no está disponible en este momento",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },

  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "Esta criptomoneda no está disponible pero volverá pronto",
    "unknown": "Error de servidor desconocido",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "El límite ha excedido",
    "phone": "Phone is not supported"
  },
  "tags": {
    "title-btc": "Cambie Bitcoin BTC a {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Compre [cryptocurrency] con una tarjeta de crédito o débito en [country] insidiosamente - Indacoin",
    "title2": "Compre Bitcoin con una tarjeta de crédito o débito en todo el mundo - Indacoin",
    "title3": "Compre {{{cryptocurrency}}} con una tarjeta de crédito o débito en {{{country}}} insidiosamente - Indacoin",
    "title4": "Compre criptomonedas con tarjeta de crédito o débito en {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Programa de afiliados de Indacoin",
      "news": "Las últimas noticias de Indacoin",
      "help": "Intercambio de criptomonedas F.A.Q. - Indacoin",
      "api": "API de intercambio de criptomonedas - Indacoin",
      "terms-of-use": "Términos de uso y política ALD - Indacoin",
      "login": "Iniciar sesión - Indacoin",
      "register": "Registrarse - Indacoin",
      "locale": "Cambiar región - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) gráficos de precios y volumen comercial - Indacoin"
    },
    "descriptions": {
      "affiliate": "El programa de afiliados de Indacoin le permite ganar hasta el 3% de cada compra con una tarjeta de crédito / débito. La remuneración será enviada a su billetera Bitcoin Indacoin",
      "news": "¡Visite nuestro blog de noticias oficial para estar al tanto de las últimas noticias sobre la industria de criptomonedas! ¡Una gran cantidad de información útil y comentarios exhaustivos de expertos de Indacoin en un solo lugar!",
      "help": "Encuentre las respuestas a todas sus preguntas sobre cómo comprar, vender e intercambiar criptomonedas en Indacoin.",
      "api": "Nuestra API proporciona la compra e intercambio de criptomonedas instantáneamente con Visa y Mastercard. Verifique la integración y los ejemplos.",
      "terms-of-use": "Información general. Términos de Uso. Servicios prestados. Transferencia de fondos. Transferencia de derechos. Comisiones. Política AML.",
      "login": "Inicia sesión o crea una nueva cuenta",
      "register": "Crear una nueva cuenta",
      "locale": "Elija su región, y seleccione esto y recordaremos su región la próxima vez que visite Indacoin",
      "currency": "Precios actuales para {{{fullCurrency}}} en USD con información sobre el volumen de transacciones y la criptomoneda histórica"
    }
  },
  "usAlert": "Lo sentimos, pero Indacoin no opera en los Estados Unidos de América",
  "wrongPhoneCode": "Has ingresado un código incorrecto desde tu teléfono",
  "skip": "Omitir",
  "minSum": "Monto mínimo para comprar",
  "currency": {
    "no-currency": "Para una criptomoneda dada, el gráfico no está disponible",
    "presented": {
      "header": "Bitcoin fue presentado al mundo en 2009 y en 2017 sigue siendo la criptomoneda primaria del mundo"
    },
    "difference": {
      "title": "¿En qué se diferencia Bitcoin del dinero corriente?",
      "header1": "Emisión limitada",
      "text1": "El número total de Bitcoins que se pueden extraer es 21M. Más de 16 millones de Bitcoin están en circulación en este momento. Cuanto más popular se vuelve, más caro.",
      "header2": "Moneda mundial",
      "text2": "Ningún gobierno tiene control sobre Bitcoin. Se basa únicamente en las matemáticas",
      "header3": "Anonimato",
      "text3": "Puedes usar tus Bitcoins en cualquier momento y en cualquier cantidad. Este es un verdadero hallazgo en lugar de transferir dinero desde su cuenta bancaria, cuando necesita explicar el destino del pago, así como el origen de los fondos.",
      "header4": "Velocidad",
      "text4": "Su banco tomará tiempo para transferir dinero, especialmente cuando se trata de grandes valores. Con Bitcoin, la rutina de enviar y recibir tomará segundos.",
      "header5": "Transparencia",
      "text5": "Todas las transacciones de Bitcoin se pueden ver en BlockChain."
    },
    "motivation": {
      "title": "Al ser la moneda primogénita de su tipo, logró cambiar la visión del dinero tal como lo conocíamos. En este momento, el límite del mercado de Bitcoin alcanza más de $ 70 mil millones, lo que supera a gigantes como Tesla, General Motors y FedEx."
    },
    "questions": {
      "header1": "¿Qué es una moneda digital y una criptomoneda?",
      "text1": "La criptomoneda es un activo digital que funciona como un medio de intercambio mediante criptografía para asegurar las transacciones y controlar la creación de unidades adicionales de la moneda. No tiene forma física y está descentralizado en comparación con los sistemas bancarios habituales",
      "header2": "¿Cómo ocurre la transacción de Bitcoin?",
      "text2": "Cada transacción tiene 3 dimensiones: Entrada: el origen de Bitcoin utilizado en la transacción. Nos dice dónde lo consiguió el propietario actual. Cantidad: la cantidad de \"monedas\" utilizadas. Salida: la dirección del destinatario del dinero.",
      "header3": "¿De dónde vino Bitcoin?",
      "text3": "Aunque el mundo está familiarizado con el nombre del creador de Bitcoin, \"Satoshi Nakamoto\" es probablemente un alias para un programador o incluso para un grupo de este tipo. Básicamente, Bitcoin proviene de 31,000 líneas de código creadas por \"Satoshi\" y está controlado solo por software.",
      "header4": "¿Cómo funciona Bitcoin?",
      "text4": "Las transacciones se producen entre billeteras de Bitcoin y son transparentes. En el blockchain, cualquiera puede verificar el origen y el ciclo de vida de cada \"moneda\". Sin embargo, no hay una \"moneda\". Bitcoin no tiene forma física y no tiene forma virtual, solo el historial de transacciones.",
      "header5": "¿Qué es una billetera de Bitcoin?",
      "text5": "Una billetera de Bitcoin es un programa que le permite administrar Bitcoin: recibir y enviar, verificar el saldo y la tasa de Bitcoin. Aunque no existe una \"moneda\" en dichas carteras, contiene claves digitales, que se utilizan para acceder a las direcciones públicas de Bitcoin y las transacciones de \"firma\". Carteras populares:",
    },
    "history": {
      "title": "La breve historia de Bitcoin",
      "text1": "Según la leyenda, aquí es cuando Satoshi Nakamoto comienza su trabajo en Bitcoin",
      "text2": "Primera transacción realizada en Bitcoin. El valor de Bitcoin, basado en el costo de la electricidad utilizada para generar Bitcoin, se establece en 1 USD por 1.306.03 BTC",
      "text3": "25% de todos los Bitcoins ya están generados",
      "text4": "Dell y Windows comienzan a aceptar Bitcoin",
      "text5": {
        "part1": "El precio de BTC aumenta de 1,402 USD a 19,209 USD en unos pocos meses.",
        "part2": "La tasa de Bitcoin alcanza los 20,000 USD por moneda el 18 de diciembre de 2017"
      },
      "text6": "El registro de bitcoin.org",
      "text7": {
        "part1": "Un momento verdaderamente histórico, cuando se usan 10,000 BTC para comprar indirectamente 2 pizzas. Hoy puedes comprar 2,500 pizzas de pepperoni por la misma cantidad.",
        "part2": "El precio de Bitcoin aumenta de 0.008 USD a 0.08 USD por 1 BTC en solo 5 días.",
        "part3": "Se produce la primera transacción móvil"
      },
      "text8": {
        "part1": "El Tesoro de EE. UU. Clasifica a BTC como una moneda virtual descentralizada convertible.",
        "part2": "El primer cajero automático de Bitcoin encontrado en San Diego, California.",
        "part3": "El precio de Bitcoin se duplica y alcanza los 503.10 USD. Crece a 1,000 USD el mismo mes"
      },
      "text9": "El juez federal de EE. UU. Descarta que BTC sean \"fondos dentro del significado claro de ese término\""
    },
    "graph": {
      "buy-button": "Comprar"
    },
    "ethereum": {
      "presented": {
        "header": "Ya tenemos Bitcoin, entonces, ¿por qué necesitamos Ethereum? ¡Examinémoslo! Bitcoin asegura transferencias de dinero justas y transparentes. Ether, la moneda digital de Ethereum, puede jugar el mismo papel. Sin embargo, hay mucho más para la funcionalidad, ofrecida por Ethereum."
      },
      "questions": {
        "header1": "What is Ethereum?",
        "text1": "Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
        "header2": "Where did it come from?",
        "text2": "Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
        "header3": "What is ICO?",
        "text3": "Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
        "header4": "What is Ether?",
        "text4": "Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
        "header5": "What’s so special about “smart contracts” and what’s its difference from usual contracts?",
        "text5": "Each transaction has 3 dimensions: Input – the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount – the number of “coins” used. The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
        "header6": "How is Ethereum different from Bitcoin?",
        "text6": "Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens – Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
        "header7": "What are the most popular Ethereum wallets?",
        "text7": "An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets: "
      },
      "history": {
        "title": "The brief history of Ethereum",
        "text1": "Crowdsale is initiated to gather funds for creating Ethereum",
        "text2": "The Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history",
        "text3": "Ethereum gains 50% market share in initial coin offering",
        "text4": "Ethereum exchange rate reaches 400 USD (5,000% rise since January)",
        "text5": "The idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine",
        "text6": "30th of July platform goes online",
        "text7": "The DAO is hacked, Ethereum value drops from 21.5 USD to 8 USD. This leads to Ethereum being forked in two blockchains in 2017",
        "text8": "Enterprise Ethereum Alliance is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc",
        "text9": {
          "part1": "All cryptocurrencies drop after China bans ICO, but quickly recover afterward.",
          "part2": "Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "¿Estás listo para dejar que tus clientes compren crypto con una tarjeta de crédito / débito? Por favor, déjenos un correo electrónico y nos pondremos en contacto lo antes posible",
        "input": "Dirección de correo electrónico",
        "button": "Empezar",
        "mail-success": "Gracias. Le responderemos"
      },
      "api": {
        "title": "Integración API",
        "text": "Con nuestra API, puede dejar que sus clientes compren criptomonedas de manera instantánea desde su sitio. Nuestra plataforma antifraude integrada en su página web o aplicación móvil también se puede utilizar como una herramienta que le permitirá monitorear y filtrar los pagos realizados en su plataforma."
      },
      "affiliate": {
        "title": "Programa de Afiliados",
        "text": "Obtenga recompensas por asesorar a Indacoin a otros clientes. Ganará hasta un 3% de las compras de las referencias.",
        "block-1": {
          "title": "¿Cómo puedo unirme al Programa de Afiliados de Indacoin?",
          "text": "No dude en registrarse en nuestro sitio."
        },
        "block-2": {
          "title": "¿Cómo podré retirar los fondos recaudados en mi cuenta?",
          "text": "Puedes comprar BTC / ETH / otras altcoins en Indacoin sin ningún costo o recibirlas en tu tarjeta bancaria."
        },
        "block-3": {
          "title": "¿Cuál es la ventaja de su programa de afiliados?",
          "text": "En nuestra plataforma, los clientes tienen la oportunidad de comprar más de 100 monedas diferentes utilizando tarjetas de crédito / débito sin necesidad de registrarse. Teniendo en cuenta el hecho de que las compras se realizan casi de inmediato, los socios pueden obtener sus ganancias al instante."
        },
        "block-4": {
          "title": "¿Puedo ver el ejemplo del enlace de referencia?",
          "text": "Por supuesto, así es como se ve: ({{{link}}})"
        }
      },
      "more": "Saber más",
      "close": "Cerca"
    },
    "achievements": {
      "partnership-title": "Asociación",
      "partnership-text": "Hay muchas maneras de colaborar con Indacoin. El Programa de asociación de Indacoin es para empresas relacionadas con la criptografía que están dispuestas a conectar el mundo de las criptomonedas con el sistema tradicional de dinero fiduciario para aumentar la participación en la industria de cifrado. Con Indacoin, podrá brindarles a sus clientes la experiencia personalizada del cripto de compras con las tarjetas de crédito / débito.",
      "block-1": {
        "header": "De confianza",
        "text": "Hemos estado trabajando en el mercado de cifrado desde 2013 y nos hemos asociado con muchas empresas respetadas que lideran la industria. Más de 500 000 clientes de más de 190 países han utilizado nuestro servicio para comprar criptomonedas."
      },
      "block-2": {
        "header": "Seguro",
        "text": "Creamos una plataforma innovadora y altamente eficiente contra el fraude con una tecnología central desarrollada especialmente para la detección y prevención de actividades fraudulentas para proyectos criptográficos."
      },
      "block-3": {
        "header": "Solución lista para usar",
        "text": "Nuestro producto ya está siendo utilizado por ICOs, Cryptocurrency Exchanges, Blockchain Platforms y Crypto Funds."
      },

      "header": "Gana dinero con nosotros en el avance",
      "subheader": "Tecnologías Blockchain",
      "text1": "{{{company}}} es una plataforma global que permite a las personas comprar instantáneamente Bitcoin, Ethereum, Ripple, Waves y 200 otras criptomonedas con una tarjeta de crédito o débito.",
      "text2": "El servicio ha estado funcionando desde 2013 casi en todo el mundo, incluyendo pero no limitado a:",
      "text3": "Reino Unido, Canadá, Alemania, Francia, Rusia, Ucrania, España, Italia, Polonia, Turquía, Australia, Filipinas, Indonesia, India, Bielorrusia, Brasil, Egipto, Arabia Saudita, Emiratos Árabes Unidos, Nigeria y otros"
    },
    "clients": {
      "header": "Más de 500 000 clientes",
      "subheader": "Desde el 2013"
    },
    "benefits": {
      "header": "Nuestros beneficios clave para las empresas:",
      "text1": "Servicio único que permite a los clientes comprar y enviar bitcoins sin registrarse. Por lo tanto, los clientes que acudan a Indacoin siguiendo el enlace de referencia deberán ingresar los detalles de la tarjeta y la dirección de la billetera, donde se debe enviar la criptografía. Por lo tanto, los clientes potenciales realizarán las compras de inmediato y nuestros socios obtendrán ganancias.",
      "text2": "Hasta el 3% de cada transacción es obtenida por nuestros socios. Además, los webmasters obtendrán el 3% de todas las compras futuras de la referencia.",
      "text3": "Bitcoin y tarjetas bancarias podrían usarse para retirar los ingresos."
    },
    "accounts": {
      "header": "Únete a nosotros ahora",
      "emailPlaceholder": "Ingrese su dirección de correo electrónico",
      "captchaPlaceholder": "Captcha",
      "button": "Empezar"
    }
  },
  "faq": {
    "title": "Preguntas y respuestas",
    "header1": "Preguntas generales",
    "header2": "Criptomoneda",
    "header3": "Verificación",
    "header4": "Comisiones",
    "header5": "Compra",
    "question1": {
      "title": "¿Qué es Indacoin?",
      "text": "Somos una empresa que trabaja en criptomonedas desde 2013, con sede en Londres, Reino Unido. Puede usar nuestro servicio para comprar más de 100 diferentes criptomonedas a través del pago con tarjeta de crédito / débito sin necesidad de registrarse."
    },
    "question2": {
      "title": "¿Cómo puedo contactarte?",
      "text": "Puede ponerse en contacto con nuestro equipo de soporte por correo electrónico (support@indacoin.com), por teléfono (+ 44-207-048-2582) o mediante el uso de una opción de chat en vivo en nuestra página web."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "¿Qué criptomonedas puedo comprar aquí?",
      "text": "Puede comprar más de 100 diferentes criptomonedas que se enumeran en nuestro sitio web."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "¿Qué países pueden usar este servicio?",
      "text": "Trabajamos con todos los países, con la única excepción de EE. UU. (Por el momento)"
    },
    "question7": {
      "title": "¿Puedo vender bitcoin aquí?",
      "text": "No, solo puedes comprar bitcoins aquí."
    },
    "question8": {
      "title": "¿Qué es criptomoneda?",
      "text": "Сryptocurrency es una moneda digital que usa la criptografía para la seguridad, función que dificulta la falsificación. No es emitido por ninguna autoridad central, por lo que es teóricamente inmune a la interferencia o manipulación del gobierno."
    },
    "question9": {
      "title": "¿Dónde puedo almacenar criptomonedas?",
      "text": "Puede configurar una billetera separada para cada criptomoneda en su PC o puede usar nuestro servicio de monedero gratuito y almacenar más de 100 criptomonedas, todo en un solo lugar."
    },
    "question10": {
      "title": "¿Qué quiere decir con \"dirección bitcoin\"?",
      "text": "Es la dirección de tu billetera bitcoin. También puede ingresar la dirección de la persona a la que desea enviar bitcoins o crear una billetera en Indacoin."
    },
    "question11": {
      "title": "¿Qué tan rápido es la transacción de criptomonedas?",
      "text": "Los tiempos de transacción de las criptomonedas dependen de la criptomoneda que esté enviando, las transacciones de Bitcoin tardan entre 15 y 20 minutos en completarse y, en promedio, permiten hasta 1 hora para el resto de las monedas."
    },
    "question12": {
      "title": "¿Qué debo hacer para verificar mi tarjeta?",
      //"text": "Para completar la verificación, deberá ingresar el código de 4 dígitos que recibe a través de una llamada telefónica. Para su segundo paso, es posible que también se le pida que se someta a la verificación de video, donde graba un video mostrando su cara y cargando un escaneo o foto de su ID / pasaporte / licencia de conducir. Dependiendo de su verificación, este paso puede ser obligatorio ya que debemos estar seguros de que usted es el titular de la tarjeta."
      "text": "Para completar la verificación, deberá ingresar el código de 4 dígitos que recibe a través de una llamada telefónica. Para su segundo paso, también se le puede pedir que se someta a una verificación con foto, lo que requiere que cargue una foto de su pasaporte / identificación y una autofoto con identificación y una hoja de papel con el texto requerido, confirmando la compra. Como alternativa, podemos solicitar una verificación de video, en la que tendrá que grabar su rostro, por ejemplo: \"Verificación de Indacoin para criptografía\" muestre su identificación y la tarjeta con la que realizó el pago. Cuando muestre la tarjeta, muestre solo los últimos 4 dígitos y el nombre."
    },
    "question13": {
      "title": "¿Qué pasa si no puedo grabar una verificación de video?",
      "text": "Puede abrir el mismo enlace de la página de pedido desde su dispositivo móvil y registrarlo desde allí. Si tiene problemas, también puede enviar el video requerido como archivo adjunto por correo electrónico a support@indacoin.com. Asegúrese de indicar su número de orden de intercambio."
    },
    "question14": {
      "title": "Hice un pedido e ingresé todos los detalles, ¿por qué todavía está procesando?",
      "text": "Si ha esperado más de 2 minutos, la orden no llegó a la transacción. Es posible que su tarjeta no sea segura en 3D y es posible que deba volver a intentarlo. Asegúrese de confirmar su compra a través de un código PIN con su banco o puede intentar comprar con una moneda diferente (EUR / USD). De lo contrario, si está seguro de que su tarjeta es 3D y cambiar la moneda no ayuda, entonces necesita contactar a su banco, ya que pueden estar bloqueando sus intentos de compra."
    },
    "question15": {
      "title": "¿Tengo que pasar por la verificación cada vez que realizo una compra?",
      "text": "Solo necesita verificar su tarjeta una vez y todas las siguientes transacciones serán automáticas. Sin embargo, necesitaremos una verificación si decide usar otra tarjeta bancaria."
    },
    "question16": {
      "title": "Mi pedido fue rechazado por el banco, ¿qué debo hacer?",
      "text": "Debe ponerse en contacto con su banco y preguntar el motivo de la disminución, quizás puedan levantar la restricción para que se pueda realizar una compra."
    },
    "question17": {
      "title": "¿Por qué debo verificar mi tarjeta bancaria o enviar un video?",
      "text": "El proceso de verificación lo protege en caso de robo de tarjeta, piratería informática o fraude; esto funciona de la manera opuesta en caso de que alguien intente realizar una compra utilizando la tarjeta bancaria de otra persona."
    },
    "question18": {
      "title": "¿Cómo puedo confiar en que mis datos y la información de mi tarjeta son seguros?",
      "text": "Nunca compartimos sus datos con terceros sin su consentimiento. Además, solo aceptamos tarjetas 3D-Secure, y nunca solicitamos ningún dato confidencial como el código CCV o el número completo de la tarjeta. Solo proporciona dichos detalles de pago a través de los terminales de puerta de enlace Visa o Mastercard. Solo recopilamos la información necesaria para asegurarnos de que eres el propietario de la tarjeta."
    },
    "question19": {
      "title": "¿Cuál es su tarifa?",
      "text": "Nuestra tarifa varía de una compra a otra ya que se basa en numerosos factores, por eso hemos creado una calculadora que le indicará la cantidad exacta de criptomonedas que recibirá, todas las tarifas incluidas."
    },
    "question20": {
      "title": "¿Cuánto me cobrarán por la transacción?",
      "text": "Solo se le cobrará el importe que especifique durante su pedido en nuestra calculadora. Todas las tarifas están incluidas en esa cantidad."
    },
    "question21": {
      "title": "¿Tienes algún descuento?",
      "text": "Ocasionalmente tenemos descuentos, puede obtener más información de nuestro personal de soporte."
    },
    "question22": { // TODO: link
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "¿Cuán rápido se enviará mi criptomoneda?",
      "text": "Por lo general, demora unos 30 minutos después de realizar un pedido o completar la verificación (si es la primera vez que usa la tarjeta en Indacoin)."
    },
    "question24": {
      "title": "¿Cuáles son las compras mínimas y máximas?",
      "text": "Tendrá un límite máximo de $ 200 para la primera transacción, $ 200 adicionales para la segunda transacción disponible después de 4 días de la compra inicial, $ 500 después de 7 días y $ 2000 en 14 días de la primera compra. En un mes desde su primera compra, no hay límites al pagar con su tarjeta. Tenga en cuenta que el límite mínimo es siempre de $ 50."
    },
    "question25": {
      "title": "¿Qué tipo de tarjetas son aceptadas?",
      "text": "Solo aceptamos Visa y Mastercard con seguridad 3D."
    },
    "question26": {
      "title": "¿Qué es 3D seguro?",
      "text": "La tecnología 3D Secure consiste en los programas Verified by Visa y MasterCard SecureCode. Después de ingresar los datos de su tarjeta de crédito en nuestra tienda en línea, aparecerá una nueva ventana solicitando su código de seguridad personal. Su institución financiera autenticará la transacción en cuestión de segundos, y confirmará que usted es la persona que realiza la compra."
    },
    "question27": {
      "title": "¿Qué otros sistemas de pago puedo usar para comprar bitcoins?",
      "text": "Actualmente solo aceptamos pagos con tarjeta Visa / Master en nuestra plataforma."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": { // TODO: translate
    "message": "La cantidad debe estar entre {{{limitsMin}}} {{{limitsCur}}} y {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "La cantidad debe ser de {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">Este sitio web utiliza cookies para garantizar que obtenga la mejor experiencia en nuestro sitio web.</span> {{{link}}}",
      "accept-button": "Lo tengo",
      "link": "Aprende más"
    },
    "success-video": {
      "text": "¡Gracias! Ahora comenzamos el proceso de verificación, que puede llevar un tiempo. Cuando se completa la verificación, las monedas de cifrado se entregarán a usted"
    },
    "accept-agreement": {
      //"text": "Indique que ha leído y acepta el Acuerdo del usuario y la Política ALD"
      "text": "Indique que ha leído y acepta los Términos de uso y la Política de privacidad"
    },
    "price-changed": {
      "header": "Precio cambiado",
      "agree": "De acuerdo",
      "cancel": "Cancelar"
    },
    "neoAttention": "Debido a las características de la moneda criptográfica NEO, solo los enteros están disponibles para la compra",
    "login": {
      "floodDetect": "Inundación",
      "resetSuccess": "Restablecer contraseña exitosa. Por favor revise su correo electrónico",
      "wrongEmail": "Este correo electrónico no está registrado todavía",
      "unknownError": "Error del Servidor. Por favor, vuelva a intentar la solicitud",
      "alreadyLoggedIn": "Ya se ha autentificado",
      "badLogin": "Usuario o contraseña incorrectos",
      "badCaptcha": "Captcha incorrecto",
      "googleAuthenticator": "Por favor ingrese el código de autentificación de google"
    },
    "registration": {
      "unknownError": "Error desconocido",
      "floodDetect": "Inundación",
      "success": "Registro exitoso. Por favor revise su correo electrónico",
      "badLogin": "Usuario o contraseña incorrectos",
      "badCaptcha": "Captcha incorrecto",

      "empty": "No todos los campos están completos. Por favor llénalos a todos",
      "repeatedRequest": "La confirmación de registro se envió a su dirección de correo electrónico. Por favor, asegúrese de haberlo recibido. Por si acaso, revisa también una carpeta de spam",
      "badEmail": "Usaste la dirección de correo electrónico incorrecta o temporal",
      "blockedDomain": "Intente con una dirección de correo electrónico diferente para registrarse"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "No se han encontrado resultados",
  "locale": {
    "title": "Por favor elija su país"
  }
}