export default {
  "lang": "ru",
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "recordVideo": {
    "send": "Отправить",
    "stop": "Остановить",
    "record": "Запись",
    "button": "Записать видео"
  },
  "links": {
    "terms-of-use": {
      "text": "Я согласен с {{{terms}}} и {{{policy}}}",
      "link-1": "Условиями Использования",
      "link-2": "Политикой Конфиденциальности"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "Ожидание платежа",
      "status-2": "Обмен",
      "status-2-sub": "Мы подбираем лучший курс для вас",
      "status-3": "Отправка на Ваш кошелек",
      "transaction-completed": "Поздравляем, Ваша транзакция завершена!",
      "rate": "Курс обмена 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Вы отправили: ",
      "toAddress": "на {{{cryptoAdress}}}",
      "receiveText": "Вы получите: "
    },
    "changeCrypto": {
      "send": "Отправьте {{{amount}}} BTC на адрес ниже",
      "confirmation": "После двух подтверждений цепочки bitcoin ≈ {{{amount}}} {{{shortName}}} поступят на Ваш адрес {{{wallet}}}...",
      "copy": "Копировать адрес",
      "copied": "Скопировано"
    },
    "order": "Заказ",
    "status": {
      "title": "Статус",
      "completed": "Заявка исполнена"
    },
    "payAmount": {
      "title": "Оплачено"
    },
    "check": "Проверить статус операции на blockexplorer.net",
    "resend-phone-code": "Не получили звонок?",
    "change-phone": "Изменить телефон",
    "sms-code": "Прослушайте 4-х значный код автоинформатора и введите его здесь",
    "registration": "Перейти на страницу регистрации",

    "internalAccount-1": "Пожалуйста, проверьте свой кошелек Indacoin,",
    "internalAccount-link": "войдя в систему",
    "internalAccount-2": "",
    "your-phone": "Ваш телефон {{{phone}}}",
    "many-requests": "Извините. Слишком много запросов на изменение телефона"
  },
  "app":{
    "detect_lang":{
      "title":"Определение Вашего языка",
      "sub":"Пожалуйста, подождите..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Покупайте и обменивайте моментально {{{longName}}} и еще 100 других криптовалют по лучшей цене в {{{country}}}",
    "title":"Покупайте криптовалюту кредитной или дебетовой картой - Indacoin",
    "description":"Покупайте и обменивайте моментально любую криптовалюту за EUR или USD: [cryptocurrency], Ethereum, Litecoin, Ripple и 100 других валют",
    "description-bitcoin":"Покупайте и обменивайте моментально любую криптовалюту: Bitcoin, Ethereum, Litecoin, Ripple и еще 100 других валют за EUR или USD",
    "description-ethereum":"Покупайте и обменивайте моментально любую криптовалюту: Ethereum, Bitcoin, Litecoin, Ripple и еще 100 других валют за EUR или USD",
    "description-litecoin":"Покупайте и обменивайте моментально любую криптовалюту: Litecoin, Bitcoin, Ethereum, Ripple и еще 100 других валют за EUR или USD",
    "description-ripple":"Покупайте и обменивайте моментально любую криптовалюту: Ripple, Bitcoin, Ethereum, Litecoin и еще 100 других валют за EUR или USD",

    "description2":"Покупайте и обменивайте моментально любую криптовалюту за EUR или USD: Bitcoin, Ethereum, Litecoin, Ripple и 100 других валют",
    "description3": "Покупайте и обменивайте моментально любую криптовалюту: Bitcoin, Ethereum, Litecoin, Ripple и 100 других валют с карт Visa & Mastercard",
    "buy": "Купить",
    "subTitle": "с помощью кредитной карты",
    "more": "более чем в 100 странах, включая",
    //"header": "Обменять Bitcoin на <span class='sub'>{{{name}}}</span>",
    "header": "Обменять мгновенно <span class='sub'>BTC</span> на <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "Покупайте криптовалюту моментально",
    "header-alt": "Обменивайте криптовалюту моментально",
    "sub": "Indacoin — это простой способ купить биткоины, а также более 100 других криптовалют при помощи Visa &amp; Mastercard",
    "sub-alt": "Indacoin — это простой способ обменять биткоины на любую из 100+ других криптовалют",
    "observe": "Узнайте о доступных крипто-валютах"
  },
  "navbar": {
    "main":"Главная страница",
    "btc": "Купить Bitcoin",
    "eth": "Купить криптовалюту",
    "affiliate": "Партнерство",
    "news": "Новости",
    "faq": "FAQ",
    "login": "Войти",
    "registration": "Зарегистрироваться",
    "platform": "Продукты",

    "logout": "Выйти",
    "buy-instantly": "Купить криптовалюту мгновенно",
    "trading-platform": "Торговая площадка",
    "mobile-wallet": "Мобильный кошелек",
    "payment-processing": "Прием банковских карт"
  },
  "currency-table": {
    "rank": "Ранк",
    "coin": "Валюта",
    "market": "Рыночная стоимость",
    "price": "Цена",
    "volume": "Объем за 24 часа",
    "change": "Рост за 24 часа",
    "available-supply": "Доступно",
    "search-cryptocurrency": "Поиск криптовалюты",

    "change-1h": "Изм. за 1ч",
    "change-24h": "Изм. за 24ч",
    "change-7d": "Изм. за 7д"
  },

  "buy-blocks":{
    "give":"Вы отдаете",
    "get":"Вы получаете"
  },
  "exchange-form": {
    "title": "Моментальная покупка криптовалюты",
    "submit": "Обменять",
    "give": "Вы даете",
    "get": "Вы получаете",
    "search": "Поиск",
    "error-page": "Приносим извинения, но покупка в настоящее время недоступна"
  },
  "pagination": {
    "page": "Страница",
    "next": "Вперед",
    "prev": "Назад",
    "first": "В начало"
  },
  "sidebar":{
    "accept":{
      "title":"Мы принимаем кредитные карты",
      "button":"Купить криптовалюту"
    },
    "youtube":{
      "title":"О нас",
      "play":"Запустить"
    },
    "orders":{
      "title": "Управление заказами",
      "order": "Заказ #",
      "confirm": "Код подтверждения",
      "check": "Проверить заказ",
      "new": "Создать новый заказ",
      "or": "Или"
    },
    "social":{
      "title":"Мы в социальных сетях"
    }
  },
  "wiki":{
    "etymology":"Этимология"
  },
  "loading": {
    "title": "Загрузка",
    "sub": "Пожалуйста подождите",
    "text": "Загрузка"
  },
  "error": {
    "title": "Ошибка",
    "sub": "Что-то пошло не так, не паникуйте",
    "text": "Мы столкнулись с неизвестно ошибкой и уже работаем над ее исправлением. Пожалуйста возвращайтесь позже.",
    "home": "Главная",
    "mainPageError1": "К сожалению, информация о доступных криптовалютах недоступна",
    "mainPageError2": "Попробуйте зайти на сайт чуть позже",
    "page-404": "Мы сожалеем, данная страница не найдена",
    "page-error": "Приносим извинения, произошла ошибка"
  },
  "features":{
    "title":"Что можно<br><span class=\"offset\">делать на <span class=\"blue\">Indacoin</span></span>",
    "column1":{
      "title":"Покупать криптовалюту",
      "text":"На Indacoin вы можете мгновенно приобрести более чем 100 различных криптовалют с помощью кредитной или дебетовой карты"
    },
    "column2":{
      "title":"Узнать подробнее о криптовалютах",
      "text":"Простые руководства на нашем сайте помогут вам разобраться в том, как работают альткоины"
    },
    "column3":{
      "title":"Принимать инвестиционные решения",
      "text":"Мощные аналитические инструменты позволят вам выявить лучший момент для ввода и вывода валюты"
    },
    "column4":{
      "title":"Покупать биткоины без регистрации",
      "text":"После проведения оплаты битконы придут на указанный вами адрес"
    },
    "column5":{
      "title":"Высказать свое мнение",
      "text":"Вы можете обсудить последние тенденции в мире криптовалют с ведущими инвесторами"
    },
    "column6":{
      "title":"Хранить все активы в одном месте",
      "text":"Вы можете хранить более чем 100 цифровых валют и управлять ими в одном кошельке Indacoin"
    }
  },
  "mockup":{
    "title":"МОБИЛЬНЫЕ КОШЕЛЬКИ",
    "text":"Наш простой смарт-кошелек Indacoin работает на Android-устройствах и на iPhone, а также в любом браузере. Отправить криптовалюту другу на телефон стало так же легко, как отослать SMS-сообщение."
  },
  "footer":{
    "column1":{
      "title":"Купить",
      "link1":"Купить Bitcoin",
      "link2":"Купить Ethereum",
      "link3":"Купить Ripple",
      "link4":"Купить Bitcoin Cash",
      "link5":"Купить Litecoin",
      "link6":"Купить Dash"
    },
    "column2":{
      "title":"Информация",
      "link1":"О нас",
      "link2":"Вопросы",
      "link3":"Правила пользования",
      "link4":"Инструкции"
    },
    "column3":{
      "title":"Инструменты",
      "link1":"API",
      "link2":"Сменить регион",
      "link3":"Мобильное приложение"
    },
    "column4":{
      "title":"Контакты"
    }
  },
  "country":{
    "AF":{
      "name":"Афганистан"
    },
    "AX":{
      "name":"Аландские острова"
    },
    "AL":{
      "name":"Албания"
    },
    "DZ":{
      "name":"Алжир"
    },
    "AS":{
      "name":"Американское Самоа"
    },
    "AD":{
      "name":"Андорра"
    },
    "AO":{
      "name":"Ангола"
    },
    "AI":{
      "name":"Ангилья"
    },
    "AQ":{
      "name":"Антарктика"
    },
    "AG":{
      "name":"Антигуа и Барбуда"
    },
    "AR":{
      "name":"Аргентина"
    },
    "AM":{
      "name":"Армения"
    },
    "AW":{
      "name":"Аруба"
    },
    "AU":{
      "name":"Австралия "
    },
    "AT":{
      "name":"Австрия"
    },
    "AZ":{
      "name":"Азербайджан"
    },
    "BS":{
      "name":"Багамские Острова"
    },
    "BH":{
      "name":"Бахрейн"
    },
    "BD":{
      "name":"Бангладеш"
    },
    "BB":{
      "name":"Барбадос"
    },
    "BY":{
      "name":"Беларусь"
    },
    "BE":{
      "name":"Бельгия"
    },
    "BZ":{
      "name":"Белиз"
    },
    "BJ":{
      "name":"Бенин"
    },
    "BM":{
      "name":"Бермудские Острова"
    },
    "BT":{
      "name":"Бутан"
    },
    "BO":{
      "name":"Боливия"
    },
    "BA":{
      "name":"Босния и Герцеговина"
    },
    "BW":{
      "name":"Ботсвана"
    },
    "BV":{
      "name":"Остров Буве"
    },
    "BR":{
      "name":"Бразилия"
    },
    "IO":{
      "name":"Британская Территория в Индийском Океане"
    },
    "BN":{
      "name":"Бруней-Даруссалам"
    },
    "BG":{
      "name":"Болгария"
    },
    "BF":{
      "name":"Буркина-Фасо"
    },
    "BI":{
      "name":"Бурунди"
    },
    "KH":{
      "name":"Камбоджа"
    },
    "CM":{
      "name":"Камерун"
    },
    "CA":{
      "name":"Канада"
    },
    "CV":{
      "name":"Кабо-Верде"
    },
    "KY":{
      "name":"Острова Кайман"
    },
    "CF":{
      "name":"Центральноафриканская Республика"
    },
    "TD":{
      "name":"Чад"
    },
    "CL":{
      "name":"Чили"
    },
    "CN":{
      "name":"Китай"
    },
    "CX":{
      "name":"Остров Рождества"
    },
    "CC":{
      "name":"Кокосовые острова"
    },
    "CO":{
      "name":"Колумбия"
    },
    "KM":{
      "name":"Коморские острова"
    },
    "CG":{
      "name":"Конго"
    },
    "CD":{
      "name":"Конго, Демократическая республика"
    },
    "CK":{
      "name":"Острова Кука"
    },
    "CR":{
      "name":"Коста-Рика"
    },
    "CI":{
      "name":"Кот-д'Ивуар"
    },
    "HR":{
      "name":"Хорватия"
    },
    "CU":{
      "name":"Куба"
    },
    "CY":{
      "name":"Кипр"
    },
    "CZ":{
      "name":"Чехия"
    },
    "DK":{
      "name":"Дания"
    },
    "DJ":{
      "name":"Джибути"
    },
    "DM":{
      "name":"Доминика"
    },
    "DO":{
      "name":"Доминиканская Республика"
    },
    "EC":{
      "name":"Эквадор"
    },
    "EG":{
      "name":"Египет"
    },
    "SV":{
      "name":"Сальвадор"
    },
    "GQ":{
      "name":"Экваториальная Гвинея"
    },
    "ER":{
      "name":"Эритрея"
    },
    "EE":{
      "name":"Эстония"
    },
    "ET":{
      "name":"Эфиопия"
    },
    "FK":{
      "name":"Фолклендские острова"
    },
    "FO":{
      "name":"Фарерские острова"
    },
    "FJ":{
      "name":"Фиджи"
    },
    "FI":{
      "name":"Финляндия"
    },
    "FR":{
      "name":"Франция"
    },
    "GF":{
      "name":"Гвиана"
    },
    "PF":{
      "name":"Французская Полинезия"
    },
    "TF":{
      "name":"Французские Южные и Антарктические территории"
    },
    "GA":{
      "name":"Габон"
    },
    "GM":{
      "name":"Гамбия"
    },
    "GE":{
      "name":"Грузия",
      "nameForMap":"Грузию"
    },
    "DE":{
      "name":"Германия"
    },
    "GH":{
      "name":"Гана"
    },
    "GI":{
      "name":"Гибралтар"
    },
    "GR":{
      "name":"Греция"
    },
    "GL":{
      "name":"Гренландия"
    },
    "GD":{
      "name":"Гренада"
    },
    "GP":{
      "name":"Гваделупа"
    },
    "GU":{
      "name":"Гуам"
    },
    "GT":{
      "name":"Гватемала"
    },
    "GG":{
      "name":"Гернси"
    },
    "GN":{
      "name":"Гвинея"
    },
    "GW":{
      "name":"Гвинея-Бисау"
    },
    "GY":{
      "name":"Гайана"
    },
    "HT":{
      "name":"Республика Гаити"
    },
    "HM":{
      "name":"Остров Херд и острова МакДональд"
    },
    "VA":{
      "name":"Ватикан"
    },
    "HN":{
      "name":"Гондурас"
    },
    "HK":{
      "name":"Гонконг"
    },
    "HU":{
      "name":"Венгрия"
    },
    "IS":{
      "name":"Исландия"
    },
    "IN":{
      "name":"Индия"
    },
    "ID":{
      "name":"Индонезия"
    },
    "IR":{
      "name":"Иран, Исламская Республика"
    },
    "IQ":{
      "name":"Ирак"
    },
    "IE":{
      "name":"Ирландия"
    },
    "IM":{
      "name":"Остров Мэн"
    },
    "IL":{
      "name":"Израиль"
    },
    "IT":{
      "name":"Италия"
    },
    "JM":{
      "name":"Ямайка"
    },
    "JP":{
      "name":"Япония"
    },
    "JE":{
      "name":"Джерси"
    },
    "JO":{
      "name":"Иордания"
    },
    "KZ":{
      "name":"Казахстан"
    },
    "KE":{
      "name":"Кения"
    },
    "KI":{
      "name":"Кирибати"
    },
    "KP":{
      "name":"Корейская Народно-Демократическая Республика"
    },
    "KR":{
      "name":"Корея, Республика"
    },
    "KW":{
      "name":"Кувейт"
    },
    "KG":{
      "name":"Киргизия",
      "nameForMap":"Киргизию"
    },
    "LA":{
      "name":"Лаосская Народно-Демократическая Республика"
    },
    "LV":{
      "name":"Латвия"
    },
    "LB":{
      "name":"Ливан"
    },
    "LS":{
      "name":"Лесото"
    },
    "LR":{
      "name":"Либерия"
    },
    "LY":{
      "name":"Ливия"
    },
    "LI":{
      "name":"Лихтенштейн"
    },
    "LT":{
      "name":"Литва"
    },
    "LU":{
      "name":"Люксембург"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Македония, Бывшая Югославская Республика"
    },
    "MG":{
      "name":"Мадагаскар"
    },
    "MW":{
      "name":"Малави"
    },
    "MY":{
      "name":"Малайзия"
    },
    "MV":{
      "name":"Мальдивы"
    },
    "ML":{
      "name":"Мали"
    },
    "MT":{
      "name":"Мальта"
    },
    "MH":{
      "name":"Маршалловы Острова"
    },
    "MQ":{
      "name":"Мартиника"
    },
    "MR":{
      "name":"Мавритания"
    },
    "MU":{
      "name":"Маврикий"
    },
    "YT":{
      "name":"Майотта"
    },
    "MX":{
      "name":"Мексика"
    },
    "FM":{
      "name":"Федеративные Штаты Микронезии"
    },
    "MD":{
      "name":"Молдова, Республика"
    },
    "MC":{
      "name":"Монако"
    },
    "MN":{
      "name":"Монголия"
    },
    "MS":{
      "name":"Монтсеррат"
    },
    "MA":{
      "name":"Марокко"
    },
    "MZ":{
      "name":"Мозамбик"
    },
    "MM":{
      "name":"Мьянма"
    },
    "NA":{
      "name":"Намибия"
    },
    "NR":{
      "name":"Науру"
    },
    "NP":{
      "name":"Непал"
    },
    "NL":{
      "name":"Нидерланды"
    },
    "AN":{
      "name":"Нидерландские Антильские острова"
    },
    "NC":{
      "name":"Новая Каледония"
    },
    "NZ":{
      "name":"Новая Зеландия"
    },
    "NI":{
      "name":"Никарагуа"
    },
    "NE":{
      "name":"Нигер"
    },
    "NG":{
      "name":"Нигерия"
    },
    "NU":{
      "name":"Ниуэ"
    },
    "NF":{
      "name":"Остров Норфолк"
    },
    "MP":{
      "name":"Северные Марианские Острова"
    },
    "NO":{
      "name":"Норвегия"
    },
    "OM":{
      "name":"Оман"
    },
    "PK":{
      "name":"Пакистан"
    },
    "PW":{
      "name":"Палау"
    },
    "PS":{
      "name":"Палестинская территория, Оккупированная"
    },
    "PA":{
      "name":"Панама"
    },
    "PG":{
      "name":"Папуа — Новая Гвинея"
    },
    "PY":{
      "name":"Парагвай"
    },
    "PE":{
      "name":"Перу"
    },
    "PH":{
      "name":"Филиппины"
    },
    "PN":{
      "name":"Острова Питкэрн"
    },
    "PL":{
      "name":"Польша"
    },
    "PT":{
      "name":"Португалия"
    },
    "PR":{
      "name":"Пуэрто-Рико"
    },
    "QA":{
      "name":"Катар"
    },
    "RE":{
      "name":"Реюньон"
    },
    "RO":{
      "name":"Румыния"
    },
    "RU":{
      "name":"Российская Федерация",
      "nameForMap":"Российскую Федерацию"
    },
    "RW":{
      "name":"РУАНДА"
    },
    "SH":{
      "name":"Остров Святой Елены"
    },
    "KN":{
      "name":"Сент-Китс и Невис"
    },
    "LC":{
      "name":"Сент-Люсия"
    },
    "PM":{
      "name":"Сен-Пьер и Микелон"
    },
    "VC":{
      "name":"Сент-Винсент и Гренадины"
    },
    "WS":{
      "name":"Самоа"
    },
    "SM":{
      "name":"Сан-Марино"
    },
    "ST":{
      "name":"Сан-Томе и Принсипи"
    },
    "SA":{
      "name":"Саудовская Аравия"
    },
    "SN":{
      "name":"Сенегал"
    },
    "CS":{
      "name":"Сербия и Черногория"
    },
    "SC":{
      "name":"Сейшельские Острова"
    },
    "SL":{
      "name":"Сьерра-Леоне"
    },
    "SG":{
      "name":"Сингапур"
    },
    "SK":{
      "name":"Словакия"
    },
    "SI":{
      "name":"Словения"
    },
    "SB":{
      "name":"Соломоновы Острова"
    },
    "SO":{
      "name":"Сомали"
    },
    "ZA":{
      "name":"Южная Африка"
    },
    "GS":{
      "name":"Южная Георгия и Южные Сандвичевы Острова"
    },
    "ES":{
      "name":"Испания"
    },
    "LK":{
      "name":"Шри Ланка"
    },
    "SD":{
      "name":"Судан"
    },
    "SR":{
      "name":"Суринам"
    },
    "SJ":{
      "name":"Шпицберген и Ян-Майен"
    },
    "SZ":{
      "name":"Свазиленд"
    },
    "SE":{
      "name":"Швеция"
    },
    "CH":{
      "name":"Швейцария"
    },
    "SY":{
      "name":"Сирийская Арабская Республика"
    },
    "TW":{
      "name":"Тайвань, Провинция Китайской Республики"
    },
    "TJ":{
      "name":"Таджикистан"
    },
    "TZ":{
      "name":"Танзания, Объединенная Республика"
    },
    "TH":{
      "name":"Таиланд"
    },
    "TL":{
      "name":"Восточный Тимор"
    },
    "TG":{
      "name":"Того"
    },
    "TK":{
      "name":"Токелау"
    },
    "TO":{
      "name":"Тонга"
    },
    "TT":{
      "name":"Тринидад и Тобаго"
    },
    "TN":{
      "name":"Тунис"
    },
    "TR":{
      "name":"Турция"
    },
    "TM":{
      "name":"Туркмения"
    },
    "TC":{
      "name":"Теркс и Кайкос"
    },
    "TV":{
      "name":"Тувалу"
    },
    "UG":{
      "name":"Уганда"
    },
    "UA":{
      "name":"Украина",
      "nameForMap":"Украину"
    },
    "AE":{
      "name":"Объединенные Арабские Эмираты"
    },
    "GB":{
      "name":"Великобритания",
      "nameForMap": "Великобританию"
    },
    "US":{
      "name":"США"
    },
    "UM":{
      "name":"Внешние малые острова США"
    },
    "UY":{
      "name":"Уругвай"
    },
    "UZ":{
      "name":"Узбекистан"
    },
    "VU":{
      "name":"Вануату"
    },
    "VE":{
      "name":"Венесуэла"
    },
    "VN":{
      "name":"Вьетнам"
    },
    "VG":{
      "name":"Виргинские Острова, Британские"
    },
    "VI":{
      "name":"Виргинские Острова, Американские"
    },
    "WF":{
      "name":"Уоллис и Футуна"
    },
    "EH":{
      "name":"Западная Сахара"
    },
    "YE":{
      "name":"Йемен"
    },
    "ZM":{
      "name":"Замбия"
    },
    "ZW":{
      "name":"Зимбабве"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Купить",
  "sell":"Продать",
  "weWillCallYou":"Если вы впервые используете данную карту, вам необходимо будет ввести 2 секретных кода - из вашей банковской выписки и от звонка автоинформатора на ваш телефон.",
  "exchangerStatuses":{
        "TimeOut": "Время заявки истекло",
        "Error": "Произошла ошибка, пожалуйста напишите в службу поддержки support@indacoin.com",
        "WaitingForAccountCreation": "Ожидание подтверждения создания аккаунта на Indacoin (мы послали вам регистрационное письмо)",
        "CashinWaiting": "Ваш платеж обрабатывается",
        "BillWaiting": "Ожидание оплаты выставленного счета клиентом",
        "Processing": "Заявка обрабатывается",
        "MoneySend": "Средства отправлены",
        "Completed": "Заявка исполнена",
        "Verifying": "Верификация",
        "Declined": "Отклонено",
        "cardDeclinedNoFull3ds": "Ваша карта отклонена, потому что ваш банк не поддерживает опцию полного 3ds по данной карте, обратитесь в службу поддержки вашего банка",
        "cardDeclined": "Ваша карта отклонена, если у вас есть вопросы обращайтесь в службу поддержки support@indacoin.com",
        //"Non3DS": "Ваш платеж обрабатывается, это может занять до 3-х минут. Если уже прошло 3 минуты, и вы все еще видите этот статус - это значит, что к сожалению ваш платеж был отклонен. Убедитесь в вашем банке, что вы используете карту с 3D-secure (Verified by Visa или MasterCard SecureCode), что означает ввод секретного кода при каждом платеже в интернете, который обычно приходит на ваш номер вашего телефона."
        "Non3DS": "К сожалению, Ваш платеж был отклонен. Убедитесь в Вашем банке, что вы используете карту с 3D-secure (Verified by Visa или MasterCard SecureCode), что означает ввод секретного кода при каждом платеже в интернете, который обычно приходит на номер Вашего телефона."
  },
  "exchanger_fields_ok":"Нажмите, чтобы купить биткоины",
  "exchanger_fields_error":"Ошибка заполнения. Проверьте все поля",
  "bankRejected":"Транзакция была отклонена вашим банком. Пожалуйста свяжитесь с банком и повторите платеж с карты",
  "wrongAuthCode":"Вы ввели неправильный код авторизации, пожалуйста обратитесь в службу поддержки support@indacoin.com",
  "wrongSMSAuthCode":"Вы ввели неправильный код из телефона",
  "getCouponInfo":{
    "indo":"Данный купон является недопустимым",
    "exetuted":"Данный купон уже использован",
    "freeRate":"Активирован купон на скидку"
  },
  "ip":"Your IP has been temporarily blocked",
  "priceChanged":"Цена изменилась. Ок? Вы получите ",
  "exchange_go":"– Обменять",
  "exchange_buy_btc":"– Купить биткоины",
  "month":"Янв Фев Мар Апр Май Июн Июл Авг Сен Окт Нояб Дек",
  "discount":"Cкидка",
  "cash":{
    "equivalent":" эквивалентно ",
    "card3DS":"Мы принимаем к оплате только 3D-Secure карты (Verified by Visa or MasterCard SecureCode)",
    "cashIn":{
      "1": "Банковские карты (USD)",
      "2": "Платежные терминалы",
      "3": "QIWI",
      "4": "Башкомснаббанк",
      "5": "Терминалы Новоплат",
      "6": "Терминалы Элекснет",
      "7": "Банковские карты (USD)",
      "8": "Альфа-клик",
      "9": "Международный банк. перевод",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Яндекс.Деньги",
      "21": "Элекснет",
      "22": "Кассира.нет",
      "23": "Мобил элемент",
      "24": "Связной",
      "25": "Евросеть",
      "26": "PerfectMoney",
      "27": "Indacoin-внутренний",
      "28":	"CouponBonus",
      "29": "Яндекс.Деньги",
      "30": "OKPay",
      "31": "Партнерская программа",
      "33": "Payza",
      "35": "BTC-E код",
      "36": "Банковские карты (USD)",
      "37": "Банковские карты (USD)",
      "39": "Международный банковский перевод",
      "40": "Яндекс.Деньги",
      "42": "QIWI(Ручное)",
      "43": "UnionPay",
      "44": "QIWI(Автоматическое)",
      "45": "Оплата со счета телефона",
      "49": "LibrexCoin",
      "50": "Банковские карты (EURO)",
      "51": "QIWI(Автоматическое)",
      "52": "Оплата со счета телефона",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Пожалуйста, введите корректный крипто адрес",
      "country": "Недопустимая страна",
      "alfaclick": "Недопустимый логин",
      "pan": "Недопустимый номер карты",
      "cardholdername": "Недопустимые данные",
      "fio": "Недопустимые данные",
      "bday": "Неправильная дата рождения",
      "city": "Недопустимые данные",
      "address": "Недопустимый адрес",
      "ypurseid": "Недопустимый номер кошелька",
      "wpurseid": "Недопустимый номер кошелька",
      "pmpurseid": "Недопустимый номер аккаунта",
      "payeer": "Недопустимый номер аккаунта",
      "okpayid": "Недопустимый номер кошелька",
      "purse": "Недопустимый номер кошелька",
      "phone": "Недопустимый телефон",
      "btcaddress": "Недопустимый адрес",
      "ltcaddress": "Недопустимый адрес",
      "lxcaddress": "Недопустимый адрес",
      "ethaddress": "Недопустимый адрес",
      "properties": "Недопустимые реквизиты",
      "email": "Недопустимый email",
      "card_number": "Данная карта не поддерживается проверьте номер",
      "inc_card_number": "Неверный номер карты",
      "inn": "Некорректный номер",
      "poms": "Некорректный номер",
      "snils": "Некорректный номер",
      "cc_expr": "Неверная дата",
      "cc_name": "Неверное имя",
      "cc_cvv": "Неверный cvv"
    }
  },
  "change": {
    "wallet": "Создать новый кошелек / У меня уже есть учетная запись Indacoin",
    //"accept": "Я согласен с Пользовательским Соглашением и AML политикой",
    "accept": "Я согласен с Условиями Использования и Политикой Конфиденциальности",
    "coupone": "У меня есть купон",
    "watchVideo": "Посмотреть видео",
    "seeInstructions": "Помошь",
    "step1": {
      "title": "Создание заказа",
      "header": "Введите платежную информацию",
      "subheader": "Позвольте нам узнать, что Вы желаете приобрести"
    },
    "step2": {
      "title": "Платежная информация",
      "header": "Введите информацию о Вашей кредитной карте",
      "subheader": "Мы принимаем только Visa и Mastercard",
      "card": {
        "header": "Где я могу найти этот код?",
        "text": "Вы можете найти CVV на обратной стороне карты. Этот код состоит из 3-х цифр"
      }
    },
    "step3": {
      "title": "Контактная информация",
      "header": "Введите Вашу контактную информацию",
      "subheader": "Позвольте нам лучше узнать наших клиентов"
    },
    "step4": {
      "title": "Верификация",
      "header": "Введите Ваш код со страницы оплаты",
      "subheader": "Ваша платежная информация"
    },
    "step5": {
      "title": "Информация по заказу"
    },
    "step6": {
      "title": "Запись видео",
      "header": "Покажите Ваш паспорт и Ваше лицо",
      "description": "Покажите нам, что с вами можно иметь дело",
      "startRecord": "Нажмите на Старт кнопку чтобы начать запись",
      "stopRecord": "Нажмите Стоп для завершения записи"
    },
    "nextButton": "Далее",
    "previousButton": "Назад",
    "passVideoVerification": "Видео верификация",
    "passKYCVerification": "KYC верификация",
    "soccer-legends": "Я согласен с тем, что мои личные данные будут переданы и обработаны компанией Soccer Legends Limited",
    "withdrawalAmount": "Сумма к зачислению на счет"
  },
  "form": {
    "tag": "{{{currency}}}-тэг",
    "loading": "Загрузка... Пожалуйста, подождите",
    "youGive": "Вы отдаете",
    "youTake": "Вы получаете",
    "email": "Почта",
    "password": "Пароль",
    //"cryptoAdress": "Адрес {{{name}}}-кошелька",
    "cryptoAdress": "Адрес крипто-кошелька",
    "externalTransaction": "ID транзакции",
    "coupone": "У меня есть купон",
    "couponeCode": "Введите номер купона",
    "cardNumber": "Номер карты",
    "month": "ММ",
    "year": "ГГ",
    "name": "Имя",
    "fullName": "ФИО",
    "mobilePhone": "Номер мобильного телефона",
    "birth": "Дата Вашего рождения в формате ДД.ММ.ГГГГ",
    "videoVerification": "Видео верификация",
    "verification": "Верификация",
    "status": "Проверить статус",
    "login": {
      "captcha": "Каптча",
      "reset": "Сбросить",
      "forgot": "Забыли пароль?",
      "signIn": "Войти"
    },
    "registration": {
        "create": "Создать"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Простота проверка",
      "title-2": "Доступно более 100 альткоинов",
      "title-3": "Нет скрытых комиссий",
      "title-4": "Без регистрации"
    },
    "title": "Мы работаем с более чем 100 странами",
    "subtitle": "включая {{{country}}}",
    "shortTitle": "Мы работаем с более чем 100 странами",
    "text1": "Простота проверки",
    "popupText1": "Только первые покупки требуют подтверждения по телефону",
    "text2": "100 поддерживаемых альткоинов",
    "popupText2": "Вы можете купить более чем 100 самых перспективных криптовалют с кредитной картой",
    "text3": "Нет скрытых комиссий",
    "popupText3": "Вы получите именно указанное количество альткоинов, сколько было указано в сделке",
    "text4": "Нет регистрации",
    "popupText4": "Вам не нужна авторизация, криптовалюта отправляется по указанному Вами адресу"
  },
  "buying-segment": {
      "title": "Более 500 000 человек",
      "sub-title": "пользуются услугами Indacoin с 2015"
  },
  "phone-segment": {
      "title": "Загрузите наше приложение",
      "text1": "Покупка и хранение более 100 криптовалют в одном кошельке",
      "text2": "Отслеживание динамики изменения инвестиционного портфеля",
      "text3": "Отправить криптовалюту другу на телефон стало так же легко, как отослать SMS-сообщение"
  },
  "social-segment": {
    "buttonText": "Читать на Facebook",
    "on": "на Facebook",
    "facebook-review-1": "хорошая страница биткойнов, быстрый и простой процесс, я рекомендую их",
    "facebook-review-2": "Блестящее обслуживание и удобство в использовании, люди там дружелюбны и очень полезны. Хотя это цифровая транзакция, на ее стороне есть человек (который может быть МАССИВНОЙ справкой) и, конечно же, уверен, услуга 5star, которую я буду использовать снова",
    "facebook-review-3": "Поскольку я только начинаю испытывать все, что окружает мир криптовалюты, сделать обмен на их веб-сайте более чем просто. и их поддержка в случае, если ур застрял или просто обратится за помощью, это гораздо больше, чем просто поразительно.",
    "facebook-review-4": "Очень быстрая оплата, отличная поддержка и отличные обменные курсы.",
    "facebook-review-5": "Я решил купить эту услугу по битуам на 100 евро, и было легко начать оплату. Хотя у меня были некоторые проблемы и вопросы, мне почти мгновенно помогала их команда поддержки. Это заставило меня чувствовать себя намного более комфортно, покупая биткойн с Indacoin. И я планирую купить больше в будущем.",
    "facebook-review-6": "Я просто использовал Indacoin для совершения транзакции для покупки биткойнов. Сделка прошла гладко, а затем проверочный звонок. Его очень простой в использовании и удобный портал для покупки битовых монет. Я бы порекомендовал эту платформу для покупки биткойнов!",
    "facebook-review-7": "Трудно найти легкую сделку по кредитным картам для покупки криптографической валюты, и Индкоин очень хорошо относился ко мне, терпение с пользователями из других стран, которые не понимают их язык, онлайн-чат о готовности. Indcoin Спасибо !!!",
    "facebook-review-8": "Быстро и легко. Онлайн-поддержка была очень быстрой и полезной. ProTip: использование приложения снижает расходы на 9%.",
    "facebook-review-9": "Это мой второй раз, используя Indacoin - отличное обслуживание, оперативная оперативная блестящая поддержка клиентов. Я обязательно буду использовать снова.",
    "facebook-review-10": "Чрезвычайно большое обслуживание и общение, пошаговая помощь с чатом в реальном времени и абсолютно надежная, они даже позвонили мне со мной, спросив меня и объяснив мне, как сделать проверку на транзакцию, самый простой способ купить биткойн без каких-либо проблем, спасибо Indacoin будет делать больше транзакций с вами.",
    "facebook-review-11": "Отличный сервис, быстрый процесс. Полезное обслуживание клиентов. Быстро реагирует. Простой способ купить криптовалюту. В частности, сервис, честно говоря, это единственная услуга, которую я использую для покупки криптовалюты. Я доверяю им, чтобы обработать мою покупку. Рекомендуется всем, кто занимается покупкой криптовалюты. И последнее, но не менее важное: только одно слово для Indacoin: Отлично !!!",
    //"facebook-review-12": "IndaCoin имеет супер легкое, надежное и быстрое обслуживание. Они также очень быстро отвечали на все мои вопросы и помогали мне с моей передачей. Очень рекомендую их услуги всем! 10/10!",
    "facebook-review-12": "Я купил биткойны за 200 евро, и сайт является законным. Хотя для транзакции потребовалось некоторое время, обслуживание клиентов отличное. Я обратился к их службе поддержки, и они немедленно разобрали мою проблему. Определенно рекомендую этот сайт всем. иногда из-за высокого трафика время транзакции колеблется между 8-10 часами. Не паникуйте, если вы не получаете монеты мгновенно, иногда это займет время, но вы наверняка получите свои монеты.",
    "facebook-review-13": "Отличная работа Indacoin. Имел лучший опыт до сих пор в покупке монеты с моей кредитной карточкой с Indacoin .. Консультант Andrew сделал отличное обслуживание клиентов, и я был впечатлен, как передача слишком меньше 3 минут ... продолжит использовать их :)",
    "facebook-review-14": "Indacoin - это простой и удобный интерфейс, который покупатели могут легче купить биткойны на платформе. Легко поэтапно помогает от консультантов, и они действительно готовы помочь вам решить вашу проблему сразу. Я намеренно собираюсь отсылать людей к Indacoin, и они постоянно обновляют меня в моих ситуациях, и они готовы пройти лишнюю милю, чтобы решить ваши проблемы. Спасибо, индакоин!",
    "facebook-review-15": "Самый простой и быстрый способ приобрести криптовалюты, которые я нашел во многих поисках. Отличная работа, ребята, спасибо!",
    "facebook-review-16": "По-настоящему простой способ купить монеты онлайн. Отличное обслуживание клиентов тоже. Удивительно, что у них есть также монеты, чтобы покупать прямо. Принимает осложнения из обменов. Настоятельно рекомендую это всем после первого положительного опыта.",
    "facebook-review-17": "Отличный сервис. Удобный и удобный для покупки различных монет / токенов.",
    "facebook-review-18": "Это одна из лучших платформ для обмена BTC. Я сделал несколько обменов и это было быстро.",
    "facebook-review-19": "Отличное обслуживание, удобство в использовании, легко купить различные криптовалюты и токены ERC20 с кредитной карты. Очень быстро и безопасно! Сервис и платформа, которые я искал! Рекомендую покупать на Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "Более {{{count}}} различных криптовалют доступны для покупки на {{{name}}}"
  },

  "invalidEmail": "Введите корректный Email",
  "invalidWallet": "Введите корректный адрес кошелька",
  "invalidFullName": "Введите Ваше полное имя",
  "invalidPhone": "Введите корректный номер телефона",
  "invalidBirthday": "Введите корректную дату рождения. Формат: ДД.ММ.ГГГГ",
  
  "mobileMockups": {
    "title": "Indacoin в Вашем мобильном",
    "text": "Наш простой смарт-кошелек Indacoin работает на Android-устройствах и на iPhone, а также в любом браузере. Отправить криптовалюту другу на телефон стало так же легко, как отослать SMS-сообщение."
  },
  
  "news-segment": {
    "no-news": "Для Вашего языка нет новостей",
    "news-error": "Приносим извинения, новости в настоящее время недоступны",
    "comments": "Комментарии",
    "send": "Отправить",
    "news1": "Друзья, мы рады сообщить, что теперь вы можете приобрести Ethereum/Эфир (ETH) со своей банковской карты на Indacoin. Если вы уже использовали свою карту для покупки биткоинов, то вам не потребуется никакой дополнительной верификации.",
    "news2": "Внимание, комиссия на торгах в бирже изменена, теперь за исполнение своего отложенного ордера вы получаете награду в 1%, за исполнение же ордера по рыночной цене комиссия была повышена до 2%. Также мы понизили комиссию по вводу денег с карт с 7% до 4%. Данная акция была сделана для уменьшения торгового спреда.",
    "news3": "Друзья, у нас важные нововведения касательно инвестирования денежных средств на Indacoin. С 29 декабря прекращает действовать автоматическое начисление процентов на остаток вашего счета в долларах исходя из ставки 8% годовых. Для получения гарантированного дохода, вам необходимо зайти в раздел \"инвестиции\" и открыть вклад на сумму от 10$ до 15 000$ сроком от 30 до 365 дней. Ставка варьируется от 10.76% до 11.98%. Также мы снизили штраф за досрочное изъятие средств с 10% до 5%.",
    "news4": "Дорогие пользователи, вынуждены сообщить, что начиная с 9 ноября 2015 года комиссия за исполнение отложенных и рыночных ордеров составляет 0.3%"
  },

  "news": {
    "anotherNews": "Другие новости",
    "readMore": "Читать далее",
    "topic1": {
      "title": "Запуск нового сайта",
      "full": "Мы рады сообщить, что сделали полный редизайн нашего сайта. Теперь покупать криптовалюту стало еще удобнее и быстрее. Увеличился и выбор цифровой валюты: мы добавили покупку более 200 самых популярных коинов, чтобы вы могли сформировать по-настоящему диверсифицированный инвестиционный портфель в одном месте.",
      "short": "Мы рады сообщить, что сделали полный редизайн нашего сайта...",
      "date": "2 Декабря, 2017"
    },
    "topic2": {
      "title": "Новое приложение для Android",
      "full": "Друзья, в магазине Google Play стало доступно наше приложение для устройств на платформе Android, с помощью которого вы можете не только приобрести более 100 альткоинов и управлять ими, используя всего один мобильный кошелек, но и осуществлять денежные переводу по номеру телефона получателя.",
      "short": "Друзья, в магазине Google Play стало доступно наше приложение для устройств на платформе Android...",
      "date": "8 Апреля, 2017"
    },
    "topic3": {
      "title": "Долгожданное приложение для iPhone",
      "full": "Наконец-то мы готовы объявить о запуске приложения для iOS с возможностью покупки более 100 самых популярных цифровых валют, которое может позволить вам отслеживать динамику ваших инвестиций в криптовалюту из единого мобильного приложения.",
      "short": "Наконец-то мы готовы объявить о запуске приложения для iOS с возможностью покупки более 100 самых...",
      "date": "21 Июня, 2017"
    }
  },

  "orderText": "Введите данные о вашем заказе, чтобы получить актуальный статус транзакции",
  "tags": {
    "title-btc": "Обменять Bitcoin BTC на {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Купить [cryptocurrency] с кредитной карты в [country] - Indacoin",
    "title2": "Купить Bitcoin с кредитной карты по всему миру - Indacoin",
    "title3": "Купить {{{cryptocurrency}}} с кредитной карты в {{{country}}} - Indacoin",
    "title4": "Купить криптовалюту с кредитной карты в {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Партнерская программа Indacoin",
      "news": "Последние новости Indacoin",
      "help": "Обмен криптовалюты F.A.Q. - Indacoin",
      "api": "API обмена криптовалют - Indacoin",
      "terms-of-use": "Правила использования и AML политика - Indacoin",
      "login": "Вход - Indacoin",
      "register": "Регистрация - Indacoin",
      "locale": "Изменить регион - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) графики цен и торговый оборот - Indacoin"
    },
    "descriptions": {
      "affiliate": "Партнерская программа Indacoin позволяет зарабатывать до 3% каждой покупки с помощью кредитной карты. Вознаграждение будет отправлено на Ваш биткоин-кошелек Indacoin",
      "news": "Посетите наш официальный новостной блог, чтобы оставаться в курсе последних новостей криптовалютной индустрии! Много полезной информации и комментарии экспертов Indacoin в одном месте!",
      "help": "Найдите ответы на все Ваши вопросы о том, как покупать, продавать и обменивать криптовалюту на Indacoin.",
      "api": "Наш API обеспечивает мгновенную покупку и обмен любой криптовалюты с помощью карт Visa & Mastercard. Проверьте интеграцию и примеры.",
      "terms-of-use": "Общая информация. Правила пользования. Предоставляемые услуги. Перевод средств. Передача прав. Комиссии. Политика AML.",
      "login": "Войти или создать новый аккаунт",
      "register": "Создать новый аккаунт",
      "locale": "Выберите Ваш регион и мы запомним Ваш выбор для следующего посещения Indacoin",
      "currency": "Актуальные цены для {{{fullCurrency}}} за USD с объемом торгов и информацией о прошлых курсах"
    }
  },
  "registerChange": {
    "unavailable": "Данная криптовалюта в настоящее время недоступна, но в скором времени она снова будет доступна",
    "unknown": "Неизвестная ошибка сервера",
    "error": "Ошибка отправки данных",
    "3ds": "Карта не 3ds. Пожалуйста, используйте другую карту",
    "invalidWalletAddress": "Адрес кошелька неверный",
    "scam": "Мы заподозрили мошенническую активность. Мы не можем завершить Вашу транзакцию. Пожалуйста, напишите на support@indacoin.com",
    "limit": "Превышен лимит",
    "phone": "Указанный Вами телефон не поддерживается"
  },
  "usAlert": "К сожалению Indacoin не работает с гражданами США",
  "wrongPhoneCode": "Вы ввели неправильный код со своего телефона",
  "skip": "Пропустить",
  "minSum": "Минимальная цена для покупки",
  "currency": {
    "no-currency": "Для данной криптовалюты график недоступен",
    "presented": {
      "header": "Биткойн был представлен миру в 2009 году и в 2018 году все еще остается основной криптовалютой мира"
    },
    "difference": {
      "title": "Чем биткоин отличается от обычных денег?",
      "header1": "Ограниченная эмиссия",
      "text1": "Общее количество биткоинов, которое может быть выпущено, составляет 21 миллион. В настоящее время в обороте находится более 16 миллионов монет. И чем популярнее становится биткойн, тем быстрее он растет в цене.",
      "header2": "Глобальная валюта",
      "text2": "Ни одно правительство не может контролировать биткойн. Он управляется только математическим алгоритмом.",
      "header3": "Анонимность",
      "text3": "Вы можете использовать биткоины в любое время и в любом количестве. Криптовалюта — настоящая находка для тех, кто не хочет раскрывать назначение платежа, а также источник средств при проведении операций со своим банковским счетом.",
      "header4": "Скорость",
      "text4": "Вашему банку потребуется время, чтобы перевести деньги, особенно когда дело доходит до больших сумм. На блокчейне биткоина процедура отправки и получения средств займет считанные минуты.",
      "header5": "Прозрачность",
      "text5": "Все транзакции биткоина можно могут быть проверены на блокчейне."
    },
    "motivation": {
      "title": "Будучи самой первой валютой такого рода, биткоину удалось изменить саму идею денег. К настоящему моменту рыночная капитализация Bitcoin достигает более $191 млрд. и превосходит показатели таких гигантов, как Tesla, General Motors и FedEx"
    },
    "questions": {
      "header1": "Что такое цифровая валюта или криптовалюта?",
      "text1": "Криптовалюта — это цифровой актив, который функционирует как средство обмена с использованием криптографии для проведения транзакций и контроля над созданием дополнительных единиц валюты. Она не имеет физической формы и децентрализована в отличие от обычных банковских систем.",
      "header2": "Как происходит транзакция в сети Bitcoin?",
      "text2": "Каждая транзакция имеет 3 измерения: Ввод – источник биткойна, используемый в транзакции, который предоставляет информацию о том, где владелец получил коин. Сумма – количество используемых «монет». Вывод – адрес получателя денег.",
      "header3": "Откуда появился Биткоин?",
      "text3": "Хотя имя создателя биткоина всем известно — Сатоши Накамото, скорее всего, это псевдонимом программиста или даже группы специалистов-криптографов. В принципе, биткоин создается из 31 000 строк кода, написанных «Сатоши» и контролируется только программным обеспечением.",
      "header4": "Как работает биткоин?",
      "text4": "Транзакции происходят между кошельками биткоина, и они полностью прозрачны. Каждый может проверить на блокчейне происхождение и историю обращения любой «монеты». По факту, сами коины не имеют ни физической, ни виртуальной формы, у них есть только история транзакций.",
      "header5": "Что такое биткойн-кошелек?",
      "text5": "Биткойн-кошелек – это программа, которая позволяет вам управлять вашей криптовалютой: получать ее и отправлять другим пользователям, проверять баланс и курс обмена. Хотя в таких кошельках нет «монет» как таковых, он содержит цифровые ключи, используемые для доступа к адресам биткоинов и подписания транзакций. Популярные кошельки: "
    },
    "history": {
      "title": "Краткая история биткоина",
      "text1": "Согласно легенде, в этот момент Сатоши Накамото начинает работу над биткоином",
      "text2": "В сети биткоина проведена первая транзакция. Стоимость BTC  рассчитывается на основе стоимости затраченной электроэнергии, используемой для создания одной монеты. За 1 доллар США можно получить 1,306,03 BTC",
      "text3": "25% всех биткоинов уже сгенерированы",
      "text4": "Dell и Windows начинают принимать к оплате биткоины",
      "text5": {
        "part1": "Цена BTC увеличивается с 1,402 до 19,209 долларов США всего за несколько месяцев.",
        "part2": "17 декабря 2017 года стоимость биткоина достигает 20 000 долларов США за одну монету"
      },
      "text6": "Появление портала bitcoin.org",
      "text7": {
        "part1": "Поистине исторический момент: 10 000 BTC используются для косвенной покупки 2 пицц. Сегодня за ту же сумму вы можете приобрести уже 2 500 пиццы пепперони.",
        "part2": "Цена биткоина растет с 0,008 доллара США до 0,08 доллара США за 1 BTC всего за 5 дней.",
        "part3": "Первая мобильная транзакция"
      },
      "text8": {
        "part1": "Министерство финансов США классифицирует BTC как конвертируемую децентрализованную виртуальную валюту.",
        "part2": "В Сан-Диего, Калифорния, установлен первый биткоин-банкомат.",
        "part3": "Цена биткоина удваивается и достигает 503,10 долларов США. В том же месяце она вырастет до 1 000 долларов США"
      },
      "text9": "Федеральный судья США исключает, что биткоин является «денежным средством в обычном значении этого термина»"
    },
    "graph": {
      "buy-button": "Купить"
    },
    "ethereum": {
      "presented": {
        "header": "Если у нас уже есть Bitcoin, тогда зачем нам Ethereum? Давайте разберемся! Биткоин обеспечивает честные и прозрачные денежные переводы. Эфириум  может играть ту же роль, однако он предлагает гораздо больше функциональных возможностей."
      },
      "questions": {
        "header1": "Что такое Ethereum?",
        "text1": "Ethereum — это платформа с открытым исходным кодом, которая использует технологию блокчейна для распределенных вычислений. Она функционирует на основе смарт контрактов и имеет собственную криптовалюту, называемую «эфиром», в качестве платежного средства.",
        "header2": "Как появился Ethereum?",
        "text2": "Проект Ethereum был впервые предложен в конце 2013 года канадским программистом Виталиком Бутериным. После сбора средств на строительство платформы летом 2014, сеть была представлена миру в июле 2015 года.",
        "header3": "Что такое ICO?",
        "text3": "Многие проекты основаны на блокчейне Ethereum, и они также могут собирать средства с помощью внутренней валюты Ether во время ICO, так называемого первоначального предложения монет. Весь процесс проведения ICO очень похож на то, что делает Kickstarter. Программисты предлагают разные значения стоимости в обмен на криптовалюту (как правило, Ether) для покрытия расходов на разработку своего проекта. Когда они достигают определенной заранее заданной суммы или даты, собранные средства попадают в распоряжение компании, организовавшей ICO. Если финансовая цель не будет достигнута, валюта возвращаются к первоначальным инвесторам.",
        "header4": "Что такое эфир?",
        "text4": "Эфир (ETH) — это цифровая валюта или токен на блокчейне Ethereum. Помимо того, что он является средством оплаты различных услуг на платформе Ethereum, он также используется на криптовалютных биржах и может быть обменен на фиатные деньги.",
        "header5": "Что такого особенного в «умных контрактах» и каково их отличие от обычных договоров?",
        "text5": "Каждая транзакция имеет 3 измерения: ввод — источник эфира, используемый в транзакции, указывающий на то, где его владелец получил ту или иную монету. Сумма — количество используемых «монет». Основная цель смарт контракта — убедиться, что все условия контракта выполнены надлежащим образом. Его децентрализованность обеспечивает большую безопасность для участников сети Ethereum при помощи несмещенных приложений, хранящихся на блокчейне и необходимых для проверки и контроля над соблюдением условий контракта. Эта технология минимизирует риски мошенничества по несоблюдению условий договоров хотя бы одной из сторон. При использовании обычного контракта вам, как правило, нужна третья сторона (юрист) для решения любых проблем с нарушением пунктов договора, смарт контракты же беспристрастны и для обеспечения безопасности пользуются математическими методами.",
        "header6": "Чем Ethereum отличается от Bitcoin?",
        "text6": "Вдохновленный биткоином, эфир также использует технологию блокчейна, и функционирует при помощи множества персональных компьютеров со всего мира. В качестве вознаграждения за поддержание работы сети люди получают криптовалюту –– Ether. Цена монеты устанавливается на биржах в процессе продажи и покупки этой криптовалюты, так же как и у биткоина. Но разница между двумя валютами огромна. Ethereum –– это, прежде всего, платформа для распределенных вычислений, она дает возможность программистам создавать на своей базе различные децентрализованные приложения (Dapps), получать средства для операционных расходов. Таким образом, она не требует посредников и, как следствие, дополнительных издержек. К тому же, блокчейн Ethereum использует смарт контракты для упрощения отношений между сторонами и собственную криптовалюту «эфир» в качестве способа оплаты услуг внутри сети. Хотя биткоин и эфириум крупнейшие криптовалюты и имеют некоторое сходство, их цели изначально различны. Bitcoin –– это только валюта, а Ether пригоден в основном для внутренних целей на платформе Ethereum.",
        "header7": "Какие кошельки для Ethereum самые популярные?",
        "text7": "Кошелек Ethereum – это программа, которая позволяет вам управлять криптовалютой: получать ее и отправлять на другие кошельки, проверять баланс и обменный курс Ethereum. Популярные кошельки: "
      },
      "history": {
        "title": "Краткая история Ethereum",
        "text1": "Начало сбора средств для создания сети Ethereum",
        "text2": "Децентрализованная автономная организация (DAO) создана на платформе Ethereum",
        "text3": "Ethereum завоевывает 50%-ую долю рынка в первоначальном выпуске монет (ICO)",
        "text4": "Обменный курс Ethereum достигает 400 долларов США (рост на 5 000% с января того же года)",
        "text5": "Канадский программист и основатель журнала «Биткойн» Виталик Бутерин предлагает проект Ethereum",
        "text6": "30 июля – запуск платформы в интернете",
        "text7": "DAO взломан, стоимость Ethereum падает с 21,5 до 8 долларов США. Это приводит к хард-форку криптовалюты в 2017 году",
        "text8": "Создается Enterprise Ethereum Alliance. Среди участников Toyota, Samsung, Microsoft, Intel, Deloitte и другие крупные компании. Основная цель альянса –– определить, как функционал Ethereum может использоваться в различных сферах: в управлении, банковском деле, здравоохранении, IT и т. д.",
        "text9": {
          "part1": "Стоимость всех криптовалют снижается после того, как Китай запрещает ICO, но быстро восстанавливается.",
          "part2": "Рыночная капитализация Ethereum достигает своего исторического максимума на отметке в 36 миллиардов долларов"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "name": "Ваше имя",
        "phone": "Ваш телефон",
        "telegram": "Ваш Telegram",
        "email": "Адрес почты*",
        "website": "Сайт",
        "project-kind": "Какой у Вас проект?",
        "message": "Введите Ваше сообщение",
        "mail-message": "Поле почта* обязательно",
          "kinds": {
            "default": "Тип проекта",
            "ico": "ICO",
            "exchange": "Обмен криптовалюты \\ биржа",
            "wallet": "Криптокошелек",
            "crypto-fund": "Криптофонд",
            "mobile-app": "Мобильное приложение",
            "other": "Другое"
          },
        "open": "Заполнить форму",
        "close": "Свернуть форму",
        "text": "Готовы ли Вы, чтобы Ваши клиенты покупали криптовалюту с помощью кредитной/дебетовой карты? Пожалуйста, заполните форму и мы свяжемся с Вами как можно скорее",
        "button": "Отправить",
        "mail-success": "Спасибо. Мы ответим Вам"
      },
      "api": {
        "title": "API интеграция",
        "text": "Используя наш API, вы можете позволить своим клиентам мгновенно покупать криптовалюту с вашего сайта. Наша платформа для борьбы с мошенничеством, интегрированная на вашу веб-страницу или мобильное приложение, также может использоваться в качестве инструмента, который позволит вам отслеживать и фильтровать платежи."
      },
      "affiliate": {
        "title": "Партнерский маркетинг",
        "text": "Получайте вознаграждения, рекомендуя Indacoin другим пользователям. Вы будете зарабатывать до 3% от стоимости всех покупок ваших рефералов.",
        "block-1": {
          "title": "Как я могу присоединиться к реферальной программе?",
          "text": "Для того, чтобы стать нашим реферальным партнером, вы должны {{{link}}} у нас на сайте. Всю необходимую информацию вы сможете найти внутри вашего личного кабинета.",
          "link-text": "зарегистрироваться"
        },
        "block-2": {
          "title": "Как я могу выводить средства, собранные по партнерской программе?",
          "text": "Вы можете покупать BTC/ETH и другие криптовалюты на Indacoin без комиссии или выводить средства на свою банковскую карту."
        },
        "block-3": {
          "title": "В чем преимущество вашей партнерской программы?",
          "text": "На нашей платформе пользователи имеют возможность покупать более 100 различных коинов посредством кредитных и дебетовых карт и без регистрации. Учитывая тот факт, что покупки совершаются почти мгновенно, наши партнеры могут получать прибыль в моменте. "
        },
        "block-4": {
          "title": "Как выглядит реферальная ссылка?",
          "text": "Партнерская ссылка выглядит следующим образом: ({{{link}}})"
        }
      },
      "more": "Узнать больше",
      "close": "Скрыть"
    },
    "achievements": {
      "partnership-title": "Партнерство",
      "partnership-text": "Развитие партнерских отношений — одна из ключевых стратегий Indacoin. Наша партнерская программа создана для того, чтобы поддерживать криптовалютные проекты и помогать соединять мир традиционных, фиатных денег и только зарождающуюся область цифровых валют. Сотрудничая с Indacoin, вы можете предоставлять своим клиентам возможность приобретать криптовалюту максимально простым и быстрым способом — при помощи банковских карт и получать при этом стабильный доход.",
      "block-1": {
        "header": "Нам доверяют",
        "text": "Мы работаем на рынке криптовалюты с 2013 года и за это время уже наладили сотрудничество с лидерами индустрии. Более 500 000 клиентов из более чем 190 стран уже воспользовались нашим сервисом, чтобы купить биткоин, эфириум и другие альткоины."
      },
      "block-2": {
        "header": "Безопасность",
        "text": "Мы создали инновационную и высокоэффективную антифрод платформу, основанную на технологии, разработанной специально для обнаружения и предотвращения мошеннических действий для крипто валютных компаний."
      },
      "block-3": {
        "header": "Готовое решение",
        "text": "Наш продукт уже востребован компаниями, проводящими ICO, криптовалютными биржами и обменниками, блокчейн-платформами и крипто-фондами."
      },

      "header": "Зарабатывайте с нами на прорывной",
      "subheader": "технологии блокчейна",
      "text1": "{{{company}}} - это международная платформа, которая позволяет максимально быстро покупать Bitcoin, Ethereum, Ripple, Litecoin, Waves и более 100 других криптовалют без регистрации с помощью банковской карты.",
      "text2": "Сервис работает с 2013 года практически во всем мире, включая:",
      "text3": "Великобританию, Канаду, Германию, Францию, Россию, Украину, Испанию, Италию, Польшу, Турцию, Австралию, Филиппины, Индонезию, Индию, Беларусь, Бразилию, Египет, Саудовскую Аравию, ОАЭ, Нигерию и многие другие страны."
    },
    "clients": {
      "header": "Indacoin воспользовалось более 500 000 человек",
      "subheader": "за прошедшие 5 лет"
    },
    "benefits": {
      "header": "Преимущества для компаний:",
      "text1": "Уникальность нашего сервиса в том, что он предоставляет клиентам  возможность покупать и отправлять криптовалюту без регистрации. Пользователи, которые переходят на платформу Indacoin по реферальной ссылке, просто должны ввести данные своей карты и адрес кошелька, куда стоит отправлять коины. Таким образом, потенциальные клиенты могут немедленно совершать покупки, а компании получать прибыль.",
      "text2": "Наши партнеры получают до 3% от каждой транзакции. Более того, наша реферальная программа распространяется и на все будущие покупки пользователей.",
      "text3": "Для вывода средств используются биткоин и банковские карты."
    },
    "accounts": {
      "header": "Присоединяйтесь к нам прямо сейчас!",
      "emailPlaceholder": "Enter your email address",
      "captchaPlaceholder": "Captcha",
      "button": "Начать"
    }
  },
  "faq": {
    "title": "Вопросы и ответы",
    "header1": "Общие вопросы",
    "header2": "Криптовалюты",
    "header3": "Верификация",
    "header4": "Комиссии",
    "header5": "Покупка",
    "question1": {
      "title": "Что такое Индакойн?",
      "text": "Мы работаем в сфере криптовалют с 2013 года, головной офис находится в Лондоне, Великобритания. На нашем сайте вы можете приобрести более 100 криптовалют без регистрации, с оплатой по банковской карте."
    },
    "question2": {
      "title": "Как я могу с вами связаться?",
      "text": "Вы можете связаться с нашей службой поддержки в любое время по почте(support@indacoin.com), телефону (+44-207-048-2582) или через онлайн-чат на нашем сайте."
    },
    "question3": {
      "title": "У вас есть партнерская программа?",
      "text": "Да, всю необходимую информацию вы можете найти здесь: "
    },
    "question4": {
      "title": "Какие криптовалюты можно у вас купить?",
      "text": "На нашем сайте можно приобрести более 100 криптовалют включая Bitcoin, Ethereum и Ripple."
    },
    "question5": {
      "title": "Нужно ли создавать аккаунт перед покупкой?",
      "text-part1": "Нет, регистрация не обязательна. Вам достаточно ввести сумму платежа тут ",
      "text-part2": ", на которую вы хотите совершить покупку, ваш номер мобильного телефона, e-mail, данные вашей карты и адрес крипто кошелька. Затем вы будете перенаправлены на страницу заказа."
    },
    "question6": {
      "title": "Какие страны могут пользоваться вашими услугами?",
      "text": "В настоящее время мы работаем со всеми странами, за исключением США."
    },
    "question7": {
      "title": "Могу ли я тут продать свои биткойны?",
      "text": "На данный момент у нас доступна только покупка криптовалюты."
    },
    "question8": {
      "title": "Что такое криптовалюта?",
      "text": "Криптовалюта - это электронная валюта, для защиты которой используется криптография, что делает ее невозможной для фальсификации.  Она не контролируется никакими правительствами, а переводы защищены от вмешательств и воздействий государств."
    },
    "question9": {
      "title": "Где я могу хранить свою криптовалюту?",
      "text": "Вы можете создать отдельный кошелёк для каждой криптовалюты на своём персональном компьютере или вы можете использовать наш онлайн-кошелёк, для хранения более 100 различных видов криптовалют в одном месте."
    },
    "question10": {
      "title": "Что такое биткойн адрес?",
      "text": "Это адрес вашего биткоин-кошелька. Вы также можете ввести адрес кошелька человека, которому хотите отправить биткоины или создать кошелёк на сайте Индакоин."
    },
    "question11": {
      "title": "Как быстро происходят переводы криптовалют?",
      "text": "Время прохождения  транзакций  зависит от того, какую именно криптовалюту вы приобретаете. Транзакции с биткоинами занимают около 15-20 минут и около часа для остальных криптовалют."
    },
    "question12": {
      "title": "Как мне верифицировать свою карту?",
      //"text": "Чтобы завершить процесс верификации, вам необходимо ввести четырёхзначный код, который вы получите по звонку от  автоинформатора. На втором этапе может также потребоваться видеоверификация, где вам нужно будет записать видео, в котором вам необходимо показать своё лицо и загрузить скан или фото вашей идентификационной Карты/паспорта/водительских прав.  В зависимости от требуемой верификации этот шаг может не потребоваться или будет являться обязательным, так как мы должны убедиться, что именно вы являетесь держателем карты."
      "text": "Чтобы завершить процесс верификации, вам необходимо ввести четырёхзначный код, который вы получите по звонку от автоинформатора. На втором этапе может потребоваться загрузить скан или фото вашего паспорта/водительских прав и затем загрузить селфи вместе с документом и листом бумаги с рукописными сообщением, что вы подтверждаете покупку в Indacoin. В зависимости от требуемой верификации этот шаг может не потребоваться или будет являться обязательным, так как мы должны убедиться, что именно вы являетесь держателем карты."
    },
    "question13": {
      "title": "Что если я не могу записать видео?",
      "text": "Вы можете открыть ту же самую ссылку со страницей платежа на вашем мобильном телефоне и записать видео там.  В случае возникновения проблем вы также можете отправить требуемое видео во вложении на нашу почту support@indacoin.com. Убедитесь перед отправкой, что вы указали ваш номер заказа."
    },
    "question14": {
      "title": "Я сделал платеж, но он все еще верифицируется.",
      "text": "Если ваше ожидание длится более двух минут, значит платёж не был создан. Такая ситуация может возникнуть, если ваша карта без 3D защиты или возможно вам нужно сделать новый платёж. Убедитесь, что вы подтверждаете вашу покупку с помощью кода из вашего банка или вы также можете попробовать сделать платёж а другой валюте(EUR/USD). Если вы уверены, что ваша карта оснащена 3D защитой и изменение валюты не помогло, тогда вам нужно связаться с банком, так как возможно они блокируют ваши попытки совершения платежа."
    },
    "question15": {
      "title": "Нужно ли проходить верификацию для каждой покупки?",
      "text": "Достаточно верифицировать карту один раз, и все последующие транзакции с этой карты будут проходить автоматически. Но если вы решите воспользоваться другой картой, для неё будет необходима новая верификация."
    },
    "question16": {
      "title": "Мой банк отклонил покупку, что делать?",
      "text": "Вам нужно связаться с банком и уточнить у них причину отмены платежа, возможно, они снимут ограничения и вы сможете сделать новый платеж."
    },
    "question17": {
      "title": "Зачем мне нужно верифицировать карту или записывать видео?",
      "text": "Процесс верификации защищает вас в случае кражи карты, хакерских атак и действий мошенников,  также она требуется в случае, если кто-то  пытается сделать платёж с помощью чужой банковской карты"
    },
    "question18": {
      "title": "Могу ли я доверять вам свои данные карты?",
      "text": "Мы не разглашаем детали третьим лицам без вашего согласия. Также мы принимаем только карты с 3D защитой, и мы не когда не требуем такие данные как CVV код или полный номер карты. Вы также проводите свои платежи только через терминалы Visa/MasterCard. Мы  собираем информацию только для того, чтобы убедиться, что именно вы являетесь держателем карты."
    },
    "question19": {
      "title": "Какая у вас комиссия?",
      "text": "Наша комиссия варьируется в зависимости от различных факторов, именно поэтому на нашем сайте расположен калькулятор, который выдаст вам конечную сумму и получаемое количество криптовалюты с учётом комиссии."
    },
    "question20": {
      "title": "Сколько у меня спишется с карты при покупке?",
      "text": "Вы заплатите только сумму, указанную в калькуляторе, который расположен на нашем сайте."
    },
    "question21": {
      "title": "У вас есть скидки?",
      "text": "В некоторых случаях мы предоставляем скидки, для подробной информации обратитесь к службе поддержки."
    },
    "question22": {
      "title": "Как купить криптовалюту на Индакойн?",
      "text": "Перед покупкой убедитесь, что вы платите со своей карты Visa/Mastercard с подключенной опцией 3D secure, имеете при себе паспорт или водтельские права и доступ к вашему телефону. Затем выберите валюту для покупки с нашего сайта, введите сумму платежа и заполните требуемую информацию (e-mail, телефон и т.д.). После вам нужно будет ввести 3D secure код от банка, который вы получите по смс. Затем вам поступит звонок с 4ех значным кодом от нашей системы, который вы сможете ввести на экране для подтверждения покупки. Страница так же предложит вам записать видео верификацию, если это будет необходимо для вашего заказа, наши сотрудники свяжутся с вами по телефону."
    },
    "question23": {
      "title": "Как быстро я получу валюту после платежа?",
      "text": "После прохождения верификации, сразу высылается криптовалюта. Время транзакции зависит от загруженности блокчейна. В среднем наши клиенты получают криптовалюту в течении 30 минут после платежа."
    },
    "question24": {
      "title": "Какие у вас минимальные и максимальные лимиты по покупке?",
      "text": "Максимальная покупка при первой транзакции 500$,лимит будет увеличен спустя 4 дня до 1000$.В течение первой недели после покупки максимальный лимит может составлять 2000$.Спустя 14 дней лимит будет составлять 5000$. Спустя месяц ограничения будут сняты. Минимальная транзакция составляет 50$."
    },
    "question25": {
      "title": "Какие карты вы принимаете?",
      "text": "Мы принимаем только Visa/Mastercard с функцией 3D secure."
    },
    "question26": {
      "title": "Что такое 3D secure?",
      "text": "3D secure это система безопасности в рамках карт VISA и MasterCard. После того как вы введете данные вашей карты на нашем сайте, откроется новое окно, в котором вам будет необходимо ввести ваш одноразовый код безопасности от банка для подтверждения покупки."
    },
    "question27": {
      "title": "Можно ли оплатить другими способами?",
      "text": "В данный момент мы принимаем платежи только по банковским картам Visa/Mastercard."
    }
  },
  "api": {
    "introduction": {
      "header-1": "api",
      "header-2": "Типы интеграции",
      "text-1": "На этой странице вы можете найти методы, которые используются для обеспечения вашего сервиса функцией обмена криптовалюты Indacoin. Например, если вы хотите, чтобы ваши пользователи оплачивали свои покупки, используя банковские карты, наш API идеально подойдет для решения этой задачи.",
      "text-2": "Для получения более подробной информации о возможностях нашего API пишите на почту <span class=\"blue\">pr@indacoin.com</span>",
      "title-1": "Как стать партнером Indacoin:",
      "title-2": "Быстрая интеграция",
      "title-3": "Стандартная интеграция",
      "title-4": "Возможные статусы транзакций:",
      "title-5": "User limit",
      "title-6": "Создание транзакции",
      "title-7": "Get price",
      "title-8": "Transaction history",
      "title-9": "Transaction info",
      "title-10": "Callback section",
      "title-11": "Методы:",

      "text-3": "Выберите тип интеграции, который вам больше всего подходит.",
      "text-4": "Свяжитесь с нами, чтобы верифицировать аккаунт своей компании.",
      "text-5": "Получите ключи (partnername и secret), необходимые для интеграции.",
      "text-6": "Начинайте интеграцию у себя на сайте.",
      "text-7": "В обоих случаях пользователь должен посетить нашу страницу оплаты, чтобы сделать платеж, пройти верификацию и получить подтверждение покупки.",
      "text-8": "После того, как клиент осуществил платеж, вам будет приходить коллбэк о каждом изменении статуса транзакции.",
      //"text-9": "Мы рекомендуем сделать реквест <code>exgw_getUserlimits</code>, чтобы проверить лимит конкретного пользователя (лимит зависит от региона проживания клиента и истории его/ее покупок у нас на платформе). (Этот шаг не является обязательным)",
      "text-9": "Минимальный лимит на транзакцию составляет 30 USD / EUR, а максимальный - 6000 USD / EUR",
      "text-10": "Когда пользователь совершает платеж, вы должны получить ID-transaction для проведения этого платежа. Чтобы это сделать, отправьте реквест <code>exgw_createTransaction</code> с выбранной суммой, валютой и адресом кошелька.",
      "text-11": "Для редиректа клиента на страницу платежа вы должны использовать ID-transaction, чтобы создать URL.",
      "text-12": "Вы можете форвардить пользователей на URL адрес без создания транзакции с помощью API, <span class=\"blue\">cur_from</span> должен быть выбран из списка валют (USD, EURO, RUB) а <span class=\"blue\">cur_to</span> представляет собой <span class=\"blue\">short_name</span> коина.",
      "text-13": "Полный список криптовалют: <span class=\"blue\">https://indacoin.com/api/mobgetcurrenciesinfoi</span>",
      "text-14": "Общий вид запроса: <span class=\"blue\">https://indacoin.com/gw/payment_form?partner={YourPartnerName}&cur_from={CurrencyFrom}&cur_to={CurrencyTo}&amount={Amount}&address={WalletAddress}&user_id={UserID}</span>",
      "text-15": "Покупка считается успешной после достижения статуса Finished.",
      "text-16": "создание новой транзакции",
      "text-17": "Параметры в json:",
      "text-18": "the method is valid only for crypto processing",
      "text-19": "receive amount user will get",
      "text-20": "Параметры в GET:",
      "text-21": "позвращает информацию по последним транзакциям",
      "text-22": "Параметры в json:",
      "text-23": "все параметры необязательны",
      "text-24": "возвращает информацию по транзакции",
      "text-25": "Параметры в json:",
      "text-26": "Пример создания запроса на PHP и Node.js",
      "text-27": "Пример создания запроса для URL",
      "text-28": "Для создания запроса Вам нужно получить ID транзакции из предыдущего запроса.",
      "text-29": "получить лимиты пользователя (и создает, если они не существуют)",
      "text-30": "Параметры в json:",
      "text-31": "We send callbacks in json POST format. To verify callback we will send you headers <code>gw-sign</code>, <code>gw-nonce</code> where nonce will be random number and sign will be",
      "text-32": "Пример:",

      "NotFound": "транзакция не найдена",
      "Chargeback": "получен чарджбек",
      "Declined": "платеж отклонен, вероятно, по той причине, что карта не 3D secure",
      "Cancelled": "платеж не был осуществлен в течение нескольких часов после создания ордера",
      "Failed": "общий случай, когда платеж не удался",
      "Draft": "оплата еще не поступала или не было попыток оплаты",
      "Paid": "списание средств с карты пользователя",
      "Verification": "пользователь приступил к верификации",
      "FundsSent": "криптовалюта отправлена на адрес кошелька пользователя, но еще не получена",
      "Finished": "криптовалюта получена пользователем"
    },

    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Простая интеграция",
      "text1": "Самая простая интеграция заключается в использовании следующей ссылки, которая должна включать в себя такие параметры, как адрес кошелька, сумма, валюта (cardeur, cardusd or cardrub) и id скидки. Общий вид запроса: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "Посмотреть пример"
      },
      "functions-segment": {
        "header": "Плотная интеграция",
        "title": "Пожалуйста, отправляйте описание Вашей компании и информацию о том, почему Вы заинтересованы в сотрудничестве с нами, на почту pr@indacoin.com"
      },
      "partnership-segment": {
        "block1": {
          "title1": "Логика",
          "text1": "Проверьте лимит пользователя (создаться, если еще не существует)",
          "text2": "Создаете транзакцию",
          "text3": "Направьте клиента на URL-адрес Indacoin, где пользователь должен будет совершить оплату и пройти верификацию",
          "text4": "Вне зависимости от того, дойдет ли пользователь до конца, Вы должны показать ему URL для завершения верификации",
          "text5": "На ваш URL-адрес поступит callback о состоянии транзакции (успех или неудача)",
          "text6": "Чтобы убедиться, что у вас есть подпись, вам нужно отправить в headers ‘gw-partner’, ‘gw-nonce’, ‘gw-sign’. Заголовок ‘gw-sign’ формируется как HMAC sha256 от partnerName + \"_\" + nonce",
          "text7": "Подпись для URL-формы подобна API, как одноразовые транзакции по ID. Также в кодировке Double Base64",
          "text8": "Пример формы оплаты",
          "text9": "Минимальная сумма платежа на данный момент составляет 50 USD/EURO, максимум - 500 USD/EURO",
          "text10": "Возможные статусы транзакций:",
          "title2": "Методы:",
          "text11": "Создать транзакцию",
          "text12": "- создать новую транзакцию",
          "text13": "Параметры в json:",
          "text14": "Лимиты пользователя",
          "text15": "Параметры в json:",
          "text16": "Получить стоимость",
          "text17": "- возвращает лимит пользователя (и создает, если он не существует)",
          "text18": "Параметры в GET:",
          "text19": "Транзакция",
          "text20": "- возвращает информацию о последних транзакциях",
          "text21": "Параметры в json:",
          "text22": "Все параметры опциональны",
          "text23": "информация о транзакции",
          "text24": "- возвращает информацию о транзакции",
          "text25": "Параметры в json:",
          "text26": "Пример создания запроса на PHP",
          "text27": "Пример создания запроса для URL",
          "text28": "Чтобы создать запрос, вам нужно получить ID транзакции из предыдущего запроса",
          "text29": "Проверка лимита пользователя",
          "text30": "Так как у каждого пользователя есть лимит, который изменяется в процессе платежей, перед созданием транзакции желательно его определить через запрос exgw_getUserlimits. Лимит зависит от региона пользователя и истории его покупок на нашей платформе",
          "text31": "Создание транзакции на Indacoin",
          "text32": "Перед тем, как пользователь совершит оплату, вы должны получить ID транзакции для проведения платежа на Indacoin, для этого используется запрос exgw_createTransaction",
          "text33": "Отправка клиента на Indacoin",
          "text34": "Для отправки клиента на страницу оплаты, надо сформировать URL с помощью полученной ID транзакции",
          "text35": "Обработка запроса",
          "text36": "Далее клиент переходит на площадку Indacoin, где происходит процесс оплаты, верификации и подтверждение покупки",
          "text37": "Проверка статуса по Callback или API",
          "text38": "После совершения оплаты клиентом Вам будет отправлен callback на каждое изменение статуса транзакции"
        }
      },
      "secret-segment": {
        "title": "За дополнительной информацией обращайтесь к нашему менеджеру по почте {{{company}}}"
      },
      "statuses-segment": {
        "title": "Статус транзакции можно узнать через методы:"
      }
  },
  "limits-warning": {
    "message": "Сумма должна быть от {{{limitsMin}}} {{{limitsCur}}} до {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "Сумма должна быть от {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">На этом сайте используются cookie для предоставления услуг лучшего качества.</span> {{{link}}}",
      "accept-button": "Принять",
      "link": "Узнать больше"
    },
    "success-video": {
      "text": "Спасибо! Теперь мы начали процесс проверки, который может занять некоторое время. Когда проверка будет завершена, криптовалюта придет на Ваш кошелек"
    },
    "accept-agreement": {
      "text": "Просьба указать, что Вы прочитали и соглашаетесь с Пользовательским Соглашением и AML политикой",
      //"text": "Просьба указать, что Вы прочитали и согласны с {{{terms}}} и Политикой Конфиденциальности",
      "link": "Условиями Использования"
    },
    "price-changed": {
      "header": "Цена изменилась",
      "agree": "Принять",
      "cancel": "Отмена"
    },
    "neoAttention": "Из-за особенностей криптовалюты NEO для покупки доступны только целые числа",
    "login": {
      "floodDetect": "Обнаружен спам",
      "resetSuccess": "Запрос сброса пароля успешен. Пожалуйста, проверьте Вашу почту",
      "wrongEmail": "Данная почта не зарегистрирована",
      "unknownError": "Ошибка на сервере. Пожайлуйста, повторите запрос",
      "alreadyLoggedIn": "Вход уже выполнен",
      "badLogin": "Неверный логин или пароль",
      "badCaptcha": "Неверная каптча",
      "googleAuthenticator": "Пожалуйста, введите код Google Authenticator"
    },
    "registration": {
      "unknownError": "Неизвестная ошибка",
      "floodDetect": "Обнаружен спам",
      "success": "Регистрация прошла успешно. Пожалуйста, проверьте Вашу почту",
      "badLogin": "Неверный логин или пароль",
      "badCaptcha": "Неверная каптча",

      "empty": "Заполните все поля формы",
      "repeatedRequest": "Письмо для подтверждения регистрации было отправлено на указанный Вами email. Пожалуйста, проверьте Вашу почту. Возможно, письмо попало в папку со спамом",
      "badEmail": "Вы указали некорректную или временную почту",
      "blockedDomain": "Пожалуйста, используйте другой почтовый адрес для регистрации"
    }
  },
  "stop-sidebar": {
    "title": "Уважаемые клиенты!",
    "text": "Мы вынуждены уведомить вас, что в связи с недавним увеличением объема продаж криптовалюты мы сделаем паузу на нашем сайте и в мобильных приложениях. Приносим извинения за доставленные неудобства! Покупка будет доступна снова к",
    "time": "среде, 16:00 (UTC+0)"
  },
  "noResults": "Ничего не найдено",
  "locale": {
    "title": "Выберите Вашу страну"
  }
}