export default {
    'lang': 'tr',
    "language-select": {
        "language": {
          "de": "German",
          'ar': 'Arabic',
          'en': 'English',
          'es': 'Spanish',
          "fr": "French",
          "it": "Italian",
          "pt": "Portuguese",
          'ru': 'Russian',
          'tr': 'Turkish'
        }
    },
    "recordVideo": {
      "send": "Gönder",
      "stop": "Durdur",
      "record": "Kaydet"
    },
    "links": {
      "terms-of-use": {
        "text": "{{{terms}}} ve {{{policy}}} Kabul ediyorum",
        "link-1": "Kullanım Şartları",
        "link-2": "Gizlilik Politikası"
      }
    },
    "exchangeOrder": {
      "step17": {
          "status-1": "Ödemenizi bekliyorum",
          "status-2": "Alıp verme",
          "status-2-sub": "Sizin için en uygun fiyatı arıyoruz.",
          "status-3": "Cüzdanınıza gönderiliyor.",
          "transaction-completed": "Tebrikler, işleminiz tamamlandı!",
          "rate": "Döviz kuru 1 BTC = {{{rate}}} {{{shortName}}}",
          "sentText": "Gönderdin:",
          "toAddress": "{{{cryptoAdress}}} için",
          "receiveText": "Alacaksınız:"
        },
      "changeCrypto": {
          "send": "Aşağıdaki adrese {{{amount}}} BTC gönderin.",
          "confirmation": "İki Bitcoin onayı {{{amount}}} {{{shortname}}} adresinize gönderilecektir sonra {{{wallet}}} ...",
          "copy": "Adresi kopyala",
          "copied": "Panoya kopyalandı"
        },
        "order": "Sipariş numarası",
        "status": {
            "title": "Statü",
            "completed": "Tamamlandı."
        },
        "payAmount": {
            "title": "Miktarı öde"
        },
        "check": "blockexplorer.net'te işlemi kontrol et",
        "resend-phone-code": "Bir çağrı almadın mı?",
        "sms-code": "Lütfen telefonu cevaplayın ve buraya 4 haneli kodu girin",
        "registration": "Kayıt sayfasına git",
  
        "internalAccount-1": "Lütfen Indacoin cüzdanınızı",
        "internalAccount-link": "giriş yaparak",
        "internalAccount-2": "kontrol edin",
        "your-phone": "telefonunuz {{{phone}}}",
        "many-requests": "Afedersiniz. Telefon değişikliği için birçok istek"
    },
    "app": {
        "detect_lang": {
            "title": "Dili algıla",
            "sub": "Lütfen bekleyiniz..."
        }
    },
    "meta": {
        "description-btc": "(BTC/{{{shortName}}}) {{{longName}}} {{{country}}} tarihinde en iyi fiyattan, anında ve 700 farklı kripto para alıp paylaşın",
        "title": "Kripto Parayı kredi ya da banka kartı ile alın. - Indacoin",
        "description": "[cryptocurrency], Ethereum, Litecoin, Ripple ve diğer 700 kripto para dahil olmak üzere bütün kripto paraları EURO ya da USD için hemen bozdurun ve satın alın.",
        "description-bitcoin":"Herhangi bir kripto parayı anında satın alın veya değiştirin: EURO veya USD için Bitcoin, Ethereum, Litecoin, Ripple ve diğer 700 kripto para birimi",
        "description-ethereum":"Herhangi bir kripto parayı anında satın alın veya değiştirin: EURO veya USD için Ethereum, Bitcoin, Litecoin, Ripple ve diğer 700 kripto para birimi",
        "description-litecoin":"Herhangi bir kripto parayı anında satın alın veya değiştirin: EURO veya USD için Litecoin, Bitcoin, Ethereum, Ripple ve diğer 700 kripto para birimi",
        "description-ripple":"Herhangi bir kripto parayı anında satın alın veya değiştirin: EURO veya USD için Ripple, Bitcoin, Ethereum, Litecoin ve diğer 700 kripto para birimi",
  
        "description2": "Herhangi bir kripto para birimini anında satın alın veya değiştirin: Bitcoin, Ethereum, Litecoin, Ripple ve EUR veya USD için 700 diğer dijital para birimi",
        "description3": "Herhangi bir kripto parayı anında satın alın veya değiştirin: Visa ve Mastercard ile Bitcoin, Ethereum, Litecoin, Ripple ve 700 diğer dijital para birimi",
        "buy": "Satın al",
        "subTitle": "Kredi veya Banka Kartı ile",
        "more": "Dahil olmak üzere 100'den fazla ülkede",
        "header": "<span class='sub'>{{{shortName}}}’ye</span> <span class='sub'>BTC’ye</span> Anında haberleşme"
    },
    "fullpage": {
        "header": "ANINDA KRİPTO PARA SATIN ALIN!",
        "header-alt": "Anında Kripto para değişimi!",
        "sub": "Indacoin, BitCoin ve diğer 30 farklı kripto parayı Visa & Mastercard yoluyla almanın en basit yolu!",
        "sub-alt": "Indacoin, 700 farklı kripto paranın herhangi birine bitcoin değişimi için kolaylık sağlar",
        "observe": "Bütün para birimlerini incele"
    },
    "navbar": {
        "main": "Ana sayfa",
        "btc": "Bitcoin Satın Al",
        "eth": "Kripto Para Satın Al",
        "affiliate": "Ortaklık",
        "news": "Haberler",
        "faq": "SSS",
        "login": "Giriş",
        "registration": "Kayıt",
        "platform": "Ürünler",
  
        "logout": "Çıkış Yap",
        "buy-instantly": "Anında kripto para satın al",
        "trading-platform": "Ticaret platformu",
        "mobile-wallet": "Mobil cüzdan",
        "payment-processing": "Ödeme işleme API'sı"
    },
    "currency-table": {
        "rank": "Numara",
        "coin": "Döviz",
        "market": "Piyasa Değeri",
        "price": "Fiyat",
        "volume": "Tutar",
        "change": "Çevir",
        "available-supply": "Mevcut kaynağı",
        "search-cryptocurrency": "Kripto para için arama yap",
        "change-1h": "1 saat Değişiklik",
        "change-24h": "1 gün Değişiklik",
        "change-7d": "1 hafta Değişiklik"
    },
    "buy-blocks": {
        "give": "Öde",
        "get": "Al"
    },
    "exchange-form": {
        "title": "Kripto parayı hemen satın alın",
        "submit": "SATIN AL",
        "give": "Öde",
        "get": "Al",
        "search": "Arama",
        "error-page": "Özür dileriz, satın alma şu anda kullanılamıyor"
    },
    "pagination": {
        "page": "Sayfa",
        "next": "Sonraki",
        "prev": "Önceki",
        "first": "İlk"
    },
    "sidebar": {
        "accept": {
            "title": "Debit (Banka) kartları geçerlidir",
            "button": "Kripto al"
        },
        "youtube": {
            "title": "Hakkımızda",
            "play": "Oyna"
        },
        "orders": {
            "title": "Sipariş yönetimi",
            "order": "Sipariş #",
            "confirm": "Kodu onayla",
            "check": "Siparişinizi kontrol ediniz",
            "new": "Yeni siparişi oluşturunuz",
            "or": "ya da"
        },
        "social": {
            "title": "Bizi takip edin."
        }
    },
    "wiki": {
        "etymology": "Etimoloji"
    },
    "loading": {
        "title": "Yükleniyor",
        "sub": "Lütfen, bekleyiniz",
        "text": "Yükleniyor"
    },
    "error": {
        "title": "Yanlış",
        "sub": "Bir şeyler ters gitti, panik yapmayınız.",
        "text": "Bilinmeyen hata ortaya çıktı, onu tespit etmeye çalısıyoruz. Lütfen daha sonra tekrar deneyiniz.",
        "home": "Ana Sayfa",
        "mainPageError1": "Maalesef, mevcut kripto para birimleri hakkında herhangi bir bilgi yok",
        "mainPageError2": "Siteyi biraz sonra ziyaret etmeyi deneyin",
        "page-404": "Özür dileriz, sayfa bulunamadı",
        "page-error": "Özür dileriz, hata oluştu"
    },
    "features": {
        "title": "<span class=\"blue\">Indacoin</span>'de<br><span class=\"offset\">Neler yapabilirsiniz?</span>",
        "column1": {
            "title": "Kripto Para Satın alın",
            "text": "Indacoin'de 700 farklı kripto parayı Kredi/Debit(Banka) kartı ile anında satın almak mümkündür."
        },
        "column2": {
            "title": "Kripto Para ile ilgili daha fazla bilgi al",
            "text": "Bizim basit kılavuzlarımız, herhangi bir altcoin'in temel prensiplerini daha iyi anlamanıza yardımcı olacak."
        },
        "column3": {
            "title": "Yatırım kararlarını al",
            "text": "Güçlü analitik araçlar en iyi giriş ve çıkış anlarını yakalamanızı sağlayacak."
        },
        "column4": {
            "title": "Kayıt olmadan Bitcoin Satın Al",
            "text": "Ödeme tamamlanmıştır. Bitcoin belirtilen adrese gönderilecektir."
        },
        "column5": {
            "title": "Fikirlerinizi paylaşınız",
            "text": "Diğer önde gelen yatırımcılarla, Kripto para alanındaki son gelişmelerle ilgili sohbetlere katılın"
        },
        "column6": {
            "title": "Bütün Kripto Paraları aynı yerde sakla",
            "text": "Indacoin Cüzdanında 700'den fazla dijital para birimini yönetebilir ve saklayabilirsiniz."
        }
    },
    "mockup": {
        "title": "MOBİL CÜZDAN",
        "text": "Bizim akıllı ve basit Indacoin cüzdanımız, Android veya Iphone'nunuzda web tarayıcınıza ek olarak çalışmaktadır. Artık arkadaşlarınıza kripto para göndermek, mesaj göndermek kadar kolaylaştı."
    },
    "footer": {
        "column1": {
            "title": "Satın Al",
            "link1": "Bitcoin Satın Al",
            "link2": "Ethereum Satın Al",
            "link3": "Ripple Satın Al",
            "link4": "Bitcoin Cash Satın Al",
            "link5": "Litecoin Satın Al",
            "link6": "Dash Satın Al"
        },
        "column2": {
            "title": "Bilgiler için tıkla",
            "link1": "Hakkımızda",
            "link2": "SSS",
            "link3": "Kullanma Şartları",
            "link4": "Klavuz"
        },
        "column3": {
            "title": "Araçlar",
            "link1": "UPA",
            "link2": "Bölge değiştir",
            "link3": "Telefon Uygulaması"
        },
        "column4": {
            "title": "İletişim"
        }
    },
    "country": {
        "AF": {
            "name": "Afganistan"
        },
        "AX": {
            "name": "Åland Adaları"
        },
        "AL": {
            "name": "Arnavutluk"
        },
        "DZ": {
            "name": "Cezayir"
        },
        "AS": {
            "name": "Amerikan Samoası"
        },
        "AD": {
            "name": "Andorra"
        },
        "AO": {
            "name": "Angola"
        },
        "AI": {
            "name": "Anguilla"
        },
        "AQ": {
            "name": "Antarktika"
        },
        "AG": {
            "name": "Antigua ve Barbuda"
        },
        "AR": {
            "name": "Arjantin"
        },
        "AM": {
            "name": "Ermenistan"
        },
        "AW": {
            "name": "Aruba"
        },
        "AU": {
            "name": "Avustralya"
        },
        "AT": {
            "name": "Avustruya"
        },
        "AZ": {
            "name": "Azerbeycan"
        },
        "BS": {
            "name": "Bahamalar"
        },
        "BH": {
            "name": "Bahreyn"
        },
        "BD": {
            "name": "Bangladeş"
        },
        "BB": {
            "name": "Barbados"
        },
        "BY": {
            "name": "Beyaz Rusya"
        },
        "BE": {
            "name": "Belçika"
        },
        "BZ": {
            "name": "Belize"
        },
        "BJ": {
            "name": "Benin"
        },
        "BM": {
            "name": "Bermuda"
        },
        "BT": {
            "name": "Butan"
        },
        "BO": {
            "name": "Bolivya"
        },
        "BA": {
            "name": "Bosna Hersek"
        },
        "BW": {
            "name": "Botsvana"
        },
        "BV": {
            "name": "Bouvet Adası"
        },
        "BR": {
            "name": "Brezilya"
        },
        "IO": {
            "name": "İngiliz Hint Okyanusu Bölgesi"
        },
        "BN": {
            "name": "Brunei Darussalam"
        },
        "BG": {
            "name": "Bulgaristan"
        },
        "BF": {
            "name": "Burkina Faso"
        },
        "BI": {
            "name": "Burundi"
        },
        "KH": {
            "name": "Kamboçya"
        },
        "CM": {
            "name": "Kamerun"
        },
        "CA": {
            "name": "Kanada"
        },
        "CV": {
            "name": "Yeşil Burun Adaları"
        },
        "KY": {
            "name": "Cayman Adaları"
        },
        "CF": {
            "name": "Orta Afrika Cumhuriyeti"
        },
        "TD": {
            "name": "Çad"
        },
        "CL": {
            "name": "Şili"
        },
        "CN": {
            "name": "Çin"
        },
        "CX": {
            "name": "Noel Adası"
        },
        "CC": {
            "name": "Cocos (Keeling) Adaları"
        },
        "CO": {
            "name": "Kolombiya"
        },
        "KM": {
            "name": "Komorlar"
        },
        "CG": {
            "name": "Kongo"
        },
        "CD": {
            "name": "Demokratik Kongo Cumhuriyeti"
        },
        "CK": {
            "name": "Cook Adaları"
        },
        "CR": {
            "name": "Kosta Rika"
        },
        "CI": {
            "name": "Fildişi Sahili Cumhuriyeti"
        },
        "HR": {
            "name": "Hırvatistan"
        },
        "CU": {
            "name": "Küba"
        },
        "CY": {
            "name": "Cyprus"
        },
        "CZ": {
            "name": "Çek Cumhuriyeti"
        },
        "DK": {
            "name": "Danimarka"
        },
        "DJ": {
            "name": "Cibuti"
        },
        "DM": {
            "name": "Dominika"
        },
        "DO": {
            "name": "Dominik Cumhuriyeti"
        },
        "EC": {
            "name": "Ekvador"
        },
        "EG": {
            "name": "Mısır"
        },
        "SV": {
            "name": "El Salvador"
        },
        "GQ": {
            "name": "Ekvator Ginesi"
        },
        "ER": {
            "name": "Eritre"
        },
        "EE": {
            "name": "Estonya"
        },
        "ET": {
            "name": "Etiyopya"
        },
        "FK": {
            "name": "Falkland Adaları (Malvinas)"
        },
        "FO": {
            "name": "Faroe Adaları"
        },
        "FJ": {
            "name": "Fiji"
        },
        "FI": {
            "name": "Finlanda"
        },
        "FR": {
            "name": "Fransa"
        },
        "GF": {
            "name": "Fransız Guyanası"
        },
        "PF": {
            "name": "Fransız Polinezyası"
        },
        "TF": {
            "name": "Fransız Güney ve Antarktika Toprakları"
        },
        "GA": {
            "name": "Gabon"
        },
        "GM": {
            "name": "Gambiya"
        },
        "GE": {
            "name": "Gürcistan"
        },
        "DE": {
            "name": "Almanya"
        },
        "GH": {
            "name": "Gana"
        },
        "GI": {
            "name": "Cebelitarık"
        },
        "GR": {
            "name": "Yunanistan"
        },
        "GL": {
            "name": "Grönland"
        },
        "GD": {
            "name": "Grenada"
        },
        "GP": {
            "name": "Guadeloupe"
        },
        "GU": {
            "name": "Guam"
        },
        "GT": {
            "name": "Guatemala"
        },
        "GG": {
            "name": "Guernsey"
        },
        "GN": {
            "name": "Gine"
        },
        "GW": {
            "name": "Gine-Bissau"
        },
        "GY": {
            "name": "Guyana"
        },
        "HT": {
            "name": "Haiti"
        },
        "HM": {
            "name": "Heard Adası ve McDonald Adaları"
        },
        "VA": {
            "name": "Santa Sede (Papalık Makamı)"
        },
        "HN": {
            "name": "Honduras"
        },
        "HK": {
            "name": "Hong Kong"
        },
        "HU": {
            "name": "Macaristan"
        },
        "IS": {
            "name": "İzlanda"
        },
        "IN": {
            "name": "Hindistan"
        },
        "ID": {
            "name": "Endonezya"
        },
        "IR": {
            "name": "İran, İslam Cumhuriyeti"
        },
        "IQ": {
            "name": "Irak"
        },
        "IE": {
            "name": "İrlanda"
        },
        "IM": {
            "name": "Man Adası"
        },
        "IL": {
            "name": "İsrail"
        },
        "IT": {
            "name": "İtalya"
        },
        "JM": {
            "name": "Jamaika"
        },
        "JP": {
            "name": "Japonya"
        },
        "JE": {
            "name": "Jersey"
        },
        "JO": {
            "name": "Ürdün"
        },
        "KZ": {
            "name": "Kazakistan"
        },
        "KE": {
            "name": "Kenya"
        },
        "KI": {
            "name": "Kiribati"
        },
        "KP": {
            "name": "Kore Demokratik Halk Cumhuriyeti"
        },
        "KR": {
            "name": "Kore Cumhuriyeti"
        },
        "KW": {
            "name": "Kuveyt"
        },
        "KG": {
            "name": "Kırgızistan"
        },
        "LA": {
            "name": "Laos Demokratik Halk Cumhuriyeti"
        },
        "LV": {
            "name": "Letonya"
        },
        "LB": {
            "name": "Lübnan"
        },
        "LS": {
            "name": "Lesotho Krallığı"
        },
        "LR": {
            "name": "Liberya"
        },
        "LY": {
            "name": "Libya Cumhuriyeti"
        },
        "LI": {
            "name": "Lihtenştayn"
        },
        "LT": {
            "name": "Litvanya"
        },
        "LU": {
            "name": "Lüksemburg"
        },
        "MO": {
            "name": "Macao"
        },
        "MK": {
            "name": "Makedonya Cumhuriyeti"
        },
        "MG": {
            "name": "Madagaskar"
        },
        "MW": {
            "name": "Malawi"
        },
        "MY": {
            "name": "Malezya"
        },
        "MV": {
            "name": "Maldivler"
        },
        "ML": {
            "name": "Mali"
        },
        "MT": {
            "name": "Malta"
        },
        "MH": {
            "name": "Marşal Adaları"
        },
        "MQ": {
            "name": "Martinik"
        },
        "MR": {
            "name": "Moritanya"
        },
        "MU": {
            "name": "Mauritius"
        },
        "YT": {
            "name": "Mayotte"
        },
        "MX": {
            "name": "Meksika"
        },
        "FM": {
            "name": "Mikronezya Federal Devletleri"
        },
        "MD": {
            "name": "Moldova Cumhuriyeti"
        },
        "MC": {
            "name": "Monako"
        },
        "MN": {
            "name": "Moğolistan"
        },
        "MS": {
            "name": "Montserrat"
        },
        "MA": {
            "name": "Fas"
        },
        "MZ": {
            "name": "Mozambik"
        },
        "MM": {
            "name": "Myanmar"
        },
        "NA": {
            "name": "Namibia"
        },
        "NR": {
            "name": "Nauru"
        },
        "NP": {
            "name": "Nepal"
        },
        "NL": {
            "name": "Hollanda"
        },
        "AN": {
            "name": "Hollanda Antilleri"
        },
        "NC": {
            "name": "Yeni Kaledonya"
        },
        "NZ": {
            "name": "Yeni Zelanda"
        },
        "NI": {
            "name": "Nikaragua"
        },
        "NE": {
            "name": "Nijer"
        },
        "NG": {
            "name": "Nijerya"
        },
        "NU": {
            "name": "Niue"
        },
        "NF": {
            "name": "Norfolk Adası"
        },
        "MP": {
            "name": "Kuzey Mariana Adaları"
        },
        "NO": {
            "name": "Norveç"
        },
        "OM": {
            "name": "Umman"
        },
        "PK": {
            "name": "Pakistan"
        },
        "PW": {
            "name": "Palau"
        },
        "PS": {
            "name": "Işgal altındaki Filistin Bölgesi"
        },
        "PA": {
            "name": "Panama"
        },
        "PG": {
            "name": "Papua Yeni Gine"
        },
        "PY": {
            "name": "Paraguay"
        },
        "PE": {
            "name": "Peru"
        },
        "PH": {
            "name": "Filipinler"
        },
        "PN": {
            "name": "Pitcairn"
        },
        "PL": {
            "name": "Polonya"
        },
        "PT": {
            "name": "Portekiz"
        },
        "PR": {
            "name": "Porto Riko"
        },
        "QA": {
            "name": "Katar"
        },
        "RE": {
            "name": "Reunion"
        },
        "RO": {
            "name": "Romanya"
        },
        "RU": {
            "name": "Rusya Federasyonu"
        },
        "RW": {
            "name": "RUANDA"
        },
        "SH": {
            "name": "Saint Helena"
        },
        "KN": {
            "name": "Saint Kitts ve Nevis Federasyonu"
        },
        "LC": {
            "name": "Saint Lucia"
        },
        "PM": {
            "name": "Saint Pierre ve Miquelon"
        },
        "VC": {
            "name": "Saint Vincent ve Grenadinler"
        },
        "WS": {
            "name": "Samoa"
        },
        "SM": {
            "name": "San Marino"
        },
        "ST": {
            "name": "São Tomé ve Príncipe Demokratik Cumhuriyeti"
        },
        "SA": {
            "name": "Suudi Arabistan"
        },
        "SN": {
            "name": "Senegal"
        },
        "CS": {
            "name": "Sırbistan ve Karadağ"
        },
        "SC": {
            "name": "Seyşeller"
        },
        "SL": {
            "name": "Sierra Leone"
        },
        "SG": {
            "name": "Singapur"
        },
        "SK": {
            "name": "Slovakya"
        },
        "SI": {
            "name": "Slovenya"
        },
        "SB": {
            "name": "Solomon Adaları"
        },
        "SO": {
            "name": "Somali"
        },
        "ZA": {
            "name": "Güney Afrika"
        },
        "GS": {
            "name": "Güney Georgia ve Güney Sandwich Adaları"
        },
        "ES": {
            "name": "İspanya"
        },
        "LK": {
            "name": "Sri Lanka"
        },
        "SD": {
            "name": "Sudan"
        },
        "SR": {
            "name": "Surinam"
        },
        "SJ": {
            "name": "Svalbard ve Jan Mayen"
        },
        "SZ": {
            "name": "Svaziland"
        },
        "SE": {
            "name": "İsveç"
        },
        "CH": {
            "name": "İsviçre"
        },
        "SY": {
            "name": "Suriye Arap Cumhuriyeti"
        },
        "TW": {
            "name": "Tayvan ili"
        },
        "TJ": {
            "name": "Tacikistan"
        },
        "TZ": {
            "name": "Tanzanya Birleşik Cumhuriyeti"
        },
        "TH": {
            "name": "Tayland"
        },
        "TL": {
            "name": "Doğu Timor"
        },
        "TG": {
            "name": "Togo Cumhuriyeti"
        },
        "TK": {
            "name": "Tokelau"
        },
        "TO": {
            "name": "Tonga Krallığı"
        },
        "TT": {
            "name": "Trinidad ve Tobago"
        },
        "TN": {
            "name": "Tunus"
        },
        "TR": {
            "name": "Türkiye"
        },
        "TM": {
            "name": "Türkmenistan"
        },
        "TC": {
            "name": "Turks ve Caicos Adaları"
        },
        "TV": {
            "name": "Tuvalu"
        },
        "UG": {
            "name": "Uganda"
        },
        "UA": {
            "name": "Ukrayna"
        },
        "AE": {
            "name": "Birleşik Arap Emirlikleri"
        },
        "GB": {
            "name": "Birleşik Krallık"
        },
        "US": {
            "name": "Amerika Birleşik Devletleri"
        },
        "UM": {
            "name": "Amerika Birleşik Devletleri'nin küçük dış adaları"
        },
        "UY": {
            "name": "Uruguay"
        },
        "UZ": {
            "name": "Özbekistan"
        },
        "VU": {
            "name": "Vanuatu"
        },
        "VE": {
            "name": "Venezuela"
        },
        "VN": {
            "name": "Vietnam"
        },
        "VG": {
            "name": "Britanya Virjin Adaları"
        },
        "VI": {
            "name": "Amerika Birleşik Devletleri Virjin Adaları"
        },
        "WF": {
            "name": "Wallis ve Futuna Adaları"
        },
        "EH": {
            "name": "Batı Sahra"
        },
        "YE": {
            "name": "Yemen"
        },
        "ZM": {
            "name": "Zambiya"
        },
        "ZW": {
            "name": "Zimbabve"
        }
    },
    "currency": {
        "RUB": "RUB",
        "BTC": "BTC",
        "USD": "USD"
    },
    "buy": "Satın Al",
    "sell": "Sat",
    "weWillCallYou": "Banka kartınızı Indacoin'de ilk defa kullanıyorsanız lütfen online kart ekstrenizdeki 3 sayıdan ibaret kodu giriniz. Kart ekstrenize online ulaşamıyorsanız bankanızın müşteri hizmetlerini arayınız.",
    "exchangerStatuses": {
        "TimeOut": "Sipariş zaman aşımına uğradı",
        "Error": "Bir şeyler ters gitti. Lütfen Indacoin müşteri hizmetleri ile iletişime geçiniz. \n(support@indacoin.com).",
        "WaitingForAccountCreation": "Indacoin hesabının oluşturulması bekleniyor. ( E-postanıza onay mektubu gönderildi)",
        "CashinWaiting": "Şu anda ödemeniz işleniyor",
        "BillWaiting": "Hesabın ödenmesini bekleniyor.",
        "Processing": "İşlem devam ediyor...",
        "MoneySend": "Para gönderildi.",
        "Completed": "Tamamlandı.",
        "Verifying": "Doğrulanıyor...",
        "Declined": "Reddedildi.",
        "cardDeclinedNoFull3ds": "Bankanız bu kart için 3D Güvenlik opsiyonunu tamamen (ancak deneme versiyonu geçerlidir) desteklemediği için ödemeniz reddedilmiştir. Lütfen, bankanızın müşteri hizmetlerini arayınız.",
        "cardDeclined": "Banka kartınız reddedilmiştir. Sorularınız varsa lütfen Indacoin destek hizmeti ile iletişime geçiniz. \n(support support@indacoin.com).",
        "Non3DS": "Maalesef ödemeniz reddedildi. Bankanızı arayıp banka kartınızın 3D Güvenlik destekli banka kartı (Verified by Visa or Mastercard Securecode) olup olmadığını kontrol ediniz. Bu, internetteki ödemeleri her yaptığınızda banka kartınıza bağlanmış olan gizli kodu/ pasaportu girmeniz gerektiği anlamına geliyor. Bazen gizli kodun cep numaranıza gönderilebilir."
    },
    "exchanger_fields_ok": "Bitcon almak için tıkla!",
    "exchanger_fields_error": "Lütfen, bütün pencereleri kontrol ediniz!",
    "bankRejected": "İşlem bankanız tarafından reddedilmiştir. Lütfen, bankanızın müşteri hizmetlerini arayıp bu banka kartından yeni ödeme yapınız.",
    "wrongAuthCode": "Kart ekstresindeki kod yanlıştır! Lütfen, online banka servisinizi kontrol ediniz ya da geçerli bir kod almak için bankanızı arayınız.",
    "wrongSMSAuthCode": "Telefon aramasındaki kodu yanlış girdiniz.\n",
    "getCouponInfo": {
        "indo": "Kuponunuz geçerli değildir.",
        "exetuted": "Bu kupon artık daha önce kullanılmıştır.",
        "freeRate": "OFF Kuponu aktif hale getirilmiştir."
    },
    "ip": "IP'niz geçici olarak bloke edilmiştir.",
    "priceChanged": "Fiyat değişti. Kabul ediyor musunuz?",
    "exchange_go": "- ÇEVİR",
    "exchange_buy_btc": "- ANINDA BITCOIN SATIN AL",
    "month": "Oc Şub Mar Ni May Haz Tem Ağ Ey Ek Kas Ara",
    "discount": "İndirim",
    "cash": {
        "equivalent": "...    eşittir.",
        "card3DS": "Sadece 3D Güvenlik banka kartlarını (Verified by Visa or MasterCard SecureCode) kabul ediyoruz.",
        "cashIn": {
            "1": "Kredi/Banka Kartları",
            "2": "Rus ödeme terminalleri",
            "3": "QIWI",
            "4": "Bashkomsnabbank",
            "5": "Novoplat terminalleri",
            "6": "Elexnet terminalleri",
            "7": "Kredi/Debit kartları (USD)",
            "8": "Alfa-click",
            "9": "Elektronik transfer",
            "10": "Bitcoin",
            "12": "Litecoin",
            "13": "Astropay",
            "15": "QIWI",
            "16": "USD",
            "17": "Pinpay",
            "18": "Payeer",
            "19": "Liqpay",
            "20": "Yandex.Money",
            "21": "Elexnet",
            "22": "Kassira.net",
            "23": "Mobile element",
            "24": "Svyaznoy",
            "25": "Euroset",
            "26": "PerfectMoney",
            "27": "IndacoinInternal",
            "28": "CouponBonus",
            "29": "Yandex.Money",
            "30": "OKPay",
            "31": "Referral",
            "33": "Payza",
            "35": "BTC-E kodu",
            "36": "Kredi/Debit kartları (USD)",
            "37": "Kredi/Debit kartları (USD)",
            "39": "Elektronik transfer",
            "40": "Yandex.Money",
            "42": "QIWI(El işi)",
            "43": "UnionPay",
            "44": "QIWI(Otomatik)",
            "45": "Ödemeyi cep telefonundan yap (sadece Rusya'da geçerli)",
            "49": "LibrexCoin",
            "50": "Kredi/Debit kartları (EUR)",
            "51": "QIWI (Otomatik)",
            "53": "Ethereum"
        },
        "tip": {
            "country": "Ülke geçerli değildir.",
            "alfaclick": "Kullanıcı adı geçerli değildir.",
            "pan": "Kart numarası geçerli değildir.",
            "cardholdername": "Kart sahibi adı geçerli değildir.",
            "fio": "Örnek: Ian John Marr",
            "bday": "Doğum tarihi geçerli değildir.",
            "city": "Şehir geçerli değildir.",
            "address": "Adres geçerli değildir.",
            "ypurseid": "Cüzdan kimliği geçerli değildir.",
            "wpurseid": "Cüzdan kimliği geçerli değildir.",
            "pmpurseid": "Cüzdan kimliği geçerli değildir.",
            "purse": "Cüzdan kimliği geçerli değildir.",
            "phone": "Telefon numarası geçerli değildir.",
            "btcaddress": "Adres geçerli değildir.",
            "ltcaddress": "Adres geçerli değildir.",
            "lxcaddress": "Adres geçerli değildir.",
            "ethaddress": "Adres geçerli değildir.",
            "properties": "Bilgiler geçerli değildir.",
            "payeer": "Cüzdan kimliği geçerli değildir.",
            "okpayid": "Cüzdan kimliği geçerli değildir.",
            "email": "E-posta adresi geçerli değildir.",
            "card_number": "Bu kart türü desteklenmiyor. Numarayı kontrol ediniz.",
            "inc_card_number": "Kart numarası geçerli değildir.",
            "inn": "Numara geçerli değildir.",
            "poms": "Numara geçerli değildir.",
            "snils": "Numara geçerli değildir.",
            "cc_expr": "Tarih geçerli değildir.",
            "cc_name": "Kart sahibi geçerli değildir.",
            "cc_cvv": "CVV geçerli değildir."
        }
    },
    "change": {
        "wallet": "Yeni cüzdan oluştur / Zaten bir Indacoin hesabım var",
        "accept": "Anlaşmanın şartlarını kabul ediyorum",
        "coupone": "Kuponum var",
        "watchVideo": "Videoyu izle",
        "seeInstructions": "Kılavuzları izle",
        "step1": {
            "title": "Sipariş oluşturuluyor.",
            "header": "Ödeme bilgilerini giriniz.",
            "subheader": "İhtiyaçlarınızı bize bildiriniz."
        },
        "step2": {
            "title": "Ödeme bilgileri",
            "header": "Kart bilgilerinizi giriniz.",
            "subheader": "Sadece Visa & Mastercard'ı kabul ederiz.",
            "card": {
                "header": "CVV'yi nerede bulabilirim?",
                "text": "CVV'yi kartınızın arkasında bulabilirsiniz. Genelde 3-4 sayıdan ibarettir."
            }
        },
        "step3": {
            "title": "İletişim bilgileri",
            "header": "İletişim bilgilerinizi giriniz.",
            "subheader": "Müşterilerimizi daha yakından tanıyalım"
        },
        "step4": {
            "title": "Doğrulanıyor...",
            "header": "Ödeme sayfasındaki kodunuzu giriniz.",
            "subheader": "Ödeme bilgileriniz"
        },
        "step5": {
            "title": "Sipariş bilgileri"
        },
        "step6": {
            "title": "Videoyu kaydet.",
            "header": "Pasaportunuzu ve yüzünüzü gösteriniz.",
            "description": "Bir aldatmaca olmadığından emin olalım.",
            "startRecord": "Aşağıdaki kaydet düğmesine basınız.",
            "stopRecord": "İşlemi bitirdikten sonra \"stop\" düğmesine basınız."
        },
        "nextButton": "Sonraki",
        "previousButton": "Önceki",
        "error": "Sunucu hatası bağlantısı. Lütfen verileri tekrar gönderin",
        "passVideoVerification": "Videoyla doğrulamayı geçirin",
        "passKYCVerification": "KYC doğrulaması",
        "soccer-legends": "Kişisel bilgilerimin Soccer Legends Limited şirketine aktarıldığını ve işlendiğini kabul ediyorum.",
        "withdrawalAmount": "Hesap için yatırılacak tutar",
        "inc": "100'den fazla ülkede hizmet veriyoruz! "
        },
        "form": {
          "tag": "{{{currency}}} etiketi",
          "loading": "Yükleniyor lütfen bekleyin...",
          "youGive": "ÖDEMEK İSTEDİĞİNİZ TUTAR",
          "youTake": "ALACAĞINIZ TUTAR",
          "email": "E-posta",
          "password": "Parola",
          "cryptoAdress": "{{{name}}} Cüzdan Adresi",
          "cryptoAdress": "Kripto cüzdan adresi",
          "externalTransaction": "İşlem Kimliği",
          "coupone": "İndirim kuponum var",
          "couponeCode": "Kupon Kodu",
          "cardNumber": "Kart numarası",
          "month": "AA",
          "year": "YY",
          "name": "İsim",
          "fullName": "Ad Soyad",
          "mobilePhone": "Cep telefonu",
          "birth": "Doğum tarihiniz AA.GG.YYYY formatındadır.",
          "videoVerification": "Video doğrulama",
          "verification": "Doğrulama",
          "status": "Durumu kontrol et",
          "login": {
              "captcha": "Captcha",
              "reset": "Sıfırla",
              "forgot": "Parolanızı mı unuttunuz?",
              "signIn": "Oturum aç"
            },
          "registration": {
              "create": "Oluştur"
          }
        },
        "topic1": {
          "paragraph1": "Bitcoin ilk olarak 2009 yılında ortaya çıktı ve halen 2018 yılında dünyanın en popüler kripto para birimi olmaya devam ediyor. Kripto para biriminin ilk örneği olan Bitcoin, alışılgelmiş para tanımını değiştirmeyi başardı. Bitcoin'in piyasa değeri, Tesla, General Motors ve FedEx gibi devleri geride bıraktı ve 70 milyar dolardan fazla paraya ulaştı.",
          "paragraph2": "Dijital para birimi veya kripto para birimi nedir? Kripto para birimi, işlemleri güvenceye almak ve para biriminin ek birimlerinin oluşturulmasını kontrol etmek için kriptografi kullanarak bir değişim aracı olarak çalışan bir dijital varlıktır. Fiziksel bir şekli yoktur ve her zamanki alışılan bankacılık sistemlerinden farklıdır.",
          "paragraph3": "Bitcoin nereden geliyor? Dünya, Bitcoin'in yaratıcısının adını bilmesine rağmen, (Satoshi Nakamoto) bu isim büyük ihtimalle bir programcı, hatta bir grup için bir takma addır. Temel olarak, Bitcoin “Satoshi” tarafından oluşturulan 31.000 satır koddan gelir ve yalnızca yazılım tarafından kontrol edilir.",
          "paragraph4": "Bitcoin nasıl çalışır? İşlemler Bitcoin cüzdanları arasında gerçekleşir ve şeffaftır. Blok zincirinde herkes, her “madeni para” için kökenini ve yaşam döngüsünü kontrol edebilir. Ancak, “madeni para” yoktur. Bitcoin'in fiziksel bir formu yoktur ve sanal bir formu yoktur.",
          "paragraph5": "Bitcoin'in düzenli paradan farkı nedir? - Dünya çapında para birimidir ve hiçbir hükümetin Bitcoin üzerinde kontrolü yoktur. Sadece matematiğe dayanır. - Şeffaflık: Tüm Bitcoin işlemleri BlockChain'de görülebilir. - Sınırlı emisyon: Çıkarılabilecek toplam Bitcoin sayısı 21M'dir. Şu anda 16M'den fazla Bitcoin dolaşımda. Ne kadar popüler olursa o kadar pahalı olur. - Anonimlik: Bitcoin'lerinizi istediğiniz zaman ve istediğiniz miktarda kullanabilirsiniz. Bu, ödeme hedefini ve fonların kaynağını açıklamanız gerektiğinde, banka hesabınızdan para aktarmanın aksine gerçek bir bulgudur. - Hız: Bankanız özellikle büyük değerler söz konusu olduğunda para transferi için zaman alacaktır. Bitcoin ile, gönderme ve alma rutini saniye sürecektir.",
          "paragraph6": "Bitcoin işlemi nasıl gerçekleşir? Her işlemin 3 boyutu vardır: Girdi, miktar ve çıktı. Girdi –– İşlemde kullanılan Bitcoin kaynağı. Bize mevcut sahibinin nereden geldiğini söyler. Tutar - - kullanılan \"coin\" sayısı. Çıktı –– paranın alıcının adresi.",
          "paragraph7": "Bitcoin cüzdanı nedir? Bitcoin cüzdanı, Bitcoin'i yönetmenize izin veren bir programdır: Alma ve gönderme, dengeyi kontrol etme ve Bitcoin oranlarını takip etme vb. Bu tür cüzdanlarda “madeni para” olmamasına rağmen, ortak Bitcoin adreslerine ve “imza” işlemlerine erişmek için kullanılan dijital anahtarlar içerir. Popüler cüzdanlar: TREZOR, Ledger, Blockchain.info, Coinbase.",
          "paragraph8": "Bitcoin'in kısa tarihi 2007 –– Efsaneye göre, Satoshi Nakamoto çalışmalarına Bitcoin 2008, Ağustos - bitcoin.org'un tesciliyle başladı. 2009, Ocak –– Bitcoin'de ilk işlem yapıldı. 2009, Ekim –– Bitcoin değeri, Bitcoin üretmek için kullanılan elektriğin maliyetine dayanarak, 1 USD = 1.306.03 BTC 2010, Mayıs –– 10,000 BTC'nin 2 pizzayı dolaylı olarak satın almak için kullanıldığı, gerçekten tarihi bir an olarak belirlendi. Bugün aynı miktarda 2500 pepperoni pizza satın alabilirsiniz. 2010, Temmuz –– Bitcoin fiyatı sadece 5 gün içinde 1 BTC için 0,008 USD'dan 0,080 USD'ye çıktı. 2010, Aralık –– ilk mobil işlem gerçekleşti. 2011, Ocak –– Tüm Bitcoin'lerin% 25'i oluşturuldı. 2013 –– ABD Hazinesi, BTC'yi dönüştürülebilir bir merkezi olmayan sanal para birimi olarak sınıflandırdı. 2013, Mart –– Bitcoin piyasa değeri 1.000.000 ABD dolarına ulaştı. 2013, Mayıs –– San Diego, Kaliforniya'da bulunan ilk Bitcoin ATM. 2013, Kasım –– Bitcoin fiyatı ikiye katlandı ve 503.10USD'ye ulaştı. Aynı ay 1.000 USD'ye kadar büyüdü. 2014 –– Dell ve Windows Bitcoin'i kabul etmeye başladı. 2016, Eylül –– ABD federal yargıcı, BTC'nin “bu terimin sade anlamındaki fonlar” olduğunu belirtti. 2017, Mayıs –– BTC fiyatı 20 gün içinde 1.402 USD'den 2.000 USD'ye yükseldi. 2017, Ağustos –– Bitcoin oranı, 21 Ağustos'a kadar madalyonun başına 4.489.10 USD seviyesine kadar en yüksek orana sahip olan madeni para başına 4.000USD'ye ulaştı. ",
        },
        "topic2": {
          "paragraph1": "Zaten Bitcoin'imiz var, o zaman neden Ethereum'a ihtiyacımız olsun ki? Hadi kazalım! Bitcoin, adil ve şeffaf para transferleri sağlar. Ethereum'un dijital para birimi olan Ether, aynı rolü oynayabilir. Bununla birlikte, Ethereum tarafından sunulan işlevsellikten çok daha fazlası var.",
          "paragraph2": "Ethereum nedir? Ethereum, dağıtık bilgisayar kullanımına izin vermek için blok zinciri teknolojisini kullanan açık kaynaklı bir platformdur. Akıllı sözleşmelerle güvence altına alınmış ve ödeme aracı olarak “ether” olarak adlandırılan kendi kripto para birimine sahiptir.",
          "paragraph3": "Ethereum nereden geliyor? Ethereum ilk olarak Kanadalı programcı Vitalik Buterin tarafından 2013'ün sonunda bir fikir olarak sunuldu. 2014 yazında gerçekleşen bir crowdsale tarafından finanse edilen platform, 2015 yılının Temmuz ayında dünyaya sunuldu.",
          "paragraph4": "Ethereum'un Bitcoin'den farkı nedir? Bitcoin'den esinlenilen Ethereum, dünyanın her yerinden gönüllü programcıların kişisel bilgisayarları tarafından barındırılan blok zinciri teknolojisini de kullanır. Ethereum fiyatı, Bitcoin ile olduğu gibi insanlar arasındaki değişimlerle belirlenir. Fark büyük olsa da, Ethereum her şeyden önce dağıtılmış hesaplama için bir platformdur. Programcıların üsünde çeşitli merkezi olmayan uygulamalar (Dapps) oluşturmalarına ve işletme giderleri için fon almasına olanak sağlar. Bu sebepten orta yönetim gerekli değildir ve ek masraflar yoktur. Ethereum platformu aynı zamanda akıllı sözleşmeleri, taraflar arasındaki ilişkilerin kolaylaştırılması ve ödeme aracı olarak kendi şifreli para birimi olan “ether” olarak kullanmaktadır. Hem Bitcoin hem de Ethereum büyük ve bazı benzerliklere sahip olmasına rağmen, amaçlarının başlangıçta farklı olduğunu söylemek doğrudur. Birincisi sadece bir para birimidir, ikincisi ise sadece Ether'i esas alarak kendi iç amaçları için kullanır.",
          "paragraph5": "Akıllı sözleşmeler” konusunda ne kadar özel ve normal sözleşmelerden farkı nedir? Akıllı sözleşmenin temel amacı, bir sözleşmenin tüm şartlarının düzgün bir şekilde yerine getirildiğinden emin olmaktır. Merkezi olmayan işlevsellik, şartlarını doğrulamak ve zorlamak için, blockchain'de saklanan tarafsız uygulamaları kullanarak, Ethereum'a katkıda bulunanlar için daha fazla güvenlik sağlar. Bu teknoloji sahtekarlık için neredeyse hiç yer bırakmaz.",
          "paragraph6": "ICO nedir? Birçok uygulama Ethereum üzerine kurulmuştur ve ilk para teklifi (ICO) olarak adlandırılan bir süreçte de Ether ile fon toplayabilirler. Çalışma şekli, Kickstarter'ın yaptığıyla oldukça benzerdir. Programcılar, uygulamayı geliştirmek ve masrafları karşılamak için fonlar karşılığında (genellikle Ether) farklı jetonlar sunarlar. Belirli bir önceden belirlenmiş bir miktara ulaştığında veya belirli bir tarih gerçekleştiğinde, fonlar programcılara bırakılır. Hedef karşılanamazsa, fonlar ilk katkıda bulunanlara geri döner.",
          "paragraph7": "Ether nedir? Ether (ETH), bir dijital para birimi ve Ethereum'da kullanılan bir değer jetondur. Ethereum platformunda farklı servisler için ödeme aracı olmanın yanı sıra, kripto para birimi takaslarında da bulunur ve gerçek parayla değiştirilebilir.",
          "paragraph8": "En popüler Ethereum cüzdanları hangileridir? Bir Ethereum cüzdanı, kripto para birimini yönetmenize izin veren bir programdır: alma ve gönderme, dengeyi kontrol etme ve Ethereum döviz kurunu takip etme gibi işlemler yapabilirsiniz. Popüler cüzdanlar Jaxx, KeepKey, Ledger Nano S, Mist.",
          "paragraph9": "Ethereum 2013'ün kısa tarihi - Ethereum fikri, Kanadalı bir programcı ve Bitcoin Dergisi'nin kurucusu Vitalik Buterin tarafından sunulmaktadır. 2014, yaz –– Etherdsum yaratmak için fon toplamak için crowdsale başlatıldı. 2015, 30 Temmuz –– platformu çevrimiçi. 2016, Mayıs –– Merkezi olmayan otonom organizasyon (DAO), Ethereum platformunda yaratıldı. Bir jeton crowdsale tarafından finanse edilen, şu anda tarihin en büyük crowdfunding kampanyası. 2016, Haziran –– DAO saldırıya uğradı, Ethereum değeri 21.50USD'den 8USD'ye düştü. Bu, Ethereum'un 2017'de iki blokta çatallanmasına yol açtı. 2017 –– Ethereum, ilk para arzında (ICO)% 50 pazar payını kazandı. 2017, Mart –– Kurumsal Ethereum Alliance (EEA) oluşturuldu. Üyeler arasında Toyota, Samsung, Microsoft, Intel, Deloitte ve diğer oldukça büyük isimler. İttifak'ın temel amacı, Ethereum işlevselliğinin yönetim, bankacılık, sağlık, teknoloji vb. gibi yaşamın çeşitli alanlarında nasıl kullanılabileceğini belirlemektir. 2017, Haziran –– Ethereum döviz kuru 400USD (Ocak ayından bu yana% 5.000) yükseldi. 2017, Eylül –– Çin KOBİ'leri yasak getirdikten sonra tüm kripto para birimleri düştü, ancak daha sonra hızla toparlandı. 2017, Eylül –– Ethereum market cap, Çin'in ICO yasağı sonrasında 36 milyar doları aştı.",
          },     
   "buyPage": {
          "leftTitle": "Kredi veya Banka kartıyla Bitcoin Satın Al"
        },
        "map-segment": {
          "blocks": {
              "title-1": "Basit doğrulama",
              "title-2": "700 altcoin destekleniyor",
              "title-3": "Gizli ücret yok",
              "title-4": "Kayıt yok"
          },
          "title": "100'den fazla ülkede çalışıyoruz!",
          "subtitle": "{{{country}}} dahil",
          "shortTitle": "100'den fazla ülkede çalışıyoruz!",
          "text1": "Basit doğrulama",
          "popupText1": "İlk kez alım yapanlar sadece telefon numarasını doğrulamak zorundalardır.",
          "text2": "700 altcoin destekleniyor",
          "popupText2": "Bir Kredi/Banka kartıyla 700'den kripto dövizi satın alabilirsiniz",
          "text3": "Gizli masraf yok",
          "popupText3": "Anlaşma öncesi gösterilen tam altbağam miktarını tam olarak alırsınız",
          "text4": "Kayıt yok",
          "popupText4": "Kayıt olmanız gerekmiyor, belirttiğiniz adrese kriptolanabilirlik gönderilecektir."
        },
        "buying-segment": {
          "title": "500.000'den fazla müşteri",
          "sub-title": "2015 yılından beri Indacoin kullanıyor"
        },
        "phone-segment": {
            "title": "Uygulamamızı indirin",
            "text1": "Bir cüzdan içinde 700'den fazla kripto para birimi satın alarak depolayın",
            "text2": "Yatırım portföyünüzdeki değişiklikleri takip edin",
            "text3": "Arkadaşınıza kripto para göndermek, bir mesaj göndermek kadar kolaylaştı."
        },
        "social-segment": {
          "buttonText": "Facebook'ta oku",
          "on": "Facebook'ta",
          "facebook-review-1": "Harika bir Bitcoin sayfası, hızlı ve basit bir süreç. Indacoin'i tavsiye ederim",
          "facebook-review-2": "Çok iyi hizmet ve kullanımı gayet kolay, kullanıcı dostu ve çok yararlı. Her ne kadar dijital bir işlemler yapsam da, işlemin diğer tarafında güvenebileceğiniz bir kişi var, bu hizmeti tekrar kullanacağım.",
          "facebook-review-3": "Sadece kripto para dünyasını çevreleyen her şeyin tadına bakmaya başladığımdan beri şunu söyleyebilirim ki; Indacoin'de satın alma yapmak diğer sitelere göre çok daha basit. Ve takılıp kaldığınızda müşteri hizmetleri desteğinden çok daha fazlası var.",
          "facebook-review-4": "Çok hızlı ödeme, mükemmel destek ve yüksek döviz kurları.",
          "facebook-review-5": "Bu hizmeti kullanarak 100 Euro'luk bitcoin almaya karar verdim ve ödeme işlemi çok kolaydı. Bazı sıkıntılarım ve sorularım olmasına rağmen, destek ekibi neredeyse anında yardım etti. Indacoin çok daha rahat bir şekilde Bitcoin satın almamı sağladı. Ve gelecekte daha da fazla almayı planlıyorum.",
          "facebook-review-6": "Bitcoin satın almak için Indacoin'i kullandım. İşlem sorunsuz geçti, ardından doğrulama yapıldı. Kullanımı çok kolay ve kripto para satın almak için kullanıcı dostu bir portal. Bitcoin satın almak için bu platformu tavsiye ederim!",
          "facebook-review-7": "Kredi kartı ile kripto para almak zor ama Indacoin bu konuda çok iyi bana davranıyordu. Kendi dilini anlamayan diğer ülkelerden gelen müşterilere de çevrim içi destek imkanı var. Indacoin Teşekkür ederim!",
          "facebook-review-8": "Hızlı ve kolay. Çevrim içi destek çok hızlı ve anında yardımcı oldu. Profesyonel İpucu: Mobil uygulamayı kullanmak komisyonları %9 oranında azaltır.",
          "facebook-review-9": "Büyük hizmet, hızlı işlem harika müşteri desteği. Kesinlikle tekrar kullanacağım.",
          "facebook-review-10": "Son derece büyük hizmet. Ve iletişim, canlı sohbet ve tamamen güvenilir. Adım adım yardım alabilirsiniz. Bitcoin satın almanın en kolay yolu. Teşekkür Indacoin sizinle daha fazla işlem yapacağım.",
          "facebook-review-11": "Harika servis ve hızlı işlem. Müşteri hizmetleri çok iyi ve anında cevap veriyorlar. Kripto para satın almak için basit bir yol. Dürüst olmak gerekirse diğer hizmetlerin arasında Indacoin kripto para satın almak için kullandığım tek hizmet. Kripto para satın alma işlemlerinde onlara güveniyorum. Kripto para ile ilgilenen herkese Indacoin'i tavsiye ederim. Son olarak, Indacoin tek kelimeyle: Mükemmel !!!",
          "facebook-review-12": "200 Euro değerinde Bitcoin aldım ve web sitesi yasaldır. İşlemin gerçekleşmesi için biraz zaman alsa da, müşteri hizmetleri mükemmeldir. Müşteri desteğine ulaştım ve anında benimle ilgilendiler. Bu siteyi herkese kesinlikle tavsiye ederim. Bazen yoğun site trafiği nedeniyle işlem süresi 8-10 saat arasında değişmektedir. Paralarınızı anında almazsanız panik yapmayın, bazen zaman alacaktır.",
          "facebook-review-13": "Büyük iş Indacoin. Indacoin'de kredi kartı ile satın alınan paralar şimdiye kadar en iyi deneyimi vardı. Danışman Andrew yardımcı oldu ve 3 dakikadan daha az sürede tamamlanan transfer işlemi beni etkiledi. Indacoin'i kullanmaya devam edeceğim :)",
          "facebook-review-14": "Indacoin, alıcıların platformdaki Bitcoin'leri daha kolay satın alabilecekleri kolay ve kullanıcı dostu bir arayüzdür. Danışmanlardan adım adım yardım almak kolaydır ve sorununuzu çözmenize yardımcı olmak için gerçekten isteklidirler. Kesin olarak insanları Indacoin'e yönlendireceğim ve sürekli olarak beni bilgilendirecekler. Onların hatalarını çözmek için ekstra mesafe katetmeye istekli olacaklar. Teşekkürler Indacoin!",
          "facebook-review-15": "Birçok kripto para birimini satın almanın en kolay ve hızlı yolu. İyi iş çocuklar, teşekkürler!",
          "facebook-review-16": "Kripto paraları satın almak için çok kolay bir yol. Müşteri hizmetleri de muhteşem! Doğrudan satın almak için altcoinler de var. İlk olumlu deneyimimin ardından Indacoin'i herkese tavsiye ederim.",
          "facebook-review-17": "Harika servis! Kullanıcı dostu ve ayrıca çeşitli jeton satın almak çok kolay.",
          "facebook-review-18": "Indacoin BTC alışverişi için en iyi platformlardan biridir. Birkaç değişim yaptım ve çok hızlıydı.",
          "facebook-review-19": "Indacoin'de hizmet çok iyi ve kullanıcı dostu bir portal. Kredi veya banka kartı ile çeşitli kripto para ve ERC20 jeton satın almak çok kolay. Ayrıca çok hızlı ve güvenli! Aradığım hizmet ve platform! Indacoin'i kesinlikle tavsiye ederim!"
        },
        "cryptocurrencies-segment": {
            "title": "{{{name}}}'de {{{count}}}'den fazla farklı kripto para birimi satın alınabilir"
        },
        "invalidEmail": "Lütfen geçerli bir e-posta adresi girin",
        "invalidWallet": "Lütfen geçerli bir cüzdan adresi girin",
        
        "invalidFullName": "Lütfen geçerli bir isim girin",
        "invalidPhone": "Lütfen geçerli bir telefon numarası girin",
        "invalidBirthday": "Lütfen geçerli bir doğum tarihi girin. (Format: AA.GG.YYYY)",
      
        "mobileMockups": {
          "title": "Telefonunuzda Indacoin",
          "text": "Akıllı cüzdanımız Indacoin, Android, iOS kullanıcıları ve her internet tarayıcısında kullanılabilir. Arkadaşınıza kripto para göndermek, bir mesaj göndermek kadar kolaylaştı."
        },
        "news-segment": {
          "no-news": "Diliniz için haber yok",
          "news-error": "Özür dileriz, haberlere şu anda ulaşılamıyor",
          "comments": "Yorumlar",
          "send": "Gönder",
          "news1": "Ethereum'u Indacoin'de banka kartınızla satın alabileceğinizi söylemekten memnuniyet duyarız. Eğer kartınızla Bitcoin satın aldıysanız, ek bir doğrulama gerekmeyecektir.",
          "news2": "İşlem ücretleri değişti! Artık limit emrinizin uygulanmasından %1 kazanabilirsiniz ve market siparişi için %3 ücret belirlenir. Kredi / banka kartları ile fon yatırımı ücreti %7'den %4'e düşürülmüştür.",
          "news3": "Sevgili dostlar, Indacoin'deki son derece önemli değişiklikler hakkında sizi bilgilendirmek isteriz. 29 Aralık’a kadar, günlük olarak USD mevduatınız üzerinden %8’lik yıllık faiz ödemeniz otomatik olarak yapılmayacaktır. Bir garanti geliri elde etmek için, “yatırım” bölümünü ziyaret etmeniz ve 30 ila 365 gün arasında bir süre için 10 ila 15.000 $ arasında yatırım yapmanız gerekmektedir. Faiz oranı %10.76 ila %11.98 arası değişmektedir. Ayrıca erken çekilme cezasını %10'dan %5'e düşürdük.",
          "news4": "Sevgili Indacoin müşterileri, 9 Kasım'dan bu yana limitinizi ve siparişlerinizi uygulamak için işlem ücretinin %0,3 olacağını bildirmek isteriz."
        },
  
        "news": {
          "anotherNews": "Diğer haberler",
          "readMore": "Daha fazla oku",
          "topic1": {
            "title": "Yeni web sitesinin lansmanı",
            "full": "Kripto para birimini daha hızlı ve daha kolay bir şekilde satın almak için sitemizin tamamen yeniden tasarlanmasını tamamladığımızı bildirmekten mutluluk duyuyoruz. Ayrıca, satın alınabilecek dijital para sayısını da artırdık: Artık tek bir yerde gerçekten çeşitlendirilmiş bir yatırım portföyü oluşturmak için 200'den fazla seçenek arasından seçim yapabilirsiniz.",
            "short": "Bir satın alma işlemi yapmak için sitemizin tamamen yeniden tasarlanmasını tamamladığımızı duyurmaktan mutluluk duyuyoruz...",
            "date": "December 2, 2017"
          },
          "topic2": {
            "title": "Android için yeni uygulama",
            "full": "Yeni uygulamamız artık Google Play'de mevcut! Bu App ile, sadece bir cep cüzdanını kullanarak 100'den fazla altcoin edinebilir ve bunları yönetebilirsiniz. Ayrıca, Indacoin App ayrıca cep telefonu üzerinden alıcıya kripto para göndermek için bir fırsat sağlar.",
            "short": "Yeni uygulamamız artık Google Play'de mevcut!",
            "date": "April 8, 2017"
          },
          "topic3": {
            "title": "Beklediğiniz iPhone Uygulaması",
            "full": "İOS için Indacoin Uygulaması'nın bir sürümünü yayınlamaya hazırız. Bunlarla 200'den fazla altcoin satın alabilir ve tüm kripto varlıklarınızı tek bir yerde tutabilirsiniz. Destek ekibi size 7/24 yardımcı olmak için her zaman burada.",
            "short": "iOS için Indacoin Uygulaması'nın bir sürümünü yayınlamaya hazırız.",
            "date": "July 21, 2017"
          }
        },
  
        "registerChange": {
          "unavailable": "Bu kripto para birimi satın alınamaz",
          "unknown": "Bilinmeyen sunucu hatası",
          "error": "Veri gönderiminde hata",
          "3ds": "Kart 3D güvenlik sistemini desteklemiyor. Lütfen başka kart kullanın",
          "invalidWalletAddress": "Cüzdan adresi geçerli değil",
          "scam": "SCAM uyarısı. İşleminizi gerçekleştiremiyoruz, Lütfen support@indacoin.com adresinden bize ulaşın",
          "limit": "Sınır aşıldı",
          "phone": "Telefon desteklenmiyor"
        },
        "tags": {
          "title-btc": "Bitcoin BTC'yi {{{longName}}} {{{shortName}}} - Indacoin'e aktarın",
          "title": "[cryptocurrency] ile [country] bir kredi kartı veya bankamatik kartı satın alın - Indacoin",
          "title2": "Bitcoin'i Bir Kredi Veya Banka Kartıyla Dünya Çapında Satın Alın - Indacoin",
          "title3": "{{{cryptocurrency}}} ile {{{country}}} bir kredi kartı veya bankamatik kartı satın alın - Indacoin",
          "title4": "{{{country}}} içinde kredi kartı veya banka kartıyla cryptocurrency satın alın - Indacoin",
          "titles": {
              "affiliate": "Indacoin ortaklık programı",
              "news": "Indacoin'den son haberler",
              "help": "Kripto döviz kuru SSS - Indacoin",
              "api": "Kripto-Döviz Değişim API'si - Indacoin",
              "terms-of-use": "Kullanım şartları ve AML politikası - Indacoin",
              "login": "Oturum aç - Indacoin",
              "register": "Üye ol - Indacoin",
              "locale": "Bölge değiştir - Indacoin",
              "currency": "{{{{fullCurrency}}} ({{{shortCurrency}}}) fiyat çizelgeleri ve işlem hacmi - Indacoin"
            },
            "descriptions": {
              "affiliate": "Indacoin bağlı kuruluş programı, her bir satın alımın %3'üne kadar bir kredi / banka kartı ile para kazanmanızı sağlar. Ücret, Indacoin bitcoin cüzdanınıza gönderilecektir",
              "news": "Kripto para piyasasındaki en son haberlerle irtibatta kalmak için resmi haber blogumuzu ziyaret edin! Indacoin uzmanlarından tek bir yerde faydalı bilgiler ve kapsamlı yorumlar!",
              "help": "Indacoin'de kripto para'yı nasıl satın alacağınıza, satacağınıza ve değiştireceğinize dair tüm sorularınızın cevaplarını bulun.",
              "api": "API'miz, Visa ve Mastercard ile anında bir cryptocurrency alış verişi sağlar. Entegrasyon ve Örnekleri Kontrol Et.",
              "terms-of-use": "Genel bilgi. Kullanım Şartları. Sağlanan hizmetler. Fonların transferi. Komisyonlar. AML Politikası.",
              "login": "Giriş yap veya yeni hesap oluştur",
              "register": "Yeni hesap oluştur",
              "locale": "Bölgenizi seçin ve bunu seçin ve bir dahaki sefere Indacoin'i ziyaret ettiğinizde bölgenizi hatırlayacağız",
              "currency": "İşlem hacmi ve geçmiş kripto para birimi bilgileri ile birlikte ABD Doları için {{{fullCurrency}}} için geçerli fiyatlar"
            }
        },
        "usAlert": "Üzgünüz, ancak Indacoin Amerika Birleşik Devletleri'nde çalışmıyor",
        "wrongPhoneCode": "Telefonunuzdan yanlış bir kod girdiniz",
        "skip": "Atla",
        "minSum": "Alınacak minimum tutar",
        "currency": {
          "no-currency": "Belirli bir kripto para birimi için grafik mevcut değil",
          "presented": {
            "header": "Bitcoin, 2009 yılında dünyaya sunuldu ve 2018 yılında dünyanın birincil kripto para birimi olarak kaldı"
          },
          "difference": {
            "title": "Bitcoin normal paradan nasıl farklıdır?",
            "header1": "Sınırlı emisyon",
            "text1": "Çıkarılabilecek toplam Bitcoin sayısı 21M'dir. Şu anda 16M'den fazla Bitcoin dolaşımda. Ne kadar popüler olursa o kadar pahalı olur.",
            "header2": "Dünya çapında para",
            "text2": "Hiçbir hükümetin Bitcoin üzerinde kontrolü yoktur. Sadece matematiğe dayanıyor",
            "header3": "Anonimlik",
            "text3": "Bitcoin'lerinizi istediğiniz zaman ve herhangi bir miktarda kullanabilirsiniz. Bu, ödeme hedefini ve fonların kaynağını açıklamanız gerektiğinde, banka hesabınızdan para aktarmanın aksine gerçek bir bulgudur.",
            "header4": "Hız",
            "text4": "Bankanız, özellikle de büyük değerler söz konusu olduğunda para transferi için zaman alacaktır. Bitcoin ile, gönderme ve alma rutini saniye sürecektir.",
            "header5": "Şeffaflık",
            "text5": "Tüm Bitcoin işlemleri BlockChain'de görülebilir."
          },
          "motivation": {
            "title": "Kendi türünün ilk doğan para birimi olan parayı, bildiğimiz şekilde değiştirmeyi başardı. Şimdiye kadar, Bitcoin market cap, Tesla, General Motors ve FedEx gibi devleri geride bırakan 70 milyar dolardan fazla kişiye ulaştı."
          },
          "questions": {
            "header1": "Dijital para birimi ve kripto para birimi nedir?",
            "text1": "Kripto para, işlemleri güvenceye almak ve para biriminin ek birimlerinin oluşturulmasını kontrol etmek için kriptografi kullanarak bir değişim aracı olarak çalışan bir dijital varlıktır. Fiziksel bir şekli yoktur ve normal bankacılık sistemlerine göre merkezi olmayan bir yapıya sahiptir.",
            "header2": "Bitcoin işlemi nasıl gerçekleşir?",
            "text2": "Her işlemin 3 boyutu vardır: Girdi - işlemde kullanılan Bitcoin kaynağı. Bize mevcut sahibinin nereden geldiğini söyler. Tutar - kullanılan “coin” sayısı. Çıktı - parayı alan kişinin adresi.",
            "header3": "Bitcoin nereden geliyor?",
            "text3": "Dünya, Bitcoin'in yaratıcısının adını bilmesine rağmen, “Satoshi Nakamoto” büyük olasılıkla bir programcı veya bir grup için bir takma addır. Temel olarak, Bitcoin “Satoshi” tarafından oluşturulan 31.000 satır koddan gelir ve yalnızca yazılım tarafından kontrol edilir.",
            "header4": "Bitcoin nasıl çalışır?",
            "text4": "İşlemler Bitcoin cüzdanları arasında gerçekleşir ve şeffaftır. Blok zincirinde herkes, her “madeni para” için kökeni ve yaşam döngüsünü kontrol edebilir. Ancak, “madeni para” yoktur. Bitcoin'in fiziksel bir formu yoktur ve sanal bir formu yoktur.",
            "header5": "Bitcoin cüzdanı nedir?",
            "text5": "Bitcoin cüzdanı Bitcoin'u yönetmenize izin veren bir programdır. Bitcoin'i alma ve gönderme, dengeyi kontrol etme ve bitcoin'in kur oranlarına bakmaya işe yarar. Bu tür cüzdanlarda “madeni para” olmamasına rağmen, ortak Bitcoin adreslerine ve “imza” işlemlerine erişmek için kullanılan dijital anahtarlar içerir. Popüler cüzdanlar:",
          },
          "history": {
            "title": "Bitcoin'in kısa tarihi",
            "text1": "Efsaneye göre bu Satoshi Nakamoto'nun Bitcoin'deki çalışmalarına başladığı zamandır.",
            "text2": "Bitcoin'de yapılan ilk işlem. Bitcoin üretmek için kullanılan elektriğin maliyetine dayanan bitcoin değeri, 1.306.03 BTC için 1 USD olarak belirlenmiştir.",
            "text3": "Halihazırda tüm Bitcoin'lerin %25'i oluşturuldu",
            "text4": "Dell ve Windows Bitcoin'i kabul etmeye başladı",
            "text5": {
              "part1": "BTC fiyatı birkaç ayda 1,402 USD'den 19,209 USD'ye yükseldi.",
              "part2": "Bitcoin oranı 18 Aralık 2017'de jeton başına 20.000 USD'ye ulaştı"
            },
            "text6": "Bitcoin.org'un tescili",
            "text7": {
              "part1": "10,000 BTC'nin 2 pizzayı dolaylı olarak satın alması gerçekten tarihi bir an. Bugün aynı miktar için 2.500 pepperoni pizza satın alabilirsiniz.",
              "part2": "Bitcoin fiyatı sadece 5 günde 1 BTC için 0,008 USD'dan 0,08 USD'ye yükseldi.",
              "part3": "İlk mobil işlem gerçekleşir"
            },
            "text8": {
              "part1": "ABD Hazinesi, BTC'yi dönüştürülebilir bir merkezi olmayan sanal para birimi olarak sınıflandırdı.",
              "part2": "San Diego, Kaliforniya'da bulunan ilk Bitcoin ATM'si.",
              "part3": "Bitcoin fiyatı iki katına çıkar ve 503,10 USD'a ulaşır. Aynı ayda 1.000 USD'ye kadar büyür"
            },
            "text9": "ABD federal yargıcı, BTC'nin “bu terimin sade anlamı içindeki fonlar” olduğunu belirtti."
          },
          "graph": {
            "buy-button": "Satın Al"
          },
          "ethereum": {
            "presented": {
              "header": "Zaten Bitcoin'imiz var, o zaman neden Ethereum'a ihtiyacımız olsun ki? Hadi kazalım! Bitcoin, adil ve şeffaf para transferleri sağlar. Ethereum'un dijital para birimi olan Ether, aynı rolü oynayabilir. Bununla birlikte, Ethereum tarafından sunulan işlevsellikten daha fazlası var."
            },
            "questions": {
              "header1": "Ethereum nedir?",
              "text1": "Ethereum, dağıtık bilgisayar kullanımına izin vermek için blockchain teknolojisini kullanan açık kaynaklı bir platformdur. Akıllı sözleşmelerle güvence altına alınmış ve ödeme aracı olarak “Ether” olarak adlandırılan kendi kripto para birimine sahiptir.",
              "header2": "Ethereum'un kökeni nedir?",
              "text2": "Ethereum ilk olarak Kanadalı programcı Vitalik Buterin tarafından 2013'ün sonunda bir fikir olarak sunuldu. 2014 yazında gerçekleşen bir crowdsale tarafından finanse edilen platform, 2015 yılının Temmuz ayında dünyaya sunuldu.",
              "header3": "ICO nedir?",
              "text3": "Birçok uygulama Ethereum üzerine kurulmuştur ve ilk para teklifi (ICO) olarak adlandırılan bir süreçte de Ether ile fon toplayabilirler. Çalışma şekli, Kickstarter'ın yaptığıyla oldukça benzerdir. Programcılar, uygulamayı geliştirmek ve masrafları karşılamak için fonlar karşılığında (genellikle Ether) farklı jetonlar sunarlar. Belirli bir önceden belirlenmiş bir miktara ulaştığında veya belirli bir tarih gerçekleştiğinde, fonlar programcılara bırakılır. Hedef karşılanamazsa, fonlar ilk katkıda bulunanlara geri döner.",
              "header4": "Ether nedir?",
              "text4": "Ether (ETH), bir dijital para birimi ve Ethereum'da kullanılan bir değer jetonudur. Ethereum platformunda farklı servisler için ödeme aracı olmanın yanı sıra, kripto para birimi takaslarında da kullanılır ve gerçek parayla değiştirilebilir.",
              "header5": "Akıllı sözleşmeler konusunda ne gibi özellikler vardır? Diğer normal sözleşmelerden farkı nedir?",
              "text5": "Akıllı sözleşmenin temel amacı, bir sözleşmenin tüm şartlarının düzgün bir şekilde yerine getirildiğinden emin olmaktır. Merkezi olmayan işlevsellik, şartlarını doğrulamak ve zorlamak için, blockchain'de saklanan tarafsız uygulamaları kullanarak, Ethereum'a katkıda bulunanlar için daha fazla güvenlik sağlar. Bu teknoloji sahtekarlık için neredeyse hiç yer bırakmaz.",
              "header6": "Ethereum'un Bitcoin'den farkı nedir?",
              "text6": "Bitcoin'den esinlenilen Ethereum, dünyanın her yerinden gönüllü programcıların kişisel bilgisayarları tarafından barındırılan blok zinciri teknolojisini de kullanır. Ethereum fiyatı, Bitcoin ile olduğu gibi insanlar arasındaki değişimlerle belirlenir. Fark büyük olsa da, Ethereum her şeyden önce dağıtılmış hesaplama için bir platformdur. Programcıların üsünde çeşitli merkezi olmayan uygulamalar (Dapps) oluşturmalarına ve işletme giderleri için fon almasına olanak sağlar. Bu sebepten orta yönetim gerekli değildir ve ek masraflar yoktur. Ethereum platformu aynı zamanda akıllı sözleşmeleri, taraflar arasındaki ilişkilerin kolaylaştırılması ve ödeme aracı olarak kendi şifreli para birimi olan “ether” olarak kullanmaktadır. Hem Bitcoin hem de Ethereum büyük ve bazı benzerliklere sahip olmasına rağmen, amaçlarının başlangıçta farklı olduğunu söylemek doğrudur. Birincisi sadece bir para birimidir, ikincisi ise sadece Ether'i esas alarak kendi iç amaçları için kullanır.",
              "header7": "En popüler Ethereum cüzdanları hangileridir?",
              "text7": "Bir Ethereum cüzdanı, kripto para birimini yönetmenize izin veren bir programdır: alma ve gönderme, dengeyi kontrol etme ve Ethereum döviz kurunu takip etme gibi işlemler yapabilirsiniz. Popüler cüzdanlar Jaxx, KeepKey, Ledger Nano S, Mist.",
            },
            "history": {
              "title": "Ethereum'un kısa tarihi",
              "text1": "Ethereum yaratmak amacıyla fon toplamak için Crowdsale başlatıldı",
              "text2": "Merkezi olmayan otonom organizasyon (DAO), Ethereum platformunda yaratılmıştır. Bir jeton crowdsale tarafından finanse edilen, şu anda tarihin en büyük crowdfunding kampanyasıdır.",
              "text3": "İlk para teklifinde Ethereum %50 pazar payı kazandı",
              "text4": "Ethereum döviz kuru 400 USD seviyesine yükseldi (Ocak ayından bu yana% 5.000 artış)",
              "text5": "Ethereum fikri, Kanadalı bir programcı ve Bitcoin Dergisi'nin kurucusu Vitalik Buterin tarafından ortaya atıldı.",
              "text6": "30 Temmuz'da Ethereum platformu çevrimiçi oldu",
              "text7": "DAO saldırıya uğradı, Ethereum'un değeri 21,5 USD'den 8 USD'ye düştü. Bu, Ethereum'un 2017'de iki blokta çatallanmasına yol açtı",
              "text8": "Kurumsal Ethereum Alliance oluşturulur. Üyeler arasında Toyota, Samsung, Microsoft, Intel, Deloitte ve diğer oldukça büyük isimler vardı. İttifak'ın temel amacı Ethereum işlevselliğinin yönetim, bankacılık, sağlık, teknoloji vb. Gibi yaşamın çeşitli yönlerinde nasıl kullanılabileceğini belirlemekti.",
              "text9": {
                "part1": "Çin, ICO'yu yasakladıktan sonra tüm kripto para birimlerinin değeri düştü, ancak daha sonra hızla iyileşti.",
                "part2": "Ethereum pazar değeri, Çin'in ICO yasağının ardından sadece en yüksek tarihsel değeri olan 36 milyar dolardan fazla paraya ulaştı."
              }
            }
          }
        },
        "affiliate": {
          "promo": {
              "offer-block": {
                  "text": "Müşterilerinizin kredi kartı/banka kartıyla kripto almasına izin vermeye hazır mısınız? Lütfen bize bir e-posta bırakın ve en kısa sürede iletişime geçelim",
                  "input": "E-posta",
                  "button": "Başla",
                  "mail-success": "Teşekkür ederim. Sana cevap vereceğiz"
              },
              "api": {
                "title": "API entegrasyonu",
                "text": "API'mızı kullanarak müşterilerinizin sitenizden anında kripto para kazanmalarını sağlayabilirsiniz. Web sayfanıza veya mobil Uygulamanıza entegre edilen sahtecilik karşıtı platformumuz, platformunuzda yapılan ödemelerinizi izlemenize ve filtrelemenize olanak tanıyan bir araç olarak da kullanılabilir."
              },
              "affiliate": {
                "title": "Ortaklık Programı",
                "text": "Indacoin'i diğer müşterilere tavsiye edin. Eğer tavsiye ettiğiniz arkadaşınız bize katılırsa %3'e kadar kazanacaksınız.",
                "block-1": {
                  "title": "Indacoin Ortaklık Programına nasıl katılabilirim?",
                  "text": "Sitemize üye olmaktan çekinmeyin."
                },
                "block-2": {
                  "title": "Hesabımda toplanan paraları nasıl çekebilirim?",
                  "text": "Indacoin üzerinde BTC / ETH / diğer altcoin'leri herhangi bir ücret almadan veya banka kartınıza gönderebilirsiniz."
                },
                "block-3": {
                  "title": "Ortaklık Programınızın avantajı nedir?",
                  "text": "Platformumuzda müşteriler kayıtsız kredi / banka kartlarını kullanarak 700'den fazla farklı para satın alma şansına sahipler. Satın alımların hemen gerçekleştiği göz önüne alındığında, ortaklar karlarını anında elde edebilirler."
                },
                "block-4": {
                  "title": "Yönlendirme bağlantısının örneğini görebilir miyim?",
                  "text": "Elbette, bu şekilde görünür: ({{{link}}})"
                }
              },
              "more": "Daha fazlasını bul",
              "close": "Kapat"
            },
          "achievements": {
              "partnership-title": "Ortaklık",
              "partnership-text": "Indacoin ile işbirliği yapmanın birçok yolu var. Indacoin Ortaklık Programı, kripto endüstrisini, kripto endüstrisine katılımı artırmak için geleneksel fiyat para sistemi ile birleştirmek isteyen kripto ile ilgili şirketler içindir. Indacoin ile müşterilerinize kredi / banka kartları ile satın alma kriptosunun özelleştirilmiş deneyimini sunabileceksiniz.",
              "block-1": {
                  "header": "Güvenilir",
                  "text": "Kripto pazarında 2013 yılından beri çalışıyoruz ve sektöre öncülük eden saygın şirketlerle ortaklık kurduk. 190'dan fazla ülkeden 500.000'den fazla müşteri, kripto para satın almak için hizmetimizi kullandı."
              },
              "block-2": {
                  "header": "Güvenli",
                  "text": "Kripto projeleri için hileli faaliyetlerin tespiti ve önlenmesi için özellikle geliştirilmiş bir çekirdek teknolojisine sahip yenilikçi ve yüksek verimli bir sahtecilik karşıtı platform oluşturduk."
              },
              "block-3": {
                  "header": "Kullanıma hazır çözüm",
                  "text": "Ürünümüz zaten ICO, Cryptocurrency Exchanges, Blockchain Platforms ve Crypto Funds tarafından kullanılıyor."
              },
  
            "header": "Devrimde bizimle para kazanın",
            "subheader": "Blok zincir teknolojileri",
            "text1": "Indacoin, insanlara Bitcoin, Ethereum, Ripple, Dalgalar ve 200 farklı şifreli para birimi kredi veya bankamatik kartı ile anında satın almalarını sağlayan global bir platformdur",
            "text2": "Hizmet 2013'ten beri dünyanın hemen hemen her yerinde çalışıyor ancak bunlarla sınırlı değil:",
            "text3": "İngiltere, Kanada, Almanya, Fransa, Rusya, Ukrayna, İspanya, İtalya, Polonya, Türkiye, Avustralya, Filipinler, Endonezya, Hindistan, Beyaz Rusya, Brezilya, Mısır, Suudi Arabistan, BAE, Nijerya ve diğerleri"
          },
          "clients": {
            "header": "500.000'den fazla müşteri",
            "subheader": "2013'ten beri"
          },
          "benefits": {
              "header": "Şirketler için anahtar avantajlarımız:",
              "text1": "Müşterilerin kayıt olmadan kripto paralar satın alıp göndermelerine olanak tanıyan eşsiz hizmet. Dolayısıyla, Indacoin'e gelen müşteriler, kripto gönderilecek kart ayrıntılarını ve cüzdan adresini girmek zorunda kalacaklardır. Bu nedenle, potansiyel müşteriler satın alma işlemlerini hemen yapacak ve ortaklarımız kar elde edecektir.",
              "text2": "Her bir işlemin% 3'ü ortaklarımız tarafından yapılır. Ayrıca webmasters, bundan sonraki tüm satın alma işlemlerinin %3'ünü alacaktır.",
              "text3": "Geliri geri çekmek için bit eşi ve banka kartları kullanılabilir."
            },
          "accounts": {
            "header": "Şimdi bize katıl",
            "emailPlaceholder": "Email adresinizi giriniz",
            "captchaPlaceholder": "Captcha",
            "button": "Başla"
          }
        },
        "faq": {
          "title": "Sorular ve cevaplar",
          "header1": "Genel Sorular",
          "header2": "Kripto Para",
          "header3": "Doğrulama",
          "header4": "Komisyonlar",
          "header5": "Satın alma",
          "question1": {
            "title": "Indacoin nedir?",
            "text": "2013'ten beri Londra, İngiltere'de şifreleme alanında çalışan bir şirketiz. Kredi/Banka kartı ile ödemeden 100'ün üzerinde farklı kripto para birimi satın almak için hizmetimizden faydalanabilirsiniz."
          },
          "question2": {
            "title": "Sizinle nasıl irtibat kurabilirim?",
            "text": "Destek ekibimize e-posta (support@indacoin.com), telefon (+ 44-207-048-2582) veya web sayfamızdaki canlı sohbet seçeneğini kullanarak ulaşabilirsiniz."
          },
          "question3": { // TODO: link
            "title": "Bir ortaklık programınız var mı?",
            "text": "Evet, tüm detayları web sitemizde bulabilirsiniz"
          },
          "question4": {
            "title": "Burada hangi şifreli para birimlerini alabilirim?",
            "text": "Web sitemizde listelenen 100'ün üzerinde farklı kripto para birimini satın alabilirsiniz."
          },
          "question5": { // TODO: link
            "title": "Satın alma işlemi yapmak için üye olmam ya da hesap oluşturmam gerekir mi?",
            "text-part1": "Hayır, sadece giriş yapmanız yeterli",
            "text-part2": "Harcamak istediğiniz USD miktarı, e-posta, cep telefonu numarası, kart bilgileri ve bitcoin cüzdan adresi. Daha sonra sipariş sayfanıza yönlendirileceksiniz."
          },
          "question6": {
            "title": "Bu hizmet hangi ülkelerde  taşıyabilir?",
            "text": "ABD hariç (şu anda) tüm ülkelerle çalışıyoruz."
          },
          "question7": {
            "title": "Burada bitcoin satabilir miyim?",
            "text": "Hayır, sadece satın alabilirsiniz."
          },
          "question8": {
            "title": "Kripto para nedir?",
            "text": "Şifresizlik, sahte olmasını zorlaştıran güvenlik, özellik için şifreleme kullanan dijital bir para birimidir. Herhangi bir merkezi otorite tarafından verilmez, dolayısıyla hükümetin müdahalesine veya manipülasyonuna teorik olarak bağışıklık getirir."
          },
          "question9": {
            "title": "Şifreleme sırasını nerede saklayabilirim?",
            "text": "PC'nizdeki her kriptokrasi için ayrı bir cüzdan ayarlayabilir veya ücretsiz cüzdan servisimizi kullanabilir ve 100'den fazla kripto para birimini tek bir yerde saklayabilirsiniz."
          },
          "question10": {
            "title": "\"Bitcoin adresi\" ile ne demek istiyorsun?",
            "text": "Bitcoin cüzdan adresidir. Bitcoinlerin gönderilmesini istediğiniz kişinin adresini de girebilir veya Indacoin'de cüzdan oluşturabilirsiniz."
          },
          "question11": {
            "title": "Kriptokrasi işlemi ne kadar hızlı?",
            "text": "Kriptokrasi işlemleri süreleri gönderdiğiniz kriptokrasiye bağlıdır, Bitcoin işlemlerinin tamamlanması yaklaşık 15-20 dakika alır ve madalyonun geri kalanı için ortalama 1 saat kadar süre tanıyın."
          },
          "question12": {
            "title": "Kartımı doğrulamak için ne yapmam gerekir?",
            "text": "Doğrulamayı tamamlamak için telefonla aldığınız 4 haneli kodu girmeniz gerekir. İkinci adımınız için, yüzünüzü göstererek bir video kaydettiğiniz ve kimlik / Pasaport / Sürücü belgenizin bir taramasını veya fotoğrafını yüklediğiniz video doğrulama işleminden de geçmeniz istenebilir. Doğrulamanıza bağlı olarak, kart sahibi olduğunuzu bildiğimizden, bu adım zorunlu olabilir."
          },
          "question13": {
            "title": "Ya bir video doğrulaması kaydedemediysem?",
            "text": "Aynı sipariş sayfası bağlantısını mobil cihazınızdan açıp oradan da kaydedebilirsiniz. Sorun yaşarsanız, gerekli videoyu ek olarak support@indacoin.com adresine e-posta ile gönderebilirsiniz. Değişim sipariş numaranızı belirttiğinizden emin olun."
          },
          "question14": {
            "title": "Bir sipariş verdim ve tüm ayrıntıları girdim, neden hala işleme devam ediyor?",
            "text": "Eğer 2 dakikadan fazla beklediyseniz, işlem yapılmadı. Kartınız 3D güvenli olmayabilir, ve tekrar denemeniz gerekebilir. Satın alma işleminizi bankanızla PIN koduyla onayladığınızdan emin olun veya farklı bir para birimi cinsinden (EUR / USD) alışveriş yapmayı deneyebilirsiniz. Aksi takdirde, kartınızın 3D olduğu ve değişen para birimi yardımcı olmadığından eminseniz, o zaman bankanızla iletişim kurmanız gerekir; satın alma girişimleri engellenmiş olabilir."
          },
          "question15": {
            "title": "Satın aldığım her sefer yeni bir doğrulama yapmam gerekiyor mu?",
            "text": "Kartınızı bir kez doğrulamanız yeterlidir ve takip eden tüm işlemler otomatik olacaktır. Bununla birlikte, başka bir banka kartı kullanmaya karar verirseniz, doğrulamayı isteyeceğiz."
          },
          "question16": {
            "title": "Siparişim banka tarafından reddedildi, ne yapmalıyım?",
            "text": "Bankanızla iletişime geçip reddedilme nedenini sorun, kısıtlamayı kaldırabilirler. Böylece bir satın alma işlemi yapılabilir."
          },
          "question17": {
            "title": "Bankacılık kartımı doğrulamalı veya video göndermem gerekiyor?",
            "text": "Doğrulama işlemi; kartın çalınması, hacklenmesi veya herhangi bir dolandırıcılık olaylarında sizi korur; birisinin başkasının banka kartını kullanarak bir alışveriş yapmaya çalışması durumunda bunun tersi bir şekilde çalışır."
          },
          "question18": {
            "title": "Bilgilerimin ve kart bilgilerinin güvende olduğuna nasıl güvenebilirim?",
            "text": "Bilgilerinizi hiçbir şekilde üçüncü taraflarla rızanız olmadan paylaşmayız. Ayrıca, yalnızca 3D-Güvenli kartları kabul ederiz ve CCV kodu veya tam kart numarası gibi hassas verileri istemeyiz. Bu tür ödeme ayrıntılarını yalnızca Visa veya Mastercard ağ geçidi aracılığıyla sağlarsınız. Yalnızca, kartın sahibi olduğunuzdan emin olmak için gerekli bilgileri toplarız."
          },
          "question19": {
            "title": "Ücretiniz nedir?",
            "text": "Ücretimiz satın alma işleminden satın almaya kadar çeşitli faktörlere bağlı olarak değişmektedir. Bu nedenle, alacağınız tüm ücretler dahil, alacağınız kripto para biriminin tam tutarını belirten bir hesap makinesi oluşturduk."
          },
          "question20": {
            "title": "İşlem için ne kadar ücret alınacak?",
            "text": "Sadece hesap makinenizde siparişiniz sırasında belirttiğiniz miktardan ücret alınır. Tüm ücretler bu tutarda yer almaktadır."
          },
          "question21": {
            "title": "İndirim var mı?",
            "text": "Bazen indirimlerimiz var, destek ekibimizden daha fazla bilgi alabilirsiniz."
          },
          "question22": { // TODO: link
            "title": "Indacoin'den nasıl kripto para alabilirim?",
            "text": "3D güvenli Visa veya MasterCard'ı kullandığınızdan ve video doğrulama gerektirdiğimiz için ehliyet, kimlik veya pasaportunuzun bulunduğundan emin olun. Daha sonra lütfen bu link'i kullanarak hesap makinemize girin, satın almak istediğiniz kripto parayı seçin, EUR / USD cinsinden kartınızdan alınacak miktarı girin ve kripto adresini girin (veya Indacoin cüzdanına para yatırmak istiyorsanız boş bırakın). Gerekli alanları doldurun ve kartınız tahsil edildikten sonra, satın alma işleminizi doğrulamak için destek ekibimiz sizinle iletişime geçecektir. "
          },
          "question23": {
            "title": "Şifreleme sırası ne kadar hızlı gönderilecek?",
            "text": "Bir sipariş veya tam doğrulama yaptıktan (genellikle Indacoin'de kartı ilk kullandığınızda) sonra yaklaşık 30 dakika geçmesi gerekir."
          },
          "question24": {
            "title": "Minimum ve maksimum alımlar nelerdir?",
            "text": "İlk işlem için maksimum 200 $, ilk satın alım işleminin 4 gününden sonra mevcut ikinci işleminiz için 200 $, 7 günden sonra 500 $ ve ilk alışverişten 14 gün sonra 2000 $ 'lık limitiniz olacaktır. İlk alışverişinizden sonraki bir ayda kartınızla ödeme yaparken herhangi bir sınırlama yoktur. Lütfen minimum limitin her zaman 50 $ olduğunu unutmayın."
          },
          "question25": {
            "title": "Ne tür kartlar kabul edilir?",
            "text": "Yalnızca 3D güvenli Visa ve Mastercard'ı kabul ediyoruz."
          },
          "question26": {
            "title": "3D ne güvenli?",
            "text": "3D Secure teknolojisi, Verified by Visa ve MasterCard SecureCode programlarından oluşmaktadır. Online mağazalarımızda kredi kartı bilgilerinizi girdikten sonra, kişisel güvenlik kodunuzu isteyen yeni bir pencere açılacaktır. Finansal kurumunuz işlemi saniyeler içinde doğrulayacak ve satın almayı gerçekleştiren kişi olduğunuzu onaylayacaktır."
          },
          "question27": {
            "title": "Bitcoin satın almak için başka hangi ödeme sistemlerini kullanabilirim?",
            "text": "Platformumuzda Visa/Master kart ödemelerini kabul ediyoruz."
          }
        },
        "api": {
          "describe-segment": {
            "title1": "Ortalıklık İşlevleri",
            "header1": "Kolay uyum",
            "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
            "placeholder": "Danışmana bir soru sor",
            "example": "Örnek gör"
          },
          "functions-segment": {
            "header": "Sıkı uyum",
            "title": "Lütfen işinizi, şirketinizin boyutunu ve Indacoin ile işbirliğinize gösterdiğiniz ilgiyi açıklayın. Daha fazla bilgi almak için pr@indacoin.com ile iletişime geçin. "
          },
          "partnership-segment": {
            "block1": {
              "title1": "Mantık",
              "text1": "Kullanıcının sınırını kontrol edin (Henüz mevcut değilse bir tane oluşturacaktır.)",
              "text2": "Daha fazla işlem yap",
              "text3": "Kullanıcıyı Indacoin URL'ye yönlendirin, kullanıcı ödeme yapacak ve doğrulayacak",
              "text4": "Sonuna kadar gelecek, Doğrulama işlemini bitirmek için ona URL göstermelisiniz",
              "text5": "URL’nizde başarısızlık veya işlem başarısı durumunda geri arama",
              "text6": "İmzanız olduğunu doğrulamak için - 'gw-partner', 'gw-nonce', 'gw-sign' başlıklarına göndermeniz gerekir. Header gw-sign, partnerAdı + \"_\" + nonce'dan HMAC sha256 olarak oluşturulur.",
              "text7": "URL, tıpkı işlem dışı kimliği ve Double Base64 kodlaması yerine API'lar gibi için imza",
              "text8": "Ödeme formu örneği",
              "text9": "Şu an için minimum miktar 50 USD / EURO, maksimum 500 USD / EURO",
              "text10": "İşlemin olası durumları:",
              "title2": "Yöntemler",
              "text11": "İşlem oluştur",
              "text12": " - Yeni işlem oluştur",
              "text13": "JSondaki parametreler:",
              "text14": "kullanıcı sınırı",
              "text15": "JSondaki parametreler:",
              "text16": "Fiyat Al",
              "text17": "- Kullanıcının alacağı tutar",
              "text18": "GET'deki parametreler:",
              "text19": "işlem",
              "text20": "- Son işlemlerle ilgili bilgi al",
              "text21": "JSondaki parametreler:",
              "text22": "Tüm parametreler isteğe bağlıdır",
              "text23": "işlem bilgileri",
              "text24": "- İşlemin bilgilerini al",
              "text25": "JSondaki parametreler:",
              "text26": "PHP'de istek oluşturma örneği",
              "text27": "URL için istek örneği",
              "text28": "İsteği oluşturmak için önceki isteğinizden işlem kimliği almanız gerekir",
              "text29": "Mevcut kullanıcı için limit kontrol ediliyor",
              "text30": "Bundan önce, bu somut kullanıcı için mevcut olan limiti kontrol etmek için exgw_getUserlimits bir istekte bulunmanızı öneririz (limit müşterinin bulunduğu bölgeye ve platformumuzdaki alımlarının geçmişine bağlıdır).",
              "text31": "Indacoin'de işlem oluşturuluyor",
              "text32": "Bir kullanıcı bir ödeme yaptığında, bu ödemeyi yapmak için ID-işlemi almanız gerekir. Bunu yapmak için bir istek exgw_createTransaction yapın.",
              "text33": "Indacoin'e yönlendiriliyor",
              "text34": "Müşteriyi ödemenin sayfasına yönlendirmek için, bir URL oluşturmak için kimlik işlemini kullanmanız gerekir.",
              "text35": "Müşteri Indacoin'de işleniyor",
              "text36": "Daha sonra müşteri, ödemeyi yaptığı, doğrulandığı ve satın alma onayı aldığı platformumuza gider.",
              "text37": "Durumu geri aramayla ya da API ile kontrol et",
              "text38": "Müşteri ödeme yaptıktan sonra, işlemin durumundaki her değişiklik hakkında bir geri bildirim alırsınız."
            }
          },
          "secret-segment": {
            "title": "Daha fazla bilgi için yöneticimize {{{company}}}'den bilgi verin"
          },
          "statuses-segment": {
            "title": "Ayrıca, durumu buradan kontrol edebilirsiniz:"
          }
        },
        "limits-warning": { // TODO: translate
          "message": "Miktar {{{limitsMin}}} {{{limitsCur}}} ve {{{limitsMax}}} {{{limitsCur}}} arasında olmalıdır",
          "message-only-min": "Miktar {{{limitsMin}}} {{{limitsCur}}} dan olmalıdır"
        },
        "modals": {
          "cookie-modal": {
              "text": "<span class=\"text\">Bu web sitesi, web sitemizde en iyi deneyimi yaşamanızı sağlamak için çerezleri kullanır.</span> {{{link}}}",
              "accept-button": "Anladım",
              "link": "Daha fazla bilgi edin"
            },
          "success-video": {
              "text": "Teşekkür ederim! Şimdi doğrulama sürecini başlattık, bu da biraz zaman alabilir. Doğrulama tamamlandığında, kripto paraları size teslim edeceğiz!"
          },
          "accept-agreement": {
              "text": "Lütfen Kullanım Koşullarını ve Gizlilik Politikasını okuduğunuzu ve kabul ettiğinizi belirtin"
          },
          "price-changed": {
              "header": "Fiyat değişti",
              "agree": "Kabul et",
              "cancel": "İptal et"
          },
          "neoAttention": " NEO, özelliklerinden dolayı sadece tamsayı değeriyle satın alınabilir.",
          "login": {
            "floodDetect": "Flood tespit edildi",
            "resetSuccess": "Şifre sıfırlama başarılı. Lütfen e-postanızı kontrol edin",
            "wrongEmail": "Bu e-posta henüz kayıtlı değil",
            "unknownError": "Sunucu hatası. Lütfen tekrar deneyin",
            "alreadyLoggedIn": "zaten giriş yaptınız",
            "badLogin": "Yanlış giriş veya şifre",
            "badCaptcha": "Yanlış CAPTCHA",
            "googleAuthenticator": "Lütfen google kimlik doğrulama kodunu girin"
          },
          "registration": {
            "unknownError": "Bilinmeyen hata",
            "floodDetect": "Flood tespit edildi",
            "success": "Kayıt başarılı. Lütfen emailinizi kontrol edin",
            "badLogin": "Yanlış giriş veya şifre.",
            "badCaptcha": "Yanlış CAPTCHA",
      
            "empty": "Tüm alanlar tamamlanmadı. Lütfen hepsini doldurun",
            "repeatedRequest": "Kayıt Onayı e-posta adresinize gönderilmiştir. Lütfen e-postayı aldığınızdan emin olun. Ayrıca bir spam klasörünü de kontrol edin",
            "badEmail": "Yanlış veya geçici e-posta adresini kullandınız",
            "blockedDomain": "Kayıt olmak için lütfen farklı bir e-posta adresi deneyin"
          }
        },
        "stop-sidebar": {
          "title": "Değerli Müşteriler!",
          "text": "Şu an bütün kripto paralar satılmış durumda. Sabrınız ve anlayışınız için teşekkür ederiz.",
          "time": "Şubat 27, 6 pm UTC"
        },
        "noResults": "Sonuç bulunamadı",
        "locale": {
          "title": "Lütfen ülkenizi seçiniz"
        }
      }