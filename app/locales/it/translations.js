export default {
  'lang': 'it',
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    } 
  },
  "recordVideo": {
    "send": "Inviare",
    "stop": "Stop",
    "record": "Disco"
  },
  "links": {
    "terms-of-use": {
      "text": "Accetto i {{{terms}}} e {{{policy}}}",
      "link-1": "Condizioni d'uso",
      "link-2": "l'Informativa sulla privacy"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "In attesa del tuo pagamento",
      "status-2": "Scambio",
      "status-2-sub": "Stiamo cercando la tariffa migliore per te",
      "status-3": "Invio al tuo portafoglio",
      "transaction-completed": "Congratulazioni, la tua transazione è stata completata!",
      "rate": "Tasso di cambio 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Hai inviato:",
      "toAddress": "a {{{cryptoAdress}}}",
      "receiveText": "Riceverai:"
    },
    "changeCrypto": {
      "send": "Invia {{{amount}}} BTC all'indirizzo sottostante",
      "confirmation": "Dopo due conferme di bitcoin will {{{amount}}} {{{shortName}}} verrà inviato al tuo indirizzo {{{wallet}}} ...",
      "copy": "Copia l'indirizzo",
      "copied": "Copiato negli appunti"
    },
    "order": "Ordine",
    "status": {
      "title": "Stato",
      "completed": "Richiesta completata"
    },
    "payAmount": {
      "title": "Pagato"
    },
    "check": "Verificare lo stato dell’operazione su blockexplorer.net",
    "resend-phone-code": "Non hai ricevuto una chiamata?",
    "sms-code": "Si prega di rispondere al telefono e inserire il codice a 4 cifre qui",
    "registration": "Vai alla pagina di registrazione",

    "internalAccount-1": "Controlla il tuo portafoglio Indacoin con",
    "internalAccount-link": "login",
    "internalAccount-2": "",
    "your-phone": "votre téléphone {{{phone}}}",
    "many-requests": "Scusate. A molte richieste per cambiare telefono"
  },
  "app":{
    "detect_lang":{
      "title":"Indicare la vostra lingua",
      "sub":"Attendere prego..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Acquista e scambia {{{longName}}} all'istante e 100 altre criptovalute al miglior prezzo in {{{country}}}",
    "title":"Acquistate la criptovaluta con carta di credito o di debito - Indacoin",
    "description":"Acquista e scambia istantaneamente qualsiasi criptomoneta [cryptocurrency], Ethereum, Litecoin, Ripple e altre 100 valute digitali per EUR o USD.",
    "description-bitcoin":"Acquista e scambia immediatamente qualsiasi criptovaluta: Bitcoin, Ethereum, Litecoin, Ripple e altre 100 valute digitali per EUR o USD",
    "description-ethereum":"Acquista e scambia immediatamente qualsiasi criptovaluta: Ethereum, Bitcoin, Litecoin, Ripple e altre 100 valute digitali per EUR o USD",
    "description-litecoin":"Acquista e scambia istantaneamente qualsiasi criptovaluta: Litecoin, Bitcoin, Ethereum, Ripple e altre 100 valute digitali per EUR o USD",
    "description-ripple":"Acquista e scambia istantaneamente qualsiasi criptovaluta: Ripple, Bitcoin, Ethereum, Litecoin e altre 100 valute digitali per EUR o USD",

    "description2": "Acquista e scambia immediatamente qualsiasi crittografia: Bitcoin, Ethereum, Litecoin, Ripple e altre 100 valute digitali per EUR o USD",
    "description3": "Acquista e scambia istantaneamente qualsiasi criptovaluta: Bitcoin, Ethereum, Litecoin, Ripple e altre 100 valute digitali con Visa e Mastercard",
    "buy": "Acquista",
    "subTitle": "con carta di credito o di debito",
    "more": "In più di 100 paesi tra cui",
    //"header": "Scambia Bitcoin in <span class='sub'>{{{name}}}</span>",
    "header": "Scambia immediatamente da <span class='sub'>BTC</span> a <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "ACQUISTA ISTANTANEAMENTE CRIPTOVALUTA",
    "header-alt": "Scambia Cryptocurrency istantaneamente",
    "sub": "Indacoin fornisce un modo semplice per acquistare Bitcoin e oltre 100 criptovalute con Visa e Mastercard",
    "sub-alt": "Indacoin fornisce un modo semplice per scambiare bitcoin con qualsiasi di 100+ diverse criptovalute",
    "observe": "Ulteriori informazioni sulle criptovalute, sono disponibili qui sotto"
  },
  "navbar": {
    "main":"Pagina principale",
    "btc": "Acquista Bitcoin",
    "eth": "Acquista Criptovaluta",
    "affiliate": "Partnership",
    "news": "Novità",
    "faq": "FAQ",
    "login": "Accedi",
    "registration": "Registrati",
    "platform": "Prodotti",

    "logout": "Disconnettersi",
    "buy-instantly": "Acquista cripto all'istante",
    "trading-platform": "Piattaforma di trading",
    "mobile-wallet": "Portafoglio mobile",
    "payment-processing": "API di elaborazione dei pagamenti"
  },
  "currency-table": {
    "rank": "Rango",
    "coin": "Valuta",
    "market": "Valore di mercato",
    "price": "Prezzo",
    "volume": "Volume 24h",
    "change": "Crescita 24h",
    "available-supply": "Fornitura disponibile",
    "search-cryptocurrency": "Cerca criptovaluta",

    "change-1h": "1H Modificare",
    "change-24h": "1D Modificare",
    "change-7d": "1W Modificare"
  },
  "buy-blocks":{
    "give":"Hai ceduto",
    "get":"Hai ricevuto"
  },
  "exchange-form": {
    "title": "Acquisto istantaneo di criptovaluta",
    "submit": "Cambiare",
    "give": "Dai",
    "get": "Ricevi",
    "search": "Ricerca",
    "error-page": "Ci scusiamo, ma l'acquisto non è al momento disponibile"
  },
  "pagination": {
    "page": "Pagina",
    "next": "Avanti",
    "prev": "Indietro",
    "first": "All’inizio"
  },
  "sidebar":{
    "accept":{
      "title":"Accettiamo carte di credito",
      "button":"Acquistare criptovaluta"
    },
    "youtube":{
      "title":"Chi siamo",
      "play":"Avviare"
    },
    "orders":{
      "title": "Gestione degli ordini",
      "order": "Ordine #",
      "confirm": "Codice di conferma",
      "check": "Verificare l’ordine",
      "new": "Creare un nuovo ordine",
      "or": "o"
    },
    "social":{
      "title":"Siamo sui social"
    }
  },
  "wiki":{
    "etymology":"Etimologia"
  },
  "loading": {
    "title": "Caricamento",
    "sub": "Attendere prego...",
    "text": "Caricamento"
  },
  "error": {
    "title": "Errore",
    "sub": "Qualcosa è andato storto, non abbiate paura",
    "text": "Ci siamo imbattuti in un errore sconosciuto e stiamo già lavorando per risolverlo. Riprova più tardi.",
    "home": "Principale",
    "mainPageError1": "Sfortunatamente, le informazioni sulle cripto-valute disponibili non sono disponibili",
    "mainPageError2": "Prova a visitare il sito un po 'più tardi",
    "page-404": "Ci scusiamo, pagina non trovata",
    "page-error": "Ci scusiamo, errore si è verificato"
  },
  "features":{
    "title":"Cosa puoi<br><span class=\"offset\">fare su <span class=\"blue\">Indacoin</span>?</span>",
    "column1":{
      "title":"Acquista criptovaluta",
      "text":"Su Indacoin puoi acquistare più di 100 diverse criptovalute istantaneamente con una carta di credito o di debito"
    },
    "column2":{
      "title":"Ulteriori informazioni sulla criptovaluta",
      "text":"Le nostre semplici guide ti aiuteranno a comprendere meglio i principi fondamentali di ogni altcoin"
    },
    "column3":{
      "title":"Prendere decisioni di investimento",
      "text":"Gli strumenti analitici potenti consentono di catturare il momento migliore per le entrate e le uscite"
    },
    "column4":{
      "title":"Acquista bitcoin senza registrazione",
      "text":"I Bitcoins verranno inviati all'indirizzo indicato dal momento in cui viene completato il pagamento"
    },
    "column5":{
      "title":"Condividi i tuoi pensieri",
      "text":"Unisciti alla discussione sulle ultime tendenze della criptomoneta con altri importanti investitori"
    },
    "column6":{
      "title":"Deposita il denaro in un unico luogo",
      "text":"È possibile depositare e gestire più di 100 valute digitali in un portafoglio Indacoin"
    }
  },
  "mockup":{
    "title":"PORTAFOGLI MOBILI",
    "text":"Il nostro piccolo e semplice portafoglio Indacoin funziona sul tuo dispositivo Android o iPhone, oltre che sul tuo browser web. Ora inviare criptovaluta ad un telefono di amici è facile come mandare un sms"
  },
  "footer":{
    "column1":{
      "title":"Acquistare",
      "link1":"Acquistare Bitcoin",
      "link2":"Acquistare Ethereum",
      "link3":"Acquistare Ripple",
      "link4":"Acquistare Bitcoin Cash",
      "link5":"Acquistare Litecoin",
      "link6":"Acquistare Dash"
    },
    "column2":{
      "title":"Informazioni",
      "link1":"Chi siamo",
      "link2":"Domande",
      "link3":"Condizioni d’utilizzo",
      "link4":"Istruzioni"
    },
    "column3":{
      "title":"Strumenti",
      "link1":"API",
      "link2":"Cambia regione",
      "link3":"App mobile"
    },
    "column4":{
      "title":"Contatti"
    }
  },
  "country":{
    "AF":{
      "name":"Afghanistan"
    },
    "AX":{
      "name":"Åland Islands"
    },
    "AL":{
      "name":"Albania"
    },
    "DZ":{
      "name":"Algeria"
    },
    "AS":{
      "name":"American Samoa"
    },
    "AD":{
      "name":"AndorrA"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguilla"
    },
    "AQ":{
      "name":"Antarctica"
    },
    "AG":{
      "name":"Antigua e Barbuda"
    },
    "AR":{
      "name":"Argentina"
    },
    "AM":{
      "name":"Armenia"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Australia"
    },
    "AT":{
      "name":"Austria"
    },
    "AZ":{
      "name":"Azerbaijan"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Bahrain"
    },
    "BD":{
      "name":"Bangladesh"
    },
    "BB":{
      "name":"Barbados"
    },
    "BY":{
      "name":"Bielorussia"
    },
    "BE":{
      "name":"Belgio"
    },
    "BZ":{
      "name":"Belize"
    },
    "BJ":{
      "name":"Benin"
    },
    "BM":{
      "name":"Bermuda"
    },
    "BT":{
      "name":"Bhutan"
    },
    "BO":{
      "name":"Bolivia"
    },
    "BA":{
      "name":"Bosnia ed Erzegovina"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Isola Bouvet"
    },
    "BR":{
      "name":"Brasile"
    },
    "IO":{
      "name":"Territorio dell’oceano indiano britannico"
    },
    "BN":{
      "name":"Brunei Darussalam"
    },
    "BG":{
      "name":"Bulgaria"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Cambogia"
    },
    "CM":{
      "name":"Cameroon"
    },
    "CA":{
      "name":"Canada"
    },
    "CV":{
      "name":"Capo verde"
    },
    "KY":{
      "name":"Isole Cayman"
    },
    "CF":{
      "name":"Repubblica Centrafricana"
    },
    "TD":{
      "name":"Chad"
    },
    "CL":{
      "name":"Cile"
    },
    "CN":{
      "name":"Cina"
    },
    "CX":{
      "name":"Isola di Natale"
    },
    "CC":{
      "name":"Isole Cococs (Keeling)"
    },
    "CO":{
      "name":"Colombia"
    },
    "KM":{
      "name":"Comoros"
    },
    "CG":{
      "name":"Congo"
    },
    "CD":{
      "name":"Repubblica democratica del Congo"
    },
    "CK":{
      "name":"Isole Cook"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Costa d’Avorio"
    },
    "HR":{
      "name":"Croazia"
    },
    "CU":{
      "name":"Cuba"
    },
    "CY":{
      "name":"Cipro"
    },
    "CZ":{
      "name":"Repubblica Ceca"
    },
    "DK":{
      "name":"Danimarca"
    },
    "DJ":{
      "name":"Gibuti"
    },
    "DM":{
      "name":"Dominica"
    },
    "DO":{
      "name":"Repubblica Domenicana"
    },
    "EC":{
      "name":"Ecuador"
    },
    "EG":{
      "name":"Egitto"
    },
    "SV":{
      "name":"El Salvador"
    },
    "GQ":{
      "name":"Guinea equatoriale"
    },
    "ER":{
      "name":"Eritrea"
    },
    "EE":{
      "name":"Estonia"
    },
    "ET":{
      "name":"Etiopia"
    },
    "FK":{
      "name":"Isole Falkland (Malvinas)"
    },
    "FO":{
      "name":"Isole Faroe"
    },
    "FJ":{
      "name":"Fiji"
    },
    "FI":{
      "name":"Finlandia"
    },
    "FR":{
      "name":"Francia"
    },
    "GF":{
      "name":"Guiana francese"
    },
    "PF":{
      "name":"Polinesia francese"
    },
    "TF":{
      "name":"Territori del sud della Francia"
    },
    "GA":{
      "name":"Gabon"
    },
    "GM":{
      "name":"Gambia"
    },
    "GE":{
      "name":"Georgia"
    },
    "DE":{
      "name":"Germania"
    },
    "GH":{
      "name":"Ghana"
    },
    "GI":{
      "name":"Gibilterra"
    },
    "GR":{
      "name":"Grecia"
    },
    "GL":{
      "name":"Groenlandia"
    },
    "GD":{
      "name":"Grenada"
    },
    "GP":{
      "name":"Guadalupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernsey"
    },
    "GN":{
      "name":"Guinea"
    },
    "GW":{
      "name":"Guinea-Bissau"
    },
    "GY":{
      "name":"Guyana"
    },
    "HT":{
      "name":"Haiti"
    },
    "HM":{
      "name":"Heard e Mcdonald Islands"
    },
    "VA":{
      "name":"Città del Vaticano (Stato del Vaticano)"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hong Kong"
    },
    "HU":{
      "name":"Ungheria"
    },
    "IS":{
      "name":"Islanda"
    },
    "IN":{
      "name":"India"
    },
    "ID":{
      "name":"Indonesia"
    },
    "IR":{
      "name":"Repubblica Islamica dell’Iran"
    },
    "IQ":{
      "name":"Iraq"
    },
    "IE":{
      "name":"Irlanda"
    },
    "IM":{
      "name":"Isola di Man"
    },
    "IL":{
      "name":"Israele"
    },
    "IT":{
      "name":"Italia"
    },
    "JM":{
      "name":"Jamaica"
    },
    "JP":{
      "name":"Giappone"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Giordania"
    },
    "KZ":{
      "name":"Kazakhstan"
    },
    "KE":{
      "name":"Kenya"
    },
    "KI":{
      "name":"Kiribati"
    },
    "KP":{
      "name":"Repubblica Popolare Democratica di Corea"
    },
    "KR":{
      "name":"Repubblica di Corea"
    },
    "KW":{
      "name":"Kuwait"
    },
    "KG":{
      "name":"Kyrgyzstan"
    },
    "LA":{
      "name":"Repubblica Popolare democratica di Lao"
    },
    "LV":{
      "name":"Lettonia"
    },
    "LB":{
      "name":"Libano"
    },
    "LS":{
      "name":"Lesotho"
    },
    "LR":{
      "name":"Liberia"
    },
    "LY":{
      "name":"Stato della Libia"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Lituania"
    },
    "LU":{
      "name":"Lussemburgo"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Macedonia"
    },
    "MG":{
      "name":"Madagascar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malaysia"
    },
    "MV":{
      "name":"Maldives"
    },
    "ML":{
      "name":"Mali"
    },
    "MT":{
      "name":"Malta"
    },
    "MH":{
      "name":"Isole Marhsall"
    },
    "MQ":{
      "name":"Martinica"
    },
    "MR":{
      "name":"Mauritania"
    },
    "MU":{
      "name":"Mauritius"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"Messico"
    },
    "FM":{
      "name":"Stati Federati della Micronesia"
    },
    "MD":{
      "name":"Repubblica di Moldavia"
    },
    "MC":{
      "name":"Monaco"
    },
    "MN":{
      "name":"Mongolia"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Marocco"
    },
    "MZ":{
      "name":"Mozambico"
    },
    "MM":{
      "name":"Myanmar"
    },
    "NA":{
      "name":"Namibia"
    },
    "NR":{
      "name":"Nauru"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Olanda"
    },
    "AN":{
      "name":"Antille Olandesi"
    },
    "NC":{
      "name":"Nuova Caledonia"
    },
    "NZ":{
      "name":"Nuova Zelanda"
    },
    "NI":{
      "name":"Nicaragua"
    },
    "NE":{
      "name":"Niger"
    },
    "NG":{
      "name":"Nigeria"
    },
    "NU":{
      "name":"Niue"
    },
    "NF":{
      "name":"Isole Norfolk"
    },
    "MP":{
      "name":"Isole Marianna settentrionali"
    },
    "NO":{
      "name":"Norvegia"
    },
    "OM":{
      "name":"Oman"
    },
    "PK":{
      "name":"Pakistan"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Territorio occupato di Palestina"
    },
    "PA":{
      "name":"Panama"
    },
    "PG":{
      "name":"Papa Nuova Guinea"
    },
    "PY":{
      "name":"Paraguay"
    },
    "PE":{
      "name":"Peru"
    },
    "PH":{
      "name":"Filippine"
    },
    "PN":{
      "name":"Pitcairn"
    },
    "PL":{
      "name":"Polonia"
    },
    "PT":{
      "name":"Portogallo"
    },
    "PR":{
      "name":"Porto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"Reunion"
    },
    "RO":{
      "name":"Romania"
    },
    "RU":{
      "name":"Federazione Russa"
    },
    "RW":{
      "name":"RUANDA"
    },
    "SH":{
      "name":"Sant’Elenca"
    },
    "KN":{
      "name":"Saint Kitts and Nevis"
    },
    "LC":{
      "name":"Saint Lucia"
    },
    "PM":{
      "name":"Saint Pierre and Miquelon"
    },
    "VC":{
      "name":"Saint Vincent and the Grenadines"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"San Marino"
    },
    "ST":{
      "name":"SArabia Sauditaao Tome and Principe"
    },
    "SA":{
      "name":"Saudi Arabia"
    },
    "SN":{
      "name":"Senegal"
    },
    "CS":{
      "name":"Serbia e Montenegro"
    },
    "SC":{
      "name":"Seychelles"
    },
    "SL":{
      "name":"Sierra Leone"
    },
    "SG":{
      "name":"Singapore"
    },
    "SK":{
      "name":"Slovacchia"
    },
    "SI":{
      "name":"Slovenia"
    },
    "SB":{
      "name":"Isole Solomon"
    },
    "SO":{
      "name":"Somalia"
    },
    "ZA":{
      "name":"Sud Africa"
    },
    "GS":{
      "name":"Georgia del Sud e Isole Sandwich Australi"
    },
    "ES":{
      "name":"Spagna"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Sudan"
    },
    "SR":{
      "name":"Suriname"
    },
    "SJ":{
      "name":"Svalbard e Jan Mayen"
    },
    "SZ":{
      "name":"Swaziland"
    },
    "SE":{
      "name":"Svezia"
    },
    "CH":{
      "name":"Svizzera"
    },
    "SY":{
      "name":"Siria"
    },
    "TW":{
      "name":"Taiwan, Provincia della Cina"
    },
    "TJ":{
      "name":"Tajikistan"
    },
    "TZ":{
      "name":"Repubblica Unita di Tanzania"
    },
    "TH":{
      "name":"Tailandia"
    },
    "TL":{
      "name":"Timor-Leste"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad e Tobago"
    },
    "TN":{
      "name":"Tunisia"
    },
    "TR":{
      "name":"Turchia"
    },
    "TM":{
      "name":"Turkmenistan"
    },
    "TC":{
      "name":"Isoole Turks e Caicos"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Uganda"
    },
    "UA":{
      "name":"Ucraina"
    },
    "AE":{
      "name":"Emirati Arabi Uniti"
    },
    "GB":{
      "name":"Regno Unito"
    },
    "US":{
      "name":"USA"
    },
    "UM":{
      "name":"Isole Minori Esterne degli Stati Uniti d'America"
    },
    "UY":{
      "name":"Uruguay"
    },
    "UZ":{
      "name":"Uzbekistan"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Vietnam"
    },
    "VG":{
      "name":"Isole vergini, Britanniche"
    },
    "VI":{
      "name":"Isole Vergini, USA"
    },
    "WF":{
      "name":"Wallis e Futuna"
    },
    "EH":{
      "name":"Sahaera Occidentale"
    },
    "YE":{
      "name":"Yemen"
    },
    "ZM":{
      "name":"Zambia"
    },
    "ZW":{
      "name":"Zimbabwe"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Acquistare",
  "sell":"Vendere",
  "weWillCallYou":"Se utilizzi questa carta per la prima volta, occorre inserire i 2 codici segreti, quello dell’estratto bancario e quello fornito con la telefonata automatica.",
  "exchangerStatuses":{
        "TimeOut": "Il tempo della richiesta è scaduto",
        "Error": "In caso di errori, vi invitiamo a scrivere al servizio assistenza all’indirizzo support@indacoin.com",
        "WaitingForAccountCreation": "In attesa della creazione dell'account di Indacoin (ti abbiamo inviato una mail di registrazione)",
        "CashinWaiting": "Il tuo pagamento sta per essere elaborato",
        "BillWaiting": "In attesa di pagamento della fattura emessa, da parte del cliente",
        "Processing": "La richiesta sta per essere elaborata",
        "MoneySend": "Fondi inviati",
        "Completed": "Richiesta completata",
        "Verifying": "Verifica",
        "Declined": "Rifiutato",
        "cardDeclinedNoFull3ds": "La tua carta è stata rifiutata perché la tua banca non supporta l’opzione del 3ds per questa carta, rivolgiti al servizio assistenza della tua banca",
        "cardDeclined": "La tua carta è stata rifiutata, in caso di domande rivolgiti al servizio assistenza support@indacoin.com",
        "Non3DS": "Purtroppo il tuo pagamento è stato rifiutato. Assicurati con la tua banca, che utilizzi una carta con 3D-secure (Verificata da codice di sicurezza Visa o MasterCard), il che significa immettere un codice segreto per ogni pagamento su Internet, che di solito arriva sul tuo telefono con un sms."
  },
  "exchanger_fields_ok":"Clicca per acquistare bitcoins",
  "exchanger_fields_error":"Errore di compilazione Verifica tutti i campi",
  "bankRejected":"La transazione è stata rifiutata dalla tua banca. Contatta la banca e ripeti il pagamento con la carta",
  "wrongAuthCode":"Hai inserito un codice di autorizzazione errato, rivolgiti al servizio assistenza all’indirizzo support@indacoin.com",
  "wrongSMSAuthCode":"Hai inserito il codice errato ricevuto sul telefono",
  "getCouponInfo":{
    "indo":"Questo codice non è valido",
    "exetuted":"Questo coupon è già stato utilizzato",
    "freeRate":"Coupon di sconto attivato"
  },
  "ip":"Il tuo IP è stato temporaneamente bloccato",
  "priceChanged":"I prezzi sono cambiati. Ok? Hai ricevuto ",
  "exchange_go":"– Cambiare",
  "exchange_buy_btc":"– Acquistare bitcoin",
  "month":"Gen Feb Mar Apr Mag Giu Lug Ago Sett Ott Nov Dic",
  "discount":"Sconto",
  "cash":{
    "equivalent":" Equivalente ",
    "card3DS":"Accettiamo per il pagamento soltanto carte 3D- Secure (Verificate tramite codice di sicurezza Visa o MasterCard)",
    "cashIn":{
      "1": "Carte di pagamento (USD)",
      "2": "Terminali per il pagamento",
      "3": "QIWI",
      "4": "Bashkomsnabbank",
      "5": "Terminali Novoplat",
      "6": "Terminali Eleksnet",
      "7": "Carte di pagamento (USD)",
      "8": "Al’fa click",
      "9": "Banca internazionale. Trasferimento",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex. Denaro",
      "21": "Eleksnet",
      "22": "Kassira.net",
      "23": "Mobil element",
      "24": "Svjasnoj",
      "25": "Evroset’",
      "26": "PerfectMoney",
      "27": "Indacoin - interno",
      "28":	"CouponBonus",
      "29": "Yandex. Denaro",
      "30": "OKPay",
      "31": "Programma di partnership",
      "33": "Payza",
      "35": "Codice BTC-E",
      "36": "Carte di pagamento (USD)",
      "37": "Carte di pagamento (USD)",
      "39": "Trasferimento bancario internazionale",
      "40": "Yandex. Denaro",
      "42": "QIWI(Manuale)",
      "43": "UnionPay",
      "44": "QIWI(Automatico)",
      "45": "Pagamento con accredito sul telefono",
      "49": "LibrexCoin",
      "50": "Carte di pagamento (EURO)",
      "51": "QIWI(Automatico)",
      "52": "Pagamento con accredito sul telefono",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Inserisci l'indirizzo crittografico corretto",
      "country": "Paese non valido",
      "alfaclick": "Login non valido",
      "pan": "Numero di carta non valido",
      "cardholdername": "Dati non validi",
      "fio": "Dati non validi",
      "bday": "Data di nascita errata",
      "city": "Dati non validi",
      "address": "Indirizzo non valido",
      "ypurseid": "Numero di portafoglio non valido",
      "wpurseid": "Numero di portafoglio non valido",
      "pmpurseid": "Numero di account non valido",
      "payeer": "Numero di account non valido",
      "okpayid": "Numero di portafoglio non valido",
      "purse": "Numero di portafoglio non valido",
      "phone": "Telefono non valido",
      "btcaddress": "Indirizzo non valido",
      "ltcaddress": "Indirizzo non valido",
      "lxcaddress": "Indirizzo non valido",
      "ethaddress": "Indirizzo non valido",
      "properties": "Requisiti non validi",
      "email": "E-mail non valida",
      "card_number": "Questa scheda non è supportata, controllare il numero",
      "inc_card_number": "Numero della carta sbagliato",
      "inn": "Numero errato",
      "poms": "Numero errato",
      "snils": "Numero errato",
      "cc_expr": "Data sbagliata",
      "cc_name": "Nome sbagliato",
      "cc_cvv": "CVV sbagliato"
    }
  },
  "change": {
    "wallet": "Crea nuovo portafoglio / Ho già un account Indacoin",
    //"accept": "Accetto l'Accordo per gli utenti e la politica AML",
    "accept": "Accetto i Termini d'uso e l'Informativa sulla privacy",
    "coupone": "Ho un coupon",
    "watchVideo": "Guarda il video",
    "seeInstructions": "Aiuto",
    "step1": {
      "title": "Fai un ordine",
      "header": "Inserisci le informazioni per il pagamento",
      "subheader": "Informaci su cosa desiderai acquistare"
    },
    "step2": {
      "title": "Informazioni per il pagamento",
      "header": "Inserisci le informazioni sulla tua carta di credito",
      "subheader": "Accettiamo soltanto carte Visa o Mastercard",
      "card": {
        "header": "Dove posso trovare questo codice?",
        "text": "E’ possibile trovare il CVV sulla parte posteriore della carta. Questo codice si compone di 3 cifre"
      }
    },
    "step3": {
      "title": "Informazioni di contatto",
      "header": "Inserisci le informazioni di contatto",
      "subheader": "Dateci la possibilità di conoscere meglio i nostri clienti"
    },
    "step4": {
      "title": "Verifica",
      "header": "Inserisci il tuo codice dalla pagina di pagamento",
      "subheader": "Le tue informazioni di pagamento"
    },
    "step5": {
      "title": "Informazioni sull’ordine"
    },
    "step6": {
      "title": "Registrazione video",
      "header": "Mostra il tuo passaporto e il tuo volto",
      "description": "Dimostraci che possiamo fare affari con te",
      "startRecord": "Premere il pulsante Start per avviare la registrazione.",
      "stopRecord": "Premere Stop per terminare la registrazione."
    },
    
    "nextButton": "Prochain",
    "previousButton": "Précédent",
    "passKYCVerification": "Verifica KYC",
    "soccer-legends": "Accetto che i miei dati personali vengano trasferiti e trattati alla società Soccer Legends Limited",
    "withdrawalAmount": "Importo da accreditare sul conto",
  },
  "form": {
    "tag": "Tag {{{currency}}}",
    "loading": "Attendere il caricamento prego",
    "youGive": "Tu dai",
    "youTake": "Tu prendi",
    "email": "E-mail",
    "password": "Parola d'ordine",
    //"cryptoAdress": "{{{name}}} Portafoglio Indirizzo",
    "cryptoAdress": "Indirizzo del portafoglio crittografico",
    "externalTransaction": "ID transazione",
    "coupone": "Ho un buono sconto",
    "couponeCode": "Codice coupon",
    "cardNumber": "Numero di carta",
    "month": "MM",
    "year": "YY",
    "name": "Nome",
    "fullName": "Nome e cognome",
    "mobilePhone": "Cellulare",
    "birth": "La tua data di nascita nel formato MM.DD.YYYY",
    "videoVerification": "Verifica video",
    "verification": "Verifica",
    "status": "Controllare lo stato",
    "login": { // TODO: translate
      "captcha": "Captcha",
      "reset": "Reset",
      "forgot": "Ha dimenticato la password",
      "signIn": "registrati"
    },
    "registration": { // TODO: translate
        "create": "Creare"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin è stato presentato al mondo nel 2009 e nel 2017 rimane la principale criptovaluta del mondo. Essendo la prima moneta del suo genere, è riuscito a cambiare la visione del denaro come lo sapevamo. Ormai, la capitalizzazione di mercato di Bitcoin raggiunge oltre $ 70 miliardi, superando giganti come Tesla, General Motors e FedEx.",
    "paragraph2": "Cos'è una valuta digitale o criptovaluta? La criptovaluta è una risorsa digitale, che funziona come mezzo di scambio utilizzando la crittografia per proteggere le transazioni e controllare la creazione di unità aggiuntive della valuta. Non ha forma fisica ed è decentralizzato rispetto ai soliti sistemi bancari.",
    "paragraph3": "Da dove viene Bitcoin? Sebbene il mondo abbia familiarità con il nome del creatore di Bitcoin, \"Satoshi Nakamoto\" è molto probabilmente un alias per un programmatore o anche un gruppo di tali. Fondamentalmente, Bitcoin proviene da 31.000 linee di codice create da \"Satoshi\" ed è controllato solo da software.",
    "paragraph4": "Come funziona Bitcoin? Le transazioni avvengono tra i portafogli Bitcoin e sono trasparenti. Nella blockchain, chiunque può controllare l'origine e il ciclo di vita di ogni \"moneta\". Tuttavia, non esiste una \"moneta\". Bitcoin non ha una forma fisica e non ha forma virtuale, solo la storia delle transazioni.",
    "paragraph5": "In che modo Bitcoin è diverso dal denaro normale? - Valuta mondiale: nessun governo ha il controllo su Bitcoin. È basato solo sulla matematica - Trasparenza: tutte le transazioni Bitcoin possono essere viste su BlockChain. - Emissione limitata: il numero totale di Bitcoin che possono essere estratti è 21M. Più di 16 milioni di Bitcoin sono in circolazione in questo momento. Più diventa popolare, più è costoso. - Anonimato: puoi usare i tuoi Bitcoin in qualsiasi momento e in qualsiasi quantità. Questa è una vera scoperta rispetto al trasferimento di denaro dal tuo conto bancario, quando è necessario spiegare la destinazione del pagamento e l'origine dei fondi. - Velocità: la tua banca impiegherà del tempo per trasferire denaro, soprattutto quando si tratta di valori importanti. Con Bitcoin, la routine di invio e ricezione richiederà pochi secondi.",
    "paragraph6": "Come si verifica la transazione con Bitcoin? Ogni transazione ha 3 dimensioni: input, quantità e output. Input - l'origine del Bitcoin utilizzato nella transazione. Ci dice dove l'ha preso l'attuale proprietario. Importo: il numero di \"monete\" utilizzate. Output - l'indirizzo del destinatario del denaro.",
    "paragraph7": "Cos'è un portafoglio Bitcoin? Un portafoglio Bitcoin è un programma che ti permette di gestire Bitcoin: ricevere e inviare loro, controllare il saldo e il tasso di Bitcoin. Sebbene non ci sia una \"moneta\" in tali portafogli, essa contiene chiavi digitali, utilizzate per accedere a indirizzi pubblici di Bitcoin e transazioni \"firmate\". Portafogli popolari: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "La breve storia di Bitcoin 2007 - secondo la leggenda, è quando Satoshi Nakamoto inizia il suo lavoro su Bitcoin 2008, agosto - la registrazione di bitcoin.org. 2009, gennaio - prima transazione effettuata in Bitcoin. 2009, ottobre - Il valore di Bitcoin, basato sul costo dell'elettricità utilizzato per generare Bitcoin, è fissato a 1USD = 1.306.03BTC 2010, maggio - un momento veramente storico, quando 10.000 BTC vengono utilizzati per acquistare indirettamente 2 pizze. Oggi è possibile acquistare 2500 pizze al peperoncino per lo stesso importo. 2010, luglio - Il prezzo del bitcoin aumenta da 0,008 USD a 0,080 USD per 1 TB in soli 5 giorni. 2010, dicembre - si verifica la prima transazione mobile. 2011, gennaio: il 25% di tutti i Bitcoin sono già stati generati. 2013 - Il Tesoro degli Stati Uniti classifica BTC come valuta virtuale decentralizzata convertibile. 2013, marzo - Il capital market di Bitcoin raggiunge 1.000.000 USD. 2013, maggio - il primo ATM Bitcoin trovato a San Diego, in California. 2013, novembre - Il prezzo del bitcoin raddoppia e raggiunge 503.10 USD. Cresce a 1.000 USD lo stesso mese. 2014 - Dell e Windows iniziano ad accettare Bitcoin. 2016, settembre - Il giudice federale degli Stati Uniti esclude che i BTC siano \"fondi nel significato comune di quel termine\". 2017, maggio - Il prezzo di BTC aumenta da 1.402 USD a 2.000 USD in 20 giorni. 2017, agosto - Il tasso di Bitcoin raggiunge 4.000 USD per moneta con il suo tasso più alto al momento di 4.489.10 USD per moneta il 21 agosto.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Semplice verifica",
      "title-2": "Più di 100 altcoin disponibili",
      "title-3": "Nessun costo nascosto",
      "title-4": "Nessuna registrazione"
    },
    "title": "Stiamo lavorando in più di 100 paesi",
    "subtitle": "incluso {{{country}}}",
    "shortTitle": "Stiamo lavorando in più di 100 paesi",
    "text1": "Semplice verifica",
    "popupText1": "I nuovi acquirenti devono solo verificare il numero di telefono",
    "text2": "Sono supportati 100 altcoin",
    "popupText2": "Puoi acquistare oltre 100 criptovalute più promettenti con una carta di credito / debito",
    "text3": "Nessun costo nascosto",
    "popupText3": "Otterrai esattamente la stessa quantità di altcoin, che è stato mostrato prima dell'affare",
    "text4": "Nessuna registrazione",
    "popupText4": "Non è necessario registrarsi, сryptocurrency verrà inviato all'indirizzo che hai indicato"
  },
  "buying-segment": {
    "title": "Più di 500.000 clienti",
    "sub-title": "utilizzano Indacoin dal 2015"
  },
  "phone-segment": {
      "title": "Scarica la nostra applicazione",
      "text1": "Acquista e conserva più di 100 criptovalute in un unico portafoglio",
      "text2": "Tieni traccia dei cambiamenti del tuo portafoglio di investimenti",
      "text3": "L'invio di criptovaluta al tuo amico è diventato facile come inviare un messaggio"
  },
  "social-segment": {
    "buttonText": "Leggi su Facebook",
    "on": "su Facebook",
    "facebook-review-1": "buona pagina bitcoin, processo veloce e semplice, li raccomando",
    "facebook-review-2": "Servizio eccellente e comodo da usare, il folk è amichevole e molto utile. Anche se si tratta di una transazione digitale, c'è una persona dall'altra parte (che può essere un valido aiuto) e rassicurante di sicuro, un servizio a 5 stelle che userò ancora",
    "facebook-review-3": "Poiché sto solo iniziando a gustare tutto ciò che circonda il mondo della criptovaluta, fare uno scambio sul loro sito Web è più che semplice. e il loro supporto nel caso in cui rimangano bloccati o si stia solo cercando aiuto è molto più che sorprendente.",
    "facebook-review-4": "Pagamento molto veloce, grande supporto e ottimi tassi di cambio.",
    "facebook-review-5": "Ho deciso di acquistare 100 euro di bitcoin utilizzando questo servizio, ed è stato facile procedere con il pagamento. Anche se ho avuto alcuni problemi e domande, sono stato quasi subito aiutato dal loro team di supporto. Mi ha fatto sentire molto più a mio agio nell'acquistare Bitcoin con Indacoin. E sto pensando di comprare di più in futuro.",
    "facebook-review-6": "Ho appena usato Indacoin per effettuare una transazione per l'acquisto di Bitcoin. La transazione è andata liscia, seguita da una chiamata di verifica. È molto facile da usare e un portale facile da usare per l'acquisto di monete bit. Consiglierei questa piattaforma per l'acquisto di bitcoin !!",
    "facebook-review-7": "È difficile trovare una facilità di transazione con carta di credito facile da acquistare valuta crittografica e Indcoin mi ha trattato molto bene, la pazienza con gli utenti di altri paesi che non capiscono la loro lingua, chat online di prontezza. Indcoin grazie !!!",
    "facebook-review-8": "Veloce e facile. Il supporto online è stato molto rapido e utile. Suggerimento Pro: l'utilizzo dell'app riduce i costi del 9%.",
    "facebook-review-9": "Questa è la mia seconda volta che utilizzo Indacoin: un servizio eccellente, un servizio clienti brillante e veloce. Sicuramente userò di nuovo.",
    "facebook-review-10": "Ottimo servizio e comunicazione, aiuto passo dopo passo con la live chat e totalmente affidabile, mi hanno persino chiamato senza chiedermelo e mi hanno spiegato come fare la verifica per la transazione, il modo più semplice per comprare Bitcoin senza problemi, grazie Indacoin farà molte più transazioni con te.",
    "facebook-review-11": "Ottimo servizio, processo veloce. Servizio clienti utile. Risposta veloce. Modo semplice per acquistare valuta criptata. Tra gli altri servizi, onestamente questo è l'unico servizio che utilizzo per acquistare la valuta criptata. Mi fido di loro per elaborare i miei acquisti. Consigliato a tutti coloro che si occupano di acquistare valuta criptata. Ultimo ma non meno importante, solo una parola per Indacoin: Eccellente !!!",
    //"facebook-review-12": "IndaCoin offre un servizio facile, affidabile e rapido. Sono stati anche estremamente veloci nel rispondere a tutte le mie domande e ad aiutarmi con il mio trasferimento. Consiglio vivamente i loro servizi a tutti! 10/10!",
    "facebook-review-12": "Ho comprato bitcoin per 200 euro e il sito Web è legittimo. Sebbene ci sia voluto un po 'prima che la transazione abbia luogo, il servizio clienti è eccellente. Ho contattato l'assistenza clienti e hanno immediatamente risolto il problema. Consiglio vivamente questo sito a tutti. a volte a causa del traffico elevato il tempo di transazione varia tra 8-10 ore. Non farti prendere dal panico se non ricevi le tue monete istantaneamente a volte ci vorrà del tempo ma sicuramente otterrai le tue monete.",
    "facebook-review-13": "Ottimo lavoro Indacoin. Ho avuto la migliore esperienza finora nell'acquisto di monete con la mia carta di credito con Indacoin .. Il consulente Andrew ha fatto un ottimo servizio clienti, e sono rimasto impressionato dal trasferimento troppo meno di 3 minuti ... Continuerò ad usarli :)",
    "facebook-review-14": "Indacoin è un'interfaccia semplice e intuitiva che gli acquirenti possono acquistare più facilmente i bitcoin sulla piattaforma. È facile con gli aiuti passo passo degli agenti consulenti e sono davvero pronti ad aiutarvi a risolvere il problema immediatamente. Sinceramente, riferirò la gente a Indacoin e mi aggiornano costantemente sulle mie situazioni e sono disposti a fare qualcosa in più per risolvere i vostri problemi. Grazie Indacoin!",
    "facebook-review-15": "Il modo più semplice e veloce per acquistare criptovaluta che ho trovato in molte ricerche. Ottimo lavoro ragazzi, grazie!",
    "facebook-review-16": "Really modo semplice per comprare monete online. Ottimo servizio clienti. È fantastico che abbiano anche monete alt da acquistare direttamente. Toglie le complicazioni dagli scambi. Lo consiglio vivamente a tutti dopo la mia prima esperienza positiva.",
    "facebook-review-17": "Buon servizio. Facile da usare e facile da acquistare varie monete / gettoni.",
    "facebook-review-18": "Questa è una delle migliori piattaforme per lo scambio di BTC. Ho fatto diversi scambi ed è stato veloce.",
    "facebook-review-19": "Ottimo servizio, facile da usare, facile da acquistare vari criptovaluta e token ERC20 con la tua carta di credito o di debito. Molto veloce e sicuro! Il servizio e la piattaforma che stavo cercando! Assolutamente consigliato l'uso di Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "Oltre {{{count}}} diverse criptovalute sono disponibili per l'acquisto su {{{name}}}"
  },
  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin nel tuo telefono",
    "text": "Il nostro smart-wallet Indacoin è disponibile per gli utenti di Android, iOS e in ogni browser internet. L'invio di valuta criptata a un tuo amico è diventato facile come l'invio di un messaggio"
  },

  "news-segment": {
    "no-news": "Non ci sono novità per la tua lingua",
    "news-error": "Ci scusiamo, la notizia non è al momento disponibile",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },
  
  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "Questa cryptocurrency non è attualmente disponibile ma tornerà presto",
    "unknown": "Errore del server sconosciuto",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "Il limite ha superato",
    "phone": "Phone is not supported"
  },
  "tags": {
    "title-btc": "Scambio Bitcoin BTC a {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Acquista [cryptocurrency] con carta di credito o debito in [country] insano - Indacoin",
    "title2": "Acquista Bitcoin con una carta di credito o di debito in tutto il mondo - Indacoin",
    "title3": "Acquista {{{cryptocurrency}}} con carta di credito o debito in {{{country}}} insano - Indacoin",
    "title4": "Acquista criptovaluta con carta di credito o di debito in {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Programma di affiliazione Indacoin",
      "news": "Indacoin ultime notizie",
      "help": "Scambio di criptovaluta F.A.Q. - Indacoin",
      "api": "API di scambio di criptovaluta - Indacoin",
      "terms-of-use": "Condizioni d'uso e politica AML - Indacoin",
      "login": "Accedi - Indacoin",
      "register": "Iscriviti - Indacoin",
      "locale": "Cambia regione - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) grafici dei prezzi e volume degli scambi - Indacoin"
    },
    "descriptions": {
      "affiliate": "Il programma di affiliazione Indacoin ti consente di guadagnare fino al 3% di ogni acquisto con una carta di credito / debito. La remunerazione verrà inviata al tuo portafoglio Bitcoin Indacoin",
      "news": "Visita il nostro blog ufficiale per rimanere in contatto con le ultime notizie sul settore delle criptovalute! Un sacco di informazioni utili e commenti completi degli esperti Indacoin in un unico posto!",
      "help": "Trova le risposte a tutte le tue domande su come acquistare, vendere e scambiare criptovaluta su Indacoin.",
      "api": "La nostra API fornisce l'acquisto e lo scambio istantaneo di qualsiasi criptovaluta con Visa e Mastercard. Controlla l'integrazione e gli esempi.",
      "terms-of-use": "Informazione generale. Condizioni d'uso. Servizio fornito. Trasferimento di fondi Trasferimento di diritti. Commissioni. Politica AML.",
      "login": "Accedi o crea un nuovo account",
      "register": "Crea un nuovo account",
      "locale": "Scegli la tua regione, e seleziona questo e ricorderemo la tua regione la prossima volta che visiti Indacoin",
      "currency": "Prezzi correnti per {{{fullCurrency}}} in USD con volume di trading e informazioni cronocriptovalute"
    }
  },
  "usAlert": "Siamo spiacenti, ma Indacoin non opera negli Stati Uniti d'America",
  "wrongPhoneCode": "Hai inserito un codice sbagliato dal tuo telefono",
  "skip": "Salta",
  "minSum": "Importo minimo da acquistare",
  "currency": {
    "no-currency": "Per una determinata criptovaluta, il grafico non è disponibile",
    "presented": {
      "header": "Bitcoin è stato presentato al mondo nel 2009 e nel 2017 rimane la principale criptovaluta del mondo"
    },
    "difference": {
      "title": "In che modo Bitcoin è diverso dal denaro normale?",
      "header1": "Emissione limitata",
      "text1": "Il numero totale di Bitcoin che possono essere estratti è 21M. Più di 16 milioni di Bitcoin sono in circolazione in questo momento. Più diventa popolare, più è costoso.",
      "header2": "Valuta mondiale",
      "text2": "Nessun governo ha il controllo su Bitcoin. Si basa solo sulla matematica",
      "header3": "anonimia",
      "text3": "Puoi usare i tuoi Bitcoin in qualsiasi momento e in qualsiasi quantità. Questa è una vera scoperta rispetto al trasferimento di denaro dal tuo conto bancario, quando è necessario spiegare la destinazione del pagamento e l'origine dei fondi.",
      "header4": "Velocità",
      "text4": "La tua banca impiegherà del tempo per trasferire denaro, soprattutto quando si tratta di valori importanti. Con Bitcoin, la routine di invio e ricezione richiederà pochi secondi.",
      "header5": "Trasparenza",
      "text5": "Tutte le transazioni Bitcoin possono essere viste su BlockChain."
    },
    "motivation": {
      "title": "Essendo la prima moneta del suo genere, è riuscito a cambiare la visione del denaro come lo sapevamo. Ormai, la capitalizzazione di mercato di Bitcoin raggiunge oltre $ 70 miliardi, superando giganti come Tesla, General Motors e FedEx"
    },
    "questions": {
      "header1": "Cos'è una valuta digitale e criptovaluta?",
      "text1": "La criptovaluta è una risorsa digitale, che funziona come mezzo di scambio utilizzando la crittografia per proteggere le transazioni e controllare la creazione di unità aggiuntive della valuta. Non ha forma fisica ed è decentralizzato rispetto ai soliti sistemi bancari",
      "header2": "Come si verifica la transazione con Bitcoin?",
      "text2": "Ogni transazione ha 3 dimensioni: Input - l'origine del Bitcoin utilizzato nella transazione. Ci dice dove l'ha preso l'attuale proprietario. Importo: il numero di \"monete\" utilizzate. Output - l'indirizzo del destinatario del denaro.",
      "header3": "Da dove viene Bitcoin?",
      "text3": "Sebbene il mondo abbia familiarità con il nome del creatore di Bitcoin, \"Satoshi Nakamoto\" è molto probabilmente un alias per un programmatore o anche un gruppo di tali. Fondamentalmente, Bitcoin proviene da 31.000 linee di codice create da \"Satoshi\" ed è controllato solo da software.",
      "header4": "Come funziona Bitcoin?",
      "text4": "Le transazioni avvengono tra i portafogli Bitcoin e sono trasparenti. Nella blockchain, chiunque può controllare l'origine e il ciclo di vita di ogni \"moneta\". Tuttavia, non esiste una \"moneta\". Bitcoin non ha una forma fisica e non ha forma virtuale, solo la storia delle transazioni.",
      "header5": "Cos'è un portafoglio Bitcoin?",
      "text5": "Un portafoglio Bitcoin è un programma che ti permette di gestire Bitcoin: ricevere e inviare loro, controllare il saldo e il tasso di Bitcoin. Sebbene non ci sia una \"moneta\" in tali portafogli, essa contiene chiavi digitali, utilizzate per accedere a indirizzi pubblici di Bitcoin e transazioni \"firmate\". Portafogli popolari:",
    },
    "history": {
      "title": "La breve storia di Bitcoin",
      "text1": "Secondo la leggenda, questo è quando Satoshi Nakamoto inizia il suo lavoro su Bitcoin",
      "text2": "Prima transazione fatta in Bitcoin. Il valore di Bitcoin, basato sul costo dell'elettricità utilizzato per generare Bitcoin, è fissato a 1 USD per 1.306,03 BTC",
      "text3": "Il 25% di tutti i Bitcoin sono già stati generati",
      "text4": "Dell e Windows iniziano ad accettare Bitcoin",
      "text5": {
        "part1": "Il prezzo di BTC aumenta da 1.402 USD a 19.209 USD in pochi mesi.",
        "part2": "Il tasso di Bitcoin raggiunge i 20.000 USD per moneta il 18 dicembre 2017"
      },
      "text6": "La registrazione di bitcoin.org",
      "text7": {
        "part1": "Un momento davvero storico, quando 10.000 BTC vengono usati per comprare indirettamente 2 pizze. Oggi è possibile acquistare 2.500 pizze di peperoni per lo stesso importo.",
        "part2": "Il prezzo del bitcoin aumenta da 0,008 USD a 0,08 USD per 1 BTC in soli 5 giorni.",
        "part3": "Si verifica la prima transazione mobile"
      },
      "text8": {
        "part1": "Il Tesoro degli Stati Uniti classifica BTC come valuta virtuale decentralizzata convertibile.",
        "part2": "Il primo ATM Bitcoin trovato a San Diego, in California.",
        "part3": "Il prezzo del bitcoin raddoppia e raggiunge 503,10 USD. Cresce a 1.000 USD lo stesso mese"
      },
      "text9": "Il giudice federale degli Stati Uniti esclude che i BTC siano \"fondi nel significato comune di quel termine\""
    },
    "graph": {
      "buy-button": "Acquistare"
    },
    "ethereum": {
      "presented": {
        "header": "Abbiamo già Bitcoin, quindi perché abbiamo bisogno di Ethereum? Scopriamolo! Bitcoin garantisce trasferimenti di denaro equi e trasparenti. Ether, la moneta digitale di Ethereum, può svolgere lo stesso ruolo. Tuttavia, c'è molto di più per la funzionalità, offerta da Ethereum."
      },
      "questions": {
        "header1": "Cos'è Ethereum?",
        "text1": "Ethereum è una piattaforma open source che utilizza la tecnologia blockchain per consentire l'elaborazione distribuita. È protetto da contratti intelligenti e presenta la propria criptovaluta, chiamata \"etere\", come mezzo di pagamento.",
        "header2": "Da dove proviene?",
        "text2": "Ethereum è stato inizialmente proposto come idea alla fine del 2013 dal programmatore canadese Vitalik Buterin. Finanziato da un crowdsale che ha avuto luogo durante l'estate del 2014, la piattaforma è stata finalmente presentata al mondo a luglio 2015.",
        "header3": "Cos'è l'ICO?",
        "text3": "Molte applicazioni sono basate su Ethereum e possono anche raccogliere fondi con Ether in un processo che viene chiamato offerta iniziale di monete (ICO). Il modo in cui funziona è abbastanza simile a quello che fa Kickstarter. I programmatori offrono diversi token di valore in cambio di fondi (generalmente Ether) per coprire le spese per lo sviluppo dell'app. Quando raggiungono un certo importo predeterminato o una certa data si verifica, i fondi vengono rilasciati ai programmatori. Se l'obiettivo non viene raggiunto, i fondi tornano ai contributori iniziali.",
        "header4": "Cos'è Ether?",
        "text4": "Ether (ETH) è una valuta digitale e un token valore utilizzato in Ethereum. Oltre ad essere il mezzo di pagamento per diversi servizi sulla piattaforma Ethereum, è anche presente negli scambi di criptovaluta e può essere modificato con denaro reale.",
        "header5": "Cosa c'è di così speciale nei \"contratti intelligenti\" e quale è la differenza dai soliti contratti?",
        "text5": "Ogni transazione ha 3 dimensioni: Input - l'origine del Bitcoin utilizzato nella transazione. Ci dice dove l'ha preso l'attuale proprietario. Importo: il numero di \"monete\" utilizzate. L'obiettivo principale del contratto intelligente è assicurarsi che tutti i termini di un contratto siano eseguiti correttamente. La sua funzionalità decentralizzata offre maggiore sicurezza per i contributori di Ethereum, utilizzando applicazioni imparziali, memorizzate nella blockchain, per verificare e far rispettare i suoi termini. Questa tecnologia non lascia quasi spazio a frodi. E mentre per il solito contratto avresti generalmente bisogno di una terza parte (avvocato) per affrontare eventuali problemi, come la violazione, i contratti intelligenti sono imparziali e utilizzare la matematica per fornire sicurezza.",
        "header6": "How is Ethereum different from Bitcoin?",
        "text6": "Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens – Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
        "header7": "What are the most popular Ethereum wallets?",
        "text7": "An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets: "
      },
      "history": {
        "title": "La breve storia di Ethereum",
        "text1": "Crowdsale viene avviato per raccogliere fondi per la creazione di Ethereum",
        "text2": "L'organizzazione autonoma decentralizzata (DAO) è creata sulla piattaforma Ethereum. Finanziato da una folla di falsari, è al momento la più grande campagna di crowdfunding della storia",
        "text3": "Ethereum guadagna una quota di mercato del 50% nell'offerta iniziale di monete",
        "text4": "Il tasso di cambio di Ethereum raggiunge i 400 USD (aumento del 5,000% da gennaio)",
        "text5": "L'idea di Ethereum è offerta da Vitalik Buterin, un programmatore canadese e fondatore di Bitcoin Magazine",
        "text6": "La piattaforma del 30 luglio è online",
        "text7": "Il DAO viene violato, il valore di Ethereum scende da 21,5 USD a 8 USD. Ciò porta Ethereum a essere biforcuta in due blockchain nel 2017",
        "text8": "Enterprise Ethereum Alliance è stata creata. Tra i membri ci sono Toyota, Samsung, Microsoft, Intel, Deloitte e altri nomi piuttosto grandi. Lo scopo principale dell'Alleanza è determinare come la funzionalità di Ethereum può essere utilizzata in vari aspetti della vita, come la gestione, le banche, la salute, la tecnologia ecc.",
        "text9": {
          "part1": "Tutte le criptovalute diminuiscono dopo che la Cina ha vietato l'ICO, ma in seguito si è ripresa rapidamente.",
          "part2": "La capitalizzazione di mercato di Ethereum raggiunge più di 36 miliardi di dollari, il suo valore storico più alto, per poi scendere a 30 miliardi dopo il divieto di ICO della Cina"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "Sei pronto a consentire ai tuoi clienti di acquistare Crypto con una carta di credito / debito? Si prega di lasciare la tua email e ci metteremo in contatto al più presto",
        "input": "Indirizzo email",
        "button": "Iniziare",
        "mail-success": "Grazie. Ti risponderemo"
      },
      "api": {
        "title": "Integrazione API",
        "text": "Utilizzando la nostra API, puoi consentire ai tuoi clienti di acquistare criptovalute istantaneamente dal tuo sito. La nostra piattaforma antifrode integrata nella tua pagina web o applicazione mobile può anche essere utilizzata come strumento che ti consente di monitorare e filtrare i pagamenti effettuati sulla tua piattaforma."
      },
      "affiliate": {
        "title": "Programma di affiliazione",
        "text": "Ricevi ricompensa per aver consigliato Indacoin ad altri clienti. Guadagnerai fino al 3% dagli acquisti dei referral.",
        "block-1": {
          "title": "Come posso aderire al programma di affiliazione di Indacoin?",
          "text": "Sentiti libero di registrarti sul nostro sito."
        },
        "block-2": {
          "title": "Come potrò ritirare i fondi raccolti nel mio account?",
          "text": "Puoi acquistare BTC / ETH / altri altcoin su Indacoin senza costi aggiuntivi o riceverli sulla tua carta di credito."
        },
        "block-3": {
          "title": "Qual è il vantaggio del tuo programma di affiliazione?",
          "text": "Sulla nostra piattaforma, i clienti hanno l'opportunità di acquistare più di 100 monete diverse utilizzando carte di credito / debito senza registrazione. Considerando il fatto che gli acquisti procedono quasi immediatamente, i partner possono ottenere immediatamente i loro profitti."
        },
        "block-4": {
          "title": "Posso vedere l'esempio del link di riferimento?",
          "text": "Certo, questo è il modo in cui appare: ({{{link}}})"
        }
      },
      "more": "Scopri di più",
      "close": "Vicino"
    },
    "achievements": {
      "partnership-title": "Associazione",
      "partnership-text": "Esistono molti modi per collaborare con Indacoin. Il programma di partnership Indacoin è rivolto alle società criptate che sono disposte a collegare il mondo delle criptovalute con il tradizionale sistema di moneta fiat per aumentare l'impegno nell'industria della criptazione. Con Indacoin, sarai in grado di fornire ai tuoi clienti l'esperienza personalizzata della crittografia d'acquisto con le carte di credito / debito.",
      "block-1": {
        "header": "di fiducia",
        "text": "Lavoriamo nel mercato della crittografia dal 2013 e abbiamo collaborato con molte aziende autorevoli che guidano il settore. Oltre 500.000 clienti da oltre 190 paesi hanno utilizzato il nostro servizio per acquistare criptovalute."
      },
      "block-2": {
        "header": "Sicuro",
        "text": "Abbiamo creato una piattaforma anti-frode innovativa e altamente efficiente con una tecnologia di base sviluppata appositamente per l'individuazione e la prevenzione di attività fraudolente per progetti di criptazione."
      },
      "block-3": {
        "header": "Soluzione pronta all'uso",
        "text": "Il nostro prodotto è già utilizzato da ICO, Cryptocurrency Exchanges, Blockchain Platforms e Crypto Funds."
      },
      
      "header": "Guadagna con noi sulla svolta",
      "subheader": "Tecnologie blockchain",
      "text1": "{{{company}}} è una piattaforma globale che consente alle persone di acquistare istantaneamente Bitcoin, Ethereum, Ripple, Waves e altre 100 criptovalute con una carta di credito o di debito",
      "text2": "Il servizio ha funzionato dal 2013 in tutto il mondo, incluso ma non limitato a:",
      "text3": "Regno Unito, Canada, Germania, Francia, Russia, Ucraina, Spagna, Italia, Polonia, Turchia, Australia, Filippine, Indonesia, India, Bielorussia, Brasile, Egitto, Arabia Saudita, Emirati Arabi Uniti, Nigeria e altri"
    },
    "clients": {
      "header": "Più di 500.000 clienti",
      "subheader": "dal 2013"
    },
    "benefits": {
      "header": "I nostri principali vantaggi per le aziende:",
      "text1": "Servizio unico che consente ai clienti di acquistare e inviare bitcoin senza registrazione. Quindi i clienti che vengono a Indacoin seguendo il link di riferimento dovranno semplicemente inserire i dettagli della carta e l'indirizzo del portafogli, dove deve essere inviata la crittografia. Pertanto i potenziali clienti effettueranno immediatamente gli acquisti e i nostri partner otterranno profitto.",
      "text2": "Fino al 3% di ogni transazione viene guadagnata dai nostri partner. Inoltre, i webmaster riceveranno il 3% di tutti gli acquisti futuri del referral.",
      "text3": "Bitcoin e carte bancarie potrebbero essere utilizzati per prelevare il reddito."
    },
    "accounts": {
      "header": "Unisciti a noi adesso",
      "emailPlaceholder": "Inserisci il tuo indirizzo email",
      "captchaPlaceholder": "Captcha",
      "button": "Iniziare"
    }
  },
  "faq": {
    "title": "Domande e risposte",
    "header1": "Domande generali",
    "header2": "criptovaluta",
    "header3": "Verifica",
    "header4": "commissioni",
    "header5": "Acquista",
    "question1": {
      "title": "Cos'è Indacoin?",
      "text": "Siamo una società che opera nel settore delle criptovalute dal 2013, con sede a Londra, nel Regno Unito. Puoi utilizzare il nostro servizio per acquistare oltre 100 diverse criptovalute tramite il pagamento con carta di credito / debito senza registrazione."
    },
    "question2": {
      "title": "Come posso contattarti?",
      "text": "Puoi contattare il nostro team di supporto via e-mail (support@indacoin.com), telefono (+ 44-207-048-2582) o utilizzando un'opzione di chat dal vivo sulla nostra pagina web."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "Quali criptovalute posso comprare qui?",
      "text": "Puoi acquistare oltre 100 diverse criptovalute che sono tutte elencate sul nostro sito web."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "Quali paesi possono utilizzare questo servizio?",
      "text": "Lavoriamo con tutti i paesi, con la sola eccezione degli Stati Uniti (al momento)"
    },
    "question7": {
      "title": "Posso vendere bitcoin qui?",
      "text": "No, puoi acquistare solo bitcoin qui."
    },
    "question8": {
      "title": "Cos'è la criptovaluta?",
      "text": "Сryptocurrency è una valuta digitale che utilizza la crittografia per sicurezza, funzionalità, che rende difficile la contraffazione. Non è emesso da alcuna autorità centrale, rendendolo teoricamente immune da interferenze o manipolazioni governative."
    },
    "question9": {
      "title": "Dove posso memorizzare la criptovaluta?",
      "text": "Puoi impostare un portafoglio separato per ogni criptovaluta sul tuo PC oppure puoi utilizzare il nostro servizio di portafoglio gratuito e archiviare oltre 100 criptovalute in un unico posto."
    },
    "question10": {
      "title": "Cosa intendi per \"indirizzo bitcoin\"?",
      "text": "È l'indirizzo del tuo portafoglio bitcoin. Puoi anche inserire l'indirizzo di una persona a cui desideri inviare bitcoin o creare un portafoglio su Indacoin"
    },
    "question11": {
      "title": "Quanto è veloce la transazione di criptovaluta?",
      "text": "I tempi delle transazioni di criptovaluta dipendono dalla criptovaluta che si sta inviando, le transazioni con Bitcoin richiedono circa 15-20 minuti e in media consentono fino a 1 ora per il resto delle monete."
    },
    "question12": {
      "title": "Cosa devo fare per verificare la mia carta?",
      //"text": "Per completare la verifica dovrai inserire il codice a 4 cifre che ricevi tramite una telefonata. Per il tuo secondo passaggio ti potrebbe anche essere richiesto di sottoporsi a una verifica video, in cui registri un video mostrando il tuo viso e carichi una scansione o una foto del tuo ID / passaporto / patente di guida. A seconda della verifica, questo passaggio può essere obbligatorio poiché è necessario essere certi di essere il titolare della carta."
      "text": "Per completare la verifica dovrai inserire il codice a 4 cifre che ricevi tramite una telefonata. Per la seconda fase, potrebbe anche essere richiesto di sottoporsi a una verifica fotografica, che richiede di caricare una foto del passaporto / ID e un selfie con ID e un pezzo di carta con il testo richiesto, a conferma dell'acquisto. In alternativa, possiamo chiedere la verifica del video, in cui dovrai registrare il tuo viso, diciamo: \"Verifica di Indacoin per criptazione\" mostra il tuo ID e la carta con cui hai effettuato il pagamento. Quando mostri la carta, mostra solo le ultime 4 cifre e il nome."
    },
    "question13": {
      "title": "Cosa succede se non riesco a registrare una verifica video?",
      "text": "Puoi aprire lo stesso link alla pagina dell'ordine dal tuo dispositivo mobile e registrarlo da lì. In caso di problemi, è inoltre possibile inviare il video richiesto come allegato via e-mail a support@indacoin.com. Assicurati di indicare il numero dell'ordine di scambio."
    },
    "question14": {
      "title": "Ho effettuato un ordine e inserito tutti i dettagli, perché è ancora in elaborazione?",
      "text": "Se hai aspettato più di 2 minuti, allora l'ordine non è arrivato alla transazione. È possibile che la tua carta non sia sicura 3D e potresti aver bisogno di riprovare. Assicurati di confermare l'acquisto tramite codice PIN con la tua banca o puoi provare ad acquistare con una valuta diversa (EUR / USD). Altrimenti, se sei sicuro che la tua carta sia in 3D e la modifica della valuta non ti aiuta, allora devi contattare la tua banca, potrebbero bloccare i tuoi tentativi di acquisto."
    },
    "question15": {
      "title": "Devo passare attraverso la verifica ogni volta che effettuo un acquisto?",
      "text": "Devi solo verificare la tua carta una volta e tutte le seguenti transazioni saranno automatiche. Tuttavia, se deciderai di utilizzare un'altra carta di credito, sarà necessaria una verifica."
    },
    "question16": {
      "title": "Il mio ordine è stato rifiutato dalla banca, cosa dovrei fare?",
      "text": "Devi contattare la tua banca e chiedere il motivo del rifiuto, forse possono sollevare la restrizione in modo da poter effettuare un acquisto."
    },
    "question17": {
      "title": "Perché devo verificare la mia carta bancaria o inviare un video?",
      "text": "Il processo di verifica ti protegge in caso di furto di carte, hacking o truffatori; questo funziona in modo opposto nel caso in cui qualcuno stia tentando di effettuare un acquisto utilizzando la carta bancaria di qualcun altro."
    },
    "question18": {
      "title": "Come posso credere che i miei dati e le informazioni della mia carta siano al sicuro?",
      "text": "Non condividiamo mai i tuoi dati con terze parti senza il tuo consenso. Inoltre, accettiamo solo schede 3D-Secure e non richiediamo mai dati sensibili come il codice CCV o il numero completo della carta. Fornisci tali dettagli di pagamento solo tramite i terminali gateway Visa o Mastercard. Raccogliamo solo le informazioni richieste per assicurarci che tu sia il proprietario della carta."
    },
    "question19": {
      "title": "Qual è la tua tariffa?",
      "text": "La nostra commissione varia da acquisto ad acquisto in quanto si basa su numerosi fattori, per questo motivo abbiamo creato una calcolatrice che ti indicherà l'importo esatto di criptovaluta che riceverai, tutte le commissioni incluse."
    },
    "question20": {
      "title": "Quanto mi verrà addebitato per la transazione?",
      "text": "Ti verrà addebitato solo l'importo specificato durante il tuo ordine nella nostra calcolatrice. Tutte le tasse sono incluse in tale importo."
    },
    "question21": {
      "title": "Avete sconti?",
      "text": "Occasionalmente abbiamo sconti, puoi ottenere maggiori informazioni dal nostro staff di supporto."
    },
    "question22": { // TODO: link
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "Quanto velocemente verrà trasmessa la mia criptovaluta?",
      "text": "Di solito ci vogliono circa 30 minuti dopo aver effettuato un ordine o una verifica completa (se è la prima volta che usi la carta su Indacoin)."
    },
    "question24": {
      "title": "Quali sono gli acquisti minimi e massimi?",
      "text": "Avrai il limite massimo di $ 200 per la prima transazione, $ 200 aggiuntivi per la seconda transazione disponibile dopo 4 giorni dall'acquisto iniziale, $ 500 dopo 7 giorni e $ 2000 in 14 giorni del primo acquisto. In un mese dal tuo primo acquisto non ci sono limiti al pagamento con la tua carta. Si prega di tenere presente che il limite minimo è sempre $ 50."
    },
    "question25": {
      "title": "Che tipo di carte sono accettate?",
      "text": "Accettiamo solo Visa e Mastercard con 3D sicuro."
    },
    "question26": {
      "title": "Cos'è il 3D sicuro?",
      "text": "La tecnologia 3D Secure comprende i programmi Verified by Visa e MasterCard SecureCode. Dopo aver inserito i dati della carta di credito nel nostro negozio online, verrà visualizzata una nuova finestra che richiede il codice di sicurezza personale. Il tuo istituto finanziario autenticherà la transazione in pochi secondi e confermerà che tu sei l'individuo che effettua l'acquisto."
    },
    "question27": {
      "title": "Quali altri sistemi di pagamento posso utilizzare per acquistare bitcoin?",
      "text": "Attualmente accettiamo solo pagamenti con carte Visa / Master sulla nostra piattaforma."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": { // TODO: translate
    "message": "L'importo deve essere compreso tra {{{limitsMin}}} {{{limitsCur}}} e {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "L'importo deve essere da {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">Questo sito Web utilizza i cookie per assicurarti di ottenere la migliore esperienza sul nostro sito web.</span> {{{link}}}",
      "accept-button": "Fatto",
      "link": "Per saperne di più"
    },
    "success-video": {
      "text": "Grazie! Ora abbiamo avviato il processo di verifica, che può richiedere del tempo. Una volta completata la verifica, le monete crittografiche ti verranno consegnate"
    },
    "accept-agreement": {
      //"text": "Si prega di indicare di aver letto e accettato l'Accordo per gli utenti e la politica AML"
      "text": "Si prega di indicare di aver letto e accettato le Condizioni d'uso e l'Informativa sulla privacy"
    },
    "price-changed": {
      "header": "Prezzo cambiato",
      "agree": "Essere d'accordo",
      "cancel": "Annulla"
    },
    "neoAttention": "A causa delle caratteristiche della valuta crittografica NEO, solo gli interi sono disponibili per l'acquisto",
    "login": {
      "floodDetect": "Rileva alluvione",
      "resetSuccess": "Reimposta la password con successo. Si prega di controllare la tua email",
      "wrongEmail": "Questa email non è ancora stata registrata",
      "unknownError": "Errore del server. Si prega di riprovare la richiesta",
      "alreadyLoggedIn": "Sei già loggato",
      "badLogin": "nome utente o password errati",
      "badCaptcha": "Captcha errato",
      "googleAuthenticator": "Inserisci il codice di autenticazione di Google"
    },
    "registration": {
      "unknownError": "Errore sconosciuto",
      "floodDetect": "Rileva alluvione",
      "success": "Registrati al successo. Si prega di controllare la tua email",
      "badLogin": "nome utente o password errati",
      "badCaptcha": "Captcha errato",

      "empty": "Non tutti i campi sono completati. Si prega di riempirli tutti",
      "repeatedRequest": "La conferma della registrazione è stata inviata al tuo indirizzo email. Si prega di assicurarsi di averlo ricevuto. Nel caso, controlla anche una cartella spam",
      "badEmail": "Hai usato l'indirizzo email errato o temporaneo",
      "blockedDomain": "Si prega di provare un indirizzo email diverso per la registrazione"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "nessun risultato trovato",
  "locale": {
    "title": "Per favore scegli il tuo paese"
  }
}