export default {
  'lang': 'de',
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    }
  },
  "recordVideo": {
    "send": "Senden",
    "stop": "Halt",
    "record": "Aufzeichnung"
  },
  "links": {
    "terms-of-use": {
      "text": "Ich stimme den {{{terms}}} und {{{policy}}} zu",
      "link-1": "Nutzungsbedingungen",
      "link-2": "Datenschutzrichtlinien"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "Warte auf deine Zahlung",
      "status-2": "Tauschen",
      "status-2-sub": "Wir suchen den besten Preis für Sie",
      "status-3": "Senden an Ihre Brieftasche",
      "transaction-completed": "Herzlichen Glückwunsch, Ihre Transaktion ist abgeschlossen!",
      "rate": "Wechselkurs 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Du hast gesendet:",
      "toAddress": "an {{{cryptoAdress}}}",
      "receiveText": "Du wirst erhalten:"
    },
    "changeCrypto": {
      "send": "Senden Sie {{{amount}}} BTC an die unten angegebene Adresse",
      "confirmation": "Nach zwei Bitcoin-Bestätigungen ≈ {{{amount}}} {{{shortName}}} wird an Ihre Adresse {{{wallet}}} gesendet ...",
      "copy": "Adresse kopieren",
      "copied": "In die Zwischenablage kopiert"
    },
    "order": "Bestellung",
    "status": {
      "title": "Status",
      "completed": "Die Anfrage wurde bearbeitet"
    },
    "payAmount": {
      "title": "Bezahlt"
    },
    "check": "Überprüfen Sie den Status der Transaktion auf blockexplorer.net",
    "resend-phone-code": "Ich habe keinen Anruf erhalten?",
    "sms-code": "Bitte nehmen Sie den Anruf entgegen und geben Sie hier den vierstelligen Code ein",
    "registration": "Gehe zur Registrierungsseite",

    "internalAccount-1": "Bitte überprüfen Sie Ihre Indacoin Wallet mit",
    "internalAccount-link": "Einloggen",
    "internalAccount-2": "",
    "your-phone": "Ihr Telefon: {{{phone}}}",
    "many-requests": "Es tut uns leid. Zu viele Anfragen zum Telefonwechsel"

  },
  "app":{
    "detect_lang":{
      "title":"Ihre Sprache wird erkannt",
      "sub":"Bitte warten..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Kaufe und tausche sofort {{{longName}}} und 100 andere Kryptowährungen zum besten Preis in {{{country}}}",
    "title":"Indacoin: Kaufen Sie Kryptowährung mit einer Kredit- oder Debitkarte",
    "description":"Kaufen und wechseln Sie alle Kryptowährungen sofort: [cryptocurrency], Ethereum, Litecoin, Ripple und 100 andere digitale Währungen für EUR oder USD.",
    "description-bitcoin":"Kaufen und tauschen Sie sofort jede Kryptowährung aus: Bitcoin, Ethereum, Litecoin, Ripple und 100 andere digitale Währungen für EUR oder USD",
    "description-ethereum":"Kaufen und tauschen Sie sofort jede Kryptowährung aus: Ethereum, Bitcoin, Litecoin, Ripple und 100 andere digitale Währungen für EUR oder USD",
    "description-litecoin":"Kaufen und tauschen Sie sofort jede Kryptowährung aus: Litecoin, Bitcoin, Ethereum, Ripple und 100 andere digitale Währungen für EUR oder USD",
    "description-ripple":"Kaufen und tauschen Sie sofort jede Kryptowährung aus: Ripple, Bitcoin, Ethereum, Litecoin und 100 andere digitale Währungen für EUR oder USD",

    "description2": "Kaufen und tauschen Sie Kryptocurreny sofort aus: Bitcoin, Ethereum, Litecoin, Ripple und 100 andere digitale Währungen für EUR oder USD",
    "description3": "Kaufen und tauschen Sie sofort jede Kryptowährung: Bitcoin, Ethereum, Litecoin, Ripple und 100 andere digitale Währungen mit Visa und Mastercard",
    "buy": "Kaufen Sie",
    "subTitle": "mit einer Kredit- oder Debitkarte",
    "more": "In mehr als 100 Ländern einschließlich",
    //"header": "Tausche Bitcoin gegen <span class='sub'>{{{name}}}</span>",
    "header": "Sofort <span class='sub'>BTC</span> gegen <span class='sub'>{{{shortName}}}</span> austauschen"
  },
  "fullpage": {
    "header": "KAUFEN SIE KRYPTOWÄHRUNG SOFORT",
    "header-alt": "Indacoin bietet eine einfache Möglichkeit, Bitcoins gegen eine von 100 verschiedenen Kryptowährungen auszutauschen",
    "sub": "Indacoin bietet eine einfache Möglichkeit, Bitcoins und mehr als 100 Kryptowährungen mit Visa & amp; MasterCard",
    "sub-alt": "Indacoin bietet eine einfache Möglichkeit, Bitcoins gegen eine von 100+ verschiedenen Kryptowährungen auszutauschen",
    "observe": "Erfahren Sie unten mehr über die verfügbaren Kryptowährungen"
  },
  "navbar": {
    "main":"Startseite",
    "btc": "Bitcoins kaufen",
    "eth": "Kryptowährung kaufen",
    "affiliate": "Partner",
    "news": "Neuigkeiten",
    "faq": "FAQ",
    "login": "Anmelden",
    "registration": "Registrieren",
    "platform": "Produkte",

    "logout": "Ausloggen",
    "buy-instantly": "Sofort Krypto kaufen",
    "trading-platform": "Handelsplattform",
    "mobile-wallet": "Mobile Brieftasche",
    "payment-processing": "Zahlungsverarbeitungs-API"
  },
  "currency-table": {
    "rank": "Rang",
    "coin": "Währung",
    "market": "Marktkapitalisierung",
    "price": "Kosten",
    "volume": "Menge, 24 h",
    "change": "Veränderung, 24 h",
    "available-supply": "Verfügbares Angebot",
    "search-cryptocurrency": "Suche nach Kryptowährung",

    "change-1h": "1H Änderungen",
    "change-24h": "1D Änderungen",
    "change-7d": "1W Änderungen"
  },
  "buy-blocks":{
    "give":"Sie bezahlen",
    "get":"Sie erhalten"
  },
  "exchange-form": {
    "title": "Kaufen Sie Kryptowährung sofort",
    "submit": "Wechseln",
    "give": "Sie bezahlen",
    "get": "Sie erhalten",
    "search": "Suche",
    "error-page": "Wir entschuldigen uns, aber der Kauf ist derzeit nicht verfügbar"
  },
  "pagination": {
    "page": "Seite",
    "next": "Vorwärts",
    "prev": "Zurück",
    "first": "Zurück nach oben"
  },
  "sidebar":{
    "accept":{
      "title":"Wir akzeptieren Kreditkarten",
      "button":"Kryptowährung kaufen"
    },
    "youtube":{
      "title":"Über uns",
      "play":"Start"
    },
    "orders":{
      "title": "Bestellverwaltung",
      "order": "Bestellnr.",
      "confirm": "Bestätigungscode",
      "check": "Bestellung überprüfen",
      "new": "Neue Bestellung",
      "or": "oder"
    },
    "social":{
      "title":"Soziale"
    }
  },
  "wiki":{
    "etymology":"Etymologie"
  },
  "loading": {
    "title": "Upload",
    "sub": "Bitte warten",
    "text": "Upload"
  },
  "error": {
    "title": "Fehler",
    "sub": "Hoppla! Da ist etwas schiefgelaufen.",
    "text": "Es ist ein unbekannter Fehler aufgetreten. Wir arbeiten daran. Bitte schauen Sie später noch einmal vorbei.",
    "home": "Startseite"
  },
  "features":{
    "title":"Was Sie auf<br><span class=\"offset\"><span class=\"blue\">Indacoin tun können</span></span>",
    "column1":{
      "title":"Kryptowährung kaufen",
      "text":"Auf Indacoin können Sie mit einer Kredit- oder Debitkarte sofort mehr als 100 verschiedene Kryptowährungen kaufen"
    },
    "column2":{
      "title":"Erfahren Sie mehr über Kryptowährungen",
      "text":"Unsere einfachen Anleitungen unterstützen Sie dabei, die grundlegenden Prinzipien von alternativen Währungen besser zu verstehen"
    },
    "column3":{
      "title":"Treffen Sie Anlageentscheidungen",
      "text":"Leistungsfährige Analysetools helfen Ihnen dabei, den besten Moment für Käufe und Verkäufe zu finden."
    },
    "column4":{
      "title":"Kaufen Sie Bitcoins ohne Registrierung",
      "text":"Die Bitcoins werden an die von Ihnen gesendete Adresse geschickt, sobald der Bezahlvorgang abgeschlossen ist"
    },
    "column5":{
      "title":"Teilen Sie Ihre Gedanken mit",
      "text":"Nehmen Sie an Diskussionen über die neuen Krypto-Trends mit anderen führenden Anlegern teil"
    },
    "column6":{
      "title":"Bewahren Sie alle Vermögenswerte an einem Ort auf",
      "text":"Sie können in einem Indacoin-Konto mehr als 100 digitale Währungen aufbewahren und verwalten"
    }
  },
  "mockup":{
    "title":"MOBILE KONTEN",
    "text":"Unser smartes und einfaches Indacoin-Konto funktioniert auf Ihrem Android- oder iPhone-Telefon und zusätzlich auch mit Ihrem Webbrowser. Jetzt ist das Versenden von Kryptowährung an das Telefon eines Freundes so einfach wie das Verschicken einer SMS"
  },
  "footer":{
    "column1":{
      "title":"Kaufen",
      "link1":"Kaufen Bitcoin",
      "link2":"Kaufen Ethereum",
      "link3":"Kaufen Ripple",
      "link4":"Kaufen Bitcoin Cash",
      "link5":"Kaufen Litecoin",
      "link6":"Kaufen Dash"
    },
    "column2":{
      "title":"Informationen",
      "link1":"Über uns",
      "link2":"Fragen",
      "link3":"Allgemeine Geschäftsbedingungen",
      "link4":"Anleitungen"
    },
    "column3":{
      "title":"Tools",
      "link1":"API",
      "link2":"Region ändern",
      "link3":"Mobile App"
    },
    "column4":{
      "title":"Kontakte"
    }
  },
  "country":{
    "AF":{
      "name":"Afghanistan"
    },
    "AX":{
      "name":"Åland-Inseln"
    },
    "AL":{
      "name":"Albanien"
    },
    "DZ":{
      "name":"Algerien"
    },
    "AS":{
      "name":"Amerikanisch-Samoa"
    },
    "AD":{
      "name":"Andorra"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguilla"
    },
    "AQ":{
      "name":"Antarktik"
    },
    "AG":{
      "name":"Antigua und Barbuda"
    },
    "AR":{
      "name":"Argentinien"
    },
    "AM":{
      "name":"Armenien"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Australien"
    },
    "AT":{
      "name":"Österreich"
    },
    "AZ":{
      "name":"Aserbeidschan"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Bahrain"
    },
    "BD":{
      "name":"Bangladesch"
    },
    "BB":{
      "name":"Barbados"
    },
    "BY":{
      "name":"Weißrussland"
    },
    "BE":{
      "name":"Belgien"
    },
    "BZ":{
      "name":"Belize"
    },
    "BJ":{
      "name":"Benin"
    },
    "BM":{
      "name":"Bermuda"
    },
    "BT":{
      "name":"Bhutan"
    },
    "BO":{
      "name":"Bolivien"
    },
    "BA":{
      "name":"Bosnien und Herzegovina"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Bouvet Island"
    },
    "BR":{
      "name":"Brasilien"
    },
    "IO":{
      "name":"Britische Gebiete im Indischen Ozean"
    },
    "BN":{
      "name":"Brunei Darussalam"
    },
    "BG":{
      "name":"Bulgarien"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Kambodscha"
    },
    "CM":{
      "name":"Kamerun"
    },
    "CA":{
      "name":"Kanada"
    },
    "CV":{
      "name":"Kap Verde"
    },
    "KY":{
      "name":"Kaiman-Inseln"
    },
    "CF":{
      "name":"Zentralafrikanische Republik"
    },
    "TD":{
      "name":"Chad"
    },
    "CL":{
      "name":"Chile"
    },
    "CN":{
      "name":"China"
    },
    "CX":{
      "name":"Weihnachtsinsel"
    },
    "CC":{
      "name":"Cocos (Keeling)-Inseln"
    },
    "CO":{
      "name":"Kolumbien"
    },
    "KM":{
      "name":"Comoros"
    },
    "CG":{
      "name":"Kongo"
    },
    "CD":{
      "name":"Kongo, Demokratische Republik"
    },
    "CK":{
      "name":"Cook-Inseln"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Elfenbeinküste"
    },
    "HR":{
      "name":"Kroatien"
    },
    "CU":{
      "name":"Kuba"
    },
    "CY":{
      "name":"Zypern"
    },
    "CZ":{
      "name":"Tschechische Republik"
    },
    "DK":{
      "name":"Dänemark"
    },
    "DJ":{
      "name":"Dschibuti"
    },
    "DM":{
      "name":"Dominica"
    },
    "DO":{
      "name":"Dominikanische Republik"
    },
    "EC":{
      "name":"Ecuador"
    },
    "EG":{
      "name":"Ägypten"
    },
    "SV":{
      "name":"El Salvador"
    },
    "GQ":{
      "name":"Äquatorial-Guinea"
    },
    "ER":{
      "name":"Eritrea"
    },
    "EE":{
      "name":"Estland"
    },
    "ET":{
      "name":"Äthiopien"
    },
    "FK":{
      "name":"Falkland-Inseln (Malvinas)"
    },
    "FO":{
      "name":"Faröer"
    },
    "FJ":{
      "name":"Fidschi"
    },
    "FI":{
      "name":"Finnland"
    },
    "FR":{
      "name":"Frankreich"
    },
    "GF":{
      "name":"Frazösisch-Guiana"
    },
    "PF":{
      "name":"Französisch-Polynesien"
    },
    "TF":{
      "name":"Französische Südgebiete"
    },
    "GA":{
      "name":"Gabon"
    },
    "GM":{
      "name":"Gambia"
    },
    "GE":{
      "name":"Georgien"
    },
    "DE":{
      "name":"Deutschland"
    },
    "GH":{
      "name":"Ghana"
    },
    "GI":{
      "name":"Gibraltar"
    },
    "GR":{
      "name":"Griechenland"
    },
    "GL":{
      "name":"Grönland"
    },
    "GD":{
      "name":"Grenada"
    },
    "GP":{
      "name":"Guadeloupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernsey"
    },
    "GN":{
      "name":"Guinea"
    },
    "GW":{
      "name":"Guinea-Bissau"
    },
    "GY":{
      "name":"Guyana"
    },
    "HT":{
      "name":"Haiti"
    },
    "HM":{
      "name":"Heard-Insel und McDonald-Inseln"
    },
    "VA":{
      "name":"Vatikan"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hongkong"
    },
    "HU":{
      "name":"Ungarn"
    },
    "IS":{
      "name":"Island"
    },
    "IN":{
      "name":"Indien"
    },
    "ID":{
      "name":"Indonesien"
    },
    "IR":{
      "name":"Iran, Islamische Republik"
    },
    "IQ":{
      "name":"Iran"
    },
    "IE":{
      "name":"Irland"
    },
    "IM":{
      "name":"Isle of Man"
    },
    "IL":{
      "name":"Israel"
    },
    "IT":{
      "name":"Italien"
    },
    "JM":{
      "name":"Jamaika"
    },
    "JP":{
      "name":"Japan"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Jordanien"
    },
    "KZ":{
      "name":"Kasachstan"
    },
    "KE":{
      "name":"Kenia"
    },
    "KI":{
      "name":"Kiribati"
    },
    "KP":{
      "name":"Korea, Volksrepublik"
    },
    "KR":{
      "name":"Korea, Republik"
    },
    "KW":{
      "name":"Kuwait"
    },
    "KG":{
      "name":"Kirgistan"
    },
    "LA":{
      "name":"Lao, Volksrepublik"
    },
    "LV":{
      "name":"Lettland"
    },
    "LB":{
      "name":"Libanon"
    },
    "LS":{
      "name":"Lesotho"
    },
    "LR":{
      "name":"Liberia"
    },
    "LY":{
      "name":"Lybien"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Litauen"
    },
    "LU":{
      "name":"Luxemburg"
    },
    "ME":{
      "name":"Montenegro"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Mazedonien"
    },
    "MG":{
      "name":"Madagaskar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malaysia"
    },
    "MV":{
      "name":"Maldives"
    },
    "ML":{
      "name":"Mali"
    },
    "MT":{
      "name":"Malta"
    },
    "MH":{
      "name":"Marshall-Inseln"
    },
    "MQ":{
      "name":"Martinique"
    },
    "MR":{
      "name":"Mauritania"
    },
    "MU":{
      "name":"Mauritius"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"Mexiko"
    },
    "FM":{
      "name":"Micronesien"
    },
    "MD":{
      "name":"Moldova, Republik"
    },
    "MC":{
      "name":"Monaco"
    },
    "MN":{
      "name":"Mongolei"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Marokko"
    },
    "MZ":{
      "name":"Mosambique"
    },
    "MM":{
      "name":"Myanmar"
    },
    "NA":{
      "name":"Namibia"
    },
    "NR":{
      "name":"Nauru"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Niederlande"
    },
    "AN":{
      "name":"Niederländische Antillen"
    },
    "NC":{
      "name":"Neu-Kaledonien"
    },
    "NZ":{
      "name":"Neuseeland"
    },
    "NI":{
      "name":"Nicaragua"
    },
    "NE":{
      "name":"Niger"
    },
    "NG":{
      "name":"Nigeria"
    },
    "NU":{
      "name":"Niue"
    },
    "NF":{
      "name":"Norfolk-Insel"
    },
    "MP":{
      "name":"Nördliche Mariana-Inseln"
    },
    "NO":{
      "name":"Norwegen"
    },
    "OM":{
      "name":"Oman"
    },
    "PK":{
      "name":"Pakistan"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Palästina, Besetztes Gebiet"
    },
    "PA":{
      "name":"Panama"
    },
    "PG":{
      "name":"Papua Neu-Guinea"
    },
    "PY":{
      "name":"Paraguay"
    },
    "PE":{
      "name":"Peru"
    },
    "PH":{
      "name":"Philippinen"
    },
    "PN":{
      "name":"Pitcairn"
    },
    "PL":{
      "name":"Polen"
    },
    "PT":{
      "name":"Portugal"
    },
    "PR":{
      "name":"Puerto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"Reunion"
    },
    "RO":{
      "name":"Rumänien"
    },
    "RU":{
      "name":"Russische Föderation"
    },
    "RW":{
      "name":"Rwanda"
    },
    "SH":{
      "name":"Saint Helena"
    },
    "KN":{
      "name":"Saint Kitts und Nevis"
    },
    "LC":{
      "name":"Saint Lucia"
    },
    "PM":{
      "name":"Saint Pierre und Miquelon"
    },
    "VC":{
      "name":"Saint Vincent und die Grenadinen"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"San Marino"
    },
    "ST":{
      "name":"Sao Tome und Principe"
    },
    "SA":{
      "name":"Saudi-Arabien"
    },
    "SN":{
      "name":"Senegal"
    },
    "CS":{
      "name":"Serbien und Montenegro"
    },
    "SC":{
      "name":"Seychellen"
    },
    "SL":{
      "name":"Sierra Leone"
    },
    "SG":{
      "name":"Singapur"
    },
    "SK":{
      "name":"Slowakei"
    },
    "SI":{
      "name":"Slowenien"
    },
    "SB":{
      "name":"Solomon-Inseln"
    },
    "SO":{
      "name":"Somalia"
    },
    "ZA":{
      "name":"Südafrika"
    },
    "GS":{
      "name":"Süd-Georgien und die südlichen Sandwich-Inseln"
    },
    "ES":{
      "name":"Spanien"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Sudan"
    },
    "SR":{
      "name":"Surinam"
    },
    "SJ":{
      "name":"Svalbard und Jan Mayen"
    },
    "SZ":{
      "name":"Swasiland"
    },
    "SE":{
      "name":"Schweden"
    },
    "CH":{
      "name":"Schweiz"
    },
    "SY":{
      "name":"Syrien, Arabische Republik"
    },
    "TW":{
      "name":"Taiwan, Republik China"
    },
    "TJ":{
      "name":"Tadschikistan"
    },
    "TZ":{
      "name":"Tansania"
    },
    "TH":{
      "name":"Thailand"
    },
    "TL":{
      "name":"Timor-Leste"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad und Tobago"
    },
    "TN":{
      "name":"Tunesien"
    },
    "TR":{
      "name":"Türkei"
    },
    "TM":{
      "name":"Turkmenistan"
    },
    "TC":{
      "name":"Turks- und Caicos-Inseln"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Uganda"
    },
    "UA":{
      "name":"Ukraine"
    },
    "AE":{
      "name":"Vereinigte Arabische Emirate"
    },
    "GB":{
      "name":"Vereinigtes Königreich"
    },
    "US":{
      "name":"USA"
    },
    "UM":{
      "name":"USA, Inseln"
    },
    "UY":{
      "name":"Uruguay"
    },
    "UZ":{
      "name":"Usbekistan"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Vietnam"
    },
    "VG":{
      "name":"Jungferninseln, britisch"
    },
    "VI":{
      "name":"Jungferninseln, amerikanisch"
    },
    "WF":{
      "name":"Wallis und Futuna"
    },
    "EH":{
      "name":"Westsahara"
    },
    "YE":{
      "name":"Jemen"
    },
    "ZM":{
      "name":"Sambia"
    },
    "ZW":{
      "name":"Simbabwe"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Kaufen",
  "sell":"Verkaufen",
  "weWillCallYou":"Wenn Sie diese Karte zum ersten Mal verwenden, müssen Sie zwei geheime Codes eingeben. Der eine befindet sich auf Ihrem Kontoauszug. Den anderen erhalten Sie über das Telefon.",
  "exchangerStatuses":{
        "TimeOut": "Ihre Anfrage ist abgelaufen.",
        "Error": "Es ist ein Fehler aufgetreten. Bitte kontaktieren Sie unser Support-Team unter support@indacoin.com",
        "WaitingForAccountCreation": "Warten auf Indacoin Kontoerstellung (wir haben Ihnen eine Registrierungsmail geschickt)",
        "CashinWaiting": "Ihre Zahlung wird verarbeitet",
        "BillWaiting": "Es wird darauf gewartet, dass der Kunde die Rechnung bezahlt",
        "Processing": "Die Anfrage verarbeitet.",
        "MoneySend": "Der Geldbetrag wurde gesendet",
        "Completed": "Die Anfrage wurde bearbeitet",
        "Verifying": "Überprüfung",
        "Declined": "Abgelehnt",
        "cardDeclinedNoFull3ds": "Ihre Karte wurde abgelehnt, da 3D-Secure nicht für Ihre Karte aktiviert ist. Bitte wenden Sie sich an den Kundenservice Ihrer Bank",
        "cardDeclined": "Ihre Karte wurde abgelehnt. Bei Fragen kontaktieren Sie bitte unser Support-Team untersupport@indacoin.com",
        "Non3DS": "Leider wurde Ihre Zahlung abgelehnt. Fragen Sie bei Ihrer Bank nach, ob 3D-Secure für Ihre Karte aktiviert ist (Verified by Visa oder MasterCard SecureCode). Dies beinhaltet, dass Sie bei Online-Zahlungen einen geheimen Code eingeben. Der Code wird normalerweise an Ihr Telefon gesendet."
  },
  "exchanger_fields_ok":"Klicken, um Bitcoins zu kaufen",
  "exchanger_fields_error":"Beim Ausfüllen der Felder ist ein Fehler aufgetreten. Bitte überprüfen Sie alle Felder.",
  "bankRejected":"Die Transaktion wurde von Ihrer Bank abgelehnt. Bitte wenden Sie sich an Ihre Bank und versuchen Sie nochmals, die Kartenzahlung auszuführen",
  "wrongAuthCode":"Sie haben einen fehlerhaften Authentifizierungs-Code eingegeben. Bitte kontaktieren Sie unser Support-Team unter support@indacoin.com",
  "wrongSMSAuthCode":"Der Telefoncode, den Sie eingegeben haben, ist fehlerhaft.",
  "getCouponInfo":{
    "indo":"Der Gutschein ist ungültig",
    "exetuted":"Der Gutschein wurde bereits verwendet",
    "freeRate":"Der Rabattgutschein wurde aktiviert"
  },
  "ip":"Ihre IP wurde vorübergehend gesperrt",
  "priceChanged":"Der Preis hat sich geändert. Okay? Sie erhalten ",
  "exchange_go":"– Wechseln",
  "exchange_buy_btc":"– Bitcoins kaufen",
  "month":"Jan Feb Mrz Apr Mai Jun Jul Aug Sep Okt Nov Dez",
  "discount":"Rabatt",
  "cash":{
    "equivalent":" entspricht ",
    "card3DS":"Wir akzeptieren nur Karten mit aktiviertem 3D-Secure (Verified by Visa oder MasterCard SecureCode)",
    "cashIn":{
      "1": "Bankkarten (USD)",
      "2": "e-Zahlung",
      "3": "QIWI",
      "4": "Bashcomsnabbank",
      "5": "Novoplat-Terminals",
      "6": "Elecsnet-Terminals",
      "7": "Bankkarten (USD)",
      "8": "Alpha Click",
      "9": "Internationale Banküberweisung",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Money",
      "21": "Elecsnet",
      "22": "Kassira.net",
      "23": "Mobile Element",
      "24": "Svyaznoy",
      "25": "Euroset",
      "26": "PerfectMoney",
      "27": "Indacoin — intern",
      "28":	"CouponBonus",
      "29": "Yandex.Money",
      "30": "OKPay",
      "31": "Partnerprogramm",
      "33": "Payza",
      "35": "BTC-E code",
      "36": "Bankkarten (USD)",
      "37": "Bankkarten (USD)",
      "39": "Internationale Banküberweisung",
      "40": "Yandex.Money",
      "42": "QIWI (manuell)",
      "43": "UnionPay",
      "44": "QIWI (automatisch)",
      "45": "Zahlung über Telefon",
      "49": "LibrexCoin",
      "50": "Bankkarten (USD)",
      "51": "QIWI (automatisch)",
      "52": "Zahlung über Telefon",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Bitte korrekte crypto-adresse eingeben",
      "country": "Ungültiges Land",
      "alfaclick": "Ungültige Anmeldung",
      "pan": "Ungültige Kartennummer",
      "cardholdername": "Ungültige Daten",
      "fio": "Ungültige Daten",
      "bday": "Ungültiges Geburtsdatum",
      "city": "Ungültige Daten",
      "address": "Ungültige Adresse",
      "ypurseid": "Ungültige Wallet-Nummer",
      "wpurseid": "Ungültige Wallet-Nummer",
      "pmpurseid": "Ungültige Kontonummer",
      "payeer": "Ungültige Kontonummer",
      "okpayid": "Ungültige Wallet-Nummer",
      "purse": "Ungültige Wallet-Nummer",
      "phone": "Ungültige Telefonnummer",
      "btcaddress": "Ungültige Adresse",
      "ltcaddress": "Ungültige Adresse",
      "lxcaddress": "Ungültige Adresse",
      "ethaddress": "Ungültige Adresse",
      "properties": "Ungültige Details",
      "email": "Ungültige E-Mai-Adresse",
      "card_number": "Diese Karte wird nicht unterstützt Bitte überprüfen Sie die Nummer",
      "inc_card_number": "Ungültige Kartennummer",
      "inn": "Ungültige Nummer",
      "poms": "Ungültige Nummer",
      "snils": "Ungültige Nummer",
      "cc_expr": "Ungültiges Datum",
      "cc_name": "Ungültiger Name",
      "cc_cvv": "Ungültiger Prüfcode"
    }
  },
  "change": {
    "wallet": "Neue Geldbörse erstellen / Ich habe bereits ein Konto bei Indacoin",
    //"accept": "Ich stimme der Nutzervereinbarung und der AML-Richtlinie zu",
    "accept": "Ich stimme den Nutzungsbedingungen und Datenschutzbestimmungen zu",
    "coupone": "Ich habe einen Rabattgutschein",
    "watchVideo": "Das Video anschauen",
    "seeInstructions": "Hilfe",
    "step1": {
      "title": "Neue Bestellung",
      "header": "Geben Sie Ihre Zahlungsinformationen ein",
      "subheader": "Bitte teilen Sie uns mit, was Sie kaufen wollen"
    },
    "step2": {
      "title": "Zahlungsinformationen",
      "header": "Geben Sie Ihre Kreditkartendaten ein",
      "subheader": "Wir akzeptieren nur Zahlungen über Visa und MasterCard",
      "card": {
        "header": "Wo finde ich diesen Code?",
        "text": "Der Prüfcode befindet sich auf der Rückseite Ihrer Karte. Es handelt sich um einen dreistelligen Code."
      }
    },
    "step3": {
      "title": "Kontaktdaten",
      "header": "Bitte geben Sie Ihre Kontaktdaten ein",
      "subheader": "Wir möchten unsere Kunden besser kennenlernen"
    },
    "step4": {
      "title": "Überprüfung",
      "header": "Bitte geben Sie Ihren Code von der Zahlungsseite ein",
      "subheader": "Ihre Zahlungsinformationen"
    },
    "step5": {
      "title": "Bestellübersicht"
    },
    "step6": {
      "title": "Video aufnehmen",
      "header": "Bitte zeigen Sie Ihren Pass und Ihr Gesicht",
      "description": "Bitte zeigen Sie uns, dass wir Ihnen vertrauen können",
      "startRecord": "Bitte klicken Sie auf die Schaltfläche „Start“, um mit der Aufnahme zu beginnen",
      "stopRecord": "Bitte klicken Sie auf die Schaltfläche „Stopp“, um die Aufnahme zu beenden"
    },

    "nextButton": "Nächster",
    "previousButton": "Bisherige",
    "passKYCVerification": "KYC-Verifizierung",
    "soccer-legends": "Ich bin damit einverstanden, dass meine persönlichen Daten an die Firma Soccer Legends Limited übermittelt und verarbeitet werden",
    "withdrawalAmount": "Betrag, der dem Konto gutgeschrieben wird"
  },
  "form": {
    "tag": "{{{Währung}}}-Tag",
    "loading": "Laden, bitte warten",
    "youGive": "Sie geben",
    "youTake": "Du nimmst",
    "email": "Email",
    "password": "Passwort",
    //"cryptoAdress": "{{{name}}} Wallet-Adresse",
    "cryptoAdress": "Crypto Brieftasche Adresse",
    "externalTransaction": "Transaktions-ID",
    "coupone": "Ich habe einen Rabattcoupon",
    "couponeCode": "Gutscheincode",
    "cardNumber": "Kartennummer",
    "month": "MM",
    "year": "YY",
    "name": "Name",
    "fullName": "Vollständiger Name",
    "mobilePhone": "Mobiltelefon",
    "birth": "Ihr Geburtsdatum im Format MM.DD.YYYY",
    "videoVerification": "Videoüberprüfung",
    "verification": "Überprüfung",
    "status": "Status überprüfen",
    "login": { // TODO: translate
      "captcha": "Captcha",
      "reset": "Zurücksetzen",
      "forgot": "Passwort vergessen",
      "signIn": "Anmelden"
    },
    "registration": { // TODO: translate
        "create": "Erstellen"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Einfache Überprüfung",
      "title-2": "Mehr als 100 Altmünzen verfügbar",
      "title-3": "Keine versteckten Kosten",
      "title-4": "Keine Registration"
    },
    "title": "Wir arbeiten in mehr als 100 Ländern",
    "subtitle": "einschließlich {{{country}}}",
    "shortTitle": "Wir arbeiten in mehr als 100 Ländern",
    "text1": "Kartensegment",
    "popupText1": "Erstkäufer müssen nur die Telefonnummer verifizieren",
    "text2": "100 Altcoins werden unterstützt",
    "popupText2": "Sie können mehr als 100 vielversprechendste Kryptowährungen mit einer Kredit/Debitkarte kaufen",
    "text3": "Keine versteckten Kosten",
    "popupText3": "Sie erhalten genau die gleiche Menge an Altcoins, die vor dem Deal gezeigt wurde",
    "text4": "Keine Registration",
    "popupText4": "Sie brauchen sich nicht anzumelden, Ihre Kreditkarte wird an die von Ihnen angegebene Adresse gesendet"
  },
  "buying-segment": {
    "title": "Mehr als 500 000 Kunden",
    "sub-title": "nutzen seit 2015 Indacoin"
  },
  "phone-segment": {
      "title": "Laden Sie unsere Anwendung herunter",
      "text1": "Kaufen und speichern Sie mehr als 100 Kryptowährungen in einer Brieftasche",
      "text2": "Verfolgen Sie die Veränderungen Ihres Anlageportfolios",
      "text3": "Das Senden von Kryptowährung an Ihren Freund wurde so einfach wie das Senden einer Nachricht"
  },
  "social-segment": {
    "buttonText": "Lesen Sie auf Facebook",
    "on": "auf Facebook",
    "facebook-review-1": "gute Bitcoins Seite, schneller und einfacher Prozess, empfehle ich ihnen",
    "facebook-review-2": "Brilliant Service und praktisch zu bedienen, die Leute dort sind freundlich und sehr hilfsbereit. Obwohl es eine digitale Transaktion ist, gibt es eine Person auf der anderen Seite (die eine massive Hilfe sein kann) und beruhigend sicher, 5-Sterne-Service, den ich wieder verwenden werde",
    "facebook-review-3": "Da ich nur anfange, alles zu kosten, was die Welt der Kryptowährung umgibt, ist der Austausch auf ihrer Website mehr als einfach. und ihre Unterstützung für den Fall, dass Sie stecken bleiben oder einfach nur nach Hilfe greifen, ist weit mehr als erstaunlich.",
    "facebook-review-4": "Sehr schnelle Bezahlung, tolle Unterstützung und tolle Wechselkurse.",
    "facebook-review-5": "Ich entschied mich, Bitcoins im Wert von 100 Euro mit diesem Service zu kaufen, und es war einfach, mit der Zahlung fortzufahren. Obwohl ich einige Probleme und Fragen hatte, wurde mir fast sofort von ihrem Support-Team geholfen. Es hat mir das Gefühl gegeben, Bitcoin mit Indacoin zu kaufen. Und ich plane, in Zukunft mehr zu kaufen.",
    "facebook-review-6": "Ich habe gerade Indacoin verwendet, um eine Transaktion für den Bitcoin-Kauf zu tätigen. Die Transaktion verlief reibungslos, gefolgt von einem Bestätigungsanruf. Es ist sehr einfach zu bedienen und ein benutzerfreundliches Portal für den Kauf von Bitmünzen. Ich würde diese Plattform zum Kauf von Bitcoins empfehlen !!",
    "facebook-review-7": "Es ist schwierig, eine einfache Kreditkartentransaktion zu finden, um kryptographische Währung zu kaufen, und Indcoin behandelte mich sehr gut, Geduld mit Benutzern aus anderen Ländern, die ihre Sprache nicht verstehen, Online-Chat der Bereitschaft. Indcoin Danke !!!",
    "facebook-review-8": "Schnell leicht. Online-Support war sehr schnell und hilfreich. ProTip: Die Nutzung der App reduziert Gebühren um 9%.",
    "facebook-review-9": "Dies ist das zweite Mal, dass ich Indacoin nutze - Toller Service, schnelle Transaktion brillanter Kundensupport. Ich werde auf jeden Fall wieder verwenden.",
    "facebook-review-10": "Sehr guter Service und Kommunikation, Schritt für Schritt Hilfe mit dem Live-Chat und absolut zuverlässig, sie rief mich ohne mich fragen und erklärte mir, wie man Verifizierung für die Transaktion, einfachste Weg, Bitcoin ohne Probleme überhaupt zu kaufen, danke Indacoin wird viel mehr Transaktionen mit Ihnen machen.",
    "facebook-review-11": "Großer Service, schneller Prozess. Hilfreiche Kundenbetreuung. Schnell reagiert. Einfache Möglichkeit, Kryptowährung zu kaufen. Unter anderem Service, ehrlich gesagt, das ist der einzige Service, den ich benutze, um Kryptowährung zu kaufen. Ich vertraue ihnen, meinen Einkauf zu verarbeiten. Empfohlen für alle, die sich mit dem Kauf von Kryptowährung beschäftigen. Last but not least, nur ein Wort für Indacoin: Ausgezeichnet !!!",
    //"facebook-review-12": "IndaCoin hat super einfachen, zuverlässigen und schnellen Service. Sie waren auch extrem schnell bei der Beantwortung all meiner Fragen und halfen mir mit meiner Überweisung. Ich würde ihre Dienste jedem empfehlen! 10/10!",
    "facebook-review-12": "Ich kaufte Bitcoins für 200 Euro und die Website ist legit. Obwohl es einige Zeit für die Transaktion dauerte, ist der Kundenservice ausgezeichnet. Ich habe mich an ihre Kundenbetreuung gewandt und sie haben mein Problem sofort behoben. Empfehlen Sie diese Seite unbedingt jedem. manchmal aufgrund des hohen Verkehrs die Transaktionszeit liegt zwischen 8-10 Stunden. Keine Panik, wenn Sie Ihre Münzen nicht sofort erhalten, manchmal dauert es einige Zeit, aber Sie werden sicher Ihre Münzen bekommen.",
    "facebook-review-13": "Toller Job Indacoin. Hatte die beste Erfahrung bis jetzt in Münzen mit meiner Kreditkarte mit Indacoin .. Der Berater Andrew hat einen tollen Kundenservice, und ich war beeindruckt, wie die Übertragung zu weniger als 3 Minuten ... Wird weiterhin mit ihnen :)",
    "facebook-review-14": "Indacoin ist eine einfache und benutzerfreundliche Oberfläche, mit der Käufer Bitcoins auf ihrer Plattform einfacher kaufen können. Es ist einfach, mit Schritt für Schritt hilft von den Berater-Agenten und sie sind wirklich bereit, Ihnen zu helfen, Ihr Problem sofort zu lösen. Ich werde definitiv Leute zu Indacoin verweisen, und sie aktualisieren mich ständig über meine Situationen, und sie sind bereit, zusätzliche Meile zu gehen, um Ihre Probleme zu lösen. Danke Indacoin!",
    "facebook-review-15": "Der einfachste und schnellste Weg, Kryptowährung zu kaufen, die ich bei vielen Suchen gefunden habe. Gute Arbeit, Jungs, danke!",
    "facebook-review-16": "Reallly einfacher Weg, Münzen online zu kaufen. Toller Kundenservice auch. Es ist fantastisch, sie haben auch alte Münzen direkt zu kaufen. Nimmt die Komplikationen aus dem Austausch. Ich kann es allen nach meiner ersten positiven Erfahrung empfehlen.",
    "facebook-review-17": "Guter Service. Benutzerfreundlich und einfach, verschiedene Münzen / Tokens zu kaufen.",
    "facebook-review-18": "Dies ist eine der besten Plattformen, um BTC auszutauschen. Ich habe mehrere Tauschgeschäfte gemacht und war schnell.",
    "facebook-review-19": "Großartiger Service, benutzerfreundlich, einfach zu kaufen verschiedene Kryptowährung und ERC20-Token mit Ihrer Kredit- oder Debitkarte. Sehr schnell und auch sicher! Der Service und die Plattform, nach der ich gesucht habe! Total empfehlen, Indacoin zu verwenden!"
  },
  "cryptocurrencies-segment": {
      "title": "Mehr als {{{count}}} verschiedene Kryptowährungen sind auf {{{name}}} erhältlich"
  },
  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin in Ihrem Handy",
    "text": "Unser Smart-Wallet Indacoin ist für Nutzer von Android, iOS und jedem Internet-Browser verfügbar. Kryptowährung an Ihren Freund senden wurde so einfach wie das Senden einer Nachricht"
  },

  "news-segment": {
    "no-news": "Es gibt keine Neuigkeiten für Ihre Sprache",
    "news-error": "Wir entschuldigen uns, die Nachricht ist zur Zeit nicht verfügbar",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },

  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "Diese kryptowährung ist derzeit nicht verfügbar, wird aber bald zurück sein",
    "unknown": "Unbekannter Serverfehler",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "Limit hat überschritten",
    "phone": "Phone is not supported",
    "mainPageError1": "Leider sind Informationen zu verfügbaren Kryptowährungen nicht verfügbar",
    "mainPageError2": "Versuchen Sie, die Website ein wenig später zu besuchen",
    "page-404": "Wir entschuldigen uns, Seite nicht gefunden",
    "page-error": "Wir entschuldigen uns, der Fehler ist aufgetreten"
  },
  "tags": {
    "title-btc": "Tausche Bitcoin BTC gegen {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "[cryptocurrency] mit einer Kredit- oder Debitkarte in [country] kaufen - Indacoin",
    "title2": "Bitcoin mit einer Kredit- oder Debitkarte weltweit kaufen - Indacoin",
    "title3": "{{{cryptocurrency}}} mit einer Kredit- oder Debitkarte in {{{country}}} kaufen - Indacoin",
    "title4": "Kaufen Sie Cryptocurrency mit Kredit- oder Debitkarte in {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Indacoin Partnerprogramm",
      "news": "Indacoin neuesten Nachrichten",
      "help": "Kryptowährungsaustausch F.A.Q. - Indacoin",
      "api": "Cryptocurrency Austausch-API - Indacoin",
      "terms-of-use": "Nutzungsbedingungen & AML-Richtlinien - Indacoin",
      "login": "Einloggen - Indacoin",
      "register": "Anmelden - Indacoin",
      "locale": "Region ändern - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) Preisdiagramme & Handelsvolumen - Indacoin"
    },
    "descriptions": {
      "affiliate": "Mit dem Indacoin Affiliate-Programm können Sie bis zu 3% eines jeden Kaufs mit einer Kredit- / Debitkarte verdienen. Die Vergütung wird an Ihre Indacoin Bitcoin Wallet gesendet",
      "news": "Besuchen Sie unseren offiziellen Nachrichtenblog, um mit den neuesten Nachrichten über die Kryptowährungsindustrie in Kontakt zu bleiben! Viele nützliche Informationen und umfassende Kommentare von Indacoin-Experten an einem Ort!",
      "help": "Finden Sie die Antworten auf all Ihre Fragen zum Kauf, Verkauf und Austausch von Kryptowährung auf Indacoin.",
      "api": "Unsere API ermöglicht den sofortigen Kauf und Austausch von Kryptowährungen mit Visa und Mastercard. Überprüfen Sie die Integration und Beispiele.",
      "terms-of-use": "Allgemeine Information. Nutzungsbedingungen. Dienstleistungen zur Verfügung gestellt. Geldtransfer. Übertragung von Rechten. Kommissionen. AML-Richtlinie",
      "login": "Log dich ein oder erstelle ein neues Konto",
      "register": "Neuen Account erstellen",
      "locale": "Wählen Sie Ihre Region aus und wählen Sie diese aus. Wir werden uns bei Ihrem nächsten Besuch bei Indacoin an Ihre Region erinnern",
      "currency": "Aktuelle Preise für {{{fullCurrency}}} in USD mit Handelsvolumen und historischen Kryptowährungsinformationen"
    }
  },
  "usAlert": "Es tut uns leid, aber Indacoin ist nicht in den USA tätig",
  "wrongPhoneCode": "Sie haben einen falschen Code von Ihrem Telefon eingegeben",
  "skip": "Überspringen",
  "minSum": "Mindestbetrag zum Kauf",
  "currency": {
    "no-currency": "Für eine gegebene Kryptowährung ist das Diagramm nicht verfügbar",
    "presented": {
      "header": "Bitcoin wurde 2009 der Welt präsentiert und bleibt 2017 die wichtigste Kryptowährung der Welt"
    },
    "difference": {
      "title": "Wie unterscheidet sich Bitcoin von normalem Geld?",
      "header1": "Begrenzte Emission",
      "text1": "Die Gesamtzahl der Bitcoins, die abgebaut werden können, beträgt 21 Millionen. Mehr als 16 Millionen Bitcoins sind derzeit im Umlauf. Je beliebter es wird, desto teurer.",
      "header2": "Weltweite Währung",
      "text2": "Keine Regierung hat Kontrolle über Bitcoin. Es basiert nur auf Mathematik",
      "header3": "Anonymität",
      "text3": "Sie können Ihre Bitcoins jederzeit und in beliebiger Menge verwenden. Dies ist ein wahrer Befund im Gegensatz zur Überweisung von Geld von Ihrem Bankkonto, wenn Sie das Ziel der Zahlung sowie die Herkunft der Mittel erklären müssen.",
      "header4": "Geschwindigkeit",
      "text4": "Ihre Bank wird Zeit brauchen, Geld zu überweisen, besonders wenn es um große Werte geht. Bei Bitcoin dauert die Sende- und Empfangsroutine Sekunden.",
      "header5": "Transparenz",
      "text5": "Alle Bitcoin-Transaktionen sind bei Blockchain zu sehen."
    },
    "motivation": {
      "title": "Als erste Währung dieser Art hat sie die Vision des Geldes, wie wir es kennen, verändert. Inzwischen erreicht die Marktkapitalisierung von Bitcoin mehr als 70 Milliarden Dollar, was solche Giganten wie Tesla, General Motors und FedEx übertrifft"
    },
    "questions": {
      "header1": "Was ist eine digitale Währung und Kryptowährung?",
      "text1": "Cryptocurrency ist ein digitales Asset, das als Kryptographiemedium fungiert, um die Transaktionen zu sichern und die Erstellung zusätzlicher Einheiten der Währung zu kontrollieren. Es hat keine physische Form und ist dezentral im Gegensatz zu den üblichen Banksystemen",
      "header2": "Wie erfolgt die Bitcoin-Transaktion?",
      "text2": "Jede Transaktion hat 3 Dimensionen: Eingabe - der Ursprung von Bitcoin, der in der Transaktion verwendet wird. Es sagt uns, wo der aktuelle Besitzer es bekommen hat. Betrag - die Anzahl der verwendeten \"Münzen\". Ausgabe - die Adresse des Geldempfängers.",
      "header3": "Woher kam Bitcoin?",
      "text3": "Obwohl die Welt mit dem Namen des Erstellers von Bitcoin vertraut ist, ist \"Satoshi Nakamoto\" höchstwahrscheinlich ein Alias für einen Programmierer oder sogar eine Gruppe von solchen. Im Grunde stammt Bitcoin aus 31.000 Codezeilen, die von \"Satoshi\" erstellt wurden und nur von Software gesteuert werden.",
      "header4": "Wie funktioniert Bitcoin?",
      "text4": "Transaktionen treten zwischen Bitcoin Wallets auf und sind transparent. In der Blockchain kann jeder den Ursprung und den Lebenszyklus für jede \"Münze\" überprüfen. Es gibt jedoch keine \"Münze\". Bitcoin hat keine physische Form und es hat keine virtuelle Form, nur die Geschichte der Transaktionen.",
      "header5": "Was ist ein Bitcoin Wallet?",
      "text5": "Ein Bitcoin Wallet ist ein Programm, mit dem Sie Bitcoin verwalten können: Empfangen und senden Sie sie, Schecksaldo und Bitcoin-Rate. Obwohl es in solchen Brieftaschen keine \"Münze\" gibt, enthält es digitale Schlüssel, die für den Zugriff auf öffentliche Bitcoin-Adressen und \"signieren\" Transaktionen verwendet werden. Beliebte Brieftaschen: ",
    },
    "history": {
      "title": "Die kurze Geschichte von Bitcoin",
      "text1": "Der Legende nach beginnt Satoshi Nakamoto seine Arbeit an Bitcoin",
      "text2": "Erste Transaktion in Bitcoin. Der Bitcoin-Wert, basierend auf den Stromkosten, die zur Erzeugung von Bitcoin verwendet werden, wird für 1.306,03 BTC auf 1 USD festgelegt",
      "text3": "25% aller Bitcoins sind bereits generiert",
      "text4": "Dell und Windows beginnen Bitcoin zu akzeptieren",
      "text5": {
        "part1": "Der BTC-Preis steigt in einigen Monaten von 1.402 USD auf 19.209 USD.",
        "part2": "Bitcoin Rate erreicht am 18. Dezember 2017 20.000 USD pro Münze"
      },
      "text6": "Die Registrierung von bitcoin.org",
      "text7": {
        "part1": "Ein wahrhaft historischer Moment, wenn 10.000 BTC indirekt für den Kauf von 2 Pizzen verwendet werden. Heute können Sie 2500 Pepperoni-Pizzas für die gleiche Menge kaufen.",
        "part2": "Der Bitcoin-Preis steigt in nur 5 Tagen von 0,008 USD auf 0,08 USD für 1 BTC.",
        "part3": "Erste mobile Transaktion tritt auf"
      },
      "text8": {
        "part1": "US-Schatzamt klassifiziert BTC als konvertierbare, dezentrale virtuelle Währung.",
        "part2": "Der erste Bitcoin-Geldautomat in San Diego, Kalifornien.",
        "part3": "Der Bitcoin-Preis verdoppelt sich und erreicht 503,10 USD. Sie wächst im selben Monat auf 1.000 USD"
      },
      "text9": "US-Bundesrichter schließt aus, dass BTC \"Mittel im Sinne dieses Begriffs\" sind"
    },
    "graph": {
      "buy-button": "Kaufen"
    },
    "ethereum": {
      "presented": {
        "header": "Wir haben bereits Bitcoin, also warum brauchen wir Ethereum? Lass uns hinein graben! Bitcoin sorgt für faire und transparente Geldtransfers. Ether, die digitale Währung von Ethereum, kann die gleiche Rolle spielen. Die Funktionalität, die Ethereum bietet, ist jedoch viel mehr."
      },
      "questions": {
        "header1": "What is Ethereum?",
        "text1": "Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
        "header2": "Where did it come from?",
        "text2": "Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
        "header3": "What is ICO?",
        "text3": "Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
        "header4": "What is Ether?",
        "text4": "Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
        "header5": "What’s so special about “smart contracts” and what’s its difference from usual contracts?",
        "text5": "Each transaction has 3 dimensions: Input – the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount – the number of “coins” used. The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
        "header6": "How is Ethereum different from Bitcoin?",
        "text6": "Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens – Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
        "header7": "What are the most popular Ethereum wallets?",
        "text7": "An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets: "
      },
      "history": {
        "title": "The brief history of Ethereum",
        "text1": "Crowdsale is initiated to gather funds for creating Ethereum",
        "text2": "The Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history",
        "text3": "Ethereum gains 50% market share in initial coin offering",
        "text4": "Ethereum exchange rate reaches 400 USD (5,000% rise since January)",
        "text5": "The idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine",
        "text6": "30th of July platform goes online",
        "text7": "The DAO is hacked, Ethereum value drops from 21.5 USD to 8 USD. This leads to Ethereum being forked in two blockchains in 2017",
        "text8": "Enterprise Ethereum Alliance is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc",
        "text9": {
          "part1": "All cryptocurrencies drop after China bans ICO, but quickly recover afterward.",
          "part2": "Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "Sind Sie bereit, Ihre Kunden Krypto mit einer Kredit- / Debitkarte kaufen zu lassen? Bitte hinterlassen Sie eine E-Mail und wir werden uns umgehend melden",
        "input": "E-Mail-Addresse",
        "button": "Loslegen",
        "mail-success": "Vielen Dank. Wir werden dir antworten"
      },
      "api": {
        "title": "API-Integration",
        "text": "Mit unserer API können Sie Ihre Kunden Cryptocurrencies sofort von Ihrer Site kaufen lassen. Unsere auf Ihrer Webseite oder Ihrer mobilen App integrierte Anti-Fraud-Plattform kann auch als ein Tool verwendet werden, mit dem Sie die Zahlungen auf Ihrer Plattform überwachen und filtern können."
      },
      "affiliate": {
        "title": "Partnerprogramm",
        "text": "Lassen Sie sich belohnen, wenn Sie Indacoin anderen Kunden empfehlen. Sie werden bis zu 3% von den Käufen der Empfehlungen erhalten.",
        "block-1": {
          "title": "Wie kann ich dem Partnerprogramm von Indacoin beitreten?",
          "text": "Fühlen Sie sich frei, sich auf unserer Seite anzumelden."
        },
        "block-2": {
          "title": "Wie kann ich die auf meinem Konto gesammelten Beträge abheben?",
          "text": "Sie können BTC / ETH / andere altcoins auf Indacoin ohne Gebühr kaufen oder erhalten sie auf Ihre Bankkarte."
        },
        "block-3": {
          "title": "Was ist der Vorteil Ihres Partnerprogramms?",
          "text": "Auf unserer Plattform haben die Kunden die Möglichkeit, mehr als 100 verschiedene Münzen mit Kredit- / Debitkarten ohne Registrierung zu kaufen. Angesichts der Tatsache, dass die Einkäufe fast sofort ablaufen, können die Partner sofort ihren Gewinn erzielen."
        },
        "block-4": {
          "title": "Kann ich das Beispiel des Verweislinks sehen?",
          "text": "So sieht es natürlich aus: ({{{link}}})"
        }
      },
      "more": "Finde mehr heraus",
      "close": "Schließen"
    },
    "achievements": {
      "partnership-title": "Partnerschaft",
      "partnership-text": "Es gibt viele Möglichkeiten, mit Indacoin zusammenzuarbeiten. Das Indacoin Partnership Program richtet sich an kryptobezogene Unternehmen, die bereit sind, die Welt der Kryptowährungen mit dem traditionellen Fiat-Money-System zu verbinden, um das Engagement in der Kryptoindustrie zu erhöhen. Mit Indacoin können Sie Ihren Kunden mit den Kredit- / Debitkarten die individuelle Erfahrung des Einkaufskrypto bieten.",
      "block-1": {
        "header": "Vertrauenswürdige",
        "text": "Wir arbeiten seit 2013 auf dem Kryptomarkt und haben uns mit vielen angesehenen Unternehmen zusammengeschlossen, die in der Branche führend sind. Mehr als 500 000 Kunden aus mehr als 190 Ländern haben unseren Service genutzt, um Kryptowährungen zu kaufen."
      },
      "block-2": {
        "header": "Sichern",
        "text": "Wir haben eine innovative und hocheffiziente Anti-Fraud-Plattform mit einer Kerntechnologie geschaffen, die speziell für die Erkennung und Verhinderung von betrügerischen Aktivitäten für Kryptoprojekte entwickelt wurde."
      },
      "block-3": {
        "header": "Gebrauchsfertige Lösung",
        "text": "Unser Produkt wird bereits von den ICOs, Cryptocurrency Exchanges, Blockchain Platforms und Crypto Funds verwendet."
      },

      "header": "Verdiene Geld mit uns beim Durchbruch",
      "subheader": "Blockchain-Technologien",
      "text1": "{{{company}}} ist eine globale Plattform, auf der Menschen Bitcoin, Ethereum, Ripple, Waves und 100 andere Kryptowährungen sofort mit einer Kredit- oder Debitkarte kaufen können",
      "text2": "Der Service funktioniert seit 2013 fast überall auf der Welt, einschließlich, aber nicht beschränkt auf:",
      "text3": "Großbritannien, Kanada, Deutschland, Frankreich, Russland, Ukraine, Spanien, Italien, Polen, Türkei, Australien, Philippinen, Indonesien, Indien, Weißrussland, Brasilien, Ägypten, Saudi-Arabien, Vereinigte Arabische Emirate, Nigeria und andere"
    },
    "clients": {
      "header": "Mehr als 500 000 Kunden",
      "subheader": "seit 2013"
    },
    "benefits": {
      "header": "Unsere wichtigsten Vorteile für Unternehmen:",
      "text1": "Einzigartiger Service, mit dem Kunden Bitcoins ohne Registrierung kaufen und senden können. Kunden, die nach dem Empfehlungslink nach Indacoin kommen, müssen lediglich die Kartendetails und die Brieftaschenadresse eingeben, an die der Krypto gesendet werden soll. Daher werden potenzielle Kunden sofort einkaufen und unsere Partner profitieren davon.",
      "text2": "Bis zu 3% jeder Transaktion werden von unseren Partnern verdient. Außerdem erhalten Webmaster 3% aller zukünftigen Käufe der Empfehlung.",
      "text3": "Bitcoin und Bankkarten könnten verwendet werden, um das Einkommen abzuheben."
    },
    "accounts": {
      "header": "Begleiten Sie uns jetzt",
      "emailPlaceholder": "Geben sie ihre E-Mailadresse ein",
      "captchaPlaceholder": "Captcha",
      "button": "Loslegen"
    }
  },
  "faq": {
    "title": "Fragen und Antworten",
    "header1": "Allgemeine Fragen",
    "header2": "Kryptowährung",
    "header3": "Überprüfung",
    "header4": "Kommissionen",
    "header5": "Kauf",
    "question1": {
      "title": "Was ist Indacoin?",
      "text": "Wir sind ein Unternehmen im Bereich Kryptowährung, das seit 2013 in London, Großbritannien, tätig ist. Sie können unseren Service nutzen, um über 100 verschiedene Kryptowährungen per Kredit- / Debitkartenzahlung ohne Registrierung zu kaufen."
    },
    "question2": {
      "title": "Wie kann ich mit Ihnen Kontakt aufnehmen?",
      "text": "Sie erreichen unser Support-Team per E-Mail (support@indacoin.com), Telefon (+ 44-207-048-2582) oder über eine Live-Chat-Option auf unserer Webseite."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "Welche Kryptowährungen kann ich hier kaufen?",
      "text": "Sie können über 100 verschiedene Kryptowährungen kaufen, die alle auf unserer Website aufgeführt sind."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "Welche Länder können diesen Service nutzen?",
      "text": "Wir arbeiten mit allen Ländern, mit Ausnahme der USA (im Moment)"
    },
    "question7": {
      "title": "Kann ich Bitcoin hier verkaufen?",
      "text": "Nein, du kannst hier nur Bitcoins kaufen."
    },
    "question8": {
      "title": "Was ist Kryptowährung?",
      "text": "Verschlüsselungswährung ist eine digitale Währung, die Kryptografie für die Sicherheit verwendet, eine Funktion, die das Fälschen erschwert. Es wird von keiner zentralen Behörde herausgegeben und ist daher theoretisch immun gegen Eingriffe oder Manipulationen seitens der Regierung."
    },
    "question9": {
      "title": "Wo kann ich Kryptowährung speichern?",
      "text": "Sie können eine separate Brieftasche für jede Kryptowährung auf Ihrem PC einrichten oder Sie können unseren kostenlosen Brieftaschen-Dienst nutzen und mehr als 100 Kryptowährungen an einem Ort speichern."
    },
    "question10": {
      "title": "Was meinst du mit \"Bitcoin Adresse\"?",
      "text": "Es ist die Adresse Ihrer Bitcoin Wallet. Sie können auch die Adresse einer Person eingeben, an die Bitcoins gesendet werden sollen, oder eine Brieftasche auf Indacoin erstellen"
    },
    "question11": {
      "title": "Wie schnell ist die Kryptowährungstransaktion?",
      "text": "Cryptocurrency-Transaktionszeiten hängen davon ab, welche Kryptowährung Sie senden, Bitcoin-Transaktionen dauern etwa 15-20 Minuten und ermöglichen im Durchschnitt bis zu 1 Stunde für den Rest der Münzen."
    },
    "question12": {
      "title": "Was muss ich tun, um meine Karte zu bestätigen?",
      //"text": "Um die Überprüfung abzuschließen, müssen Sie den vierstelligen Code eingeben, den Sie per Telefonanruf erhalten. Für den zweiten Schritt werden Sie möglicherweise auch aufgefordert, sich einer Videoüberprüfung zu unterziehen, bei der Sie ein Video aufnehmen, indem Sie Ihr Gesicht zeigen und einen Scan oder ein Foto Ihres Ausweises / Passes / Führerscheins hochladen. Je nach Überprüfung kann dieser Schritt obligatorisch sein, da wir sicherstellen müssen, dass Sie der Karteninhaber sind."
      "text": "Um die Bestätigung abzuschließen, müssen Sie den vierstelligen Code eingeben, den Sie telefonisch erhalten. Für den zweiten Schritt werden Sie möglicherweise aufgefordert, sich einer Fotokontrolle zu unterziehen. Dazu müssen Sie ein Foto Ihres Passes / Ausweises und ein Selfie mit Ausweis und ein Stück Papier mit dem erforderlichen Text hochladen, um den Kauf zu bestätigen. Wir können alternativ nach einer Videoüberprüfung fragen, bei der Sie Ihr Gesicht aufzeichnen müssen, beispielsweise: \"Indacoin-Bestätigung für Krypto\". Zeigen Sie Ihren Personalausweis und die Karte, mit der Sie die Zahlung getätigt haben. Beim Anzeigen der Karte nur die letzten 4 Ziffern und den Namen anzeigen."
    },
    "question13": {
      "title": "Was passiert, wenn ich keine Videoverifizierung aufzeichnen kann?",
      "text": "Sie können den Link für die Bestellseite von Ihrem Mobilgerät aus öffnen und von dort aufzeichnen. Bei Problemen können Sie das benötigte Video auch als Anhang per E-Mail an support@indacoin.com senden. Stellen Sie sicher, dass Sie Ihre Bestellnummer angeben."
    },
    "question14": {
      "title": "Ich habe eine Bestellung gemacht und alle Details eingegeben, warum wird noch bearbeitet?",
      "text": "Wenn Sie länger als 2 Minuten gewartet haben, hat die Bestellung nicht zur Transaktion geführt. Es ist möglich, dass Ihre Karte nicht 3D-sicher ist und Sie es möglicherweise erneut versuchen müssen. Vergewissern Sie sich, dass Sie Ihren Kauf über den PIN-Code bei Ihrer Bank bestätigen, oder versuchen Sie, mit einer anderen Währung (EUR / USD) zu kaufen. Andernfalls, wenn Sie sicher sind, dass Ihre Karte 3Ds ist und das Ändern der Währung nicht hilft, dann müssen Sie Ihre Bank kontaktieren, sie blockiert möglicherweise Ihre Kaufversuche."
    },
    "question15": {
      "title": "Muss ich jedes Mal, wenn ich einen Kauf tätige, eine Verifizierung durchführen?",
      "text": "Sie müssen Ihre Karte nur einmal bestätigen und alle folgenden Transaktionen werden automatisch ausgeführt. Wir benötigen jedoch eine Bestätigung, wenn Sie sich für eine andere Bankkarte entscheiden."
    },
    "question16": {
      "title": "Meine Bestellung wurde von der Bank abgelehnt, was soll ich tun?",
      "text": "Sie müssen sich an Ihre Bank wenden und nach dem Grund für den Rückgang fragen, vielleicht können sie die Beschränkung aufheben, damit ein Kauf getätigt werden kann."
    },
    "question17": {
      "title": "Warum muss ich meine Bankkarte bestätigen oder ein Video senden?",
      "text": "Der Verifizierungsprozess schützt Sie vor Diebstahl, Hackerangriffen oder Betrügern. Dies funktioniert umgekehrt, wenn jemand versucht, einen Kauf mit der Bankkarte eines anderen zu tätigen."
    },
    "question18": {
      "title": "Wie kann ich darauf vertrauen, dass meine Daten und Karteninformationen sicher sind?",
      "text": "Wir geben Ihre Daten niemals ohne Ihre Zustimmung an Dritte weiter. Außerdem akzeptieren wir nur 3D-Secure-Karten, und wir verlangen niemals vertrauliche Daten wie den CCV-Code oder die vollständige Kartennummer. Sie geben solche Zahlungsdetails nur über Visa oder Mastercard Gateway-Terminals an. Wir erfassen nur die Informationen, die erforderlich sind, um sicherzustellen, dass Sie der Besitzer der Karte sind."
    },
    "question19": {
      "title": "Was ist dein Honorar?",
      "text": "Unsere Gebühr variiert von Kauf zu Kauf, da sie auf zahlreichen Faktoren basiert. Aus diesem Grund haben wir einen Taschenrechner erstellt, der Ihnen die genaue Höhe der Kryptowährung, die Sie erhalten, einschließlich aller Gebühren, mitteilen wird."
    },
    "question20": {
      "title": "Wie viel wird mir die Transaktion in Rechnung gestellt?",
      "text": "Ihnen wird nur der Betrag in Rechnung gestellt, den Sie bei Ihrer Bestellung in unserem Taschenrechner angegeben haben. Alle Gebühren sind in diesem Betrag enthalten."
    },
    "question21": {
      "title": "Haben Sie Rabatte?",
      "text": "Wir haben gelegentlich Rabatte, Sie können mehr Informationen von unseren Support-Mitarbeitern erhalten."
    },
    "question22": {
      "title": "Wie kaufe ich auf Indacoin Kryptowährung?", // TODO: link
      "text": "Stellen Sie sicher, dass Sie Ihre eigene, 3D-sichere Visa oder MasterCard verwenden und einen Führerschein, Personalausweis oder Reisepass haben, falls wir eine Videoüberprüfung benötigen. Dann folgen Sie bitte diesem Link ___ zu unserem Taschenrechner, wählen Sie die Kryptowährung, die Sie kaufen möchten, geben Sie den Betrag von Ihrer Karte in EUR / USD ein und geben Sie die Kryptoadresse ein (oder lassen Sie dieses Feld leer, wenn Sie in Indacoin Wallet einzahlen möchten). Füllen Sie die erforderlichen Felder aus und sobald Ihre Karte belastet ist, wird sich unser Support-Team mit Ihnen in Verbindung setzen, um Ihren Kauf zu bestätigen."
    },
    "question23": {
      "title": "Wie schnell wird meine Kryptowährung gesendet?",
      "text": "Normalerweise dauert es etwa 30 Minuten, nachdem Sie eine Bestellung aufgegeben oder die Verifizierung abgeschlossen haben (wenn Sie die Karte zum ersten Mal auf Indacoin verwenden)."
    },
    "question24": {
      "title": "Was sind die Mindest- und Höchsteinkäufe?",
      "text": "Sie haben das Limit von maximal $ 200 für die erste Transaktion, weitere $ 200 für die zweite Transaktion nach 4 Tagen des ursprünglichen Kaufs, $ 500 nach 7 Tagen und $ 2000 in 14 Tagen des ersten Kaufs. In einem Monat ab dem ersten Kauf sind bei der Bezahlung mit Ihrer Karte keine Grenzen gesetzt. Bitte beachten Sie, dass das Mindestlimit immer 50 US-Dollar beträgt."
    },
    "question25": {
      "title": "Welche Art von Karten werden akzeptiert?",
      "text": "Wir akzeptieren nur Visa und Mastercard mit 3D Secure."
    },
    "question26": {
      "title": "Was ist 3D sicher?",
      "text": "Die 3D Secure-Technologie besteht aus den Programmen Verified by Visa und MasterCard SecureCode. Nachdem Sie Ihre Kreditkartendaten in unserem Online-Shop eingegeben haben, erscheint ein neues Fenster, in dem Sie nach Ihrem persönlichen Sicherheitscode gefragt werden. Ihr Finanzinstitut wird die Transaktion innerhalb von Sekunden authentifizieren und bestätigen, dass Sie der Käufer sind."
    },
    "question27": {
      "title": "Mit welchen anderen Zahlungssystemen kann ich Bitcoins kaufen?",
      "text": "Momentan akzeptieren wir nur Visa / Master Kartenzahlungen auf unserer Plattform."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",  "api": {
          "describe-segment": {
            "title1": "Partnership functions",
            "header1": "Simple integration",
            "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
            "placeholder": "Ask a question to a consultant",
            "example": "See example"
          },
          "functions-segment": {
            "header": "Tight integration",
            "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
          },
          "partnership-segment": {
            "block1": {
              "title1": "Logic",
              "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
              "text2": "Further create transaction",
              "text3": "Forward him to indacoin URL, the user will pay and passes verification",
              "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
              "text5": "On your URL arrives a callback in case of failure or success of transaction",
              "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
              "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and in Base64 encoding",
              "text8": "Example of payment form is",
              "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
              "text10": "Possible statuses of transaction:",
              "title2": "Methods",
              "text11": "create transaction",
              "text12": " - create new transaction",
              "text13": "Parameters in json:",
              "text14": "user limit",
              "text15": "Parameters in json:",
              "text16": "get price",
              "text17": "- receive amount user will get",
              "text18": "Parameters in GET:",
              "text19": "transaction",
              "text20": "- receive info about last transactions",
              "text21": "Parameters in json:",
              "text22": "all parameters are optional",
              "text23": "transaction info",
              "text24": "- receive transaction's info",
              "text25": "Parameters in json:",
              "text26": "Example of how to create the request on PHP and Node.js",
              "text27": "Example of the request for URL",
              "text28": "To create the request you need to get transaction ID from the previous request",
              "text29": "Checking limit for current user",
              "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
              "text31": "Creating transaction on Indacoin",
              "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
              "text33": "Forwarding to Indacoin",
              "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
              "text35": "Processing client on Indacoin",
              "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
              "text37": "Check status by Callback or API",
              "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
            }
          },
          "secret-segment": {
            "title": "Ask our manager on {{{company}}} for a further information"
          },
          "statuses-segment": {
            "title": "Also, you can check the status here:"
          }
        },
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in GET:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": { // TODO: translate
    "message": "Betrag muss zwischen {{{limitsMin}}} {{{limitsCur}}} und {{{limitsMax}}} {{{limitsCur}}} sein",
    "message-only-min": "Betrag muss von {{{limitsMin}}} {{{limitsCur}}} sein"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">Diese Website verwendet Cookies, um sicherzustellen, dass Sie die beste Erfahrung auf unserer Website erhalten.</span> {{{link}}}",
      "accept-button": "Ich habs",
      "link": "Erfahren Sie mehr"
    },
    "success-video": {
      "text": "Vielen Dank! Jetzt haben wir den Verifizierungsprozess gestartet, der einige Zeit dauern kann. Wenn die Überprüfung abgeschlossen ist, werden die Krypto-Münzen an Sie geliefert"
    },
    "accept-agreement": {
      //"text": "Bitte geben Sie an, dass Sie die Benutzervereinbarung und die AML-Richtlinie gelesen und akzeptiert haben"
      "text": "Bitte geben Sie an, dass Sie die Nutzungsbedingungen und die Datenschutzrichtlinie gelesen haben und ihnen zustimmen"
    },
    "price-changed": {
      "header": "Preis geändert",
      "agree": "Zustimmen",
      "cancel": "Stornieren"
    },
    "neoAttention": "Aufgrund der Funktionen der NEO-Kryptowährung sind nur ganze Zahlen zum Kauf verfügbar",
    "login": { // TODO: translate
      "floodDetect": "Flood detect",
      "resetSuccess": "Reset password success. Please check your email",
      "wrongEmail": "This e-mail is not registered yet",
      "unknownError": "Server error. Please retry the request",
      "alreadyLoggedIn": "You are already logged in",
      "badLogin": "Wrong login or password",
      "badCaptcha": "Wrong captcha",
      "googleAuthenticator": "Bitte geben Sie den Google Authenticator-Code ein"
    },
    "registration": { // TODO: translate
      "unknownError": "Unknown error",
      "floodDetect": "Flood detect",
      "success": "Register success. Please check your email",
      "badLogin": "Wrong login or password",
      "badCaptcha": "Wrong captcha",

      "empty": "Nicht alle Felder sind ausgefüllt. Bitte füllen Sie alle aus",
      "repeatedRequest": "Die Bestätigung der Registrierung wurde an Ihre E-Mail-Adresse gesendet. Bitte stellen Sie sicher, dass Sie es erhalten haben. Nur für den Fall, überprüfen Sie auch einen Spam-Ordner",
      "badEmail": "Sie haben die falsche oder temporäre E-Mail-Adresse verwendet",
      "blockedDomain": "Bitte versuchen Sie eine andere E-Mail-Adresse zu registrieren"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "keine Ergebnisse gefunden",
  "locale": {
    "title": "Bitte wählen Sie Ihr Land"
  }
}