export default {
  'lang': 'fr',
  "language-select":{
    "language":{
      "de": "German",
      'ar': 'Arabic',
      'en': 'English',
      'es': 'Spanish',
      "fr": "French",
      "it": "Italian",
      "pt": "Portuguese",
      'ru': 'Russian',
      'tr': 'Turkish'
    } 
  },
  "recordVideo": {
    "send": "Envoyer",
    "stop": "Arrêtez",
    "record": "Record"
  },
  "links": {
    "terms-of-use": {
      "text": "J'accepte les {{{terms}}} et la {{{policy}}}",
      "link-1": "Conditions d'utilisation",
      "link-2": "politique de confidentialité"
    }
  },
  "exchangeOrder": {
    "step17": {
      "status-1": "En attente de votre paiement",
      "status-2": "Échanger",
      "status-2-sub": "Nous recherchons le meilleur tarif pour vous",
      "status-3": "Envoi à votre portefeuille",
      "transaction-completed": "Félicitations, votre transaction est terminée!",
      "rate": "Taux de change 1 BTC = {{{rate}}} {{{shortName}}}",
      "sentText": "Vous avez envoyé:",
      "toAddress": "au {{{cryptoAdress}}}",
      "receiveText": "Vous allez recevoir:"
    },
    "changeCrypto": {
      "send": "Envoyer {{{amount}}} BTC à l'adresse ci-dessous",
      "confirmation": "Après deux confirmations bitcoin ≈ {{{amount}}} {{{shortName}}} sera envoyé à votre adresse {{{wallet}}} ...",
      "copy": "Adresse de copie",
      "copied": "Copié dans le presse-papier"
    },
    "order": "Commande",
    "status": {
      "title": "Statut",
      "completed": "La requête a été exécutée"
    },
    "payAmount": {
      "title": "Payé"
    },
    "check": "Vérifier l’état de l’opération sur blockexplorer.net",
    "resend-phone-code": "Vous n'avez pas reçu d'appel?",
    "sms-code": "Veuillez répondre au téléphone et entrer le code à 4 chiffres ici",
    "registration": "Aller à la page d'inscription",

    "internalAccount-1": "Veuillez vérifier votre portefeuille Indacoin en",
    "internalAccount-link": "connectant",
    "internalAccount-2": "",
    "your-phone": "votre téléphone {{{phone}}}",
    "many-requests": "Pardon. À de nombreuses demandes de changement de téléphone"
  },
  "app":{
    "detect_lang":{
      "title":"Définir votre langue",
      "sub":"Merci de patienter..."
    }
  },
  "meta":{
    "description-btc": "(BTC/{{{shortName}}}) Achetez et échangez {{{longName}}} instantanément et 100 autres cryptomonnaies au meilleur prix dans {{{country}}}",
    "title":"Achetez des cryptodevises avec une carte de crédit-débit Indacoin",
    "description":"Achetez et échangez instantanément des [cryptocurrency], Ethereum, Litecoin, Ripplle et 100 autres devises numériques en euros ou dollars US.",
    "description-bitcoin":"Achetez et échangez n'importe quelle crypto-monnaie instantanément: Bitcoin, Ethereum, Litecoin, Ripple et 100 autres devises numériques pour EUR ou USD",
    "description-ethereum":"Achetez et échangez instantanément n'importe quelle cryptomonnaie: Ethereum, Bitcoin, Litecoin, Ripple et 100 autres devises numériques pour EUR ou USD",
    "description-litecoin":"Achetez et échangez n'importe quelle crypto-monnaie instantanément: Litecoin, Bitcoin, Ethereum, Ripple et 100 autres devises numériques pour EUR ou USD",
    "description-ripple":"Achetez et échangez n'importe quelle crypto-monnaie instantanément: Ripple, Bitcoin, Ethereum, Litecoin et 100 autres devises numériques pour EUR ou USD",

    "description2": "Achetez et échangez n'importe quelle monnaie crypto instantanément: Bitcoin, Ethereum, Litecoin, Ripple, et 100 autres devises numériques pour EUR ou USD",
    "description3": "Achetez et échangez n'importe quelle crypto-monnaie instantanément: Bitcoin, Ethereum, Litecoin, Ripple et 100 autres devises numériques avec Visa & Mastercard",
    "buy": "Achetez des",
    "subTitle": "avec une carte de crédit ou de débit",
    "more": "Dans plus de 100 pays dont",
    //"header": "Exchange Bitcoin à <span class='sub'>{{{name}}}</span>",
    "header": "Échangez instantanément <span class='sub'>BTC</span> à <span class='sub'>{{{shortName}}}</span>"
  },
  "fullpage": {
    "header": "ACHETEZ DES CRYPTODEVISES INSTANTANÉMENT",
    "header-alt": "Exchange Crypto-monnaie instantanément",
    "sub": "Indacoin offre un moyen facile d’acheter des Bitcoins et plus de 100 crypto-monnaies avec Visa et Mastercard.",
    "sub-alt": "Indacoin fournit un moyen facile d'échanger bitcoin à l'une des 100+ cryptocurrences différentes",
    "observe": "Voir ci-dessous les cryptodevises disponibles"
  },
  "navbar": {
    "main":"Page d’accueil",
    "btc": "Acheter des Bitcoins",
    "eth": "Acheter des Crypto-monnaie",
    "affiliate": "Partenariat",
    "news": "Nouveautés",
    "faq": "FAQ",
    "login": "Entrer",
    "registration": "S’inscrire",
    "platform": "Des produits",

    "logout": "Connectez - Out",
    "buy-instantly": "Acheter crypto instantanément",
    "trading-platform": "Plateforme d'échanges",
    "mobile-wallet": "Portefeuille mobile",
    "payment-processing": "API de traitement des paiements"
  },
  "currency-table": {
    "rank": "Rank",
    "coin": "Devise",
    "market": "Cours du marché",
    "price": "Prix",
    "volume": "Volume 24&#160;h",
    "change": "Augmentation 24&#160;h",
    "available-supply": "Approvisionnement disponible",
    "search-cryptocurrency": "Recherche de crypto-monnaie",

    "change-1h": "1H Changement",
    "change-24h": "1D Changement",
    "change-7d": "1W Changement"
  },
  "buy-blocks":{
    "give":"Vous donnez",
    "get":"Vous recevez"
  },
  "exchange-form": {
    "title": "Achat instantané de cryptodevises",
    "submit": "Échanger",
    "give": "Vous donnez",
    "get": "Vous recevez",
    "search": "Chercher",
    "error-page": "Nous nous excusons, mais l'achat est actuellement indisponible"
  },
  "pagination": {
    "page": "Page",
    "next": "Suivante",
    "prev": "Précédente",
    "first": "Revenir au début"
  },
  "sidebar":{
    "accept":{
      "title":"Nous acceptons les cartes de crédit",
      "button":"Acheter des cryptodevises"
    },
    "youtube":{
      "title":"Nous connaître",
      "play":"Démarrer"
    },
    "orders":{
      "title": "Gestion des commandes",
      "order": "Commande #",
      "confirm": "Code confirmation",
      "check": "Vérifier la commande",
      "new": "Faire une nouvelle commande",
      "or": "Ou"
    },
    "social":{
      "title":"Nous sommes sur les réseaux sociaux"
    }
  },
  "wiki":{
    "etymology":"Étymologie"
  },
  "loading": {
    "title": "Chargement",
    "sub": "Merci de patienter",
    "text": "Chargement"
  },
  "error": {
    "title": "Erreur",
    "sub": "Quelque chose n’a pas fonctionné, pas de panique",
    "text": "Nous avons rencontré une erreur inconnue, et nous sommes en train de la corriger. Merci de revenir plus tard.",
    "home": "Page d’accueil",
    "mainPageError1": "Malheureusement, les informations sur les crypto-monnaies disponibles ne sont pas disponibles",
    "mainPageError2": "Essayez de visiter le site un peu plus tard",
    "page-404": "Nous nous excusons, page introuvable",
    "page-error": "Nous nous excusons, une erreur est survenue"
  },
  "features":{
    "title":"Ce que vous pouvez<br><span class=\"offset\">faire sur <span class=\"blue\">Indacoin</span></span>",
    "column1":{
      "title":"Acheter des cryptodevises",
      "text":"Sur Indacoin, vous pouvez acheter plus de 100 crypto devises différentes instantanément avec une carte de crédit ou de débit"
    },
    "column2":{
      "title":"En savoir plus sur les cryptodevises",
      "text":"Nos guides vous aideront à comprendre les principes de base de n’importe quelle monnaie virtuelle"
    },
    "column3":{
      "title":"Prendre la décision d’investir",
      "text":"De puissants outils d’analyse vous permettent de saisir le meilleur moment pour entrer et sortir"
    },
    "column4":{
      "title":"Acheter des bitcoins sans inscription",
      "text":"Les bitcoins vous sont envoyés à l’adresse que vous avez indiquée dès que le règlement est effectué"
    },
    "column5":{
      "title":"Partagez vos idées",
      "text":"Participez aux discussions sur les dernières tendances crypto avec d’autres investisseurs confirmés"
    },
    "column6":{
      "title":"Conservez tous les actifs au même endroit",
      "text":"Vous pouvez conserver et gérer plus de 100 devises virtuelles dans un portefeuille Indacoin"
    }
  },
  "mockup":{
    "title":"PORTEFEUILLES MOBILES",
    "text":"Notre portefeuille Indacoin, simple et intelligent, fonctionne sur votre Android ou votre iPhone en complément de votre navigateur web. Maintenant, envoyer des cryptodevises sur le téléphone d’un ami est aussi simple qu’un texto."
  },
  "footer":{
    "column1":{
      "title":"Acheter",
      "link1":"Acheter Bitcoin",
      "link2":"Acheter Ethereum",
      "link3":"Acheter Ripple",
      "link4":"Acheter Bitcoin Cash",
      "link5":"Acheter Litecoin",
      "link6":"Acheter Dash"
    },
    "column2":{
      "title":"Information",
      "link1":"Nous connaître",
      "link2":"Questions",
      "link3":"Règles d’utilisation",
      "link4":"Instructions"
    },
    "column3":{
      "title":"Instruments",
      "link1":"API",
      "link2":"Changer de région",
      "link3":"Application mobile"
    },
    "column4":{
      "title":"Contacts"
    }
  },
  "country":{
    "AF":{
      "name":"Afghanistan"
    },
    "AX":{
      "name":"Îles Åland"
    },
    "AL":{
      "name":"Albanie"
    },
    "DZ":{
      "name":"Algérie"
    },
    "AS":{
      "name":"Samoa américaines"
    },
    "AD":{
      "name":"Andorre"
    },
    "AO":{
      "name":"Angola"
    },
    "AI":{
      "name":"Anguilla"
    },
    "AQ":{
      "name":"Antarctique"
    },
    "AG":{
      "name":"Antigua-et-Barbuda"
    },
    "AR":{
      "name":"Argentine"
    },
    "AM":{
      "name":"Armenie"
    },
    "AW":{
      "name":"Aruba"
    },
    "AU":{
      "name":"Australie"
    },
    "AT":{
      "name":"Autriche"
    },
    "AZ":{
      "name":"Azerbaidjan"
    },
    "BS":{
      "name":"Bahamas"
    },
    "BH":{
      "name":"Bahrein"
    },
    "BD":{
      "name":"Bangladesh"
    },
    "BB":{
      "name":"Barbade"
    },
    "BY":{
      "name":"Biélorussie"
    },
    "BE":{
      "name":"Belgique"
    },
    "BZ":{
      "name":"Belize"
    },
    "BJ":{
      "name":"Bénin"
    },
    "BM":{
      "name":"Bermudes"
    },
    "BT":{
      "name":"Bhoutan"
    },
    "BO":{
      "name":"Bolivie"
    },
    "BA":{
      "name":"Bosnie-Herzegovine"
    },
    "BW":{
      "name":"Botswana"
    },
    "BV":{
      "name":"Île Bouvet"
    },
    "BR":{
      "name":"Brésil"
    },
    "IO":{
      "name":"Territoires britanniques de l’Océan indien"
    },
    "BN":{
      "name":"Brunei Darussalam"
    },
    "BG":{
      "name":"Bulgarie"
    },
    "BF":{
      "name":"Burkina Faso"
    },
    "BI":{
      "name":"Burundi"
    },
    "KH":{
      "name":"Cambodge"
    },
    "CM":{
      "name":"Cameroun"
    },
    "CA":{
      "name":"Canada"
    },
    "CV":{
      "name":"Cap Vert"
    },
    "KY":{
      "name":"Îles Caïman"
    },
    "CF":{
      "name":"République centrafricaine"
    },
    "TD":{
      "name":"Tchad"
    },
    "CL":{
      "name":"Chili"
    },
    "CN":{
      "name":"Chine"
    },
    "CX":{
      "name":"Île Christmas"
    },
    "CC":{
      "name":"Îles Cocos (Keeling)"
    },
    "CO":{
      "name":"Colombie"
    },
    "KM":{
      "name":"Comores"
    },
    "CG":{
      "name":"Congo"
    },
    "CD":{
      "name":"Congo, République démocratique du"
    },
    "CK":{
      "name":"Îles Cook"
    },
    "CR":{
      "name":"Costa Rica"
    },
    "CI":{
      "name":"Côte d'Ivoire"
    },
    "HR":{
      "name":"Croatie"
    },
    "CU":{
      "name":"Cuba"
    },
    "CY":{
      "name":"Chypre"
    },
    "CZ":{
      "name":"République tchèque"
    },
    "DK":{
      "name":"Danemark"
    },
    "DJ":{
      "name":"Djibouti"
    },
    "DM":{
      "name":"La Dominique"
    },
    "DO":{
      "name":"République dominicaine"
    },
    "EC":{
      "name":"Équateur"
    },
    "EG":{
      "name":"Égypte"
    },
    "SV":{
      "name":"Salvador"
    },
    "GQ":{
      "name":"Guinée équatoriale"
    },
    "ER":{
      "name":"Érythrée"
    },
    "EE":{
      "name":"Estonie"
    },
    "ET":{
      "name":"Éthiopie"
    },
    "FK":{
      "name":"Îles Falkland (malouines)"
    },
    "FO":{
      "name":"Îles Féroé"
    },
    "FJ":{
      "name":"Fidjis"
    },
    "FI":{
      "name":"Finlande"
    },
    "FR":{
      "name":"France"
    },
    "GF":{
      "name":"Guyane française"
    },
    "PF":{
      "name":"Polynésie française"
    },
    "TF":{
      "name":"Terres australes et antarctiques françaises"
    },
    "GA":{
      "name":"Gabon"
    },
    "GM":{
      "name":"Gambie"
    },
    "GE":{
      "name":"Géorgie"
    },
    "DE":{
      "name":"Allemagne"
    },
    "GH":{
      "name":"Ghana"
    },
    "GI":{
      "name":"Gibraltar"
    },
    "GR":{
      "name":"Grèce"
    },
    "GL":{
      "name":"Groenland"
    },
    "GD":{
      "name":"Grenade"
    },
    "GP":{
      "name":"Guadeloupe"
    },
    "GU":{
      "name":"Guam"
    },
    "GT":{
      "name":"Guatemala"
    },
    "GG":{
      "name":"Guernesey"
    },
    "GN":{
      "name":"Guinée"
    },
    "GW":{
      "name":"Guinée-Bissau"
    },
    "GY":{
      "name":"Guyane"
    },
    "HT":{
      "name":"Haïti"
    },
    "HM":{
      "name":"Îles Heard-et-MacDonald"
    },
    "VA":{
      "name":"Vatican (état du)"
    },
    "HN":{
      "name":"Honduras"
    },
    "HK":{
      "name":"Hong Kong"
    },
    "HU":{
      "name":"Hongrie"
    },
    "IS":{
      "name":"Islande"
    },
    "IN":{
      "name":"Inde"
    },
    "ID":{
      "name":"Indonésie"
    },
    "IR":{
      "name":"Iran, République islamique d’"
    },
    "IQ":{
      "name":"Irak"
    },
    "IE":{
      "name":"Irlande"
    },
    "IM":{
      "name":"Île de Man"
    },
    "IL":{
      "name":"Israël"
    },
    "IT":{
      "name":"Italie"
    },
    "JM":{
      "name":"Jamaïque"
    },
    "JP":{
      "name":"Japon"
    },
    "JE":{
      "name":"Jersey"
    },
    "JO":{
      "name":"Jordanie"
    },
    "KZ":{
      "name":"Kazakhstan"
    },
    "KE":{
      "name":"Kenya"
    },
    "KI":{
      "name":"Kiribati"
    },
    "KP":{
      "name":"Corée République populaire démocratique de"
    },
    "KR":{
      "name":"Corée, République de"
    },
    "KW":{
      "name":"Koweit"
    },
    "KG":{
      "name":"Kirghistan"
    },
    "LA":{
      "name":"Laos, République populaire démocratique du"
    },
    "LV":{
      "name":"Lettonie"
    },
    "LB":{
      "name":"Liban"
    },
    "LS":{
      "name":"Lesotho"
    },
    "LR":{
      "name":"Liberia"
    },
    "LY":{
      "name":"Lybie, Républlique arabe"
    },
    "LI":{
      "name":"Liechtenstein"
    },
    "LT":{
      "name":"Lithuanie"
    },
    "LU":{
      "name":"Luxembourg"
    },
    "MO":{
      "name":"Macao"
    },
    "MK":{
      "name":"Macédoine, anciennement république yougoslave de"
    },
    "MG":{
      "name":"Madagascar"
    },
    "MW":{
      "name":"Malawi"
    },
    "MY":{
      "name":"Malaysie"
    },
    "MV":{
      "name":"Maldives"
    },
    "ML":{
      "name":"Mali"
    },
    "MT":{
      "name":"Malte"
    },
    "MH":{
      "name":"Îles Marshall"
    },
    "MQ":{
      "name":"Martinique"
    },
    "MR":{
      "name":"Mauritanie"
    },
    "MU":{
      "name":"Île Maurice"
    },
    "YT":{
      "name":"Mayotte"
    },
    "MX":{
      "name":"Mexique"
    },
    "FM":{
      "name":"États fédérés de Micronésie"
    },
    "MD":{
      "name":"Moldavie, République de"
    },
    "MC":{
      "name":"Monaco"
    },
    "MN":{
      "name":"Mongolie"
    },
    "MS":{
      "name":"Montserrat"
    },
    "MA":{
      "name":"Maroc"
    },
    "MZ":{
      "name":"Mozambique"
    },
    "MM":{
      "name":"Birmanie"
    },
    "NA":{
      "name":"Namibie"
    },
    "NR":{
      "name":"Nauru"
    },
    "NP":{
      "name":"Nepal"
    },
    "NL":{
      "name":"Pays-bas"
    },
    "AN":{
      "name":"Antilles néerlandaises"
    },
    "NC":{
      "name":"Nouvelle Calédonie"
    },
    "NZ":{
      "name":"Nouvelle Zélande"
    },
    "NI":{
      "name":"Nicaragua"
    },
    "NE":{
      "name":"Niger"
    },
    "NG":{
      "name":"Nigeria"
    },
    "NU":{
      "name":"Niue"
    },
    "NF":{
      "name":"Île Norfolk"
    },
    "MP":{
      "name":"Îles Mariannes du Nord"
    },
    "NO":{
      "name":"Norvège"
    },
    "OM":{
      "name":"Oman"
    },
    "PK":{
      "name":"Pakistan"
    },
    "PW":{
      "name":"Palau"
    },
    "PS":{
      "name":"Territoite palestinien occupé"
    },
    "PA":{
      "name":"Panama"
    },
    "PG":{
      "name":"Papouasie-Nouvelle-Guinée"
    },
    "PY":{
      "name":"Paraguay"
    },
    "PE":{
      "name":"Pérou"
    },
    "PH":{
      "name":"Philippines"
    },
    "PN":{
      "name":"Îles Pitcairn"
    },
    "PL":{
      "name":"Pologne"
    },
    "PT":{
      "name":"Portugal"
    },
    "PR":{
      "name":"Porto Rico"
    },
    "QA":{
      "name":"Qatar"
    },
    "RE":{
      "name":"La Réunion"
    },
    "RO":{
      "name":"Roumanie"
    },
    "RU":{
      "name":"Fédération de Russie"
    },
    "RW":{
      "name":"Rwanda"
    },
    "SH":{
      "name":"Sainte- Hélène"
    },
    "KN":{
      "name":"Saint-Christophe-et-Niévès"
    },
    "LC":{
      "name":"Sainte-Lucie"
    },
    "PM":{
      "name":"Saint Pierre et Miquelon"
    },
    "VC":{
      "name":"Saint-Vincent-et-les-Grenadines"
    },
    "WS":{
      "name":"Samoa"
    },
    "SM":{
      "name":"Saint-Marin"
    },
    "ST":{
      "name":"Sao Tomé-et-Principe"
    },
    "SA":{
      "name":"Arabie saoudite"
    },
    "SN":{
      "name":"Sénégal"
    },
    "CS":{
      "name":"Serbie et Monténégro"
    },
    "SC":{
      "name":"Seychelles"
    },
    "SL":{
      "name":"Sierra Leone"
    },
    "SG":{
      "name":"Singapour"
    },
    "SK":{
      "name":"Slovaquie"
    },
    "SI":{
      "name":"Slovénie"
    },
    "SB":{
      "name":"Îles Salomon"
    },
    "SO":{
      "name":"Somalie"
    },
    "ZA":{
      "name":"Afrique du Sud"
    },
    "GS":{
      "name":"Géorgie du Sud-et-les Îles Sandwich du Sud"
    },
    "ES":{
      "name":"Espagne"
    },
    "LK":{
      "name":"Sri Lanka"
    },
    "SD":{
      "name":"Soudan"
    },
    "SR":{
      "name":"Surinam"
    },
    "SJ":{
      "name":"Svalbard et Jan Mayen"
    },
    "SZ":{
      "name":"Swaziland"
    },
    "SE":{
      "name":"Suède"
    },
    "CH":{
      "name":"Suisse"
    },
    "SY":{
      "name":"Syrie, République arabe de"
    },
    "TW":{
      "name":"Taiwan, Province de Chine"
    },
    "TJ":{
      "name":"Tadjikistan"
    },
    "TZ":{
      "name":"Tanzanie, République unie de"
    },
    "TH":{
      "name":"Thaïlande"
    },
    "TL":{
      "name":"Timor oriental"
    },
    "TG":{
      "name":"Togo"
    },
    "TK":{
      "name":"Tokelau"
    },
    "TO":{
      "name":"Tonga"
    },
    "TT":{
      "name":"Trinidad et Tobago"
    },
    "TN":{
      "name":"Tunisie"
    },
    "TR":{
      "name":"Turquie"
    },
    "TM":{
      "name":"Turkmenistan"
    },
    "TC":{
      "name":"Îles Turks-et-Caïcos"
    },
    "TV":{
      "name":"Tuvalu"
    },
    "UG":{
      "name":"Ouganda"
    },
    "UA":{
      "name":"Ukraine"
    },
    "AE":{
      "name":"Émirats arabes unis"
    },
    "GB":{
      "name":"Royaume uni"
    },
    "US":{
      "name":"États unis"
    },
    "UM":{
      "name":"Îles mineures éloignées des États-Unis"
    },
    "UY":{
      "name":"Uruguay"
    },
    "UZ":{
      "name":"Ouzbékistan"
    },
    "VU":{
      "name":"Vanuatu"
    },
    "VE":{
      "name":"Venezuela"
    },
    "VN":{
      "name":"Viêt Nam"
    },
    "VG":{
      "name":"Îles Vierges britanniques"
    },
    "VI":{
      "name":"Îles Vierges des États-Unis"
    },
    "WF":{
      "name":"Wallis et Futuna"
    },
    "EH":{
      "name":"Sahara occidental"
    },
    "YE":{
      "name":"Yémen"
    },
    "ZM":{
      "name":"Zambie"
    },
    "ZW":{
      "name":"Zimbabwe"
    }
  },
  "currency":{
    "RUB":"RUB",
    "BTC":"BTC",
    "USD":"USD"
  },
  "buy":"Acheter",
  "sell":"Vendre",
  "weWillCallYou":"Si vous utilisez cette carte pour la première fois, il vous faut saisir 2 codes secrets&#160;: celui de votre relevé bancaire et celui de l’appel de l’auto-informateur sur votre téléphone.",
  "exchangerStatuses":{
        "TimeOut": "Le délai de la requête est dépassé",
        "Error": "Une erreur s’est produite, merci d’écrire au service Assistance support@indacoin.com",
        "WaitingForAccountCreation": "En attente de création de compte Indacoin (nous vous avons envoyé un mail d'enregistrement)",
        "CashinWaiting": "Votre paiement est en cours de traitement",
        "BillWaiting": "En attente du paiement de la facture par le client",
        "Processing": "La requête est en cours de traitement",
        "MoneySend": "Fonds envoyés",
        "Completed": "La requête a été exécutée",
        "Verifying": "Vérification",
        "Declined": "Refusé",
        "cardDeclinedNoFull3ds": "Votre carte a été refusée parce que votre banque ne dispose pas de l’option 3D sur cette carte, adressez-vous au service clients de votre banque",
        "cardDeclined": "Votre carte a été refusée, si vous avez des questions, adressez-vous au service Assistance support@indacoin.com",
        "Non3DS": "Malheureusement, votre paiement a été refusé. Assurez-vous auprès de votre banque que vous utilisez une carte avec la sécurité 3D (vérifiée par un code de sécurité Visa ou MasterCard), ce qui implique de saisir à chaque paiement sur l’internet un code secret, qui arrive habituellement sur votre téléphone."
  },
  "exchanger_fields_ok":"Cliquez pour acheter des bitcoins",
  "exchanger_fields_error":"Erreur de remplissage. Vérifiez tous les champs",
  "bankRejected":"La transaction a été refusée par votre banque. Merci de contacter votre banque et de renouveler le paiement par carte",
  "wrongAuthCode":"Vous avez saisi un code d’autorisation incorrect, merci de vous adresser au service Assistancesupport@indacoin.com",
  "wrongSMSAuthCode":"Vous avez saisi un code incorrect sur votre téléphone",
  "getCouponInfo":{
    "indo":"Ce coupon n’est pas disponible",
    "exetuted":"Ce coupon est déjà utilisé",
    "freeRate":"Coupon de réduction activé "
  },
  "ip":"Votre adresse IP a été temporairement bloquée",
  "priceChanged":"Le prix a changé. OK&#160;? Vous recevrez ",
  "exchange_go":"– Échanger",
  "exchange_buy_btc":"– Acheter des bitcoins",
  "month":"Jan Fév Mar Avr Mai Juin Juil  Aou Sep Oct Nov Déc",
  "discount":"Remise",
  "cash":{
    "equivalent":" Équivalent ",
    "card3DS":"Nous n’acceptons pour le paiement que les cartes sécurisées 3D (vérifiées par code Visa ou MasterCard)",
    "cashIn":{
      "1": "Cartes bancaires (USD)",
      "2": "Terminaux de paiement",
      "3": "QIWI",
      "4": "Bashkomsnabbank",
      "5": "Terminal Novoplat",
      "6": "Terminal Elexnet",
      "7": "Cartes bancaires (USD)",
      "8": "Alfa-klik",
      "9": "Virement bancaire international",
      "10": "Bitcoin",
      "12": "Litecoin",
      "13": "Astropay",
      "15": "QIWI",
      "16": "USD",
      "17": "Pinpay",
      "18": "Payeer",
      "19": "Liqpay",
      "20": "Yandex.Dengi",
      "21": "Elexnet",
      "22": "Kassira.net",
      "23": "Mobil Element",
      "24": "Sviaznoï",
      "25": "Euroset",
      "26": "PerfectMoney",
      "27": "Indacoin interne",
      "28":	"CouponBonus",
      "29": "Yandex.Dengi",
      "30": "OKPay",
      "31": "Programme partenaire",
      "33": "Payza",
      "35": "Code BTC",
      "36": "Cartes bancaires (USD)",
      "37": "Cartes bancaires (USD)",
      "39": "Virement bancaire international",
      "40": "Yandex.Dengi",
      "42": "QIWI (manuel)",
      "43": "UnionPay",
      "44": "QIWI (automatique)",
      "45": "Paiement sur un compte téléphone",
      "49": "LibrexCoin",
      "50": "Cartes bancaires (EURO)",
      "51": "QIWI (automatique)",
      "52": "Paiement sur un compte téléphone",
      "53": "Ethereum",
      "54": "RUB",
      "55": "AUD",
      "56": "GBP"
    },
    "tip":{
      "correctCrypto": "Veuillez entrer une adresse cryptographique correcte",
      "country": "Pays non valide",
      "alfaclick": "Login non valide",
      "pan": "Numéro de carte non valide",
      "cardholdername": "Données invalides",
      "fio": "Données invalides",
      "bday": "Date de naissance incorrecte",
      "city": "Données invalides",
      "address": "Adresse non valide",
      "ypurseid": "Numéro de portefeuille non valide",
      "wpurseid": "Numéro de portefeuille non valide",
      "pmpurseid": "Numéro de compte non valide",
      "payeer": "Numéro de compte non valide",
      "okpayid": "Numéro de portefeuille non valide",
      "purse": "Numéro de portefeuille non valide",
      "phone": "Téléphone non valide",
      "btcaddress": "Adresse non valide",
      "ltcaddress": "Adresse non valide",
      "lxcaddress": "Adresse non valide",
      "ethaddress": "Adresse non valide",
      "properties": "Coordonnées non valides",
      "email": "Email non valide",
      "card_number": "Carte non acceptée, vérifiez le numéro",
      "inc_card_number": "Numéro de carte inexact",
      "inn": "Numéro incorrect",
      "poms": "Numéro incorrect",
      "snils": "Numéro incorrect",
      "cc_expr": "Date inexacte",
      "cc_name": "Nom inexact",
      "cc_cvv": "cvv non valide"
    }
  },
  "change": {
    "wallet": "Créer un nouveau portefeuille / J'ai déjà un compte Indacoin",
    //"accept": "J'accepte les conditions d'utilisation et la politique de lutte contre le blanchiment d'argent",
    "accept": "J'accepte les conditions d'utilisation et la politique de confidentialité",
    "coupone": "J’ai un coupon",
    "watchVideo": "Voir la vidéo",
    "seeInstructions": "Aide",
    "step1": {
      "title": "Création de commande",
      "header": "Saisissez les informations de paiement",
      "subheader": "Veuillez nous indiquer ce que vous souhaitez acquérir"
    },
    "step2": {
      "title": "Informations de paiement",
      "header": "Entrez les informations de votre carte bancaire",
      "subheader": "Nous n’acceptons que Visa et MasterCard",
      "card": {
        "header": "Où puis-je trouver ce code&#160;?",
        "text": "Vous trouverez ce code au verso de la carte. Ce code se compose de 3 chiffres"
      }
    },
    "step3": {
      "title": "Informations de contact",
      "header": "Entrez vos coordonnées de contact",
      "subheader": "Permettez-nous de mieux connaître nos clients"
    },
    "step4": {
      "title": "Vérification",
      "header": "Entrez votre code depuis la page de paiement",
      "subheader": "Vos informations de paiement"
    },
    "step5": {
      "title": "Informations sur la commande"
    },
    "step6": {
      "title": "Enregistrement vidéo",
      "header": "Montrez votre passeport et votre visage",
      "description": "Montrez-nous que nous pouvons faire affaire avec vous",
      "startRecord": "Appuyez sur le bouton Start pour commencer l’enregistrement",
      "stopRecord": "Appuyez sur Stop pour terminer l’enregistrement"
    },
    
    "nextButton": "Prochain",
    "previousButton": "Précédent",
    "passKYCVerification": "Vérification KYC",
    "soccer-legends": "J'accepte que mes données personnelles soient transférées et traitées à la société Soccer Legends Limited",
    "withdrawalAmount": "Montant à créditer sur le compte"
  },
  "form": {
    "tag": "Tag {{{currency}}}",
    "loading": "Chargement, veuillez patienter...",
    "youGive": "Nous voulons",
    "youTake": "Tu prends",
    "email": "Email",
    "password": "Mot de passe",
    //"cryptoAdress": "Crypto {{{name}}} Adresse",
    "cryptoAdress": "Adresse du portefeuille Crypto",
    "externalTransaction": "identifiant de transaction",
    "coupone": "J'ai un coupon de réduction",
    "couponeCode": "Code promo",
    "cardNumber": "Numéro de carte",
    "month": "MM",
    "year": "YY",
    "name": "prénom",
    "fullName": "Nom complet",
    "mobilePhone": "Téléphone portable",
    "birth": "Votre date de naissance au format MM.JJ.AAAA",
    "videoVerification": "Vérification vidéo",
    "verification": "Vérification",
    "status": "Vérifier le statut",
    "login": { // TODO: translate
      "captcha": "Captcha",
      "reset": "Réinitialiser",
      "forgot": "Mot de passe oublié",
      "signIn": "se connecter"
    },
    "registration": { // TODO: translate
        "create": "Créer"
    }
  },
  "topic1": {
    "paragraph1": "Bitcoin was presented to the world in 2009 and in 2017 it remains the primary cryptocurrency of the world. Being the firstborn currency of its kind, it managed to change the vision of money as we knew it. By now, Bitcoin market cap reaches more than $70 billion, which surpasses such giants as Tesla, General Motors, and FedEx.",
    "paragraph2": "What is a digital currency or cryptocurrency? Cryptocurrency is a digital asset, that works as a medium of exchange using cryptography to secure the transactions and to control the creation of additional units of the currency. It has no physical form and is decentralized as opposed to the usual banking systems.",
    "paragraph3": "Where did Bitcoin come from? Although the world is familiar with the name of the creator of Bitcoin, “Satoshi Nakamoto” is most likely an alias for a programmer or even a group of such. Basically, Bitcoin comes from 31,000 lines of code created by “Satoshi” and is controlled only by software.",
    "paragraph4": "How does Bitcoin work? Transactions occur between Bitcoin wallets and are transparent. In the blockchain, anyone can check the origin and life cycle for every “coin”. However, there is no “coin”. Bitcoin has no physical form and it has no virtual form, only history of transactions.",
    "paragraph5": "How is Bitcoin different from regular money? - Worldwide currency: no government has control over Bitcoin. It’s based on mathematics only - Transparency: all Bitcoin transactions can be seen at BlockChain. - Limited emission: the total number of Bitcoins that can be mined is 21M. More than 16M Bitcoin are in circulation right now. The more popular it gets, the more expensive. - Anonymity: you can use your Bitcoins anytime and in any amount. This is a true finding as opposed to transferring money from your bank account, when you need to explain the destination of payment as well as the origin of the funds. - Speed: your bank will take time to transfer money, especially when it comes to large values. With Bitcoin, the send-and-receive routine will take seconds.",
    "paragraph6": "How does Bitcoin transaction occur? Each transaction has 3 dimensions: input, amount, and output. Input –– the origin of Bitcoin used in the transaction. It tells us where the current owner got it. Amount –– the number of “coins” used. Output –– the address of the recipient of the money.",
    "paragraph7": "What is a Bitcoin wallet? A Bitcoin wallet is a program that allows you to manage Bitcoin: receive and send them, check balance and Bitcoin rate. Although there is no “coin” in such wallets, it contains digital keys, used to access public Bitcoin addresses and “sign” transactions. Popular wallets: TREZOR, Ledger, Blockchain.info, Coinbase.",
    "paragraph8": "The brief history of Bitcoin 2007 –– according to the legend, this is when Satoshi Nakamoto starts his work on Bitcoin 2008, August –– the registration of the bitcoin.org. 2009, January –– first transaction made in Bitcoin. 2009, October –– Bitcoin value, based on the cost of electricity used to generate Bitcoin, is set at 1USD = 1.306.03BTC 2010, May –– a truly historical moment, when 10,000BTC is used to indirectly buy 2 pizzas. Today you can buy 2500 pepperoni pizzas for the same amount. 2010, July –– Bitcoin price shoots up from 0.008USD to 0.080USD for 1BTC in just 5 days. 2010, December –– first mobile transaction occurs. 2011, January –– 25% of all Bitcoins are already generated. 2013 –– U.S. Treasury classifies BTC as a convertible decentralized virtual currency. 2013, March –– Bitcoin market cap reaches 1,000,000USD. 2013, May –– the first Bitcoin ATM found in San Diego, California. 2013, November –– Bitcoin price doubles and reaches 503.10USD. It grows to 1,000USD the same month. 2014 –– Dell and Windows begin to accept Bitcoin. 2016, September –– U.S. federal judge rules out that BTC are “funds within the plain meaning of that term”. 2017, May –– BTC price increases from 1,402USD to 2,000USD in 20 days. 2017, August –– Bitcoin rate reaches 4,000USD per coin with its highest rate to the moment of 4,489.10USD per coin on the 21st of August.",
  },
  "topic2": {
    "paragraph1": "We already have Bitcoin, so why do we need Ethereum? Let’s dig into it! Bitcoin ensures fair and transparent money transfers. Ether, the digital currency of Ethereum, can play the same role. However, there’s much more to the functionality, offered by Ethereum.",
    "paragraph2": "What is Ethereum? Ethereum is an open-source platform, that uses blockchain technology to allow distributed computing. It is secured by smart contracts and features its own cryptocurrency, called “ether”, as means of payment.",
    "paragraph3": "Where did it come from? Ethereum was first offered as an idea at the end of 2013 by the Canadian programmer, Vitalik Buterin. Funded by a crowdsale that took place during the summer of 2014 the platform was finally presented to the world in July 2015.",
    "paragraph4": "How is Ethereum different from Bitcoin? Inspired by Bitcoin, Ethereum also uses Blockchain technology, being hosted by the personal computers of volunteer programmers from all over the world. For this job, they get value tokens –– Ether. Ethereum price is established by exchanges between people, just as it is with Bitcoin. The difference is huge though. Ethereum is, first of all, a platform for distributed computing. It provides an opportunity for programmers to create various decentralized apps (Dapps) on its base, get funds for operational expenses. This way no middle management is needed and so there are no additional expenses. Ethereum platform also uses smart contracts as ways of facilitation of relationships between parties and its own cryptocurrency, “ether”, as means of payment. It is fair to say that, though both Bitcoin and Ethereum are big and have some similarities, their purposes are initially different. The first is only a currency, while the second only uses Ether mainly for its internal purposes.",
    "paragraph5": "What’s so special about “smart contracts” and what’s its difference from usual contracts? The main goal of the smart contract is to make sure all terms of a contract are executed properly. Its decentralized functionality provides more security for Ethereum contributors, using unbiased applications, stored in the blockchain, to verify and enforce its terms. This technology leaves almost no room for fraud. And while for the usual contract you would generally need a third party (lawyer) to deal with any issues, such as breach, smart contracts are unbiased and use mathematics to provide security.",
    "paragraph6": "What is ICO? Many applications are built on Ethereum and they can also raise funds with Ether in a process that is called initial coin offering (ICO). The way it works is quite similar to what Kickstarter does. Programmers offer different tokens of value in exchange for funds (generally Ether) to cover the expenses for developing the app. When they reach a certain predetermined amount or a certain date occurs, the funds are released to programmers. If the goal fails to be met, funds go back to the initial contributors.",
    "paragraph7": "What is Ether? Ether (ETH) is a digital currency and a value token used in Ethereum. Apart from being the means of payment for different services on the Ethereum platform, it is also featured on cryptocurrency exchanges and can be changed for real money.",
    "paragraph8": "What are the most popular Ethereum wallets? An Ethereum wallet is a program that allows you to manage your cryptocurrency: receive and send them, check balance and Ethereum exchange rate. Popular wallets include Jaxx, KeepKey, Ledger Nano S, Mist.",
    "paragraph9": "Brief history of Ethereum 2013 –– the idea of Ethereum is offered by Vitalik Buterin, a Canadian programmer and founder of Bitcoin Magazine. 2014, summer –– crowdsale is initiated to gather funds for creating Ethereum. 2015, 30th of July –– platform goes online. 2016, May –– the Decentralized autonomous organization (DAO) is created on Ethereum platform. Funded by a token crowdsale, it is at the moment the largest crowdfunding campaign in history. 2016, June –– the DAO is hacked, Ethereum value drops from 21.50USD to 8USD. This leads to Ethereum being forked in two blockchains in 2017. 2017 –– Ethereum gains 50% market share in initial coin offering (ICO). 2017, March –– Enterprise Ethereum Alliance (EEA) is created. Among members are Toyota, Samsung, Microsoft, Intel, Deloitte and other quite big names. The main aim of the Alliance is to determine how Ethereum functionality can be used in various aspects of life, such as management, banking, health, technology etc. 2017, June –– Ethereum exchange rate reaches 400USD (5,000% rise since January). 2017, September –– all cryptocurrencies drop after China bans ICO, but quickly recover afterward. 2017, September –– Ethereum market cap reaches more than 36 billion dollars, its highest historical value, only to drop back to 30 billion after China’s ICO ban."
  },
  "buyPage": {
    "leftTitle": "Buy Bitcoin with Credit/Debit card"
  },
  "map-segment": {
    "blocks": {
      "title-1": "Vérification simple",
      "title-2": "Plus de 100 altcoins disponibles",
      "title-3": "Pas de frais cachés",
      "title-4": "Pas d'inscription"
    },
    "title": "Nous travaillons dans plus de 100 pays",
    "subtitle": "y compris {{{country}}}",
    "shortTitle": "Nous travaillons dans plus de 100 pays",
    "text1": "Vérification simple",
    "popupText1": "Les premiers acheteurs doivent simplement vérifier le numéro de téléphone",
    "text2": "100 altcoins sont supportés",
    "popupText2": "Vous pouvez acheter plus de 100 cryptocurrencies les plus prometteuses avec une carte de crédit / débit",
    "text3": "Pas de frais cachés",
    "popupText3": "Vous obtiendrez exactement le même nombre d'altcoins, qui a été montré avant l'affaire",
    "text4": "Pas d'inscription",
    "popupText4": "Vous n'avez pas besoin de vous inscrire, la cryptomonnaie sera envoyée à l'adresse que vous avez indiquée"
  },
  "buying-segment": {
    "title": "Plus de 500 000 clients",
    "sub-title": "utilise Indacoin depuis 2015"
  },
  "phone-segment": {
      "title": "Téléchargez notre application",
      "text1": "Achetez et stockez plus de 100 cryptocurrences dans un portefeuille",
      "text2": "Suivre les changements de votre portefeuille d'investissement",
      "text3": "L'envoi de crypto-monnaie à votre ami est devenu aussi facile que l'envoi d'un message"
  },
  "social-segment": {
    "buttonText": "Lire sur Facebook",
    "on": "sur Facebook",
    "facebook-review-1": "bonne page bitcoins, processus rapide et simple, je les recommande",
    "facebook-review-2": "Service génial et pratique à utiliser, les gens sont sympas et très serviables. Bien qu'il s'agisse d'une transaction numérique, il y a une personne de l'autre côté (ce qui peut être une aide MASSIVE) et rassurant à coup sûr, un service 5 étoiles que je vais utiliser à nouveau",
    "facebook-review-3": "Comme je commence seulement à goûter tout ce qui entoure le monde de la cryptomonnaie, faire un échange sur leur site web est plus que simple. et leur soutien dans le cas où vous rester coincé ou simplement tendre la main pour obtenir de l'aide est beaucoup plus qu'étonnant.",
    "facebook-review-4": "Paiement très rapide, excellent support et bon taux de change.",
    "facebook-review-5": "J'ai décidé d'acheter 100 euros de bitcoins en utilisant ce service, et il était facile de procéder au paiement. Bien que j'ai eu quelques problèmes et questions, j'ai été presque immédiatement aidé par leur équipe de soutien. Cela m'a fait me sentir beaucoup plus à l'aise d'acheter Bitcoin avec Indacoin. Et je prévois d'acheter plus dans le futur.",
    "facebook-review-6": "Je viens d'utiliser Indacoin pour faire une transaction pour l'achat de Bitcoin. La transaction s'est déroulée sans problème, suivie d'un appel de vérification. Son très facile à utiliser et un portail convivial pour l'achat de pièces de monnaie peu. Je recommanderais cette plate-forme pour l'achat de bitcoins !!",
    "facebook-review-7": "Il est difficile de trouver une facilité de transaction de carte de crédit facile à acheter de la monnaie cryptographique et Indcoin m'a très bien traité, la patience avec les utilisateurs d'autres pays qui ne comprennent pas leur langue, le chat en ligne de préparation. Indcoin Merci !!!",
    "facebook-review-8": "Rapide et facile Le support en ligne a été très rapide et utile. ProTip: L'utilisation de l'application réduit les frais de 9%.",
    "facebook-review-9": "C'est la deuxième fois que j'utilise Indacoin - Excellent service, rapidité de la transaction, support client brillant. Je vais certainement utiliser à nouveau.",
    "facebook-review-10": "Extrêmement grand service et de la communication, étape par étape aide avec le chat en direct et totalement fiable, ils m'ont même appelé sans me demander et m'a expliqué comment faire la vérification pour la transaction, le moyen le plus facile d'acheter Bitcoin sans problèmes du tout, merci Indacoin fera beaucoup plus de transactions avec vous.",
    "facebook-review-11": "Excellent service, processus rapide. Service à la clientèle utile Rapide répond. Moyen simple d'acheter de la monnaie crypto. Entre autres services, honnêtement, c'est le seul service que j'utilise pour acheter de la monnaie crypto. Je leur fais confiance pour traiter mes achats. Recommandé à tous ceux qui s'occupent de l'achat de monnaie crypto. Last but not least, un seul mot pour Indacoin: Excellent !!!",
    //"facebook-review-12": "IndaCoin a un service super facile, fiable et rapide. Ils ont également été extrêmement rapides pour répondre à toutes mes questions et m'aider avec mon transfert. Je recommande fortement leurs services à tout le monde! 10/10!",
    "facebook-review-12": "J'ai acheté des bitcoins pour 200 Euros et le site internet est légitime. Bien qu'il ait fallu un certain temps pour que la transaction ait lieu, le service client est excellent. J'ai contacté leur service client et ils ont immédiatement résolu mon problème. Je recommande vivement ce site à tout le monde. Parfois, en raison d'un trafic élevé, le temps de transaction varie entre 8 et 10 heures. Ne paniquez pas si vous ne recevez pas vos pièces immédiatement, cela prendra du temps, mais vous obtiendrez sûrement vos pièces de monnaie.",
    "facebook-review-13": "Excellent travail Indacoin. J'ai eu la meilleure expérience jusqu'ici pour acheter des pièces de monnaie avec ma carte de crédit avec Indacoin .. Le consultant Andrew a fait un excellent service client, et j'ai été impressionné comment le transfert trop moins de 3 minutes ... Continuera à les utiliser :)",
    "facebook-review-14": "Indacoin est une interface facile et conviviale que les acheteurs peuvent acheter plus facilement des bitcoins sur leur plateforme. Il est facile de vous aider pas à pas par les agents consultants et ils sont vraiment prêts à vous aider à résoudre votre problème immédiatement. Je vais certainement référer les gens à Indacoin et ils me mettent constamment à jour sur mes situations et ils sont prêts à faire un effort supplémentaire pour résoudre vos problèmes. Merci Indacoin!",
    "facebook-review-15": "Le moyen le plus simple et le plus rapide d'acheter crypto-monnaie que j'ai trouvé dans beaucoup de recherche. Bon travail les gars, merci!",
    "facebook-review-16": "Reallly moyen facile d'acheter des pièces en ligne. Excellent service client aussi. C'est génial, ils ont des pièces de rechange à acheter directement. Prend les complications hors des échanges. Je le recommande vraiment à tous après ma première expérience positive.",
    "facebook-review-17": "Très bon service. Facile à utiliser et facile à acheter diverses pièces / jetons.",
    "facebook-review-18": "C'est l'une des meilleures plate-forme pour échanger BTC. J'ai fait plusieurs échanges et était rapide.",
    "facebook-review-19": "Excellent service, convivial, facile à acheter divers jetons crypto-monnaie et ERC20 avec votre carte de crédit ou de débit. Très rapide et sécurisé Le service et la plateforme que je cherchais! Je recommande vivement d'utiliser Indacoin!"
  },
  "cryptocurrencies-segment": {
      "title": "Plus de {{{count}}} cryptocurrencies différentes sont disponibles à l'achat sur {{{name}}}"
  },



  "invalidEmail": "Please enter valid email",
  "invalidWallet": "Please enter valid wallet",
  
  "invalidFullName": "Please enter valid fullname",
  "invalidPhone": "Please enter valid phone number",
  "invalidBirthday": "Please enter valid birth date. Format: MM.DD.YYYY",
  
  "mobileMockups": {
    "title": "Indacoin dans votre téléphone",
    "text": "Notre porte-monnaie intelligent Indacoin est disponible pour les utilisateurs d'Android, iOS et dans tous les navigateurs internet. Envoyer de la monnaie à votre ami est devenu aussi facile que d'envoyer un message"
  },

  "news-segment": {
    "no-news": "Il n'y a pas de nouvelles pour votre langue",
    "news-error": "Nous nous excusons, les nouvelles sont actuellement indisponibles",
    "comments": "Comments",
    "send": "Send",
    "news1": "Guys, we are glad to announce that you can now purchase Ethereum with your banking card on Indacoin. If you already bought Bitcoins with your card, no additional verification will be required.",
    "news2": "Trading fees have been changed! Now you can earn 1% from the executing of your limit order and 3% fee is set up for the market order. The fee to deposit funds via credit/debit cards has been decreased from 7% to 4%.",
    "news3": "Dear friends, we would like to inform you about extremely important changes on Indacoin. As per December 29, there won’t be automatic payment of the 8% annual interest on your USD deposit on a daily basis. To get a guarantee income, you need to visit “investment” section and invest from $10 to $15 000 for a period from 30 to 365 days. Interest rate varies from 10.76% to 11.98%. We have also decreased the penalty for premature withdrawal from 10% to 5%.",
    "news4": "Dear Indacoin customers, we would like to inform you that since November 09, trading fee for execution of your limit and market orders will be 0.3%"
  },

  "news": {
    "anotherNews": "Another news on",
    "readMore": "Read more",
    "topic1": {
      "title": "The Launch of a New Site",
      "full": "We are happy to announce that we finished a complete redesign of our site to make a purchase of cryptocurrency faster and easier. We also increased the number of digital coins available for buying: now you can choose among more than 200 options to create a really diversified investment portfolio in one place.",
      "short": "We are happy to announce that we finished a complete redesign of our site to make a purchase...",
      "date": "December 2, 2017"
    },
    "topic2": {
      "title": "New App for Android",
      "full": "Guys, our new App for gadgets based on Android platform is now available on Google Play. With this App you can acquire more than 100 altcoins and manage them, using only one mobile wallet. Besides, Indacoin App also provides you with an opportunity to send cryptocurrencies to the receiver via mobile number.",
      "short": "Guys, our new App for gadgets based on Android platform is now available on Google Play...",
      "date": "April 8, 2017"
    },
    "topic3": {
      "title": "The iPhone App Which You Were Waiting for",
      "full": "We are ready to declare a release of Indacoin App for iOS, with which you are able to purchase more than 200 the most famous altcoins and maintain all your crypto assets in one place. The support team is always there to help you 24/7.",
      "short": "We are ready to declare a release of Indacoin App for iOS, with which you are able to...",
      "date": "July 21, 2017"
    }
  },

  "orderText": "Enter your order information to get the current status of the transaction",

  "registerChange": {
    "unavailable": "Cette cryptocurrency est actuellement pas disponible, mais sera bientôt de retour",
    "unknown": "Erreur de serveur inconnue",
    "error": "Error send data",
    "3ds": "Card is not 3ds. Please use another card",
    "invalidWalletAddress": "Wallet address is invalid",
    "scam": "SCAM alert. We can't process your request correctly, please write to support@indacoin.com",
    "limit": "La limite a dépassé",
    "phone": "Phone is not supported"
  },
  "tags": {
    "title-btc": "Échangez Bitcoin BTC avec {{{longName}}} {{{shortName}}} - Indacoin",
    "title": "Acheter [cryptocurrency] avec une carte de crédit ou de débit dans [country] insantly - Indacoin",
    "title2": "Acheter Bitcoin avec une carte de crédit ou de débit dans le monde entier - Indacoin",
    "title3": "Acheter {{{cryptocurrency}}} avec une carte de crédit ou de débit dans {{{country}}} insantly - Indacoin",
    "title4": "Acheter crypto monnaie avec carte de crédit ou de débit dans {{{country}}} - Indacoin",
    "titles": {
      "affiliate": "Programme d'affiliation Indacoin",
      "news": "Indacoin dernières nouvelles",
      "help": "Echange de crypto-monnaie F.A.Q. - Indacoin",
      "api": "API d'échange de crypto-monnaie - Indacoin",
      "terms-of-use": "Conditions d'utilisation et politique AML - Indacoin",
      "login": "Connexion - Indacoin",
      "register": "Inscrivez-vous - Indacoin",
      "locale": "Changer de région - Indacoin",
      "currency": "{{{fullCurrency}}} ({{{shortCurrency}}}) tableaux de prix et volumes de transactions - Indacoin"
    },
    "descriptions": {
      "affiliate": "Indacoin programme d'affiliation vous permet de gagner jusqu'à 3% de chaque achat avec une carte de crédit / débit. La rémunération sera envoyée à votre porte-monnaie Bitcoin Indacoin",
      "news": "Visitez notre blog officiel des nouvelles pour rester en contact avec les dernières nouvelles sur l'industrie de la cryptomonnaie! Une foule d'informations utiles et des commentaires complets des experts Indacoin en un seul endroit!",
      "help": "Trouvez les réponses à toutes vos questions sur comment acheter, vendre et échanger des cryptomonnaies sur Indacoin.",
      "api": "Notre API vous permet d'acheter et d'échanger instantanément des cryptomonnaies avec Visa et Mastercard. Vérifiez l'intégration et les exemples.",
      "terms-of-use": "Informations générales. Conditions d'utilisation. Services fournis. Transfert de fonds Transfert de droits. Commissions. Politique AML.",
      "login": "Connectez-vous ou créez un nouveau compte",
      "register": "Créer un nouveau compte",
      "locale": "Choisissez votre région, sélectionnez cette option et nous nous souviendrons de votre région la prochaine fois que vous visiterez Indacoin",
      "currency": "Prix courants pour {{{fullCurrency}}} en USD avec volume d'échange et informations sur les cryptomonnaies historiques"
    }
  },
  "usAlert": "Nous sommes désolés, mais Indacoin ne fonctionne pas aux Etats-Unis d'Amérique",
  "wrongPhoneCode": "Vous avez entré un mauvais code depuis votre téléphone",
  "skip": "Sauter",
  "minSum": "Montant minimum à acheter",
  "currency": {
    "no-currency": "Pour une crypto-monnaie donnée, le graphique n'est pas disponible",
    "presented": {
      "header": "Bitcoin a été présenté au monde en 2009 et en 2017 il reste la principale crypto-monnaie du monde"
    },
    "difference": {
      "title": "En quoi Bitcoin est-il différent de l'argent régulier?",
      "header1": "Emission limitée",
      "text1": "Le nombre total de Bitcoins pouvant être extraits est de 21M. Plus de 16 millions de Bitcoin sont actuellement en circulation. Le plus populaire, le plus cher.",
      "header2": "Monnaie mondiale",
      "text2": "Aucun gouvernement n'a de contrôle sur Bitcoin. Il est basé uniquement sur les mathématiques",
      "header3": "Anonymat",
      "text3": "Vous pouvez utiliser vos Bitcoins n'importe quand et n'importe quel montant. Ceci est une vraie constatation par opposition à transférer de l'argent de votre compte bancaire, lorsque vous avez besoin d'expliquer la destination du paiement ainsi que l'origine des fonds.",
      "header4": "La vitesse",
      "text4": "Votre banque prendra du temps pour transférer de l'argent, surtout quand il s'agit de grandes valeurs. Avec Bitcoin, la routine d'envoi et de réception prendra quelques secondes.",
      "header5": "Transparence",
      "text5": "Toutes les transactions Bitcoin peuvent être vues à BlockChain."
    },
    "motivation": {
      "title": "Étant la première monnaie du genre, elle a réussi à changer la vision de l'argent tel que nous le connaissions. À l'heure actuelle, la capitalisation boursière Bitcoin atteint plus de 70 milliards de dollars, ce qui surpasse des géants tels que Tesla, General Motors et FedEx"
    },
    "questions": {
      "header1": "Qu'est-ce qu'une monnaie numérique et une cryptomonnaie?",
      "text1": "Crypto-monnaie est un actif numérique, qui fonctionne comme un moyen d'échange en utilisant la cryptographie pour sécuriser les transactions et de contrôler la création d'unités supplémentaires de la monnaie. Il n'a pas de forme physique et est décentralisé par opposition aux systèmes bancaires habituels",
      "header2": "Comment se produit la transaction Bitcoin?",
      "text2": "Chaque transaction a 3 dimensions: Entrée - l'origine de Bitcoin utilisé dans la transaction. Il nous dit où le propriétaire actuel l'a eu. Montant - le nombre de \"pièces\" utilisées. Sortie - l'adresse du destinataire de l'argent.",
      "header3": "D'où vient Bitcoin?",
      "text3": "Bien que le monde connaisse le nom du créateur de Bitcoin, \"Satoshi Nakamoto\" est probablement un alias pour un programmeur ou même un groupe de tel. Fondamentalement, Bitcoin provient de 31 000 lignes de code créées par \"Satoshi\" et est contrôlé uniquement par un logiciel.",
      "header4": "Comment fonctionne Bitcoin?",
      "text4": "Les transactions se produisent entre les portefeuilles Bitcoin et sont transparentes. Dans la blockchain, n'importe qui peut vérifier l'origine et le cycle de vie de chaque \"pièce\". Cependant, il n'y a pas de \"pièce\". Bitcoin n'a pas de forme physique et n'a pas de forme virtuelle, seulement l'historique des transactions.",
      "header5": "Qu'est-ce qu'un portefeuille Bitcoin?",
      "text5": "Un portefeuille Bitcoin est un programme qui vous permet de gérer Bitcoin: recevoir et envoyer, vérifier l'équilibre et le taux Bitcoin. Bien qu'il n'y ait pas de \"coin\" dans de tels portefeuilles, il contient des clés numériques, utilisées pour accéder aux adresses Bitcoin publiques et aux transactions \"signées\". Portefeuilles populaires:",
    },
    "history": {
      "title": "La brève histoire de Bitcoin",
      "text1": "Selon la légende, c'est à ce moment que Satoshi Nakamoto commence son travail sur Bitcoin",
      "text2": "Première transaction réalisée en Bitcoin. La valeur de Bitcoin, basée sur le coût de l'électricité utilisée pour générer Bitcoin, est fixée à 1 USD pour 1.306.03 BTC",
      "text3": "25% de tous les Bitcoins sont déjà générés",
      "text4": "Dell et Windows commencent à accepter Bitcoin",
      "text5": {
        "part1": "Le prix BTC passe de 1 402 USD à 19 209 USD en quelques mois.",
        "part2": "Le taux de Bitcoin atteint 20 000 USD par pièce le 18 décembre 2017"
      },
      "text6": "L'enregistrement du bitcoin.org",
      "text7": {
        "part1": "Un moment vraiment historique, quand 10 000 BTC sont utilisés pour acheter indirectement 2 pizzas. Aujourd'hui, vous pouvez acheter 2 500 pizzas au pepperoni pour le même montant.",
        "part2": "Le prix du bitcoin grimpe de 0,008 USD à 0,08 USD pour 1 BTC en seulement 5 jours.",
        "part3": "La première transaction mobile a lieu"
      },
      "text8": {
        "part1": "Le Trésor des États-Unis classe la CTB en tant que monnaie virtuelle décentralisée convertible.",
        "part2": "Le premier guichet automatique Bitcoin trouvé à San Diego, en Californie.",
        "part3": "Le prix Bitcoin double et atteint 503.10 USD. Il passe à 1000 USD le même mois"
      },
      "text9": "Le juge fédéral américain exclut que la CTB soit «un fonds au sens ordinaire de ce terme»"
    },
    "graph": {
      "buy-button": "Acheter"
    },
    "ethereum": {
      "presented": {
        "header": "Nous avons déjà Bitcoin, alors pourquoi avons-nous besoin d'Ethereum? Fouillons dedans! Bitcoin assure des transferts d'argent justes et transparents. Ether, la monnaie numérique d'Ethereum, peut jouer le même rôle. Cependant, il y a beaucoup plus à la fonctionnalité, offerte par Ethereum."
      },
      "questions": {
        "header1": "Qu'est-ce que Ethereum?",
        "text1": "Ethereum est une plate-forme open-source, qui utilise la technologie blockchain pour permettre l'informatique distribuée. Il est sécurisé par des contrats intelligents et dispose de sa propre crypto-monnaie, appelée \"ether\", comme moyen de paiement.",
        "header2": "D'où vient-il?",
        "text2": "Ethereum a été offert à titre d'idée à la fin de 2013 par le programmeur canadien, Vitalik Buterin. Financée par un crowdsale qui a eu lieu durant l'été 2014, la plateforme a finalement été présentée au monde en juillet 2015.",
        "header3": "Qu'est-ce que ICO?",
        "text3": "De nombreuses applications sont construites sur Ethereum et elles peuvent également collecter des fonds avec Ether dans le cadre d'un processus baptisé Initial Coin Offering (ICO). La façon dont cela fonctionne est assez similaire à ce que fait Kickstarter. Les programmeurs offrent différents jetons de valeur en échange de fonds (généralement Ether) pour couvrir les frais de développement de l'application. Quand ils atteignent un certain montant prédéterminé ou une certaine date se produit, les fonds sont libérés aux programmeurs. Si l'objectif n'est pas atteint, les fonds reviennent aux contributeurs initiaux.",
        "header4": "Qu'est-ce que Ether?",
        "text4": "Ether (ETH) est une monnaie numérique et un jeton de valeur utilisé dans Ethereum. En plus d'être le moyen de paiement pour différents services sur la plate-forme Ethereum, il est également présenté sur les échanges de crypto-monnaie et peut être changé pour de l'argent réel.",
        "header5": "Qu'y a-t-il de si spécial au sujet des «contrats intelligents» et quelle est sa différence par rapport aux contrats habituels?",
        "text5": "Chaque transaction a 3 dimensions: Entrée - l'origine de Bitcoin utilisé dans la transaction. Il nous dit où le propriétaire actuel l'a eu. Montant - le nombre de \"pièces\" utilisées. L'objectif principal du contrat intelligent est de s'assurer que toutes les conditions d'un contrat sont exécutées correctement. Sa fonctionnalité décentralisée offre plus de sécurité aux contributeurs d'Ethereum, en utilisant des applications impartiales, stockées dans la blockchain, pour vérifier et appliquer ses termes. Cette technologie ne laisse presque aucune place à la fraude. Et tandis que pour le contrat habituel, vous aurez généralement besoin d'un tiers (avocat) pour traiter de toute question, comme la violation, les contrats intelligents sont impartiaux et utilisent les mathématiques pour assurer la sécurité.",
        "header6": "En quoi Ethereum est-il différent de Bitcoin?",
        "text6": "Inspiré par Bitcoin, Ethereum utilise également la technologie Blockchain, hébergée par les ordinateurs personnels de programmeurs bénévoles du monde entier. Pour ce travail, ils obtiennent des jetons de valeur - Ether. Le prix Ethereum est établi par les échanges entre les personnes, tout comme Bitcoin. La différence est énorme cependant. Ethereum est, tout d'abord, une plate-forme pour l'informatique distribuée. Il fournit une opportunité pour les programmeurs de créer diverses applications décentralisées (Dapps) sur sa base, obtenir des fonds pour les dépenses opérationnelles. De cette façon, aucune gestion intermédiaire n'est nécessaire et il n'y a donc pas de frais supplémentaires. La plate-forme Ethereum utilise également des contrats intelligents comme moyens de faciliter les relations entre les parties et sa propre crypto-monnaie, «ether», comme moyen de paiement. Il est juste de dire que, même si Bitcoin et Ethereum sont grands et ont quelques similitudes, leurs objectifs sont initialement différents. Le premier est seulement une monnaie, tandis que le second utilise uniquement Ether principalement pour ses besoins internes.",
        "header7": "Quels sont les portefeuilles Ethereum les plus populaires?",
        "text7": "Un portefeuille Ethereum est un programme qui vous permet de gérer votre crypto-monnaie: recevez et envoyez-les, vérifiez le solde et le taux de change Ethereum. Portefeuilles populaires:"
      },
      "history": {
        "title": "La brève histoire d'Ethereum",
        "text1": "Crowdsale est initié pour recueillir des fonds pour la création d'Ethereum",
        "text2": "L'organisation autonome décentralisée (DAO) est créée sur la plateforme Ethereum. Financé par une crowdsale symbolique, c'est actuellement la plus grande campagne de crowdfunding de l'histoire",
        "text3": "Ethereum gagne 50% de part de marché dans l'offre initiale de pièces de monnaie",
        "text4": "Le taux de change de l'Ethereum atteint 400 USD (hausse de 5 000% depuis janvier)",
        "text5": "L'idée d'Ethereum est offerte par Vitalik Buterin, programmeur canadien et fondateur de Bitcoin Magazine",
        "text6": "La plateforme du 30 juillet est en ligne",
        "text7": "Le DAO est piraté, la valeur Ethereum passe de 21,5 USD à 8 USD. Cela conduit à Ethereum étant fourchu dans deux blockchains en 2017",
        "text8": "Enterprise Ethereum Alliance est créé. Parmi les membres sont Toyota, Samsung, Microsoft, Intel, Deloitte et d'autres grands noms. L'objectif principal de l'Alliance est de déterminer comment la fonctionnalité d'Ethereum peut être utilisée dans divers aspects de la vie, tels que la gestion, la banque, la santé, la technologie, etc.",
        "text9": {
          "part1": "Toutes les cryptomonnaies chutent après que la Chine a interdit ICO, mais se rétablissent rapidement après.",
          "part2": "La capitalisation boursière d'Ethereum atteint plus de 36 milliards de dollars, sa valeur historique la plus élevée, avant de retomber à 30 milliards après l'interdiction de la Chine par l'OIC"
        }
      }
    }
  },
  "affiliate": {
    "promo": {
      "offer-block": {
        "text": "Êtes-vous prêt à laisser vos clients acheter de la crypto avec une carte de crédit / débit? S'il vous plaît laissez-vous un email et nous allons entrer en contact dès que possible",
        "input": "Adresse e-mail",
        "button": "Commencer",
        "mail-success": "Je vous remercie. Nous vous répondrons"
      },
      "api": {
        "title": "Intégration d'API",
        "text": "Grâce à notre API, vous pouvez laisser vos clients acheter des cryptocurrences instantanément depuis votre site. Notre plate-forme anti-fraude intégrée sur votre page Web ou votre application mobile peut également être utilisée comme un outil qui vous permettra de surveiller et de filtrer les paiements effectués sur votre plateforme."
      },
      "affiliate": {
        "title": "Programme d'affiliation",
        "text": "Soyez récompensé pour avoir conseillé Indacoin à d'autres clients. Vous gagnerez jusqu'à 3% des achats des renvois.",
        "block-1": {
          "title": "Comment puis-je rejoindre le programme d'affiliation d'Indacoin?",
          "text": "N'hésitez pas à vous inscrire sur notre site."
        },
        "block-2": {
          "title": "Comment serai-je en mesure de retirer les fonds collectés sur mon compte?",
          "text": "Vous pouvez acheter BTC / ETH / autres altcoins sur Indacoin sans frais ou les recevoir sur votre carte bancaire."
        },
        "block-3": {
          "title": "Quel est l'avantage de votre programme d'affiliation?",
          "text": "Sur notre plateforme, les clients ont la possibilité d'acheter plus de 100 pièces différentes en utilisant des cartes de crédit / débit sans inscription. Compte tenu du fait que les achats se déroulent presque immédiatement, les partenaires peuvent obtenir leurs profits instantanément."
        },
        "block-4": {
          "title": "Puis-je voir l'exemple du lien de parrainage?",
          "text": "Bien sûr, voici à quoi cela ressemble: ({{{link}}})"
        }
      },
      "more": "En savoir plus",
      "close": "Fermer"
    },
    "achievements": {
      "partnership-title": "Partenariat",
      "partnership-text": "Il y a plusieurs façons de collaborer avec Indacoin. Le programme de partenariat Indacoin s'adresse aux entreprises liées à la cryptographie qui sont disposées à connecter le monde des crypto-monnaies avec le système de monnaie fiduciaire traditionnel pour accroître l'engagement dans l'industrie de la cryptographie. Avec Indacoin, vous serez en mesure de fournir à vos clients l'expérience personnalisée de cryptage d'achat avec les cartes de crédit / débit.",
      "block-1": {
        "header": "De confiance",
        "text": "Nous travaillons sur le marché de la cryptographie depuis 2013 et nous avons établi des partenariats avec de nombreuses entreprises respectées qui dirigent l'industrie. Plus de 500 000 clients de plus de 190 pays ont utilisé notre service pour acheter des crypto-monnaies."
      },
      "block-2": {
        "header": "Garantir",
        "text": "Nous avons créé une plate-forme anti-fraude innovante et très efficace avec une technologie de base développée spécialement pour la détection et la prévention des activités frauduleuses pour les projets cryptographiques."
      },
      "block-3": {
        "header": "Solution prête à l'emploi",
        "text": "Notre produit est déjà utilisé par les ICO, Cryptocurrency Exchanges, Blockchain Platforms et Crypto Funds."
      },
      
      "header": "Gagnez de l'argent avec nous sur la percée",
      "subheader": "Technologies Blockchain",
      "text1": "{{{company}}} est une plate-forme mondiale, qui permet aux gens d'acheter instantanément Bitcoin, Ethereum, Ripple, Waves et 200 autres cryptocurrencies différentes avec une carte de crédit ou de débit",
      "text2": "Le service a fonctionné depuis 2013 presque partout dans le monde, y compris mais sans s'y limiter:",
      "text3": "Royaume-Uni, Canada, Allemagne, France, Russie, Ukraine, Espagne, Italie, Pologne, Turquie, Australie, Philippines, Indonésie, Inde, Biélorussie, Brésil, Égypte, Arabie Saoudite, Emirats Arabes Unis, Nigeria et autres"
    },
    "clients": {
      "header": "Plus de 500 000 clients",
      "subheader": "depuis 2013"
    },
    "benefits": {
      "header": "Nos principaux avantages pour les entreprises:",
      "text1": "Service unique permettant aux clients d'acheter et d'envoyer des bitcoins sans inscription. Ainsi, les clients qui viennent à Indacoin en suivant le lien de parrainage devront simplement entrer les détails de la carte et l'adresse du portefeuille, où le crypto doit être envoyé. Par conséquent, les clients potentiels effectueront les achats immédiatement et nos partenaires réaliseront des bénéfices.",
      "text2": "Jusqu'à 3% de chaque transaction est réalisée par nos partenaires. De plus, les webmasters recevront 3% de tous les futurs achats de la référence.",
      "text3": "Bitcoin et cartes bancaires pourraient être utilisés pour retirer le revenu."
    },
    "accounts": {
      "header": "Rejoignez-nous maintenant",
      "emailPlaceholder": "Entrez votre adresse email",
      "captchaPlaceholder": "Captcha",
      "button": "Commencer"
    }
  },
  "faq": {
    "title": "Questions et réponses",
    "header1": "Questions générales",
    "header2": "Crypto-monnaie",
    "header3": "Vérification",
    "header4": "Commissions",
    "header5": "achat",
    "question1": {
      "title": "Qu'est-ce que l'Indacoin?",
      "text": "Nous sommes une entreprise travaillant dans le domaine de la crypto-monnaie depuis 2013, basée à Londres, au Royaume-Uni. Vous pouvez utiliser notre service pour acheter plus de 100 cryptocurrencies différentes par paiement par carte de crédit / débit sans inscription."
    },
    "question2": {
      "title": "Comment puis-je te contacter?",
      "text": "Vous pouvez contacter notre équipe de support par e-mail (support@indacoin.com), par téléphone (+ 44-207-048-2582) ou en utilisant une option de chat en direct sur notre page Web."
    },
    "question3": { // TODO: link
      "title": "Do you have an affiliate program?",
      "text": "Yes, all details could be found here "
    },
    "question4": {
      "title": "Quelles cryptocurrences puis-je acheter ici?",
      "text": "Vous pouvez acheter plus de 100 cryptocurrences différentes qui sont toutes répertoriées sur notre site Web."
    },
    "question5": { // TODO: link
      "title": "Do I need to sign up/create an account to buy here?",
      "text-part1": "No, you just need to enter here ",
      "text-part2": " amount of USD you want to spend, email, mobile phone number, card details and bitcoin wallet address. Then you will be forwarded to your order page."
    },
    "question6": {
      "title": "Quels pays peuvent utiliser ce service?",
      "text": "Nous travaillons avec tous les pays, à la seule exception des USA (pour le moment)"
    },
    "question7": {
      "title": "Puis-je vendre du bitcoin ici?",
      "text": "Non, vous ne pouvez acheter que des bitcoins ici."
    },
    "question8": {
      "title": "Qu'est-ce que Crypto-monnaie?",
      "text": "Сryptocurrency est une monnaie numérique qui utilise la cryptographie pour la sécurité, la fonctionnalité, ce qui rend difficile la contrefaçon. Il n'est émis par aucune autorité centrale, ce qui le rend théoriquement insensible à l'ingérence ou à la manipulation du gouvernement."
    },
    "question9": {
      "title": "Où puis-je stocker la crypto-monnaie?",
      "text": "Vous pouvez configurer un portefeuille séparé pour chaque crypto-monnaie sur votre PC ou vous pouvez utiliser notre service de portefeuille gratuit et stocker plus de 100 crypto-monnaies en un seul endroit."
    },
    "question10": {
      "title": "Que voulez-vous dire par \"adresse bitcoin\"?",
      "text": "C'est l'adresse de votre portefeuille Bitcoin. Vous pouvez également entrer l'adresse d'une personne à qui vous voulez envoyer des bitcoins ou créer un portefeuille sur Indacoin"
    },
    "question11": {
      "title": "À quelle vitesse la transaction de crypto-monnaie est-elle rapide?",
      "text": "Les temps de transaction Crypto-monnaie dépendent de la crypto-monnaie que vous envoyez, les transactions Bitcoin prennent environ 15-20 minutes à compléter et permettent en moyenne jusqu'à 1 heure pour le reste des pièces."
    },
    "question12": {
      "title": "Que dois-je faire pour vérifier ma carte?",
      //"text": "Pour terminer la vérification, vous devrez entrer le code à 4 chiffres que vous recevez par appel téléphonique. Pour votre deuxième étape, vous pouvez également être invité à subir une vérification vidéo, où vous enregistrez une vidéo en montrant votre visage et télécharger une analyse ou une photo de votre carte d'identité / passeport / pilote. Selon votre vérification, cette étape peut être obligatoire car nous devons être certains que vous êtes le titulaire de la carte."
      "text": "Pour terminer la vérification, vous devez entrer le code à 4 chiffres que vous recevez par appel téléphonique. Pour votre deuxième étape, vous pouvez également être soumis à une vérification de photo, ce qui vous oblige à télécharger une photo de votre passeport / carte d'identité, ainsi qu'un selfie avec carte d'identité et un morceau de papier avec le texte requis, confirmant l'achat. Nous pouvons également demander une vérification vidéo, où vous devrez enregistrer votre visage, par exemple: \"Vérification indacoin pour crypto\", montrez votre identité et la carte avec laquelle vous avez effectué le paiement. Lors de l'affichage de la carte, affichez uniquement les 4 derniers chiffres et le nom."
    },
    "question13": {
      "title": "Que faire si je ne peux pas enregistrer une vérification vidéo?",
      "text": "Vous pouvez ouvrir le même lien de page de commande à partir de votre appareil mobile et l'enregistrer à partir de là. En cas de problème, vous pouvez également envoyer la vidéo requise en pièce jointe par e-mail à support@indacoin.com. Assurez-vous d'indiquer votre numéro de commande d'échange."
    },
    "question14": {
      "title": "J'ai fait une commande et entré tous les détails, pourquoi est-ce encore le traitement?",
      "text": "Si vous avez attendu plus de 2 minutes, la commande n'est pas passée à la transaction. Il est possible que votre carte ne soit pas sécurisée en 3D et vous devrez peut-être réessayer. Assurez-vous de confirmer votre achat via le code PIN de votre banque ou essayez d'acheter dans une devise différente (EUR / USD). Sinon, si vous êtes certain que votre carte est en 3D et que le changement de devise n'aide pas alors vous devez contacter votre banque, ils peuvent bloquer vos tentatives d'achat."
    },
    "question15": {
      "title": "Dois-je procéder à une vérification chaque fois que je fais un achat?",
      "text": "Vous avez seulement besoin de vérifier votre carte une fois et toutes les transactions suivantes seront automatiques. Cependant, nous exigerons une vérification si vous décidez d'utiliser une autre carte bancaire."
    },
    "question16": {
      "title": "Ma commande a été refusée par la banque, que dois-je faire?",
      "text": "Vous devez contacter votre banque et demander la raison du déclin, peut-être qu'ils peuvent lever la restriction afin qu'un achat puisse être fait."
    },
    "question17": {
      "title": "Pourquoi dois-je vérifier ma carte bancaire ou envoyer une vidéo?",
      "text": "Le processus de vérification vous protège en cas de vol de carte, de piratage ou de fraude; Cela fonctionne dans le sens opposé au cas où quelqu'un essaie de faire un achat en utilisant la carte bancaire de quelqu'un d'autre."
    },
    "question18": {
      "title": "Comment puis-je croire que mes coordonnées et les informations de la carte sont sécurisées?",
      "text": "Nous ne partageons jamais vos coordonnées avec des tiers sans votre consentement. En outre, nous n'acceptons que les cartes 3D-Secure, et nous ne demandons jamais de données sensibles telles que le code CCV ou le numéro complet de la carte. Vous ne fournissez ces détails de paiement que via les terminaux de passerelle Visa ou Mastercard. Nous recueillons uniquement les informations nécessaires pour être sûr que vous êtes le propriétaire de la carte."
    },
    "question19": {
      "title": "Quel est votre frais?",
      "text": "Nos frais varient d'un achat à l'autre, car ils sont basés sur de nombreux facteurs, c'est pourquoi nous avons créé une calculatrice qui vous indiquera le montant exact de la crypto-monnaie que vous recevrez, tous frais compris."
    },
    "question20": {
      "title": "Combien serai-je facturé pour la transaction?",
      "text": "Vous serez facturé uniquement le montant que vous spécifiez lors de votre commande dans notre calculatrice. Tous les frais sont inclus dans ce montant."
    },
    "question21": {
      "title": "Avez-vous des réductions?",
      "text": "Nous avons parfois des réductions, vous pouvez obtenir plus d'informations auprès de notre personnel de soutien."
    },
    "question22": { // TODO: link
      "title": "How to buy cryptocurrency on Indacoin?",
      "text": "Make sure you are using your own, 3D secure Visa or MasterCard and have drivers license, ID or passport available in case we require a video verification. Then please follow this link ___ to our calculator, select the cryptocurrency you want to buy, input the amount to be charged from your card in EUR/USD and input crypto address (or leave blank if you want to deposit to Indacoin wallet). Fill in the required fields and once your card is charged our support team will be in contact with you to verify your purchase."
    },
    "question23": {
      "title": "A quelle vitesse ma crypto-monnaie sera-t-elle envoyée?",
      "text": "Habituellement, cela prend environ 30 minutes après que vous ayez passé une commande ou effectué une vérification complète (si c'est la première fois que vous utilisez la carte sur Indacoin)."
    },
    "question24": {
      "title": "Quels sont les achats minimum et maximum?",
      "text": "Vous aurez la limite de maximum 200 $ pour la première transaction, 200 $ supplémentaires pour la deuxième transaction disponible après 4 jours de l'achat initial, 500 $ après 7 jours et 2000 $ dans 14 jours du premier achat. Dans un mois à partir de votre premier achat, il n'y a aucune limite lors du paiement avec votre carte. S'il vous plaît gardez à l'esprit que la limite minimale est toujours de 50 $."
    },
    "question25": {
      "title": "Quel type de cartes sont acceptées?",
      "text": "Nous acceptons seulement les cartes Visa et Mastercard avec 3D Secure."
    },
    "question26": {
      "title": "Qu'est-ce que 3D secure?",
      "text": "La technologie 3D Secure comprend les programmes Verified by Visa et MasterCard SecureCode.Après avoir entré les détails de votre carte de crédit dans notre boutique en ligne, une nouvelle fenêtre apparaîtra, vous demandant votre code de sécurité personnel. Votre institution financière authentifiera la transaction en quelques secondes, et confirmera que vous êtes la personne qui effectue l'achat."
    },
    "question27": {
      "title": "Quels autres systèmes de paiement puis-je utiliser pour acheter des bitcoins?",
      "text": "Actuellement, nous n'acceptons que les paiements par carte Visa / Master sur notre plateforme."
    }
  },
  "api": {
    "describe-segment": {
      "title1": "Partnership functions",
      "header1": "Simple integration",
      "text1": "The most simple integration could be by using the following link, that should include params like cryptowallet address, amount, currency (cardeur, cardusd or cardrub) and discount id. General view of the query: /change/buy-{CurrencyTo}-with-card{CurrencyFrom}?amount_pay={Amount}&wallet={WalletAddress}&discount={Discount}",
      "placeholder": "Ask a question to a consultant",
      "example": "See example"
    },
    "functions-segment": {
      "header": "Tight integration",
      "title": "Please describe your business, the size of your company and your interest in a co-operation with Indacoin. Get in touch with pr@indacoin.com to receive further information"
    },
    "partnership-segment": {
      "block1": {
        "title1": "Logic",
        "text1": "Check the limit of the user (it will create one if he doesn't exist yet)",
        "text2": "Further create transaction",
        "text3": "Forward him to indacoin URL, the user will pay and passes verification",
        "text4": "Either will pass or not until the end, you should show him URL for finishing verification",
        "text5": "On your URL arrives a callback in case of failure or success of transaction",
        "text6": "To verify that you have a signature - you need to send in headers 'gw-partner', 'gw-nonce', 'gw-sign'. Header gw-sign is formed as HMAC sha256 from partnerName + \"_\" + nonce",
        "text7": "Singature for URL forms just like API's instead of nonce - transaction's ID and Double Base64 encoding",
        "text8": "Example of payment form is",
        "text9": "Minimum amount for now is 50 USD/EURO, maximum is 500 USD/EURO",
        "text10": "Possible statuses of transaction:",
        "title2": "Methods",
        "text11": "create transaction",
        "text12": " - create new transaction",
        "text13": "Parameters in json:",
        "text14": "user limit",
        "text15": "Parameters in json:",
        "text16": "get price",
        "text17": "- receive amount user will get",
        "text18": "Parameters in GET:",
        "text19": "transaction",
        "text20": "- receive info about last transactions",
        "text21": "Parameters in json:",
        "text22": "all parameters are optional",
        "text23": "transaction info",
        "text24": "- receive transaction's info",
        "text25": "Parameters in json:",
        "text26": "Example of how to create the request on PHP and Node.js",
        "text27": "Example of the request for URL",
        "text28": "To create the request you need to get transaction ID from the previous request",
        "text29": "Checking limit for current user",
        "text30": "Before that, we recommend making a request exgw_getUserlimits to check the limit which is available for this concrete user (the limit depends on the region of the client and on the history of his/her purchases on our platform)",
        "text31": "Creating transaction on Indacoin",
        "text32": "When a user makes a payment, you have to get ID-transaction for conducting this payment. To do so make a request exgw_createTransaction.",
        "text33": "Forwarding to Indacoin",
        "text34": "To forward the client to the page of the payment, you need to use ID-transaction to create a URL",
        "text35": "Processing client on Indacoin",
        "text36": "Then the customer goes to our platform, where s/he makes the payment, gets verified and receives the confirmation of the purchase",
        "text37": "Check status by Callback or API",
        "text38": "After the client makes the payment, you get a callback about every change in the status of the transaction"
      }
    },
    "secret-segment": {
      "title": "Ask our manager on {{{company}}} for a further information"
    },
    "statuses-segment": {
      "title": "Also, you can check the status here:"
    }
  },
  "limits-warning": {
    "message": "Le montant doit être compris entre {{{limitsMin}}} {{{limitsCur}}} et {{{limitsMax}}} {{{limitsCur}}}",
    "message-only-min": "Le montant doit être de {{{limitsMin}}} {{{limitsCur}}}"
  },
  "modals": {
    "cookie-modal": {
      "text": "<span class=\"text\">Ce site utilise des cookies pour vous garantir la meilleure expérience sur notre site.</span> {{{link}}}",
      "accept-button": "Je l'ai",
      "link": "Apprendre encore plus"
    },
    "success-video": {
      "text": "Je vous remercie! Nous avons maintenant commencé le processus de vérification, ce qui peut prendre du temps. Lorsque la vérification est terminée, les pièces cryptées vous seront livrées"
    },
    "accept-agreement": {
      //"text": "Veuillez indiquer que vous avez lu et accepté les conditions d'utilisation et la politique AML"
      "text": "Veuillez indiquer que vous avez lu et accepté les conditions d'utilisation et la politique de confidentialité"
    },
    "price-changed": {
      "header": "Prix changé",
      "agree": "Se mettre d'accord",
      "cancel": "Annuler"
    },
    "neoAttention": "En raison des caractéristiques de la devise cryptographique NEO, seuls les entiers sont disponibles à l'achat",
    "login": {
      "floodDetect": "Mauvais captcha",
      "resetSuccess": "Réinitialiser le mot de passe Merci de consulter vos emails",
      "wrongEmail": "Cet email n'est pas encore enregistré",
      "unknownError": "Erreur du serveur. Veuillez réessayer la demande",
      "alreadyLoggedIn": "Vous êtes déjà connecté",
      "badLogin": "Identifiant ou mot de passe incorrect",
      "badCaptcha": "Mauvais captcha",
      "googleAuthenticator": "Veuillez entrer le code google authentificateur"
    },
    "registration": {
      "unknownError": "Erreur inconnue",
      "floodDetect": "Détection d'inondation",
      "success": "Inscrivez-vous au succès Merci de consulter vos emails",
      "badLogin": "Identifiant ou mot de passe incorrect",
      "badCaptcha": "Mauvais captcha",

      "empty": "Tous les champs ne sont pas terminés. Veuillez les remplir tous",
      "repeatedRequest": "La confirmation d'inscription a été envoyée à votre adresse électronique. S'il vous plaît assurez-vous que vous l'avez reçu. Juste au cas où, vérifiez aussi un dossier spam",
      "badEmail": "Vous avez utilisé l'adresse e-mail incorrecte ou temporaire",
      "blockedDomain": "Veuillez essayer une autre adresse e-mail pour vous enregistrer"
    }
  },
  // "stop-sidebar": {
  //   "title": "Dear Customers!",
  //   "text": "At the moment all our cryptocurrencies are sold out. Thank you for your patience and understanding. We are planning to restart our work on",
  //   "time": "February 27, 6 pm UTC"
  // },
  "noResults": "Aucun résultat trouvé",
  "locale": {
    "title": "Veuillez choisir votre pays"
  }
}