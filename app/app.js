import Ember from 'ember';
import Resolver from './resolver';
import loadInitializers from 'ember-load-initializers';
import config from './config/environment';
import RouteAliasResolver from 'ember-route-alias/mixins/route-alias-resolver';

// import ENV from 'fastboot/config/environment';
// const IS_PROD = ENV.environment === 'production';
// const IS_DEV = ENV.environment === 'development';
// const IS_TEST = ENV.environment === 'test';

const App = Ember.Application.extend({
  // hinting: IS_TEST, // Disable linting for all builds but test
  // tests: IS_TEST, // Don't even generate test files unless a test build
  // "ember-cli-babel": {
  //   includePolyfill: IS_PROD // Only include babel polyfill in prod
  // },
  // autoprefixer: {
  //   sourcemap: false // Was never helpful
  // },
  // sourcemaps: {
  //   //enabled: IS_PROD // CMD ALT F in chrome is *almost* as fast as CMD P
  //   enabled: false // CMD ALT F in chrome is *almost* as fast as CMD P
  // },
  rootElement: '#app',
  modulePrefix: config.modulePrefix,
  podModulePrefix: config.podModulePrefix,
  Resolver: Resolver.extend(RouteAliasResolver)
});

Ember.warn = function() { };
Ember.deprecate = function() { };
Ember.Logger.log = function() { };
//Ember.assert = function() { };

/*case 'assert':
return assert;
case 'info':
return info;
case 'warn':
return warn;
case 'debug':
return debug;
case 'deprecate':
return deprecate;
case 'debugSeal':
return debugSeal;
case 'debugFreeze':
return debugFreeze;
case 'runInDebug':
return runInDebug;
case 'deprecateFunc':
return deprecateFunc;*/

Ember.Route.reopen({
  hasLayout: true,
  hasHeader: true,
  stopSidebar: true, // Informer right sidebar about expired currency
  setupController() {
    this._super(...arguments);
    this.controllerFor('application').set('showLayout', this.get('hasLayout'));
    this.controllerFor('application').set('showHeader', this.get('hasHeader'));
    //this.controllerFor('application').set('stopSidebar', this.get('stopSidebar'))
    //this.controllerFor('lang.change').set('stopSidebar', this.get('stopSidebar'))

    // console.log('IS_PROD = ' + IS_PROD);
    // console.log('IS_DEV = ' + IS_DEV);
    // console.log('IS_TEST = ' + IS_TEST);
  }
})

loadInitializers(App, config.modulePrefix);

export default App;