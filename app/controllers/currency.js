import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['name', 'id'],
  name: null,
  id: null,
  chartData: Ember.computed('model', function(){
  var chartData = this.get('model.chart.history');
  // console.log(chartData)
  var result = [];
  for (var i = 0; i < chartData.length; i++) {
    // console.log(`chartData.${prop} = ${chartData[prop]}`);
    var price = chartData[i].price.usd;
    var coords = {
      x: i,
      y: price
    };
    result.push(price);
    // console.log(x);
  }
    // return JSON.stringify(result, null, 4);
    // console.log(result);
    // console.log(data);
    return result;
  }),
  chartLabels: Ember.computed('model', function(){
    var chartData = this.get('model.chart.history');
    var result = [];
    for (var i = 0; i < chartData.length; i++) {
      let date = chartData[i].date;
      result.push(date);
    }
    // console.log(result);
    return result;
  }),
  chart: Ember.computed('model', function(){
    return {
      data: {
        labels: this.get('chartLabels'),
        datasets: [{
          label: 'Price of ' + this.get('model.chart.history.0.name'),
          data: this.get('chartData'),
          pointRadius: 0,
          fill: false,
          borderColor: '#00B57A',
          pointBackgroundColor: '#00B57A',
          lineTension: 0.5,
          DrawOnChartArea: false
          // borderWidth: '2px'
        }]
      },
      options: {
        scales: {
            yAxes: [{
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        return '$' + value;
                    }
                }
            }]
        }
      }
    }
  })
});
