import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['confirm_code', 'request_id']
});
