import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['page', 'size', 'give', 'take'],
  page: 0,
  size: 20,
  category: false,
  give: 'usd',
  take: 'btc',
  giveCurStatusUSD: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return false;
    } else {
      return 'teal';
    }
  }),
  giveCurStatusEUR: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return 'teal';
    } else {
      return false;
    }
  }),
  giveCurSymbol: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return '€';
    } else {
      return '$';
    }
  }),
  giveCur: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return 'EUR';
    } else {
      return 'USD';
    }
  }),
  // header: Ember.computed('page', 'header','model', function(){
  //   if(this.get('page') === 0) {
  //     return true
  //   } else {
  //     return false
  //   }
  // }),
  firstPage: Ember.computed('size', function() {
    return this.get('size') > 40 ? true : false;
  }),
  prevPageState: Ember.computed('size', function(){
    return this.get('size') > 20 ? true : false;
  }),
  prevPage: Ember.computed('size', function(){
    return this.get('size') - 20;
  }),
  nextPage: Ember.computed('size', 'model', function(){
    return this.get('size') + 20;
  }),
  color: Ember.computed('model', function() {
    return 'green';
  }),
  loading: false,
  modelChange: Ember.observer('model', function() {
    this.set('loading', false);
}),
  actions: {
    refreshCurrentModel() {
      let route = Ember.getOwner(this).lookup(`route:${get(this, 'currentRouteName')}`);
      return route.refresh();
    },
    getMore() {
      this.set('loading', 'loading');
      this.set('size', this.get('nextPage'));
    }
  }
});
