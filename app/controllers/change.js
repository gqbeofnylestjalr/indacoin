import Ember from 'ember';
import ENV from 'fastboot/config/environment';
import { hrefTo } from 'ember-href-to/helpers/href-to';
import isMobile from 'ismobilejs';
import googleAnalytics from 'google-analytics';

export default Ember.Controller.extend({
  ENV: ENV,
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  cookies: Ember.inject.service(),
  store: Ember.inject.service(),
  //count: 0,

  queryParams: ['confirm_code', 'request_id', 'discount', 'partner', 'extra_info', 'transaction_id', 'tag'],
  confirm_code: null,
  request_id: null,
  countryName: null,
  buyingLeftTitleText: null,
  buyingLeftTitleText2: null,
  coinName: null,

  isRipple: false,
  isBitcoin: false,
  qrcode: false,

  //isVideoRecord: true,
  isVideoRecord: false,
  pressVideoRecord: false,

  give: 'usd',
  take: 'btc',
  couponeCheckBox: false,
  createWalletCheckBox: false,
  coinsName: 'Bitcoins',
  coinsCountry: '',
  returned: false, // Пользователь вернулся со страницы банка
  leftCur: '',
  step: 1,
  step1: true,
  hide: false,
  videoVerificationPassed: false,
  formData: [],

  haveTransactionId: false,

  priceChangedRequest: false,

  loading: false,
  load: false,

  // Error-flag variables
  errorNews: false,
  errorMobGetCurrencies: false,

  skipTwoStep: false, // В случае выбора валюты EUR ввод платежной информации осуществляется на стороне банка, поэтому для EUR этот шаг должен опускаться

  copiedToClipboard : false,

  init: function () {
    this._super();
    //console.log('controller init');
    //console.log('getShowAlert: ' + this.get("share").getShowAlert());
    let showAlert = this.get("share").getShowAlert();
    this.set('stopSidebar', showAlert);
  },

  //**************************************************************
  //********** computed property from payment-step16.js **********
  //**************************************************************

  sendCurrencyText: Ember.computed('ENV.i18n.lang', function(){
    const i18n = this.get('i18n');
    const amount = this.get('share').getPayInAmount();
    return Ember.String.htmlSafe(i18n.t('exchangeOrder.changeCrypto.send', { amount: amount }));
  }),

  // sentStep17: Ember.computed('ENV.i18n.lang', function(){
  //   const i18n = this.get('i18n');
  //   const amount = this.get('share').getPayInAmount();
  //   return Ember.String.htmlSafe(i18n.t('exchangeOrder.step17.sent', { amount: amount }));
  // }),

  // receiveStep17: Ember.computed('ENV.i18n.lang', function(){
  //   const i18n = this.get('i18n');
  //   const amount = this.get('share').getPayInAmount();
  //   return Ember.String.htmlSafe(i18n.t('exchangeOrder.step17.receive', { amount: amount, shortName: '12' }));
  // }),




  // confirmationText: Ember.computed('ENV.i18n.lang', function(){
  //   const i18n = this.get('i18n');
  //   const amount = this.get('share').getPayInAmount();
  //   return Ember.String.htmlSafe(i18n.t('exchangeOrder.changeCrypto.send', { amount: amount }));
  // }),
  

  //**************************************************************
  //********** end computed property from payment-step16.js ******
  //**************************************************************

  termsOfUse: Ember.computed('ENV.i18n.lang', function(){
    const i18n = this.get('i18n');
    let href = hrefTo(this, 'lang.terms-of-use', ENV.i18n.lang); // "/de_GB/terms-of-use"
    let text1 = i18n.t('links.terms-of-use.link-1');
    let text2 = i18n.t('links.terms-of-use.link-2');
    //let link = `<a href="${href}">${text}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
    //let link = `<a href="${href}#privacy-policy">${text}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
    let link1 = `<a href="${href}">${text1}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
    let link2 = `<a href="${href}?anchor=privacypolicy">${text2}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
    return Ember.String.htmlSafe(i18n.t('links.terms-of-use.text', { terms: link1, policy: link2 }));
  }),

  notifTermsOfUse: Ember.computed('ENV.i18n.lang', function(){
    const i18n = this.get('i18n');
    let href = hrefTo(this, 'lang.terms-of-use', ENV.i18n.lang); // "/de_GB/terms-of-use"
    let text = i18n.t('modals.accept-agreement.link'); // Terms of Use
    let link = `<a href="${href}">${text}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
    return Ember.String.htmlSafe(i18n.t('modals.accept-agreement.text', { terms: link }));
  }),

  loadingEffect: Ember.computed('loading', function(){

    //let cryptocurrency = this.get("share").getOutCur();
    let loading = this.get('loading');
    
    if (loading === true) {
      console.log('Call computed loading: ' + loading);
      //$("#buying-block").addClass('loading');
      $('#nextStepBtn').addClass('loading');
      return "loading";
    }
    else {
      console.log('Call computed loading: ' + loading);
      //$("#buying-block").removeClass('loading');
      $('#nextStepBtn').removeClass('loading');
      return "not loading";
    }
  }),

  pageDescription: Ember.computed('buyingLeftTitleText', function(){
    const i18n = this.get('i18n');
    let cryptocurrency = this.get("share").getOutCur(); // "Bitcoin"
    //console.log('cryptocurrency = ' + cryptocurrency);

    // "(BTC/{{{shortName}}}) Buy and exchange {{{longName}}} instantly and 700 other cryptocurrencies at the best price in {{{country}}}",
    let crypto = this.get('share').getAmountOut(); // btc
    if (crypto.toUpperCase() === 'BTC') {

      let countryName = this.get('model.lang.lang').split('_')[1]; // RU
      let country = i18n.t('country.' + countryName.toUpperCase() + '.name').string; // "Российская Федерация"
      let shortName = "";

      // TODO: в роуте получать сразу LMN
      var r = this.get('store').peekAll('value');
      r.forEach(function(item){
        if (cryptocurrency.toLowerCase() === item.data.name.toLowerCase()) {
          shortName = item.data.short_name;
        }
      });
      let title = i18n.t('meta.description-btc', {shortName: shortName, longName: cryptocurrency, country: country}).string;
      return title;
    } else {
      let template = "";
      if (cryptocurrency.toLowerCase() === 'bitcoin')
        template = i18n.t('meta.description-bitcoin').string;
      else if (cryptocurrency.toLowerCase() === 'ethereum')
        template = i18n.t('meta.description-ethereum').string;
      else if (cryptocurrency.toLowerCase() === 'litecoin')
        template = i18n.t('meta.description-litecoin').string;
      else if (cryptocurrency.toLowerCase() === 'ripple')
        template = i18n.t('meta.description-ripple').string;
      else
        template = i18n.t('meta.description').string;
      let title = template.replace('[cryptocurrency]', cryptocurrency);
      return title;
    }
  }),


  // Exchange Bitcoin BTC to Ethereum ETH - Indacoin 
  // Buy Bitcoin With a Credit Or Debit Card in Germany - Indacoin
  pageTitle: Ember.computed('buyingLeftTitleText', function(){

    const i18n = this.get('i18n');
    let cryptocurrency = this.get("share").getOutCur(); // Lumen
    let shortName = "";

    // TODO: в роуте получать сразу LMN
    var r = this.get('store').peekAll('value');
    r.forEach(function(item){
      if (cryptocurrency.toLowerCase() === item.data.name.toLowerCase()) {
        shortName = item.data.short_name;
      }
    });
    
    let crypto = this.get('share').getAmountOut(); // btc
    
    if (crypto.toUpperCase() === 'BTC') {
      let title = i18n.t('tags.title-btc', {longName: cryptocurrency, shortName: shortName}).string;
      return title;
    } else {
    //console.log('pageTitle: '+ crypto + ' to ' + cryptocurrency + ' (' + shortName + ')');
      let countryName = this.get('model.lang.lang').split('_')[1]; // RU
      let country = i18n.t('country.' + countryName.toUpperCase() + '.name').string; // "Российская Федерация"
      let title = i18n.t('tags.title3', {cryptocurrency: cryptocurrency, country: country}).string;
      return title;
    }
  }),

  cryptocurrencyName: Ember.computed('buyingLeftTitleText', function(){
    let cryptocurrency = this.get("share").getOutCur(); // "Bitcoin"
    //let cur = cryptocurrency;

    if (typeof FastBoot === 'undefined') {
      $('#takeCur-select > div').dropdown('set selected', cryptocurrency);
    }

    //let coinText = Ember.currencyText;
    return cryptocurrency.toLowerCase();
  }),

  buyingLeftTitle: Ember.computed('buyingLeftTitleText', function(){
    const i18n = this.get('i18n');
    let buyCryptoTitle = i18n.t('meta.buy').string;
    return buyCryptoTitle;
  }),

  buyingLeftSubTitle: Ember.computed('buyingLeftTitleText', function(){
    const i18n = this.get('i18n');
    let buyCryptoTitle = i18n.t('meta.subTitle').string;
    return buyCryptoTitle;
  }),

  // cryptoHeader: Ember.computed('buyingLeftTitleText', 'buyingLeftTitleText2', function(){
  //   const i18n = this.get('i18n');
  //   let str = '';
  //   let cryptocurrencyName = this.get('cryptocurrencyName');
  //   let crypto = this.get('share').getAmountOut();
  //   if (crypto.toUpperCase() === 'BTC') {
  //     let cryptocurrency = this.get("share").getOutCur(); // Lumen
  //     let shortName = '';
  //     // TODO: в роуте получать сразу LMN
  //     var r = this.get('store').peekAll('value');
  //     r.forEach(function(item){
  //       if (cryptocurrency.toLowerCase() === item.data.name.toLowerCase()) {
  //         shortName = item.data.short_name;
  //       }
  //     });
  //     let text = i18n.t('meta.header', {shortName: shortName}).string;
  //     str = `<p id="buyCryptoTitle">${text}</p><p id="buyCryptoSubTitle"></p>`;
  //   } else {
  //     let buyingLeftTitle = i18n.t('meta.buy').string;
  //     let buyingLeftSubTitle = i18n.t('meta.subTitle').string;
  //     str = `<p id="buyCryptoTitle">${buyingLeftTitle} ${cryptocurrencyName}</p><p id="buyCryptoSubTitle">${buyingLeftSubTitle}</p>`;
  //   }
  //   return Ember.String.htmlSafe(str);
  // }),

  cryptoHeader: Ember.computed('buyingLeftTitleText', 'buyingLeftTitleText2', function(){
    const i18n = this.get('i18n');
    let str = '';
    let cryptocurrencyName = this.get('cryptocurrencyName'); // Litecoin -> bitcoin
    let crypto = this.get('share').getAmountOut(); // btc -> usd
    if (crypto.toUpperCase() === 'BTC') {
      let cryptocurrency = this.get("share").getOutCur();
      let shortName = '';
      // TODO: в роуте получать сразу LMN
      var r = this.get('store').peekAll('value');
      r.forEach(function(item){
        if (cryptocurrency.toLowerCase() === item.data.name.toLowerCase()) {
          shortName = item.data.short_name;
        }
      });
      let text = i18n.t('meta.header', {shortName: shortName}).string;
      str = `<p id="buyCryptoTitle">${text}</p><p id="buyCryptoSubTitle"></p>`;
    } else {
      let href = hrefTo(this, 'lang.currency', ENV.i18n.lang, cryptocurrencyName); // "/de_GB/bitcoin"
      let link = `<a class="link" href="${href}">${cryptocurrencyName}</a>`; // "<a href="/de_GB/bitcoin">Bitcoin</a>"

      let buyingLeftTitle = i18n.t('meta.buy').string;
      let buyingLeftSubTitle = i18n.t('meta.subTitle').string;//debugger;
      str = `<p id="buyCryptoTitle">${buyingLeftTitle} ${link}</p><p id="buyCryptoSubTitle">${buyingLeftSubTitle}</p>`;
    }
    return Ember.String.htmlSafe(str);
  }),



  // countryLang: Ember.computed('ENV.i18n.lang', function(){ // WE ARE WORKING IN MORE THAN 100 COUNTRIES INCLUDING RUSSIAN FEDERATION
  //   const i18n = this.get('i18n');
  //   let region = this.get("share").getRegion();
  //   //let countryName = this.get('model.lang.lang').split('_')[1];
  //   let countryName = Ember.get(ENV.i18n, 'lang').split('_')[1];
  //   let country = i18n.t('country.' + countryName.toUpperCase() + '.nameForMap').string;
  //   if (country === undefined)
  //     country = i18n.t('country.' + countryName.toUpperCase() + '.name').string;
  //   let title = "";
  //   if (countryName === 'US') {
  //     title = i18n.t('map-segment.shortTitle').string;
  //     return [ title ];
  //   }
  //   else {
  //     title = i18n.t('map-segment.title').string;
  //     return [ title, country ];
  //   }
  // }),
  countryLang: Ember.computed('countryName', function(){ // WE ARE WORKING IN MORE THAN 100 COUNTRIES INCLUDING RUSSIAN FEDERATION
    const i18n = this.get('i18n');
    let region = this.get("share").getRegion();
    let countryName = this.get('model.lang.lang').split('_')[1];
    let country = i18n.t('country.' + countryName.toUpperCase() + '.nameForMap').string;
    if (country === undefined)
      country = i18n.t('country.' + countryName.toUpperCase() + '.name').string;
    let title = "";
    if (countryName === 'US') {
      title = i18n.t('map-segment.shortTitle').string;
    }
    else {
      title = i18n.t('map-segment.subtitle', {country: country}).string;
    }
    return title;
  }),

  langRoute: Ember.computed('model.lang', function(){
    if (typeof FastBoot === 'undefined') {
      let lang = document.location.origin + '/' + this.get('model.lang.lang');
      return lang;
    }
  }),

  lang: Ember.computed('model.lang', function(){
    let lang = this.get('model.lang.lang');
    //console.log('lang = ' + lang);
    if (lang.match('_')) {
      lang = lang.split('_');
      return lang[0]
    } else {
      return lang;
    }
  }),
  country: Ember.computed('model.lang', function(){
    let country = this.get('model.lang.lang');
    if (country === undefined)
      return false;
    if (country.match('_')) {
      country = country.split('_');
      let i18n = this.get('i18n');

      // Получаем список всех доступных стран из country.*
      // TODO: может прийти другое
      //let locales = this.get('i18n.locales'); // ["dz", "en", "ru", "tr"]
      let countryName = country[1].toUpperCase();
      if (countryName === 'XX')
        countryName = 'DE';

      let translation = 'country.' + countryName + '.name';
      return i18n.t(translation);
    } else {
      return false;
    }
  }),
  giveCur: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return 'EUR';
    } else {
      return 'USD';
    }
  }),
  cur: Ember.computed('give', function(){
    let cur = this.get('give');
    if(cur === 'EUR') {
      return 'EUR';
    } else {
      return 'USD';
    }
  }),
  prevStep: Ember.computed('step', function(){
    let step = this.get('step');
    if(step > 1) {
      return true
    } else {
      return false
    }
  }),
  nextStep: Ember.computed('step', function(){
    let step = this.get('step');
    if(step < 5) {
      return true
    } else {
      return false
    }
  }),
  stepNextVerification: Ember.computed('step', function(){
    let step = this.get('step');
    if ( step === 1 ) {
      return true;
    } else {
      return false;
    }
  }),
  actions: {

    turnVideoRecord() {
      const me = this;
      me.set('pressVideoRecord', true);
      console.log('turnVideoRecord');
    },

    // Upload video on mobile version
    uploadVideo() {

      function toggleButton(button, state) {
        button.disabled = !state;
        if (state) {
            button.removeClass("disabled");
            button.prop("disabled", false);
        } else {
            button.addClass("disabled");
            button.prop("disabled", "disabled");
        }
      }

      console.log('Call button upload');
      const me = this;
      function uploadMyVideo(file, filename, callback) {
        var fd = new FormData();
        let confirm_code = me.get('share').getConfirmCode();
        let request_id = me.get('share').getRequestId();
        fd.append('request_id', request_id);
        fd.append('confirm_code', confirm_code);

        if (file) {
            fd.append('file_0', file, filename);
        } else
            return false;

        let url = "";
        if (ENV.environment === 'production') {
          url = "/AddVerificationVideo.ashx";
        }
        if (ENV.environment === 'development') {
          url = "https://indacoin.com/AddVerificationVideo.ashx";
        }
        if (ENV.environment === 'test') {
          url = "https://indacoin.com/AddVerificationVideo.ashx";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: fd,
            contentType: false, processData: false,
            success: function (data) {
                if (callback)
                    callback();
                return true;
            },
            error: function (xhr, textStatus, errorThrown) {
                switch (xhr.status) {
                    case 429:
                        //note(window.t.floodError, "notyRed");
                        alert("notyRed");
                        break;
                    default:
                        break;
                }
            }
        });
        return false;
      }



      /*if ($("#verification_video_upload")[0].files[0]) {
        toggleButton($("#uploadVideoButton"), false);
      }*/

      let file = $('#verification_video_upload')[0].files[0];
      let fileName = "";

      if (file !== undefined) {
        fileName = $('#verification_video_upload')[0].files[0].name;
      } else {
        return false;
      }

      // Call callback only in success case
      uploadMyVideo(file, fileName, function() {
              //$('.videoUploadedCongrats').show();
              //$("#uploadVideoButton").removeClass("flash").removeClass("flash-slow");
              //toggleButton($("#uploadVideoButton"), true);

              $('#successVideoRecord')
              .modal({
                closable  : false,
                onApprove : function() {
                }
              })
              .modal('show');
      
              me.set('isVideoRecord', false);

              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target6', 1);

              return false;
      });
    },

    stopRecording() {
      function toggleButton(button, state) {
        button.disabled = !state;
        if (state) {
            button.removeClass("disabled");
            button.prop("disabled", false);
        } else {
            button.addClass("disabled");
            button.prop("disabled", "disabled");
        }
      }

      var videoElement = document.getElementById('video');
      //if ($(this).hasClass('disabled')) return false;
      window.startedRecording = false;
      toggleButton($("#startRecordingButton"), true);
      toggleButton($("#stopRecordingButton"), false);
      toggleButton($("#sendRecordingButton"), true);

      //$("#stop-recording").removeClass("flash").removeClass("flash-slow");
      //$("#send-recording").addClass("flash").addClass("flash-fast");

      window.audioVideoRecorder.stopRecording(function (url) {
          videoElement.src = url;
          videoElement.muted = false;
          videoElement.play();
          window.currentStream.stop();
          window.currentStream = null;
          videoElement.onended = function () {
              videoElement.pause();

              // dirty workaround for: "firefox seems unable to playback"
              videoElement.src = URL.createObjectURL(audioVideoRecorder.getBlob());
          };
      });
    },

    sendRecording(){
      function toggleButton(button, state) {
        button.disabled = !state;
        if (state) {
            button.removeClass("disabled");
            button.prop("disabled", false);
        } else {
            button.addClass("disabled");
            button.prop("disabled", "disabled");
        }
      }

      const me = this;
      //if ($(this).hasClass('disabled')) return false;
      var file = window.audioVideoRecorder.getBlob();
      //window.loadingEffect("#send-recording", true);

      function uploadMyVideo(file,filename,callback) {
        var fd = new FormData();

        let confirm_code = me.get('share').getConfirmCode();
        let request_id = me.get('share').getRequestId();

        fd.append('request_id', request_id);
        fd.append('confirm_code', confirm_code);

        if (file) {
            fd.append('file_0', file, filename);
        } else
            return false;

        let url = "";
        if (ENV.environment === 'production') {
          url = "/AddVerificationVideo.ashx";
        }
        if (ENV.environment === 'development') {
          url = "https://indacoin.com/AddVerificationVideo.ashx";
        }
        if (ENV.environment === 'test') {
          url = "https://indacoin.com/AddVerificationVideo.ashx";
        }

        $.ajax({
            url: url,
            type: "POST",
            data: fd,
            contentType: false, processData: false,
            success: function (data) {
                if (callback)
                    callback();
                return true;
            },
            error: function (xhr, textStatus, errorThrown) {
                switch (xhr.status) {
                    case 429:
                        //note(window.t.floodError, "notyRed");
                        alert("notyRed");
                        break;
                    default:
                        break;
                }
            }
        });
      }

      uploadMyVideo(file,new Date().getTime()+'.webm',function() {

        function toggleButton(button, state) {
          button.disabled = !state;
          if (state) {
              button.removeClass("disabled");
              button.prop("disabled", false);
          } else {
              button.addClass("disabled");
              button.prop("disabled", "disabled");
          }
        }
          //alert(i18n.t('thank-message'));
          // Notification if pick NEO about can buy only integer amounts

        $('#successVideoRecord')
        .modal({
          closable  : false,
          onApprove : function() {
          }
        })
        .modal('show');

        me.set('isVideoRecord', false);

        if (window.audioVideoRecorder && window.audioVideoRecorder.getBlob()) {
            window.audioVideoRecorder.clearRecordedData();
        }
        toggleButton($("#startRecordingButton"), false);
        toggleButton($("#stopRecordingButton"), false);
        toggleButton($("#sendRecordingButton"), false);

        googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target6', 1);
      });
    },


    startRecording() {
    
      function toggleButton(button, state) {
        button.disabled = !state;
        if (state) {
            button.removeClass("disabled");
            button.prop("disabled", false);
        } else {
            button.addClass("disabled");
            button.prop("disabled", "disabled");
        }
      }

      var MediaDevices = [];
      
      var audioInputDevices = [];
      var audioOutputDevices = [];
      var videoInputDevices = [];
      
      if (navigator.mediaDevices && navigator.mediaDevices.enumerateDevices) {
          // Firefox 38+ seems having support of enumerateDevices
          // Thanks @xdumaine/enumerateDevices
          navigator.enumerateDevices = function(callback) {
              navigator.mediaDevices.enumerateDevices().then(callback).catch(function() {
                  callback([]);
              });
          };
      }
      
      // Media Devices detection
      var canEnumerate = false;
      
      /*global MediaStreamTrack:true */
      if (typeof MediaStreamTrack !== 'undefined' && 'getSources' in MediaStreamTrack) {
          canEnumerate = true;
      } else if (navigator.mediaDevices && !!navigator.mediaDevices.enumerateDevices) {
          canEnumerate = true;
      }
      
      window.hasMicrophone = false;
      window.hasSpeakers = false;
      window.hasWebcam = false;
      
      window.isWebsiteHasMicrophonePermissions = false;
      window.isWebsiteHasWebcamPermissions = false;
      
      // http://dev.w3.org/2011/webrtc/editor/getusermedia.html#mediadevices
      function checkDeviceSupport(callback) {
          if (!canEnumerate) {
              if (callback) {
                  callback();
              }
              return;
          }
      
          if (!navigator.enumerateDevices && window.MediaStreamTrack && window.MediaStreamTrack.getSources) {
              navigator.enumerateDevices = window.MediaStreamTrack.getSources.bind(window.MediaStreamTrack);
          }
      
          if (!navigator.enumerateDevices && navigator.enumerateDevices) {
              navigator.enumerateDevices = navigator.enumerateDevices.bind(navigator);
          }
      
          if (!navigator.enumerateDevices) {
              if (callback) {
                  callback();
              }
              return;
          }
      
          MediaDevices = [];
      
          audioInputDevices = [];
          audioOutputDevices = [];
          videoInputDevices = [];
      
          navigator.enumerateDevices(function(devices) {
              devices.forEach(function(_device) {
                  var device = {};
                  for (var d in _device) {
                      device[d] = _device[d];
                  }
      
                  // if it is MediaStreamTrack.getSources
                  if (device.kind === 'audio') {
                      device.kind = 'audioinput';
                  }
      
                  if (device.kind === 'video') {
                      device.kind = 'videoinput';
                  }
      
                  var skip;
                  MediaDevices.forEach(function(d) {
                      if (d.id === device.id && d.kind === device.kind) {
                          skip = true;
                      }
                  });
      
                  if (skip) {
                      return;
                  }
      
                  if (!device.deviceId) {
                      device.deviceId = device.id;
                  }
      
                  if (!device.id) {
                      device.id = device.deviceId;
                  }
      
                  if (!device.label) {
                      device.label = 'Please invoke getUserMedia once.';
                      if (location.protocol !== 'https:') {
                          if (document.domain.search && document.domain.search(/localhost|127.0./g) === -1) {
                              device.label = 'HTTPs is required to get label of this ' + device.kind + ' device.';
                          }
                      }
                  } else {
                      if (device.kind === 'videoinput' && !window.isWebsiteHasWebcamPermissions) {
                          window.isWebsiteHasWebcamPermissions = true;
                      }
      
                      if (device.kind === 'audioinput' && !window.isWebsiteHasMicrophonePermissions) {
                          window.isWebsiteHasMicrophonePermissions = true;
                      }
                  }
      
                  if (device.kind === 'audioinput') {
                      window.hasMicrophone = true;
      
                      if (audioInputDevices.indexOf(device) === -1) {
                          audioInputDevices.push(device);
                      }
                  }
      
                  if (device.kind === 'audiooutput') {
                      window.hasSpeakers = true;
      
                      if (audioOutputDevices.indexOf(device) === -1) {
                          audioOutputDevices.push(device);
                      }
                  }
      
                  if (device.kind === 'videoinput') {
                      window.hasWebcam = true;
      
                      if (videoInputDevices.indexOf(device) === -1) {
                          videoInputDevices.push(device);
                      }
                  }
      
                  // there is no 'videoouput' in the spec.
      
                  if (MediaDevices.indexOf(device) === -1) {
                      MediaDevices.push(device);
                  }
              });
      
              if (callback) {
                  callback();
              }
          });
      }
      
      // check for microphone/camera support!
      checkDeviceSupport();

    //if ($(this).hasClass('disabled')) return false;
    $('#video').show();
    toggleButton($("#startRecordingButton"), false);
    toggleButton($("#stopRecordingButton"), false);
    toggleButton($("#sendRecordingButton"), false);
    //$("#start-recording").removeClass("flash").removeClass("flash-slow");

    setTimeout(function() {
      toggleButton($("#stopRecordingButton"), true);
    }, 10000);

    if (window.audioVideoRecorder && window.audioVideoRecorder.getBlob()) {
        window.audioVideoRecorder.clearRecordedData();
    }
    captureUserMedia00(function (stream) {
        window.audioVideoRecorder = window.RecordRTC(stream, {
            type: 'video'
        });
        window.audioVideoRecorder.startRecording();
    });

    var videoElement = document.getElementById('video');

    function captureUserMedia(mediaConstraints, successCallback, errorCallback) {
        var isBlackBerry = !!(/BB10|BlackBerry/i.test(navigator.userAgent || ''));
        if (isBlackBerry && !!(navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia)) {
            navigator.getUserMedia = navigator.getUserMedia || navigator.webkitGetUserMedia || navigator.mozGetUserMedia;
            navigator.getUserMedia(mediaConstraints, successCallback, errorCallback);
            return;
        }
        navigator.mediaDevices.getUserMedia(mediaConstraints).then(successCallback).catch(errorCallback);
    }

    function captureUserMedia00(callback) {
      captureUserMedia({
          audio: true,
          video: true
      }, function (stream) {
          window.currentStream = stream;
          videoElement.src = URL.createObjectURL(stream);
          videoElement.muted = true;
          videoElement.controls = true;
          videoElement.play();
          if (!window.hasWebcam) {
              if ($('#videoForMobile>p').length) {
                  $('#videoForMobile>p').remove();
                  $('#videoForMobile div label.uploadbutton .button').html('Choose');
                  $('#videoForMobile div label.uploadbutton .input').html('Not choosen');
                  if (isMobile) {
                      $("#videoForDesktop").show();
                  } else {
                      $("#videoForMobile").show();
                  }
                  alert('We cannot find WebCam. Try to upload video');
              }
          }
          callback(stream);
      }, function (error) {
          if (!window.hasWebcam) {
              if ($('#videoForMobile>p').length) {
                  $('#videoForMobile>p').remove();
                  $('#videoForMobile div label.uploadbutton .button').html('Choose');
                  $('#videoForMobile div label.uploadbutton .input').html('Not choosen');
                  if (isMobile) {
                      $("#videoForDesktop").show();
                  } else {
                      $("#videoForMobile").show();
                  }
                  alert('We cannot find WebCam. Try to upload video');
              }
          }

          alert("Please check permissions for video recording in browser");
      });
    }

  },


    /*signIn() {
      console.log('SignIn');
      //https://indacoin.com/Services.asmx/TryLogin
      let email = "hjhjhjhg.uuuuu@mail.ru";
      let pass = "Hy7ujgReds";
      let captcha = "12345";

      $.ajax({
        url: "https://indacoin.com/Services.asmx/TryLogin",
        contentType: 'application/json; charset=utf-8',
        method: 'POST',
        async: false,
        data: "{'email':'" + email + "','pass':'" + pass + "','captcha':'" + captcha + "'}",
        success: function(n) {},
        error: function() {}
      })

      // https://indacoin.com/Services.asmx/GetCaptcha

      $.ajax({
        url: "https://indacoin.com/Services.asmx/GetCaptcha",
        contentType: 'application/json; charset=utf-8',
        method: 'POST',
        data: "{}",
        async: false,
        success: function(n) {},
        error: function(n) {
          if (n.status === 429)
            alert('Flood detect');
        }
      })

    },*/
    resendButton(){
      const me = this;
      let phone_id = me.get('share').getPhoneId();
      let confirm_code = me.get('share').getConfirmCode();
      let request_id = me.get('share').getRequestId();

      $.ajax({
        url: "/notify/ResendCallConfirmation",
        contentType: 'application/json; charset=utf-8', 
        method: 'POST',
        async: false,
        data: "{'phoneId':'" + phone_id + "','confirmation_hash':'" + confirm_code + "','request_id':'" + request_id + "'}",
        success: function(n) {
          if (n.d.res == "1" || n.d.res == "true") {
            // Показываем кнопку

            alert('The duplicate code was sent. Enter the number you received');
            $('#authCodeForm').val('');
          } else {
            alert('Expect. Support will contact you');
            $('#resendPhoneCode').hide();
          }
        },
        error: function() {
          alert('Error requesting resend code');
        }
      })
    },

    toggleSoccerLegends() {
      if ($('#soccerLegends input').is(':checked'))
        $('#form-step6 .KYCVerification').removeClass('disabled');
      else
        $('#form-step6 .KYCVerification').addClass('disabled');
    },

    videoVerification() {
      const me = this;
      
      let KYCUrl = me.get('share').getKYCUrl();
      //let KYCUrl = 'https://indacoin.com/iframe/kyc_frame.aspx?access_token=4a63a587-376c-4f2c-a7df-e9a8c9b8be08\u0026applicant_id=5ae3031a0a975a64b8ee4c22\u0026user_id=6818\u0026lang=en" style="width: 100%;"'
      //let KYCUrl = "http://localhost:4200/ru_RU";
      if (KYCUrl !== null) {
        $('#KYCiframe').attr("src", KYCUrl);
        //console.log('get true KYCUrl from payment-step6: ' + share.getKYCUrl());
        
        var iframe = $('#KYCiframe', parent.document.body);

        //iframe.height($(document.body).height()*0.7);
        //iframe.height(window.screen.height);
        iframe.height(window.innerHeight * 0.7);
        $('#KYCModal')
        .modal({
            closable  : true,
            onApprove : function() {
          }
        })
        .modal('show');
      } else {
        return 0;
      }
    },

    nextStep7(){
      console.log('nextStep7');
      const me = this;
      const i18n = this.get('i18n');

      let phoneId = this.get('share').getPhoneId();
      let authCode = $("#authCodeForm").val();
      let confirmCode = this.get('share').getConfirmCode();
      let requestId = this.get('share').getRequestId();
      $.ajax({
        url: "/notify/CheckPhoneAuthCode",
        contentType: 'application/json; charset=utf-8',
        method: 'POST',
        async: false,
        data: "{'phoneId':'" + phoneId + "','authCode':'" + authCode + "','confirmation_hash':'" + confirmCode + "','request_id':'" + requestId + "'}",
        success: function(n) {
          if (n.d.res == "1" || n.d.res == "true") {
            googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target7', 1);
            me.set('step1', false);
            me.set('step7', false);
            me.set('step6', true);
            me.set('step', 6);
            me.set('returned', true);
          }
          else {
            $('#authCodeForm').val('');
            alert(i18n.t('wrongPhoneCode'));
          }
        },
        error: function() {
          $('#authCodeForm').val('');
          alert(i18n.t('wrongPhoneCode'));
        }
      })
    },
    toggleCheckBox(){
      this.get("share").setCouponeCheckBox(
        !this.get("share").getCouponeCheckBox()
      );

      if (this.get("share").getCouponeCheckBox()) {
        Ember.$('#couponeCode').show();
      }
      else {
        Ember.$('#couponeCode').hide();
      }
    },
    toggleCreateWallet() {
      this.get("share").setCreateWalletCheckBox(
        !this.get("share").getCreateWalletCheckBox()
      );
      $('#exchangeOutAddress').prop('disabled', this.get("share").getCreateWalletCheckBox());

      if (this.get("share").getCreateWalletCheckBox()) {
        $('#exchangeOutAddress').val('');
        this.get("share").setRippleTag('');
      }
    },

    toggleAcceptAgreement() {
      console.log('toggleAcceptAgreement: ' + this.get("share").getAcceptAgreementCheckBox());
      this.get("share").setAcceptAgreementCheckBox(
        !this.get("share").getAcceptAgreementCheckBox()
      );
    },
    setStep(step){
      console.log('Set step = ' + step);
      if (step > 3)
        return;
      let stepProp = this.get('step');
      this.set('step' + stepProp, false);
      $('#payment-steps div.active').removeClass('active');
      $('#payment-steps div.step:nth-child('+ step +')').addClass('active').removeClass('disabled');
      this.set('step', step);
      this.set('step' + step, true);
      $('#nextStepBtn').show();
    },
    prevStep(){
      let step = this.get('step');
      $('#payment-steps div.active').removeClass('active');

      // Блокировка кнопки возврат на 4 шаге при возврате со страницы банка
      $('#prev-step').show();
      
      this.set('step' + step, false);

      let skip = this.get('skipTwoStep');
      if (skip === true && step === 3) {
        step -= 2;
      }
      else {
        step -= 1;
      }

      $('#payment-steps div.step:nth-child('+ step +')').addClass('active').removeClass('disabled');
      this.set('step', step);
      this.set('step' + step, true);
      $('#nextStepBtn').show();
    },
    outerStep(){
      //console.log('transactionId = ' + transactionId);
      //let transactionId = '6335d36a89e143db7877d8b4d73702281e4fa66b5c6c83809057a6483d8d3db5';
      
      //let transactionId = this.get('transactionId');
      let transactionId = this.get("share").getTransactionId();
      window.open("https://blockexplorer.com/tx/" + transactionId);
    },
    backToVideo(){ 
      console.log('backToVideo');

      let requestId = this.get("share").getRequestId();
      let confirmCode = this.get("share").getConfirmCode();
      this.get("share").notify(requestId, confirmCode);
      
      console.log('setup step #6');
      this.set('step1', false);
      this.set('step5', false);
      this.set('step6', true);
      this.set('step', 6);
      this.set('returned', true);
    },
    skipVideoVerification(){
      console.log('skipVideoVerification');

      let requestId = this.get("share").getRequestId();
      let confirmCode = this.get("share").getConfirmCode();
      this.get("share").notify(requestId, confirmCode);

      console.log('setup step #5');
      this.set('step1', false);
      this.set('step6', false);
      this.set('step5', true);
      this.set('step', 5);
      this.set('returned', true);
    },
    nextStep: function(callback) {
      var me = this;
      var i18n = this.get('i18n');

      // Button time delay
      var waitTime = 1000;
      var waitTimeLow = 1000; // 300
      //$('#nextStepBtn').addClass('loading');

      //var promise = function() {
      var promise = new Ember.RSVP.Promise(function(resolve, reject) {
      try {
        ga('send', 'event', { eventCategory: 'Screen', eventAction: 'Reject3ds', eventLabel: 'Server', eventValue: 1 });
      } catch (e) {
        console.log('Error: ' + e);
      }

      $('#nextStepBtn').attr("disabled", "disabled");
      //$('#nextStepBtn').addClass('loading');

      if (me.get('load')) {
        console.log('load true');
        me.set('load', false);
      } else {
        console.log('load false');
        me.set('load', true);
      }

      console.log('DEBUG: loading = ' + me.get('loading'));
      if (me.get('loading') === false) {
        me.set('loading', true); // Continue request
      } else {
        console.log('Double request');
        return false;
      }

      me.get('loadingEffect');

      function errorModal(message) {
        //$('#errorModal .button').attr("disabled", "disabled");
        $('#errorModalMessage').text(message);
        $('#errorModal')
        .modal({
          closable  : false,
          onApprove : function() {
            console.log('Close');
            //$('#errorModal .button').removeAttr("disabled");
          }
        })
        .modal('show');
      }

      function attentionModal() {
        $('#attentionModal')
        .modal({
          closable  : false,
          onApprove : function() {}
        })
        //.modal('attach events', 'click', function(){ console.log('Click detect'); })
        .modal('show');
      }

      // Сохраняем настройки языка для возможности восстановить их при возврате на страницу
      if (typeof FastBoot === 'undefined') {
        let lang = me.get("share").getLanguage(); // Like 'ru', 'en
        let region = me.get("share").getRegion(); // Like 'US', 'RU'
        localStorage.setItem('lang', lang + '_' + region);
      }

      let step = me.get('step');
      
      if (step === 1) {
        let currencyText = $('#curid').text().toLowerCase(); // EUR
        me.get('share').setCurrencyText(currencyText); // USD

        // setCurrencyText дублирует setAmountOut, удалить один

        //let coinText = $('#coinid').text().toLowerCase(); // ltc
        let coinText = $('#coinid .scrolling-short-cur').text().toLowerCase(); // ltc

        me.set('take', coinText);
        me.get('share').setCoinText(coinText); // ltc

        let isRipple = me.get("isRipple");
        if (isRipple) {
          let rippleValue = $('#exchangeRippleTag').val();
          me.get("share").setRippleTag(rippleValue);
        } else
          me.get("share").setRippleTag("");

        //let coinId = $( "div.currencyClass.item.active.selected").data().value;
        let coinId = $('#coinid > .coinFullName').text();
        me.get('share').setCoinId(coinId); // Litecoin

        Ember.leftCur = coinId;
        Ember.currencyText = coinText.toUpperCase();

        me.send('checkIndentEmail');
      }

      let form = '#form-step' + step;
      let formData = me.get('formData');
      $(form).form('validate form');
      if($(form).form('is valid')){

         // TODO: card_limit
        if (step === 1) {

          // Check limits on card
          let currencyText = $('#curid').text().toLowerCase(); // eur

          let cardLimit = '';
          var cookieService = me.get('cookies');

          cardLimit = me.get('share').getCardLimit();
          if (cardLimit < 1000)
            cardLimit = 1000;

          let amount = $('#exchange-give').val(); // 35
          let limits = me.get('share').getLimits();
          let min;

          if (currencyText === 'usd')
            min = limits['usdmin'];
          else if (currencyText === 'eur')
            min = limits['eurmin'];
          else if (currencyText === 'btc') {
            min = 0.01;//limits['btcmin'];
            cardLimit = 0.2; // TODO: forcee limit 10 BTC
          }
          else if (currencyText === 'aud')
            min = limits['audmin'];
          else if (currencyText === 'gbp')
            min = limits['gbpmin'];
          else {
            cardLimit *= me.get('share').getCurrentUsd();
            min = limits['rubmin'];
          }

          console.log('cardLimit = ' + cardLimit);
          console.log('min = ' + min);

          cardLimit = limits[currencyText+'max']; // TODO: card_limit

          if (amount > cardLimit) {

            // Заместо модалки текст и шейк формы
            errorModal(i18n.t('registerChange.limit') + ' ' + cardLimit + ' ' + currencyText);
            me.set('loading', false);
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectCardLimit', eventLabel: 'CreateOrder', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }
            
            setTimeout(load, waitTime);
            function load() {
              resolve();
            }      
            return false;
          } else 
          if (amount < min) {
            errorModal(i18n.t('minSum') + ' ' + min + ' ' + currencyText);
            me.set('loading', false);
            console.log('DEBUG: loading = ' + me.get('loading'));
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectMinSum', eventLabel: 'CreateOrder', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }

            setTimeout(load, waitTime);
            function load() {
              resolve();
            }
            return false;
          }
        }

        // Request cryptocurrency validation
        let createWallet = $('#createWalletCheckBox').is(':checked');
        if (step === 1 && !createWallet) {
        //if (step === 1) {
          // https://indacoin.com/api/check_addr/btc/1FcNnitx5xyQtBjMdxDt42GYso41b7ZxyL
          var valid = "";
          //var _currency = "DOGE";
          //let _currency = $('#coinid').text().toLowerCase(); // ltc
          let _currency = $('#coinid .scrolling-short-cur').text().toLowerCase(); // ltc

          //var _address = "DDHx76KLaNMfrMyN3B2TfyJSzRhrJL6Myh";
          var _address = $('#exchangeOutAddress').val();

          console.log('_currency = ' + _currency);
          console.log('getCreateWalletCheckBox = ' + (me.get("share").getCreateWalletCheckBox() === true));

          // TODO: делать проверку наличия чекбокса "создать кошелек на Indacoin"
          //if (!(me.get("share").getCreateWalletCheckBox() === true && me.get("share").getAcceptAgreementCheckBox() === true && _currency === "btc")) {
          
          //if (!(me.get("share").getCreateWalletCheckBox() === true && _currency === "btc")) {
            console.log('res = in');
            var _data = JSON.stringify({ currency: _currency, address: _address });
            $.ajax({
              async: false,
              method: 'POST',
              contentType: 'application/json; charset=utf-8',
              url: "/api/check_addr",
              data: _data,
              success: function onSucess(res) {
                valid = res.result.toString();
              }
            });
            if (valid === "not_valid") {
              me.set('loading', false);
              console.log('DEBUG: loading = ' + me.get('loading'));
              $('#nextStepBtn').prop('disabled', false);
              me.get('loadingEffect');
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectAddress', eventLabel: 'CreateOrder', eventValue: 1 });
              errorModal(i18n.t('cash.tip.correctCrypto'));
              
              setTimeout(load, waitTime);
              function load() {
                resolve();
              }
              return false;
            }
          //}
        }

        // Check Acceptance of Terms
        if (step === 1) {
          let acceptAgreement = $('#acceptAgreementCheckBox').is(':checked');
          if (acceptAgreement === false) {
            attentionModal();
            me.set('loading', false);
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            /*try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectCardLimit', eventLabel: 'CreateOrder', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }*/
            
            setTimeout(load, waitTime);
            function load() {
              resolve();
            }
            return false;
          }
        }

        // Если input с количеством криптовалюты (BTC) пустое или равно нулю, то выводится сообщение, что данная криптовалюта недоступна
        var exchangeTake = $('#exchange-take').val();
        if (Number(exchangeTake) <= 0) {
          errorModal(i18n.t('registerChange.unavailable'));
          me.set('loading', false);
          $('#nextStepBtn').prop('disabled', false);
          me.get('loadingEffect');
          try {
            ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectUnavailable', eventLabel: 'CreateOrder', eventValue: 1 });
          } catch (e) {
            console.log('Error: ' + e);
          }

          setTimeout(load, waitTime);
          function load() {
            resolve();
          }
          return false;
        }
        let currency = me.get('share').getAmountOut().toLowerCase();
        let couponeCode = $('#couponeCode').val();
        formData[step-1] = $(form).form('get values');
        me.set('formData', formData);

        saveFormData(me, formData, step);
        if (step === 3 || (step === 2 && (currency === 'eur' || currency === 'aud' || currency === 'gbp'))) {
          me.send('registerChange', formData);

          // Если ошибка на сервере, то getPayComplete вернет false. Дальше не идем
          if (me.get('share').getPayComplete() === false) {
            me.set('loading', false);
            console.log('DEBUG: loading = ' + me.get('loading'));
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectServer', eventLabel: 'ContactDetails', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }

            setTimeout(load, waitTime);
            function load() {
              resolve();
            }      
            return false;
          }
        }

        let userIdent = me.get('share').getUserIdent();
        console.log('userIdent = ' + userIdent + ', step = ' + step + ', currency = ' + currency);

        // Если почта известна, выбран USD или RUB на 2 шаге
        if (step === 3 && userIdent && (currency === 'usd' || currency === 'rub')) { // 2
          console.log('skip 3 step');
          me.send('registerChange', formData);

          // Если ошибка на сервере, то getPayComplete вернет false. Дальше не идем
          if (me.get('share').getPayComplete() === false) {
            me.set('loading', false);
            console.log('DEBUG: loading = ' + me.get('loading'));
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectServer', eventLabel: 'PaymentDetails', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }

            setTimeout(load, waitTime);
            function load() {
              resolve();
            }
            return false;
          }
        }

        // Если выбрано BTC на крипто на 1 шаге
        if (step === 1 && currency === 'btc') {
          console.log('send data identity btc');
          me.send('registerChange', formData);

          // Если ошибка на сервере, то getPayComplete вернет false. Дальше не идем
          if (me.get('share').getPayComplete() === false) {
            me.set('loading', false);
            console.log('DEBUG: loading = ' + me.get('loading'));
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectServer', eventLabel: 'CreateOrder', eventValue: 0 });
            } catch (e) {
              console.log('Error: ' + e);
            }

            setTimeout(load, waitTime);
            function load() {
              resolve();
            }
            return false;
          }
        }

        // Если почта известна, выбрано EUR на 1 шаге
        if (step === 1 && userIdent && (currency === 'eur' || currency === 'aud' || currency === 'gbp')) {
          console.log('send data identity euro\aud\gbp');
          me.send('registerChange', formData);

          // Если ошибка на сервере, то getPayComplete вернет false. Дальше не идем
          if (me.get('share').getPayComplete() === false) {
            me.set('loading', false);
            console.log('DEBUG: loading = ' + me.get('loading'));
            $('#nextStepBtn').prop('disabled', false);
            me.get('loadingEffect');

            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectServer', eventLabel: 'CreateOrder', eventValue: 0 });
            } catch (e) {
              console.log('Error: ' + e);
            }

            setTimeout(load, waitTime);
            function load() {
              resolve();
            }
            return false;
          }
        }

        // Если на 3 шаге оплата не прошла, то не переходим дальше
        if (step === 2 && me.get('share').getPayComplete()) { // 3
          me.set('loading', false);
          console.log('DEBUG: loading = ' + me.get('loading'));
          $('#nextStepBtn').prop('disabled', false);
          me.get('loadingEffect');

          setTimeout(load, waitTime);
          function load() {
            resolve();
          }
          return false;
        }

        //if (step !== 3 && payComplete) {
          $('#payment-steps div.active').removeClass('active');
          me.set('step' + step, false);
          let cur = $('#giveCur-select > div').dropdown('get text');

          switch (step) {
            case 1:
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target10', 1);
              break;
            case 2:
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target11', 1);
              break;
            case 3:
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target12', 1);
              break;
          }

          step += 1;

          me.set('step', step);
          $('#payment-steps div.step:nth-child('+ step +')').addClass('active').removeClass('disabled');
          me.set('step' + step, true);

          if (me.get('step') === 5) {
            $('#prevStepBtn').hide();
            $('#payment-steps div.step:nth-child(4)').addClass('disabled');
          }
          else
            $('#prevStepBtn').show();
        //}

      } else {
        $(form).transition('shake');
      }
      me.set('loading', false);
      console.log('DEBUG: loading = ' + me.get('loading'));
      $('#nextStepBtn').prop('disabled', false);
      me.get('loadingEffect');
      
      setTimeout(load, waitTimeLow);
      function load() {
        resolve();
      }

    });
    callback(promise);  
    },

    showHelp(){
      $('#video-help-message').toggle();
    },

    checkIndentEmail(){
      var me = this;
      let inputEmail = $('#exchangeFormEmail').val();
      var regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i; 
      var match = regex.exec(inputEmail);
      if (match === null) {
      } else
      if (match.length > 1) {
        let card_number = "";
        let country = "";
        let email = $('#exchangeFormEmail').val();
        $.ajax({
          async: false,
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          url: "/change/GetUserInfo",
          data: "{'card_number':'" + card_number + "','country':'" + country + "','email':'" + email + "'}",
          success: function onSucess(result) {
            // TODO: проверка лимита по карте
            me.get('share').setCardLimit(result.d.cardLimit);

            let isVerified = result.d.isVerified;
            if (isVerified) {
              me.get('share').setUserIdent(true);
              console.log('know email');
            }
            else {
              me.get('share').setUserIdent(false);
              console.log('unknow email');
            }
          }
        });
      }
    },
    registerChange(formData){

      googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target13', 1);

      $('#nextStepBtn').attr("disabled", "disabled");
      $('#nextStepBtn').addClass('loading');

      function errorModal(message) {
        $('#nextStepBtn').removeAttr("disabled");
        $('#errorModalMessage').text(message);
        $('#errorModal')
        .modal({
          closable  : false,
          onApprove : function() {
            console.log('Close');
          }
        })
        .modal('show');
      }

      function checkCardLimits(__cardNumber, __email){
        return 100000000; // TODO: card_limit
        var me = this;
        var __country = "";

        $.ajax({
          url: "https://payment.quickpay.net/payment-methods?bin=" + __cardNumber.substring(0,6),
          type: "get",
          async: false,
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function (result) {
            __country = result[0].country_alpha2;
          }
        });

        let res = "";
        $.ajax({
          async: false,
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          url: "/change/GetUserInfo",
          data: "{'card_number':'" + __cardNumber + "','country':'" + __country + "','email':'" + __email + "'}",
          success: function onSucess(result) {
            res = result.d.cardLimit;
          }
        });
        if (res === "")
          return false;
        else
          return res;
      }
      
      var me = this;
      var i18n = this.get('i18n');
      var share = me.get('share');
      let skip = this.get('skipTwoStep');
      var _email = formData[0].email;

      /*
      Методом loadPayments загружается инфа о платежке
      https://indacoin.com/change.aspx/getCashInfo
      Запрашиваеются доступные валюты и лимиты minSum и maxSum
      */


      let __cardNumber = "";
      /*if (formData.indexOf(1) != -1)
        __cardNumber = formData[1].cardNumber.replace(/\s+/g,'');*/
      if (typeof formData[2] !== "undefined") // 1
        __cardNumber = formData[2].cardNumber.replace(/\s+/g,''); // 1

      let __email = formData[0].email;
      var _amountIn = formData[0].giveCurAmount;

      var _cashinTypeId;
      if (me.get('share').getAmountOut().toLowerCase() === 'usd')
        _cashinTypeId = 16; // USD
      else if (me.get('share').getAmountOut().toLowerCase() === 'eur')
        _cashinTypeId = 50; // EUR
      else if (me.get('share').getAmountOut().toLowerCase() === 'btc')
        _cashinTypeId = 10; // BTC
      else if (me.get('share').getAmountOut().toLowerCase() === 'aud')
        _cashinTypeId = 55; // AUD
      else if (me.get('share').getAmountOut().toLowerCase() === 'gbp')
        _cashinTypeId = 56; // GBP
      else
        _cashinTypeId = 54; // RUB

      // Check card limits only for USD or RUB
      if (_cashinTypeId === 16 || _cashinTypeId === 54) {
        let customerLimit = checkCardLimits(__cardNumber, __email);
        if (customerLimit === false) {
          errorModal(i18n.t('registerChange.error')); // 'Error send data'
          me.get('share').setPayComplete(false);
          return false;
        }
        let rubCur = me.get('share').getCurrentUsd();
        if (_cashinTypeId === 54)
          customerLimit *= rubCur;
        if (customerLimit < _amountIn) {
          errorModal('Card limit is ' + customerLimit + ' ' + me.get('share').getAmountOut()); // 'Error send data'
          me.get('share').setPayComplete(false);
          return false;
        }
      }



      let _couponCode = "";
      if (formData[0].coupone === "on")
        _couponCode = formData[0].couponeCode;

      // Обновляем сессионные преременные насервере
      // try {
      //   var _calcData = JSON.stringify({
      //     amountCalc: _amountIn, // 50 USD
      //     calcType:'1',
      //     cashinTypeId: _cashinTypeId, // USD - 16
      //     cashoutTypeId:'10',
      //     couponCode: _couponCode,
      //     price:'10854.429',
      //     realPrice:'19999.999999999996'
      //   });

      //   Ember.$.ajax({
      //     url: "/change/CalcAmountOut",
      //     async: false,
      //     type: "post",
      //     contentType: "application/json; charset=utf-8",
      //     dataType: "json",
      //     data: _calcData,
      //     success: function(result) {
      //       console.log('CalcAmountOut success');
      //       console.log(result);
      //     },
      //     error: function() {
      //       console.log('CalcAmountOut error');
      //     }
      //   })
      // } catch(e) {
      //   console.log('CalcAmountOut error');
      // }

      var _cashoutCurrencyId = 100; // По-умолчанию Bitcoin
      let findCurrency = false;

      console.log('take = ' + me.get('take'));

      this.get('store').peekAll('value').forEach(function(item){
        if (item.get('short_name').toLowerCase() === me.get('take')) {
          _cashoutCurrencyId = item.get('cur_id');
          findCurrency = true;
        }
      });
      if (findCurrency === false)
        throw "Выбрана несуществующая валюта, которой нет на https://indacoin.com/api/mobgetcurrencies/. По-умолчанию установлены Bitcoins";

      var cashoutAddInfoObj; // Адрес кошелька для крипты или почта
      var userIdent = this.get('share').getUserIdent();

      // На первом шаге компонент недоступен
      var _phone = "";
      if (!(this.get('step') === 2 && userIdent))
        if (this.get('step') !== 1)
          _phone = window.itiPaymentStep.getNumber();
      _phone = _phone.replace('+', '');

      //if (formData[0].outAddress == "" || (this.get("share").getCreateWalletCheckBox() && this.get("share").getAcceptAgreementCheckBox()))
      console.log('DEBUG: share.getCreateWalletCheckBox = ' + this.get("share").getCreateWalletCheckBox());
      if (formData[0].outAddress == "" || this.get("share").getCreateWalletCheckBox())
        cashoutAddInfoObj = { email: formData[0].email };
      else
        cashoutAddInfoObj = { email: formData[0].email, btcAddress: formData[0].outAddress };

      if (this.get("share").getRippleTag())
        cashoutAddInfoObj.tag = this.get("share").getRippleTag();
      var _cashoutAddInfo = JSON.stringify(cashoutAddInfoObj);

      // If price changed then send new "amountOut" field
      let newTakeCurAmount = null;
      if (share.getPriceChangedRequest()) {

        // Делаем повторный запрос цены
        let giveCur = share.getAmountOut().toUpperCase(); // USD
        //let giveCur = 'EUR'; // Delete this
        let takeCur = me.get('take'); // BTC
        let amount = formData[0].giveCurAmount; // 1500
        
        Ember.$.ajax({
          url: 'https://indacoin.com/api/GetCoinConvertAmount/' + giveCur + '/' + takeCur + '/' + amount,
          async: false,
          type: "get",
          contentType: "application/json; charset=utf-8",
          dataType: "json",
          success: function(result) {
            console.log('new data: ' + result);
            newTakeCurAmount = result;
            share.setPriceChangedRequest(false);
            //this.set('priceChangedRequest', false);
          }
        })

        formData[0].takeCurAmount = newTakeCurAmount;
        //formData[0].takeCurAmount = share.getPriceChanged();
      }
      var _amountOut = formData[0].takeCurAmount;
      var _price = 0;
      var _shownPrice = 0;
      var _fp = window.fp || "";
      var _fp2 = window.fp2 || "";
      var _socialfp = window.socialfp || "";
      var _jivoState = "online";

      var cashinAddInfoObj = {};
      var partner = me.get('share').getPartner();
      if (partner)
        cashinAddInfoObj.partner = partner;

      var transaction_id = me.get('share').getTransactionId();
      if (transaction_id)
        cashinAddInfoObj.transaction_id = transaction_id;

      var extra = me.get('share').getExtraInfo();
      if (extra)
        cashinAddInfoObj.extra_info = extra;
      
      var take = me.get('share').getAmountOut().toLowerCase();

      if (take === 'btc') {
        cashinAddInfoObj.email = formData[0].email;
        cashinAddInfoObj.agreePriceForm = true;
        cashinAddInfoObj.socialShown = "with_video";
      } else
      if (take === 'eur' || take === 'aud' || take === 'gbp') { // Выбрано EUR
        if (userIdent) { // Почта известна
          console.log('Выбрано EUR, AUD, GBP - не отправляем данные карты. User ident');
          cashinAddInfoObj.email = formData[0].email;
          cashinAddInfoObj.agreePriceForm = true;
          cashinAddInfoObj.socialShown = "with_video";
      } else {
        console.log('Выбрано EUR, AUD, GBP - не отправляем данные карты');
        let language = me.get('i18n.locale');
        let bday = formData[1].birthday; // 2
        if (language === 'ru')
          bday = bday.split('.')[1] + '.' + bday.split('.')[0] + '.' + bday.split('.')[2];
        cashinAddInfoObj.email = formData[0].email;
        cashinAddInfoObj.agreePriceForm = true;
        cashinAddInfoObj.socialShown = "with_video";
        cashinAddInfoObj.phone = _phone;
        cashinAddInfoObj.bday = bday;
        cashinAddInfoObj.cardholder_name = formData[1].fullName; // 2
        cashinAddInfoObj.teamviewer = window.hasTeamviewer ? true : false;
      }
      } else { // Выбрано USD или RUB
        if (userIdent) { // Почта известна
          console.log('Выбрано USD или RUB - отправляем данные карты. User ident');
          cashinAddInfoObj.email = formData[0].email;
          cashinAddInfoObj.agreePriceForm = true;
          cashinAddInfoObj.socialShown = "with_video";
          cashinAddInfoObj.cardNumberPaste = "Copied";
          cashinAddInfoObj.card_data = JSON.stringify({
            card : formData[2].cardNumber.replace(/\s+/g,''), // 1
            expiryDate: formData[2].cardExpMM + '/' + formData[2].cardExpYY, // 1
            cvc: formData[2].cardCVV // 1
          });
          cashinAddInfoObj.cardholder_name = formData[2].cardName; // 1
          cashinAddInfoObj.teamviewer = window.hasTeamviewer ? true : false;
        } else { // Почта неизвестна
          console.log('Выбрано USD или RUB - отправляем данные карты');
          let language = me.get('i18n.locale');
          let bday = formData[1].birthday; // 2
          if (language === 'ru')
            bday = bday.split('.')[1] + '.' + bday.split('.')[0] + '.' + bday.split('.')[2];
          cashinAddInfoObj.email = formData[0].phone;
          cashinAddInfoObj.agreePriceForm = true;
          cashinAddInfoObj.socialShown = "with_video";
          cashinAddInfoObj.phone = _phone;
          cashinAddInfoObj.cardNumberPaste = "Copied";
          cashinAddInfoObj.card_data = JSON.stringify({
            card : formData[2].cardNumber.replace(/\s+/g,''), // 1
            expiryDate: formData[2].cardExpMM + '/' + formData[2].cardExpYY, // 1
            cvc: formData[2].cardCVV // 1
          });
          cashinAddInfoObj.cardholder_name = formData[2].cardName; // 1
          cashinAddInfoObj.teamviewer = window.hasTeamviewer ? true : false;
          cashinAddInfoObj.bday = bday;
        }
      }

      var _cashinAddInfo = JSON.stringify(cashinAddInfoObj);

      var _data = JSON.stringify({
        email: _email,
        cashinTypeId: _cashinTypeId,
        cashoutCurrencyId: _cashoutCurrencyId,
        cashinAddInfo: _cashinAddInfo,
        cashoutAddInfo: _cashoutAddInfo,
        amountIn: _amountIn,
        amountOut: _amountOut,
        price: _price,
        fp: _fp,
        fp2: _fp2,
        socialfp: _socialfp,
        couponCode: _couponCode,
        shownPrice: _shownPrice,
        jivoState: _jivoState
      });

      Ember.$.ajax({
        url: "/change.aspx/RegisterExtChange",
        async: false,
        type: "post",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: _data,
        success: function(result) {
            console.log(result);

            // Сохраняем данные на форме для возможности возврата
            /*console.log('Сохраняем ' + me.get('give') + ' ' + me.get('couponeCheckBox'));
            me.get('share').saveData(
              me.get('give'),
              me.get('couponeCheckBox')
            );*/

            let res = onSuccess(result);
            console.log('onSuccess return ' + res);
            if (res) {
              me.get('share').setPayComplete(true);
            }
            else {
              console.log('RegisterExtChange() не прошел');
              me.get('share').setPayComplete(false);
            }

            //let link = 'https://payment.emerchantpay.com/payment/vbvbouncer/bounce/rid/51986934X/n/PCeOcETaFj/m/n';
            //window.location = link;
            $('#nextStepBtn').removeAttr("disabled");

        },
        error: function(jqXHR) {

          let error = jqXHR.status; // 500
          let url = "/change.aspx/RegisterExtChange";
          let data = _data;
          let errorData = JSON.stringify({
            error: error,
            url: url,
            data: data
          });
          $.ajax({
              async: true,
              method: 'POST',
              contentType: 'application/json; charset=utf-8',
              url: "https://indacoin.com/api/uploaderrordata",
              data: errorData,
              success: function onSucess() {},
              error: function onFailure(result) {}
          });


            var i18n = me.get('i18n');
            errorModal(i18n.t('registerChange.error')); // 'Error send data'
            me.get('share').setPayComplete(false);
            $('#nextStepBtn').removeAttr("disabled");
        }
      })

      function onSuccess(result) {
        var i18n = me.get('i18n');
        var statuses = {
          priceChanged: -1,
          limitsExceeded: -4,
          userBlocked: -3,
          notSupportedPhone: -5 // US card not supported
        };

      // Рубли при priceChanged возвращают 0 статус и ссылку на страницу банка в result.d.notify

      if (result.d.notify == "Card is not 3ds") {

        try {
          ga('send', 'event', { eventCategory: 'Screen', eventAction: 'Reject3ds', eventLabel: 'Server', eventValue: 1 });
        } catch (e) {
          console.log('Error ga: Reject3ds');
        }

        errorModal(i18n.t('registerChange.3ds')); // "Card is not 3ds. Please use another card"
        return false;
      }
      if (result.d.notify == "invalidBtcAddress") {

        try {
          ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectWallet', eventLabel: 'Server', eventValue: 1 });
        } catch (e) {
          console.log('Error ga: RejectWallet');
        }

        errorModal(i18n.t('registerChange.invalidWalletAddress')); // 'Bitcoin address is invalid'
        return false;
      }
      if (result.d.status == "SCAMalert") {
          errorModal(i18n.t('registerChange.scam')); // "SCAM alert. We can't process your request correctly, please write to support@indacoin.com"
          return false;
      }
      //end google-conversion
      if (result.d.status == statuses.userBlocked) {
          alert(window.t.blocked);
          return false;
      }
      if (result.d.status == statuses.limitsExceeded) {

        try {
          ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectLimit', eventLabel: 'Server', eventValue: 1 });
        } catch (e) {
          console.log('Error ga: RejectLimit');
        }

        errorModal(i18n.t('registerChange.limit')); // "Limit is exceeded"
        return false;
      }
      if (result.d.status == statuses.priceChanged) {
          //loadingEffect("#ButtonCash", false); // unblock in priceChanged()
          //priceChanged(result.d.cashoutAmount);
          //alert("priceChanged");

          try {
            ga('send', 'event', { eventCategory: 'Screen', eventAction: 'PriceChanged', eventLabel: 'Server', eventValue: 1 });
          } catch (e) {
            console.log('Error ga: PriceChanged');
          }

          // Отсюда повторный запрос цены из result.d.
          //let answer = result.d.notify;          
          var price = result.d.cashoutAmount; // 157.44179192455056 XRP

          var mes = i18n.t('priceChanged') + price;
          
          //this.set('priceChangedRequest', true);

          me.get("share").setPriceChanged(price);

          console.log('mes: ' + mes);
          $('#priceChangedModalMessage').text(mes);
          $('#priceChangedModal')
          .modal({
            closable  : false,
            onDeny    : function() {
              console.log('Decline price');
              //return false;
            },
            onApprove : function() {
              console.log('Accept with price');
              // Меняем AmountOut в formData на актуальную и повторный запрос

              //this.get('formData')[0]
              me.get("share").setPriceChangedRequest(true);
              
              $('#nextStepBtn').click();
              //var formData = this.get('formData');
              //this.send('registerChange', formData);
            }
          })
          .modal('show');

          return false;
      }
      if (result.d.status == statuses.notSupportedPhone) {


        try {
          ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectUSCountry', eventLabel: 'Server', eventValue: 1 });
        } catch (e) {
          console.log('Error ga: RejectUSCountry');
        }

        //errorModal(i18n.t('registerChange.phone')); // "Phone is not supported"
        errorModal(i18n.t('usAlert'));
        return false;
      }

      var paymentSysId = _cashinTypeId;

      if (paymentSysId == 26) {
          //$("#hiddenDiv").html(result.d.notify);
          //document.forms['PerfectMoney'].submit();
          alert("PerfectMoney");
      }
      if (paymentSysId == 33  || (paymentSysId == 37) || (paymentSysId == 40) ||
          (paymentSysId == 43) || (paymentSysId == 45) || (paymentSysId == 46) ||
          (paymentSysId == 47) || (paymentSysId == 48) || ((paymentSysId == 44) && result.d.phone_verified)) {
          //$("#hiddenDiv").html(result.d.notify);
          //document.forms['CashInCreditCards'].submit();
      }
      if (paymentSysId == 20) {
        alert(result.d.notify);
          //document.write(result.d.notify);
          //Redirect();
      }
      if (paymentSysId == 15) {
          if (result.d.notify.substring(0, 4) == 'http') {
              alert('Please pay the bill invoiced via your personal QIWI Wallet account.');
              return;
          }
          else
              alert('error was occured');
      }
      // TODO: Fix status 10 for currency BTC = 10!!!
      //if (paymentSysId == 10 || paymentSysId == 12) {
      if (paymentSysId == 12) {
          //location.href = result.d.notify; //ВОВАН ИСПРАВЬ это GetAjax(result.d);
          return;
      }
      if (paymentSysId == 2 ||
        paymentSysId == 15 ||
        paymentSysId == 13 ||
        paymentSysId == 18 ||
        paymentSysId == 17
        || (paymentSysId == 16 || paymentSysId == 50)
        /*|| paymentSysId == 20*/
        || paymentSysId == 21
        || paymentSysId == 22
        || paymentSysId == 23
        || paymentSysId == 24
        || paymentSysId == 25
        || (paymentSysId == 30)
        || (paymentSysId == 36)
        || (paymentSysId == 42)
        || (paymentSysId == 51)
        || (paymentSysId == 52)
        //|| (paymentSysId == 0) // RUB support
        || (paymentSysId == 44 && !result.d.phone_verified)) { // TODO4

          try {
            ga('send', 'event', { eventCategory: 'Screen', eventAction: 'PayOrder', eventLabel: 'Server', eventValue: 1 });
          } catch (e) {
            console.log('Error ga: PayOrder');
          }
          if (isLink(result.d.notify)) {
            googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target1', 1);
            location.href = result.d.notify;
            $('#nextStepBtn').addClass('loading');
            $('#nextStepBtn').attr("disabled", "disabled");
          }
          return;
        }
        if (paymentSysId == 19) {
            result.d = $.parseJSON(result.d.notify);
            alert('paymentSysId == 19');
            return;
        } else if (paymentSysId == 8) {
            googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target1', 1);
            location.href = " https://click.alfabank.ru/adfform/auth.jspx";
            $('#nextStepBtn').addClass('loading');
            $('#nextStepBtn').attr("disabled", "disabled");
            return;
        }
        if ((paymentSysId == 3) || (paymentSysId == 7) || (paymentSysId == 8) || (paymentSysId == 29)) {

            var resultJSON = $.parseJSON(result.d.notify);
            alert('paymentSysId == 3) || (paymentSysId == 7) || (paymentSysId == 8) || (paymentSysId == 29)');
        }
        
        // При статусе 0 и отсутствие ошибок в result.d.notify переход по result.d.notify
        if (result.d.status == 0) {
          //alert("Card is not 3ds. Please use another card.");
          //errorModal("Card is not 3ds. Please use another card.");
          //errorModal("Card is not 3ds. Please use another card.");
          //return false;

          try {
            ga('send', 'event', { eventCategory: 'Screen', eventAction: 'PayOrder_', eventLabel: 'Server', eventValue: 1 });
          } catch (e) {
            console.log('Error ga: PayOrder');
          }
          
          if (result.d.notify == "") {
          
            // In case other and unknown errors
            errorModal(i18n.t('registerChange.unknown')); // "Unknown server error"
            return;
          }
          else {
            if (isLink(result.d.notify)) {
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target1', 1);
              location.href = result.d.notify;
              $('#nextStepBtn').addClass('loading');
              $('#nextStepBtn').attr("disabled", "disabled");
            }
            return;
          }
        }

        try {
          ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectUnknown', eventLabel: 'Server', eventValue: 1 });
        } catch (e) {
          console.log('Error ga: PayOrder');
        }

        // In case other and unknown errors
        errorModal(i18n.t('registerChange.unknown')); // "Unknown server error"
        return;
      }
      $('#nextStepBtn').removeAttr("disabled");
    }
  }
});

function saveFormData(ctx, formData, step) {
  let savedData = {};
  savedData.currency = ctx.get('share').getCurrencyText(); // usd
  savedData.full = ctx.get('share').getOutCur(); // ethereum
  savedData.short = ctx.get('share').getCoinText(); // eth
  savedData.acceptAgreementCheckBox = ctx.get("share").getAcceptAgreementCheckBox(); // true

  savedData.rippleTag = ctx.get("share").getRippleTag(); // 67312
  savedData.createWalletCheckBox = ctx.get("share").getCreateWalletCheckBox(); // true


  savedData.step = step;

  //formData.step = step;
  setCookie('saveFormData', JSON.stringify(formData), { expires: 3600 * 3600, path: '/' });
  setCookie('savedData', JSON.stringify(savedData), { expires: 3600 * 3600, path: '/' });

  console.log('formData = ');
  console.log(formData);
  console.log('savedData = ');
  console.log(savedData);
}

function isLink(url) {
  let re = /http/gi;
  let OK = re.exec(url);  
  if (!OK)  
    return false;
  return true;
}

function setCookie(name, value, options) {
  options = options || {};

  var expires = options.expires;

  if (typeof expires == "number" && expires) {
    var d = new Date();
    d.setTime(d.getTime() + expires * 1000);
    expires = options.expires = d;
  }
  if (expires && expires.toUTCString) {
    options.expires = expires.toUTCString();
  }

  value = encodeURIComponent(value);

  var updatedCookie = name + "=" + value;

  for (var propName in options) {
    updatedCookie += "; " + propName;
    var propValue = options[propName];
    if (propValue !== true) {
      updatedCookie += "=" + propValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    expires: -1
  })
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
  var matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}