import Ember from 'ember';

export default Ember.Controller.extend({
  queryParams: ['page', 'size', 'give', 'take', 'discount'],
  page: 0,
  size: 20,
  category: false,
  cashInfo: [], // Информация о принимаемых валютах и лимитах
  give: 'usd',
  take: 'btc',
  //errorLoadData: true,
  giveCurStatusUSD: Ember.computed('give', function(){
    let cur = this.get('give').toUpperCase();
    if(cur === 'EUR') {
      return false;
    } else {
      return 'teal';
    }
  }),
  giveCurStatusEUR: Ember.computed('give', function(){
    let cur = this.get('give').toUpperCase();
    if(cur === 'EUR') {
      return 'teal';
    } else {
      return false;
    }
  }),
  giveCurStatusRUB: Ember.computed('give', function(){
    let cur = this.get('give').toUpperCase();
    if(cur === 'RUB') {
      return 'teal';
    } else {
      return false;
    }
  }),
  giveCurSymbol: Ember.computed('give', function(){
    let cur = this.get('give').toUpperCase();
    if(cur === 'EUR') {
      return '€';
    } else if (cur === 'USD') {
      return '$';
    } else {
      return '₽';
    }
  }),
  giveCur: Ember.computed('give', function(){
    let cur = this.get('give').toUpperCase();
    //console.log('Computed give ' + cur);
    if(cur === 'EUR') {
      return 'EUR';
    } else if (cur === 'USD') {
      return 'USD';
    } else {
      return 'RUB';
    }
  }),
  // header: Ember.computed('page', 'header','model', function(){
  //   if(this.get('page') === 0) {
  //     return true
  //   } else {
  //     return false
  //   }
  // }),
  firstPage: Ember.computed('size', function() {
    return this.get('size') > 40 ? true : false;
  }),
  prevPageState: Ember.computed('size', function(){
    return this.get('size') > 20 ? true : false;
  }),
  prevPage: Ember.computed('size', function(){
    return this.get('size') - 20;
  }),
  nextPage: Ember.computed('size', 'model', function(){
    return this.get('size') + 20;
  }),
  color: Ember.computed('model', function() {
    return 'green';
  }),
  loading: 'loading',
  modelChange: Ember.observer('model', function() {
    this.set('loading', false);
}),
  actions: {
    // refreshCurrentModel() {
    //   let route = Ember.getOwner(this).lookup(`route:${get(this, 'currentRouteName')}`);
    //   return route.refresh();
    // },
    getMore() {

      try {
        ga('send', 'event', { eventCategory: 'Buttons', eventAction: 'Click', eventLabel: 'MainForwardButton', eventValue: 1 });
      } catch (e) {
        console.log('Error ga: MainForwardButton');
      }

      this.set('loading', 'loading');
      this.set('size', this.get('nextPage'));
    }
  }
});
