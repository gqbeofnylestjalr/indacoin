import Ember from 'ember';

export default Ember.Controller.extend({
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  queryParams: ['name', 'id'],
  name: null,
  id: null,
  isBitcoin: false,
  isEthereum: false,
  count: true,

  noFoundCurrency: false, // false - крипта есть

  chartData: Ember.computed('model', function(){
    var chartData = this.get('model.chart');
    var result = [];
    for (var i = 0; i < chartData.length; i++) {
      var price = chartData[i].high;
      
      var coords = {
        x: i,
        y: price
      };
      result.push(price);
    }
    return result;
  }),
  chartLabels: Ember.computed('model', function(){
    
    var chartData = this.get('model.chart');
    var result = [];
    for (var i = 0; i < chartData.length; i++) {
      //let date = chartData[i].time.split(' ')[0]; // "time": "2017-12-27 00:00:00",

      let date = new Date();
      date.setTime(chartData[i].time * 1000);
      
      //let formatDate = date.toLocaleString("ru", { year: 'numeric', month: 'long', day: 'numeric' });
      //let region = this.get("share").getRegion();
      let region = this.get('i18n.locale');
      let formatDate = date.toLocaleString(region, { month: 'long' });
      //formatDate = formatDate.charAt(0).toUpperCase() + formatDate.substr(1);
      formatDate = formatDate.charAt(0).toUpperCase() + formatDate.charAt(1) + formatDate.charAt(2);

      //let formatDate = date.toLocaleString(region, { year: 'numeric', month: 'long', day: 'numeric' });
      
      //console.log( date.toLocaleString("en", { year: 'numeric', month: 'long', day: 'numeric' }) );
      //console.log( date.toLocaleString("ar", { year: 'numeric', month: 'long', day: 'numeric' }) );
      
      //let date = chartData[i].time; // "time":1345248000
      result.push(formatDate);
    }
    // console.log(result);
    return result;
  }),
  chart: Ember.computed('model', function(){
    //console.log('Charttt');
    //console.log(this.get('model.info.0.name'));
    //let ctx = this.get('chart').getContext('2d');
    //var context = this.get('element').getContext('2d');


    /*console.log('Call gradient' + this.get('count'));
    if (this.get('count')) {
      this.set('count', false)
      var canvas = this.get('chart')
      var ctx = canvas.getContext('2d');
      var gradient = ctx.createLinearGradient(0, 0, 200, 0);
      gradient.addColorStop(0, 'green');
      gradient.addColorStop(1, 'white');
    }*/

    //$('#chartjs-render-monitor').createLinearGradient(500, 0, 100, 0);
    //gradientFill.addColorStop(0, "rgba(128, 182, 244, 0.6)");
    //gradientFill.addColorStop(1, "rgba(244, 144, 128, 0.6)");
    /*
    if (this.get('count')) {
      this.set('count', false)
      var gradientFill = this.get('chart').createLinearGradient(500, 0, 100, 0);
      gradientFill.addColorStop(0, "rgba(128, 182, 244, 0.6)");
      gradientFill.addColorStop(1, "rgba(244, 144, 128, 0.6)");
    }*/

    return {

      type: 'line',
      data: {
        labels: this.get('chartLabels'),
        display: false,
        datasets: [{
          label: 'Price of ' + this.get('model.info.0.name') + " $",
          data: this.get('chartData'),
          pointRadius: 0,
          hitRadius: 5,
          //fill: false,
          fill: 'start',
          //borderColor: '#00B57A',
          /*backgroundColor: function() {
            console.log('call backgroundColor()');
          },*/
          //backgroundColor: 'rgba(43, 62, 80, 0.05)',
          //backgroundColor : '#62bcbc',
          //backgroundColor: gradientFill,
          borderColor: '#64aecc',
          pointBackgroundColor: '#64aecc',
          lineTension: 0.5,
          // borderWidth: '2px'
          DrawOnChartArea: true
        }]
      },
      options: {
        legend: {
          display: false
        },

        color: function(context) {
          console.log('call color');
          var index = context.dataIndex;
          var value = context.dataset.data[index];
          return value < 0 ? 'red' :  // draw negative values in red
              index % 2 ? 'blue' :    // else, alternate values in blue and green
              'green';
        },
        scales: {
            yAxes: [{
                display: true,
                fontFamily: 'Helvetica Header',
                type: 'linear',
                ticks: {
                    // Include a dollar sign in the ticks
                    callback: function(value, index, values) {
                        //alert(value);
                        return '$ ' + value;
                    }
                }
            }],
            xAxes: [{
              display: true,
              fontFamily: 'Helvetica Header',
              //fontSize: 30,
              //type: 'linear',
              ticks: {
                maxTicksLimit: 12
              }
            }]

        }
    }
    }
  
  })


  /*}),
  chart: Ember.computed('model', function(){
    return {
      data: {
        //labels: this.get('chartLabels'),
        //labels: [1, 2, 3],
        labels: this.get('chartLabels'),
        datasets: [{
          
          fontSize: 30,
          //label: 'Price of ' + this.get('model.chart.history.0.name'),
          label: 'Price of Bitcoin 1',
          data: this.get('chartData'),
          pointRadius: 0,
          fill: false,
          borderColor: '#5796bd',
          pointBackgroundColor: '#5796bd',
          lineTension: 0.5,
          DrawOnChartArea: false
          // borderWidth: '2px'
        }]
      },
      options: {
        scales: {
          yAxes: [{
            ticks: {
              callback: function(value, index, values) {
                console.log(value);
                return value + " $";
              }
            }
          }]
        }
      }
    }
  })*/
});
