import Ember from 'ember';

export default Ember.Controller.extend({
  errorLoadData: false,
  queryParams: ['article'],
  article: null,
  actions: {
    expandNews(id) {
      //console.log('call expandNews = ' + id);
      this.get('store').peekAll('news').forEach(function(item){
        item.set('show', false);
      })
      let record = this.get('store').peekRecord('news', id);
      if (record !== null) {
        record.set('show', true);
        this.set('article', id);
      }
    }
  }/*,
  setupController: function(controller, model) {
    console.log('call setupController from news');
    this._super(controller, model);
    controller.set('article', null);
  }*/

  

  /*,filteredArticles: Ember.computed('article', function() {
    var article = this.get('article');
    this.send('expandNews', article);

    var article = this.get('article');
    console.log('filteredArticles = ' + article);
    this.send('expandNews', article);
    return article;
    
    
    
    var articles = this.get('model');

    if (category) {
      return articles.filterBy('category', category);
    } else {
      return articles;
    }
  })*/

});
