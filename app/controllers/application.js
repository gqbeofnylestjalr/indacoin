import Ember from 'ember';
import ENV from 'fastboot/config/environment';
import { hrefTo } from 'ember-href-to/helpers/href-to';

export default Ember.Controller.extend({
    share: Ember.inject.service('share'),
    i18n: Ember.inject.service(),
    cookies: Ember.inject.service(),
    fastboot: Ember.inject.service(),
    ENV: ENV,


    init: function () {
        this._super();
        const i18n = this.get('i18n');
        const me = this;
        let showAlert = this.get("share").getShowAlert();
        this.set('stopSidebar', showAlert);

        // For show banner when run only without fastboot
        if (typeof FastBoot === 'undefined') {
            this.set('FASTBOOT', false);

            this.set('bannerTitle', i18n.t('exchangeOrder.registration'));


        } else
            this.set('FASTBOOT', true);

        if (typeof FastBoot === 'undefined') {
            var cookieService = this.get('cookies');

            var visited = cookieService.read('visited');
            if (!visited) {
                me.set('cookieAgree', true);
            }

            // Special mark for partners via key 'InstantCoin' in 'User-Agent'
            let showSmartBanner = "";
            const userAgent = navigator.userAgent; // "Mozilla/5.0 (Linux; Android 8.0.0; FIG-LX1 Build/HUAWEIFIG-LX1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.99 Mobile Safari/537.36 InstantCoin";
            if (userAgent) {
                if (userAgent.toLowerCase().indexOf('instantcoin') !== -1) {
                    showSmartBanner = "InstantCoin";
                }
            }
            if (showSmartBanner) {
                me.set('showSmartBanner', false);
                me.set('cookieAgree', false);
            } else {
                me.set('showSmartBanner', true);
            }
        }
    },
    cookieMessage: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        let href = hrefTo(this, 'lang.terms-of-use', ENV.i18n.lang); // "/de_GB/terms-of-use"
        let text = i18n.t('modals.cookie-modal.link'); // Learn More
        let linkToPolicy = `<a href="${href}?anchor=cookiepolicy">${text}</a>`; // "<a href="/de_GB/terms-of-use">Cookie policy</a>"
        return Ember.String.htmlSafe(i18n.t('modals.cookie-modal.text', { link: linkToPolicy }));
      }),
    actions: {
        confirmCookiePolicy() {
            var cookieService = this.get('cookies');
            //cookieService.write('visited', 'yes', { expires: '2038-06-02T06:02:34.000Z', path: '/' });
            //cookieService.write('visited', 'yes', { 'max-age': '2038-06-02T06:02:34.000Z', path: '/' });
            //$.cookie('visited', 'yes', { expires: 365, path: '/' });

            function setCookie(name, value, options) {
                options = options || {};
              
                var expires = options.expires;
              
                if (typeof expires == "number" && expires) {
                  var d = new Date();
                  d.setTime(d.getTime() + expires * 1000);
                  expires = options.expires = d;
                }
                if (expires && expires.toUTCString) {
                  options.expires = expires.toUTCString();
                }
              
                value = encodeURIComponent(value);
              
                var updatedCookie = name + "=" + value;
              
                for (var propName in options) {
                  updatedCookie += "; " + propName;
                  var propValue = options[propName];
                  if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                  }
                }
              
                document.cookie = updatedCookie;
              }
              setCookie('visited', 'yes', { expires: 100000000, path: '/' });

            this.set('cookieAgree', false);
        }
    }
});