import Ember from 'ember';

export default Ember.Service.extend({

  lang: null,
  inCur: null,

  // Данные сохраненной формы
  step: null,
  couponeCheckBox: false,

  redirectFromBank: false,
  confirmCode: null,
  requestId: null,

  showPhoneVerification: false,
  //showCardVerification: false,
  showRecordVideo: false,
  getShowRecordVideo() {
    return this.get('showRecordVideo');
  },

  currency: [],

  language: null, // ru
  region: 'GB', // US

  shouldRedirect: null,
  setShouldRedirect(value) {
    this.set('shouldRedirect', value);
  },
  getShouldRedirect() {
    return this.get('shouldRedirect');
  },
  startRedirectTimer: function(duration, url) {
    var timer = duration, minutes, seconds;
    setInterval(function () {
                minutes = parseInt(timer / 60, 10);
                seconds = parseInt(timer % 60, 10);
                minutes = minutes < 10 ? "0" + minutes : minutes;
                seconds = seconds < 10 ? "0" + seconds : seconds;
                $("#partnerBackLink a").html("You will be redirected to partner in " + seconds + " seconds");
                if (--timer < 0) {
                    window.location = url;
                    timer = duration;
                }
        },
        1000);
  },

  phoneNumber: null,
  getPhoneNumber() {
    return this.get('phoneNumber');
  },
  setPhoneNumber(value) {
    this.set('phoneNumber', value);
  },

  transactionId: null,
  getTransactionId() {
    return this.get('transactionId');
  },
  setTransactionId(value) {
    this.set('transactionId', value);
  },

  extraInfo: null,
  getExtraInfo() {
    return this.get('extraInfo');
  },
  setExtraInfo(value) {
    this.set('extraInfo', value);
  },

  partner: null,
  getPartner() {
    return this.get('partner');
  },
  setPartner(value) {
    this.set('partner', value);
  },

  isVideoRecord: false,
  getIsVideoRecord() {
    return this.get('isVideoRecord');
  },
  setIsVideoRecord(value) {
    this.set('isVideoRecord', value);
  },

  showAlert: false,
  getShowAlert() {
    return this.get('showAlert');
  },
  setShowAlert(value) {
    this.set('showAlert', value);
  },

  availableCurrency: [],
  getAvailableCurrency() {
    return this.get('availableCurrency');
  },
  setAvailableCurrency(value) {
    this.set('availableCurrency', value);
  },

  //transactionId: '6335d36a89e143db7877d8b4d73702281e4fa66b5c6c83809057a6483d8d3db5',
  //transactionId: 'local_payment',
  transactionId: null,
  getTransactionId() {
    return this.get('transactionId');
  },

  KYCUrl: null,
  //KYCUrl: "https://indacoin.com/iframe/kyc_frame.aspx?access_token=a9fb90d8-daaa-4692-8c33-cc5797033417\u0026applicant_id=5a952e010a975a5163bc739f\u0026user_id=-1\u0026lang=en",
  getKYCUrl() {
    return this.get('KYCUrl');
  },

  // Incoming Force from get-parameter
  force: null,
  getForce() {
    return this.get('force');
  },
  setForce(value) {
    this.set('force', value);
  },

  // Incoming email address from get-parameter
  email: null,
  getEmail() {
    return this.get('email');
  },
  setEmail(value) {
    this.set('email', value);
  },

  // Incoming wallet address from get-parameter
  wallet: null,
  getWallet() {
    return this.get('wallet');
  },
  setWallet(value) {
    this.set('wallet', value);
  },

  // Incoming wallet address from get-parameter
  // blockWallet: false,
  // getBlockWallet() {
  //   if (this.get('blockWallet') == true) {
  //     this.set('blockWallet', false);
  //     return true;
  //   } else {
  //     return false;
  //   }
  // },
  // setBlockWallet() {
  //   this.set('blockWallet', true);
  // },

  //Incoming get-param amount_out
  amountNum: null,
  getAmountNum() {
    return this.get('amountNum');
  },
  setAmountNum(value) {
    this.set('amountNum', value);
  },

  amountPay: null,
  getAmountPay() {
    return this.get('amountPay');
  },
  setAmountPay(value) {
    this.set('amountPay', value);
  },

  // new
  amountAltToSend: null,
  getAmountAltToSend() {
    return this.get('amountAltToSend');
  },
  setAmountAltToSend(value) {
    this.set('amountAltToSend', value);
  },

  priceChangedRequest: false,
  getPriceChangedRequest() {
    return this.get('priceChangedRequest');
  },
  setPriceChangedRequest(value) {
    this.set('priceChangedRequest', value);
    console.log('set priceChangedRequest is ' + this.get('priceChangedRequest'));
  },
  priceChanged: "",
  getPriceChanged() {
    return this.get('priceChanged');
  },
  setPriceChanged(value) {
    this.set('priceChanged', value);
  },

  couponeCheckBox: false,
  getCouponeCheckBox() {
    return this.get('couponeCheckBox');
  },
  setCouponeCheckBox(value) {
    this.set('couponeCheckBox', value);
  },

  rippleTag: null,
  getRippleTag() {
    return this.get('rippleTag');
  },
  setRippleTag(value) {
    this.set('rippleTag', value);
  },

  createWalletCheckBox: false,
  getCreateWalletCheckBox() {
    return this.get('createWalletCheckBox');
  },
  setCreateWalletCheckBox(value) {
    this.set('createWalletCheckBox', value);
  },

  acceptAgreementCheckBox: false,
  getAcceptAgreementCheckBox() {
    return this.get('acceptAgreementCheckBox');
  },
  setAcceptAgreementCheckBox(value) {
    this.set('acceptAgreementCheckBox', value);
  },

  // Hiding fileds in case known email. "True" in case email is known, "false" in case email is unknown
  userIdent: false,
  getUserIdent() {
    return this.get('userIdent');
  },
  setUserIdent(value) {
    this.set('userIdent', value);
  },

  // Значение для выпадающего списка USD
  currencyText: "",
  getCurrencyText() {
    return this.get('currencyText');
  },
  setCurrencyText(value) {
    //console.log('DEBUG: setCurrencyText = ' + value);
    this.set('currencyText', value);
  },

  // Значение для выпадающего списка LTC
  coinText: "",
  getCoinText() {
    return this.get('coinText');
  },
  setCoinText(value) {
    this.set('coinText', value);
  },

  // Значение для выпадающего списка litecoin
  coinId: "",
  getCoinId() {
    return this.get('coinId');
  },
  setCoinId(value) {
    this.set('coinId', value);
  },

  // Текущий курс доллара
  currentUsd: 0,
  getCurrentUsd() {
    return this.get('currentUsd');
  },
  setCurrentUsd(value) {
    this.set('currentUsd', value);
  },

  // Текущий курс евро
  currentEur: 0,
  getCurrentEur() {
    return this.get('currentEur');
  },
  setCurrentEur(value) {
    this.set('currentEur', value);
  },

  // Лимит на покупку пользователя
  cardLimit: 30,
  getCardLimit() {
    return this.get('cardLimit');
  },
  setCardLimit(value) {
    this.set('cardLimit', value);
  },

  // Лимиты на покупку в платежных шлюзах
  limits: [],
  getLimits() {
    return this.get('limits');
  },
  setLimits(value) {
    this.set('limits', value);
  },

  leftCur: "",
  getLeftCur() {
    return this.get('leftCur');
  },
  setLeftCur(value) {
    this.set('leftCur', value);
  },

  amountOut: 'usd',
  getAmountOut() {
    return this.get('amountOut');
  },
  setAmountOut(value) {
    //console.log('Сохранили ' + value);
    this.set('amountOut', value);
  },

  phoneId: null,
  getPhoneId() {
    return this.get('phoneId');
  },
  setPhoneId(value) {
    this.set('phoneId', value);
  },

  formStatus: null,
  getFormStatus() {
    return this.get('formStatus');
  },

  formData: null,
  getFormData() {
    return this.get('formData');
  },
  setFormData(value) {
    this.set('formData', value);
  },

  savedData: null,
  getSavedData() {
    return this.get('savedData');
  },
  setSavedData(value) {
    this.set('savedData', value);
  },

  // ============ Данные с getDate() ============

  // Код валюты из mobgetcurrency для вывода на странице
  // Если ее нет, то берем валюту из 
  //altCurrencyId: 101,
  altCurrencyId: null,
  getAltCurrencyId() {
    return this.get('altCurrencyId');
  },
  setAltCurrencyId(value) {
    this.set('altCurrencyId', value);
  },


  payOutSystem: 10,
  getPayOutSystem() {
    return this.get('payOutSystem');
  },
  setPayOutSystem(value) {
    this.set('payOutSystem', value);
  },

  //payDate: null,
  payDate: "19.09.2017 10:44:05",
  getPayDate() {
    return this.get('payDate');
  },

  payInAmount: 100,
  //payInAmount: null,
  getPayInAmount() {
    return this.get('payInAmount');
  },

  //cashIn: 10,
  cashIn: null,
  getCashIn() {
    return this.get('cashIn');
  },

  //cryptoAdress: '0x6cace0528324a8afc2b157ceba3cdd2a27c4e21f',
  cryptoAdress: "",
  getCryptoAdress() {
    return this.get('cryptoAdress');
  },

  //cryptoAdress: '0x6cace0528324a8afc2b157ceba3cdd2a27c4e21f',
  cryptoAdressIn: "",
  getCryptoAdressIn() {
    return this.get('cryptoAdressIn');
  },

  //cryptoAdress: '0x6cace0528324a8afc2b157ceba3cdd2a27c4e21f',
  iADhash: "",
  getiADhash() {
    return this.get('iADhash');
  },

  payOutAmount: '0.000456',
  //payOutAmount: null,
  getPayOutAmount() {
    return this.get('payOutAmount');
  },

  //payOutCurrency: 'BTC',
  payOutCurrency: "",
  getPayOutCurrency() {
    return this.get('payOutCurrency');
  },

  status: 'Completed',
  //status: null,
  getStatus() {
    return this.get('status');
  },

  getRegion() { // like 'RU'
    return this.get('region');
  },
  setRegion(value) {
    //console.log('Сохраняем регион: ' + value);
    this.set('region', value);
  },

  getLanguage() { // like 'ru', 'en'
    return this.get('language');
  },
  setLanguage(value) {
    this.set('language', value);
  },

  getCurrency() {
    return this.get('currency');
  },
  setCurrency(value) {
    this.set('currency', value);
  },

  getShowPhoneVerification() {
    return this.get('showPhoneVerification');
  },
  setShowPhoneVerification(complete) {
    this.set('showPhoneVerification', complete);
  },

  getShowCardVerification() {
    return this.get('showCardVerification');
  },
  setShowCardVerification(complete) {
    this.set('showCardVerification', complete);
  },

  getShowRecordVideo() {
    return this.get('showRecordVideo');
  },
  setShowRecordVideo(complete) {
    this.set('showRecordVideo', complete);
  },

  payComplete: false,
  getPayComplete() {
    return this.get('payComplete');
  },
  setPayComplete(complete) {
    this.set('payComplete', complete);
  },

  getConfirmCode() {
    return this.get('confirmCode');
  },
  getRequestId() {
    return this.get('requestId');
  },
  setConfirmCode(confirmCode) {
    this.set('confirmCode', confirmCode);
  },
  setRequestId(requestId) {
    this.set('requestId', requestId);
  },
  setRedirectFromBank(value) {
    this.set('redirectFromBank', value);
  },
  getRedirectFromBank() {
    return this.get('redirectFromBank');
  },
  saveData(step, couponeCheckBox) {
    localStorage.setItem('step', step);
    localStorage.setItem('couponeCheckBox', couponeCheckBox); 

    this.set('step', step);
    this.set('couponeCheckBox', couponeCheckBox);
  },

  // Load data from localStorage for redirected back users
  loadData() {
    //console.log(localStorage.getItem('step'));
    //debugger;
    //let step = localStorage.getItem('step');
    //let couponeCheckBox = localStorage.getItem('couponeCheckBox');
    /*return {
      'step': step,
      'requestId': couponeCheckBox
    };*/
  },

  init() {
    this._super(...arguments);
    //this.set('redirectFromBank', false);
    //this.set('items', []);
    //this.set('linkToRedirect', 'foo')
  },
  setLang(value) {
    this.set('lang', value);
  },

  // Cryptocurrency - BTC
  outCur: 'Bitcoin',
  setOutCur(value) {
    //this.set('outCur', value.toString());
    this.set('outCur', value);
  },
  getOutCur() {
    return this.get('outCur');
  },

  setInCur(value) {
    this.set('inCur', value.toString().toLowerCase());
  },
  getLink() {
    let outCur = this.get('outCur').toLowerCase();
    let inCur = this.get('inCur').toLowerCase();
    let link = '/' + this.get('lang') + '/change/buy-' + outCur + '-with-card' + inCur;
    return link;
  },
  setPathName(){
    this.set('pathname', window.location.pathname);
  },
  getPathName(){
    return this.get('pathname');
  },
  changeUrl() {
    let url = window.location.pathname;

    // TODO: получить из i18n все доступные языки и страны ru и RU
    //debugger;
    var regex = /^\/(\w\w_\w\w)\/([\w-]+)\/([\w-]+)/i;
    var match = regex.exec(url);
    try {
      if (match.length > 4) {
        throw "Много параметров";
      } else
      if (match.length === 4) {
        let first = match[1].toLowerCase();
        let second = match[2].toLowerCase();
        let third = match[3].toLowerCase();

        console.log('1 : ' + first);
        console.log('2 : ' + second);
        console.log('3 : ' + third);
      } else
      if (match.length === 3) {
        let first = match[1].toLowerCase();
        let second = match[2].toLowerCase();

        console.log('1 : ' + first);
        console.log('2 : ' + second);
      }
    }
    catch (e) {
      console.log('Ошибка: ' + e.message)
      // Редирект на определенный язык /ru_RU
    }

    //window.history.replaceState( {}, 'linkToRedirect', '/linkToRedirect' );
  },
  /*,
  getNotify(){
    let url = window.location.pathname;
    // /ru_RU/change/buy-bitcoin-with-cardusd/notify?confirm_code=a11EaaD224&request_id=7141495
    // /ru_RU/change/buy-bitcoin-with-cardusd
    var regex = /^\/(\w\w_\w\w)\/([\w-]+)\/([\w-]+)\/notify\?confirm_code=(\w+)&request_id=(\d+)/i;
    var match = regex.exec(url);
    try {
      if (match.length === 6) {
        let confirmCode = match[5].toLowerCase();
        let requestId = match[6].toLowerCase();

        return {'confirmCode': confirmCode, 'requestId': requestId};
      } else return -1;
    }
    catch (e) {
      console.log(e.message);
      return -1;
      // Редирект на определенный язык /ru_RU
    }
  }*/
  notify(requestId, confirmCode) {
    //if (typeof FastBoot === 'undefined') {

    const me = this;
    //notify(){
    //var requestId = '7139084';
    //var confirmCode = 'f53c3D9167';
    //https://indacoin.com/gw/payment_status?request_id=7139084&confirm_code=
    console.log('Call Notify()');
    console.log('requestId = ' + requestId);
    console.log('confirmCode = ' + confirmCode);
    $.ajax({
      async: false,
      method: 'POST',
      contentType: 'application/json; charset=utf-8',
      url: "/Notify/getData",
      //data: "{'requestId':'" + getParamFromUrl("request_id") + "','confirmCode':'" + getParamFromUrl("confirm_code") + "'}",
      data: "{'requestId':'" + requestId + "','confirmCode':'" + confirmCode + "'}",
      success: function onSucess(result) {
        console.log("RefreshGlobalData success");
        var NotifyData = $.parseJSON(result.d);
        //window.NotifyData = NotifyData;

        let exchangerStatuses = {
          BillWaiting: "Awaiting bill to be payed",
          CashinWaiting: "Your payment is being processed now, which should take up to 2 minutes. If 2 minutes has passed and you still see this status, unfortunately your payment has been declined. You need to check with your bank that your card does support 3D-Secure (Verified by Visa or Mastercard Securecode), meaning that you always enter a secret code/password tied to the card, when pay over the internet. Sometimes the code could be sent to your mobile phone number.",
          Completed: "Completed",
          Declined: "Declined",
          Error: "Error was occured, please contact Indacoin support support@indacoin.com",
          MoneySend: "The funds have been sent",
          Processing: "In progress",
          TimeOut: "Order timeout",
          Verifying: "Verifying",
          WaitingForAccountCreation: "Waiting for Indacoin account creation (we have sent to you a registration mail)",
          cardDeclined: "Your card was declined, if you have questions, please contact Indacoin support support@indacoin.com",
          cardDeclinedNoFull3ds: "Declined, because your bank doesn't support full 3ds (only Attempted) option on this card, please contact your bank customer service."
        };

        let cashIn = {
          1: "Credit/Debit cards",
          2: "Russian pay terminals",
          3: "QIWI",
          4: "Bashkomsnabbank",
          5: "Novoplat terminals",
          6: "Elexnet terminals",
          7: "Credit/Debit cards (USD)",
          8: "Alfa-click",
          9: "Wire transfer",
          10: "Bitcoin",
          12: "Litecoin",
          13: "Astropay",
          15: "QIWI",
          16: "Credit/Debit cards (USD)",
          17: "Pinpay",
          18: "Payeer",
          19: "Liqpay",
          20: "Yandex.Money",
          21: "Elexnet",
          22: "Kassira.net",
          23: "Mobil element",
          24: "Svyaznoy",
          25: "Euroset",
          26: "PerfectMoney",
          27: "IndacoinInternal",
          29: "Yandex.Money",
          30: "OKPay",
          31: "Referral",
          33: "Payza",
          35: "BTC-E code",
          36: "Credit/Debit cards (USD)",
          37: "Credit/Debit cards (USD)",
          39: "Wire transfer",
          40: "Yandex.Money",
          42: "QIWI(Handmade)",
          43: "UnionPay",
          44: "QIWI(Automatic)",
          45: "Mobile commerce(Russia only)",
          49: "LibrexCoin",
          50: "Credit/Debit cards (EUR)",
          51: "QIWI(Automatic)", 
          53: "Ethereum"
        };

        let cashOut = {
          1: "Webmoney",
          2: "Yandex.Money",
          3: "Beeline",
          4: "Megafon",
          5: "MTS",
          6: "Wire transfer",
          7: "Credit/Debit Cards (Russia)",
          8: "Bank cards (Ukraine)",
          9: "Bank cards (PrivateBank)",
          10: "Bitcoin",
          12: "Litecoin",
          15: "Cashier",
          16: "PerfectMoney",
          18: "Payeer",
          19: "Indacoin internal",
          20: "OKPay",
          23: "Bank cards (World)",
          24: "QIWI",
          25: "TELE2",
          26: "Payza",
          28: "Credit/Debit Cards (All countries)",
          29: "Credit/Debit Cards (Tinkoff & Avangard)",
          30: "BTCe Code",
          31: "LibrexCoin",
          32: "Credit/Debit Cards (Russia, CIS)",
          33: "Yandex.Money",
          34: "Ethereum"
        };

        let cardStatus = '';
        if (NotifyData.s == 'CashinWaiting') {
          if (NotifyData.iT == 10) {
            if (NotifyData.cnf == '-1')
              cardStatus = 'cashinWaitingBtc';
            else
              cardStatus = 'processingBtc';
          }
          else
            cardStatus = 'cashinWaiting';
        }
        else if (NotifyData.s == 'Completed')
          cardStatus = 'completed';
        else if (NotifyData.s == 'Processing') {
          if (NotifyData.iT == 10)
            cardStatus = 'processingBtc';
          else
            cardStatus = 'processing';
        }
        else if (NotifyData.s == 'TimeOut') {
          if (NotifyData.Non3DS == '1') {
            cardStatus = 'Non3DS';
          } else {
            cardStatus = 'orderTimeOut';
          }
        }
        //else if (NotifyData.Non3DS == true)
        //  cardStatus = 'processing';

        if (NotifyData.s == "Completed" || NotifyData.s == "MoneySend" || NotifyData.s == "Declined"|| NotifyData.s == "TimeOut") {
          var url = NotifyData.partner_url;
          if (url && url.length > 0 && url.toLowerCase() != "null")
            if (NotifyData.ex_transaction_id && NotifyData.ex_transaction_id > 0) {
                if (NotifyData.partner_url.includes('?')) {
                    url = NotifyData.partner_url + "&transaction_id=" + NotifyData.ex_transaction_id;
                } else {
                    url = NotifyData.partner_url + "?transaction_id=" + NotifyData.ex_transaction_id;
                }
                me.set('shouldRedirect', url);
            }
        }

        if (NotifyData.id_add_status && NotifyData.id_add_status > 0) {
          console.log('isVideoRecord > 0');
          me.set('isVideoRecord', true);
        }

        let exTransactionId = NotifyData.ex_transaction_id;

        let cryptoAdress = NotifyData.oAd;
        me.set('cryptoAdress', cryptoAdress);
        me.set('cryptoAdressIn', NotifyData.iAd);
        me.set('iADhash', NotifyData.iADhash);


        let payDate = NotifyData.d;
        me.set('payDate', payDate);
        me.set('status', NotifyData.s); // Completed

        me.set('altCurrencyId', NotifyData.alt_currency_id); // ID валюты, например: 101 - Litecoin

        let orderId = NotifyData.id;
        let status = exchangerStatuses[NotifyData.s] ? exchangerStatuses[NotifyData.s] : NotifyData.s;
        var someHelpShown = false;

        function PrepareForPhoneAuth(NotifyData) {
          console.log('4 - SMS-верификация');
          me.set('phoneId', NotifyData.phoneId);
          me.set('phoneNumber', NotifyData.phoneNumber);
        }

        if (NotifyData.s == "Verifying" || NotifyData.s == "Declined") {

          // https://indacoin.com/notify?confirm_code=2Fcd930B00&request_id=7141439
          if (NotifyData.cardStatus && NotifyData.cardStatus != "" && NotifyData.cardStatus == "Declined") {
            cardStatus = 'cardDeclined';
            if (NotifyData.card3DS && NotifyData.card3DS == "Half3Ds")
              console.log('1 - Ошибка 3Ds'); // Status 3: отклонено
            else if (NotifyData.verifyStatusAddInfo && NotifyData.verifyStatusAddInfo == "bank_rejected") {
              console.log('2 - card rejected'); // Status 1: отклонено
            } else {
              console.log('3 - card declined'); // Status 2: отклонено
            }
          } else 
            if (NotifyData.iT != 10) {
              if (NotifyData.phoneStatusAuthCode == "Verifying") {
                cardStatus = 'phoneVerification';
                PrepareForPhoneAuth(NotifyData); // Status 4: ввод телефона
                someHelpShown = true;
              } else if (NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying") {
                cardStatus = 'videoVerification';
                console.log('5 - NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying"'); // Status 5: видос ( код устарел в прошлой версии)
              }
            } else
            if (NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying") { // Недопустимый статус: валюты такой нет
              console.log('6 - NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying"');

            } else if (NotifyData.phoneStatusAuthCode == "Verifying") {
              console.log('7 - NotifyData.phoneStatusAuthCode == "Verifying"');
              cardStatus = 'videoVerification';
              PrepareForPhoneAuth(NotifyData);
              someHelpShown = true;
            }
            if (NotifyData.s == "Verifying" && !someHelpShown) { // Status 5: только видос если не показывали ранее
              cardStatus = 'videoVerification';
              console.log('8 - NotifyData.s == "Verifying" && !someHelpShown');
            }
        }
        if (!someHelpShown) {
          console.log('9 - !someHelpShown');
        }
        if (NotifyData.direction == 0) {
          console.log('10 - NotifyData.direction == 0');
        }
        if (NotifyData.video_verification_id < 0) {
          console.log('11 - Try new kind of video verification');
          // Записать видео
        }

        // Для полной транзакции используется промежуточный перевод в bitcoin, поэтому смотрим на поле alt_currency_id
        /*if (NotifyData.alt_currency_id < 0 && NotifyData.amount_alt_to_send) {
          me.set('payInAmount', NotifyData.iA); // "0.00043"

        }*/

        if (cardStatus === 'videoVerification') {
          console.log('get cardStatus from share: ' + cardStatus);
          console.log(NotifyData);
          console.log('get KYCUrl from share: ' + NotifyData.KYCUrl);
          me.set('KYCUrl', NotifyData.KYCUrl);
        }

        // Поле Pay amount
        //let payInAmount = NotifyData.iA; // "50"
        me.set('payInAmount', NotifyData.iA);

        //let payInSystem = cashIn[NotifyData.iT]; // "Credit/Debit cards (EUR)""
        me.set('cashIn', NotifyData.iT);
        let payInCurrency = NotifyData.iC; // "USD"

        let payOutAmount = NotifyData.oA; // 0.008679
        me.set('payOutAmount', payOutAmount);

        let altCurrencyId = NotifyData.alt_currency_id; // 202
        me.set('altCurrencyId', altCurrencyId);

        let amountAltToSend = NotifyData.amount_alt_to_send; // 0.0002323
        me.set('amountAltToSend', amountAltToSend);

        let payOutCurrency = NotifyData.oC; // BTC
        me.set('payOutCurrency', payOutCurrency);

        let payOutSystem = NotifyData.oT; // 10 - (Bitcoin)
        //cashOut[NotifyData.oT]; // Bitcoin
        me.set('payOutSystem', payOutSystem);

        if (NotifyData.cashoutExternalTransactionId && NotifyData.cashoutExternalTransactionId != "") {
          // Добавляем поле "Внешняя транзакция"

          me.set('transactionId', NotifyData.cashoutExternalTransactionId);
          console.log('12');
        }

        if ((NotifyData.iT == 51 || NotifyData.iT == 52) && NotifyData.s == "Verifying" && NotifyData.phoneStatusAuthCode != "Verifying") {
          // Платежка QIWI
          console.log('13');
        }

        if ((NotifyData.iC == "BTC" || NotifyData.iC == "LTC" || NotifyData.iC == "LXC" || NotifyData.iC == "ETH") && NotifyData.s == "CashinWaiting") {
          // Поле с QR-кодом
          console.log('14');
        }

        if (NotifyData.s == "Completed" || NotifyData.s == "MoneySend") {
          // Показ социальный кнопок

          if (NotifyData.iT == 10)
            cardStatus = 'moneySendBtc';
          else
            cardStatus = 'moneySend';

          //cardStatus = 'moneySend';
          console.log('15');
        }

        if (NotifyData.s == "WaitingForAccountCreation") {
          // Ссылка на страницу регистрации
          cardStatus = 'waitingForAccountCreation';
          console.log('16');
        }


        // Если не пришли текстовые статусы, то анализируем числа
        switch (NotifyData.s)
        {
        case (-9):
          cardStatus = "WaitingForAccountCreation";
          me.set('formStatus', cardStatus);
          //return;
        break;
        case (-1):
        case (-2):
          cardStatus = "TimeOut";
          me.set('formStatus', cardStatus);
          //return;
        break;
        case (-3):
        case (-4):
        case (100):
          cardStatus = "Processing";//"Error";
          me.set('formStatus', cardStatus);
          //return;
        break;
        case (0):
          cardStatus = "CashinWaiting";
          me.set('formStatus', cardStatus);
          //return;
        break;
        case (1):
        case (2):
        case (3):
        case (4):
        case (5):
          cardStatus = "Processing";
          me.set('formStatus', cardStatus);
          //return;
        break;
          case (6):
          cardStatus = "MoneySend";
          me.set('formStatus', cardStatus);
          //return;
        break;
        case (7):
          cardStatus = "Completed";
          me.set('formStatus', cardStatus);
          //return;
        break;
        }

        let statuses = {
          '100': "CashOutPaySystemServerError",
          '-9': "WaitingForAccountCreation",
          '-8': "CashOutPartnerError",
          '-7:': "MarketExchangeSuspendedDueBlackListing",
          '-6': "BTC2CCPriceChanged",
          '-5': "RejectedManual",
          '-4': "CashOutFailed",
          '-3': "MarketExchangeFailed",
          '-2': "Rejected",
          '-1': "CanceledTimeout",
          '0': "WaitingForCashin",
          '1': "CashedIn",
          '2': "ExchangingFunds",
          '3': "WaitingForAcception",
          '4': "ExchangingAccepted",
          '5': "CashOutInProcess",
          '0': "FundsSent",
          '7': "FundsReceived",

          // Не будут приходить - не обрабатывать
          '10': "ReadyToTradeAlt",
          '11': "AltOrderPlaced",
          '12': "AltTradePartialComplete",
          '13': "AltTradeComplete",
          '14': "AltCashOutInProcess",
          '15': "AltCashOutPlaced",
          '16': "AltFundsSent"
        }



        console.log('NotifyData.s = ' + NotifyData.s);
        console.log('cardStatus = ' + cardStatus);
        me.set('formStatus', cardStatus);

          //console.log(NotifyData);
      },
      error: function() {
          console.log("RefreshGlobalData error");
      }
    });
  }


//}




});
