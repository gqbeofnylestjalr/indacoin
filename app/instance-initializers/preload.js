import $ from "jquery";
import ENV from 'fastboot/config/environment';
import Ember from 'ember';


//import fetch from 'fetch';
//import syncRequest from 'npm:sync-request';
//import syncRequest from 'fastboot/node_modules/sync-request';



//import MyCoolModule from "npm:my-cool-module";

export function initialize( container ) {

  // appInstance.inject('route', 'foo', 'service:foo');

  //appInstance.lookup('store:main');
}

export default {
  fastboot: Ember.inject.service(),
  //ajax: Ember.inject.service(),
  
  //share: Ember.inject.service('share'),
  name: 'preload',
  initialize: function({ container }) {

    /*if (typeof FastBoot === 'undefined') {
      Ember.set(ENV, 'isfastboot', false);
    } else {
      Ember.set(ENV, 'isfastboot', true);
    }*/
    Ember.set(ENV, 'isfastboot', false);
    
    /*if (typeof FastBoot !== 'undefined') {
      console.log('На сервере');      
      console.log('Shoebox start');
      const store      = container.lookup('service:store');
      const shoebox    = container.lookup('service:fastboot').get('shoebox');
      const modelNames = container.lookup('data-adapter:main').getModelTypes().mapBy('name');
    
      console.log('models:');
      console.log(modelNames);
  
      shoebox.put('ember-data-store', {
        get types () {
          return modelNames.reduce((hash, modelName) => {
            try {
              hash[ modelName ] =
                  store
                    .peekAll(modelName)
                    .toArray()
                    .map(record => record.serialize({includeId : true}))
            } catch (e) {
              console.error(`ember-data-fastboot: serializer crashed when trying to serialize records of "${modelName}"`, e)
            }
    
            return hash
          }, {})
        },
      })
      console.log('Shoebox end');
    } else {
      console.log('На клиенте');      
    }*/




    //if (typeof FastBoot === 'undefined') {

      var forceTitle = false;
      var setLang = "";
      let url = "";
      let search = "";
      if (typeof FastBoot === 'undefined') {
        url = window.location.pathname; // "/en_RU/change"
        search = window.location.search;
      } else {
        url    = container.lookup('service:fastboot').get('request.path'); // /en_RU/change?confirm_code=2dCbc916B1&request_id=7149242
        search = url;
      }





      let i18n = container.lookup('service:i18n');
      var share = container.lookup('service:share');
      //share.setPathName();



      /*let syncRequest = container.lookup('service:sync-request');
      var query = syncRequest('GET', 'https://indacoin.com/change.aspx/getCashInfo');
      if (String(query.statusCode) === '200') {
        var r = query.getBody();
        console.log(r);
      }*/

      // fetch("/change.aspx/getCashInfo",
      //   {
      //     method: 'POST',
      //     url: "https://indacoin.com/change.aspx/getCashInfo",
      //     headers: {
      //       "content-type": "application/json; charset=utf-8",
      //       //"accept": "application/json, text/javascript, */*; q=0.01",
      //       "x-requested-with": "XMLHttpRequest"
      //     }
      //   }).then(function(response) {
      //     console.log('getCashInfo:');
      //     console.log(response.json());
      //   })
      
      // Check available currency for show attention banner

      // var showAlert = true;
      // if (typeof FastBoot === 'undefined') {
      //   $.ajax({
      //     url: "/change.aspx/getCashInfo",
      //     type: "post",
      //     contentType: "application/json; charset=utf-8",
      //     dataType: "json",
      //     async: false, // async must be a false
      //     success: function (result) {
      //       result = $.parseJSON(result.d);
      //       for(var item in result.changeDirection) {
      //         showAlert = false;
      //       }
      //     }
      //   });
      // } else {

      //   console.log('Make request on fastboot');

      // }

      // showAlert = false;
      // share.setShowAlert(showAlert);
      




      /*if (showAlert) {
        let applicationController = Ember.getOwner(this).lookup('controller:application');
        let changeController = Ember.getOwner(this).lookup('controller:lang/change');
        applicationController.set('stopSidebar', true);
        changeController.set('stopSidebar', true);

      }*/

          //this.controllerFor('application').set('stopSidebar', this.get('stopSidebar'))
    //this.controllerFor('lang.change').set('stopSidebar', this.get('stopSidebar'))


    /*if (ENV.environment === 'development') {
      console.log('mode: development');
    } else if (ENV.environment === 'production') {
      console.log('mode: production');
    }*/



      // ======================= GetData() =======================
      // Находим confirm_code и request_id
      //var regex = /^\?confirm_code=([a-z0-9]+)\&request_id=(\d+)$/i;
      var regex = /confirm_code=([a-z0-9]+)\&request_id=(\d+)$/i;
      var match = regex.exec(search); // "?confirm_code=2dCbc916B1&request_id=7149242"
      if (match === null) {
        //console.log('confirm_code и request_id не найдено');
      } else
      if (match.length === 3) {
        //console.log('confirm_code и request_id найдено');
        var confirmCode = match[1].toLowerCase(); // A1D7b2cCe0
        var requestId = match[2].toLowerCase(); // 7141524

        console.log('FASTBOOT');
        console.log('confirmCode = ' + confirmCode);
        console.log('requestId = ' + requestId);
        
       
        if (confirmCode !== null && requestId !== null && typeof FastBoot !== 'undefined') {
          console.log('confirmCode и requestId пришли: ' + confirmCode + ' ' + requestId);
          Ember.set(ENV, 'isfastboot', true);
        } else {
          console.log('confirmCode и requestId не пришли');
          Ember.set(ENV, 'isfastboot', false);
        }

        // Делаем запрос getData() и анализируем данные   
        //console.log('Notify from preload');
        if (typeof FastBoot === 'undefined') {
          share.notify(requestId, confirmCode);
        }
      }

      //console.log('url = ' + url);
      var regex = /^\/([a-z][a-z])_([a-z][a-z])/i; // Только первая часть ru_RU, а остальное игнорируется
      var autoDetect = false;

      var match = regex.exec(url); // "/en_RU/change"

      if (match === null) {
        autoDetect = true;
      } else
      if (match.length === 3) {
        var language = match[1].toLowerCase();
        var region = match[2].toLowerCase();
  
        ////////////////////////////////////////////////////////
        var countryName = i18n.t('country.' + region.toUpperCase() + '.name');

        //console.log('countryName: ' + countryName);
        //i18n.addTranslations(language, {'morecountry' : countryName});
        //console.log('Set country: ' + i18n.t('morecountry'));

        share.setLanguage(language);
        share.setRegion(region);

        //var locales = i18n.get('locales');
        //var locales = ["dz", "en", "ru", "tr"];
        let locales = [ "de", "ar", "en", "es", "fr", "it", "pt", "ru", "tr" ];
        autoDetect = true;
        for(var i=0; i < locales.length; i++) {
          if(locales[i] == language) {
            autoDetect = false;
            //i18n.set('locale', language);
            //console.log('Определена локаль: ' + language);
            break;
          }
        }
      } else {
        autoDetect = true;
      }

      if (autoDetect) {
        var calc = calculateLocale(i18n.get('locales')); // en_US
        //console.log('calc = ' + calc);
        let lang = calc.split('-')[0].toLowerCase(); // en
        //i18n.set('locale', lang);
      }
        
      // TODO: Проверить наличие US

      // Если пришел запрос вида buy-bitcoins-with-cardusd, то ставим соответствующий title и <h2> слева
      // Если пришло только change или change/мусор, то ставим стандартный bitcoin и usd

      /*var hasChange = false;
      var regex = /^\/[a-z][a-z]_[a-z][a-z]\/(change)/i; // +++
      var match = regex.exec(url); // "/en_RU/change"    /en_RU/change?confirm_code=2dCbc916B1&request_id=7149242
      if (match === null) {
        //console.log('Оставляем адрес без изменения');
        // Оставляем адрес без изменения
      } else
      if (match.length === 2) {
        // Есть /change
        hasChange = true;
      }

      if (hasChange) {

        // Находим название крипты и валюту для title и <h2>
        var regex = /^\/[a-z][a-z]_[a-z][a-z]\/change\/buy-(.+)-with-card([a-z][a-z][a-z])/i;
        var match = regex.exec(url);
  
        if (match === null) {
          // Редиректим на /en_US/change/buy-bitcoin-with-cardusd
          //window.location.replace(language + '_' + region + "/change/buy-bitcoin-with-cardusd");
          //console.log('Редиректим на ' + language + '_' + region + "/change/buy-bitcoin-with-cardusd");
        } else
        if (match.length === 3) {
          var outCur = match[1].toLowerCase(); // bitcoin
          var inCur = match[2].toLowerCase(); // usd
          //console.log('Ставим ' + outCur + ' и ' + inCur); // bitcoin и usd
          //console.log('Test: ' + i18n.t('country.' + region.toUpperCase() + '.name'));

          
          // Учитываем падежи в русском языке при составлении <title>
          var translateCountry = "";
          if (language === 'ru')
            translateCountry = i18n.t('country.' + region.toUpperCase() + '.inName'); // Российской Федерации
          else
            translateCountry = i18n.t('country.' + region.toUpperCase() + '.name'); // Russian Federation

          let instantly = i18n.t('meta.instantly');

          
          var title = i18n.t('meta.buy') + ' ' + outCur + ' ' + i18n.t('meta.card') + ' ' + i18n.t('meta.in') + ' ' + translateCountry + ' ' + i18n.t('meta.instantly') + ' - Indacoin';


          // Запрашиваем список доступных валют. Если валюта не найдена, то ставим стандартную Bitcoin
          var leftPaymentHeader = "Bitcoin";
          
          share.getCurrency().forEach(function(item){
            if (item.name.toLowerCase() === outCur) {
              leftPaymentHeader = outCur;
              console.log('Найдена валютка: ' + leftPaymentHeader);
            }
          })
          //i18n.addTranslations(language, {'meta.title' : title});
          
          forceTitle = title;
          //document.title = title;

        }
      }
      if (forceTitle)
        ;//document.title = forceTitle;
      else
        ;//document.title = i18n.t('meta.title');
*/



      // var regex = /confirm_code=([a-z0-9]+)\&request_id=(\d+)/i;
    //}


  }
};

function calculateLocale(locales) {
  var language = null;
  if (typeof FastBoot === 'undefined') {
    //if (typeof navigator.languages != 'undefined')
    var language = navigator.language || navigator.userLanguage || navigator.languages[0];
    return  locales.includes(language.toLowerCase()) ? language : 'en-GB';
  } else {
    return 'en-GB';
  }
}