import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    headTags: function() {
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.api')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.api')
                }
            }
        ]
    },
    hasLayout: false,
    activate: function() {
        this._super();
        if (typeof FastBoot === 'undefined') {
            window.scrollTo(0,0);
        }
    }
});