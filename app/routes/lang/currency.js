import Ember from 'ember';
import fetch from 'fetch';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  hasLayout: false,
  fastboot: Ember.inject.service(),
  i18n: Ember.inject.service(),
  activate: function() {
      this._super();
      if (typeof FastBoot === 'undefined') {
          window.scrollTo(0, 0);
      }
  },

  shortCurrencyName: null,
  fullCurrencyName: null,
  aliasCurrencyName: null,

  noFoundCurrency: false,

  headTags: function() {
    const me = this;
    var link = "";
    var path = "";
    if (typeof FastBoot === 'undefined') {
      if (window.hasOwnProperty('location'))
        if (window.location.hasOwnProperty('href')) {
          link = window.location.href; //   http://indacoin.com/ru_GB/ethereum
        }
        if (window.location.hasOwnProperty('pathname')) {
          path = window.location.pathname;
        }
    }
    else {
      path = this.get('fastboot.request.path'); //   /ru_GB/ethereum
      link = "https://" + this.get('fastboot.request.host') + path;
    }

    let currencyTitle = null;
    let currencyDescription = null;

    if (me.get('noFoundCurrency') === false) {

      var shortCurrencyName = me.get('shortCurrencyName').toUpperCase();
      var fullCurrencyName = me.get('fullCurrencyName');

      // To upper first letter
      fullCurrencyName = fullCurrencyName.split(' ').map((item)=>{
        if (item[0] !== undefined)
          return item[0].toUpperCase(0) + item.slice(1);
        else
          return '';
      }).join(' ');

      currencyTitle = this.get('i18n').t('tags.titles.currency', { fullCurrency: fullCurrencyName, shortCurrency: shortCurrencyName });
      currencyDescription = this.get('i18n').t('tags.descriptions.currency', { fullCurrency: fullCurrencyName });

    } else {

      currencyTitle = this.get('i18n').t('tags.titles.notFoundCurrency');
      currencyDescription = this.get('i18n').t('tags.descriptions.notFoundCurrency');

    }

    return [
      {
        type: 'link',
        tagId: 'head-canonical',
        attrs: {
          rel: 'canonical',
          href: link
        }
      },
      {
        type: 'title',
        tagId: 'head-title',
        content: currencyTitle
      },
      {
        type: 'meta',
        tagId: 'head-description',
        attrs: {
            name: 'description',
            content: currencyDescription
        }
      }
    ]
  },

  model: function(params) {
    const me = this;
    let currency_id = (params.currency_id || 'bitcoin').toLowerCase(); // bitcoin
    // var curs = [
    //   {'short': 'btc', 'full': 'bitcoin', 'alias': 'bitcoin'}, // +
    //   {'short': 'eth', 'full': 'ethereum', 'alias': 'ethereum'}, // +
    //   {'short': 'ltc', 'full': 'litecoin', 'alias': 'litecoin'}, // +
    //   {'short': 'xrp', 'full': 'ripple', 'alias': 'ripple'}, // +
    //   {'short': 'bcc', 'full': 'bitcoin cash', 'alias': 'bitcoin-cash'},
    //   {'short': 'dash', 'full': 'dash', 'alias': 'dash'},
    //   {'short': 'btg', 'full': 'bitcoin gold', 'alias': 'bitcoin-gold'},
    //   {'short': 'neo', 'full': 'neo', 'alias': 'neo'} // +
    // ]

    var shortName = null;
    var alias = null;

    this.get('store').peekAll('value').forEach(function(item){
      if (item.get("name").toLowerCase() === currency_id) {
        shortName = item.get("short_name");
        alias = item.get("name");
      }
    })

    // curs.forEach(function(item){
    //   if (item.full === currency_id) {
    //     shortName = item.short;
    //     alias = item.alias;
    //   }
    // });
    if (shortName === null) {
      let controller = Ember.getOwner(this).lookup('controller:lang/currency');
      controller.set('noFoundCurrency', true); // Крипты нет
      me.set('noFoundCurrency', true); // Крипты нет

      // if (typeof FastBoot !== 'undefined') {
      //   me.set('fastboot.response.statusCode', 404);
      // }
      return false;
    }

    let controller = Ember.getOwner(this).lookup('controller:lang/currency');
    if (shortName.toLowerCase() === 'btc') {
      controller.set('isBitcoin', true);
      controller.set('isEthereum', false);
    }
    else if (shortName.toLowerCase() === 'eth') {
      controller.set('isBitcoin', false);
      controller.set('isEthereum', true);
    } else {
      controller.set('isBitcoin', false);
      controller.set('isEthereum', false);
    }

    me.set('shortCurrencyName', shortName);
    me.set('fullCurrencyName', currency_id);
    me.set('aliasCurrencyName', alias);

    function pullCryptoHistory() {
      return fetch('https://min-api.cryptocompare.com/data/histoday?fsym=' + shortName.toUpperCase() + '&tsym=USD&limit=360&aggregate=1&e=CCCAGG').then((data) => {
        return data.json();
      }).then((data) => {
        return data.Data;
      });
    }

    return RSVP.hash({
      info: me.get('store').findAll('value').then((data) => {
        let res = 'bitcoin';
        let found = false;
        data.forEach((item, i, arr)=>{
          if (item.get('name').toLowerCase() === currency_id) {
            found = true;
          }
        })
        if (found)
          res = alias;
        return res;
      }).then((currency) => {
        return me.get('store').query('pull-crypto', {currency: currency})
        .then((data) => {
          return data.get("firstObject"); // Get one record from query
        })
        .catch((reason) => {
          console.log('Catch model pull-crypto error in route lang/currency');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/currency');
          controller.set('noFoundCurrency', true); // Крипты нет
          me.set('noFoundCurrency', true); // Крипты нет
        })

      }),
      chart: pullCryptoHistory()
    });
  },
  // setupController: function(controller, model) {
  //   this._super(...arguments);
  // },
  beforeModel: function() {
    this._super(...arguments);
    let controller = Ember.getOwner(this).lookup('controller:lang/currency');
    // Refresh controllers properties 'cause controller is singleton
    controller.set('isBitcoin', false);
    controller.set('isEthereum', false);
    controller.set('shortCurrencyName', null);
    controller.set('fullCurrencyName', null);
    controller.set('aliasCurrencyName', null);
    controller.set('noFoundCurrency', false);
  }
});
