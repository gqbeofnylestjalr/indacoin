import Ember from 'ember';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    headTags: function() {
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.help')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.help')
                }
            }
        ]
    },
    hasLayout: false,
    activate: function() {
        this._super();
        if (typeof FastBoot === 'undefined') {
            window.scrollTo(0,0);
        }
    },
    setupController: function(controller, model) {
        
        this._super(controller, model);
        
        /*console.log('Очищаем данные в help');
        controller.set('isPlatform', true);
        controller.set('anchor', 1);*/
    }
});
