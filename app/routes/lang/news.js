import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    fastboot: Ember.inject.service(),
    queryParams: {
        article: {
          replace: true
          //refreshModel: true
        }
    },
    headTags: function() {
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.news')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.news')
                }
            }
        ]
    },
    hasLayout: false,
    activate: function() {
        this._super();
        if (typeof FastBoot === 'undefined') {
            window.scrollTo(0,0);
        }
    },
    beforeModel: function(params) {
        this._super(...arguments);
        this.get('store').unloadAll('news'); // On change language delete all news-store data
    },
    afterModel: function(model, transition) {
        //this._super(...arguments);
        //console.log('afterModel');

        //console.log(transition);
        let article = transition.queryParams.article;
        //console.log(transition.queryParams.article);
        if (article !== undefined) {
            //let article = params.article;
            //console.log('Пришло из параметров: ' + article);
            let controller = Ember.getOwner(this).lookup('controller:lang/news');
            //controller.set('article', article);
            controller.send('expandNews', article);
        }

    },
    model: function(params){
        /*if (params.article !== undefined) {
            let article = params.article;
            console.log('Пришло из параметров: ' + article);
            let controller = Ember.getOwner(this).lookup('controller:lang/news');
            //controller.set('article', article);
            controller.send('expandNews', article);
        }*/

        //var me = this;
        //console.log('Model hook');

        //debugger;

        //let news = this.get('store').findAll('news');
        //news.reload();

        
        
   
        return RSVP.hash({
            news: this.get('store').findAll('news')
            .catch((reason) => {
                console.log('Catch model news error in route lang/news');
                console.log(reason);
                let controller = Ember.getOwner(this).lookup('controller:lang/news');
                controller.set('errorLoadData', true);
            })
        });
    },

    actions: {
        error(error, transition) {
          console.log('Catch action error on route lang/news');
          console.log(error);
  
          let controller = Ember.getOwner(this).lookup('controller:lang/news');
          controller.set('errorLoadData', true);
        }
    }
});
