import Ember from 'ember';
import fetch from 'fetch';
import DS from 'ember-data';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  //errorLoadData: 'HALP',
  //fastboot: Ember.service.inject(),
  i18n: Ember.inject.service(),
  cookies: Ember.inject.service(),

  queryParams: {
    give: {
      replace: true
    },
    take: {
      replace: true
    },
    page: {
      refreshModel: true
    },
    size: {
      refreshModel: true
    }
  },
  beforeModel: function(params) {
    this.get('store').unloadAll('facebook'); // On change language delete all facebook data
  },
    model: function(params) {

      const cookieService = this.get('cookies');

      var page = params.page;
      var size = params.size;

      // Set discount to cookie
      let discountFromCookie = cookieService.read('discount');
      let discount = params.discount || discountFromCookie;
      if (discount != null) {
        let cookieService = this.get('cookies');
        cookieService.write('discount', discount, {path: '/'});
        try {
          if (typeof FastBoot === 'undefined') {
            let time = new Date().getTime();
            let timeFromCookie = localStorage.getItem('saveReferralVisit');
            localStorage.setItem('saveReferralVisit', time);
            let elapsedTime = (time - timeFromCookie) / 1000;
            console.log('elapsedTime = ' + elapsedTime);
            if (elapsedTime > 300) {
            //console.log(localStorage.getItem('saveReferralVisit'));
              let discountData = JSON.stringify({ discount: discount });
              Ember.$.ajax({
                url: "/change.aspx/saveReferralVisit",
                async: true,
                type: "post",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: discountData,
                success: function(result) {
                },
                error: function() {
                }
              })
            }
          }
        } catch(e) {
          console.log('Ошибка отправки saveReferralVisit' + e);
        }
      }

      // Strange push to model of indacoin api for exchange form
      let json = {};
      let result = [];

      function indacoin(cb){
        for(var i = 0; i < 4; i++){
          let sum;
          if(i === 0){
            sum = 15;
          } else if (i === 1) {
            sum = 100;
          } else if (i === 2) {
            sum = 250;
          } else if (i === 3) {
            sum = 500;
          }
          let cur = params.give;
          let url = 'https://indacoin.com/api2/GetExchangerAmount/'+ cur +'/' + sum;
          fetch(url).then((data) => {
            return data.json();
          }).then((data) => {
            json.cur = 'usd';
            json.price = parseFloat(data).toFixed(3);
            json.sum = sum.toFixed(2);
            json.url = 'https://indacoin.com/change/buy-bitcoin-with-card' + cur + '/RU-en?amount_pay=' + sum;
            //json.url = '/en_US/change/buy-bitcoin-with-cardusd';
            result.push(json);
            result.sort(function(a, b) {
              return a.sum - b.sum;
            });
          })
        }
        console.log('result');
        return result;
      }
      let hash = {};
      let uid = params.user;
      if (uid) {
          hash.user = uid;
      }
      return RSVP.hash({
        featured: this.get('store').query('currency', { limit: 4 }).catch((reason) => {
          console.log('Catch currenct error in route lang/index');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/index');
          controller.set('errorLoadData', true);
        }),

        currency: this.get('store').query('currency', { limit: size }).catch((reason) => {
          console.log('Catch blue screen in roue from currency');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/index');
          controller.set('errorLoadData', true);
        }),

        //exchanger_usd: this.get('store').findAll('exchanger-usd'),
        //exchanger_eur: this.get('store').findAll('exchanger-eur')

        //exchanger_usd2: this.get('store').findAll('exchange-usd'),
        exchanger_usd: this.get('store').findAll('exchanger-usd').catch((reason) => {
          console.log('Catch blue screen in roue from exchanger_usd');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/index');
          controller.set('errorLoadData', true);
        }),

        exchanger_eur: this.get('store').findAll('exchanger-eur').catch((reason) => {
          console.log('Catch blue screen in roue from exchanger_eur');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/index');
          controller.set('errorLoadData', true);
        }),

        exchanger_rub: this.get('store').findAll('exchanger-rub').catch((reason) => {
          console.log('Catch blue screen in roue from exchanger_rub');
          console.log(reason);
          let controller = Ember.getOwner(this).lookup('controller:lang/index');
          controller.set('errorLoadData', true);
        }),

        /*facebook: this.get('store').findAll('facebook').catch((reason) => {
          console.log('Catch blue screen in facebook from exchanger_rub');
          console.log(reason);
          //let controller = Ember.getOwner(this).lookup('controller:lang/index');
          //controller.set('errorLoadData', true);
        }),*/

        facebook: this.get('store').query('facebook', {lang: this.get('i18n.locale')}).catch((reason) => {
          console.log('Catch blue screen in facebook from lang/index');
          console.log(reason);
          //let controller = Ember.getOwner(this).lookup('controller:lang/index');
          //controller.set('errorLoadData', true);
        }),

        
        cashinfo: this.get('store').query('cashinfo', hash).catch((reason) => {
          console.log('Catch store error cashinfo from rpote lang/change');
          console.log(reason);
        })


      });
    },

    // setupController: function(controller, model) {
    //   this._super(controller, model);
    //   if (typeof FastBoot === 'undefined') {
    //     window.scrollTo(0,0);
    //   }
    // },

    actions: {
      error(error, transition) {
        console.log('DEBUG 1234567');
        console.log(error);

        let controller = Ember.getOwner(this).lookup('controller:lang/index');
        controller.set('errorLoadData', true);

        //this.set('errorLoadData', false);
        /*if (error) {
          return this.transitionTo('error-page');
        }*/
      }
    }

});
