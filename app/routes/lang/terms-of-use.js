import Ember from 'ember';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    //queryParams: ['privacy-policy'],

    queryParams: {
        anchor: {
          replace: false
        }
    },

    headTags: function() {
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.terms-of-use')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.terms-of-use')
                }
            }
        ]
    },
    hasLayout: false,
    anchor: undefined,
    activate: function() {
        this._super();
        const anchor = this.get('anchor');
        if (typeof FastBoot === 'undefined') {


            console.log('activate');
            //console.log('window.location.href = ' + window.location.href);
            //let anchor = window.location.href.search(/anchor=privacypolicy$/gi) === -1 ? false : true;
            
            if (anchor === 'privacypolicy') {
                console.log('получен anchor: ' + anchor);

                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: $("#privacypolicy").offset().top
                    }, 0);
                }, 1000);


            } else if (anchor === 'cookiepolicy') {
                console.log('получен anchor: ' + anchor);

                setTimeout(function() {
                    $('html, body').animate({
                        scrollTop: $("#cookiepolicy").offset().top
                    }, 0);
                }, 1000);

                
            } else {
                setTimeout(function() {
                    window.scrollTo(0,0);
                }, 1000);
            }

            //let anchor = window.location.pathname
            //console.log(window.location.hash);

            //console.log('terms-of-use activate' + this.get('anchor'));
            //window.scrollTo(2000,0);
            /*(function($) {
                $.fn.goTo = function() {
                    $('html, body').animate({
                        scrollTop: $(this).offset().top + 'px'
                    }, 'fast');
                    return this; // for chaining...
                }
            })(jQuery);*/
            

        }
    },
    model: function(params){
        //var t = params;
        //console.log('params = ' + params);
        //console.log(params);
        var anchor = params.anchor;
        //console.log('t = ' + t);
        //console.log(params);
        this.set('anchor', anchor);
        //if (t = 'privacypolicy')

    }
});
