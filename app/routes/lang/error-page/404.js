import Ember from 'ember';

export default Ember.Route.extend({
  hasLayout: false,
  fastboot: Ember.inject.service(),
  i18n: Ember.inject.service(),
  setupController: function(controller, model) {
      this._super(controller, model);
      const me = this;
      if (typeof FastBoot !== 'undefined') {
          me.set('fastboot.response.statusCode', 404);
      }
  }
});
