import Ember from 'ember';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    cookies: Ember.inject.service(),
    headTags: function() {
        let controller = this.controllerFor(this.routeName);
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.affiliate')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.affiliate')
                }
            }
        ]
    },
    hasLayout: false,
    activate: function() {
        this._super();
        if (typeof FastBoot === 'undefined') {
            window.scrollTo(0, 0);
        }
    },
    model: function(params) {
        
        const cookieService = this.get('cookies');

        // Set discount to cookie
        let discountFromCookie = cookieService.read('discount');
        let discount = params.discount || discountFromCookie;
        if (discount != null) {
            console.log('Пишем cookie ' + discount);
            let cookieService = this.get('cookies');
            cookieService.write('discount', discount, {path: '/'});
            try {
                if (typeof FastBoot === 'undefined') {
                let time = new Date().getTime();
                let timeFromCookie = localStorage.getItem('saveReferralVisit');
                localStorage.setItem('saveReferralVisit', time);
                let elapsedTime = (time - timeFromCookie) / 1000;
                console.log('elapsedTime = ' + elapsedTime);
                if (elapsedTime > 300) {
                //console.log(localStorage.getItem('saveReferralVisit'));
                    let discountData = JSON.stringify({ discount: discount });
                    Ember.$.ajax({
                    url: "/change.aspx/saveReferralVisit",
                    async: true,
                    type: "post",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: discountData,
                    success: function(result) {
                    },
                    error: function() {
                    }
                    })
                }
                }
            } catch(e) {
                console.log('Ошибка отправки saveReferralVisit' + e);
            }
        }

    }
});
