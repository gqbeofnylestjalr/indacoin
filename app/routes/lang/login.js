import Ember from 'ember';

export default Ember.Route.extend({
    i18n: Ember.inject.service(),
    headTags: function() {
        return [{
                type: 'title',
                tagId: 'head-title',
                content: this.get('i18n').t('tags.titles.login')
            },
            {
                type: 'meta',
                tagId: 'head-description',
                attrs: {
                    name: 'description',
                    content: this.get('i18n').t('tags.descriptions.login')
                }
            }
        ]
    },
    hasLayout: false
});
