import Ember from 'ember';
import fetch from 'fetch';
import DS from 'ember-data';
import RSVP from 'rsvp';
import $ from "jquery";
import ENV from 'fastboot/config/environment';
import googleAnalytics from 'google-analytics';
//import ResetScrollPositionMixin from 'app/mixins/reset-scroll-position';

const { inject, computed } = Ember;

export default Ember.Route.extend({
  fastboot: Ember.inject.service(),
  cookies: Ember.inject.service(),
  
  headTags: function() {

    //var link = "";
    var host = ""; // indacoin.com
    var canonical = ""; // https://indacoin.com/change
    if (typeof FastBoot === 'undefined') {
      if (window.hasOwnProperty('location'))
        if (window.location.hasOwnProperty('host')) {
          host = window.location.host;
        }
    }
    else {

      // let referrer = this.get('fastboot.request.headers.referer');
      // console.log('referrer:');
      // console.log(referrer);
      // if (referrer !== null) {
      //   let cookieService = this.get('cookies');
      //   cookieService.write('referrer2', referrer, { path: '/' });
      // }
      host = this.get('fastboot.request.host');
    }

    canonical = "https://" + host + "/change";

    return [{
      type: 'title',
      tagId: 'head-title',
      //content: controller.get('pageTitle')
      content: 'Buy Bitcoin With a Credit Or Debit Card in United Kingdom - Indacoin'
    },
    {
      type: 'meta',
      tagId: 'head-description',
      attrs: {
        name: 'description',
        //content: controller.get('pageDescription')
        content: 'Buy and exchange any cryptocurrency instantly: Bitcoin, Ethereum, Litecoin, Ripple and 700 other digital currencies for EUR or USD'
      }
    },
    {
      type: 'link',
      tagId: 'head-canonical',
      attrs: {
        rel: 'canonical',
        //href: link
        href: canonical
      }
    },
    {
      type: 'meta',
      tagId: 'head-robots',
      attrs: {
        name: 'robots',
        content: 'index, follow'
    }
    }]
  },

  activate: function() {
    this._super();
    if (typeof FastBoot === 'undefined') {
      window.scrollTo(0,0);
    }
  },
  queryParams: {
    amount_pay: {
      replace: false
    },
    amount_out: {
      replace: false
    },
    wallet: {
      replace: false
    },
    addrOut: {
      replace: false
    },

    cur_from: {
      replace: false
    },
    cur_to: {
      replace: false
    },
    email: {
      replace: false
    },
    force: {
      replace: false
    },
    partner: {
      replace: false
    },
    extra_info: {
      replace: false
    },
    transaction_id: {
      replace: false
    }
  },

  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  cookies: Ember.inject.service(),
  hasLayout: false,
  title: "currency",
  redirectFromBank: false,
  outCur: "",
  inCur: "",
  country: "",
  langCode: "",
  region: "",
  path: "",

  model: function(params){
        var me = this;
        this.title = params.change_id;
        var cookieService = this.get('cookies');

        // SQL-injection protection
        function checkAmountNum(value) {
          if (!(!isNaN(parseFloat(value)) && isFinite(value)))
            return false;
          if (!(value > 0 && value < 10000000000000))
            return false;
          let re = /^[0123456789/.]{1,26}$/;
          let OK = re.exec(value);
          if (!OK)
            return false;
          return true;
        }

        // SQL-injection protection
        function checkAmountPay(value) {
          if (!(!isNaN(parseFloat(value)) && isFinite(value)))
            return false;
          if (!(value > 0 && value < 1000000000))
            return false;
          let re = /^[0123456789/.]{1,26}$/;
          let OK = re.exec(value);  
          if (!OK)  
            return false;
          return true;
        }
    
        // SQL-injection protection
        function checkEmail(value) {
          let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
          let OK = re.exec(value);
          if (!OK)
            return false;
          return true;
        }
    
        // SQL-injection protection
        function checkCryptoWallet(value) {
          let re = /^[a-zA-Z0-9]{1,50}$/;
          let OK = re.exec(value);
          if (!OK)
            return false;
          return true;
        }
    
        // SQL-injection protection
        function checkDiscountCode(value) {
    
        }
    
        // Set discount to cookie
        let discountFromCookie = cookieService.read('discount');
        let discount = params.discount || discountFromCookie;
        
        if (discount != null) {
          let cookieService = this.get('cookies');
    
          //let discountFromCookie = cookieService.read('discount');
          cookieService.write('discount', discount, {path: '/'});
          
          try {
            if (typeof FastBoot === 'undefined') {
              let time = new Date().getTime();
              let timeFromCookie = localStorage.getItem('saveReferralVisit');
              localStorage.setItem('saveReferralVisit', time);
              let elapsedTime = (time - timeFromCookie) / 1000;
              console.log('elapsedTime = ' + elapsedTime);
              if (elapsedTime > 300) {
              //console.log(localStorage.getItem('saveReferralVisit'));
                let discountData = JSON.stringify({ discount: discount });
                Ember.$.ajax({
                  url: "/change.aspx/saveReferralVisit",
                  async: true,
                  type: "post",
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  data: discountData,
                  success: function(result) {
                  },
                  error: function() {
                  }
                })
              }
            }
          } catch(e) {
            console.log('Ошибка отправки saveReferralVisit' + e);
          }
        }

        if (typeof FastBoot === 'undefined') {
          let partner = params.partner;
          if (partner) {
            me.get("share").setPartner(partner);
          }
        }

        // TODO: prepare params.transaction_id on number and range
        if (params.transaction_id !== undefined) {
          let transactionId = params.transaction_id;
          this.get("share").setTransactionId(transactionId);
        }

        // TODO: prepare params.extra_info on number and range
        if (params.extra_info !== undefined) {
          let extra = params.extra_info;
          this.get("share").setExtraInfo(extra);
        }
    
        // TODO: prepare params.email on number and range
        if (params.email !== undefined) {
          let email = params.email;
          if (me.get("share").getPartner()) {
            me.get("share").setEmail(email);
          }
          else {
            if (checkEmail(email))
              me.get("share").setEmail(email);
          }
        }
    
        // TODO: prepare params.email on number and range
        if (params.force !== undefined) {
          let force = params.force;
          let coinText = this.get('share').getCoinText().toUpperCase(); // RIPPLE
          let coinId = this.get('share').getCoinId(); // XRP
          if (force.toLowerCase() == 'true')
            this.get("share").setForce('true');
        }

        // TODO: prepare params.tag on number and range
        var tag = null;
        if (params.tag !== undefined) {
          if (checkTag(params.tag))
            tag = params.tag;
        }
    
        // TODO: prepare params.amount_out on number and range
        if (params.amount_out !== undefined) {
          let amountNum = params.amount_out;
          if (checkAmountNum(amountNum))
            this.get("share").setAmountNum(amountNum);
        }
    
        // TODO: prepare params.amount_pay on number and range
        if (params.amount_pay !== undefined) {
          let amountPay = params.amount_pay;
          if (checkAmountPay(Number(amountPay)))
            this.get("share").setAmountPay(amountPay);
        }
    
        if (params.wallet !== undefined) {
          let wallet = params.wallet;
          if (checkCryptoWallet(wallet)) {
            this.get("share").setWallet(wallet);
            //this.get("share").setBlockWallet();
          }
        }
        else if (params.addrOut !== undefined) {
          let addrOut = params.addrOut;
          if (checkCryptoWallet(addrOut)) {
            this.get("share").setWallet(addrOut);
            //this.get("share").setBlockWallet();
          }
        }
    
        if (params.confirm_code && params.request_id) {
          me.get('share').setRedirectFromBank(true);
          me.get('share').setConfirmCode(params.confirm_code);
          me.get('share').setRequestId(params.request_id);
    
          // Загружаем данные на форме при редиректе со страницы банка
          let receivingData = me.get('share').loadData();
        }
    
        // http://localhost:4200/ru_RU/change?cur_from=CARDEUR&cur_to=BTC&partner=bitcoinist&discount=32256
        // ========= Определение языковых параметров ru_RU ===========
        var regex = /^buy-(.+)-with-card(.+)$/i;
        var match = regex.exec(params.change_id);
        try {
          if (match === null) {
            if (params.cur_from !== undefined) { // 'CARDEUR'
              let inCur = 'usd';
              if (params.cur_from.substr(4, 6).toLowerCase() === 'eur')
                inCur = 'eur';
              else if (params.cur_from.substr(4, 6).toLowerCase() === 'rub')
                inCur = 'rub';
              else if (params.cur_from.substr(4, 6).toLowerCase() === 'btc')
                inCur = 'btc';
              else if (params.cur_from.substr(4, 6).toLowerCase() === 'aud')
                inCur = 'aud';
              else if (params.cur_from.substr(4, 6).toLowerCase() === 'gbp')
                inCur = 'gbp';
    
              //let inCur = params.cur_from.toLowerCase(); // usd
              this.inCur = inCur;
              me.get('share').setCurrencyText(inCur);
            }
            if (params.cur_to !== undefined) {
              let cur_to = params.cur_to.toLowerCase();
              var r = this.get('store').peekAll('value');
              r.forEach(function(item){
                if (cur_to === item.data.short_name.toLowerCase()) {
                  cur_to = item.data.name.toLowerCase();
                }
              });
              let outCur = cur_to; // ehtereum
              this.outCur = outCur;
              me.get('share').setCoinText(outCur); // bitcoin
            }
          } else
          if (match.length === 3) {
            let outCur = match[1].toLowerCase(); // bitcoin
            let inCur = match[2].toLowerCase(); // usd
            this.outCur = outCur;
            this.inCur = inCur;
            me.get('share').setCurrencyText(inCur); // usd
            me.get('share').setCoinText(outCur); // bitcoin
          }
        }
        catch (e) {
          console.log(e.message);
          let ans = 'Пришло не то';
          console.log(ans);
          this.outCur = "bitcoin";
          this.inCur = "usd";
          me.get('share').setCurrencyText('usd');
          me.get('share').setCoinText('bitcoin');
        }
    
        let hash = {};
        let uid = params.user;
        if (uid) {
            hash.user = uid;
        }
        return RSVP.hash({
          news: this.get('store').query('topic', {lang: this.get('i18n.locale')})
          .catch((reason) => {
              console.log('Catch model topic error in route lang/change');
              console.log(reason);
              let controller = Ember.getOwner(this).lookup('controller:lang/change');
              controller.set('errorNews', true);
          }),
          //value: this.get("infinity").model('value'),
          value: this.get('store').findAll('value').catch((reason) => {
            console.log('Catch error on load mobgetcurrencies in route lang/change');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/change');
            controller.set('errorMobGetCurrencies', true);
            
          }),
          lang: { lang: "en_GB" },
          //lang: this.modelFor("lang"),
          facebook: this.get('store').query('facebook', {lang: this.get('i18n.locale')}).catch((reason) => {
            console.log('Catch store error facebook from rpote lang/change');
            console.log(reason);
          }),
          cashinfo: this.get('store').query('cashinfo', hash).catch((reason) => {
            console.log('Catch store error cashinfo from rpote lang/change');
            console.log(reason);
          })
        });
    
    
    
      },
    
      afterModel(params, controller) {
        this._super(...arguments);
    
        const me = this;
        // TODO: проверка наличия и редирект, а то могут передать null или uu_UU
        let lang = "en_DE";
        // if (typeof FastBoot === 'undefined') {
        //   var cookieService = this.get('cookies');
        //   if (cookieService.read('lang') !== undefined)
        //     lang = cookieService.read('lang');
        //   else
        //     if (params.lang)
        //       lang = params.lang.lang; // ru_RU
        //     else
        //       lang = 'en_GB';
        // }

          var cookieService = this.get('cookies');
          if (cookieService.read('lang') !== undefined)
            lang = cookieService.read('lang');
          else
            lang = params.lang.lang; // ru_RU
        
    
        let langCode = lang.split('_')[0]; // "ru"
        this.langCode = langCode;
        let region = lang.split('_')[1]; // "RU"


        //const i18n = this.get('i18n');
        this.set('i18n.locale', langCode); // "ru"
        Ember.set(ENV.i18n, 'lang', lang); // "ru_RU"
    
        this.region = region;
        var outCur = this.outCur;
        var flag = false;
        var currencyText = "";
        var r = this.get('store').peekAll('value');
        r.forEach(function(item){
          if (outCur === item.data.name.toLowerCase()) {
            flag = item.data.name;
            currencyText = item.data.short_name;
          }
        });
        if (flag) {
          Ember.title = flag; // "Bitcoin"
          Ember.leftCur = flag; // "Bitcoin"
          Ember.currencyText = currencyText; // "BTC"
          if (this.inCur === 'usd' || this.inCur === 'eur' || this.inCur === 'rub' || this.inCur === 'btc') {
            //console.log('Вариант: определены Bitcoins и USD');
            //this.get('share').setLink('/' + lang + '/change/buy-' + this.outCur + '-with-card' + this.inCur);
            this.get('share').setAmountOut(this.inCur); // "usd"
            this.get('share').setLang(lang); // "ru_RU"
            this.get('share').setOutCur(flag); // "Bitcoin"
            this.get('share').setInCur(this.inCur); // "usd"

            // Set field "Buy cryptocurrency..."
            let controller = Ember.getOwner(this).lookup('controller:lang/change');
            controller.set('buyingLeftTitleText', this.outCur); // "bitcoin"
            //const i18n = this.get('i18n');
            //let buyCryptoTitle = i18n.t('meta.buy');
            //buyCryptoTitle = buyCryptoTitle.toString().replace('[cryptocurrency]', flag);
            //i18n.addTranslations(langCode, {'buyCryptoTitle': buyCryptoTitle});
    
          } else {
            //console.log('Вариант: определены только Bitcoins');
            this.transitionTo('/' + lang + '/change/buy-' + this.outCur + '-with-cardusd');
          }
        }
        else { // Set default parameters needs to form
          if (this.inCur === 'usd' || this.inCur === 'eur' || this.inCur === 'rub' || this.inCur === 'btc') {
            //console.log('Вариант: определены только USD');
            this.transitionTo('/' + lang + '/change/buy-bitcoin-with-card' + this.inCur);
          }
          else {
    
            Ember.title = "Bitcoin";
            Ember.leftCur = "Bitcoin"; // "Bitcoin"
            Ember.currencyText = "BTC"; // "BTC"
    
            this.get('share').setAmountOut("usd"); // "usd"
            this.get('share').setLang(lang); // "ru_RU"
            this.get('share').setOutCur("Bitcoin"); // "Bitcoin"
            this.get('share').setInCur("usd"); // "usd"
    
            //console.log('Вариант: ничего не определено');
            let url = me.get('share').getPathName();
            //http://localhost:4200/ru_RU/change?confirm_code=152AcEbEDb&request_id=7148805
            let confirmCode = me.get('share').getConfirmCode();
            let requestId = me.get('share').getRequestId();
    
            //console.log('Определяем язык после необнаружения');
            let locales = this.get('i18n.locales');
            let country;
          }
        }
      },
      setupController: function(controller, model) {
        this._super(controller, model);
        const share = this.get('share');
    
    
        if (typeof FastBoot === 'undefined') {
          window.scrollTo(0,0);
        }
        
        // Refresh controllers properties 'cause controller is singleton
        controller.set('isRipple', false);
        
        controller.set('confirm_code', null);
        controller.set('request_id', null);
        controller.set('countryName', null);
        controller.set('buyingLeftTitleText', null);
        controller.set('coinName', null);
      
        controller.set('give', 'usd');
        controller.set('take', 'btc');
        controller.set('couponeCheckBox', false);
        controller.set('createWalletCheckBox', false);
        controller.set('coinsName', 'Bitcoins');
        controller.set('coinsCountry', '');
        controller.set('leftCur', '');
        controller.set('step', 1);
        controller.set('step1', true);
        controller.set('hide', false);
        controller.set('videoVerificationPassed', false);
        controller.set('formData', []);
    
        controller.set('skipTwoStep', false);
        
        controller.set('step2', false);
        controller.set('step3', false);
        controller.set('step4', false);
        controller.set('step5', false);
        controller.set('step6', false);
        controller.set('step7', false);
        controller.set('step8', false);
        controller.set('step9', false);
        controller.set('step10', false);
        controller.set('step11', false);
        controller.set('step12', false);
        controller.set('step13', false);
        controller.set('step14', false);
        controller.set('step15', false);
        controller.set('step16', false);
        controller.set('step17', false);
        controller.set('step18', false);
        controller.set('returned', false);
        controller.set('internalAccount', false);
        controller.set('isVideoRecord', false);
        controller.set('copiedToClipboard', false);
        
    
        //share.setAmountPay(null);
    
        if (typeof FastBoot === 'undefined') {
          $('#payment-steps div.step').addClass('disabled');
          $('#payment-steps div.active').removeClass('active');
          $('#payment-steps div.step:nth-child(1)').addClass('active').removeClass('disabled');
          $('#nextStepBtn').show();
          $('#prevStepBtn').hide();
    
          //$('#exchange-give').val('');
          //$('#exchange-take').val('');
        }
    
        //if (typeof FastBoot === 'undefined') {
          let me = this;
          var i18n = this.get('i18n');
          let locales = this.get('i18n.locales');
          var title = "";
    
          // Добавление in\в
          if (controller.get('country') !== "") {
            controller.set('coinsCountry', 'in ' + controller.get('country'));
            Ember.country = ' in ' + controller.get('country');
          }
          
          let instantly = i18n.t('meta.instantly');
          let newTitle = i18n.t('meta.title');
    
          let langCode = this.langCode;
    
          //controller.set('coinsName', this.title);
    
          let requestId = me.get('share').getRequestId();
          let confirmCode = me.get('share').getConfirmCode();
    
          if (me.get('share').getRedirectFromBank()) {
            
            var formStatus = me.get('share').getFormStatus();
            var isVideoRecord = me.get('share').getIsVideoRecord();
            controller.set('isVideoRecord', isVideoRecord);
    
            try {
              ga('send', 'event', { eventCategory: 'Screen', eventAction: formStatus, eventLabel: 'AfterPayed', eventValue: 1 });
            } catch (e) {
              console.log('Error: ' + e);
            }
    
    
    
    
    
            //var formStatus = 'orderTimeOut';
            //var formStatus = 'cashinWaiting';
            //var formStatus = 'completed';
            //var formStatus = 'phoneVerification';
            //var formStatus = 'videoVerification';
            //var formStatus = 'cardVerification';
            //var formStatus = 'cardDeclined';
            //var formStatus = 'processing';
            //var formStatus = 'moneySend';
            //var formStatus = 'waitingForAccountCreation';
            //var formStatus = 'Non3DS';
            //var formStatus = 'timeOut';
    
    
            if (ENV.environment === 'production') {}
    
            if (ENV.environment === 'development') {
              //var formStatus = 'orderTimeOut'; // + buy_coins_repeat.gif ===
              //var formStatus = 'cashinWaiting'; // + pinned balls ===
              //var formStatus = 'completed'; // + only_bank.gif
              //var formStatus = 'phoneVerification';
              //var formStatus = 'videoVerification'; // + error_verification.gif
              //var formStatus = 'cardVerification'; // deleted, changes to videoVerification
              //var formStatus = 'cardDeclined'; // + part1.gif
              //var formStatus = 'processing'; // + buy_coins.gif
              //var formStatus = 'moneySend'; // + success.gif
              //var formStatus =  'waitingForAccountCreation'; // +
              //var formStatus =  'Non3DS'; // part1.gif
              //var formStatus = 'timeOut';
              //var formStatus = 'processingBtc';
    
    
              controller.set('isVideoRecord', true); // Show video record
            }
    
            if (ENV.environment === 'test') {
              //var formStatus = 'orderTimeOut'; // + buy_coins_repeat.gif ===
              //var formStatus = 'cashinWaiting'; // + pinned balls ===
              //var formStatus = 'completed'; // + only_bank.gif
              //var formStatus = 'phoneVerification';
              //var formStatus = 'videoVerification'; // + error_verification.gif
              //var formStatus = 'cardVerification'; // deleted, changes to videoVerification
              //var formStatus = 'cardDeclined'; // + part1.gif
              //var formStatus = 'processing'; // + buy_coins.gif
              //var formStatus = 'moneySend'; // + success.gif
              //var formStatus = 'waitingForAccountCreation'; // +
              //var formStatus = 'Non3DS'; // part1.gif
              //var formStatus = 'timeOut';
            }
          
            // Остались:
            // good.gif
    
            /*console.log('setupController formStatus = ' + formStatus);
            console.log('setupController requestId = ' + requestId);
            console.log('setupController confirmCode = ' + confirmCode);
            if (requestId === null && confirmCode === null) {
              
              console.log('Форма покупки');
            } else {
              console.log('Форма заказа');
              controller.set('returned', true);
            }*/
    
            if (formStatus === 'completed') {
              console.log('setup step #5'); // Показ статуса заказа
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target2', 1);
              controller.set('step1', false);
              controller.set('step5', true);
              controller.set('step', 5);
              controller.set('returned', true);
            } else if (formStatus === 'videoVerification') { // Верификация по видео
              /*console.log('setup step #6');
              controller.set('step1', false);
              controller.set('step6', true);
              controller.set('step', 6);
              controller.set('returned', true);*/
    
              /**/
              console.log('setup step #15');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target4', 1);
              controller.set('step1', false);
              controller.set('step15', true);
              controller.set('step', 15);
              controller.set('returned', true);
    
    
            } else if (formStatus === 'phoneVerification') { // Верификация по коду с телефона
              console.log('setup step #7');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target4', 1);
              controller.set('step1', false);
              controller.set('step7', true);
              controller.set('step', 7);
              controller.set('returned', true);
            } else if (formStatus === 'orderTimeOut') { // Показ статуса OrderTimeOut
              console.log('setup step #8');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target3', 1);
              controller.set('step1', false);
              controller.set('step8', true);
              controller.set('step', 8);
              controller.set('returned', true);
            } else if (formStatus === 'cashinWaiting') {
              console.log('setup step #9');
              controller.set('step1', false);
              controller.set('step9', true);
              controller.set('step', 9);
              controller.set('returned', true);
            } else if (formStatus === 'processing') {
              console.log('setup step #10');
              controller.set('step1', false);
              controller.set('step10', true);
              controller.set('step', 10);
              controller.set('returned', true);
            } else if (formStatus === 'moneySend') {
              console.log('setup step #11');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target2', 1);
              controller.set('step1', false);
              controller.set('step11', true);
              controller.set('step', 11);
              controller.set('returned', true);
            } else if (formStatus === 'cardDeclined') {
              console.log('setup step #12');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target3', 1);
              controller.set('step1', false);
              controller.set('step12', true);
              controller.set('step', 12);
              controller.set('returned', true);
            } else if (formStatus === 'waitingForAccountCreation') {
              console.log('setup step #13');
              controller.set('step1', false);
              controller.set('step13', true);
              controller.set('step', 13);
              controller.set('returned', true);
            } else if (formStatus === 'Non3DS') {
              console.log('setup step #14');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target3', 1);
              controller.set('step1', false);
              controller.set('step14', true);
              controller.set('step', 14);
              controller.set('returned', true);
            }/* else if (formStatus === 'timeOut') {
              console.log('setup step #15');
              controller.set('step1', false);
              controller.set('step15', true);
              controller.set('step', 15);
              controller.set('returned', true);
            }*/
            else if (formStatus === 'cashinWaitingBtc') {
              console.log('setup step #16');
              controller.set('step1', false);
              controller.set('step16', true);
              controller.set('step', 16);
              controller.set('returned', true);
            } else if (formStatus === 'moneySendBtc') {
              console.log('setup step #17');
              googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target2', 1);
              controller.set('step1', false);
              controller.set('step17', true);
              controller.set('step', 17);
              controller.set('returned', true);
            } else if (formStatus === 'processingBtc') {
              console.log('setup step #18');
              controller.set('step1', false);
              controller.set('step18', true);
              controller.set('step', 18);
              controller.set('returned', true);
            }
    
            
    
    
    
            //if (typeof FastBoot === 'undefined' && formStatus !== 'videoVerification') {
            
            if (formStatus !== 'videoVerification')
            if (typeof FastBoot === 'undefined') {
              console.log('Отслеживание изменения статуса запущено');
              // TODO: проверять аналогично изменение страницы, если пользователь перейдет на другую страницу
              var lastStatus = "";
              var lastPage = window.location.href;
              let requestCount = false;
              var timerId = setInterval(function() {
              let requestId = me.get('share').getRequestId();
              let confirmCode = me.get('share').getConfirmCode();
              let page = window.location.href;
              $.ajax({
                async: false,
                method: 'POST',
                contentType: 'application/json; charset=utf-8',
                url: "/Notify/getData",
                data: "{'requestId':'" + requestId + "','confirmCode':'" + confirmCode + "'}",
                success: function onSucess(result) {
                  
                  let NotifyData = $.parseJSON(result.d);
                  let status = NotifyData.s;
    
                  console.log('iT = ' + NotifyData.iT);
    
                  if (NotifyData.iT == 10) {
                    if (status === 'CashinWaiting') {
                      if (NotifyData.cnf == '-1')
                        status = 'cashinWaitingBtc';
                      else
                        status = 'processingBtc';
                    }
                      //status = 'cashinWaitingBtc';
                    else if (status === 'Processing')
                      status = 'processingBtc';
                  }
    
                  console.log('Получен статус: ' + status + ' Прошлый статус: ' + lastStatus + ' iT = ' + NotifyData.iT);
                  console.log('Прошлая страница: ' + page + '\nТекущая страница: ' + lastPage);
      
                  if (status === "Completed" || status === "TimeOut") {
                    clearInterval(timerId);
                    console.log('Script is stopped by status: ' + status);
                    return;
                  }
      
                  if (requestCount === true) {
      
                    // Stop timer if page is changed
                    if (page !== lastPage) {
                      console.log('Script is stopped by page');
                      clearInterval(timerId);
                    }
      
                    // If status changed then reload page
                    if (lastStatus !== status)
                      window.location.reload();
                  }
                  else
                    requestCount = true;
                  lastStatus = status;
                }
              });
              }, 15000);
            }
          }
          share.setRedirectFromBank(false);
      },
      actions: {
        /*error(error, transition) {
          console.log('Поймана ошибка');
          console.log(error);
          //if (error) { return this.transitionTo('error-page'); }
        }*/
      }
    });
    
    function setCookie(name, value, options) {
      options = options || {};
    
      var expires = options.expires;
    
      if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
      }
      if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
      }
    
      value = encodeURIComponent(value);
    
      var updatedCookie = name + "=" + value;
    
      for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
          updatedCookie += "=" + propValue;
        }
      }
    
      document.cookie = updatedCookie;
    }
    
    function deleteCookie(name) {
      setCookie(name, "", {
        expires: -1
      })
    }
    
    // возвращает cookie с именем name, если есть, если нет, то undefined
    function getCookie(name) {
      var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }