import Ember from 'ember';
import fetch from 'fetch';
import RSVP from 'rsvp';

export default Ember.Route.extend({
  headTags: function() {
    return [
      {
        type: 'link',
        tagId: 'head-canonical',
        attrs: {
          rel: 'canonical',
          href: window.location.href
        }
      },
      {
        type: 'meta',
        tagId: 'head-robots',
        attrs: {
          name: 'robots',
          content: 'index, follow'
        }
      }
    ]
  },
  model: function(params) {



    console.log('params from currency 2');
    console.log(params);

    let name = params['name'].toLowerCase().split(' ').join('-');

    function pullCrypto(){
      return fetch('https://api.coinmarketcap.com/v1/ticker/' + name + '/').then((data) => {
        return data.json();
      }).then((data) => {
        return data;

        /*return `[
          {
              "id": "bitcoin", 
              "name": "Bitcoin", 
              "symbol": "BTC", 
              "rank": "1", 
              "price_usd": "6518.99510126", 
              "price_btc": "1.0", 
              "24h_volume_usd": "5135592288.95", 
              "market_cap_usd": "112110092304", 
              "available_supply": "17197450.0", 
              "total_supply": "17197450.0", 
              "max_supply": "21000000.0", 
              "percent_change_1h": "0.27", 
              "percent_change_24h": "-8.39", 
              "percent_change_7d": "-14.19", 
              "last_updated": "1533742113"
          }
      ]`;*/

      })
    }

    function pullCryptoHistory() {

      return fetch('https://graphs.coinmarketcap.com/currencies/bitcoin/1480341252000/1511877252000/').then((data) => {
      // return fetch('http://coinmarketcap.northpole.ro/history.json?coin=' + name).then((data) => {
        return data.json();
      }).then((data) => {
        return data;
      })
    }

    return RSVP.hash({
      info: pullCrypto(),
      chart: pullCryptoHistory()
    });
  }
});
