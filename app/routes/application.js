import Ember from 'ember';
import RSVP from 'rsvp';

export default Ember.Route.extend({
    model: function(params) {
        let hash = {};
        let uid = params.user;
        if (uid) {
            hash.user = uid;
        }
        return RSVP.hash({
            value: this.get('store').findAll('value').catch((reason) => {
              console.log('Catch error on load mobgetcurrencies in route appication');
              console.log(reason);
              let controller = Ember.getOwner(this).lookup('controller:lang/change');
              controller.set('errorMobGetCurrencies', true);
            }),
            cashinfo: this.get('store').query('cashinfo', hash).catch((reason) => {
                console.log('Catch store error cashinfo from rpote lang/change');
                console.log(reason);
            })
          });
    }  
});
