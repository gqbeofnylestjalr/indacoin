import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Route.extend({
  //ENV: ENV,
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  cookies: Ember.inject.service(),
  fastboot: Ember.inject.service(),

  headTags: function() {

    let me = this;
    var link = "";
    if (typeof FastBoot === 'undefined') {
      if (window.hasOwnProperty('location'))
        if (window.location.hasOwnProperty('href')) {
          link = window.location.href;
        }
    }
    else {
      let path = this.get('fastboot.request.path');
      if (path.toLowerCase() === '/en_gb' || path.toLowerCase() === '/en_gb/')
        path = '/';
      link = "https://" + this.get('fastboot.request.host') + path;
    }
    //let lang = this.get('i18n.locale');
    //let language = this.get('language');
    //console.log('Debug: ' + language);


    function toCountryName(shortName) {
      let country = shortName.slice(3, 5); // GB
      return me.get('i18n').t('country.' + country + '.name');
    }

    let controller = this.controllerFor(this.routeName);
    //console.log('country full name = ' + toCountryName(Ember.get(ENV.i18n, 'lang')));
    return [{
      type: 'title',
      tagId: 'head-title',
      //content: this.get('i18n').t('tags.title4', { country: this.get('language') })
      content: this.get('i18n').t('tags.title4', { country: toCountryName(Ember.get(ENV.i18n, 'lang')) })
    },
    {
      type: 'meta',
      tagId: 'head-description',
      attrs: {
        name: 'description',
        content: this.get('i18n').t('meta.description3')
      }
    },
    {
      type: 'meta',
      tagId: 'head-robots',
      attrs: {
        name: 'robots',
        content: 'index, follow'
      }
    },
    {
      type: 'link',
      tagId: 'head-canonical',
      attrs: {
        rel: 'canonical',
        href: link
      }
    },
    {
      type: 'meta',
      tagId: 'head-robots',
      attrs: {
        name: 'robots',
        content: 'index, follow'
      }
    }]
  //}},
  },
  
  afterModel(params, transition) {
    
    //if (typeof FastBoot === 'undefined') {
    //console.log('Установка языка в lang: ' + params.lang);
    const i18n = this.get('i18n');
    var cookieService = this.get('cookies');

    let locales = [ "de", "ar", "en", "es", "fr", "it", "pt", "ru", "tr" ];
    let regions = [ "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG", "AG", "AR", "AM", "AW", "AU", "AT", "AZ",
      "BS", "BH", "BD", "BB", "BY", "BE", "BZ", "BJ", "BM", "BT", "BO", "BA", "BW", "BV", "BR", "IO", "BN", "BG", "BF",
      "BI", "KH", "CM", "CA", "CV", "KY", "CF", "TD", "CL", "CN", "CX", "CC", "CO", "KM", "CG", "CD", "CK", "CR", "CI",
      "HR", "CU", "CY", "CZ", "DK", "DJ", "DM", "DO", "EC", "EG", "SV", "GQ", "ER", "EE", "ET", "FK", "FO", "FJ", "FI",
      "FR", "GF", "PF", "TF", "GA", "GM", "GE", "DE", "GH", "GI", "GR", "GL", "GD", "GP", "GU", "GT", "GG", "GN", "GW",
      "GY", "HT", "HM", "VA", "HN", "HK", "HU", "IS", "IN", "ID", "IR", "IQ", "IE", "IM", "IL", "IT", "JM", "JP", "JE",
      "JO", "KZ", "KE", "KI", "KP", "KR", "KW", "KG", "LA", "LV", "LB", "LS", "LR", "LY", "LI", "LT", "LU", "MO", "MK",
      "MG", "MW", "MY", "MV", "ML", "MT", "MH", "MQ", "MR", "MU", "YT", "MX", "FM", "MD", "MC", "MN", "MS", "MA", "MZ",
      "MM", "NA", "NR", "NP", "NL", "AN", "NC", "NZ", "NI", "NE", "NG", "NU", "NF", "MP", "NO", "OM", "PK", "PW", "PS",
      "PA", "PG", "PY", "PE", "PH", "PN", "PL", "PT", "PR", "QA", "RE", "RO", "RU", "RW", "SH", "KN", "LC", "PM", "VC",
      "WS", "SM", "ST", "SA", "SN", "CS", "SC", "SL", "SG", "SK", "SI", "SB", "SO", "ZA", "GS", "ES", "LK", "SD", "SR",
      "SJ", "SZ", "SE", "CH", "SY", "TW", "TJ", "TZ", "TH", "TL", "TG", "TK", "TO", "TT", "TN", "TR", "TM", "TC", "TV",
      "UG", "UA", "AE", "GB", "US", "UM", "UY", "UZ", "VU", "VE", "VN", "VG", "VI", "WF", "EH", "YE", "ZM", "ZW"
    ];

    let lang = params.lang; // en_US
    let IncLangCode = lang.split('_')[0] // en
    let langCode = "";
    let IncRegion = lang.split('_')[1]; // US
    IncRegion = IncRegion || ""; // in case if country is undefined or null
    let region = "";

    // Если XX не определен, то по-умолчанию определяется US
    let fallback2 = true;
    for(let i = 0; i < regions.length; i++) {
      if(regions[i] == IncRegion.toUpperCase()) {
        region = IncRegion;
        fallback2 = false;
        break;
      }
    }

    // Если xx не определен, то по-умолчанию определяется en
    let fallback = true;
    
    for(var i = 0; i < locales.length; i++) {
      if(locales[i] == IncLangCode.toLowerCase()) {
        langCode = IncLangCode;
        fallback = false;
        this.set('language', langCode + '_' + region.toUpperCase()); // en_US
        this.set('i18n.locale', langCode); // en
        Ember.set(ENV.i18n, 'lang', langCode + '_' + region.toUpperCase()); // ru_RU
        cookieService.write('lang', langCode + '_' + region.toUpperCase(), {path: '/'}); // ru_RU
        break;
      }
    }//debugger;
    // In case getting language wrong parameters from GET-request
    if (fallback || fallback2) { // true - если не найдено

      // Cookies

      let localLang = cookieService.read('lang');

      let lang = null;
      if (localLang !== undefined) {

        //console.log('Загружено из Cookie: ' + localLang);

        var toLangCode = 'en';
        var toRegion = 'GB';

        // Проверяем, что загрузилось что-то с черточкой xx_XX. Если нет, то ставим по-умолчанию en_GB
        // TODO: заменить на регулярку
        if (localLang.indexOf("_") != -1) {

          // TODO: проверять из белогго листа страны и язык
          langCode = localLang.split('_')[0]; // 'en';
          region = localLang.split('_')[1]; // 'US';

          // Если xx не обнаружен, то по-умолчанию остается en
          for(var i = 0; i < locales.length; i++) {
            if(locales[i] == langCode.toLowerCase()) {
              toLangCode = langCode.toLowerCase();
              break;
            }
          }

          // Если XX не обнаружен, то по-умолчанию остается DE
          for(var i = 0; i < regions.length; i++) {
            if(regions[i].toUpperCase() == region.toUpperCase()) {
              toRegion = region.toUpperCase();
              break;
            }
          }
        }

        //console.log('toLangCode = ' + toLangCode);
        //console.log('toRegion = ' + toRegion);

        langCode = toLangCode;
        region = toRegion;

        this.set('language', toLangCode + '_' + toRegion); // en_US
        this.set('i18n.locale', toLangCode);
        Ember.set(ENV.i18n, 'lang', toLangCode + '_' + toRegion);
      }
      else { // Set default language settings: 'en_US'
      
        region = 'GB';
        langCode = 'en';
        this.set('language', 'en' + '_' + region.toUpperCase()); // en_US
        this.set('i18n.locale', 'en');
        Ember.set(ENV.i18n, 'lang', 'en' + '_' + region.toUpperCase());

        // Cookies
        var cookieService = this.get('cookies');
        cookieService.write('lang', 'en_GB', {path: '/'});
      }
      
      //ENV.i18n.lang = langCode + '_' + region
      //this.transitionTo('lang.index', langCode + '_' + region);
    } else {

      //this.set('language', lang.toUpperCase()); // en_US
      //this.set('i18n.locale', langCode);
      //Ember.set(ENV.i18n, 'lang', 'en' + '_' + region.toUpperCase());
      //Ember.set(ENV.i18n, 'lang', lang.toUpperCase());

      
      // TODO: брать из CloudFlare

      
      //localStorage.setItem('lang', langCode + '_' + region.toUpperCase());

      // Cookies
      //var cookieService = this.get('cookies');
      //cookieService.write('lang', langCode + '_' + region.toUpperCase(), {path: '/'});

    }
    this.get('share').setRegion(region); // Приходит US
  /*} else {

    console.log('Detect lang on fastboot');

    let cookieService = this.get('cookies');

    //let localLang2 = cookieService.read('lang2');
    //console.log(localLang2);

    let localLang = cookieService.read('lang'); // en_US
    if (localLang === undefined) {
      this.set('language', 'en_DE'); // en_US
      this.set('i18n.locale', 'en');
      Ember.set(ENV.i18n, 'lang', 'en_DE');
    } else {
      let langCode = localLang.split('_')[0]; // 'en';
      let region = localLang.split('_')[1]; // 'US';
      this.set('language', localLang); // en_US
      this.set('i18n.locale', langCode);
      Ember.set(ENV.i18n, 'lang', localLang);
    }
  }*/


    // TODO: ставить по-умолчанию en_GB, если не пришел lang из cookie или из маршрута
    //ENV.i18n.lang = "en_GB"; // Default language
    /*let localLang = "en_GB";
    let langCode = 'en';
    this.set('language', localLang); // en_US
    this.set('i18n.locale', langCode); // en*/
  }/*,
  actions: {
    error(error, transition) {
      console.log('Поймана ошибка');
      console.log(error);
    }
  }*/

});

