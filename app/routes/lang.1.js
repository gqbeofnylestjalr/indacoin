import Ember from 'ember';

export default Ember.Route.extend({
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),

  /*model() {
    return this.store.findAll('title');
  },*/
  beforeModel(params) {
    //console.log('transition:');
    //console.log(params.intent.url);
    //debugger;

    /*let me = this;
    var i18n = this.get('i18n');
    i18n.addTranslations('ru', {'meta.title' : 'TRUE'});
    me.set('i18n.locale', 'ru');*/

  },
  
  afterModel(params) {
if (typeof FastBoot === 'undefined') {
    //debugger;
    console.log('Установка языка в lang');
    const i18n = this.get('i18n');
    let langCode = "";
    let region = "";

    // Чтение выставленных пользователем языка и региона из localStorage
      //localStorage.setItem('lang', 'ru_US');
      //console.log('localStorage: ' + localStorage.getItem('lang'));
      var localLang = localStorage.getItem('lang');
      if (localLang === null) {
        //let locales = this.get('i18n.locales'); // [ "de", "dz", "en", "fr", "it", "pt", "ru", "tr" ];
        let locales = [ "de", "dz", "en", "fr", "it", "pt", "ru", "tr" ];
        let regions = [ "RU", "US",      "AF", "AX", "AL", "DZ", "AS", "AD", "AO", "AI", "AQ", "AG" ];
        let lang = params.lang; // en_US
        let IncLangCode = lang.split('_')[0] // en
        let IncRegion = lang.split('_')[1]; // US

        // Если XX не определен, то по-умолчанию определяется US
        let fallback2 = true;
        for(let i = 0; i < regions.length; i++) {
          if(regions[i] == IncRegion.toUpperCase()) {
            region = IncRegion;
            fallback2 = false;
            console.log('Установлен регион из lang: ' + region);
            break;
          }
        }
        if (fallback2) {
          region = 'US';
          console.log('Установлен регион из lang default: ' + region);
        }
    } else {
      let lang = localLang;
      let IncLangCode = localLang.split('_')[0]; // en
      let IncRegion = localLang.split('_')[1]; // US 
    }
    

    this.get('share').setRegion(region);

    // Если xx не определен, то по-умолчанию определяется en
    let fallback = true;
    for(var i = 0; i < locales.length; i++) {
      if(locales[i] == IncLangCode.toLowerCase()) {
        langCode = IncLangCode;
        fallback = false;
        console.log('Установлен язык из lang: ' + langCode);
        this.set('language', langCode + '_' + region.toUpperCase()); // en_US
        this.set('i18n.locale', langCode); // en
        break;
      }
    }
    if (fallback) {
      langCode = 'en';
      console.log('Установлен язык из lang default: en');
      this.set('language', 'en' + '_' + region.toUpperCase()); // en_US
      this.set('i18n.locale', 'en');
    }

    // Меняем ru_RU на en_US
    if (fallback || fallback2) {
      if (typeof FastBoot === 'undefined') {
        let url = window.location.pathname;
        let search = window.location.search;
        let newUrl = url.slice(0, 1) + langCode + url.slice(3, 4) + region + url.slice(6);
        window.history.pushState("Change language", "Title", newUrl + search);
      }
    }

    // Меняем en_RU на en_US
    /*if (fallback2) {
      if (typeof FastBoot === 'undefined') {
        let url = window.location.pathname;
        let search = window.location.search;
        let newUrl = url.slice(0, 1) + region + url.slice(3);
        window.history.pushState("Change language", "Title", newUrl + search);



      }
    }*/


            /*var url = window.location.pathname;
        var search = window.location.search;
        var newUrl = url.slice(0, 4) + 'RU' + url.slice(6);
        console.log(newUrl + search);*/


    // Смена перевода "In More Than 100 Countries Including United States"
    // Перенести в функцию
    //console.log('Установлен регион из lang: ' + region.toUpperCase());
    let word = i18n.t('meta.more') + ' ' + i18n.t('country.' + region.toUpperCase() + '.name');
    i18n.addTranslations(langCode, {'unit' : word});

  }
  }
});
