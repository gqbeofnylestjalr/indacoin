import Ember from 'ember';
import fetch from 'fetch';
import DS from 'ember-data';
import RSVP from 'rsvp';
import ENV from 'fastboot/config/environment';

export default Ember.Route.extend({
    ENV: ENV,
    i18n: Ember.inject.service(),
    cookies: Ember.inject.service(),
    fastboot: Ember.inject.service(),

    setupController: function(controller, model) {
      this._super(controller, model);
      //debugger;
      //console.log('model for index: ');
      //console.log(model);
      /*if (model.get('autoEdit')) {
        controller.set('isEditing', true);
      }*/
    },

    headTags: function() {
        let me = this;
        var link = "";
        if (typeof FastBoot === 'undefined') {
          if (window.hasOwnProperty('location'))
            if (window.location.hasOwnProperty('href')) {
              link = window.location.href;
            }
        }
        else {
          link = "https://" + this.get('fastboot.request.host') + this.get('fastboot.request.path');
        }

        function toCountryName(shortName) {
          let country = shortName.slice(3, 5); // GB
          return me.get('i18n').t('country.' + country + '.name');
        }
    
        let controller = this.controllerFor(this.routeName);

        return [{
          type: 'title',
          tagId: 'head-title',
          //content: this.get('i18n').t('tags.title2')
          content: this.get('i18n').t('tags.title4', { country: toCountryName(Ember.get(ENV.i18n, 'lang')) })
        },
        {
          type: 'meta',
          tagId: 'head-description',
          attrs: {
            name: 'description',
            //content: this.get('i18n').t('meta.description2')
            content: this.get('i18n').t('meta.description3')
          }
        },
        {
          type: 'meta',
          tagId: 'head-robots',
          attrs: {
            name: 'robots',
            content: 'index, follow'
          }
        },
        {
          type: 'link',
          tagId: 'head-canonical',
          attrs: {
            rel: 'canonical',
            href: link
          }
        },
        {
          type: 'meta',
          tagId: 'head-robots',
          attrs: {
            name: 'robots',
            content: 'index, follow'
          }
        }]
      },

    queryParams: {
      give: {
        replace: true
      },
      take: {
        replace: true
      },
      page: {
        refreshModel: true
      },
      size: {
        refreshModel: true
      }
    },

    beforeModel: function(params) {
      this._super(...arguments);
      this.get('store').unloadAll('facebook'); // On change language delete all facebook data
      //this.replaceWith('lang', 'ru_RU');
      //this.transitionToRoute('lang', {queryParams: {lang: 'ru_RU'}});



      //let newParams = { lang: 'ru_RU' };
      //(this.router.router || this.router._routerMicrolib).activeTransition.abort();
      //this.replaceWith('lang', {queryParams: newParams});

    },

    afterModel: function(params) {// if (typeof FastBoot === 'undefined') {

      console.log('0 - index.js');

      var cookieService = this.get('cookies');
      var regionByIp = cookieService.read('ipCountry');

      // On user back load language settings from Cookie
      let localLang = null;

      // Cookies
      localLang = cookieService.read('lang'); // en_US

      // TODO: убрать костыль
      if (localLang !== undefined) { // Если в Cookie с lang не пусто
        if (localLang.toLowerCase().indexOf('null') == -1) { // Если в Cookie нет null
          let lang = localLang.split('_')[0];
          //console.log('Load from local storage: ' + localLang);
          this.set('i18n.locale', lang);
          Ember.set(ENV.i18n, 'lang', localLang);
          //this.transitionTo('lang.index', localLang);
        }
        else {
          //let locales = this.get('i18n.locales');
          var language = "en";
          var region = "GB";

          /*if (typeof FastBoot === 'undefined') {
            language = navigator.language.split('-')[0];
            console.log('От navigator получен язык: ' + language);
          }*/

          var lang = language + '_' + region; // en_GB
          //const i18n = this.get('i18n');
          this.set('i18n.locale', language);
          Ember.set(ENV.i18n, 'lang', lang);

          // Cookies
          //var cookieService = this.get('cookies');
          //cookieService.write('lang', lang, {path: '/'});

          //this.transitionTo('lang.index', lang);
        }
      } else { // Если в Cookie с lang пусто
        //console.log('Nothing to load');

        //let locales = this.get('i18n.locales');
        var language = "en";
        var region = "GB";
        if (regionByIp)
          region = regionByIp;

        /*if (typeof FastBoot === 'undefined') {
          language = navigator.language.split('-')[0];
          console.log('От navigator получен язык: ' + language);
        }*/
        
        var lang = language + '_' + region;
        //const i18n = this.get('i18n');
        this.set('i18n.locale', language);

        //var cookieService = this.get('cookies');
        //cookieService.write('lang', lang, {path: '/'});
        
        //this.transitionTo('lang.index', lang);
      }
    //}
  },
  model: function(params) {

        const cookieService = this.get('cookies');
    
        var page = params.page;
        var size = params.size;

        // Set discount to cookie
        let discountFromCookie = cookieService.read('discount');
        let discount = params.discount || discountFromCookie;
        
        if (discount != null) {
          let cookieService = this.get('cookies');
          cookieService.write('discount', discount, {path: '/'});
          try {
            if (typeof FastBoot === 'undefined') {
              let time = new Date().getTime();
              let timeFromCookie = localStorage.getItem('saveReferralVisit');
              localStorage.setItem('saveReferralVisit', time);
              let elapsedTime = (time - timeFromCookie) / 1000;
              console.log('elapsedTime = ' + elapsedTime);
              if (elapsedTime > 300) {
              //console.log(localStorage.getItem('saveReferralVisit'));
                let discountData = JSON.stringify({ discount: discount });
                Ember.$.ajax({
                  url: "/change.aspx/saveReferralVisit",
                  async: true,
                  type: "post",
                  contentType: "application/json; charset=utf-8",
                  dataType: "json",
                  data: discountData,
                  success: function(result) {
                  },
                  error: function() {
                  }
                })
              }
            }
          } catch(e) {
            console.log('Ошибка отправки saveReferralVisit' + e);
          }
        }
        
        let hash = {};
        let uid = params.user;
        if (uid) {
            hash.user = uid;
        }
        return RSVP.hash({
        
          featured: this.get('store').query('currency', { limit: 4 }).catch((reason) => {
            console.log('Catch currenct error in route lang/index');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/index');
            controller.set('errorLoadData', true);
          }),
  
          currency: this.get('store').query('currency', { limit: size }).catch((reason) => {
            console.log('Catch blue screen in roue from currency');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/index');
            controller.set('errorLoadData', true);
          }),
  
          //exchanger_usd: this.get('store').findAll('exchanger-usd'),
          //exchanger_eur: this.get('store').findAll('exchanger-eur')
  
          //exchanger_usd2: this.get('store').findAll('exchange-usd'),
          exchanger_usd: this.get('store').findAll('exchanger-usd').catch((reason) => {
            console.log('Catch blue screen in roue from exchanger_usd');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/index');
            controller.set('errorLoadData', true);
          }),
  
          exchanger_eur: this.get('store').findAll('exchanger-eur').catch((reason) => {
            console.log('Catch blue screen in roue from exchanger_eur');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/index');
            controller.set('errorLoadData', true);
          }),
  
          exchanger_rub: this.get('store').findAll('exchanger-rub').catch((reason) => {
            console.log('Catch blue screen in roue from exchanger_rub');
            console.log(reason);
            let controller = Ember.getOwner(this).lookup('controller:lang/index');
            controller.set('errorLoadData', true);
          }),
  
          facebook: this.get('store').query('facebook', {lang: this.get('i18n.locale')}).catch((reason) => {
            console.log('Catch blue screen in facebook from lang/index');
            console.log(reason);
            //let controller = Ember.getOwner(this).lookup('controller:lang/index');
            //controller.set('errorLoadData', true);
          }),
          
          cashinfo: this.get('store').query('cashinfo', hash).catch((reason) => {
            console.log('Catch store error cashinfo from rpote lang/change');
            console.log(reason);
          })
        });
      }
      
      
      // ,setupController: function(controller, model) {
      //   this._super(controller, model);
      //   if (typeof FastBoot === 'undefined') {
      //     window.scrollTo(0,0);
      //   }
      // }

});