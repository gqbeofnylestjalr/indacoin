import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);
    let data = payload[0];
    //console.log('payload from serializer before:');
    //console.log(payload[0]);
    //debugger;


    json.data =
      [{
        "id": data.id,
        "type": "pull-crypto",
        "attributes": {
          "name": data.name,
          "symbol": data.symbol,
          "rank": data.rank,
          "price_usd": data.price_usd,
          "price_btc": data.price_btc,
          "volume_usd_24h": data['24h_volume_usd'],
          "market_cap_usd": data.market_cap_usd,
          "available_supply": data.available_supply,
          "total_supply": data.total_supply,
          "max_supply": data.max_supply,
          "change1h": data.percent_change_1h,
          "change24h": data.percent_change_24h,
          "change7d": data.percent_change_7d
        }
      }];

    /*for(let item in payload[0]) {
      item.id = payload[0][item].id;
      item.type = 'pull-crypto';
      item.attributes = {
        name: payload[0][item].name,
        symbol: payload[0][item].symbol,
        rank: payload[0][item].rank,
        price_usd: payload[0][item].price_usd,
        price_btc: payload[0][item].price_btc,
        volume_usd_24h: payload[0][item].get('24h_volume_usd'),
        market_cap_usd: payload[0][item].market_cap_usd,
        available_supply: payload[0][item].available_supply,
        total_supply: payload[0][item].total_supply,
        max_supply: payload[0][item].max_supply,
        change1h: payload[0][item].percent_change_1h,
        change24h: payload[0][item].percent_change_24h,
        change7d: payload[0][item].percent_change_7d
      }
    }*/

    /*payload[0].forEach((item) => {
      item.id = item.id;
      item.type = 'pull-crypto';
      item.attributes = {
        name: item.name,
        symbol: item.symbol,
        rank: item.rank,
        price_usd: item.price_usd,
        price_btc: item.price_btc,
        volume_usd_24h: item.get('24h_volume_usd'),
        market_cap_usd: item.market_cap_usd,
        available_supply: item.available_supply,
        total_supply: item.total_supply,
        max_supply: item.max_supply,
        change1h: item.percent_change_1h,
        change24h: item.percent_change_24h,
        change7d: item.percent_change_7d
      }

    delete item.createdAt;
    delete item.fullText;
    delete item.header;
    delete item.shortText;
    delete item.title;
    });*/

    //console.log('payload[0] after:');
    //console.log(json.data);

    //json.data = payload[0];
    return json;
  }
});