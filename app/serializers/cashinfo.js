import DS from 'ember-data';
import ENV from 'fastboot/config/environment';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);
    //console.log("debug: cashinfo BEGIN from serializer");
    //console.log(payload);

    var obj;

    // TODO: on debug work only in unsafe run browser
    obj = JSON.parse(payload.d);
    // if (ENV.environment === 'production') {
    //   obj = JSON.parse(payload.d);
    // }
    
    // if (ENV.environment === 'development') {
    //   obj = payload.d;
    // }
  
    // if (ENV.environment === 'test') {
    //   obj = payload.d;
    // }

    // DEVELOPMENT MODE
    //let obj = payload.d;

    // PRODUCTION MODE
    //let obj = JSON.parse(payload.d);

    let usdAvailable = (!!obj.cashTypes.cashIn['16']) || false;
    let eurAvailable = (!!obj.cashTypes.cashIn['50']) || false;
    let rubAvailable = (!!obj.cashTypes.cashIn['54']) || false;
    //let btcAvailable = (!!obj.cashTypes.cashIn['10']) || false;
    let btcAvailable = false;

    let audAvailable = (!!obj.cashTypes.cashIn['55']) || false;
    let gbpAvailable = (!!obj.cashTypes.cashIn['56']) || false;


    let usdmin = 30.00000000;
    if (obj.cashTypes.cashIn['16'])
      usdmin = obj.cashTypes.cashIn['16'].fee.minSum;

    let usdmax = 3000.00000000;
    if (obj.cashTypes.cashIn['16'])
      usdmax = obj.cashTypes.cashIn['16'].fee.maxSum;

    let eurmin = 30.00000000;
    if (obj.cashTypes.cashIn['50'])
      eurmin = obj.cashTypes.cashIn['50'].fee.minSum;

    let eurmax = 3000.00000000;
    if (obj.cashTypes.cashIn['50'])
      eurmax = obj.cashTypes.cashIn['50'].fee.maxSum;

    let rubmin = 2000.00000000;
    if (obj.cashTypes.cashIn['54'])
      rubmin = obj.cashTypes.cashIn['54'].fee.minSum;

    let rubmax = 200000.00000000;
    if (obj.cashTypes.cashIn['54'])
      rubmax = obj.cashTypes.cashIn['54'].fee.maxSum;

    let btcmin = 0.01;
    let btcmax = 0.2;

    let audmin = 30.00000000;
    if (obj.cashTypes.cashIn['55'])
      audmin = obj.cashTypes.cashIn['55'].fee.minSum;

    let audmax = 30000.00000000;
    if (obj.cashTypes.cashIn['55'])
      audmax = obj.cashTypes.cashIn['55'].fee.maxSum;

    let gbpmin = 30.00000000;
    if (obj.cashTypes.cashIn['56'])
      gbpmin = obj.cashTypes.cashIn['56'].fee.minSum;

    let gbpmax = 3000.00000000;
    if (obj.cashTypes.cashIn['56'])
      gbpmax = obj.cashTypes.cashIn['56'].fee.maxSum;

    // False in case if no one currency is available
    // true - show
    let changeDirection = !(usdAvailable || eurAvailable || rubAvailable || btcAvailable || audAvailable || gbpAvailable); // show Alert

    let data =
      [{
        "id": "1",
        "type": "cashinfo",
        "attributes": {
          "usdAvailable": usdAvailable,
          "eurAvailable": eurAvailable,
          "rubAvailable": rubAvailable,
          "btcAvailable": btcAvailable,
          "audAvailable": audAvailable,
          "gbpAvailable": gbpAvailable,
          "usdmin": usdmin,
          "usdmax": usdmax,
          "eurmin": eurmin,
          "eurmax": eurmax,
          "rubmin": rubmin,
          "rubmax": rubmax,
          "btcmin": btcmin,
          "btcmax": btcmax,
          "audmin": audmin,
          "audmax": audmax,
          "gbpmin": gbpmin,
          "gbpmax": gbpmax,
          "changeDirection": changeDirection,
          "test": "testing",

          "usdCommentPath": obj.cashTypes.cashIn['16'].commentPath,
          "eurCommentPath": obj.cashTypes.cashIn['50'].commentPath,
          "rubCommentPath": obj.cashTypes.cashIn['54'].commentPath,
          "btcCommentPath": obj.cashTypes.cashIn['10'].commentPath,
          "audCommentPath": obj.cashTypes.cashIn['55'].commentPath,
          "gbpCommentPath": obj.cashTypes.cashIn['56'].commentPath

          // "usdCommentPath": "",
          // "eurCommentPath": "CashTypes.Deposit.MasterCardProblem",
          // "rubCommentPath": "",
          // "btcCommentPath": "",
          // "audCommentPath": "",
          // "gbpCommentPath": ""

          
        }
      }];
    json.data = data;

    //console.log("debug: cashinfo END from serializer");
    //console.log(json);

    return json;
  }
});
