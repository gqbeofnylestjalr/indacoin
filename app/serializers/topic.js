import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);

    // let truncation = function(text, maxLength){
    //   // Increase length of hidden text on tags length
    //   var regex = /<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)<\/\1>/gi;
    //   var res = null;
    //   while ((res = regex.exec(text)) != null) {
    //       maxLength += res[0].length - res[2].length;
    //   }
    //   if (text.length > maxLength)
    //     return text.substr(0, maxLength) + '...';
    //   else
    //     return text;
    // }

    // Filter empty news
    payload = payload.filter((item) => {
      if (item.shortText.length !== 0)
        return true;
    });
    
    payload.forEach((item) => {
      item.id = item.url;
      item.type = 'news';
      item.attributes = {
        //shortText: truncation(item.shortText, 125),
        shortText: item.shortText,
        url: item.url,
        date: item.createdAt
      }
        delete item.createdAt;
        delete item.header;
        delete item.shortText;
        delete item.title;
    });
    json.data = payload;
    return json;
  }
});