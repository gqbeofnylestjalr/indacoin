import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);
    payload.forEach( (bits) => {
      bits.id = bits.name.toLowerCase();;

      bits.type = 'lang';

      bits.attributes = {
        cur_id: bits.cur_id,
        ext_market: bits.ext_market,
        txfee: bits.txfee,
        shortName: bits.short_name,
        name: bits.name,
        cointype: bits.cointype,
        imageurl: bits.imageurl,
        availableSupply: bits.availableSupply,
        price: bits.price
      }

    });
    json.data = payload;
    return json;
  }
});
