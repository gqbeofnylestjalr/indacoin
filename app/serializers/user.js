import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);

    console.log('json');
    console.log(json);
    let data =
      [{
        "id": "1",
        "type": "user",
        "attributes": {
          "userName": payload.d.accData.Nick
        }
      }];
    json.data = data;
    return json;
  }
});