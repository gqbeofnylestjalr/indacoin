import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);
    
    //debugger;
    //console.log('serializer');
    //console.log(payload);

    let truncation = function(text, maxLength){

      // Increase length of hidden text on tags length
      var regex = /<([A-Z][A-Z0-9]*)\b[^>]*>(.*?)<\/\1>/gi;
      //var str = item.textShort;
      var res = null;
      while ((res = regex.exec(text)) != null) {
          maxLength += res[0].length - res[2].length;
          //console.log('tag = ' + res[2] + ' len = ' + (res[0].length - res[2].length));
      }
      if (text.length > maxLength)
        return text.substr(0, maxLength) + '...';
      else
        return text;
    }

    payload.forEach((item) => {
      item.id = item.url;
      item.type = 'news';
      item.attributes = {
        date: item.createdAt,
        title: item.title,
        //text: item.fullText,
        text: truncation(item.fullText, 125),
        shortText: item.shortText,
        fullText: item.fullText,
        url: item.url,
        show: false
      }

    delete item.createdAt;
    delete item.fullText;
    delete item.header;
    delete item.shortText;
    delete item.title;

    });
    
    json.data = payload;
    return json;
  }
});