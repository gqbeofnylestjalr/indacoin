import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    // debugger;
    let json = this._super(...arguments);
    payload.forEach( (currency) => {
      // console.log(currency);
      let eternal = currency.id.replace('-', ' ');
      currency.id = currency.rank;
      currency.type = 'currency';
      let color = function(){
        if(currency.percent_change_24h > 0) {
          //return 'positive';
          return 'positive-color';
        } else {
          return 'negative-color';
        }
      };
      
      let symbol = function(){
        if(currency.name == "Bitcoin Cash") {
          return "BTC"
        } else {
          return currency.symbol
        }
      }
      currency.attributes = {
        eternal: eternal,
        name: currency.name,
        symbol: symbol(),
        price_usd: currency.price_usd,
        price_btc: currency.price_btc,
        market_cap_usd: currency.market_cap_usd,
        available_supply: currency.available_supply,
        change1h: currency.percent_change_1h,
        change24h: currency.percent_change_24h,
        change7d: currency.percent_change_7d,
        volume24h: currency['24h_volume_usd'],
        color: color()
      };
      delete currency.name;
      delete currency.symbol;
      delete currency.price_usd;
      delete currency.price_btc;
      delete currency.market_cap_usd;
      delete currency.available_supply;
      delete currency.percent_change_1h;

    });
    json.data = payload;
    return json;
  }
});
