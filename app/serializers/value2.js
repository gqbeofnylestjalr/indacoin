import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);

    //debugger;
    //console.log('Пришло ' + payload);
    //console.log(payload);

    /*payload.forEach(function(item, index, object) {
      if (item.isActive == false) {
        //console.log('Удаляем: ' + item.short_name);
        //object.splice(object.length - 1 - index, 1);
        payload.splice(index, 1);
      }
    });*/
    payload.slice().reverse().forEach(function(item, index, object) {
      if (item.isActive == false) {
        payload.splice(object.length - 1 - index, 1);
      }
    });


    /*var count = 0;
    payload.forEach(function(item, index, object) {
      if (item.isActive == false) {
        count ++;
      }
    });
    console.log('Осталось: ' + count);*/


    /*var list = "";
    payload.forEach(function(item, index, object) {
      list += item.name + '(' + item.short_name + ')\r\n';
    });
    console.log(list);*/

    payload.forEach( (currency, index, object) => {
      //console.log('currency.isActive = ' + currency.isActive);
      /*if (currency.isActive === false) {
        object.splice(index, 1);
        //return;
      }*/
      currency.id = currency.cur_id;
      currency.type = 'value';

      let name = currency.name;
      if (name === "I/O Coin")
        name = "IO Coin";

      currency.attributes = {
        cur_id: currency.cur_id,
        ext_market: currency.ext_market,
        //ext_market: i,
        ext_market_id: currency.ext_market_id,
        txfee: currency.txfee,
        short_name: currency.short_name,
        //name: currency.name,
        name: name,
        cointype: currency.cointype,
        imageurl: currency.imageurl,
        availableSupply: currency.availableSupply,
        price: currency.price
      };

      delete currency.cur_id;
      delete currency.ext_market;
      delete currency.ext_market_id;
      delete currency.txfee;
      delete currency.short_name;
      delete currency.name;
      delete currency.cointype;
      delete currency.imageurl;
      delete currency.availableSupply;
      delete currency.price;
      delete currency.isactive;

    });

    let swap = [];

    function isBtc(element, index, array) {
      if (element.attributes.name === 'Bitcoin')
        return element;
      else
        return false;
    }
    let btc = payload.find(isBtc);
    if (btc !== undefined) {
      let obj = {};
      obj.id = btc.id;
      obj.type = btc.type;
      obj.attributes = {
        cur_id: btc.attributes.cur_id,
        ext_market: btc.attributes.ext_market,
        ext_market_id: btc.attributes.ext_market_id,
        txfee: btc.attributes.txfee,
        short_name: btc.attributes.short_name,
        name: btc.attributes.name,
        cointype: btc.attributes.cointype,
        imageurl: btc.attributes.imageurl,
        availableSupply: btc.attributes.availableSupply,
        price: btc.attributes.price
      };
      swap.push(obj);
    }

    for (var i = 0; i < 6; i++) {
      let obj = {};
      obj.id = payload[i].id;
      obj.type = payload[i].type;
      obj.attributes = {
        cur_id: payload[i].attributes.cur_id,
        ext_market: payload[i].attributes.ext_market,
        ext_market_id: payload[i].attributes.ext_market_id,
        txfee: payload[i].attributes.txfee,
        short_name: payload[i].attributes.short_name,
        name: payload[i].attributes.name,
        cointype: payload[i].attributes.cointype,
        imageurl: payload[i].attributes.imageurl,
        availableSupply: payload[i].attributes.availableSupply,
        price: payload[i].attributes.price
      };
      swap.push(obj);
    }

    payload = payload.sort(function(a, b) {
      var nameA = a.attributes.short_name.toUpperCase();
      var nameB = b.attributes.short_name.toUpperCase();
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    });

    payload = swap.concat(payload);

    var i = 0;
    payload.forEach(function(item){
      item.id = i;
      i++;
    });


////////////////////////////////////////////
    /*let obj2 = {};
    obj2.id = 1000;
    obj2.type = 'value';
    obj2.isActive = true;
    obj2.minTradeSize = 0;
    obj2.attributes = {
      cur_id: '100',
      ext_market: '1',
      ext_market_id: '0',
      txfee: '0.02500000',
      short_name: 'BTC',
      name: 'Bitcoin',
      cointype: 'BTC',
      imageurl: '',
      availableSupply: '',
      price: ''
    };
    payload.push(obj2);*/
    //console.log(payload);
////////////////////////////////////////////

    //console.log('После удаления актив ' + payload.length);

    /*payload.forEach(function(item, index, object) {
      console.log(String(item.attributes.isActive) + ' ' + item.attributes.short_name + ' ' + index);
    });*/

    json.data = payload;
    return json;
  }
});
