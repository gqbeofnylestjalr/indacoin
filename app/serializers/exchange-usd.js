import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);

    /*payload.forEach( (bits) => {
      bits.id = bits.name.toLowerCase();;
      bits.type = 'lang';
      bits.attributes = {
        cur_id: bits.cur_id,
        ext_market: bits.ext_market,
        txfee: bits.txfee,
        shortName: bits.short_name,
        name: bits.name,
        cointype: bits.cointype,
        imageurl: bits.imageurl,
        availableSupply: bits.availableSupply,
        price: bits.price
      }

    });
    json.data = payload;
    return json;*/

    return {
        "data":
        [{
          "id": "1",
          "type": "exchanger-usd",
          "attributes": {
            "price": "$ 13825.6",
            //"url": "https://indacoin.com/change/buy-bitcoin-with-cardusd/"+ i18n.locale +"?amount_pay=30",
            "url": "/" + "ru" + "_" + "RU" + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=30",
            "cur": "USD",
            "sum": "30,00"
          }
        },
        {
          "id": "2",
          "type": "exchanger-usd",
          "attributes": {
            "price": "$ 13825.6",
            "url": "/" + "ru" + "_" + "RU" + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=100",
            "cur": "USD",
            "sum": "100,00"
          },
        },
        {
          "id": "3",
          "type": "exchanger-usd",
          "attributes": {
            "price": "$ 13825.6",
            "url": "/" + "ru" + "_" + "RU" + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=250",
            "cur": "USD",
            "sum": "250,00"
          },
        },
        {
          "id": "4",
          "type": "exchanger-usd",
          "attributes": {
            "price": "$ 13825.6",
            "url": "/" + "ru" + "_" + "RU" + "/change/buy-bitcoin-with-cardusd" + "?amount_pay=500",
            "cur": "USD",
            "sum": "500,00"
          }
        }]
      }

  }
});



