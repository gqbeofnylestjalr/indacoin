import DS from 'ember-data';

export default DS.RESTSerializer.extend({
  normalizeResponse(store, primaryModelClass, payload, id, requestType) {
    let json = this._super(...arguments);

    /*return {
        "data":
        [{
          "id": "1",
          "type": "facebook",
          "attributes": {
            'id': 1,
            'name': 'John Yance',
            'date': '17 May 2017',
            'text': 'buena pagina de bitcoins, proceso rapido y sencillo, se los recomiendo',
            'link': 'https://www.facebook.com/john.yance.9/posts/1206759696120213:0',
            'photo': 'https://scontent.frix2-1.fna.fbcdn.net/v/t1.0-1/c0.1.720.720/p720x720/16195614_1098496233613227_1360363683957543436_n.jpg?oh=b26f1e56b3066fb5fa2a62905331fef2&oe=5B463C4F',
            'stars': true
          }
        },
        {
          "id": "2",
          "type": "facebook",
          "attributes": {
            'id': 2,
            'name': 'Lesley Brown',
            'date': '23 May 2017',
            'text': "Brilliant service and convenient to use, the folk there are friendly and very helpful. Although it is a digital transaction, there is a person at the other side of it (which can be a MASSIVE help) and reassuring for sure, 5star service which I'll use again",
            'link': 'https://www.facebook.com/LEISB/posts/1406771172699010:0',
            'photo': 'https://scontent-amt2-1.xx.fbcdn.net/v/t1.0-1/c0.0.720.720/p720x720/18921788_1417554734953987_529796032077658846_n.jpg?oh=687867db14dd7c7fff2ea3494b030504&oe=5AAABF5F',
            'stars': true
          },
        }]
      }*/


    //json.data = payload;
    //return json;
    return payload;
  }
});
