import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  //this.route('lang/index', { path: '/' });
  //this.route('lang/error-page', { path: '/' });
  //this.alias('index', '/index', 'error-page');
  //this.route('currency');
  //this.route('index', { path: '/' });
  //this.alias('news', '/news', 'api');
  //this.alias('api', '/api', 'news');



  //this.route('lang', {path: '/'})
  this.route('index', {path: '/'});

  //this.alias('lang', '/lang', 'index');
  
  //this.route('lang.change', { path: 'change'});

  this.route('lang', {
    //path: '/:lang'
    path: '/:lang'
  }, function() {
    this.route('index', {path: '/'});
    this.route('currency');
    //this.route('currency', { path: '/currency/:currency_id' });
    this.route('change');
    this.route('change', { path: '/change/:change_id' });

    //this.route('change', {path: '/change/buy-bitcoin-with-cardusd'})
    /*this.route('change', {
      path: '/change/:change_id' }, function() {
        this.route('currency');
      });*/

    this.route('locale'); // Language map of the site

    this.route('affiliate');
    this.route('help');
    this.route('news');
    this.route('login');
    this.route('register');
    this.route('api', { path: '/api_doc' });
    this.route('error-page/404', { path: '/api' });
    this.route('terms-of-use');
    //this.route('error-page', {path: '/test'});
    this.route('error-page', function() {
      this.route('404');
      this.route('400');
      this.route('500');
      this.route('502');
    });

    //this.alias('api', '/api', 'news');

    //this.alias('news', '/news', 'api');
    //this.alias('api', '/api', 'news');

    //this.alias('news', '/news', 'api');


    this.route('currency', { path: '/:currency_id' });

  });
  this.route('change');
  this.route('notify');

  this.route('404');
  this.route('404', { path: '/api' });
  this.route('404', { path: '/*path' });
  //this.route('404', { path: '/*path' });

  //this.route("error-page", {path: '404'});
  //this.route('error-page', { path: '/*path' });
});

export default Router;
