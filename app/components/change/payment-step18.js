import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',

  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step18.js');
    const share = this.get('share');
    const me = this;

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step18 .orderId').text(' #' + share.getRequestId()); // #7141495
    //$('#form-step17 .payAmount').text(share.getPayInAmount()); // 100


    


    let sentAmount = this.get('share').getPayInAmount();
    $('#form-step18 .sentAmount').text(sentAmount + ' BTC');


    let cashIn = share.getCashIn();
    let controller = "";
    let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
    if (cont === 'change') {
      controller = Ember.getOwner(this).lookup('controller:change');
    } else {
      controller = Ember.getOwner(this).lookup('controller:lang/change');
    }

    $('#form-step18 .cashIn').text(i18n.t('cash.cashIn.' + cashIn)); // Credit/Debit cards
    $('#form-step18 .status').text(i18n.t('exchangerStatuses.' + share.getStatus())); // Completed

    //let cryptoAdress = share.getCryptoAdress();

    let cryptoAdress = share.getCryptoAdressIn();
    if (cryptoAdress === "") {
      controller.set('internalAccount', true);
    } else {
      //$('#form-step16 .cryptoAdress').text(cryptoAdress);
      //$('#form-step17 .block-2 > p').text(cryptoAdress); // to 1JDhmZWBp75Pq25tg97PRTWaxXd2Z8pLrP
      $('#form-step18 .fromCryptoAdress').text(
        i18n.t('exchangeOrder.step17.toAddress', { cryptoAdress: cryptoAdress })
      );
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');


    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    var amount = "";
    var shortName = "";
    if (currencyId) {
      shortName = currencyId;
      $('#form-step18 .payOutCurrencyId').text(currencyId); // LTC
      amount = share.getAmountAltToSend();
      $('#form-step18 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345  
    } else {
      shortName = share.getPayOutCurrency();
      $('#form-step18 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      amount = share.getPayOutAmount();
      $('#form-step18 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    $('#form-step18 .toCryptoAdress').text(
      i18n.t('exchangeOrder.step17.toAddress', { cryptoAdress: share.getCryptoAdress() })
    );
    //let wallet = share.getCryptoAdress();

    let receive = amount + ' ' + shortName; //i18n.t('exchangeOrder.step17.confirmation', { amount: amount, shortName: shortName });
    $('#form-step18 .receiveCryptoAdress').text(receive);
    
    let rate = (1 / sentAmount) * amount;
    $('#form-step18 .rate').text(
      i18n.t('exchangeOrder.step17.rate', { rate: rate, shortName: shortName })
    );

  }
});
