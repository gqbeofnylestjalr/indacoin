import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',

  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step16.js');
    const share = this.get('share');
    const me = this;

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step16 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step16 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step16 .payAmount').text(share.getPayInAmount()); // 100

    let cashIn = share.getCashIn();
    let controller = "";
    let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
    if (cont === 'change') {
      controller = Ember.getOwner(this).lookup('controller:change');
    } else {
      controller = Ember.getOwner(this).lookup('controller:lang/change');
    }

    $('#form-step16 .cashIn').text(i18n.t('cash.cashIn.' + cashIn)); // Credit/Debit cards
    $('#form-step16 .status').text(i18n.t('exchangerStatuses.' + share.getStatus())); // Completed

    //let cryptoAdress = share.getCryptoAdress();

    let cryptoAdress = share.getCryptoAdressIn();
    controller.set('qrcode', true);
    let iADhash = share.getiADhash();
    let link = "https://indacoin.com/tools/ImageProxy.ashx?hash=" + iADhash + "&imgurl=http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=" + cryptoAdress;
    $('#form-step16 .qrcode').attr('src', link);

    if (cryptoAdress === "") {
      controller.set('internalAccount', true);
    } else {
      //$('#form-step16 .cryptoAdress').text(cryptoAdress);
      $('#form-step16 .block-2 > p').text(cryptoAdress);
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');


    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    var amount = "";
    var shortName = "";
    if (currencyId) {
      shortName = currencyId;
      $('#form-step16 .payOutCurrencyId').text(currencyId); // LTC
      amount = share.getAmountAltToSend();
      $('#form-step16 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345  
    } else {
      shortName = share.getPayOutCurrency();
      $('#form-step16 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      amount = share.getPayOutAmount();
      $('#form-step16 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    let wallet = share.getCryptoAdress();

    let confirmation = i18n.t('exchangeOrder.changeCrypto.confirmation', { amount: amount, shortName: shortName, wallet: wallet });
    $('#form-step16 .block-4 > p').text(confirmation);
    
    // Copy cryptoaddress on click click
    if (cryptoAdress) {
      var copyLink = document.querySelector('#form-step16 .block-3 > span');
      copyLink.onclick = function (event) {

        controller.set('copiedToClipboard', true);
        console.log('click');

          var x = document.createElement("INPUT");
          x.setAttribute("type", "text");
          x.setAttribute("value", cryptoAdress);
          document.body.appendChild(x);
          x.select();
          document.execCommand('copy');
          x.style.display="none";
          if (!Element.prototype.remove) {
            Element.prototype.remove = function remove() {
              if (this.parentNode) {
                this.parentNode.removeChild(this);
              }
            };
          }
          x.remove();
          //controller.set('copiedToClipboard', false);
      }

      // copy -> copied to clipboard
    }

  }
});
