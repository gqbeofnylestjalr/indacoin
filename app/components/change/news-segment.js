import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),
    newsLinks: [],
    //didInitAttrs: function(){
    init: function(){
      const me = this;
      const i18n = me.get('i18n');
      let lang = this.get('i18n.locale');

      var newsLinks = [
        {
          text: i18n.t('news.topic1.full'),
          link: '#',
          date: i18n.t('news.topic1.date')
        },
        {
          text: i18n.t('news.topic2.full'),
          link: '#',
          date: i18n.t('news.topic2.date')
        },
        {
          text: i18n.t('news.topic3.full'),
          link: '#',
          date: i18n.t('news.topic3.date')
        }
      ];
      this.set('newsLinks', newsLinks);

      this._super(...arguments);
      this.get('handle');
    },
    didInsertElement: function(){
        /*var mySwiper = new Swiper('news-swiper-container', {
            navigation: {
              nextEl: '.swiper-button-next',
              prevEl: '.swiper-button-prev',
            },
        });*/

        /*let newsSegmentSwiper = new Swiper('#news-swiper-container', {
            nextButton: '.news-swiper-button-next',
            prevButton: '.news-swiper-button-prev',
            loop: true,
            centeredSlides: true,
            uniqueNavElements: true,
            spaceBetween: 100,
            preventClicks: false,
            preventClicksPropagation: false
        });*/
    },
    didUpdateAttrs() {
      this._super(...arguments);
      //console.log('didUpdateAttrs news-segment');
      //var s = document.querySelector('#news-swiper-container').swiper;
      //s.update();
    },
    willUpdate() {
      this._super(...arguments);
      //console.log('willUpdate news-segment');
      //var s = document.querySelector('#news-swiper-container').swiper;
      //s.update();
    },
    didRender() {
      this._super(...arguments);
      //console.log('didRender news-segment');
      //var s = document.querySelector('#news-swiper-container').swiper;
      //s.update();
    }
});
