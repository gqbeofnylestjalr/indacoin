import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step14.js');
    const share = this.get('share');
    const me = this;

    let shouldRedirect = share.getShouldRedirect();
    if (shouldRedirect) {
      $("#partnerBackLink a").attr('href', shouldRedirect);
      $("#partnerBackLink").show();
      share.startRedirectTimer(25, shouldRedirect);
    }

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step14 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step14 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step14 .payAmount').text(share.getPayInAmount()); // 100
    $('#form-step14 .payOutAmount').text(share.getPayOutAmount()); // 0.000345

    $('#form-step14 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards
    //$('#form-step14 .payOutCurrency').text(share.getPayOutCurrency()); // BTC (Bitcoin)
    $('#form-step14 .status').text(i18n.t('exchangerStatuses.Non3DS')); // Completed

    let cryptoAdress = share.getCryptoAdress();
    if (cryptoAdress === "") {
      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
      if (cont === 'change') {
        let controller = Ember.getOwner(this).lookup('controller:change');
        controller.set('internalAccount', true);
      } else {
        let controller = Ember.getOwner(this).lookup('controller:lang/change');
        controller.set('internalAccount', true);
      }
    } else {
      $('#form-step14 .cryptoAdress').text(cryptoAdress);
    }

    let altCurrencyId = this.get('share').getAltCurrencyId();
    if (altCurrencyId == '-1')
      $('#form-step14 .payOutCurrencyId').text(this.get('share').getPayOutCurrency()); // LTC
    else {
      let currencyId = "";
      this.get('share').getCurrency().forEach(function(item){
        if (item.cur_id === altCurrencyId) {
          currencyId = item.short_name;
        }
      });
      $('#form-step14 .payOutCurrencyId').text(currencyId); // LTC
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){
        var svgContainer = document.getElementById('svgContainer'); 
        var animItem = bodymovin.loadAnimation({  wrapper: svgContainer, animType: 'svg', loop: false, path: '/assets/exclude/scripts/' + i18n.t('lang') + '/part1.json' })
        .setSpeed(0.6);
      });

  },
  actions: {
    nextVerificationStep(){

    }
  }
});

