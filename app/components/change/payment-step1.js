import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  cookies: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    var haveEmail = false;
    var me = this;
    console.log('payment-step1.js');

    $('#form-step1')
      .form({
        on: 'change',
        fields: {
          email: {
           identifier: 'email',
           rules: [
             {
              type   : 'regExp',
              value   : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i,
               //prompt : 'Please enter valid email'
               prompt : i18n.t('invalidEmail').string
             }
           ]
         },
         outAddress: {
          identifier: 'outAddress',
          rules: [
            {
              // Ethereum 0x9Ca0e998dF92c5351cEcbBb6Dba82Ac2266f7e0C
              // Bitcoin 1GwV7fPX97hmavc6iNrUZUogmjpLPrPFoE
              type   : 'empty',
              prompt : i18n.t('invalidWallet').string
            }
          ]
        },
        giveCurAmount: {
         identifier: 'giveCurAmount',
         rules: [
          {
            type   : 'number',
            prompt : 'Please enter only numbers'
          }/*,
          {
            type   : 'regExp',
            value   : /^([3][5-9])$|^([3][5-9]\.\d{0,2})$|^([4-9][0-9])$|^([4-9][0-9]\.\d{0,2})$|^([1-9][0-9]{2})$|^([1-9][0-9]{2}\.\d{0,2})$|^1000$|^1000\.0{0,2}$/i, // 35-1000 or 56.34 
            prompt : 'Please enter correct number more than 35 and less than 1000'
          }*/
         ]
       },

       takeCurAmount: {
        identifier: 'takeCurAmount',
        rules: [
          {
            type   : 'number',
            prompt : 'Please enter only numbers'
          },
        ]
      }
      },
      onSuccess: function(event, fields){
        return false;
      },
      onFailure: function(){
        return false;
      },
      }
    );

    let step = this.get('step');
    let form = '#form-step' + step;
    let formData = this.get('formData');
    console.log(formData);
    step -= 1;
    $(form).form('set values', formData[step]);

    console.log('====== payment step1.js ========');

    if (typeof FastBoot === 'undefined') {

      // Special mark for partners via key 'InstantCoin' in 'User-Agent'
      let showSmartBanner = "";
      const userAgent = navigator.userAgent; // "Mozilla/5.0 (Linux; Android 8.0.0; FIG-LX1 Build/HUAWEIFIG-LX1; wv) AppleWebKit/537.36 (KHTML, like Gecko) Version/4.0 Chrome/71.0.3578.99 Mobile Safari/537.36 InstantCoin";
      if (userAgent) {
          if (userAgent.toLowerCase().indexOf('instantcoin') !== -1) {
              showSmartBanner = "InstantCoin";
          }
      }
      if (showSmartBanner) {
          $('#createWalletField-box').addClass('disabled');
      }
    } 

    // Загружаем поля USD и BTC
    let amountOut = this.get('share').getAmountOut();
    let wallet = this.get('share').getWallet();

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    if (this.get("share").getCouponeCheckBox()) {
      console.log('getCouponeCheckBox true');
      $('#couponeCheckField').checkbox('check');
      Ember.$('#couponeCode').show();
    }
    else {
      console.log('getCouponeCheckBox false');
      $('#couponeCheckField').checkbox('uncheck');
      Ember.$('#couponeCode').hide();
    }

    if (this.get("share").getAcceptAgreementCheckBox()) {
      $('#acceptAgreementField').checkbox('check');
    }
    else {
      $('#acceptAgreementField').checkbox('uncheck');
    }

    if (this.get("share").getCreateWalletCheckBox()) {
      $('#createWalletField').checkbox('check');
      $('#exchangeOutAddress').prop('disabled', true);
      $("#exchangeOutAddress").val('');
    }
    else {
      $('#createWalletField').checkbox('uncheck');
      $('#exchangeOutAddress').prop('disabled', false);

      if (wallet !== null) {
        $("#exchangeOutAddress").val(wallet);
        $('#exchangeOutAddress').prop('disabled', false);
      }
    }
    
    let force = this.get('share').getForce();
    if (force !== null) {
      $('#cur-dropdown').addClass("disabled");
      $('#exchange-take').prop('disabled', true);
      $('#exchange-give').prop('disabled', true);
      $('#exchangeOutAddress').prop('disabled', true);
      $('#giveCur-select .dropdown').addClass("disabled");

      


      // const amountPay = me.get("share").getAmountPay();
      // const amountNum = me.get("share").getAmountNum();
      // if (amountPay) {
      //   $('#exchange-give').val(amountPay);
      // }
      // else if (amountNum) {
      //   $('#exchange-take').val(amountNum);
      // }




      if (wallet !== null) {
        $('#createWalletField-box').addClass('disabled');
      }
    } else {
      //$('#cur-dropdown').removeClass("disabled");
    }

    let email = this.get('share').getEmail();
    if (email !== null) {
      $("#exchangeFormEmail").val(email);
      $('#exchangeFormEmail').prop('disabled', true);
    }

    let tag = this.get('share').getRippleTag();
    if (tag)
      $("#exchangeRippleTag").val(tag);

  }
});
