import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  tagName: '',
  store: Ember.inject.service(),
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step8.js');
    const share = this.get('share');
    const me = this;

    let shouldRedirect = share.getShouldRedirect();
    if (shouldRedirect) {
      $("#partnerBackLink a").attr('href', shouldRedirect);
      $("#partnerBackLink").show();
      share.startRedirectTimer(25, shouldRedirect);
    }

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step8 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step8 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step8 .payAmount').text(share.getPayInAmount()); // 100
    //$('#form-step8 .payOutAmount').text(share.getPayOutAmount()); // 0.000345

    let cashIn = share.getCashIn();
    let controller = "";
    let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
    if (cont === 'change') {
      controller = Ember.getOwner(this).lookup('controller:change');
    } else {
      controller = Ember.getOwner(this).lookup('controller:lang/change');
    }

    $('#form-step8 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards
    $('#form-step8 .status').text(i18n.t('exchangerStatuses.' + share.getStatus())); // Completed

    let cryptoAdress = share.getCryptoAdress();
    if (cashIn == 10) {
      cryptoAdress = share.getCryptoAdressIn();
    }

    if (cryptoAdress === "") {
      controller.set('internalAccount', true);
    } else {
      $('#form-step8 .cryptoAdress').text(cryptoAdress);
    }

    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step8 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step8 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345
    } else {
      $('#form-step8 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step8 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){
      var svgContainer = document.getElementById('svgContainer'); 
      var animItem = bodymovin.loadAnimation({  wrapper: svgContainer, animType: 'svg', loop: true, path: '/assets/exclude/scripts/' + i18n.t('lang') + '/buy_coins_repeat.json' })
      .setSpeed(0.75);
    });

  }
});
