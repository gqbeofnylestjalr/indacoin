import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  tagName: '',
  didInsertElement: function(){
    console.log('Call payment-step4.js');
    const me = this;

    
    // Переход на страницу с платежки
    if (this.get('share').getRedirectFromBank()) {
      console.log('Пришли с банка');

      // Запрашиваем информацию о платеже и устанавливаем параметры страницы
      //notify(this.get('share').getRequestId(), this.get('share').getConfirmCode());
   
      // ============ Устанавливаем шаг 5 ============

      /*this.set('step4', false);
      this.set('step5', true);
      this.set('step1', false);
      this.set('step', 5);
      this.set('returned', true);
      

      $('#payment-steps div.step').addClass('disabled');
      $('#payment-steps div.active').removeClass('active');
      $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
      $('#nextStepBtn').show();
      $('#prevStepBtn').hide();*/



      // ============ Устанавливаем шаг 4 ============
      $('#payment-steps div.step').addClass('disabled');
      $('#payment-steps div.active').removeClass('active');
      $('#payment-steps div.step:nth-child(4)').addClass('active').removeClass('disabled');
      $('#nextStepBtn').show();
      $('#prevStepBtn').hide();

      // Анализируем код для выставления подтверждений шага 4
      /*this.set('step4_1', true); // Код с телефона
      this.set('step4_2', false); // Код для верификации карты
      this.set('step4_3', false); // Просмотр видео
      this.set('step4_4', false);
      this.set('step4_5', false);*/

    }
    else {
      console.log('Пришли не с банка');
      $('#prevStepBtn').show();
    }

    $('#nextStepBtn').hide();
    $('#cardNumber-step').hide();

    /*let returned = this.get('returned');
    console.log('returned = ' + returned);
    let requestId = this.get('share').getRequestId();
    let confirmCode = this.get('share').getConfirmCode();*/

    function notify(requestId, confirmCode) {
        console.log('requestId = ' + requestId);
        console.log('confirmCode = ' + confirmCode);
        //var requestId = '7139084';
        //var confirmCode = 'f53c3D9167';
        //https://indacoin.com/gw/payment_status?request_id=7139084&confirm_code=f53c3D9167
        $.ajax({
          async: false,
          method: 'POST',
          contentType: 'application/json; charset=utf-8',
          url: "/Notify/getData",
          //data: "{'requestId':'" + getParamFromUrl("request_id") + "','confirmCode':'" + getParamFromUrl("confirm_code") + "'}",
          data: "{'requestId':'" + requestId + "','confirmCode':'" + confirmCode + "'}",
          success: function onSucess(result) {
            console.log("RefreshGlobalData success");
            var NotifyData = $.parseJSON(result.d);
            window.NotifyData = NotifyData;

            let exchangerStatuses = {
              BillWaiting: "Awaiting bill to be payed",
              CashinWaiting: "Your payment is being processed now, which should take up to 2 minutes. If 2 minutes has passed and you still see this status, unfortunately your payment has been declined. You need to check with your bank that your card does support 3D-Secure (Verified by Visa or Mastercard Securecode), meaning that you always enter a secret code/password tied to the card, when pay over the internet. Sometimes the code could be sent to your mobile phone number.",
              Completed: "Completed",
              Declined: "Declined",
              Error: "Error was occured, please contact Indacoin support support@indacoin.com",
              MoneySend: "The funds have been sent",
              Processing: "In progress",
              TimeOut: "Order timeout",
              Verifying: "Verifying",
              WaitingForAccountCreation: "Waiting for Indacoin account creation (we have sent to you a registration mail)",
              cardDeclined: "Your card was declined, if you have questions, please contact Indacoin support support@indacoin.com",
              cardDeclinedNoFull3ds: "Declined, because your bank doesn't support full 3ds (only Attempted) option on this card, please contact your bank customer service."
            };

            let cashIn = {
              1: "Credit/Debit cards",
              2: "Russian pay terminals",
              3: "QIWI",
              4: "Bashkomsnabbank",
              5: "Novoplat terminals",
              6: "Elexnet terminals",
              7: "Credit/Debit cards (USD)",
              8: "Alfa-click",
              9: "Wire transfer",
              10: "Bitcoin",
              12: "Litecoin",
              13: "Astropay",
              15: "QIWI",
              16: "Credit/Debit cards (USD)",
              17: "Pinpay",
              18: "Payeer",
              19: "Liqpay",
              20: "Yandex.Money",
              21: "Elexnet",
              22: "Kassira.net",
              23: "Mobil element",
              24: "Svyaznoy",
              25: "Euroset",
              26: "PerfectMoney",
              27: "IndacoinInternal",
              29: "Yandex.Money",
              30: "OKPay",
              31: "Referral",
              33: "Payza",
              35: "BTC-E code",
              36: "Credit/Debit cards (USD)",
              37: "Credit/Debit cards (USD)",
              39: "Wire transfer",
              40: "Yandex.Money",
              42: "QIWI(Handmade)",
              43: "UnionPay",
              44: "QIWI(Automatic)",
              45: "Mobile commerce(Russia only)",
              49: "LibrexCoin",
              50: "Credit/Debit cards (EUR)",
              51: "QIWI(Automatic)", 
              53: "Ethereum"
            };

            let cashOut = {
              1: "Webmoney",
              2: "Yandex.Money",
              3: "Beeline",
              4: "Megafon",
              5: "MTS",
              6: "Wire transfer",
              7: "Credit/Debit Cards (Russia)",
              8: "Bank cards (Ukraine)",
              9: "Bank cards (PrivateBank)",
              10: "Bitcoin",
              12: "Litecoin",
              15: "Cashier",
              16: "PerfectMoney",
              18: "Payeer",
              19: "Indacoin internal",
              20: "OKPay",
              23: "Bank cards (World)",
              24: "QIWI",
              25: "TELE2",
              26: "Payza",
              28: "Credit/Debit Cards (All countries)",
              29: "Credit/Debit Cards (Tinkoff & Avangard)",
              30: "BTCe Code",
              31: "LibrexCoin",
              32: "Credit/Debit Cards (Russia, CIS)",
              33: "Yandex.Money",
              34: "Ethereum"
            };

            // Parse request
            if (NotifyData.s == "TimeOut") {
              // Вывод сообщения
            }

            let exTransactionId = NotifyData.ex_transaction_id;
            let payDate = NotifyData.d;
            let orderId = NotifyData.id;
            let status = exchangerStatuses[NotifyData.s] ? exchangerStatuses[NotifyData.s] : NotifyData.s;
            var someHelpShown = false;

            function PrepareForPhoneAuth( NotifyData) {
              console.log('SMS-верификация');
            }

            if (NotifyData.s == "Verifying" || NotifyData.s == "Declined") {

              // https://indacoin.com/notify?confirm_code=2Fcd930B00&request_id=7141439
              if (NotifyData.cardStatus && NotifyData.cardStatus != "" && NotifyData.cardStatus == "Declined") {
                if (NotifyData.card3DS && NotifyData.card3DS == "Half3Ds")
                  console.log('1 - Ошибка 3Ds');
                else if (NotifyData.verifyStatusAddInfo && NotifyData.verifyStatusAddInfo == "bank_rejected") {
                  console.log('2 - card rejected');
                } else {
                  console.log('3 - card declined');
                }
              } else 
                if (NotifyData.iT == 50 || NotifyData.iT == 16) {
                  if (NotifyData.phoneStatusAuthCode == "Verifying") {
                    PrepareForPhoneAuth(NotifyData);
                    someHelpShown = true;
                  } else if (NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying") {
                    //


                  }
                } else
                if (NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying") {

                } else if (NotifyData.phoneStatusAuthCode == "Verifying") {
                  PrepareForPhoneAuth(NotifyData);
                  someHelpShown = true;
                }
                if (NotifyData.s == "Verifying" && !someHelpShown) {

                }
            }
            if (!someHelpShown) {

            }
            if (NotifyData.direction == 0) {

            }
            if (NotifyData.video_verification_id < 0) {
              // Записать видео
            }

            // Поле Pay amount
            let payInAmount = NotifyData.iA; // "50"
            let payInSystem = cashIn[NotifyData.iT]; // "Credit/Debit cards (EUR)""
            let payInCurrency = NotifyData.iC; // "USD"

            let payOutAmount = NotifyData.oA; // 0.008679
            let payOutCurrency = NotifyData.oC; // BTC
            let payOutSystem = cashOut[NotifyData.oT]; // Bitcoin

            if (NotifyData.cashoutExternalTransactionId && NotifyData.cashoutExternalTransactionId != "") {
              // Добавляем поле "Внешняя транзакция"
            }

            if ((NotifyData.iT == 51 || NotifyData.iT == 52) && NotifyData.s == "Verifying" && NotifyData.phoneStatusAuthCode != "Verifying") {
              // Платежка QIWI
            }

            if ((NotifyData.iC == "BTC" || NotifyData.iC == "LTC" || NotifyData.iC == "LXC" || NotifyData.iC == "ETH") && NotifyData.s == "CashinWaiting") {
              // Поле с QR-кодом
            }

            if (NotifyData.s == "Completed" || NotifyData.s == "MoneySend") {
              // Показ социальный кнопок
            }





              //console.log(NotifyData);
          },
          error: function() {
              console.log("RefreshGlobalData error");
          }
        });
      }
    







    $('#form-step4 input[name=phoneCode]').mask('0000', {
      onComplete: function(){
        $('#form-step4').form('validate form');
        if($('#form-step4').form('is valid')){
          $('#phone-step').hide();
          $('#cardNumber-step').show();
          //$('#video-step').show();
          $('#video-step').hide();
          //$('#nextStepBtn').show();
          $('#nextStepBtn').hide();
        } else {
          $('#form-step4').transition('shake');
        }
      }
    });

    /*$('#form-step4-2 input[name=cardVerificationCode]').mask('000000', {
      onComplete: function(){
        $('#form-step4-2').form('validate form'); // Валидация ниже
        if($('#form-step4-2').form('is valid')){
          $('#phone-step').hide();
          $('#cardNumber-step').hide();
          $('#video-step').show();
          $('#nextStepBtn').show();
        } else {
          $('#form-step4-2').transition('shake');
        }
      }
    });*/





    $('#form-step4-2')
    .form({
      on: 'change',
      fields: {
        phoneCode: {
         identifier: 'cardVerificationCode',
         rules: [
           {
             type   : 'regExp',
             value  : /^[0-9]{6}$/i,
             prompt : 'Please enter payment code'
           }
         ]
       },
    },
    onSuccess: function(event, fields){
      console.log('cardVerificationCode success');
      return false;
    },
    onFailure: function(){
      console.log('cardVerificationCode failure');
      return false;
    },
    }
  );

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');
  },
  actions: {
    nextVerificationStep(){
      console.log('nextVerificationStep click');
      $('#form-step4').form('validate form');
      if($('#form-step4').form('is valid')){
        $('#phone-step').hide();
        $('#video-step').show();
        $('#nextStepBtn').show();
      } else {
        $('#form-step4').transition('shake');
      }
    },
    nextVerificationStep2(){
      console.log('nextVerificationStep2 click');
      $('#form-step4-2').form('validate form');
      if($('#form-step4-2').form('is valid')){
        $('#phone-step').hide();
        $('#video-step').show();
        $('#nextStepBtn').show();
      } else {
        $('#form-step4-2').transition('shake');
      }
    }
  }
});
