import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step5.js');
    const share = this.get('share');
    const me = this;
    //this.set('videoVerificationPassed', false);
    console.log('videoVerificationPassed = ' + this.get('videoVerificationPassed'));
    //share.setShouldRedirect('https://spheroiduniverse.io/marketplace/purchase/success/?transaction_id=545897');

    let shouldRedirect = share.getShouldRedirect();
    if (shouldRedirect) {
      $("#partnerBackLink a").attr('href', shouldRedirect);
      $("#partnerBackLink").show();
      share.startRedirectTimer(251, shouldRedirect);
    }

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом completed
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    //console.log('order id = ' + share.getRequestId());
    //debugger;
    $('#form-step5 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step5 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step5 .payAmount').text(share.getPayInAmount()); // 100

    $('#form-step5 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards


    $('#form-step5 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    //$('#form-step5 .payOutCurrency').text(share.getPayOutCurrency()); // BTC
    //$('#form-step5 .payOutSystem').text( i18n.t('cash.cashIn.' + share.getPayOutSystem() )); // (Bitcoin)



    // Show or hide link to check transaction
    let controller = Ember.getOwner(this).lookup('controller:lang/change');
    if (share.getTransactionId() === null)
      controller.set('haveTransactionId', false);
    else
      controller.set('haveTransactionId', true);

    $('#form-step5 .status').text(i18n.t('exchangerStatuses.Completed')); // Completed

    let cryptoAdress = share.getCryptoAdress();
    if (cryptoAdress === "") {
      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
      if (cont === 'change') {
        let controller = Ember.getOwner(this).lookup('controller:change');
        controller.set('internalAccount', true);
      } else {
        let controller = Ember.getOwner(this).lookup('controller:lang/change');
        controller.set('internalAccount', true);
      }
    } else {
      $('#form-step5 .cryptoAdress').text(cryptoAdress);
    }

    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step5 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step5 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345
    } else {
      $('#form-step5 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step5 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){
      var svgContainer = document.getElementById('svgContainer'); 
      var animItem = bodymovin.loadAnimation({  wrapper: svgContainer, animType: 'svg', loop: true, path: '/assets/exclude/scripts/' + i18n.t('lang') + '/verification.json' })
      .setSpeed(0.75);
    });

  }
});
