import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step9.js');
    const share = this.get('share');
    const me = this;

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step9 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step9 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step9 .payAmount').text(share.getPayInAmount()); // 100
    //$('#form-step9 .payOutAmount').text(share.getPayOutAmount()); // 0.000345

    let cashIn = share.getCashIn();
    let controller = "";
    let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
    if (cont === 'change') {
      controller = Ember.getOwner(this).lookup('controller:change');
    } else {
      controller = Ember.getOwner(this).lookup('controller:lang/change');
    }

    $('#form-step9 .cashIn').text(i18n.t('cash.cashIn.' + cashIn)); // Credit/Debit cards
    $('#form-step9 .status').text(i18n.t('exchangerStatuses.' + share.getStatus())); // Completed

    let cryptoAdress = share.getCryptoAdress();
    if (cashIn == 10) {
      cryptoAdress = share.getCryptoAdressIn();
      controller.set('qrcode', true);
      let iADhash = share.getiADhash();
      let link = "https://indacoin.com/tools/ImageProxy.ashx?hash=" + iADhash + "&imgurl=http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=" + cryptoAdress;
      //link = "https://indacoin.com/tools/ImageProxy.ashx?hash=jQGi7&imgurl=http://chart.apis.google.com/chart?cht=qr&chs=150x150&chl=13heUiw42UycaF5aoRoMVKuedxipHybQ6w";
      $('#form-step9 .qrcode').attr('src', link);
    }

    if (cryptoAdress === "") {
      controller.set('internalAccount', true);
    } else {
      $('#form-step9 .cryptoAdress').text(cryptoAdress);
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');


    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step9 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step9 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345  
    } else {
      $('#form-step9 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step9 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }


  }
});

