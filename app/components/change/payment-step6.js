import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  tagName: '',
  store: Ember.inject.service(),
  didInsertElement: function(){
    this._super(...arguments);
    console.log('Call payment-step6.js');
    const me = this;
    const share = me.get('share');
    const i18n = me.get('i18n');

    // ============ Устанавливаем шаг 4 ============
    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(4)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    $('#form-step6 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step6 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step6 .payAmount').text(share.getPayInAmount()); // 100
    $('#form-step6 .payOutAmount').text(share.getPayOutAmount()); // 0.000345

    $('#form-step6 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards
    //$('#form-step6 .payOutCurrency').text(share.getPayOutCurrency()); // BTC (Bitcoin)

    $('#form-step6 .status').text(i18n.t('exchangerStatuses.Verifying'));

    let cryptoAdress = share.getCryptoAdress();
    if (cryptoAdress === "") {
      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
      if (cont === 'change') {
        let controller = Ember.getOwner(this).lookup('controller:change');
        controller.set('internalAccount', true);
      } else {
        let controller = Ember.getOwner(this).lookup('controller:lang/change');
        controller.set('internalAccount', true);
      }
    } else {
      $('#form-step6 .cryptoAdress').text(cryptoAdress);
    }

    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step6 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step6 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345
    } else {
      $('#form-step6 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step6 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    let cur = currencyId || share.getPayOutCurrency();
    if (cur === '433') {
      $('#soccerLegends').removeClass('display_none');
      $('#form-step6 .KYCVerification').addClass('disabled');
    }

    // Loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    let KYCUrl = share.getKYCUrl();
    if (KYCUrl) {
      $('#step6-left-button').css("display", "block");
      console.log('get true KYCUrl from payment-step6: ' + KYCUrl);
    }

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){
      var svgContainer = document.getElementById('svgContainer'); 
      var animItem = bodymovin.loadAnimation({  wrapper: svgContainer, animType: 'svg', loop: false, path: '/assets/exclude/scripts/' + i18n.t('lang') + '/error_verification.json' })
      .setSpeed(0.75);
    });

  },
  actions: {
    nextVerificationStep(){

    }
  },
  init: function() {
    this._super(...arguments);

    const me = this;

    let KYCUrl = me.get('share').getKYCUrl();
    if (KYCUrl === null)
    if (typeof FastBoot === 'undefined') {
      console.log('Отслеживание изменения статуса запущено');
      // TODO: проверять аналогично изменение страницы, если пользователь перейдет на другую страницу
      var lastStatus = "";
      var lastPage = window.location.href;
      var timerId = setInterval(function() {
      let requestId = me.get('share').getRequestId();
      let confirmCode = me.get('share').getConfirmCode();
      let page = window.location.href;
      $.ajax({
        async: false,
        method: 'POST',
        contentType: 'application/json; charset=utf-8',
        url: "/Notify/getData",
        data: "{'requestId':'" + requestId + "','confirmCode':'" + confirmCode + "'}",
        success: function onSucess(result) {
          
          let NotifyData = $.parseJSON(result.d);
          let status = NotifyData.s;
          let kyc = NotifyData.KYCUrl;

          console.log('iT = ' + NotifyData.iT);
          console.log('kyc = ' + kyc);

          console.log('Получен статус: ' + status + ' Прошлый статус: ' + lastStatus + ' iT = ' + NotifyData.iT);
          console.log('Прошлая страница: ' + page + '\nТекущая страница: ' + lastPage);

          if (status === "Completed" || status === "TimeOut") {
            clearInterval(timerId);
            console.log('Script is stopped by status: ' + status);
            return;
          }

          // Stop timer if page is changed
          if (kyc) {
            clearInterval(timerId);
            $('#step6-left-button').css("display", "block");
            return;
          }

          // Stop timer if page is changed
          if (page !== lastPage) {
            console.log('Script is stopped by page');
            clearInterval(timerId);
          }

          // If status changed then reload page
          if (lastStatus !== status)
            window.location.reload();


          lastStatus = status;
        }
      });
      }, 3000);
    }










    
  }
});
