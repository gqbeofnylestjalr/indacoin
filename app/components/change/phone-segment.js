import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
    ENV: ENV,
    didInsertElement: function(){
        const i18n = this.get('i18n');
        console.log('Call phone-segment.js');
        const share = this.get('share');
        const me = this;


        var lastPage = window.location.href;
        var page = "";
        var timerId = setInterval(function() {

            // Stop timer if page is changed
            page = window.location.href;
            if (page !== lastPage) {
                console.log('Script is stopped by page');
                clearInterval(timerId);
            } else {
            
                let img1 = $('#phone-segment-pic1');
                let link1 = img1.attr("src");
                if (link1.slice(-5, -4) === '1')
                    img1.attr("src", link1.slice(0, -5) + '2' + link1.slice(-4));
                else
                    img1.attr("src", link1.slice(0, -5) + '1' + link1.slice(-4));

                let img2 = $('#phone-segment-pic2');
                let link2 = img2.attr("src");
                if (link2.slice(-5, -4) === '3')
                    img2.attr("src", link2.slice(0, -5) + '4' + link2.slice(-4));
                else
                    img2.attr("src", link2.slice(0, -5) + '3' + link2.slice(-4));
            }

        }, 3000);


    }
});
