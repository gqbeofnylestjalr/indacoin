import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
  ENV: ENV,
  i18n: Ember.inject.service(),


  title: Ember.computed('ENV.i18n.lang', function(){
    const i18n = this.get('i18n');
    let count = "<span class='highlight'>100</span>";
    let name = "<span class='highlight'>Indacoin</span>";
    return Ember.String.htmlSafe(i18n.t('cryptocurrencies-segment.title', { count: count, name: name }));
  }),

  didInsertElement: function(){

    /*let tag = "#crypto";
    for (let i = 1; i <= 10; i++)
      $(tag + i).hover(function() {
          $(this).transition('pulse');
      });*/
    /*$('#crypto1')
    .popup({
      target   : '#crypto1',
      content  : 'Ethereum',
      delay: {
        show: 50,
        hide: 5000000
      }
    });

    $('#crypto2')
    .popup({
      target   : '#crypto2',
      content  : 'Bitcoin',
      delay: {
        show: 50,
        hide: 5000000
      }
    });

    $('#crypto3')
    .popup({
      target   : '#crypto3-popup',
      content  : 'Dash'
    });*/

    /*$('#crypto1')
    .popup({
      delay: {
        show: 100,
        hide: 10000
      },
      content  : 'Bitcoin',
      variation : 'cryptocurrencies-segment-popup',
      position : 'top left'
    });

    $('#crypto2')
    .popup({
      delay: {
        show: 50,
        hide: 5000000
      },
      content  : 'Ethereum',
      variation : 'cryptocurrencies-segment-popup',
      position : 'top left'
    });

    $('#crypto3')
    .popup({
      delay: {
        show: 50,
        hide: 50
      },
      content  : 'Dash',
      variation : 'cryptocurrencies-segment-popup',
      position : 'top left'
    });*/

  }
});

/*

<div class="ui popup transition visible bottom left"
  style="top: 3042.58px; bottom: auto; left: 238.5px; right: auto; display: block !important;">
<div class="content">Ethereum</div></div>

*/