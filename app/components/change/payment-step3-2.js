import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  tagName: '',
  didInsertElement: function(){

    console.log('call mayment-step3-2.js');

    // Скрываем телефон
    

    // Подгрузка имени с карты
    let cardName = "";
    if (this.get('formData')[1] !== undefined) {
      if (this.get('formData')[1].cardName !== undefined && this.get('share').getAmountOut().toLowerCase() === 'usd') {
        cardName = this.get('formData')[1].cardName;
        $("#fullName").val(cardName);
      }
    }
    
    // Задаем телефонный формат по региону
    let region = this.get('share').getRegion().toLowerCase();
    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.1/js/intlTelInput.min.js').done(function(script, textStatus){
      $("#form-step3 input[name=phone]").intlTelInput({
        initialCountry: region,
        utilsScript: 'https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/12.0.1/js/utils.js'
      });
      $("#form-step3").removeClass('loading');
    });


    $('#form-step3-2')
      .form({
        on: 'change',
        fields: {
          fullName: {
           identifier: 'fullName',
           rules: [
             {
               type   : 'regExp[/^[a-zA-Z-]{3,16} [a-zA-Z-]{3,16}/]',
               prompt : 'Please enter valid fullname'
             }
           ]
         },
        birthday: {
         identifier: 'birthday',
         rules: [
           {
             type   : 'regExp[/^([0][0-9]|[1][0-2]).([0-2][0-9]|[3][0-1]).([1][9][0-9][0-9]|[2][0][0-1][0-7])$/]',
             prompt : 'Please enter valid birth date. Format: MM.DD.YYYY'
           }
         ]
       }
      },
      onSuccess: function(event, fields){
        return false;
      },
      onFailure: function(){
        return false;
      },
      }
    );
    let step = this.get('step');
    let form = '#form-step' + step;
    let formData = this.get('formData');
    step -= 1;
    $(form).form('set values', formData[step]);
  }
});
