import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step10.js');
    const share = this.get('share');
    const me = this;

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // Страница со статусом orderTimeOut
    // Проверяем наличие в share принятых полей и выставляем поля таблицы
    $('#form-step10 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step10 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step10 .payAmount').text(share.getPayInAmount()); // 100
    //$('#form-step10 .payOutAmount').text(share.getPayOutAmount()); // 0.000345

    $('#form-step10 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards
    //$('#form-step9 .payOutCurrency').text(share.getPayOutCurrency()); // BTC (Bitcoin)

    $('#form-step10 .status').text(i18n.t('exchangerStatuses.Processing'));

    let cryptoAdress = share.getCryptoAdress();
    if (cryptoAdress === "") {
      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
      if (cont === 'change') {
        let controller = Ember.getOwner(this).lookup('controller:change');
        controller.set('internalAccount', true);
      } else {
        let controller = Ember.getOwner(this).lookup('controller:lang/change');
        controller.set('internalAccount', true);
      }
    } else {
      $('#form-step10 .cryptoAdress').text(cryptoAdress);
    }

    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step10 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step10 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345
    } else {
      $('#form-step10 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step10 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){
      var svgContainer = document.getElementById('svgContainer'); 
      var animItem = bodymovin.loadAnimation({  wrapper: svgContainer, animType: 'svg', loop: true, path: '/assets/exclude/scripts/' + i18n.t('lang') + '/buy_coins_repeat.json' })
      .setSpeed(0.75);
    });

  }
});