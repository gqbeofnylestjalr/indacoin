import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
        linkToBuy(){
            $('html, body').animate({
                scrollTop: $("#steps-content").offset().top
            }, 1500);
        }
      }
});
