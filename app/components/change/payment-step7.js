import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){

    console.log('Call payment-step7.js');
    const me = this;
    const share = me.get('share');
    const i18n = me.get('i18n');

    function resendCodeTimer() {
      $('#resendPhoneCode').show(); // Кнопка для повторной отправки кода
    }
    setTimeout(resendCodeTimer, 30000); // 30 sec

    // ============ Устанавливаем шаг 4 ============
    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(4)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();


    // Страница со статусом orderTimeOut
    $('#form-step7 .orderId').text(share.getRequestId()); // Exchange order #7141495
    $('#form-step7 .orderDate').text(share.getPayDate()); // 19.09.2017 09:33:43
    $('#form-step7 .payAmount').text(share.getPayInAmount()); // 100

    $('#form-step7 .cashIn').text(i18n.t('cash.cashIn.' + share.getCashIn())); // Credit/Debit cards
    $('#form-step7 .payOutCurrency').text(share.getPayOutCurrency()); // BTC (Bitcoin)
    $('#form-step7 .status').text(i18n.t('exchangerStatuses.' + share.getStatus())); // Completed

    $('#form-step7 .phone').text(i18n.t('exchangeOrder.your-phone', { phone: share.getPhoneNumber() }));

    // Задаем телефонный формат по региону
    let region = this.get('share').getRegion().toLowerCase();
    let excludeCountries = ['ir', 'us', 'jp'];
    if (excludeCountries.indexOf(region) !== -1)
      region = 'gb';
    
    $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/intlTelInput.js').done(function(script, textStatus){
      $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js').done(function(script, textStatus){

        var input = document.querySelector("#form-step7 input[name=phone]");
        window.itiVerificationStep = window.intlTelInput(input, {
            initialCountry: region,
            excludeCountries: excludeCountries
      });
    })
    });

    let altCurrencyId = this.get('share').getAltCurrencyId();
    let currencyId = false;
    this.get('store').peekAll('value').forEach(function(item){
      if (item.get('data').cur_id === altCurrencyId) {
        currencyId = item.get('data').short_name;
      }
    })
    if (currencyId) {
      $('#form-step7 .payOutCurrencyId').text(currencyId); // LTC
      $('#form-step7 .payOutAmount').text(share.getAmountAltToSend()); // 0.000345
    } else {
      $('#form-step7 .payOutCurrencyId').text(share.getPayOutCurrency()); // LTC
      $('#form-step7 .payOutAmount').text(share.getPayOutAmount()); // 0.000345
    }

    let cryptoAdress = share.getCryptoAdress();
    if (cryptoAdress === "") {
      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // change lang.change
      if (cont === 'change') {
        let controller = Ember.getOwner(this).lookup('controller:change');
        controller.set('internalAccount', true);
      } else {
        let controller = Ember.getOwner(this).lookup('controller:lang/change');
        controller.set('internalAccount', true);
      }
    } else {
      $('#form-step7 .cryptoAdress').text(cryptoAdress);
    }

    $('#form-step7 input[name=phoneCode]').mask('0000', {
      onComplete: function(){
        
        $('#form-step7').form('validate form');
        if($('#form-step7').form('is valid')){
          $('#nextStepBtn').hide();
        } else {
          $('#form-step7').transition('shake');
        }
      }
    });

    $('#form-step7')
      .form({
        on: 'change',
        fields: {
          phoneCode: {
           identifier: 'phoneCode',
           rules: [
             {
               type   : 'regExp',
               value  : /^[0-9]{4}$/i,
               prompt : 'Please enter valid verification code'
             }
           ]
         },
      },
      onSuccess: function(event, fields){
        console.log('new phoneCode success');
        return false;
      },
      onFailure: function(){
        console.log('new phoneCode failure');
        return false;
      },
      }
    );
    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');
  },
  actions: {


    nextVerificationStep(){
      $('#form-step7').form('validate form');
      if($('#form-step7').form('is valid')){
        //$('#phone-step').hide();
        //$('#video-step').show();
        //$('#nextStepBtn').show();
      } else {
        $('#form-step7').transition('shake');
      }
    },
    prevStep7(){
        console.log('prevStep7');
    },
    nextStep7(){
        console.log('nextStep7');
        const me = this;

        let phone_id = this.get('share').getPhoneId();
        let authSMSCode = $("#authSMSCode").val();
        let confirm_code = this.get('share').getConfirmCode();
        let request_id = this.get('share').getRequestId();
        
        function checkSMSCode() {
          $.ajax({
              url: "/notify/CheckPhoneAuthCode",
              contentType: 'application/json; charset=utf-8',
              method: 'POST',
              async: false,
              data: "{'phoneId':'" + phone_id + "','authCode':'" + authSMSCode + "','confirmation_hash':'" + confirm_code + "','request_id':'" + request_id + "'}",
              success: function(n) {
                if (n.d.res == "1" || n.d.res == "true") {

                }
                  /*n.d.res == "1" || n.d.res == "true" ? window.location.reload() : ($("#authSMSCode").siblings(".tip").html(window.t.wrongSMSAuthCode).show(),
                  setTimeout(function() {
                      window.location.reload()
                  }, 5e3))*/
              },
              error: function() {
                console.log('Переходим к верификации по видео');
                me.set('step1', false);
                me.set('step7', false);
                me.set('step6', true);
                me.set('step', 6);
                me.set('returned', true);
              }
          })
        }
        checkSMSCode();
    }
  }
});
