import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  tagName: '',
  didInsertElement: function(){
    const me = this;
    
    // Подгрузка имени с карты
    let currency = this.get('share').getAmountOut().toLowerCase(); // usd

    // If user email is identified and currency type is EUR then hide name input
    let userIdent = me.get('share').getUserIdent();
    if (userIdent && currency === 'eur') {
      $('#form-step3 input[name=cardName]').hide();
    }
    else {
      $('#form-step3 input[name=cardName]').show();
      $('#form-step3 input[name=cardName]').bind('keyup',function(){
        $(this).val($(this).val().replace(/[^a-zA-Z-\s]/i, ""))
      });
    }

    $('#form-step3 input[name=cardNumber]').mask('0000  0000  0000  0000', {
      onComplete: function(){
        $('#form-step3 input[name=cardExpMM]').focus();
      }
    });
    $('#form-step3 input[name=cardExpMM]').mask('00', {
      onComplete: function(){
        $('#form-step3 input[name=cardExpYY]').focus();
      }
    });
    $('#form-step3 input[name=cardExpYY]').mask('00', {
      onComplete: function(){
        $('#form-step3 input[name=cardCVV]').focus();
      }
    });
    $('#form-step3 input[name=cardCVV]').mask('000', {
      onComplete: function(){
        $('#form-step3 input[name=cardName]').focus();
      }
    });

    $.getScript('https://cdnjs.cloudflare.com/ajax/libs/jquery.payment/3.0.0/jquery.payment.min.js').done(function(script, textStatus){
      
      $.fn.form.settings.rules.ruleForCardNumber = function(param) {
        return $.payment.validateCardNumber(param);;
      }

      $.fn.form.settings.rules.ruleForCardCVC = function(param) {
        return $.payment.validateCardCVC(param);;
      }

      $.fn.form.settings.rules.ruleForCardExpYY = function(param) {
        if (param < 18 || param > 28)
          return false;
        else
          return true;
      }
      

      $('#form-step3')
      .form({
        on: 'change',
        fields: {
          cardNumber: {
            identifier: 'cardNumber',
            rules: [
              {
                type   : 'ruleForCardNumber[param]',
                prompt : 'Please enter valid card number (visa or mastercard)'
              }
            ]
          },
          /*cardNumber: {
           identifier: 'cardNumber',
           rules: [
             {
               // Visa       4565340519181845
               // Mastercard 5200828282828210
               // Discover   6011111111111117
               // Unionpay   6240008631401148
               type   : 'creditCard[visa,mastercard]',
               prompt : 'Please enter valid card number (visa or mastercard)'
             }
           ]
         },*/
         cardCVV: {
          identifier: 'cardCVV',
          rules: [
            {
              type   : 'ruleForCardCVC[param]',
              prompt : 'Please enter CVV'
            }
          ]
        },
        cardExpMM: {
         identifier: 'cardExpMM',
         rules: [
           {
             type   : 'regExp[/^(([0][0-9])|([1][0-2]))$/]',
             prompt : 'Please enter valid card expirition date'
           }
         ]
       },
       cardExpYY: {
        identifier: 'cardExpYY',
        rules: [
          {
            type   : 'ruleForCardExpYY[param]',
            prompt : 'Please enter valid card expirition date'
          }
        ]
       },
      cardName: {
        identifier: 'cardName',
        // rules: [
        //   {
        //     type   : 'regExp[/^[a-zA-Z-\\s]{3,40}$/]',
        //     prompt : 'Please enter valid full name'
        //   }
        // ]
        rules: [
          {
            type   : 'regExp',
            value  : /^(\s{0,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{0,10})$|^(\s{0,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{0,10})$|^(\s{0,10}[a-zA-Z]{2,24}\s{0,10})$/i,
            prompt : 'Please enter valid card name'
           }
         ]
      }
      },
      onSuccess: function(event, fields){
        return false;
      },
      onFailure: function(){
        return false;
      },
      }
    );


    });


    
    let step = this.get('step');
    let form = '#form-step' + step;
    let formData = this.get('formData');
    step -= 1;
    $(form).form('set values', formData[step]);
    
    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');
  }
});
