import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('call mayment-step3.js v.3');

    const me = this;
    me.set('loading', true);
    me.get('loadingEffect');
    
    // Скрываем телефон

    // Подгрузка имени с карты
    let cardName = "";
    const currency = this.get('share').getAmountOut().toLowerCase();
    if (this.get('formData')[1]) {
      if (this.get('formData')[1].cardName !== undefined && (currency === 'usd' || currency === 'rub')) {
        cardName = this.get('formData')[1].cardName;
        $("#fullName").val(cardName);
      }
    }

    // Задаем телефонный формат по региону
    let region = this.get('share').getRegion().toLowerCase();
    let excludeCountries = ['ir', 'us', 'jp'];
    if (excludeCountries.indexOf(region) !== -1)
      region = 'gb';
    
    $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/intlTelInput.js').done(function(script, textStatus){
      $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js').done(function(script, textStatus){
      
      
      // $("#form-step3 input[name=phone]").intlTelInput({
      //   initialCountry: region,
      //   excludeCountries: excludeCountries
      // });

      var input = document.querySelector("#form-step2 input[name=phone]");
      window.itiPaymentStep = window.intlTelInput(input, {
          initialCountry: region,
          excludeCountries: excludeCountries
      });
      $("#bodyFeedback").removeClass('loading');
      $("#form-step2").removeClass('loading');
      me.set('loading', false);
      me.get('loadingEffect');


    })
    });

    // Навесть валидацию $("#form-step3 input[name=phone]").intlTelInput("isValidNumber");

    $('#form-step2 input[name=birthday]').mask('00.00.0000', {});

    $('#form-step2 input[name=fullName]').bind('keyup',function(){
      $(this).val($(this).val().replace(/[^a-zA-Z-\s]/i, ""))
    });

    // Switch position birthday on Russian language
    let language = me.get('i18n.locale');
    let birthdayType = 'regExp[/^([0][0-9]|[1][0-2]).([0-2][0-9]|[3][0-1]).([1][9][0-9][0-9]|[2][0][0-1][0-7])$/]'; // MM.DD.YYYY
    if (language === 'ru')
      birthdayType = 'regExp[/^([0-2][0-9]|[3][0-1]).([0][0-9]|[1][0-2]).([1][9][0-9][0-9]|[2][0][0-1][0-7])$/]'; // DD.MM.YYYY

    $('#form-step2')
      .form({
        on: 'change',
        fields: {
          fullName: {
           identifier: 'fullName',
           /*rules: [
             {
               //type   : 'regExp[/^[a-zA-Z-]{3,16} [a-zA-Z-]{3,16}/]',
               type   : 'regExp[/^(\s{0,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{0,10})$|^(\s{0,10}[a-zA-Z]{2,24}\s{0,10})$/]',
               prompt : i18n.t('invalidFullName').string
             }
           ]*/
           rules: [
            {
              type   : 'regExp',
              value  : /^(\s{0,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{0,10})$|^(\s{0,10}[a-zA-Z]{2,24}\s{1,10}[a-zA-Z]{2,24}\s{0,10})$|^(\s{0,10}[a-zA-Z]{2,24}\s{0,10})$/i,
              prompt : i18n.t('invalidFullName').string
             }
           ]
         },
         phone: {
          identifier: 'phone',
          rules: [
            {
              type   : 'empty',
              prompt : i18n.t('invalidPhone').string
            }
          ]
        },
        birthday: {
         identifier: 'birthday',
         rules: [
           {
             type   : birthdayType,
             prompt : i18n.t('invalidBirthday').string // Your date of birth in format MM.DD.YYYY
           }
         ]
       }
      },
      onSuccess: function(event, fields){
        return false;
      },
      onFailure: function(event, fields){
          try {
            if(!$('#form-step2').form('is valid', 'fullName')) 
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectName', eventLabel: 'ContactDetails', eventValue: 1 });
            if(!$('#form-step2').form('is valid', 'phone')) 
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectPhone', eventLabel: 'ContactDetails', eventValue: 1 });    
            if(!$('#form-step2').form('is valid', 'birthday')) 
              ga('send', 'event', { eventCategory: 'Screen', eventAction: 'RejectBirthday', eventLabel: 'ContactDetails', eventValue: 1 });
          } catch (e) {
            console.log('Error ga: Step3');
          }
          return false;
        },
      }
    );
    let step = this.get('step');
    let form = '#form-step' + step;
    let formData = this.get('formData');
    step -= 1;
    $(form).form('set values', formData[step]);

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');
  }
});
