import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
    ENV: ENV,
    i18n: Ember.inject.service(),
});