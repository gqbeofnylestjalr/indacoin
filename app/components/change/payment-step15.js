import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  i18n: Ember.inject.service(),
  tagName: '',
  didInsertElement: function(){
    const i18n = this.get('i18n');
    console.log('Call payment-step15.js');
    const share = this.get('share');
    const me = this;

    $('#payment-steps div.step').addClass('disabled');
    $('#payment-steps div.active').removeClass('active');
    $('#payment-steps div.step:nth-child(5)').addClass('active').removeClass('disabled');
    $('#nextStepBtn').show();
    $('#prevStepBtn').hide();

    // End loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    // Load JS animation Lottie
    $.getScript('/assets/exclude/scripts/lottie-lite.min.js').done(function(script, textStatus){

        var container = document.getElementById("svgContainer"),
        anim = bodymovin.loadAnimation({
          wrapper: container,
          animType: 'svg',
          loop: false,
          path: '/assets/exclude/scripts/' + i18n.t('lang') + '/prepare.json'
        });
  
        anim.addEventListener("enterFrame", function (animation) {
          if (animation.currentTime > (anim.totalFrames - 12)) {
              anim.pause();
          }
        });
        anim.setSpeed(0.6);

    });

  },
  init: function(){
    const me = this;
    const share = this.get('share');
    var cont = Ember.getOwner(this).lookup('controller:application').currentPath; // lang.index lang.change
    console.log('Current controller: ' + cont);

    // TODO: определять контроллер: lang.index / lang.change
    var controller = undefined;
    if (cont === 'lang.index')
      controller = Ember.getOwner(this).lookup('controller:change' );
    else
      controller = Ember.getOwner(this).lookup('controller:lang/change' );


    var requestId = me.get('share').getRequestId();
    var confirmCode = me.get('share').getConfirmCode();

    var lastPage = window.location.href;

    setTimeout(function() {

      share.notify(requestId, confirmCode);
      var formStatus = me.get('share').getFormStatus();

          // Если на шаге 15 статус verifying, то загружаем в контроллере html. Если иной статус, то перезагружаем страницу


          // TODO: проверять аналогично изменение страницы, если пользователь перейдет на другую страницу
          //var verif = false; // Статус - не verification
          /*$.ajax({
            async: false,
            method: 'POST',
            contentType: 'application/json; charset=utf-8',
            url: "/Notify/getData",
            data: "{'requestId':'" + requestId + "','confirmCode':'" + confirmCode + "'}",
            success: function onSucess(result) {

              var NotifyData = $.parseJSON(result.d);
              
              if (NotifyData.cardStatus == "Verifying" && NotifyData.cardAuthCodeStatus == "Verifying")
                vefir = true;
              if (NotifyData.phoneStatusAuthCode == "Verifying")
                vefir = true;
              if (NotifyData.s == "Verifying")
                vefir = true;
            }
          });*/

          if (ENV.environment === 'production') {}
          
          if (ENV.environment === 'development') {
            var formStatus = 'videoVerification'; // + error_verification.gif
          }
  
          if (ENV.environment === 'test') {}

          if (formStatus === 'videoVerification') {
            console.log('setup step #6');
            controller.set('step15', false);
            controller.set('step1', false);
            controller.set('step6', true);
            controller.set('step', 6);
            controller.set('returned', true);

          } else {
            window.location.reload();
          }

    }, 17000);

    this._super(...arguments);
  },
});