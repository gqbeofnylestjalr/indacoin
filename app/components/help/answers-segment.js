import Ember from 'ember';

export default Ember.Component.extend({
    didInsertElement: function(){
        var anchor = '#anchor-' + this.get('anchor');
        $('html, body').animate({
            scrollTop: $(anchor).offset().top
        }, 0);
    }
});
