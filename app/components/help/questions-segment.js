import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),

    segment: "",
    isPlatform: true,
    anchor: 1,
    block1: false,
    block2: false,
    block3: false,
    block4: false,
    block5: false,
    
    /*platform: Ember.computed('isPlatform', function(){
      let isPlatform = this.get('isPlatform');
      if (isPlatform) {
        return true;
      }
      else {
        return false;
      }
    }),*/

    actions: {
        turnPlatform() {
            this.set('isPlatform', true);

            this.set('block1', false);
            this.set('block2', false);
            this.set('block3', false);
            this.set('block4', false);
            this.set('block5', false);
        },
        questTransition(id, anchor) {
            const i18n = this.get('i18n');
            this.set('anchor', anchor);
            switch (id.toString()) {
                case '1':
                    this.set('segment', i18n.t('faq.header1'));
                break;
                case '2':
                    this.set('segment', i18n.t('faq.header2'));
                break;
                case '3':
                    this.set('segment', i18n.t('faq.header3'));
                break;
                case '4':
                    this.set('segment', i18n.t('faq.header4'));
                break;
                case '5':
                    this.set('segment', i18n.t('faq.header5'));
                break;
            }

            this.set('block1', false);
            this.set('block2', false);
            this.set('block3', false);
            this.set('block4', false);
            this.set('block5', false);

            this.set('block' + id, true);
            this.set('isPlatform', false);
            


            /*var container = $('body'),
            scrollTo = $('#row_8');
            container.scrollTop(
                scrollTo.offset().top - container.offset().top + container.scrollTop()
            );*/
            
            
            /*$('html, body').animate({
                scrollTop: $("#row_8").offset().top
            }, 0);*/
        

        }
    }
});
