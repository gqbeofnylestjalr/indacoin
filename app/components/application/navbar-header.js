import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
  cookies: Ember.inject.service(),
  ENV: ENV,
  tagName: '',
  nickName: undefined,
  didInsertElement: function(){

    // if (!window.backgroundLoaded) {
    //   window.backgroundLoaded = true;
    //   (function() {
    //     setTimeout(function() {
    //     console.log('change back');
    //     $(".poly-bg").css("background-image", "url('/ember/images/backgrounds/back.png')");
    //     }, 3000);
    //   })();
    // }

    $('#burger').on('click', function(){
      $('#burger-menu').show();
      $('#burger-menu div').one('click', function(){
        $('#burger-menu').hide();
      })
    });
    

    $('#platform-menu').dropdown();

    // Detect language from CloudFlare and show block screen for US users
    /*console.log('Detect language from navbar-header.js - didInsertElement');
    if (typeof FastBoot === 'undefined') {
      var regionByIp = "";
      var req = new XMLHttpRequest();
      req.open('GET', document.location, true);
      req.send(null);
      var headers = req.getAllResponseHeaders().toLowerCase();
      var countryPos = headers.indexOf('cfcountry');
      if (countryPos !== -1) {
        regionByIp = (headers[countryPos + 11] + headers[countryPos + 12]).toUpperCase();
      } else {
        var regionByIp = "GB";
      }

      console.log('myip = ' + regionByIp);

      if (regionByIp.toUpperCase() === 'US') {
        $('#USBlock')
        .modal({
          closable  : false,
          onApprove : function() {
            console.log('Close');
          }
        })
        .modal('show');
      }
    }*/

    /*var cookieService = this.get('cookies');
    let lang = cookieService.read('ipCountry');
    //console.log('didInsertElement ipCountry = ' + lang);
    if (lang !== undefined) {
      if (lang.toUpperCase() === 'US') {
        $('#USBlock')
        .modal({
          closable  : false,
          onApprove : function() {
            console.log('Close');
          }
        })
        .modal('show');
      }
    }*/

  },
  //didInitAttrs: function(){
  init: function(){
    var me = this;
    me._super(...arguments);

    /*console.log('Detect language from navbar-header.js - didInitAttrs');
    var cookieService = this.get('cookies');
    let localLang = cookieService.read('lang');
    console.log('get lang from cookie = ' + localLang);*/

    /*var cookieService = this.get('cookies');
    let lang = cookieService.read('ipCountry');
    console.log('didInitAttrs ipCountry = ' + lang);*/

    if (typeof FastBoot === 'undefined') {

      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // lang.index lang.change
      // TODO: rewrite to promises
      try {
        Ember.$.ajax({
          //url: "/inner/RefreshPairData",
          url: "https://indacoin.com/Services.asmx/RefreshPairData",
          type: "post",
          contentType: "application/json",
          cache: false,
          async: true,
          data: "{'pair':'BTC_USD'}",
          success: function(n) {
            try {
              var nick = n.d.accData.Nick;
              if (nick !== undefined) {
                me.set('nickName', nick);
                let search = window.location.search;
                let origin = window.location.origin;
                if (cont === 'lang.login' && search.slice(0, 45) === '?redirect=Service.aspx?TransactOutExtConfirm=')
                  window.location = origin + '/' + search.slice(10);
              }
            } catch(e) {}
          },
          failure: function() {
            console.log('RefreshPairData failure');
          }
        });

      } catch (e) {
        console.log('RefreshPairData error: ' + e);
      }
    }
  },
  actions: {
    logout() {
      var me = this;

      $.ajax({
        type: "POST",
        url: "/Services.asmx/logout",
        contentType: "application/json; charset=utf-8",
        async: false,
        dataType: "json",
        success: function(n) {
          console.log('Logout success');
        }
      });

      // Запрос RefreshPairData нужен для удаления сессии на сервере и подтверждения успешного LogOut
      try {
        $.ajax({
          url: "/inner/RefreshPairData",
          type: "post",
          contentType: "application/json",
          cache: false,
          async: true,
          data: "{'pair':'BTC_USD'}",
          success: function(n) {
            var nick = n.d.accData.Nick;
            if (nick !== undefined)
              me.set('nickName', nick);
          },
          error: function() {
            console.log('RefreshPairData: Error');
            //me.set('nickName', undefined);
            // Ошибка означает незалогиненного пользователя
            if (typeof FastBoot === 'undefined') {
              document.location.reload(true);
            }
          }
        });
  
      } catch (e) {
        console.log('RefreshPairData: catch error: ' + e);
      }

    },
    transitionToInner() {
      window.location = '/inner';
    }
  }
});

