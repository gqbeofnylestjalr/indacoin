import Ember from 'ember';
import $ from "jquery";
import ENV from 'fastboot/config/environment';
import googleAnalytics from 'google-analytics';

export default Ember.Component.extend({
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  cookies: Ember.inject.service(),
  classNames: ['language-select'],

  didInsertElement: function(){
    this._super(...arguments);
    let lang = this.get('i18n.locale');
    $('#language-select').dropdown('set selected', lang).show();
  },

  didUpdate: function(){
    const i18n = this.get('i18n');
    let langCode = this.get('i18n.locale');
    $('#language-select').dropdown('set text', '<i class="'+ langCode +' flag"></i>' + i18n.t('language-select.language.' + langCode));
    console.log('Call didUpdate with ' + langCode);
  },

  locales: Ember.computed('i18n.locale', 'i18n.locales', function() {
    const i18n = this.get('i18n');
    return this.get('i18n.locales').map(function (loc) {
      return { id: loc, text: i18n.t('language-select.language.' + loc) };
    });
  }),

  actions: {
    setLocale() {
      
      //console.log(this.get('locales'));
      console.log('Смена языка на компьютере');
      console.log('GA sending');
      googleAnalytics.sendAnalitics('Buttons', 'Click', 'ChangeLanguage', 1);
      // try {
      //   ga('send', 'event', { eventCategory: 'Buttons', eventAction: 'Click', eventLabel: 'ChangeLanguage', eventValue: 1 });
      //   console.log('ga sending');
      // } catch (e) {
      //   console.log('ga not sending');
      //   console.log('Error ga: ChangeLanguage on computer');
      // }

      const share = this.get('share');
      const i18n = this.get('i18n');

      let lang = $('#language-select').val(); // 'ru'
      let region = this.get('share').getRegion(); // 'US'

      // Сохраняем настройки языка для возможности восстановить их при возврате на страницу
      //if (typeof FastBoot === 'undefined') {
        //let lang = this.get("share").getLanguage(); // Like 'ru', 'en
        //let region = this.get("share").getRegion(); // Like 'US', 'RU'
        
        
        //localStorage.setItem('lang', lang + '_' + region);
        //console.log('Save lang from language-select: ' + lang + '_' + region);

        // Cookies
        var cookieService = this.get('cookies');
        cookieService.write('lang', lang + '_' + region.toUpperCase(), {path: '/'});
        Ember.set(ENV.i18n, 'lang', lang + '_' + region.toUpperCase());
      //}

      this.set('i18n.locale', lang);
      //this.send('changeDynLan', share, i18n, lang, region);

      

      // Меняем ru_RU на en_RU
      let url = window.location.pathname;
      let search = window.location.search;

      //console.log('Установлен язык: ' + lang);
      
      //let cryptoName = 

      //share.setLanguage(lang);

      //console.log(this.get('share').getCurrency());
      //window.location.replace(language + '_' + region + "/change/buy-bitcoin-with-cardusd");
      //debugger;
      //url[1] = lang[0];
      //url[2] = lang[1];
      //window.location.replace(url);

      // Т.к. URL всегда будет состоять как минимум из /en_US, можно заменять en
      let newUrl = url.slice(0, 1) + lang + '_' + region + url.slice(6);

      //this.transitionTo('lang.index', 'en_US');


      // Перезагрузка страниц при смене языка
      //window.location.reload();

      let cont = Ember.getOwner(this).lookup('controller:application').currentPath; // lang.index lang.change
      console.log('Current controller: ' + cont);

      if (!(cont === 'change' || cont === '404')) {
        window.history.pushState("Change language", "Title", newUrl + search);
      }

      let language = lang + '_' + region;
      if (cont === 'lang.index') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.index', language);
        //route.refresh();
        //Ember.getOwner(this).lookup('router:main').refresh();
        console.log('after transitionTo1');
      }
      else if (cont === 'lang.change') {
        let route = Ember.getOwner(this).lookup('router:main');
        let controller = Ember.getOwner(this).lookup('controller:lang/change');

        let page = window.location.href.toLocaleLowerCase();
        if (page.indexOf('confirm_code') === -1 && page.indexOf('request_id') === -1)
          route.transitionTo(newUrl + search);
        
        controller.set('countryName', lang);
        controller.set('buyingLeftTitleText', lang);
        console.log('after transitionTo lang.change');
      }


      else if (cont === 'change') {
        let route = Ember.getOwner(this).lookup('router:main');

        let page = window.location.href.toLocaleLowerCase();
        if (page.indexOf('confirm_code') === -1 && page.indexOf('request_id') === -1)
          route.transitionTo('change');
  
        console.log('after transitionTo change');
      }


      else if (cont === 'lang.help') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.help', language);
        let controller = Ember.getOwner(this).lookup('controller:lang/help' );
        console.log('after transitionTo3');
      }
      else if (cont === 'lang.api') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.api', language);
        let controller = Ember.getOwner(this).lookup('controller:lang/api' );
        console.log('after transitionTo4');
      }
      else if (cont === 'lang.affiliate') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.affiliate', language);
        console.log('after transitionTo5');
      }
      else if (cont === 'lang.login') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.login', language);
        console.log('after transitionTo6');
      }
      else if (cont === 'lang.locale') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.locale', language);
        console.log('after transitionTo7');
      }
      else if (cont === 'lang.terms-of-use') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.terms-of-use', language);
        console.log('after transitionTo8');
      }
      else if (cont === 'lang.error-page') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.error-page', language);
        console.log('after transitionTo8');
      }
      else if (cont === 'lang.news') {
        let route = Ember.getOwner(this).lookup('router:main');
        route.transitionTo('lang.news', language);
        console.log('after transitionTo11');
        

        //const currentRouteName = this.get("routing.currentRouteName");
        //const currentRouteInstance = getOwner(this).lookup(`route:${currentRouteName}`);
        //currentRouteInstance.refresh();

      }
      else if (cont === 'lang.currency') {
        let route = Ember.getOwner(this).lookup('router:main');
        //route.transitionTo('lang.currency', language);
        route.transitionTo(newUrl + search);
        console.log('after transitionTo10');
      }

        //let route = Ember.getOwner(this).lookup(`route:${get(this, 'currentRouteName')}`);
        //return route.refresh();


      $('#language-select').dropdown('set text', '<i class="'+ this.get('i18n.locale') +' flag"></i>   ' + i18n.t('language-select.language.' + this.get('i18n.locale')));

      // In more than 100 countries...
      //let reg = 'country.' + share.getRegion().toUpperCase() + '.moreName';
      //let country = i18n.t(reg);
      //$('#moreCountry').text(country);

      //window.location.replace(newUrl);
      //console.log(newUrl);

      //'/ru_RU/change/buy-ardor-with-cardusd'.slice(0, 1) + 'en' + '/ru_RU/change/buy-ardor-with-cardusd'.slice(3)

      
    },
    changeDynLan(share, i18n, lang, region) {
      let cryptocurrency = share.getOutCur();
      let word = i18n.t('meta.more') + ' ' + i18n.t('country.' + region.toUpperCase() + '.name');
      i18n.addTranslations(lang, {'unit' : word});

      
      //i18n.addTranslations(lang, {'currentCountry' : i18n.t('country.' + region.toUpperCase() + '.name')});
      
      // Меняем эти статические поля не тут, а в другом месте

      // Set field "Buy cryptocurrency..."
      //let buyCryptoTitle = i18n.t('meta.buy');
      //buyCryptoTitle = buyCryptoTitle.toString().replace('[cryptocurrency]', cryptocurrency);
      //$('#buyCryptoTitle').text(buyCryptoTitle);

      // Error: Failed to execute 'removeChild' on 'Node': The node to be removed is not a child of this node.
      // Set field "WE ARE WORKING IN..."
      /*var regionFullName = i18n.t('country.' + region + '.name').string;
      var mapSegmentTitle = i18n.t('map-segment.title').string;
      mapSegmentTitle = mapSegmentTitle.toString().replace('[country]', regionFullName);
      $('#map-segment-title').text(mapSegmentTitle);*/





      //console.log(regionFullName);
      //console.log(mapSegmentTitle);
      //debugger;
      //$('#map-segment-title').text(mapSegmentTitle);


      // Set field "BUY CLOAKCOIN WITH A CREDIT OR DEBIT CARD"
      console.log('Выставление: ' + cryptocurrency);

      // Change links manually =)
      /*let link1 = $('#link1').attr('href');
      $('#link1').attr('href', link1.slice(0, 1) + lang + link1.slice(3));

      let link2 = $('#link2').attr('href');
      $('#link2').attr('href', link2.slice(0, 1) + lang + link2.slice(3));*/

    }
  }
});
