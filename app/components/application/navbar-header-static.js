import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  didInsertElement: function(){
    $('#burger').on('click', function(){
      $('#burger-menu').show();
      $('#burger-menu div').one('click', function(){
        $('#burger-menu').hide();
      })
    });

  }
});
