import Ember from 'ember';

export default Ember.Component.extend({
  i18n: Ember.inject.service(),
  classNames: ['language-select'],

  didInsertElement: function(){
    $('#language-select-mobile').dropdown().show();
  },

  didUpdate: function(){
    const i18n = this.get('i18n');
    let langCode = this.get('i18n.locale');
    $('#language-select-mobile').dropdown('set text', '' + i18n.t('language-select.language.' + langCode));
  },

  locales: Ember.computed('i18n.locale', 'i18n.locales', function() {
    const i18n = this.get('i18n');
    return this.get('i18n.locales').map(function (loc) {
      return { id: loc, text: i18n.t('language-select.language.' + loc) };
    });
  }),

  actions: {
    setLocale() {

      console.log('Смена языка на телефоне');
      try {
        ga('send', 'event', { eventCategory: 'Buttons', eventAction: 'Click', eventLabel: 'ChangeLanguage', eventValue: 1 });
      } catch (e) {
        console.log('Error ga: ChangeLanguage on phone');
      }

      const i18n = this.get('i18n');
      this.set('i18n.locale', $('#language-select-mobile').val());
      $('#language-select-mobile').dropdown('set text', '' + i18n.t('language-select.language.' + this.get('i18n.locale')));
    }
  }
});
