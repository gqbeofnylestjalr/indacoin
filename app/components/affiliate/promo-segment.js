import Ember from 'ember';
import ENV from 'fastboot/config/environment';
import { hrefTo } from 'ember-href-to/helpers/href-to';
// import { log } from 'loglevel';
//import log from 'loglevel';
//import remote from 'loglevel-plugin-remote';

export default Ember.Component.extend({
    ENV: ENV,
    i18n: Ember.inject.service(),
    expandSegment: false,
    expandFeedback: false,
    share: Ember.inject.service('share'),

    contacts: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        let email = `<strong style="color: #00F798;">pr@indacoin.com</strong>`;
        return Ember.String.htmlSafe(i18n.t('affiliate.contacts.text', { email: email }));
    }),

    signUp: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        let href = hrefTo(this, 'lang.register', ENV.i18n.lang);
        let linkText = i18n.t('affiliate.promo.affiliate.block-1.link-text');
        let link = `<a href="${href}">${linkText}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"

        return Ember.String.htmlSafe(i18n.t('affiliate.promo.affiliate.block-1.text', { link: link }));
    }),

    discountExample: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        const link='https://indacoin.com/xx_XX/change?discount=48091'
        let href = `<a href="${link}">${link}</a>`; // "<a href="/de_GB/terms-of-use">Terms of Use</a>"
        return Ember.String.htmlSafe(i18n.t('affiliate.promo.affiliate.block-4.text', { link: href }));
    }),
    didInsertElement: function(){
        const i18n = this.get('i18n');
        // let region = this.get('share').getRegion().toLowerCase();
        // $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/intlTelInput.js').done(function(script, textStatus){
        //     $.getScript('https://intl-tel-input.com/node_modules/intl-tel-input/build/js/utils.js').done(function(script, textStatus){
        //     var input = document.querySelector("#bodyFeedback input[name=phone]");
        //     window.itiAffiliatePage = window.intlTelInput(input, {
        //         initialCountry: region
        //     });
        //     $("#bodyFeedback").removeClass('loading');
        //   })
        // });

        $('.questions-search-segment .project-kinds')
            .dropdown()
        ;

        $('#bodyFeedback')
        .form({
          on: 'change',
          fields: {
            email: {
              identifier: 'email',
              rules: [
                {
                    type    : 'regExp',
                    value   : /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i,
                    prompt : i18n.t('affiliate.promo.offer-block.mail-message').string
                }
              ]
            }
          }
        })
      ;
    },
    actions: {

        expandFeedback() {
            console.log('expandFeedback');
            $('#expandFeedback')
                .transition('toggle')
            ;
            $('#closeFeedback')
                .transition('toggle')
            ;
            $('#bodyFeedback')
                .transition({
                    animation: 'slide down',
                    duration: '1s'
                })
            ;
            
        },
        closeFeedback() {
            console.log('closeFeedback');
            $('#expandFeedback').show();
            $('#closeFeedback').hide();
            $('#bodyFeedback')
                .transition({
                    animation: 'slide down',
                    duration: '1s'
                })
            ;
        },

        expandSegment() {
            this.set('expandSegment', true);
        },
        closeSegment() {
            this.set('expandSegment', false);
        },

        goToExchange2() {
            const i18n = this.get('i18n');
            var form = "#bodyFeedback";
            $(form).form('validate form');
            if ($(form).form('is valid')){

                //let phone = $("#bodyFeedback input[name=phone]").intlTelInput("getNumber");
                //let phone = window.itiAffiliatePage.getNumber();
                let formData = $(form).form('get values');
                //formData.phone = phone;
                formData = JSON.stringify(formData);

                $.ajax({
                    async: false,
                    method: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: "/change/AddNewPotentialPartner",
                    data: formData,
                    success: function onSucess(result) {
                        $('#successModalPromo')
                        .modal({
                          closable  : false,
                          onApprove : function() {
                            $('.questions-search-segment input').val('');
                          }
                        })
                        .modal('show');
                    },
                    error: function onFailure(result) {
                        const message = i18n.t('modals.login.unknownError');
                        $('#errorModalMessagePromo').text(message);
                        $('#errorModalPromo')
                        .modal({
                          closable  : false,
                          onApprove : function() {
                            $('.questions-search-segment input').val('');
                          }
                        })
                        .modal('show');
                    }
                });


            } else {
                $(form).transition('shake');
            }
        },

        goToExchange(email) {
            const i18n = this.get('i18n');
            if (checkEmail(email)) {

                const phone = '';
                $.ajax({
                    async: false,
                    method: 'POST',
                    contentType: 'application/json; charset=utf-8',
                    url: "/change/AddNewPotentialPartner",
                    data: "{'email':'" + email + "','phone':'" + phone + "'}",
                    success: function onSucess(result) {
                        $('#successModalPromo')
                        .modal({
                          closable  : false,
                          onApprove : function() {
                            $('.questions-search-segment input').val('');
                          }
                        })
                        .modal('show');

                    },
                    error: function onFailure(result) {
                        const message = i18n.t('modals.login.unknownError');
                        $('#errorModalMessagePromo').text(message);
                        $('#errorModalPromo')
                        .modal({
                          closable  : false,
                          onApprove : function() {
                            $('.questions-search-segment input').val('');
                          }
                        })
                        .modal('show');
                    }
                });
            } else {
                const message = i18n.t('modals.registration.badEmail');
                $('#errorModalMessagePromo').text(message);
                $('#errorModalPromo')
                .modal({
                  closable  : false,
                  onApprove : function() {}
                })
                .modal('show');
            }
        }
    }
});

// SQL-injection protection
function checkEmail(value) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/i;
    let OK = re.exec(value);
    if (!OK)
        return false;
    return true;
}