import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  didInsertElement: function(){
    $( document ).ready(function() {
      var fb = [
             // Name
             // Time & Date
             // Message
             // Post Link
             // Avatar Src
         ];

         fb[0] = [
            "John Lufadeju",
            "26 October at 10:53",
            "This is my second time using Indacoin - Great service, speedy transaction & brilliant customer support. I will definitely use again.",
            "https://www.facebook.com/johnluffa/posts/10155685247532295:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/16711666_10154929416812295_3193779618206967220_n.jpg?oh=486a2c73d93e5d9ad00a32e7e7a1ceb3&oe=5AA2D162"
        ];

        fb[1] = [
            "Лада Поветьева",
            "12 October at 10:03",
            "Первый раз пользовалась услугами сервиса indacoin. Осталось очень приятное впечатление о самой услуге обмена и о качестве работы техподдержки. Спасибо за четкость и корректность.",
            "https://www.facebook.com/permalink.php?story_fbid=1456061734475853&id=100002161213224&substory_index=0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/20638987_1405290162886344_1166520707701354839_n.jpg?oh=dd7aeb8692fa1060e6c687446cc839e9&oe=5A75F094"
        ];

        fb[2] = [
            "Saulo Oliveira",
            "17 August",
            "It is difficult to find an easy credit card transaction facility to buy cryptographic currency and Indcoin treated me super well, patience with users from other countries who do not understand their language, online chat of readiness. Indcoin Thank you !!!",
            "https://www.facebook.com/saulooliver/posts/1407893592593315:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/14522933_1105947412787936_77294846013930768_n.jpg?oh=fe3b4eaa84e77d736cb7b716703392c3&oe=5AA02154"
        ];

        fb[3] = [
            "Adarsh Chandra",
            "5 July",
            "I just used Indacoin to make a transaction for Bitcoin purchase. The transaction went smooth, followed by a verification call. Its very easy to use and a user friendly portal for buying bit coins. I would recommend this platform for purchasing bitcoins!!",
            "https://www.facebook.com/permalink.php?story_fbid=102688820378273&id=100019114657607&substory_index=0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t31.0-1/c212.0.720.720/p720x720/10506738_10150004552801856_220367501106153455_o.jpg?oh=c2bd621412bac3ca642b3aca2b27cd61&oe=5A75F52E"
        ];

        fb[4] = [
            "Alejandro DeBro Gora",
            "28 June",
            "One of the easiest and most confortable ways of buying cryptocurrency via a debit or credit card account. There is no need to exchange coins for another, you can directly pay with your credit card and within a couple of minutes recieve the coins stored in your personal wallet.",
            "https://www.facebook.com/alerubio89/posts/10158851449305433:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/21687691_10159277176790433_4340595464110884141_n.jpg?oh=10c5baa8dbe3c8245436036c0688c538&oe=5AA3497C"
        ];

        fb[5] = [
            "Stijn Van Bezouwen",
            "28 June",
            "I decided to buy 100 euros worth of bitcoins using this service, and it was easy to proceed with the payment. Although i had some troubles and questions, I was almost instantly helped by their support team. It made me feel a lot more comfortable buying Bitcoin with Indacoin. And am planning to buy more in the future.",
            "https://www.facebook.com/stijn.van/posts/1632543306787133:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/21231638_1714414735266656_4547845763334167978_n.jpg?oh=68b2ba48ee0eb7a7470d87352f2838c4&oe=5A7584C8"
        ];
        
        fb[6] = [
            "Welf von Hören",
            "23 June",
            "Very quick payment, great support and great exchange rates.",
            "https://www.facebook.com/Welfvh/posts/1423492301022788:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/22552831_1530434500328567_1029723575592525174_n.jpg?oh=0cfa9279c84c845aabe42ddd32600289&oe=5A696E90"
        ];
        
        fb[7] = [
            "Elior Moore",
            "3 June",
            "As I'm only starting to taste everything that surrounds the cryptocurrency world, making an exchange at their website is more than simple. and their support in case ur getting stuck or just reaching out for help is far more than amazing.",
            "https://www.facebook.com/MooreElior/posts/10211357511991285:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/16425762_10210303663325727_2144166811873008511_n.jpg?oh=43dcfa7b2616bb7e4f1fbdfa2e38e058&oe=5A64C76C"
        ];
        
        fb[8] = [
            "Lesley Brown",
            "23 May",
            "Brilliant service and convenient to use, the folk there are friendly and very helpful. Although it is a digital transaction, there is a person at the other side of it (which can be a MASSIVE help) and reassuring for sure, 5star service which I'll use again ",
            "https://www.facebook.com/LEISB/posts/1406771172699010:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/c0.0.720.720/p720x720/18921788_1417554734953987_529796032077658846_n.jpg?oh=687867db14dd7c7fff2ea3494b030504&oe=5AAABF5F"
        ];
        
        fb[9] = [
            "John Yance",
            "17 May",
            "buena pagina de bitcoins, proceso rapido y sencillo, se los recomiendo",
            "https://www.facebook.com/john.yance.9/posts/1206759696120213:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/c0.1.720.720/p720x720/16195614_1098496233613227_1360363683957543436_n.jpg?oh=fdf8cd83c7dde48e286f8767be689b96&oe=5AA8084F"
        ];

        fb[10] = [
            "Indacoin",
            "Right now",
            "You can read more reviews on our facebook page! Click 'Read on facebook' below.",
            "https://www.facebook.com/indacoinlimited/reviews/",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/18033560_1856555191260195_2903784171229680178_n.jpg?oh=fda50b55cccc1bf0ae7cc0c8ec43a4f5&oe=5AAC60AF"
        ];
         /*fb[0] = [
                 "Chase McKinney",
                 "18 April at 18:06",
                 "I wasn't sure what to expect when indacoins David King popped up on their insta-messenger. Was it a real person, chat board or some person in India. I can tell you know it was a real person and not from India. Too sum up without being long winded, all I can say is I hope you get him when dealing with any issues. To David King's managers, watch out for this one! He should be on the management team. Thank you again David you've made me a client for life.",
                 "https://www.facebook.com/chase.mckinney.79/posts/1614316375262763:0",
                 "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/1483306_935168226510918_1581185056700086398_n.jpg?oh=a3eb02a53f476c67426d51cbf0a59610&oe=5A78D048"
         ];

         fb[1] = [
                 "Dashik Urakova",
                 "26 April at 00:05",
                 "This was my first time using such services and I am new to bitcoins. That is why I was a bit nervous and decided to contact your support first. Just had to say that I had a great support experience with Ana. She was amazing and helped me with all my questions and requests quickly and professionally! :) I will definitely be using your services in buying bitcoins.",
                 "https://www.facebook.com/dashiku1/posts/1551813271527673:0",
                 "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/13256359_1138995716142766_2690634033900448787_n.jpg?oh=6cf0c1edab649b7892c589904e7f4bff&oe=5A687243"
         ];

         fb[2] = [
            "Henry Hutcheson",
            "21 May at 06:10",
            "I have recently become interested in cryptocurrency investing, and I was surprised to learn that there was a service that offered customers the option to purchase cryptocurrencies with a Visa/Mastercard. I just made my first purchase a few minutes ago through Indacoin, and the entire purchasing process was quick and convenient. I was also impressed by the extensive security measures that have been put in place by the Indacoin service to prevent fraud. I will look forward to using your service in the future for other cryptocurrency purchases.",
            "https://www.facebook.com/henry.hutcheson.7/posts/10100981197629386:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/10612941_10100450727735306_3788529868758727698_n.jpg?oh=93c8ea44786491d946c4b10da6726b52&oe=5A754DBB"
        ];

        fb[3] = [
            "Lee Nader ",
            "20 May at 12:14",
            "Thank you so much for your excellent service and attention I received at Indacoin , this was my first time being involved with Crypto currency. I was treated very kindly by The chat Representative Mark and everyone involved that I spoke too. The support I was given has given me confidence to continue to explore the crypto currency world with Indacoin in the future because security is very important to me ( offline Cold Storage )and the user friendly app. Helps me to navigate the system .",
            "https://www.facebook.com/lnader1/posts/10213427464467323:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/19875639_10213984655556752_775952309139592641_n.jpg?oh=4af63c7bd2510a831a9505409e1835de&oe=5A7AA1C2"
        ];

        fb[4] = [
            "Trent James Lahey",
            "18 May at 00:49",
            "I've been considering bitcoin gaming for quite sometime and indacoin was the suggested purchase method from the casino i had decided on. I was hesitant at first but the support staff was super friendly and high quality. I had to do the video verification but almost as soon as i submitted the video (which was as easy as 2 mouse clicks) they verified it and I had my bitcoin instantly. Couldn't be more happy with the speed invovled and will be using there services in the future.",
            "https://www.facebook.com/TBagd/posts/10155327388866528:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/14657452_10154606476966528_1518077435466758758_n.jpg?oh=8a7a7ae4cd3a428a294a34352107bde7&oe=5AA8F221"
        ];

        fb[5] = [
            "Alejqndro Sanchez",
            "15 May at 19:18",
            "Excellent service, the experience was very professional. Security is very important for me and I am very satisfied with the service and attention. I even received a phone call minutes later from Narek to verify that everything was going on smoothly and that there were no issues. I hope they continue to be this fast and thorough for years to come!",
            "https://www.facebook.com/alejqndro.sanchez/posts/10213389801287825:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/c0.0.536.536/11701219_10207641632907208_226680877193119690_n.jpg?oh=ee4691aba12a65852b6cbebd6292ac32&oe=5AAB3776"
        ];
        fb[6] = [
            "Alex Pilică",
            "18 May at 01:19",
            "It is really easy to use this platform to buy bitcoin and they are also professional, they need to verify your informations before accepting your payment which is very safe :) Keep up the good work!",
            "https://www.facebook.com/AlexPilica/posts/1550536588297931:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/p720x720/22886057_1722145547803700_8617483771133169188_n.jpg?oh=56f19a38d53329612d84d07968472b48&oe=5A691EC9"
        ];
        fb[7] = [
            "K'shaun Summers",
            "20 May at 00:56",
            "Im just jumping into Cryptocurrency, INDACOIN is a very professional, nice and QUICK acting company. I was very skeptical at first, but they are FULLY LEGIT... if you are kind of nervous about doing transactions here, let this be that calm pat on the shoulder. ITS SAFE!",
            "https://www.facebook.com/keyshaun/posts/10206990187753650:0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t31.0-1/c90.210.540.540/p720x720/20286806_10207449854005019_6381226489533915116_o.jpg?oh=32aac878bc734daa6c43f152309f5a81&oe=5A7A2C62"
        ];
        fb[8] = [
            "Martin Acevedo",
            "22 May at 16:23",
            "This is the place to buy cryptocurrencies.It's fast, secure and easy. And best of all, you can use a Visa or MasterCard for your purchases.Extra points for the extensive security measures.I know I will be using Indacoin frequently to make my purchases.",
            "https://www.facebook.com/permalink.php?story_fbid=202146433639003&id=100015309425233&substory_index=0",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t31.0-1/c125.0.576.576/p720x720/19488886_227676951085951_466314392645408266_o.jpg?oh=276e66de970874a456a7b25c4b9d5090&oe=5AA3A14B"
        ];
        fb[9] = [
            "Indacoin",
            "Right now",
            "You can read more reviews on our facebook page! Click 'Read on facebook' below.",
            "https://www.facebook.com/indacoinlimited/reviews/",
            "https://scontent-arn2-1.xx.fbcdn.net/v/t1.0-1/18033560_1856555191260195_2903784171229680178_n.jpg?oh=fda50b55cccc1bf0ae7cc0c8ec43a4f5&oe=5AAC60AF"
        ];*/

      var fbCount = Math.floor(Math.random() * fb.length - 1);
          fbNext();

          function fbNext() {
              $("#fb-name").fadeOut(0).html(fb[fbCount][0] + '<div class="sub header">'+ fb[fbCount][1] +'</div>').fadeIn(600);
              $("#fb-msg").fadeOut(0).text(fb[fbCount][2]).fadeIn(600);
              $("#fb-link").attr("href", fb[fbCount][3]);
              $("#fb-img").fadeOut(0).attr("src", fb[fbCount][4]).fadeIn(600);
          }

          $("#fb-prev").on("click", function () {
              if (fbCount > 0) {
                  fbCount -= 1;
                  fbNext();
              } else {
                  fbCount = fb.length - 1;
                  fbNext();
              }
          });

          $("#fb-next").on("click", function () {
              if (fbCount < fb.length - 1) {
                  fbCount += 1;
                  fbNext();
              } else {
                  fbCount = 0;
                  fbNext();
              }
          });
    });
  }
});
