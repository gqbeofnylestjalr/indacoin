import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  didInsertElement: function(){
    let settings = {
      icon: 'none',
      onDisplay: function(){
        $('#youtube--play').hide();
        $("#youtube").detach().appendTo('#youtube-modal .content');
        $('#youtube-modal').modal({
          onHide: function(){
            $("#youtube").detach().prependTo('#youtube--block div').embed('destroy').embed(settings);
            $('#youtube--play').show();
          }
        }).modal('show');
      }
    };
    $('#youtube').embed(settings);
  }

});
