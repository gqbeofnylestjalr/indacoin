import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),
    isAgreement: true,
    actions: {
        turnAgreement() {
            this.set('isAgreement', true);
        },
        turnPolicy() {
            this.set('isAgreement', false);
        }
    }
});
