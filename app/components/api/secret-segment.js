import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),
    managerMessage: Ember.computed('i18n.locale', function() {
        var company = '<strong>pr@indacoin.com</strong>';
        return this.get('i18n').t('api.secret-segment.title', {
            company: Ember.String.htmlSafe(company)
        });
      })
});