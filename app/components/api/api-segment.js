import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
    ENV: ENV,
    i18n: Ember.inject.service(),
    contactEmail: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        return Ember.String.htmlSafe(i18n.t('api.introduction.text-2'));
    }),
    
    fullList: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        return Ember.String.htmlSafe(i18n.t('api.introduction.text-13'));
    }),

    generalView: Ember.computed('ENV.i18n.lang', function(){
        const i18n = this.get('i18n');
        return Ember.String.htmlSafe(i18n.t('api.introduction.text-14'));
    }),
    
    hidden1: true,

    didInsertElement: function(){
        $('#api-segmnent__tabs_1 .item')
            .tab()
        ;
        $('#api-segmnent__tabs_2 .item')
            .tab()
        ;
        $('#api-segmnent__tabs_3 .item')
            .tab()
        ;
    },
    actions: {
        turnHidden1() {
            this.set('hidden1', false);
        }
    }
});