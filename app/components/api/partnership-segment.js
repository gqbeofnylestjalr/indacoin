import Ember from 'ember';

export default Ember.Component.extend({
    //media: Ember.inject.service(),
    hidden1: true,
    hidden2: true,
    hidden3: true,
    actions: {
        turnHidden1() {
            this.set('hidden1', false);
        },
        turnHidden2() {
            this.set('hidden2', false);
        },
        turnHidden3() {
            this.set('hidden3', false);
        }
    }
});
