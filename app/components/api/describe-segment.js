import Ember from 'ember';

export default Ember.Component.extend({
    hidden1: true,
        actions: {
            turnHidden1() {
                this.set('hidden1', false);
                console.log('call hidden1');
            }
        }
});
