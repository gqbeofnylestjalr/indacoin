import Ember from 'ember';
//import ScrollReveal from 'ember';
//import ScrollReveal from '../../../assets/ember';
//import ScrollReveal from 'fastboot/assets/ember'

export default Ember.Component.extend({
  tagName: '',
  share: Ember.inject.service('share'),
  didInsertElement: function() {
    const share = this.get('share');
    
    window.sr = ScrollReveal({
      viewFactor: 0.35
    });

    //sr.reveal('#stop-sidebar', 25);
    
    // Elements on main page
    //$('#nav-main div a').css('opacity', '1');
    //sr.reveal('#nav-main div a', 25);
    $('#language-select').show();
    //sr.reveal('#language-select',25);
    //$('#observe').css('opacity', '1');
    //sr.reveal('#observe', 25);
    //sr.reveal('.featured-section div', 10);
    //sr.reveal('#sidebar div div', 25);
    //sr.reveal('tr', 25);
    //sr.reveal('#buy-blocks div', 25);
    //sr.reveal('#exchanger div div', 25);
    //sr.reveal('footer div div', 25);

    // New elements
    //sr.reveal('#orders-segment-container', 25);
    //sr.reveal('#exchanger-block', 25);

    // Elements on buying page
    //sr.reveal('#steps-content', 25);

    // Elements on buying page
    //sr.reveal('#stop-sidebar', 25);

    var region = share.getLanguage();
    var activeRegion = null;
    var count = null;
    switch (region) {
      case 'de':
        activeRegion = "#map-num-3";
        count = 826;
      break;
      case 'dz':
        activeRegion = "#map-num-4";
        count = 972;
      break;
      case 'en':
        activeRegion = "#map-num-1";
        count = 2711;
      break;
      case 'es':
        activeRegion = "#map-num-3";
        count = 826;
      break;
      case 'fr':
        activeRegion = "#map-num-3";
        count = 826;
      break;
      case 'it':
        activeRegion = "#map-num-3";
        count = 826;
      break;
      case 'pt':
        activeRegion = "#map-num-3";
        count = 826;
      break;
      case 'ru':
        activeRegion = "#map-num-5";
        count = 864;
      break;
      case 'tr':
        activeRegion = "#map-num-4";
        count = 972;
      break;
    }

    sr.reveal('#map-segment-container', { beforeReveal: mapReveal }, 0);

    //sr.reveal('#buying-segment-container', 25);
    //sr.reveal('#phone-segment-container', 25);
    //sr.reveal('#social-segment-container', 25);

    //sr.reveal('#cryptocurrencies-segment-container .ui.image', 25);

    //sr.reveal('#news-segment-container', 25);
    //sr.reveal('#links-segment-container', 25);
    //sr.reveal('#footer-segment-container', 25);

    //sr.reveal('#register-segment', 25);
    //sr.reveal('#login-segment', 25);

    //sr.reveal('#achievements-segment-container', 25);
    //sr.reveal('#clients-segment-container', 25);
    //sr.reveal('#benefits-segment-container', 25);
    //sr.reveal('#accounts-segment-container', 25);
    //sr.reveal('#services-segment-container', 25);
    
    // News segment
    //sr.reveal('#news-slider-container .news-slider-button-next', 25);
    //sr.reveal('#news-slider-container .news-slider-button-prev', 25);
    //sr.reveal('#news-slider-container .header1', 25);
    //sr.reveal('#news-slider-container .button', 25);
    //sr.reveal('#news-slider-container .news-slider-image', 25);
    //sr.reveal('#news-slider-container .header2', 25);
    //sr.reveal('#news-slider-container .column1', 25);
    //sr.reveal('#news-slider-container .column2', 25);
    //sr.reveal('#news-slider-container .column3', 25);

    // FAQ segment
    //sr.reveal('#questions-segment-container .text-link', 25);
    //sr.reveal('#questions-segment-container .header2', 25);
    //sr.reveal('#questions-segment-container .header1', 25);
    
    // FAQ segment
    //sr.reveal('#describe-segment-container', 25);
    //sr.reveal('#functions-segment-container', 25);
    //sr.reveal('#secret-segment-container', 25);
    //sr.reveal('#statuses-segment-container', 25);

    /*sr.reveal('#partnership-segment-container .header1', 25);
    sr.reveal('#partnership-segment-container .text4', 25);
    sr.reveal('#partnership-segment-container .text3', 25);
    sr.reveal('#partnership-segment-container .vert-list li', 25);
    sr.reveal('#partnership-segment-container .title2', 25);
    sr.reveal('#partnership-segment-container .title3', 25);
    sr.reveal('#partnership-segment-container .text1', 25);
    sr.reveal('#partnership-segment-container .text2', 25);
    sr.reveal('#partnership-segment-container .partnership-background-image', 25);
    sr.reveal('#partnership-segment-container .container p', 25);
    sr.reveal('#partnership-segment-container .title1', 25);
    sr.reveal('#partnership-segment-container .horiz-list li', 25);*/

    // Terms of use segment
    //sr.reveal('#terms-of-use div', 25);

    function mapReveal() {
      var durationTime = 5000;

      $('#label-1').transition('pulse');
      $('#label-2').transition('pulse');
      $('#label-3').transition('pulse');
      $('#label-4').transition('pulse');
      $('#label-5').transition('pulse');
      $('#label-6').transition('pulse');

      if (activeRegion !== null)
        $({someValue: 0}).animate({someValue: count}, {
          duration: durationTime,
          easing:'swing',
          step: function() {
              $(activeRegion).text((Math.round(this.someValue)));
          }
        });

    }
  }
});