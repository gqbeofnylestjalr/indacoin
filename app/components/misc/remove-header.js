import Ember from 'ember';

export default Ember.Component.extend({
  tagName: '',
  didInsertElement: function(){
    $('#observe').css('opacity', '0');
    $('#main').css('margin-top', '-20em');
  }
});
