import Ember from 'ember';

export default Ember.Component.extend({
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),
  isMore: true,
  more: Ember.computed('isMore', function() {
    return this.get('isMore');
  }),
  //newsList: [],
  //announceList: [],
  //commentsList: [],
  langCode: null,
  newsText: "",
  currentNewsId: null,

  didInitAttrs: function(){

    // TODO: now is available only two version: russian and english
    var langCode = 'en';
    if (this.get('i18n.locale') === 'ru')
      langCode = 'ru';

    this.set('langCode', langCode);

  },

  didInsertElement: function(){


    //************************ Masonry frid *****************/
    // external js: packery.pkgd.js
/*
    console.log('Execute packery include');
    var grid2 = document.querySelector('.grid2');
    var pckry = new Packery( grid2, {
      itemSelector: '.grid-item',
      percentPosition: true
    });

    grid2.addEventListener( 'click', function( event ) {
      let element = event.target;

      if (matchesSelector(event.target, '.grid-item-content'))
        element = event.target;
      else if (matchesSelector(event.target.parentNode, '.grid-item-content'))
        element = event.target.parentNode;
      else if (matchesSelector(event.target.parentNode.parentNode, '.grid-item-content'))
        element = event.target.parentNode.parentNode;
      else if (matchesSelector(event.target.parentNode.parentNode.parentNode, '.grid-item-content'))
        element = event.target.parentNode.parentNode.parentNode;

      if ( !matchesSelector( element, '.grid-item-content' ) ) {
        return;
      }

      var itemContent = element;
      setItemContentPixelSize( itemContent );

      var itemElem = itemContent.parentNode;

      var isExpanded = itemElem.classList.contains('is-expanded');
      itemElem.classList.toggle('is-expanded');

      var redraw = itemContent.offsetWidth;
      // renable default transition
      itemContent.style[ transitionProp ] = '';

      addTransitionListener( itemContent );
      setItemContentTransitionSize( itemContent, itemElem );

      if ( isExpanded ) {
        // if shrinking, shiftLayout
        pckry.shiftLayout();
      } else {
        // if expanding, fit it
        pckry.fit( itemElem );
      }
    });

    var docElem = document.documentElement;
    var transitionProp = typeof docElem.style.transition == 'string' ?
        'transition' : 'WebkitTransition';
    var transitionEndEvent = {
      WebkitTransition: 'webkitTransitionEnd',
      transition: 'transitionend'
    }[ transitionProp ];

    function setItemContentPixelSize( itemContent ) {
      var previousContentSize = getSize( itemContent );
      // disable transition
      itemContent.style[ transitionProp ] = 'none';
      // set current size in pixels
      itemContent.style.width = previousContentSize.width + 'px';
      itemContent.style.height = previousContentSize.height + 'px';
    }

    function addTransitionListener( itemContent ) {
      // reset 100%/100% sizing after transition end
      var onTransitionEnd = function() {
        itemContent.style.width = '';
        itemContent.style.height = '';
        itemContent.removeEventListener( transitionEndEvent, onTransitionEnd );
      };
      itemContent.addEventListener( transitionEndEvent, onTransitionEnd );
    }

    function setItemContentTransitionSize( itemContent, itemElem ) {
      // set new size
      var size = getSize( itemElem );
      itemContent.style.width = size.width + 'px';
      itemContent.style.height = size.height + 'px';
    }*/
//********************************************/


  },

  actions: {

    /*getNewsById(id) {
      return this.attrs.getNewsById('3');
    },*/

    /*refresh() {
      console.log('Call refresh');
			const currentRouteName = this.get("routing.currentRouteName");
			const currentRouteInstance = getOwner(this).lookup(`route:${currentRouteName}`);
			currentRouteInstance.refresh();
		},*/


    expandNews(id, scroll, event) {
      // console.log('expandNews ' + id);
      const me = this;
      if (me.get('currentNewsId') == id){}
      else {
        me.set('currentNewsId', id);
        if (scroll === undefined)
          scroll = true;
        if (id === undefined)
          id = 0;
        me.attrs.expandNews(id);
      }
    },

    readMore(id, scroll) {
      console.log('Call readMore ' + id + ' ' + scroll);
      if (scroll === undefined)
        scroll = true;
      if (id === undefined)
        id = 0;
        let me = this;

      this.attrs.expandNews(id);

      if (scroll) {
        var anchor = '#news-' + id;
        $('html, body').animate({
          scrollTop: $(anchor).offset().top
        }, 300);
      }

    },

    nextClick(id) {
      console.log('Call NextClick(): ' + id);
      this.set('isMore', true);
      $('#news-slider-wave').css("-webkit-mask-image", "");
      //$('#news-slider-wave').css("-webkit-mask-image", "-webkit-gradient(linear, right center, left center, from(rgba(0,0,0,1)), to(rgba(0,0,0,0)))");
      //-webkit-mask-image: -webkit-gradient(linear, left top, left bottom, from(rgba(0,0,0,1)), to(rgba(0,0,0,0)))
      
      var s = document.querySelector('#news-slider-swipe').swiper;
      s.update();
    },
    prevClick(id) {
      console.log('Call PrevClick(): ' + id);
      this.set('isMore', true);
      //$('#news-slider-wave').css("opacity", 1);
      $('#news-slider-wave').css("-webkit-mask-image", "");
      var s = document.querySelector('#news-slider-swipe').swiper;
      s.update();
    }
  }
});