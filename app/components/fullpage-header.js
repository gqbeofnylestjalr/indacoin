import Ember from 'ember';
import ENV from 'fastboot/config/environment';
import googleAnalytics from 'google-analytics';

export default Ember.Component.extend({
  tagName: '',
  ENV: ENV,
  i18n: Ember.inject.service(),
  store: Ember.inject.service(),

  filter: '',
  currency: 'USD',
  lastGiveAmount: 0,
  lastTakeAmount: 0,

  didInsertElement: function(){

    const me = this;
    // $("#scroll-down, .scroll-text").click(function (){
    //     $('html, body').animate({
    //         scrollTop: $("#main").offset().top
    //     }, 1000);

    //     try {
    //       ga('send', 'event', { eventCategory: 'Buttons', eventAction: 'Click', eventLabel: 'MainScrollDown', eventValue: 1 });
    //     } catch (e) {
    //       console.log('Error ga: MainScrollDown');
    //     }

    // });
    $('#attention')
    .modal({
      closable  : false,
      onApprove : function() {
        console.log('Approved!');
      }
    })
    .modal('show');

    $('.fullpage .you-have-field .ui.dropdown.label').dropdown();
    //$('.fullpage .you-get-field .ui.dropdown.label').dropdown();
    $('#cur-dropdown').dropdown();
    //$('#cur-dropdown').dropdown('set selected', 'Bitcoin');
    $("#coinid").replaceWith( "<span id=\"coinid\" class=\"text\"><span class=\"coinFullName\">Bitcoin</span><span class=\"right-part\"><span class=\"scrolling-short-cur-img\"><span style=\"position: absolute;\" class=\"sprite-main crypto-icon sprite-BTC\"></span></span><span class=\"scrolling-short-cur\">BTC</span></span></span>" );


    /* Валидация ввода и удаление постронних символов */
    let usd = Ember.$('.you-have-field input');
    let btc = Ember.$('.you-get-field .input-field');


    usd.on("keypress", function(e) {
      let amount = Ember.$('.you-have-field input').val();
      e = e || event;
      if (e.ctrlKey || e.altKey || e.metaKey) return;
      var chr = getChar(e);
      if ((chr === '.' || chr === ',') && (amount.indexOf('.') !== -1 || amount.indexOf(',') !== -1))
        return false;
      if (chr == null) return;
      if (chr === '.' || chr === ',') {}
      else if (chr < '0' || chr > '9') {
        return false;
      }
      // e = e || event;
      // if (e.ctrlKey || e.altKey || e.metaKey) return;
      // var chr = getChar(e);
      // if (chr === '.' && amount.indexOf('.') !== -1)
      //   return false;
      // if (chr == null) return;
      // if (chr === '.') {}
      // else if (chr < '0' || chr > '9') {
      //   return false;
      // }
    });

    btc.on("keypress", function(e) {
      let amount = Ember.$('.you-get-field .input-field').val();
      e = e || event;
      if (e.ctrlKey || e.altKey || e.metaKey) return;
      var chr = getChar(e);
      if ((chr === '.' || chr === ',') && (amount.indexOf('.') !== -1 || amount.indexOf(',') !== -1))
        return false;
      if (chr == null) return;
      if (chr === '.' || chr === ',') {}
      else if (chr < '0' || chr > '9') {
        return false;
      }
      // e = e || event;
      // if (e.ctrlKey || e.altKey || e.metaKey) return;
      // var chr = getChar(e);
      // if (chr === '.' && amount.indexOf('.') !== -1)
      //   return false;
      // if (chr == null) return;
      // if (chr === '.') {}
      // else if (chr < '0' || chr > '9') {
      //   return false;
      // }
    });

    // TODO: Add event to Symbol and check doubled events
    // let event = document.getElementById("cur-dropdown").addEventListener("click", function(event) {
      
    //         var el = event.target || event.srcElement;
    //         //console.log('Click detect on element = ' + el.getAttribute('class'));
    //         window.el = el;
      
    //         if (el.getAttribute('id') === 'qwerty')
    //           return 0;
      
            
      
    //         if ($('#takeCur-select .menu').css('display') !== 'none') {
    //           //me.set('flag', true);
    //           $('#etc').show();
    //           $('#qwerty').show();
    //           $('#takeCur-select .menu').show();
    //         } else {
    //           //me.set('flag', false);
    //           $('#etc').hide();
    //           $('#qwerty').hide();
    //           $('#takeCur-select .menu').hide();
    //         }
      
    //         let name = null;
    //         let shortName = null;
      
    //         // check if not dropdown item click
    //         if (el.parentElement.parentElement.parentElement.getAttribute('class') === 'item currencyClass' ||
    //           el.parentElement.parentElement.getAttribute('class') === 'item currencyClass' ||
    //           el.parentElement.getAttribute('class') === 'item currencyClass' ||
    //           el.getAttribute('class') === 'item currencyClass') {
              
    //             // getting coin name from dropdown
    //           if (el.getAttribute('class') === 'item currencyClass') {
    //             name = $(el.firstElementChild).text();
    //             shortName = $(el.lastElementChild.lastElementChild).text();
    //           } else
    //           if (el.getAttribute('class') === 'coinFullName') {
    //             name = $(el).text();
    //             shortName = $(el.nextElementSibling.lastElementChild).text();
    //           } else
    //           if (el.getAttribute('class') === 'scrolling-short-cur') {
    //             name = $(el.parentElement.previousElementSibling).text();
    //             shortName = $(el).text();
    //           } else
    //           if (el.getAttribute('class').slice(0, 18) === 'sprite crypto-icon') {
    //             name = $(el.parentElement.parentElement.previousElementSibling).text();
    //             shortName = $(el.parentElement.nextElementSibling).text();
    //           }
      
    //           if (name !== null) {
    //             console.log('name = ' + name);
    //             console.log('shortName = ' + shortName);
    //             $('#coinid > .coinFullName').text(name);
    //             $('#coinid .scrolling-short-cur').text(shortName);
      
    //           }
    //         }
      
    //       });
    // this.set('event', event);

          $('#qwerty').keyup(()=> {
            let val = $('#qwerty').val();
            me.set('filter', val);
          });

    this.send('price');


    this.send('pullCurrency');
    // document.getElementById('qwerty').autofocus = false;
    // document.getElementById('test').autofocus = true;
    // $('#qwerty').removeAttr( "autofocus" );
    // $('#test').focus();
    // $('#test').prop('autofocus');
  },


  fullpageHeader: Ember.computed('ENV.i18n.lang', 'currency', function() {
    const i18n = this.get('i18n');
    const currency = this.get('currency');
    let text = '';
    if (currency === 'BTC')
      text = i18n.t('fullpage.header-alt');
    else
      text = i18n.t('fullpage.header');
    return text;
  }),

  fullpageSubHeader: Ember.computed('ENV.i18n.lang', 'currency', function() {
    const i18n = this.get('i18n');
    const currency = this.get('currency');
    let text = '';
    if (currency === 'BTC')
      text = i18n.t('fullpage.sub-alt');
    else
      text = i18n.t('fullpage.sub');
    return text;
  }),


  //cryptoCurrency: 'btc',
  filteredBooks: Ember.computed('filter', 'currency', function() {
    let filterTerm = this.get('filter');
    let currency = this.get('currency');

    let showBitcoin = false;
    if (currency === 'BTC')
      showBitcoin = true;

    if (showBitcoin) {
      if (filterTerm.length == 0) {
        //console.log('var 1');
        var filtered = this.get('store').peekAll('value').filter( function(item) {
          if (!(item.get('short_name').toLowerCase() === 'btc' || item.get('short_name').toLowerCase() === 'eth')) {
          //if (item.get('short_name').toLowerCase() !== 'btc' || item.get('short_name').toLowerCase() !== 'eth') {
          //if (item.get('short_name').toLowerCase() !== 'btc') {
            return true;
          }
        });
      }
      else {
        //console.log('var 2');
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('value').filter( function(item) {

          if (item.get('short_name').toLowerCase() !== 'btc') {
            if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
              if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
                noDoubledArray.push(item.get('short_name'));
                return true;
              }
            }
          }

        });
      }
    } else {
      if (filterTerm.length == 0) {
        //console.log('var 3');
        var filtered= this.get('store').peekAll('value');
      }
      else {
        //console.log('var 4');
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('value').filter( function(item) {
          if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
            if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
              noDoubledArray.push(item.get('short_name'));
              return true;
            }

          }
        });
      }
    }
    return filtered;
  }),

  // price: Ember.computed('currency', 'cryptoCurrency', function() {

  //   let result = '';
  //   let giveCur = this.get('currency').toUpperCase(); // EUR
  //   let takeCur = this.get('cryptoCurrency').toUpperCase(); // BTC

  //   $.get( `https://indacoin.com/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/1`, function( data ) {

  //     let priceOneCoin = parseFloat(data).toFixed(2);
  //     result = `1 ${takeCur} ~ ${priceOneCoin} ${giveCur}`;
  //     console.log(result);
  //     $('.over-head.price').text(result);

  //   });

  //   result = `1 ${takeCur} ~ ${priceOneCoin} ${giveCur}`;
  //   return result;
  
  // }),

  actions: {



    // Срабатывает при изменении выпадающего списка поля BTC
    switchTakeCur() {
      console.log('call switchTakeCur');
      this.send('pullCurrency');
      //let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      //this.set('cryptoCurrency', takeCur);
      this.send('price');
    },

    // Срабатывает при изменении выпадающего списка поля USD
    switchGiveCur() {
      console.log('call switchGiveCur');
      let giveCur = $('.you-have-field .text').text();
      this.set('currency', giveCur);
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      let amount = $('.you-have-field input').val(); // 1000

      if (amount.length > 0)
        if (giveCur === 'BTC') {
          if (Number(amount) > 1)
            $('.you-have-field input').val('0.1000');
          else {
            $('.you-have-field input').val(
              parseFloat(amount).toFixed(4)
            );
          }
        } else {
          $('.you-have-field input').val(
            parseFloat(amount).toFixed(2)
          );
        }

      if (takeCur == 'BTC') {
        $('#cur-dropdown').dropdown('set selected', 'BitAir');
        $('#cur-dropdown').dropdown('set selected', 'Ripple');
        $('#cur-dropdown').dropdown('show');
        if ($('#takeCur-select .menu').css('display') !== 'none') {
          $('#etc').show();
          $('#qwerty').show();
          $('#takeCur-select .menu').show();
        } else {
          $('#etc').hide();
          $('#qwerty').hide();
          $('#takeCur-select .menu').hide();
        }
      }

      this.send('pullCurrency');
      this.send('price');
    },

    pullCurrency() {
      console.log('call pullCurrency');
      let giveCur = $('.you-have-field .text').text(); // EUR
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      let amount = $('.you-have-field input').val(); // 1000

      if (amount === '') {
        $('.you-get-field input').val('');
      } else {

        /* Трекать еще изменения валюты USD, EUR, RUB, Ethereum и прочее
        
        let lastGiveAmount = this.get('lastGiveAmount');
        if (lastGiveAmount == amount)
          return;
        else
          this.set('lastGiveAmount', amount);*/
        amount = amount.replace(',', '.');
        $.get( `https://indacoin.com/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}`, function( data ) {
          if (data < 0)
            data = 0.00000000;
          $('.you-get-field .input-field').val( parseFloat(data).toFixed(5) );
        });
      }
    },

    // Срабатывает при изменении поля BTC (keyup)
    pullCurrencyCrypto() {
      console.log('call pullCurrencyCrypto');
      const me = this;
      let giveCur = $('.you-have-field .text').text(); // EUR
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      let amount = $('.you-get-field .input-field').val(); // "0.122031"
      if (amount === '') {
        $('.you-have-field input').val('');
      } else {


          let lastTakeAmount = this.get('lastTakeAmount');
          if (lastTakeAmount == amount)
            return;
          else
            this.set('lastTakeAmount', amount);

          //$.get( `/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/${amount}`, function( data ) {
          amount = amount.replace(',', '.');
          $.get( `https://indacoin.com/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/${amount}`, function( data ) {
          if (data < 0)
            data = 0.00000000;
          //let btc = amount * 1000 / data;
          if (giveCur === 'BTC')
            $('.you-have-field input').val(parseFloat(data).toFixed(4));
          else
            $('.you-have-field input').val(parseFloat(data).toFixed(2));
          //me.get("share").setAmountOut(parseFloat(btc).toFixed(2));
        });
      }
    },



    goToExchange() {
      let region = Ember.get(ENV.i18n, 'lang'); // ru_RU
      let giveCur = $('.you-have-field .text').text().toLowerCase();// EUR
      let takeCur = $('#coinid .coinFullName').text().toLowerCase(); // BTC
      let amount = $('.you-have-field input').val(); // 1000
      amount = amount.replace(',', '.');
      //console.log('goToExchange ' + giveCur + ' ' + takeCur + ' ' + amount);
      let url = '';
      if (Number(amount) > 0)
        url = "/" + region + "/change/buy-" + takeCur + "-with-card" + giveCur + '?amount_pay=' + amount;
      else
        url = "/" + region + "/change/buy-" + takeCur + "-with-card" + giveCur;
      googleAnalytics.sendAnalitics('Buttons', 'Click', 'Target9', 1);
      window.location.replace(url);
    },

    price() {
      // let giveCurOld = $('.you-have-field .text').text(); // EUR
      // let giveCur = '';
      // if (giveCurOld == 'RUB')
      //   giveCur = 'USD';
      // else
      //   giveCur = giveCurOld;
      // let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      // $.get( `https://indacoin.com/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/1`, function( data ) {
      //   let priceOneCoin = parseFloat(data).toFixed(2);
      //   if (giveCurOld == 'RUB')
      //     priceOneCoin  = priceOneCoin * 61.77;
      //   let result = `1 ${takeCur} ~ ${priceOneCoin} ${giveCur}`;
        
      //   $('.over-head.price').text(result);
      // });

    },

  }

});

function getChar(event) {
  if (event.which == null) {
    if (event.keyCode < 32) return null;
    return String.fromCharCode(event.keyCode) // IE
  }

  if (event.which != 0 && event.charCode != 0) {
    if (event.which < 32) return null;
    return String.fromCharCode(event.which) // остальные
  }

  return null; // специальная клавиша
}