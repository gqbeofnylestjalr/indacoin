import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),
    isForgot: false,
    didInsertElement: function() {
        const me = this;
        const i18n = this.get('i18n');

        $(function() {
            $('#loginForm').each(function() {
                $(this).find('input').keypress(function(e) {
                    if(e.which == 10 || e.which == 13) {
                        console.log('submit');
                        me.send('login');
                    }
                });
            });
        });

        $(function() {
            $('#forgotForm').each(function() {
                $(this).find('input').keypress(function(e) {
                    if(e.which == 10 || e.which == 13) {
                        console.log('submit');
                        me.send('reset');
                    }
                });
            });
        });

        // Request captcha
        $.ajax({
            url: "/Services.asmx/GetCaptcha",
            contentType: 'application/json; charset=utf-8',
            method: 'POST',
            data: "{}",
            async: false,
            success: function(n) {
                $("#loginCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
            },
            error: function(n) {
                if (n.status === 429)
                    alert(i18n.t('modals.login.floodDetect'));
            }
        })
    },

    actions: {
        toForgot() {
            this.set('isForgot', true);
        },
        reset() {

            const i18n = this.get('i18n');
            let form = '#forgotForm';
            let formData = $(form).form('get values');
            let email = formData.email;
            let captcha = formData.captcha;

            var _data = JSON.stringify({
                email: email,
                captcha: captcha
            });

            function requestCaptcha() {
                $.ajax({
                    url: "/Services.asmx/GetCaptcha",
                    contentType: 'application/json; charset=utf-8',
                    method: 'POST',
                    data: "{}",
                    async: false,
                    success: function(n) {
                        $("#forgotCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date)
                            .getTime());
                    },
                    error: function(n) {
                        if (n.status === 429)
                            alert(i18n.t('modals.login.floodDetect'));
                    }
                })
            }

            $.ajax({
                url: "/Services.asmx/TryResetPass",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: _data,
                success: function(n) {
                    switch (String(n.d)) {
                        case "-100": // Sent empty fields
                            alert(i18n.t('modals.registration.empty'));
                            $('#login-segment input[name=captcha]').val('');
                            requestCaptcha();
                            break;
                        case "1":
                            alert(i18n.t('modals.login.resetSuccess'));
                            $('#login-segment input[name=captcha]').val('');
                            requestCaptcha();
                            break;
                        case "-3":
                            alert(i18n.t('modals.login.badCaptcha'));
                            $('#login-segment input[name=captcha]').val('');
                            requestCaptcha();
                            break;
                        case "-4":
                            alert(i18n.t('modals.registration.badCaptcha'));
                            $('#login-segment input[name=captcha]').val('');
                            requestCaptcha();
                            break;
                        case "-1":
                            alert(i18n.t('modals.login.wrongEmail'));
                            $('#forgotForm input[name=captcha]').val(''); // Clear captcha field
                            requestCaptcha();
                            break;
                        default:
                            $('#login-segment input[name=captcha]').val('');
                            requestCaptcha();
                    }
                    return false;
                },
                error: function(n) {
                    switch (n.status) {
                        case 429:
                            alert(i18n.t('modals.login.unknownError'));

                            break;
                        default:
                            alert(i18n.t('modals.login.unknownError'));
                    }
                }
            });


        },
        reCaptcha() {

            // Request captcha
            const i18n = this.get('i18n');
            $.ajax({
                url: "/Services.asmx/GetCaptcha",
                contentType: 'application/json; charset=utf-8',
                method: 'POST',
                data: "{}",
                async: false,
                success: function(n) {
                    $("#loginCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
                },
                error: function(n) {
                    if (n.status === 429)
                        alert(i18n.t('modals.login.floodDetect'));
                }
            })
        },
        forgotReCaptcha() {

            // Request captcha
            const i18n = this.get('i18n');
            $.ajax({
                url: "/Services.asmx/GetCaptcha",
                contentType: 'application/json; charset=utf-8',
                method: 'POST',
                data: "{}",
                async: false,
                success: function(n) {
                    $("#forgotCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
                },
                error: function(n) {
                    if (n.status === 429)
                        alert(i18n.t('modals.login.floodDetect'));
                }
            })
        },
        login() {

            const i18n = this.get('i18n');
            let form = '#loginForm';
            let formData = $(form).form('get values');

            let email = formData.email;
            let pass = formData.loginPassword;
            let captcha = formData.captcha;



            /*$('#loginForm')
            .form({
              on: 'change',
              fields: {
                email: {
                  identifier: 'email',
                  rules: [
                    {
                        type   : 'email',
                        prompt : 'Please enter a valid e-mail'
                    }
                  ]
                },
               cardCVV: {
                identifier: 'cardCVV',
                rules: [
                  {
                    type   : 'ruleForCardCVC[param]',
                    prompt : 'Please enter CVV'
                  }
                ]
              },
              cardExpMM: {
               identifier: 'cardExpMM',
               rules: [
                 {
                   type   : 'regExp[/^(([0][0-9])|([1][0-2]))$/]',
                   prompt : 'Please enter valid card expirition date'
                 }
               ]
             },
             cardExpYY: {
              identifier: 'cardExpYY',
              rules: [
                {
                  type   : 'ruleForCardExpYY[param]',
                  prompt : 'Please enter valid card expirition date'
                }
              ]
             }
            },
            onSuccess: function(event, fields){
              return false;
            },
            onFailure: function(){
              return false;
            },
            }
          );*/

            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }
            if (!(validateEmail(email))) { // Отправлен невалидный email
                alert(i18n.t('modals.login.badLogin'));
                $('#login-segment input[name=loginPassword]').val('');
                $('#login-segment input[name=captcha]').val('');
                requestCaptcha();
                return false;
            }

            let totp = '-1'; // если 0, то запросить 6 цифр
            var _data = JSON.stringify({
                email: email,
                pass: pass,
                totp: totp,
                captcha: captcha,
                fp: "",
                fp2: ""
            });

            function requestCaptcha() {
                $.ajax({
                    url: "/Services.asmx/GetCaptcha",
                    contentType: 'application/json; charset=utf-8',
                    method: 'POST',
                    data: "{}",
                    async: false,
                    success: function(n) {
                        $("#loginCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date)
                            .getTime());
                    },
                    error: function(n) {
                        if (n.status === 429)
                            alert(i18n.t('modals.login.floodDetect'));
                    }
                })
            }
            
            function tryLogin() {
                $.ajax({
                    type: "POST",
                    url: "/Services.asmx/TryLogin",
                    data: _data,
                    async: false,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function(n) {
                        switch (String(n.d)) {
                            case "1": // Успешный вход
                                window.location.href = "/inner";
                                break;
                            case "0": // need to enter google autenticate code
                                var GACode = prompt('Please enter google authenticator');
                                
                                // Change totp parameter
                                var obj = JSON.parse(_data);
                                obj.totp = GACode;
                                _data = JSON.stringify(obj);
                                
                                isGoogleAuthenticator = true;
                                /*alert(i18n.t('modals.login.unknownError'));
                                $('#login-segment input[name=loginPassword]').val('');
                                $('#login-segment input[name=captcha]').val('');
                                requestCaptcha();*/
                                break;
                            case "-2":
                                alert(i18n.t('modals.login.unknownError'));
                                $('#login-segment input[name=loginPassword]').val('');
                                $('#login-segment input[name=captcha]').val('');
                                requestCaptcha();
                                break;
                            case "-3": // Пользователь уже вошел на сайт
                                alert(i18n.t('modals.login.alreadyLoggedIn'));
                                $('#login-segment input[name=captcha]').val('');
                                requestCaptcha();
                                break;
                            case "-1": // Неверный логин или пароль
                                alert(i18n.t('modals.login.badLogin'));
                                $('#login-segment input[name=loginPassword]').val('');
                                $('#login-segment input[name=captcha]').val('');
                                requestCaptcha();
                                break;
                            case "-100": // Sent empty fields
                                alert(i18n.t('modals.registration.empty'));
                                $('#login-segment input[name=captcha]').val('');
                                requestCaptcha();
                        }
                        return false
                    },
                    error: function(n) {
                        switch (String(n.status)) {
                            case "429":
                                //note(window.t.floodError, "notyRed");
                                alert(i18n.t('modals.login.floodDetect'));
                                break;
                            default:
                                alert(i18n.t('modals.login.unknownError'));
                                //note(window.t.er17, "noty_error")
                        }
                    }
                })
            }

            var isGoogleAuthenticator = false;
            tryLogin();
            if (isGoogleAuthenticator) // Need to enter GA code
                tryLogin();



        } // Login()

    }

});
