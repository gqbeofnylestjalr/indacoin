import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
    ENV: ENV,
    share: Ember.inject.service('share'),

    graph: Ember.computed('model', function(){
      console.log('Chart need to rerender');
    }),
    graph2: Ember.computed('currency-header1', function(){
      console.log('Chart need to rerender');
      return this.get('currency-header1');
    }),

    didInsertElement: function(){
        this._super(...arguments);
        $('#currency-graph-dropdown').dropdown();

        var ctx = document.getElementById('currencyChart').getContext('2d');
        var gradientFill = ctx.createLinearGradient(770, 0, 385, 0);
        gradientFill.addColorStop(0, "rgba(177, 222, 214, 0.8)");
        gradientFill.addColorStop(1, "rgba(167, 200, 221, 0.8)");

        Chart.defaults.LineWithLine = Chart.defaults.line;
        Chart.controllers.LineWithLine = Chart.controllers.line.extend({
           draw: function(ease) {
              Chart.controllers.line.prototype.draw.call(this, ease);
        
              if (this.chart.tooltip._active && this.chart.tooltip._active.length) {
                 var activePoint = this.chart.tooltip._active[0],
                     ctx = this.chart.ctx,
                     x = activePoint.tooltipPosition().x,
                     topY = this.chart.scales['y-axis-0'].top,
                     bottomY = this.chart.scales['y-axis-0'].bottom;
        
                 // draw line
                 ctx.save();
                 ctx.beginPath();
                 ctx.moveTo(x, topY);
                 ctx.lineTo(x, bottomY);
                 ctx.lineWidth = 2;
                 ctx.strokeStyle = '#64aecc';
                 ctx.stroke();
                 ctx.restore();
              }
           }
        });

        var chart = new Chart(ctx, {
                  type: 'LineWithLine',
                  data: {
                    labels: this.get('chartLabels'),
                    display: false,
                    datasets: [{
                      label: 'Price of ' + this.get('currency-header1') + " $",
                      data: this.get('chartData'),
                      pointRadius: 0,
                      hitRadius: 0,
                      fill: 'start',
                      backgroundColor: gradientFill,
                      borderColor: '#64aecc',
                      pointBackgroundColor: '#64aecc',
                      lineTension: 0.5,
                      borderWidth: 0.01,
                      DrawOnChartArea: true
                    }]
                  },
                  options: {
                    tooltips: {
                       intersect: false
                    },
                    legend: {
                      display: false
                    },
                    color: function(context) {
                      console.log('call color');
                      var index = context.dataIndex;
                      var value = context.dataset.data[index];
                      return value < 0 ? 'red' :  // draw negative values in red
                          index % 2 ? 'blue' :    // else, alternate values in blue and green
                          'green';
                    },
                    scales: {
                        yAxes: [{
                            display: true,
                            fontFamily: 'Helvetica Header',
                            type: 'linear',
                            ticks: {
                                callback: function(value, index, values) {
                                    return '$ ' + value;
                                }
                            }
                        }],
                        xAxes: [{
                          display: true,
                          fontFamily: 'Helvetica Header',
                          ticks: {
                            maxTicksLimit: 12
                          }
                        }]
            
                    }
                }
              });
    },
    actions: {
        toPurchasePage(isBitcoin) {
            let lang = this.get("share").getLanguage(); // Like 'ru', 'en
            let region = this.get("share").getRegion(); // Like 'US', 'RU'

            var currency = "bitcoin";
            if (!isBitcoin)
                currency = "ethereum";

            let link = "https://indacoin.com/" + lang + "_" + region + "/change/buy-" + currency + "-with-cardusd";

            if (typeof FastBoot === 'undefined') {
                window.location.href = link;
            }
        }
    }
});
