import Ember from 'ember';

export default Ember.Component.extend({
    share: Ember.inject.service('share'),
    actions: {
        toPurchasePage(isBitcoin) {
            //console.log('call toPurchasePage ' + isBitcoin);

            // https://indacoin.com/ru_US/change/buy-bitcoin-with-cardusd

            //return 0;
            let lang = this.get("share").getLanguage(); // Like 'ru', 'en
            let region = this.get("share").getRegion(); // Like 'US', 'RU'

            var currency = "bitcoin";
            if (!isBitcoin)
                currency = "ethereum";

            let link = "https://indacoin.com/" + lang + "_" + region + "/change/buy-" + currency + "-with-cardusd";

            if (typeof FastBoot === 'undefined') {
                window.location.href = link;
            }

        }
    }
});
