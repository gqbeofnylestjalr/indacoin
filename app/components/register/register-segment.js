import Ember from 'ember';

export default Ember.Component.extend({
    i18n: Ember.inject.service(),
    didInsertElement: function(){
      const i18n = this.get('i18n');

      // Request captcha
      $.ajax({
        url: "/Services.asmx/GetCaptcha",
        contentType: 'application/json; charset=utf-8',
        method: 'POST',
        data: "{}",
        async: false,
        success: function(n) {
            $("#registerCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
        },
        error: function(n) {
        if (n.status === 429)
            alert(i18n.t('modals.registration.floodDetect'));
        }
      })
    },

    actions: {
        reCaptcha() {
            // Request captcha
            $.ajax({
                url: "/Services.asmx/GetCaptcha",
                contentType: 'application/json; charset=utf-8',
                method: 'POST',
                data: "{}",
                async: false,
                success: function(n) {
                    $("#registerCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
                },
                error: function(n) {
                if (n.status === 429)
                    alert(i18n.t('modals.registration.floodDetect'));
                }
            })
        },
        register() {
            const i18n = this.get('i18n');
            let form = '#registerForm';
            let formData = $(form).form('get values');
            let email = formData.email; // sdfsdf@sdfsdf.com
            let captcha = formData.captcha; // 74234234
            
            function validateEmail(email) {
                var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
                return re.test(String(email).toLowerCase());
            }
            if (!(validateEmail(email))) { // Отправлен невалидный email
                alert(i18n.t('modals.login.badLogin'));
                $('#login-segment input[name=loginPassword]').val('');
                $('#login-segment input[name=captcha]').val('');
                requestCaptcha();
                return false;
            }

            var _data = JSON.stringify({email: email, captcha: captcha});

            function requestCaptcha() {
                $.ajax({
                    url: "/Services.asmx/GetCaptcha",
                    contentType: 'application/json; charset=utf-8',
                    method: 'POST',
                    data: "{}",
                    async: false,
                    success: function(n) {
                        $("#registerCaptcha_img").attr("src", "/Tools/CaptchaHandler.ashx?type=new&timestamp=" + (new Date).getTime());
                    },
                    error: function(n) {
                    if (n.status === 429)
                        alert(i18n.t('modals.registration.floodDetect'));
                    }
                })
            }

            $.ajax({
                type: "POST",
                url: "/Services.asmx/TryCreateUser",
                data: _data,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function(n) {
                    switch (String(n.d)) {
                    case "-100": // Sent empty fields
                            alert(i18n.t('modals.registration.empty'));
                            $('#register-segment input[name=captcha]').val('');
                            requestCaptcha();
                        break;
                    case "1": // Успешная регистрация
                        alert(i18n.t('modals.registration.success'));
                        $('#register-segment input[name=captcha]').val('');
                        break;
                    case "-4": // капча вообще не была выдана или чувак пытается перебирать - флуд
                        alert(i18n.t('modals.registration.badCaptcha'));
                        $('#register-segment input[name=captcha]').val('');
                        requestCaptcha();

                        break;
                    case "-1": // Запрос на создание емейла уже был
                        alert(i18n.t('modals.registration.repeatedRequest'));
                        $('#register-segment input[name=captcha]').val('');
                        requestCaptcha();
                        break;
                    case "-2": // Отправлен невалидный email
                        alert(i18n.t('modals.registration.badEmail'));
                        $('#register-segment input[name=captcha]').val('');
                        requestCaptcha();
                        break;
                    case "-5": // Блокированный домен
                        alert(i18n.t('modals.registration.blockedDomain'));
                        $('#register-segment input[name=captcha]').val('');
                        requestCaptcha();
                        break;
                    case "-3": // Неверная каптча
                        alert(i18n.t('modals.registration.badCaptcha'));
                        $('#register-segment input[name=captcha]').val('');
                        requestCaptcha();
                        
                    }
                    return false
                },
                error: function(n) {
                    switch (String(n.status)) {
                    case "429":
                        alert(i18n.t('modals.registration.floodDetect'));
                        break;
                    default:
                        alert(i18n.t('modals.registration.unknownError'));
                    }
                }
            })

        } // Login()

    }

});
