import Ember from 'ember';

export default Ember.Component.extend({
  share: Ember.inject.service('share'),
  tagName: '',
  isValidGive: Ember.computed.match('giveCurAmount', /\d/),
  isDisabledGive: Ember.computed.not('isValidGive'),
  i18n: Ember.inject.service(),
  didInsertElement: function(){
    const me = this;

    // $('.ui.dropdown#currency')
    //   .dropdown({
    //     onChange: function(value, text, $selectedItem) {
    //       let amount = $('#exchange-give').val();
    //         $.get( `https://indacoin.com/api/GetExchangerAmount/${value}/${amount}`, function( data ) {
    //           $( "#exchange-take" ).val( parseFloat(data).toFixed(6) );
    //         });
    //     }
    //   })
    // ;

    $('#eur-btn').on('click', function(){
      $('#usd-btn').removeClass('teal');
      $('#rub-btn').removeClass('teal');
      $(this).addClass('teal');
      $('#eur-row').show();
      $('#usd-row').hide();
      $('#rub-row').hide();
    });

    $('#usd-btn').on('click', function(){
      $('#eur-btn').removeClass('teal');
      $('#rub-btn').removeClass('teal');
      $(this).addClass('teal');
      $('#eur-row').hide();
      $('#usd-row').show();
      $('#rub-row').hide();
    })

    $('#rub-btn').on('click', function(){
      $('#eur-btn').removeClass('teal');
      $('#usd-btn').removeClass('teal');
      $(this).addClass('teal');
      $('#eur-row').hide();
      $('#usd-row').hide();
      $('#rub-row').show();
    })



    $('#exchange-give').on('keypress', function(){
      let cur = $('#currency.ui.dropdown').dropdown('get text');
      let amount = $(this).val();
      if (amount === '') {
        $(this).val('');
        $( "#exchange-take" ).val('');
      } else {
        $.get( `/api/GetExchangerAmount/${cur}/${amount}`, function( data ) {
          $( "#exchange-take" ).val( parseFloat(data).toFixed(6) );
        });
      }
    });

    $('#exchange-take').on('keyup', function(){
      let cur = $('#currency.ui.dropdown').dropdown('get text');
      let amount = $(this).val()
      if (amount === '') {
        $(this).val('');
        $( "#exchange-give" ).val('');
      } else {
        $.get( `/api/GetExchangerAmount/${cur}/3000`, function( data ) {
          let btc = data * amount * 3000;
          $( "#exchange-give" ).val( parseFloat(btc).toFixed(3) );
        });
      }
    });

    // $('#exchange-submit').on('click', function(){
    //   let curGive = $('#exchange-currency').html();
    //   let curTake = 'BTC'
    //   let amount = $('#giveCurAmount').val();
    //   let url = 'https://indacoin.com/change/buy-' + curTake + '-with-card' + curGive + '/?amount_pay=' + amount;
    //   window.location.replace(url);
    // });

    $('.ui.form#exchanger')
      .form({
          currencyGive: {
            identifier: 'currencyGive',
            rules: [{
              type: 'decimal',
              promt: 'enter decimal'
            }]
          },
          currencyTake: {
            identifier: 'currencyTake',
            rules: [{
              type: 'decimal',
              promt: 'enter decimal'
            }]
          }
      },
    {
      on: 'change',
      onInvalid: function(event, fields){
        $('#exchange-give').val('');
        $('#exchange-take').val('');
        $('#exchanger .error').removeClass('error');
        $('#exchange-submit').addClass('disabled');
      },
      onValid: function(event, fields){
        $('#exchange-submit').removeClass('disabled');
      }
    });


  },
  actions: {
    /*pullCurrency() {
      let cur = this.get('giveCur');
      let amount = this.get('giveCurAmount');

      if(amount === ''){
        $('#takeCurAmount').val('');
      } else {
        $.get( `https://indacoin.com/api/GetExchangerAmount/${cur}/${amount}`, function( data ) {
          $('#takeCurAmount').val(parseFloat(data).toFixed(6));

          // this.set('takeCurAmount', parseFloat(data).toFixed(6));
          // alert(this.get('takeCurAmount'));
        });
      }
    },*/

    // Срабатывает при изменении поля USD (keyup)
    pullCurrency() {
      let giveCur = Ember.$('#exchange-give-currency').text(); // USD
      let takeCur = Ember.$('#exchange-take-currency').text(); // BTC
      let amount = this.get('giveCurAmount'); // 50

      if (amount === '') {
        $('#takeCurAmount').val('');
      } else {
        $.get( `/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}`, function( data ) {
          $('#takeCurAmount').val(parseFloat(data).toFixed(6));
        });
      }
    },


    pullCurrencyCrypto() {
      let cur = this.get('giveCur');
      let amount = this.get('takeCurAmount');

      if(amount === ''){
        $('#giveCurAmount').val('');
      } else {
        let amount = $( "#takeCurAmount" ).val();
        $.get( `/api/GetExchangerAmount/${cur}/3000`, function( data ) {
          let btc = amount * 3000 / data;
          $( "#giveCurAmount" ).val( parseFloat(btc).toFixed(3) );
        });
      }
    },
    exchange() {

      try {
        ga('send', 'event', { eventCategory: 'Buttons', eventAction: 'Click', eventLabel: 'MainExchangeButton', eventValue: 1 });
      } catch (e) {
        console.log('Error ga: MainExchangeButton');
      }

      const i18n = this.get('i18n');
      const share = this.get('share');
      let region = share.getRegion(); // US
      if (region === null)
        region = "US";

      let giveCur = Ember.$('#exchange-give-currency').text(); // USD
      let takeCur = Ember.$('#exchange-take-currency').text(); // BTC
      let amount = this.get('giveCurAmount'); // 50

      //let url = 'https://indacoin.com/change/buy-' + curTake + '-with-card' + curGive + '/' + i18n.locale + '?amount_pay=' + amount;
      let url = "/" + i18n.locale + "_" + region + "/change/buy-bitcoin-with-cardusd" + '?amount_pay=' + amount;
      console.log(url);
      window.location.replace(url);
    },


    switchToUSD() {
      this.set('giveCur', 'USD');
      this.set('give', 'USD');
    },
    switchToEUR() {
      this.set('giveCur', 'EUR');
      this.set('give', 'EUR');
    },
    switchToRUB() {
      console.log('call switchToRUB()');
      this.set('giveCur', 'RUB');
      this.set('give', 'RUB');
    }

  }
});
