import Ember from 'ember';

export default Ember.Component.extend({
    actions: {
        changeRippleTag() {
            
            // Destination tag is not required; leave this field empty
            // Ripple tag is 32-bit value
            let rippleTag = $("#exchangeRippleTag").val();

            //if (isRipple(rippleTag))
                //$("#exchangeRippleTag").val();

            function isRipple(value) {
                if (isNaN(value))
                    return false;
                else if (!Number.isInteger(Number(value)))
                    return false;
                else if (value > 4294967296 || value < 0)
                    return false;
                else
                    return true;
            }
            

        }
    }
});
