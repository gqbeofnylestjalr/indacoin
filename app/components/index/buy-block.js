import Ember from 'ember';

export default Ember.Component.extend({
  tagname: ' ',
  classNameBindings: ['ui', 'center', 'aligned', 'basic', 'segment'],
  ui: true,
  center: true,
  aligned: true,
  segment: true,
  basic: true
});
