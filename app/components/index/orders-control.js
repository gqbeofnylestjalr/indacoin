import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
    share: Ember.inject.service('share'),
    actions: {
        checkOrder(order, orderConfirm) {
            console.log('order : ' + order);
            console.log('orderConfirm: ' + orderConfirm);

            let lang = this.get("share").getLanguage(); // Like 'ru', 'en
            let region = this.get("share").getRegion(); // Like 'US', 'RU'

            if (orderConfirm !== undefined && order !== undefined) {
                //let link = "https://indacoin.com/" +  Ember.get(ENV.i18n, 'lang') + "/change?confirm_code=" + orderConfirm + "&request_id=" + order;
                let link = Ember.get(ENV.i18n, 'lang') + "/change?confirm_code=" + orderConfirm + "&request_id=" + order;

                if (typeof FastBoot === 'undefined') {
                    window.location.href = link;
                }
            }
        },
        newOrder() {
            let lang = this.get("share").getLanguage(); // Like 'ru', 'en
            let region = this.get("share").getRegion(); // Like 'US', 'RU'
            if (typeof FastBoot === 'undefined') {
                //window.location.href = "https://indacoin.com/" + Ember.get(ENV.i18n, 'lang') + "/change/buy-bitcoin-with-cardusd";
                window.location.href = Ember.get(ENV.i18n, 'lang') + "/change/buy-bitcoin-with-cardusd";
            }
        }
    }
});
