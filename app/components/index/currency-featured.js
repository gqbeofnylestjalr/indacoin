import Ember from 'ember';
import ENV from 'fastboot/config/environment';

export default Ember.Component.extend({
  ENV: ENV,

  classNameBindings: ['ui', 'eight', 'wide', 'mobile', 'four', 'wide', 'tablet', 'four', 'wide', 'computer', 'column'],
  ui: true,
  four: true,
  eight: true,
  wide: true,
  column: true,
  mobile: true,
  tablet: true,
  computer: true,
});
