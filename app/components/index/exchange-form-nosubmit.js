import Ember from 'ember';
import fetch from 'fetch';

export default Ember.Component.extend({
  isValidGive: Ember.computed.match('giveCurAmount', /\d/),
  isDisabledGive: Ember.computed.not('isValidGive'),
  cookies: Ember.inject.service(),
  i18n: Ember.inject.service(),
  share: Ember.inject.service('share'),
  store: Ember.inject.service(),
  usdAvailable: false,
  eurAvailable: false,
  rubAvailable: false,
  audAvailable: false,
  gbpAvailable: false,

  limitsMin: 0,
  limitsMax: 0,
  limitsCur: 'USD',

  limitsWarning: false,
  cardWarning: false,

  noResults: Ember.computed('i18n.locale', function(){
    let i18n = this.get('i18n');
    $('#takeCur-select > div').dropdown('refresh');
    $('#takeCur-select > div').dropdown({ // ETH, XRP, etc
      message: {
        noResults: i18n.t('noResults').string
      }
    });
  }),

  didInsertElement: function() {

    var cookieService = this.get('cookies');
    let lang = cookieService.read('ipCountry');
    if (lang !== undefined) {
      if (lang.toUpperCase() === 'US') {
        $('#USBlock')
        .modal({
          closable  : false,
          onApprove : function() {
            console.log('Close');
          }
        })
        .modal('show');
      }
    }

    const me = this;
    const i18n = this.get('i18n');

    /* Валидация ввода и удаление постронних символов */
    let usd = Ember.$('#exchange-give');
    let btc = Ember.$('#exchange-take');

    let wallet = Ember.$('#exchangeOutAddress');
    // wallet.on("keyup", function(e) {
    //   let walletValue = wallet.val();
    //   console.log('call wallet keyup');
    //   console.log(e.value);
    //   if (walletValue.length > 0) {
    //     $('#createWalletCheckBox').prop('disabled', true); // "Создать новый кошелек"
    //     me.get("share").setCreateWalletCheckBox(false);
    //     $('#createWalletField').checkbox('uncheck'); // Галочка "Создать новый кошелек"
    //     me.get('share').setWallet(walletValue);
    //   } else {
    //     $('#createWalletCheckBox').prop('disabled', false); // "Создать новый кошелек"
    //     me.get("share").setCreateWalletCheckBox(true);
    //     me.get('share').setWallet(null);
    //   }
    // });

    usd.on("keypress", function(e) {
      let amount = $('#exchange-give').val();
      e = e || event;
      if (e.ctrlKey || e.altKey || e.metaKey) return;
      var chr = getChar(e);
      if ((chr === '.' || chr === ',') && (amount.indexOf('.') !== -1 || amount.indexOf(',') !== -1))
        return false;
      if (chr == null) return;
      if (chr === '.' || chr === ',') {}
      else if (chr < '0' || chr > '9') {
        return false;
      }
    });

    btc.on("keypress", function(e) {
      let amount = $('#exchange-take').val();
      e = e || event;
      if (e.ctrlKey || e.altKey || e.metaKey) return;
      var chr = getChar(e);
      if ((chr === '.' || chr === ',') && (amount.indexOf('.') !== -1 || amount.indexOf(',') !== -1))
        return false;
      if (chr == null) return;
      if (chr === '.' || chr === ',') {}
      else if (chr < '0' || chr > '9') {
        return false;
      }
    });

    try {
      this.get('store').peekAll('cashinfo').forEach(function(item){
        me.set('usdAvailable', item.get('data').usdAvailable);
        me.set('eurAvailable', item.get('data').eurAvailable);
        me.set('rubAvailable', item.get('data').rubAvailable);

        me.set('btcAvailable', item.get('data').btcAvailable);
        me.set('audAvailable', item.get('data').audAvailable);
        me.set('gbpAvailable', item.get('data').gbpAvailable);
      })
    } catch(e) {
      me.set('usdAvailable', false);
      me.set('eurAvailable', false);
      me.set('rubAvailable', false);

      me.set('btcAvailable', false);
      me.set('audAvailable', false);
      me.set('gbpAvailable', false);
    }
        
    // Установка поля "You give" из входящего запроса
    if (this.get('share').getCurrencyText().toUpperCase() === 'EUR') {
      this.set('giveCur', 'EUR');
      Ember.$('#curid').text('EUR');
      Ember.$('#curidusd').removeClass('active');
      Ember.$('#curideur').addClass('active');
      Ember.$('#curidrub').removeClass('active');
      Ember.$('#curidbtc').removeClass('active');
      Ember.$('#curidaud').removeClass('active');
      Ember.$('#curidgbp').removeClass('active');
    } else if (this.get('share').getCurrencyText().toUpperCase() === 'RUB') {
      this.set('giveCur', 'RUB');
      Ember.$('#curid').text('RUB');
      Ember.$('#curidusd').removeClass('active');
      Ember.$('#curideur').removeClass('active');
      Ember.$('#curidrub').addClass('active');
      Ember.$('#curidbtc').removeClass('active');
      Ember.$('#curidaud').removeClass('active');
      Ember.$('#curidgbp').removeClass('active');
    } else if (this.get('share').getCurrencyText().toUpperCase() === 'BTC') {
      this.set('giveCur', 'BTC');
      Ember.$('#curid').text('BTC');
      Ember.$('#curidusd').removeClass('active');
      Ember.$('#curideur').removeClass('active');
      Ember.$('#curidrub').removeClass('active');
      Ember.$('#curidbtc').addClass('active');
      Ember.$('#curidaud').removeClass('active');
      Ember.$('#curidgbp').removeClass('active');
    } else if (this.get('share').getCurrencyText().toUpperCase() === 'AUD') {
      this.set('giveCur', 'AUD');
      Ember.$('#curid').text('AUD');
      Ember.$('#curidusd').removeClass('active');
      Ember.$('#curideur').removeClass('active');
      Ember.$('#curidrub').removeClass('active');
      Ember.$('#curidbtc').removeClass('active');
      Ember.$('#curidaud').addClass('active');
      Ember.$('#curidgbp').removeClass('active');
    } else if (this.get('share').getCurrencyText().toUpperCase() === 'GBP') {
      this.set('giveCur', 'GBP');
      Ember.$('#curid').text('GBP');
      Ember.$('#curidusd').removeClass('active');
      Ember.$('#curideur').removeClass('active');
      Ember.$('#curidrub').removeClass('active');
      Ember.$('#curidbtc').removeClass('active');
      Ember.$('#curidaud').removeClass('active');
      Ember.$('#curidgbp').addClass('active');
    } else {
      this.set('giveCur', 'USD');
      Ember.$('#curid').text('USD');
      Ember.$('#curidusd').addClass('active');
      Ember.$('#curideur').removeClass('active');
      Ember.$('#curidrub').removeClass('active');
      Ember.$('#curidbtc').removeClass('active');
      Ember.$('#curidaud').removeClass('active');
      Ember.$('#curidgbp').removeClass('active');
    }

    $('#takeCur-select > div').dropdown('refresh');
    // Set dropdown menu item
    if (Ember.leftCur !== "") {
      this.set('currency', Ember.leftCur);
      //$("#coinid").replaceWith( "<span id=\"coinid\" class=\"text\"><span class=\"coinFullName\">" + Ember.leftCur +"</span><span class=\"right-part\"><span class=\"scrolling-short-cur-img\"><img src=\"/ember/images/cryptocoins/" + Ember.currencyText + ".png\" class=\"ui small image\"></span><span class=\"scrolling-short-cur\">" + Ember.currencyText + "</span></span></span>" );
      $("#coinid").replaceWith( "<span id=\"coinid\" class=\"text\"><span class=\"coinFullName\">" + Ember.leftCur + "</span><span class=\"right-part\"><span class=\"scrolling-short-cur-img\"><span style=\"position: absolute;\" class=\"sprite crypto-icon sprite-" + Ember.currencyText + "\"></span></span><span class=\"scrolling-short-cur\">" + Ember.currencyText + "</span></span></span>" );
      //TODO: change to native code, not hack $('#takeCur-select > div').dropdown('set selected', 'Neo');
      if (Ember.leftCur.toLowerCase() === 'bitcoin' || Ember.leftCur.toLowerCase() === 'ethereum') {
        $('#takeCur-select > div').dropdown({ // BTC
          action: 'activate',
          onShow: function() {},
          scrollUp: function() {},
          fullTextSearch: "exact"
        }).dropdown('set selected', Ember.leftCur).dropdown('show');
      } else {
        $('#takeCur-select > div').dropdown({ // ETH, XRP, etc
          action: 'activate',
          fullTextSearch: "exact",
          message: {
            noResults: i18n.t('noResults').string
          }
        }).dropdown('set selected', Ember.leftCur);
      }
    } else {
      $('#takeCur-select > div').dropdown({}); // BTC
    }
    
    $('#giveCur-select > div').dropdown({}); // USD
    $('#customized-destination').dropdown({});

    let amount_pay = this.get('share').getAmountPay();
    let amount_num = this.get('share').getAmountNum();
    if (amount_pay !== null) {
      $("#exchange-give").val(amount_pay);
      //this.get('share').setAmountPay(null);
      this.send('pullCurrency');
    } else if (amount_num !== null) {
      $("#exchange-take").val(amount_num);
      this.send('pullCurrencyCrypto');
    }


    // *********** ETN notif ***************

    if (Ember.currencyText.toUpperCase() === 'ETN') {
      (function($) {
        $.getStylesheet = function (href) {
          var $d = $.Deferred();
          var $link = $('<link/>', {
            rel: 'stylesheet',
            type: 'text/css',
            href: href
          }).appendTo('head');
          $d.resolve($link);
          return $d.promise();
        };
      })(jQuery);

      $.when($.getStylesheet('/assets/exclude/styles/notifit.min.css'), $.getScript('/assets/exclude/scripts/notifit.min.js'))
      .then(function () {

        var myNotification = notif({
          'type': 'error', 'position': 'center', 'opacity': 0.9,
          'msg': 'Please use a non hit-btc wallet address for ETN'
        });

      }, function () {
        //console.log('an error occurred somewhere');
      });
    }
    // *********** End ETN notif ****************

    // Show Ripple tag field
    let controller = Ember.getOwner(this).lookup('controller:lang/change');
    var isTag = false;
    this.get('store').peekAll('value').forEach( function(item) {
      if (item.get('short_name').toLowerCase().indexOf(Ember.currencyText.toLowerCase()) !== -1)
        if (item.get('tag'))
          isTag = true;
    });
    if (isTag)
      controller.set('isRipple', true);
    else
      controller.set('isRipple', false);

    if (Ember.currencyText.toUpperCase() === 'BTC')
      controller.set('isBitcoin', true);
    else
      controller.set('isBitcoin', false);

      try {
        this.get('store').peekAll('cashinfo').forEach(function(item){

          let limits = [];
          limits['usdmin'] = item.get('data').usdmin;
          limits['usdmax'] = item.get('data').usdmax;
          //limits['usdmax'] = 1000; //result.cashTypes.cashIn['16'].fee.maxSum;

          limits['eurmin'] = item.get('data').eurmin;
          //limits['eurmax'] = 1000;//result.cashTypes.cashIn['50'].fee.maxSum;
          limits['eurmax'] = item.get('data').eurmax;

          limits['rubmin'] = item.get('data').rubmin;
          limits['rubmax'] = item.get('data').rubmax;
          //limits['rubmax'] = 180000;//result.cashTypes.cashIn['54'].fee.maxSum;
  
          limits['btcmin'] = 0.01;
          limits['btcmax'] = 0.2;//result.cashTypes.cashIn['54'].fee.maxSum;

          limits['audmin'] = item.get('data').audmin;
          limits['audmax'] = item.get('data').audmax;
          //limits['audmax'] = 1000;
          limits['gbpmin'] = item.get('data').gbpmin;
          limits['gbpmax'] = item.get('data').gbpmax;
          //limits['gbpmax'] = 1000;
  
          var cookieService = me.get('cookies');
          if (cookieService.read('discount') == '82413') {
            limits['usdmin'] = 20;
            limits['eurmin'] = 20;
            limits['rubmin'] = 1000;
            limits['btcmin'] = 0.01;
            limits['audmin'] = 20;
            limits['gbpmin'] = 20;
          } else if (cookieService.read('discount') == '1000') {
            limits['usdmin'] = 20;
            limits['eurmin'] = 20;
            limits['rubmin'] = 1500;
            limits['btcmin'] = 0.01;
            limits['audmin'] = 20;
            limits['gbpmin'] = 20;
          }
  
          me.get('share').setLimits(limits);
          
          me.get('share').setCurrentUsd('67.58');
          me.get('share').setCurrentEur('79.05');

        })
      } catch(e) {
        let limits = [];
        limits['usdmin'] = 30;
        limits['usdmax'] = 1000;//result.cashTypes.cashIn['16'].fee.maxSum;
        limits['eurmin'] = 30;
        limits['eurmax'] = 1000;//result.cashTypes.cashIn['50'].fee.maxSum;
        limits['rubmin'] = 2000;
        limits['rubmax'] = 180000;//result.cashTypes.cashIn['54'].fee.maxSum;

        limits['btcmin'] = 0.01;
        limits['btcmax'] = 0.2;//result.cashTypes.cashIn['54'].fee.maxSum;

        limits['audmin'] = 30;
        limits['audmax'] = 1000;
        limits['gbpmin'] = 30;
        limits['gbpmax'] = 1000;

        me.get('share').setLimits(limits);
        
        me.get('share').setCurrentUsd('67.58');
        me.get('share').setCurrentEur('79.05');
      }
    
    function isNumber(param)
    {
        if (param == 0)
            return true;
        return (param / param) ? true : false;
    }

    function func() {
      let currency = me.get('share').getAmountOut(); // USD
      let max;
      let min;
      let limits = me.get('share').getLimits(); // max = 1000

      if (currency.toUpperCase() === 'USD') {
        max = limits['usdmax'];
        min = limits['usdmin'];
        //max = 1000;
        //min = 50;
      }
      else if (currency.toUpperCase() === 'EUR') {
        max = limits['eurmax'];
        min = limits['eurmin'];
        //max = 10000;
      }
      else if (currency.toUpperCase() === 'RUB') {
        max = limits['rubmax'];
        min = limits['rubmin'];
        //max = 10000;
      }
      else if (currency.toUpperCase() === 'AUD') {
        max = limits['audmax'];
        min = limits['audmin'];
        //max = 10000;
      }
      else if (currency.toUpperCase() === 'GBP') {
        max = limits['gbpmax'];
        min = limits['gbpmin'];
        //max = 10000;
      }
      else {
        max = limits['btcmax'];
        min = limits['btcmin'];
        //max = 600000;
      }
      
      let amount = $('#exchange-give').val();
      if (amount < min) {
        me.set('limitsWarning', true);
        me.set('limitsMin', min);
        me.set('limitsMax', max);
        me.set('limitsCur', currency.toUpperCase());
      }
      // TODO: card_limit
      
      else if (amount > max) {
        me.set('limitsWarning', true);
        me.set('limitsMin', min);
        me.set('limitsMax', max);
        me.set('limitsCur', currency.toUpperCase());
      }
      else
        me.set('limitsWarning', false);
    }

    function func2() {
      setTimeout(func, 300);
    }

    $('#exchange-give').on('keyup', func2);
    $('#exchange-take').on('keyup', func2);

    // Loading effect
    $('#steps-content').removeClass('hide-content');
    $('#loader').removeClass('active');

    $('.ui.form#exchanger')
      .form({
          currencyGive: {
            identifier: 'currencyGive',
            rules: [{
              type: 'decimal',
              promt: 'enter decimal'
            }]
          },
          currencyTake: {
            identifier: 'currencyTake',
            rules: [{
              type: 'decimal',
              promt: 'enter decimal'
            }]
          }
      },
    {
      on: 'change',
      onInvalid: function(event, fields){
        $('#exchange-give').val('');
        $('#exchange-take').val('');
        $('#exchanger .error').removeClass('error');
        $('#exchange-submit').addClass('disabled');
      },
      onValid: function(event, fields){
        $('#exchange-submit').removeClass('disabled');
      }
    });

    // Notification if pick NEO about can buy only integer amounts
    if (Ember.currencyText.toUpperCase() === 'NEO') {
      $('#neoPickModal')
      .modal({
        closable  : false,
        onApprove : function() {
        }
      })
      .modal('show');
    }

    $('#qwerty').keyup(()=> {
      let val = $('#qwerty').val();
      me.set('filter', val);
    });

  },

  _performSearch(term, resolve, reject) {
    let url = `https://indacoin.com/api/mobgetcurrenciesinfoi/1`;
    fetch(url).then((resp) => resp.json()).then((json) => resolve(json.items), reject);
  },

  scrollLeft: 0,
  scrollTop: 0,
  flag: false,

  lastGiveAmount: 0,
  lastTakeAmount: 0,

  filter: '',
  dropdownCurrency: 'USD',

  filteredBooks: Ember.computed('filter', 'dropdownCurrency', function() {
    let filterTerm = this.get('filter');
    let currency = this.get('dropdownCurrency');

    let showBitcoin = false;
    if (currency === 'BTC')
      showBitcoin = true;

    if (showBitcoin) {
      if (filterTerm.length == 0) {
        var filtered = this.get('store').peekAll('value').filter( function(item) {
          if (!(item.get('short_name').toLowerCase() === 'btc' || item.get('short_name').toLowerCase() === 'eth')) {
            return true;
          }
        });
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('value').filter( function(item) {

          if (item.get('short_name').toLowerCase() !== 'btc') {
            if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
              if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
                noDoubledArray.push(item.get('short_name'));
                return true;
              }
            }
          }

        });
      }
    } else {
      if (filterTerm.length == 0) {
        var filtered= this.get('store').peekAll('value');
      }
      else {
        let noDoubledArray = [];
        var filtered = this.get('store').peekAll('value').filter( function(item) {
          if ((item.get('name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1) || (item.get('short_name').toLowerCase().indexOf(filterTerm.toLowerCase()) !== -1)) {
            if (noDoubledArray.indexOf(item.get('short_name')) === -1) {
              noDoubledArray.push(item.get('short_name'));
              return true;
            }
          }
        });
      }
    }
    return filtered;
  }),

  actions: {
    scrollChange(scrollLeft, scrollTop) {
      this.set('scrollLeft', scrollLeft);
      this.set('scrollTop', scrollTop);
    },
    searchRepo(term) {
      term='BT';
      //debugger;
      if (this.get('filter').length > 0)
        this.set('filter','');
      else
        this.set('filter','BT');
      /* 
      let mystore = this.get('store');
      let currencies = mystore.peekAll('value');
      let res = [];
      currencies.forEach(function(item){
        console.log(item.get('name'));
        
      if (item.get('name').toLowerCase().indexOf(term.toLowerCase()) !== -1)
      {
       let newItem= mystore.createRecord('value', {
          name: item.name,
          short_name: item.short_name
        });
        console.log(item.get('short_name'));

        res.push(newItem);
      }     
      // .cur_id === altCurrencyId) {
      //     currencyId = item.get('data').short_name;
      //   }
      })


      this.set('model.values', res);
      console.log('Поиск по строке: ' + term); */
      //console.log(res);
      return 0;
      // this.get('store').peekAll('value').forEach(function(item){
      //   if (item.get('data').cur_id === altCurrencyId) {
      //     currencyId = item.get('data').short_name;
      //   }
      // })

      //let url = `https://api.github.com/search/repositories?q=${term}`;
      // let url = `https://indacoin.com/api2/mobgetcurrencies/`;
      // return fetch(url).then((resp) => resp.json()).then(
      //   (json) => {
      //     console.log(json);
      //     let cur = this.get('model.value');
      //     console.log(cur);

      //     return json;
      //   }
      // );


      
    },

    /**/
    pullCurrency() {
      console.log('call pullCurrency');
      let giveCur = $('#giveCur-select > div').dropdown('get text'); // EUR
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      let amount = $('#exchange-give').val(); // 1000

      let partner = "";
      if (this.get("share").getPartner()) {
        partner = "/" + this.get("share").getPartner();
      }
      if (amount === '') {
        $( "#exchange-take" ).val('');
      } else {
      
        let lastGiveAmount = this.get('lastGiveAmount');
        if (lastGiveAmount == amount)
          return;
        else
          this.set('lastGiveAmount', amount);
        amount = amount.replace(',', '.');
        $.get( `https://indacoin.com/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}${partner}`, function( data ) {
          if (data < 0)
            data = 0.00000000;
          $("#exchange-take").val(parseFloat(data).toFixed(6));
        });

      }
    },



    // Срабатывает при изменении поля BTC (keyup)
    pullCurrencyCrypto() {
      console.log('call pullCurrencyCrypto');
      const me = this;
      let giveCur = $('#giveCur-select > div').dropdown('get text'); // EUR
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC
      let amount = $( "#exchange-take" ).val(); // "0.122031"
      let partner = "";
      if (this.get("share").getPartner()) {
        partner = "/" + this.get("share").getPartner();
      }
      if (amount === '') {
        $( "#exchange-give" ).val('');
      } else {


          let lastTakeAmount = this.get('lastTakeAmount');
          if (lastTakeAmount == amount)
            return;
          else
            this.set('lastTakeAmount', amount);
          
          amount = amount.replace(',', '.');
          $.get( `https://indacoin.com/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/${amount}${partner}`, function( data ) {
            if (data < 0)
              data = 0.00000000;
            if (giveCur === 'BTC')  
              $( "#exchange-give" ).val( parseFloat(data).toFixed(4) );
            else
              $( "#exchange-give" ).val( parseFloat(data).toFixed(2) );
          });
      }
    },

    // Срабатывает при изменении выпадающего списка поля USD
    switchGiveCur() {
      console.log('call switchGiveCur');
      const me = this;
      let giveCur = $('#giveCur-select > div').dropdown('get text');
      let amount = $('#exchange-give').val();
      let takeCur = $('#coinid .scrolling-short-cur').text(); // BTC

      if (amount.length > 0)
        if (giveCur === 'BTC') {
          if (Number(amount) > 1)
            $('#exchange-give').val('0.1000');
          else {
            $('#exchange-give').val(
              parseFloat(amount).toFixed(4)
            );
          }
        } else {
          $('#exchange-give').val(
            parseFloat(amount).toFixed(2)
          );
        }

      if (giveCur == 'BTC')
        if (takeCur == 'BTC' || takeCur == 'ETH') {
          $('#takeCur-select > div').dropdown('set selected', 'BitAir');
          $('#takeCur-select > div').dropdown('set selected', 'Ripple');
          $('#takeCur-select > div').dropdown('set selected', 'Ethereum Dark');
          $('#cur-dropdown').dropdown('show');
          if ($('#takeCur-select .menu').css('display') !== 'none') {
            $('#etc').show();
            $('#qwerty').show();
            $('#takeCur-select .menu').show();
          } else {
            $('#etc').hide();
            $('#qwerty').hide();
            $('#takeCur-select .menu').hide();
          }
        }
      // else {
      //   $('.you-have-field input').val(
      //     parseFloat(amount).toFixed(2)
      //   );
      // }



      

      this.set('dropdownCurrency', giveCur);
      let lastGiveCur = this.get('share').getAmountOut();

      this.get('share').setAmountOut(giveCur);
      this.set('giveCur', giveCur);
      let amountNum = this.get("share").getAmountNum(); // Блок формы когда приходят get-параметры с суммой крипты

      // Для костыля с двумя дублирующимися маршрутами: change и lang/change
      let contName = Ember.getOwner(this).lookup('controller:application').currentPath; // lang.index lang.change
      let controller = null;

      let partner = "";
      if (this.get("share").getPartner()) {
        partner = "/" + this.get("share").getPartner();
      }

      if (contName === 'change')
        controller = Ember.getOwner(this).lookup('controller:change');
      else
        controller = Ember.getOwner(this).lookup('controller:lang/change');
      controller.set('buyingLeftTitleText2', giveCur);

      if (amountNum === null) {
        if(amount === ''){
          $('#takeCurAmount').val('');
        } else {
            amount = amount.replace(',', '.');
            $.get( `https://indacoin.com/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}${partner}`, function( data ) {
              if (data < 0)
                data = 0.00000000;
              $('#exchange-take').val(parseFloat(data).toFixed(6));
            });
        }

      } else {
        amount = amount.replace(',', '.');
        $.get( `https://indacoin.com/api/GetCoinConvertAmountOut/${giveCur}/${takeCur}/${amountNum}${partner}`, function( data ) {
          if (data < 0)
            data = 0.00000000;
          $( "#exchange-give" ).val( parseFloat(data).toFixed(2) );
        });
      }

      // Смена валюты в title
      this.get('share').setInCur(giveCur);
      let linkToRedirect = this.get('share').getLink();
      window.history.replaceState( {} , linkToRedirect, linkToRedirect );
      this.set('limitsWarning', false);



      let cashinfo = me.get('store').peekRecord('cashinfo', 1);
      let commentPath = cashinfo.get(giveCur.toLowerCase() + 'CommentPath');
      if (commentPath) {

        $.get( `https://indacoin.com/api/gettranslations`, function( data ) {
          let lang = me.get('i18n.locale');
          if (!(lang === 'en' || lang === 'ru'))
            lang = 'en';
          // TODO: получать сообщения из пути
          me.set('cardWarning', data.CashTypes.Deposit.MasterCardProblem[lang]);
        });

        
      }
      else
        me.set('cardWarning', false);
    },

    // Срабатывает при изменении выпадающего списка поля BTC
    switchTakeCur() {
      console.log('call switchTakeCur');

      const share = this.get('share');
      const i18n = this.get('i18n');

      //let cur = $('#takeCur-select > div').dropdown('get value');
      let cur = $('#coinid > .coinFullName').text();
      this.set('currency', cur);

      // Обновление поля BTC
      let giveCur = $('#giveCur-select > div').dropdown('get text'); // USD
      let takeCur = $('#coinid .scrolling-short-cur').text();
      let amount = $('#exchange-give').val();

      let partner = "";
      if (this.get("share").getPartner()) {
        partner = "/" + this.get("share").getPartner();
      }

      if (amount === '') {
        $( "#exchange-take" ).val('');
      } else {
        //$.get( `/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}`, function( data ) {
        amount = amount.replace(',', '.');
        $.get(`https://indacoin.com/api/GetCoinConvertAmount/${giveCur}/${takeCur}/${amount}${partner}`, function(data) {
          if (data < 0)
            data = 0.00000000;
          $("#exchange-take" ).val(parseFloat(data).toFixed(6));
        });
      }

      let contName = Ember.getOwner(this).lookup('controller:application').currentPath; // lang.index lang.change

      // Для костыля с двумя дублирующимися маршрутами: change и lang/change
      let controller = null;
      if (contName === 'change')
        controller = Ember.getOwner(this).lookup('controller:change');
      else
        controller = Ember.getOwner(this).lookup('controller:lang/change');
      controller.set('buyingLeftTitleText', takeCur);

      // Notification if pick NEO about can buy only integer amounts
      if (takeCur.toUpperCase() === 'NEO') {
        $('#neoPickModal')
        .modal({
          closable  : false,
          onApprove : function() {
          }
        })
        .modal('show');
      }

      // *********** ETN notif ***************
      if (takeCur.toUpperCase() === 'ETN') {
            (function($) {
              $.getStylesheet = function (href) {
                var $d = $.Deferred();
                var $link = $('<link/>', {
                  rel: 'stylesheet',
                  type: 'text/css',
                  href: href
                }).appendTo('head');
                $d.resolve($link);
                return $d.promise();
              };
            })(jQuery);

            $.when($.getStylesheet('/assets/exclude/styles/notifit.min.css'), $.getScript('/assets/exclude/scripts/notifit.min.js'))
            .then(function () {

              var myNotification = notif({
                'type': 'error', 'position': 'center', 'opacity': 0.9,
                'msg': 'Please use a non hit-btc wallet address for ETN'
              });

            }, function () {
              //console.log('an error occurred somewhere');
            });
      }
      // *********** End ETN notif ****************

      // Ripple tag field
      var isTag = false;
      this.get('store').peekAll('value').forEach( function(item) {
        if (item.get('short_name').toLowerCase().indexOf(takeCur.toLowerCase()) !== -1)
          if (item.get('tag'))
            isTag = true;
      });
      if (isTag)
        controller.set('isRipple', true);
      else
        controller.set('isRipple', false);

      // Bitcoins wallet fields
      if (takeCur.toUpperCase() === 'BTC')
        controller.set('isBitcoin', true);
      else
        controller.set('isBitcoin', false);
      
      // Dropping disable checkbox on change crypto
      //$('#exchangeOutAddress').prop('disabled', false);
      
      // Смена криптовалюты в title
      this.get('share').setOutCur(cur);

      // Обнуляем поле "Я хочу создать биткоин-кошелек на Indacoin" для других валют и галочку "Согласен с условиями"
      //this.get("share").setCreateWalletCheckBox(false);
      //this.get("share").setAcceptAgreementCheckBox(false);
      
      let linkToRedirect = this.get('share').getLink();
      window.history.replaceState( {} , linkToRedirect, linkToRedirect );
    }
  }
});

function isNumber(param)
{
    if (param == 0)
        return true;
    return (param / param) ? true : false;
}


function getChar(event) {
  if (event.which == null) {
    if (event.keyCode < 32) return null;
    return String.fromCharCode(event.keyCode) // IE
  }

  if (event.which != 0 && event.charCode != 0) {
    if (event.which < 32) return null;
    return String.fromCharCode(event.which) // остальные
  }

  return null; // специальная клавиша
}