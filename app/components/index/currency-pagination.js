import Ember from 'ember';

export default Ember.Component.extend({
  didInsertElement: function(){
  $(function(){
    $('#currencyGive a button').on('click', function(){
      $('#currencyGive a button')
        .removeClass('teal')
        .addClass('disabled')
      ;
      $(this)
        .addClass('loading')
        .removeClass('disabled')
      ;
    });

    $('#currency td').on('click', function(){
      $(this).addClass('active');
      $('#loading').show();
    });
  });
},
// afterModel: function(){
//   $(function(){
//     // $('#moreButton').removeClass('loading');
//     $('#currencyGive a button').removeClass('loading');
//     $('#loading').hide();
//   });
// }
});
