var express = require('express');
var mongoose = require('mongoose');
//const request = require("request");
var syncRequest = require('sync-request');
var thenRequest = require('then-request');

const requestIp = require('request-ip');

var app = express();

app.use(function(req, res, next) {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
    next();
});

var languages = ['de', 'ar', 'en', 'es', 'fr', 'it', 'pt', 'ru', 'tr'];

var time = 0; // api/mobgetcurrenciesinfoi/1
var getArticlesCacheTime = {'de': 0, 'ar': 0, 'en': 0, 'es': 0, 'fr': 0, 'it': 0, 'pt': 0, 'ru': 0, 'tr': 0}; // api/getarticles/en
var getArticlesConcreteCacheTime = {'de': {}, 'ar': {}, 'en': {}, 'es': {}, 'fr': {}, 'it': {}, 'pt': {}, 'ru': {}, 'tr': {}}; // api/getarticles/en/bullish_run
var getDataCacheTime = {
    '/api/GetExchangerAmount/USD/100': 0,
    '/api/GetExchangerAmount/EUR/100': 0,
    '/api/GetExchangerAmount/RUB/3000': 0,
    '/change.aspx/getCashInfo': 0
};

// Settings
var SERVER_TIMEOUT = 2000;


var ENV = 'debug';
//var ENV = 'devel';
if (ENV === 'prod') {
    TIME_1 = 60 * 3; // 5 minutes mobGetCurrency
    //TIME_1 = 3; // 5 minutes mobGetCurrency
    TIME_2 = 60 * 5; // etarticles/en
    TIME_3 = 60 * 5; // getarticles/en/bullish_run

    TIME_4 = 60 * 3; // /api/GetExchangerAmount/USD/100
    TIME_5 = 60 * 3; // /api/GetExchangerAmount/EUR/100
    TIME_6 = 60 * 3; // /api/GetExchangerAmount/RUB/3000

    TIME_7 = 60 * 3; // /change.aspx/getCashInfo
} else if (ENV === 'dev'){
    TIME_1 = 3; // 5 minutes mobGetCurrency
    TIME_2 = 3; // etarticles/en
    TIME_3 = 3; // getarticles/en/bullish_run

    TIME_4 = 1; // /api/GetExchangerAmount/USD/100
    TIME_5 = 3; // /api/GetExchangerAmount/EUR/100
    TIME_6 = 3; // /api/GetExchangerAmount/RUB/3000

    TIME_7 = 3; // /change.aspx/getCashInfo
} else {
    var TIME_1 = 0; // 5 minutes mobGetCurrency
    var TIME_2 = 0; // etarticles/en
    var TIME_3 = 0; // getarticles/en/bullish_run
    
    var TIME_4 = 0; // /api/GetExchangerAmount/USD/100
    var TIME_5 = 0; // /api/GetExchangerAmount/EUR/100
    var TIME_6 = 0; // /api/GetExchangerAmount/RUB/3000
    
    var TIME_7 = 0; // /change.aspx/getCashInfo
}

// Contents cached data
var cachedMobgetcurrencies = undefined;
var cachedGetArticles = { // getarticles/en/bullish_run
    'de': {},
    'ar': {},
    'en': {},
    'es': {},
    'fr': {},
    'it': {},
    'pt': {},
    'ru': {},
    'tr': {}
};
var cachedData = {};
var cachedCashInfo = undefined;

//mongoose.connect('localhost://127.0.0.1:27017/emberData');
//mongoose.connect('mongodb://127.0.0.1:27017/emberData');
//mongoose.connect('mongodb://localhost/emberData');
//mongoose.connect('localhost','emberData');
//mongoose.connect('localhost/emberData');

// const options = {
//     autoReconnect: true,
//     socketOptions: {
//         keepAlive: 500,
//         connectTimeoutMS: 500,
//         auto_reconnect: true
//       }
// };

const options = {
    autoReconnect: false,
    socketOptions: {
        keepAlive: true,
        connectTimeoutMS: 10500,
        auto_reconnect: false
      }
};

mongoose.connect('mongodb://localhost/EmberData', options);
//mongoose.connect('mongodb://localhost/EmberData', { autoReconnect:true });

var noteSchema = new mongoose.Schema({
 title: 'string',
 content: 'string',
 author: 'string'
});

var articleSchema = new mongoose.Schema({
    content: 'string',
    lang: 'string'
});

var titleSchema = new mongoose.Schema({
    content: 'string',
    route: 'string' // en/bullish_run
});

var dataSchema = new mongoose.Schema({
    content: 'string',
    route: 'string' // en/bullish_run
});

/*mongo
use EmberData
db.notes.insert("[{'title': 'title1', 'content':'123', 'author':'michael'}]");

db.notes.find()*/

var NoteModel = mongoose.model('note', noteSchema); // mobgetcurrencies
var ArticleModel = mongoose.model('article', articleSchema); // getarticles/en
var TitleModel = mongoose.model('title', titleSchema); // getarticles/en/bullish_run
var DataModel = mongoose.model('data', titleSchema); // api/GetExchangerAmount/USD/100



// app.get('/change.aspx/getCashInfo2', function(req,res) {
//     const path = '/change.aspx/getCashInfo';
//     console.log('**********************');
//     console.log('getting ' + path);
//     console.log('**********************');
//     let currentTime = new Date().getTime();
//     let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
//     res.type('json');

//         console.log('Кеш просрочен: ' + path);
//         getDataCacheTime[path] = currentTime;

//             thenRequest('POST', 'https://indacoin.com/change.aspx/getCashInfo', {
//                 timeout: SERVER_TIMEOUT,
//                 headers: {
//                     'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
//                     'content-type': "application/json; charset=utf-8",
//                     'content-length':'0',
//                     'accept':"application/json, text/javascript, */*; q=0.01",
//                     'origin': "https://indacoin.com",
//                     'X-Requested-With':'XMLHttpRequest'
//                   }
//             }).done(function (data) {
//                 let content = data.getBody();
//                 console.log(content);
//                 console.log('status code = ' + data.statusCode);
//                 res.header("Content-Type", "text/html; charset=utf-8");
//                 res.send(content);
//                 return 0;
//             });
// });


// Test case in async req
app.get('/change.aspx/getCashInfo2', function(req,res) {
    const path = '/change.aspx/getCashInfo';
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_7) {
        //console.log('Кеш просрочен: ' + path);
        getDataCacheTime[path] = currentTime;
        try {
            thenRequest('POST', 'https://indacoin.com/change.aspx/getCashInfo', {
                timeout: SERVER_TIMEOUT,
                headers: {
                    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
                    'content-type': "application/json; charset=utf-8",
                    'content-length':'0',
                    'accept':"application/json, text/javascript, */*; q=0.01",
                    'origin': "https://indacoin.com",
                    'X-Requested-With':'XMLHttpRequest'
                  }
            }).done(function (data) {
                if (String(data.statusCode) === '200' || String(data.statusCode) === '304') {
                    var request = data.getBody();
                    console.log('Ответ получен:');
                    cachedData[path] = request;
                    var query = { route: path };
                    var newData = { route: path, content: request };
                    DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                        if (err) { // Если ошибка
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (doc) { // Если найдена запись
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(doc.content);
                            return 0;
                        } else { // Иначе создаем новую
                            var record = new DataModel({ route: path, content: 'content for ' + path });
                            record.save(function(error) {
                                if (error) {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send("Error save data" + error);
                                    return 0;
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + path);
                                    return 0;
                                }
                            });
                        }
                    });
                } else { // Если запрос к API неудачен
                    if (cachedData[path] === undefined) {
                        DataModel.findOne({ route: path }, function(err, docs) {
                            if (err) {
                                res.send(500, {error: err});
                                return 0;
                            }
                            if (docs) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send(docs.content);
                                return 0;
                            } else {
                                var record = new DataModel({ route: path, content: 'content for ' + path });
                                record.save(function(error) {
                                    if (error) {
                                        console.log("Error save data" + error);
                                    } else {
                                        res.header("Content-Type", "text/html; charset=utf-8");
                                        res.send('record save with path ' + path);
                                        return 0;
                                    }
                                });
                            }
                        });
                    } else {
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(cachedData[path]);
                        return 0;
                    }
                }
            });
        } catch (e) {
            console.log('error = ' + e);
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        }
    } else {
        console.log('Кеш не просрочен ' + elapsedTime);
        if (cachedData[path] === undefined) {
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedData[path]);
            return 0;
        }
    }
});


app.get('/change.aspx/getCashInfo', function(req,res) {
    const path = '/change.aspx/getCashInfo';
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_7) {
        getDataCacheTime[path] = currentTime;
        try {
            var query = syncRequest('POST', 'https://indacoin.com/change.aspx/getCashInfo', {
                timeout: SERVER_TIMEOUT,
                headers: {
                    'user-agent': 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36',
                    'content-type': "application/json; charset=utf-8",
                    'content-length':'0',
                    'accept':"application/json, text/javascript, */*; q=0.01",
                    'origin': "https://indacoin.com",
                    'X-Requested-With':'XMLHttpRequest'
                  }
            });
            console.log();
            console.log('Ответ получен sync: ' + query.statusCode + ' with IP = ' + req.headers['log']); //requestIp.getClientIp(req));
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var request = query.getBody();

                cachedData[path] = request;
                var query = { route: path };
                var newData = { route: path, content: request };
                DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) { // Если ошибка
                        res.send(500, {error: err});
                        return 0;
                    }
                    if (doc) { // Если найдена запись
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(doc.content);
                        return 0;
                    } else { // Иначе создаем новую
                        var record = new DataModel({ route: path, content: 'content for ' + path });
                        record.save(function(error) {
                            if (error) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send("Error save data" + error);
                                return 0;
                            } else {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send('record save with path ' + path);
                                return 0;
                            }
                        });
                    }
                });
            } else { // Если запрос к API неудачен
                console.log('Нет ответа sync');
                if (cachedData[path] === undefined) {
                    DataModel.findOne({ route: path }, function(err, docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (docs) {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        } else {
                            var record = new DataModel({ route: path, content: 'content for ' + path });
                            record.save(function(error) {
                                if (error) {
                                    console.log("Error save data" + error);
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + path);
                                    return 0;
                                }
                            });
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedData[path]);
                    return 0;
                }
            }
        } catch (e) {
            console.log('error = ' + e);
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        }
    } else {
        console.log('Кеш не просрочен ' + elapsedTime);
        if (cachedData[path] === undefined) {
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedData[path]);
            return 0;
        }
    }
});



app.get('/api/GetExchangerAmount/USD/100', function(req,res) {
    const path = '/api/GetExchangerAmount/USD/100';
    console.log('getting ' + path);
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_4) {
        console.log('Кеш просрочен: ' + path);
        getDataCacheTime[path] = currentTime;
        try {
            var query = syncRequest('GET', 'https://indacoin.com/api/GetExchangerAmount/USD/100', {
                timeout: SERVER_TIMEOUT
            });
            console.log('req status = ' + query.statusCode);
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var request = query.getBody();
                cachedData[path] = request;
                var query = { route: path };
                var newData = { route: path, content: request };
                DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) { // Если ошибка
                        res.send(500, {error: err});
                        return 0;
                    }
                    if (doc) { // Если найдена запись
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(doc.content);
                        return 0;
                    } else { // Иначе создаем новую
                        var record = new DataModel({ route: path, content: 'content for ' + path });
                        record.save(function(error) {
                            if (error) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send("Error save data" + error);
                                return 0;
                            } else {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send('record save with path ' + path);
                                return 0;
                            }
                        });
                    }
                });
            } else { // Если запрос к API неудачен
                if (cachedData[path] === undefined) {
                    DataModel.findOne({ route: path }, function(err, docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (docs) {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        } else {
                            var record = new DataModel({ route: path, content: 'content for ' + path });
                            record.save(function(error) {
                                if (error) {
                                    console.log("Error save data" + error);
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + path);
                                    return 0;
                                }
                            });
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedData[path]);
                    return 0;
                }
            }
        } catch (e) {
            console.log('error = ' + e);
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        }
    } else {
        console.log('Кеш не просрочен ' + elapsedTime);
        if (cachedData[path] === undefined) {
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedData[path]);
            return 0;
        }
    }
});

app.get('/api/GetExchangerAmount/EUR/100', function(req,res) {
    const path = '/api/GetExchangerAmount/EUR/100';
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_5) {
        console.log('Кеш просрочен: ' + path);
        getDataCacheTime[path] = currentTime;
        try {
            var query = syncRequest('GET', 'https://indacoin.com/api/GetExchangerAmount/EUR/100', {
                timeout: SERVER_TIMEOUT
            });
            console.log('req status = ' + query.statusCode);
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var request = query.getBody();
                cachedData[path] = request;
                var query = { route: path };
                var newData = { route: path, content: request };
                DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) { // Если ошибка
                        res.send(500, {error: err});
                        return 0;
                    }
                    if (doc) { // Если найдена запись
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(doc.content);
                        return 0;
                    } else { // Иначе создаем новую
                        var record = new DataModel({ route: path, content: 'content for ' + path });
                        record.save(function(error) {
                            if (error) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send("Error save data" + error);
                                return 0;
                            } else {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send('record save with path ' + path);
                                return 0;
                            }
                        });
                    }
                });
            } else { // Если запрос к API неудачен
                if (cachedData[path] === undefined) {
                    DataModel.findOne({ route: path }, function(err, docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (docs) {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        } else {
                            var record = new DataModel({ route: path, content: 'content for ' + path });
                            record.save(function(error) {
                                if (error) {
                                    console.log("Error save data" + error);
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + path);
                                    return 0;
                                }
                            });
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedData[path]);
                    return 0;
                }
            }
        } catch (e) {
            console.log('error = ' + e);
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        }
    } else {
        console.log('Кеш не просрочен ' + elapsedTime);
        if (cachedData[path] === undefined) {
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedData[path]);
            return 0;
        }
    }
});

app.get('/api/GetExchangerAmount/RUB/3000', function(req,res) {
    const path = '/api/GetExchangerAmount/RUB/3000';
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getDataCacheTime[path]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_6) {
        console.log('Кеш просрочен: ' + path);
        getDataCacheTime[path] = currentTime;
        try {
            var query = syncRequest('GET', 'https://indacoin.com/api/GetExchangerAmount/RUB/3000', {
                timeout: SERVER_TIMEOUT
            });
            console.log('req status = ' + query.statusCode);
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var request = query.getBody();
                cachedData[path] = request;
                var query = { route: path };
                var newData = { route: path, content: request };
                DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) { // Если ошибка
                        res.send(500, {error: err});
                        return 0;
                    }
                    if (doc) { // Если найдена запись
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(doc.content);
                        return 0;
                    } else { // Иначе создаем новую
                        var record = new DataModel({ route: path, content: 'content for ' + path });
                        record.save(function(error) {
                            if (error) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send("Error save data" + error);
                                return 0;
                            } else {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send('record save with path ' + path);
                                return 0;
                            }
                        });
                    }
                });
            } else { // Если запрос к API неудачен
                if (cachedData[path] === undefined) {
                    DataModel.findOne({ route: path }, function(err, docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (docs) {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        } else {
                            var record = new DataModel({ route: path, content: 'content for ' + path });
                            record.save(function(error) {
                                if (error) {
                                    console.log("Error save data" + error);
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + path);
                                    return 0;
                                }
                            });
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedData[path]);
                    return 0;
                }
            }
        } catch (e) {
            console.log('error = ' + e);
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        }
    } else {
        console.log('Кеш не просрочен ' + elapsedTime);
        if (cachedData[path] === undefined) {
            DataModel.findOne({ route: path }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new DataModel({ route: path, content: 'content for ' + path });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + path);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedData[path]);
            return 0;
        }
    }
});

app.get('/api/test', function(req, res) {
    TitleModel.findOne({ route: 'en/bullish_run' },function(err, docs) {
        if (err) {
            res.send(500, {error: err});
            console.log(err);
            return 0;
        }
        else {
            res.header("Content-Type", "text/html; charset=utf-8");
            console.log(docs);
            res.send(docs);
            return 0;
        }
    });
});


// api/getarticles/en/bullish_run
app.get('/api/getarticle/:id_1/:id_2', function(req,res) {
    const lang = req.params.id_1;
    const title = req.params.id_2;
    const route = lang + '/' + title;
    if (languages.indexOf(lang) === -1) {
        res.send(500, {error: 'lang error'});
        return 0;
    }
    let currentTime = new Date().getTime();

    let lastTime = 0;
    if (getArticlesConcreteCacheTime[lang][title] !== undefined)
    lastTime = getArticlesConcreteCacheTime[lang][title];

    let elapsedTime = (currentTime - lastTime) / 1000;
    res.type('json');
    if (elapsedTime > TIME_3) {
        console.log('Кеш просрочен: ' + elapsedTime + ' для языка ' + lang);
        getArticlesConcreteCacheTime[lang][title] = currentTime;
        try {
            var query = syncRequest('GET', 'https://indacoin.com/api/getarticle/' + route, {
                timeout: SERVER_TIMEOUT
            });
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                console.log('answer = 200 || answer = 304');
                var r = query.getBody();
                cachedGetArticles[lang][title] = r;
                var query = { 'route': route };
                var newData = { route: route, content: r };

                TitleModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) { // Если ошибка
                        res.send(500, {error: err});
                        return 0;
                    }
                    if (doc) { // Если найдена запись
                        res.header("Content-Type", "text/html; charset=utf-8");
                        res.send(doc.content);
                        return 0;
                    } else { // Иначе создаем новую
                        var record = new TitleModel({ route: route, content: 'content for ' + route });
                        record.save(function(error) {
                            if (error) {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send("Error save data" + error);
                                return 0;
                            } else {
                                res.header("Content-Type", "text/html; charset=utf-8");
                                res.send('record save with path ' + route);
                                return 0;
                            }
                        });
                    }
                });


            } else {
                console.log('answer != 200');
                if (cachedGetArticles[lang][title] === undefined) {
                    TitleModel.findOne({ route: route }, function(err, docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        if (docs) {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        } else {
                            var record = new TitleModel({ route: route, content: 'content for ' + route });
                            record.save(function(error) {
                                if (error) {
                                    console.log("Error save data" + error);
                                } else {
                                    res.header("Content-Type", "text/html; charset=utf-8");
                                    res.send('record save with path ' + route);
                                    return 0;
                                }
                            });
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedGetArticles[lang][title]);
                    return 0;
                }
            }
        } catch (e) {
            /*TitleModel.findOne({ route: route },function(err,docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                }
            });*/

            console.log('error = ' + e);
            TitleModel.findOne({ route: route }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new TitleModel({ route: route, content: 'content for ' + route });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + route);
                            return 0;
                        }
                    });
                }
            });

        }
    } else {
        console.log('Кеш еще не просрочен: ' + elapsedTime + ' для языка ' + lang);
        if (cachedGetArticles[lang][title] === undefined) {
            TitleModel.findOne({ route: route }, function(err, docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                if (docs) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                } else {
                    var record = new TitleModel({ route: route, content: 'content for ' + route });
                    record.save(function(error) {
                        if (error) {
                            console.log("Error save data" + error);
                        } else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send('record save with path ' + route);
                            return 0;
                        }
                    });
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedGetArticles[lang][title]);
        }
    }    
});

app.get('/api/getarticles/:id', function(req,res) {
    const lang = req.params.id;
    if (languages.indexOf(lang) === -1) {
        res.send(500, {error: 'lang error'});
        return 0;
    }
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - getArticlesCacheTime[lang]) / 1000;
    res.type('json');
    if (elapsedTime > TIME_2) {
        console.log('Кеш просрочен: ' + elapsedTime + ' для языка ' + lang);
        getArticlesCacheTime[lang] = currentTime;
        try {
            var query = syncRequest('GET', 'https://indacoin.com/api/getarticles/' + lang, {
                timeout: SERVER_TIMEOUT
            });
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var r = query.getBody();
                cachedGetArticles[lang] = r;
                var query = {'lang': lang};
                var newData = { lang: lang, content: r};
                ArticleModel.findOneAndUpdate(query, newData, { upsert: false }, function(err, doc){
                    if (err) {
                        return res.send(500, { error: err });
                    }
                    res.header("Content-Type", "text/html; charset=utf-8");
                    return res.send(r);
                });
            } else {
                if (cachedGetArticles[lang] === undefined) {
                    ArticleModel.findOne({ lang: lang },function(err,docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedGetArticles[lang]);
                    return 0;
                }
            }
        } catch (e) {
            ArticleModel.findOne({ lang: lang },function(err,docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                }
            });
        }
    } else {
        console.log('Кеш еще не просрочен: ' + elapsedTime + ' для языка ' + lang);
        if (cachedGetArticles[lang] === undefined) {
            ArticleModel.findOne({ lang: lang },function(err,docs) {
                if (err) {
                    res.send(500, {error: err});
                }
                else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedGetArticles[lang]);
        }
    }
});

app.get('/api/getarticlesall', function(req,res) {
    ArticleModel.find({},function(err,docs) {
     if(err) {
      res.send({error:err});
     }
     else {
      res.send({data: docs});
     }
    });
   });

app.get('/api/mobgetcurrenciesinfoi/1', function(req,res) {
    console.log('get /api/mobgetcurrenciesinfoi/1');
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - time) / 1000;
    res.type('json');
    if (elapsedTime > TIME_1) {
        time = currentTime;
        try {
            console.log('============ Запрос mobgetcurrencies отправлен ================');
            var query = syncRequest('GET', 'https://indacoin.com/api/mobgetcurrenciesinfoi/1', {
                timeout: SERVER_TIMEOUT
            });
            console.log('============ Запрос mobgetcurrencies получен ================');
            if (String(query.statusCode) === '200' || String(query.statusCode) === '304') {
                var r = query.getBody();
                cachedMobgetcurrencies = r;
                var query = {'author': 'michael'};
                var newData = {title: 'title-3', content: r};
                NoteModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
                    if (err) {
                        return res.send(500, { error: err });
                    }
                    res.header("Content-Type", "text/html; charset=utf-8");
                    return res.send(r);
                });
            } else {
                if (cachedMobgetcurrencies === undefined) {
                    NoteModel.findOne({ author: 'michael' },function(err,docs) {
                        if (err) {
                            res.send(500, {error: err});
                            return 0;
                        }
                        else {
                            res.header("Content-Type", "text/html; charset=utf-8");
                            res.send(docs.content);
                            return 0;
                        }
                    });
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(cachedMobgetcurrencies);
                    return 0;
                }
            }
        } catch (e) {
            NoteModel.findOne({ author: 'michael' },function(err,docs) {
                if (err) {
                    res.send(500, {error: err});
                    return 0;
                }
                else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                    return 0;
                }
            });
        }
    } else {
        if (cachedMobgetcurrencies === undefined) {
            NoteModel.findOne({ author: 'michael' },function(err,docs) {
                if (err) {
                    res.send(500, {error:err});
                }
                else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send(docs.content);
                }
            });
        } else {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(cachedMobgetcurrencies);
        }
    }
});

app.get('/api/clear', function(req,res) {
    NoteModel.remove({},function(err) {
     if(err) {
      res.send({error:err});
     }
     else {
      res.send({status:'success'});
     }
    });
});

app.get('/api/notes', function(req,res) {
    NoteModel.find({},function(err,docs) {
     if(err) {
      res.send({error:err});
     }
     else {
      res.send({note:docs});
     }
    });
   });

app.get('/api/update', function(req, res) {
    let currentTime = new Date().getTime();
    let elapsedTime = (currentTime - time) / 1000;
    time = currentTime;
    var a = elapsedTime + ' sec\r\n';
    if (elapsedTime > 5) {
        a+='просрочено';
    }
    res.send(a); return 0;
    var query = {'author': 'michael'};
    var newData = {title: 'title-3', content: 'content-3'};
    NoteModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
        if (err) return res.send(500, { error: err });
        return res.send("succesfully saved");
    });
});

app.get('/api/add', function(req,res) {
    var record = new NoteModel({title: 'title-1', content: 'content-1', author: 'michael'});
    record.save(function(error) {
            console.log("Your bee has been saved!");
            res.send('Your bee has been saved!');
        if (error) {
            res.send(error);
        }
    });
});

app.get('/api/values', function(req,res) {
    var query = syncRequest('GET', 'https://indacoin.com/api/mobgetcurrenciesinfoi/1');
    var r = query.getBody();
    console.log(r);
    res.send(r);
});

app.listen('4500');
console.log('mongo server start');

//https://indacoin.com/api/mobgetcurrencies/

app.get('/api/test2', function(req, res) {
    /*const path = '/api/GetExchangerAmount/USD/1002';
    DataModel.findOne({ route: path }, function(err, docs) {
        if (err) {
            res.send(500, {error: err});
            return 0;
        }
        if (docs) {
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(docs.content);
            return 0;
        } else {
            var record = new DataModel({ route: path, content: 'content for ' + path });
            record.save(function(error) {
                if (error) {
                    console.log("Error save data" + error);
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send('record save with path ' + path);
                    return 0;
                }
            });
        }
    });*/

    const path = '/api/GetExchangerAmount/USD/1008';
    var request = 'data 3';
    var query = { route: path };
    var newData = { route: path, content: request };
    DataModel.findOneAndUpdate(query, newData, {upsert:false}, function(err, doc){
        if (err) { // Если ошибка
            res.send(500, {error: err});
            return 0;
        }
        if (doc) { // Если найдена запись
            res.header("Content-Type", "text/html; charset=utf-8");
            res.send(doc.content);
            return 0;
        } else { // Иначе создаем новую
            var record = new DataModel({ route: path, content: 'content for ' + path });
            record.save(function(error) {
                if (error) {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send("Error save data" + error);
                    return 0;
                } else {
                    res.header("Content-Type", "text/html; charset=utf-8");
                    res.send('record save with path ' + path);
                    return 0;
                }
            });
        }
    });


});

app.get('/api/fill', function(req, res) {
    

    ArticleModel.remove({},function() {});
    languages.forEach((lang)=>{
        var record = new ArticleModel(
            { content: 'content for ' + lang, lang: lang }
        );
        record.save(function(error) {
                console.log("Lang added: " + lang);
            if (error) {
                console.log("Lang error for : " + lang + " error: " + error);
            }
        });
    });

    DataModel.remove({},function() {});
    var data1_instance = new DataModel({ route: '/api/GetExchangerAmount/USD/100', content: 'data 1' });
    data1_instance.save(function (err) {
    if (err) return handleError(err);
        console.log('record 1 inserted');
    });

    var data2_instance = new DataModel({ route: '/api/GetExchangerAmount/EUR/100', content: 'data 2' });
    data2_instance.save(function (err) {
    if (err) return handleError(err);
        console.log('record 2 inserted');
    });

    var data3_instance = new DataModel({ route: '/api/GetExchangerAmount/RUB/3000', content: 'data 3' });
    data3_instance.save(function (err) {
    if (err) return handleError(err);
        console.log('record 3 inserted');
    });

    res.type('json');
    res.header("Content-Type", "text/html; charset=utf-8");
    res.send('data models is fullied');
    return 0;
});






app.get('/api/values2', function(req,res) {
    res.type('json');

    thenRequest('GET', 'https://indacoin.com/api/mobgetcurrenciesinfoi/1').done(function (res) {
        res.send(res);
    });
    
});






var stdin = process.openStdin();
stdin.addListener("data", function(data) {
    let input = data.toString().trim().toLowerCase();

    //console.log("you entered: [" + input + "]");

    if (input.slice(0, 1) === 's' && input.length > 2) {
        console.log('command:');
        console.log(server[input.split(' ')[1]]);
    } else {
        switch(input) {
            case 'info':
                console.log('cachedMobgetcurrencies:');
                console.log(cachedMobgetcurrencies);
                console.log('\ncachedGetArticles:');
                console.log(cachedGetArticles);
                console.log('\ncachedData:');
                console.log(cachedData);
                break
            case 'time':
                console.log('Time');

                console.log('time:');
                console.log(time);
                console.log('\ngetArticlesCacheTime:');
                console.log(getArticlesCacheTime);
                console.log('\ngetArticlesConcreteCacheTime:');
                console.log(getArticlesConcreteCacheTime);
                console.log('\ngetDataCacheTime:');
                console.log(getDataCacheTime);
                break
            default:
                console.log('Unknown comand: ' + input);
        }
    }

  });