#!/bin/bash
echo 'Start prod build'
time=$(date +%s)
ember build --env production
least=$(date +%s)
echo 'Prod build time: '$((least-time)) sec