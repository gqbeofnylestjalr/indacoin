/* eslint-env node */
'use strict';

const environment = process.env.EMBER_ENV;
const IS_PROD = environment === 'production';
const IS_DEV = environment === 'development';
const IS_TEST = environment === 'test';


// const IS_PROD = ENV.environment === 'production';
// const IS_DEV = ENV.environment === 'development';
// const IS_TEST = ENV.environment === 'test';

const EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  let app = new EmberApp(defaults, {

    // hinting: IS_TEST, // Disable linting for all builds but test
    // tests: IS_TEST, // Don't even generate test files unless a test build
    // "ember-cli-babel": {
    //   includePolyfill: IS_PROD // Only include babel polyfill in prod
    // },
    // autoprefixer: {
    //   sourcemap: IS_DEV // Was never helpful
    // },
    // sourcemaps: {
    //   enabled: IS_PROD // CMD ALT F in chrome is *almost* as fast as CMD P
    //   //enabled: false // CMD ALT F in chrome is *almost* as fast as CMD P
    // },

    'esw-cache-rendered': {
      // changing this version number will bust the cache
      version: '1'
    },

    // Ignore images and other resources whom need static links
    fingerprint: {
      exclude: ['assets/exclude/']
    },

    // babel: {
    //   includePolyfill: true
    // },

    // TODO: отключить в проде
    minifyJS: {
      enabled: true
    },
    minifyCSS: {
      enabled: true
    },
    
    "ssl": true,
    vendorFiles: {
      "jquery": false
    },
    sassOptions: {
      extension: 'sass'
    },
    outputPaths: {
      vendor: {
        css: '/assets/ember.css',
        js: '/assets/ember.js'
      },
    },
    // Add options here
  });

  app.import('vendor/lazy.js');

  //app.import('vendor/protocolcheck.js')

  app.import('vendor/lib/cookies/cookies.js', {
    using: [
      { transformation: 'amd', as: 'cookies' }
    ]
  });

  app.import('vendor/lib/google-analytics/google-analytics.js', {
    using: [
      { transformation: 'amd', as: 'google-analytics' }
    ]
  });

  //app.import('public/ember/scripts/semantic.min.js');
  
  //app.import('https://unpkg.com/scrollreveal/dist/scrollreveal.min.js');
  //app.import('https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js', {outputFile: 'assets/additional-script.js'});
  //app.import('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js', {outputFile: 'assets/additional-script.js'});


  //app.import('assets/test.js');

  //app.import('bower_components/scripts/test.js');

  //app.import('vendor/RecordRTC.js')
  //app.import('ember/scripts/RecordRTC.js');
  //app.import('bower_components/scripts/RecordRTC.js');
  

  //app.import('bower_components/scripts/RecordRTC.js');
  //app.import('bower_components/scripts/fingerprint2.js');
  //app.import('bower_components/scripts/jivo.js');

  app.import('bower_components/semantic/dist/semantic.css');

  
  //app.import('bower_components/lodash/lodash.js', {outputFile: 'vendor3.js'});

  /*app.import('bower_components/fonts/helvetica/helveticaneuecyr-header.otf');
  app.import('bower_components/fonts/helvetica/helveticaneuecyr-bold.otf');
  app.import('bower_components/fonts/helvetica/helveticaneuecyr-light.otf');
  app.import('bower_components/fonts/helvetica/helveticaneuecyr-roman.otf');
  app.import('bower_components/fonts/helvetica/helveticaneuecyr-ultralight.otf');*/
  
  //app.import('bower_components/scripts/finger.js');
  //app.import('bower_components/scripts/video.js');

  //app.import('bower_components/scripts/finger.js');
  app.import('bower_components/scripts/socialfingerprint.js');
  //app.import('bower_components/scripts/fingerprint2.js');
  //app.import('bower_components/scripts/fontdetect.js');
  //app.import('bower_components/scripts/plugindetect.js');
  //app.import('bower_components/scripts/sha1.js');
  //app.import('bower_components/scripts/fingerprint_by_pet_portal.js');
  

  /*app.import('bower_components/scripts/fingerprint_by_pet_portal.js');
  app.import('bower_components/scripts/fingerprint2.js');
  app.import('bower_components/scripts/fontdetect.js');
  app.import('bower_components/scripts/plugindetect.js');
  app.import('bower_components/scripts/sha1.js');
  app.import('bower_components/scripts/socialfingerprint.js');*/


  // Import Semantic UI css elements
  // app.import('bower_components/semantic/dist/reset.css');
  // app.import('bower_components/semantic/dist/grid.css');
  // app.import('bower_components/semantic/dist/container.css');
  // app.import('bower_components/semantic/dist/breadcrumb.css');
  // app.import('bower_components/semantic/dist/button.css');
  // app.import('bower_components/semantic/dist/sticky.css');
  // app.import('bower_components/semantic/dist/sidebar.css');
  // app.import('bower_components/semantic/dist/segment.css');
  // app.import('bower_components/semantic/dist/modal.css');
  // app.import('bower_components/semantic/dist/message.css');
  // app.import('bower_components/semantic/dist/menu.css');
  // app.import('bower_components/semantic/dist/loader.css');
  // app.import('bower_components/semantic/dist/label.css');
  // app.import('bower_components/semantic/dist/input.css');
  // app.import('bower_components/semantic/dist/image.css');
  // app.import('bower_components/semantic/dist/icon.css');
  // app.import('bower_components/semantic/dist/form.css');
  // app.import('bower_components/semantic/dist/dropdown.css');
  // app.import('bower_components/semantic/dist/dimmer.css');
  // app.import('bower_components/semantic/dist/divider.css');
  // app.import('bower_components/semantic/dist/header.css');
  // Use `app.import` to add additional libraries to the generated
  // output files.
  //
  // If you need to use different assets in different
  // environments, specify an object as the first parameter. That
  // object's keys should be the environment name and the values
  // should be the asset to use in that environment.
  //
  // If the library that you are including contains AMD or ES6
  // modules that you would like to import into your application
  // please specify an object with the list of modules as keys
  // along with the exports of each module as its value.

  return app.toTree();
};
