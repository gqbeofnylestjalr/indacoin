/* eslint-env node */
'use strict';

module.exports = function(environment) {
  
  let ENV = {
    emberSmartBanner: {
      // appStoreLinkBase: 'https://itunes.apple.com',
      // link: 'https://itunes.apple.com',
      // descriptionIOS: 'FREE - On the App Store',
      descriptionAndroid: 'FREE - In Google Play',
      linkText: 'VIEW',
      // appIdIOS: '1255080553',
      openAfterClose: '1',
      marketLinkBase: 'market://details?id=',
      appIdAndroid: 'app-id=com.indacoin.android',
      appStoreLanguage: 'en',
      iconUrl: 'https://indacoin.com/ember/images/indacoin-logo--border.svg'
    },
/*
    <meta name="google-play-app" content="app-id=com.indacoin.android">
    <link rel="android-touch-icon" href="//indacoin.com/ex/androidApp/icon.png" />
    <!-- Start SmartBanner configuration -->
    <meta name="smartbanner:title" content="Indacoin wallet">
    <meta name="smartbanner:author" content="Indacoin LTD">
    <meta name="smartbanner:price" content="FREE">
    <meta name="smartbanner:price-suffix-apple" content=" - On the App Store">
    <meta name="smartbanner:price-suffix-google" content=" - In Google Play">
    <meta name="smartbanner:icon-apple" content="//indacoin.com/ex/androidApp/iconios.jpg">
    <meta name="smartbanner:icon-google" content="//indacoin.com/ex/androidApp/icon.png">
    <meta name="smartbanner:button" content="VIEW">
    <meta name="smartbanner:button-url-apple" content="https://itunes.apple.com/ru/app/indacoin-bitcoin-wallet-and-store/id1255080553?l=en&mt=8">
    <meta name="smartbanner:button-url-google" content="https://play.google.com/store/apps/details?id=com.indacoin.android">
    <meta name="smartbanner:enabled-platforms" content="android,ios">
    <meta name="smartbanner:disable-positioning" content="true">
*/

    baseURL: '/',

    contentSecurityPolicy: {
      'connect-src' : "'self' http://localhost:4500"
    },

    fastboot: {
      hostWhitelist: [
        /^localhost:\d+$/,
        'http://localhost:4200/ru_US',
        'http://localhost:4200',
        '127.0.0.1:4200',




        'www.indacoin.com',
        'indacoin.com',

        'ar.indacoin.com',
        'de.indacoin.com',
        'dz.indacoin.com',
        'ar.indacoin.com',
        'en.indacoin.com',
        'es.indacoin.com',
        'fr.indacoin.com',
        'it.indacoin.com',
        'pt.indacoin.com',
        'ru.indacoin.com',
        'tr.indacoin.com',

        'indacoin.io',
        'ar.indacoin.io',
        'de.indacoin.io',
        'dz.indacoin.io',
        'ar.indacoin.io',
        'en.indacoin.io',
        'es.indacoin.io',
        'fr.indacoin.io',
        'it.indacoin.io',
        'pt.indacoin.io',
        'ru.indacoin.io',
        'tr.indacoin.io',
        'www.indacoin.org',
        'indacoin.org',
        'www.indacoin.net',
        'indacoin.net',
        'mail.indacoin.com',
        'cdn.indacoin.com'
      ]
    },

    pageTitle: {
      replace: true
    },
    i18n: {
      defaultLocale: 'en',
      lang: 'en_GB' // Global variable for default language
    },
    isfastboot: false, // true - показываем заглушку на сервере (пришли confirm code и выполение на сервере)
    modulePrefix: 'fastboot',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      },
      LOG_STACKTRACE_ON_DEPRECATION: false
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    }
  };


  /*
    1) Все ссылки абсолютные https:www.indacoin/api/mobgetcurrencies
    2) Логи выводятся
    3) Код не минифицируется
  */
 //console.log('environment = ' + environment);
  // Default: development
  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;

    // ENV.yappConfigKey = 'dev';
    // ENV['ember-cli-mirage'] = {
    //   enabled: false,
    //   excludeFilesFromBuild: true // Don't include lodash and everything else mirage needs
    // };
      
      // console.log = Math.log;

  }

  /*
    1) Все ссылки абсолютные https:indacoin/api/mobgetcurrencies
    2) Логи выводятся
  */
  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  /*
    1) Все ссылки относительные /api/mobgetcurrencies
    2) Логи не выводятся
    3) Код минифицируется

  */
  if (environment === 'production') {

    // if (typeof FastBoot === 'undefined') {
    //   console.log = Math.log;
    // }

  }

  return ENV;
};
