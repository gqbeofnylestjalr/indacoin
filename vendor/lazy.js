if (typeof(document) !== "undefined") {

!function (e, t, r) { function n() { for (; d[0] && "loaded" == d[0][f];) c = d.shift(), c[o] = !i.parentNode.insertBefore(c, i) } for (var s, a, c, d = [], i = e.scripts[0], o = "onreadystatechange", f = "readyState"; s = r.shift() ;) a = e.createElement(t), "async" in i ? (a.async = !1, e.head.appendChild(a)) : i[f] ? (d.push(a), a[o] = n) : e.write("<" + t + ' src="' + s + '" defer></' + t + ">"), a.src = s }(document, "script", [
    // "/ex/js/scripts/jquery-2.1.0.min.js",
    // '<%: System.Web.Optimization.Scripts.Url("~/ex/bundles/scripts/" + Exchanger.BasePage.cutLangFromUrl())%>',
    // "/signalR/hubs",
    // "https://www.youtube.com/iframe_api",
    // "https://cdn.onesignal.com/sdks/OneSignalSDK.js",

    "/assets/exclude/scripts/lazy-load-scripts.js",

    // Concrete target load
    //"/assets/exclude/scripts/RecordRTC.js",

    "https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.11/jquery.mask.min.js",
    "https://cdnjs.cloudflare.com/ajax/libs/scrollReveal.js/3.4.0/scrollreveal.min.js"
])

}