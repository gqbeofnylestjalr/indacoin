define(function() {

  const sendAnalitics = function sendAnalitics(eventCategory, eventAction, eventLabel, eventValue) {
    try {
      ga('send', 'event', { eventCategory: eventCategory, eventAction: eventAction, eventLabel: eventLabel, eventValue: eventValue });
      console.log('GA send succesfully');
    } catch (e) {
      console.log('GA not sending');
    }
  }

  return {
    sendAnalitics: sendAnalitics
  };

});